@extends('layouts.back.master')@section('title','Shop Items')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }
    </style>
@stop

@section('content')

        <div class="row mb-2">
            @if(Auth::user()->hasRole(['Owner','Sales Agent','Super Admin']))
                <div class="form-group col-sm-3">
                    <input type="hidden" name="agent" id="agent" value="{{Auth::user()->id}}">

                </div>
            @else
            <div class="col-md-3">

                {!! Form::select('name_with_initials', $agents , null , ['class' => 'form-control','placeholder'=>'Select Agent','id'=>'agent']) !!}

            </div>
            @endif
            <div class="col-md-2">

                {!! Form::select('raw_material',$raw_materials , null , ['class' => 'form-control','placeholder'=>'Select Type Of Crate','id'=>'raw_material']) !!}

            </div>



            <!--div class="col-md-2">
                <select name="type" class="form-control select2 ">
                    <option value="">Select Move Type</option>
                    <option value="">Loaded</option>
                    <option value="">Purchased</option>
                    <option value="">Destroyed</option>


                </select>
            </div-->

            <div class="col-md-3">
                <input type="text" name="date" id="date_range" class="form-control" placeholder="Select Date" required>
            </div>

            <div class="col-md-2">
                <button class="btn btn-info " onclick="process_form(this)">Filter</button>
            </div>

            <!-- loading view -->


        </div>

    <div class="table-responsive">
        <table id="rawi_table" class="display text-center ">
            <thead>
            <tr>

                <th>Date & Time</th>
                <th>Agent</th>
                <th>Item</th>
                <th>Qty</th>
                <th>Value</th>
                <th>Action</th>
            </tr>
            </thead>
            <tfoot>
            <tr>

                <th>Date & Time</th>
                <th>Agent</th>
                <th>Item</th>
                <th>Qty</th>
                <th>Value</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
    </div>

@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script>
        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019",
            "maxDate":moment()
        });


        //$('#date_range').val('')
        $(document).ready(function () {
            table = $('#rawi_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "ajax": {
                    url: "{{url('/shopitems/list/table/data')}}",
                    type: "get",
                    error: function () {
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    }
                },
                pageLength: 100,
                responsive: true
            });
        });

        function process_form(e) {
            let raw_material = $("#raw_material").val();
            let table = $('#rawi_table').DataTable();
            let date_range = $("#date_range").val();
            let agent = $("#agent").val();

            table.ajax.url('/shopitems/list/table/data?raw_material=' + raw_material + '&date_range=' + date_range  + '&agent=' + agent + '&filter=' + true).load();
        }


        function process_form_reset() {
            $("#raw_material").val('');
            $("#agent").val('');
            let table = $('#rawi_table').DataTable();
            table.ajax.url('shopitems/list/table/data').load();
        }

    </script>

@stop

