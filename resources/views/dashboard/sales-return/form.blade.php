<div class="form-group">
    <div class="row mb-1">
        <div class="col-md-3 text-right mt-2">
            <label>Chart Model</label>
        </div>
        <div class="col-md-9">
            {!! Form::select('chart_model',['periodical'=>'Periodical','comparison'=>'Comparison'],'periodical',['class'=>'form-control chart_model','id'=>'chart_model']) !!}
        </div>
    </div>
    <div class="row mb-1">
        <div class="col-md-3 text-right mt-2">
            <label>Type</label>
        </div>
        <div class="col-md-9">
            {!! Form::select('type',['qty-wise'=>'QTY Wise','value-wise'=>'Value Wise'],'qty-wise',['class'=>'form-control type','id'=>'type']) !!}
        </div>
    </div>

    <div class="row mb-1">
        <div class="col-md-3 text-right mt-2">
            <label>Chart Type </label>
        </div>
        <div class="col-md-9">
            {!! Form::select('chart_type',['agent-wise'=>'Agent','category-wise'=>'Category','end-product-wise'=>'End Product','return-type-wise'=>'Return Type','reusable-wise'=>'Reusable'],null,['class'=>'form-control chart_type','placeholder'=>'Price/QTY','id'=>'chart_type']) !!}
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 text-right mt-2">
            <label>Date Range</label>
        </div>
        <div class="col-md-9">
            <input type="text" name='date_range' class="form-control date_input date_range" required>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 text-right mt-2">
            <label>Compare Date</label>
        </div>
        <div class="col-md-9">
            <input type="text" name='compare_start_date' class="form-control compare_start_date date_range" required>
        </div>
    </div>

    <div class="row mb-1">
        <div class="col-md-3 text-right mt-2">
            <label>Period</label>
        </div>
        <div class="col-md-9">
            {!! Form::select('period',['daily'=>'Daily','weekly'=>'Weekly','monthly'=>'Monthly','quarterly'=>'Quarterly','annually'=>'Annually'],'daily',['class'=>'form-control period','id'=>'period']) !!}
        </div>
    </div>


    <div class="row mb-1">
        <div class="col-md-3 text-right mt-2">
            <label>Return Type</label>
        </div>
        <div class="col-md-9">
            {!! Form::select('return_type',[2=>'By System Generated',3=>'By Agent',4=>'EP Merge Removals',5=>'Stores Return'], null , ['class' => 'form-control select2','placeholder'=>'Any','multiple','id'=>'return_type']) !!}
        </div>
    </div>

    <div class="row mb-1">
        <div class="col-md-3 text-right mt-2">
            <label>Days</label>
        </div>
        <div class="col-md-9">
            {!! Form::select('day_type',[0=>'Monday',1=>'Tuesday',2=>'Wednesday',3=>'Thursday',4=>'Friday',5=>'Saturday',6=>'Sunday'], [0,1,2,3,4,5,6] , ['class' => 'form-control  day_type select2','multiple','id'=>'day_type']) !!}
        </div>
    </div>



    <div class="row">
        <div class="col-md-3 text-right mt-2">
            <label>Agent</label>
        </div>
        <div class="col-md-9">
            {!! Form::select('agents',$agents,null,['class'=>'form-control agents select2','multiple','id'=>'agents','placeholder'=>'Any']) !!}
        </div>
    </div>
    <div class="row mb-1">
        <div class="col-md-3 text-right mt-2">
            <label>Category</label>
        </div>
        <div class="col-md-9">
            {!! Form::select('categories',$categories,null,['class'=>'form-control categories select2','multiple','id'=>'categories','placeholder'=>'Any']) !!}
        </div>
    </div>
    <div class="row mb-1">
        <div class="col-md-3 text-right mt-2">
            <label>End Products</label>
        </div>
        <div class="col-md-9">
            {!! Form::select('end_product',$endProducts,null,['class'=>'form-control end_products select2','multiple','id'=>'end_products','placeholder'=>'Any']) !!}
        </div>
    </div>

    <div class="row mb-1">
        <div class="col-md-3 text-right mt-2">
            <label>Reusable Type</label>
        </div>
        <div class="col-md-9">
            {!! Form::select('reuse_type',['re_use'=>'Re Use','destroy'=>'Destroy','stock_rotation'=>'Stock Rotation','factory_fault'=>'Factory Fault','lost'=>'Lost QTY'], null , ['class' => 'form-control select2 reuse_type','placeholder'=>'Any','multiple','id'=>'reuse_type']) !!}
        </div>
    </div>

</div>
