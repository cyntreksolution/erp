@extends('layouts.back.dashboard-master')@section('title','Sales Return | Dashboard')

@section('content')
    <div class="row">
        <div class="col-md-11"></div>
        <div class="col-md-1 text-right float-right p-0">
            <button class="btn btn-icon btn-primary btn-icon-style-1 pull-right mr-5" data-toggle="modal"
                    data-target="#reportFilterModal">
                <span class="btn-icon-wrap"><i class="fa fa-filter"></i></span>
            </button>
        </div>
    </div>

    <div class="col-md-12 mt-1">
        <div id="main-chart"></div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6 p-2">
                <div style="border: 1px solid grey;border-radius: 15px">
                    <h6 class="m-2" id="product_title">Product Wise</h6>
                    <div id="product-pie-chart"></div>
                </div>
            </div>
            <div class="col-md-6 p-2">
                <div style="border: 1px solid grey;border-radius: 15px">
                    <h6 class="m-2" id="agent_title">Agent Wise</h6>
                    <div id="agent-donut-chart"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6 p-2">
                <div style="border: 1px solid grey;border-radius: 15px">
                    <h6 class="m-2" id="category_title">Category Wise</h6>
                    <div id="category-chart"></div>
                </div>
            </div>
            <div class="col-md-6 p-2">
                <div style="border: 1px solid grey;border-radius: 15px">
                    <h6 class="m-2" id="re_usable_title">Re Usable Type Wise</h6>
                    <div id="re-usable-chart"></div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="reportFilterModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header p-3">
                    <h5 class="modal-title">Filter</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body mt-3 mb-3">
                    {!! Form::open(['id' => 'reportFilterForm','url'=>'/reports/country-wise-traffic/excel/data','class'=>'needs-validation','novalidate','target'=>'_blank']) !!}
                    @include('dashboard.sales-return.form')
                    <div class="float-right">
                        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
                        <button
                                onclick="submitFilter()"
                                type="button" class="btn btn-success">Filter
                        </button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@stop
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <style>
        #main-chart {
            width: 100%;
            height: 700px;
        }

        #product-pie-chart {
            width: 100%;
            height: 500px;
        }

        #agent-donut-chart {
            width: 100%;
            height: 500px;

        }
        #category-chart {
            width: 100%;
            height: 500px;
        }

        #re-usable-chart {
            width: 100%;
            height: 500px;

        }
    </style>
@stop

@section('js')
    <script src="{{asset('js/amcharts4/core.js')}}"></script>
    <script src="{{asset('js/amcharts4/charts.js')}}"></script>
    <script src="{{asset('js/amcharts4/themes/animated.js')}}"></script>
    <script src="{{asset('js/amcharts4/themes/material.js')}}"></script>

    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>

    <script>
        submitFilter = () => {
            let date_range = $("#reportFilterForm").find(".date_range").val();
            let agents = $("#reportFilterForm").find(".agents").val();
            let categories = $("#reportFilterForm").find(".categories").val();
            let return_type = $("#reportFilterForm").find(".return_type").val();
            let reuse_type = $("#reportFilterForm").find(".reuse_type").val();
            let days = $("#reportFilterForm").find(".day_type").val();
            let period = $("#reportFilterForm").find(".period").val();
            let type = $("#reportFilterForm").find(".type").val();
            let end_product = $("#reportFilterForm").find(".end_products").val();
            $("#reportFilterModal").modal('toggle');
            mainChart(date_range, agents, categories, return_type, reuse_type, period, type,end_product,days);
            productPieChart(date_range, agents, categories, return_type, reuse_type, period, type,end_product,days);
            agentDonutChart(date_range, agents, categories, return_type, reuse_type, period, type,end_product,days);
            categoryDonutChart(date_range, agents, categories, return_type, reuse_type, period, type,end_product,days);
            reusableDonutChart(date_range, agents, categories, return_type, reuse_type, period, type,end_product,days);
        }

        $(".select2").select2({
            multiple: true,
            tags: true,
            tokenSeparators: [',', ' ']
        });


        $('.date_input').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "startDate": "{{\Carbon\Carbon::now()->subMonths(3)->format('Y-m-d')}}",
            "endDate": "{{\Carbon\Carbon::now()->format('Y-m-d')}}",
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019",
            "maxDate": moment()
        });
        $('.compare_start_date').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "startDate": "{{\Carbon\Carbon::now()->subMonths(3)->format('Y-m-d')}}",
            "endDate": "{{\Carbon\Carbon::now()->format('Y-m-d')}}",
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019",
            "maxDate": moment()
        });


        $('.date_input').val('{{\Carbon\Carbon::now()->subMonths(3)->format('Y-m-d')}} - {{\Carbon\Carbon::now()->format('Y-m-d')}}')

        mainChart();
        productPieChart();
        agentDonutChart();
        categoryDonutChart();
        reusableDonutChart();

        function mainChart(date_range = '{{\Carbon\Carbon::now()->subMonths(3)->format('Y-m-d')}} - {{\Carbon\Carbon::now()->format('Y-m-d')}}', agents = null, categories = null, return_type = null, reuse_type = null, period = 'daily', type = 'qty-wise',end_product=null,days=null) {
            $.ajax({
                url: "/dashboard/sales-return/main-chart",
                type: "get",
                async: true,
                data: {
                    date_range: date_range,
                    agents: agents,
                    categories: categories,
                    return_type: return_type,
                    reuse_type: reuse_type,
                    period: period,
                    type: type,
                    days: days,
                    end_product: end_product,
                },
                success: function (data) {
                    let response = JSON.parse(data);
                    am4core.ready(function () {


                        am4core.useTheme(am4themes_animated);


                        var chart = am4core.create("main-chart", am4charts.XYChart);
                        chart.scrollbarX = new am4charts.XYChartScrollbar();


                        chart.data = response;


                        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                        categoryAxis.dataFields.category = "new_date";
                        categoryAxis.renderer.grid.template.location = 0;
                        categoryAxis.renderer.minGridDistance = 30;
                        categoryAxis.renderer.labels.template.horizontalCenter = "right";
                        categoryAxis.renderer.labels.template.verticalCenter = "middle";
                        categoryAxis.renderer.labels.template.rotation = 270;
                        categoryAxis.tooltip.disabled = true;
                        categoryAxis.renderer.minHeight = 110;

                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                        valueAxis.renderer.minWidth = 50;


                        var series = chart.series.push(new am4charts.ColumnSeries());
                        series.sequencedInterpolation = true;
                        series.dataFields.valueY = "qty";
                        series.dataFields.categoryX = "new_date";
                        series.tooltipText = "qty : [{categoryX}: bold]{valueY}[/]";
                        series.columns.template.strokeWidth = 0;

                        series.tooltip.pointerOrientation = "vertical";

                        series.columns.template.column.cornerRadiusTopLeft = 10;
                        series.columns.template.column.cornerRadiusTopRight = 10;
                        series.columns.template.column.fillOpacity = 0.8;

                        // var series2 = chart.series.push(new am4charts.ColumnSeries());
                        // series2.sequencedInterpolation = true;
                        // series2.dataFields.valueY = "price";
                        // series2.dataFields.categoryX = "new_date";
                        // series2.tooltipText = "price : [{categoryX}: bold]{valueY}[/]";
                        // series2.columns.template.strokeWidth = 0;
                        //
                        // series2.tooltip.pointerOrientation = "vertical";

                        // series2.columns.template.column.cornerRadiusTopLeft = 10;
                        // series2.columns.template.column.cornerRadiusTopRight = 10;
                        // series2.columns.template.column.fillOpacity = 0.8;

                        chart.scrollbarX.series.push(series);


                        var hoverState = series.columns.template.column.states.create("hover");
                        hoverState.properties.cornerRadiusTopLeft = 0;
                        hoverState.properties.cornerRadiusTopRight = 0;
                        hoverState.properties.fillOpacity = 1;

                        series.columns.template.adapter.add("fill", function (fill, target) {
                            return chart.colors.getIndex(target.dataItem.index);
                        });


                        chart.cursor = new am4charts.XYCursor();


                        series.columns.template.events.on("hit", function (ev) {
                            let date = ev.target.dataItem.categories.categoryX;
                            let date_string = date + ' - ' + date;
                            productPieChart(date_string);
                            agentDonutChart(date_string)
                            reusableDonutChart(date_string)
                            categoryDonutChart(date_string)
                        }, this);


                    });
                }

            });
        }

        function productPieChart(date_range = '{{\Carbon\Carbon::now()->subMonths(3)->format('Y-m-d')}} - {{\Carbon\Carbon::now()->format('Y-m-d')}}', agents = null, categories = null, return_type = null, reuse_type = null, period = 'daily', type = 'qty-wise',end_product=null,days=null) {
            $('#product_title').empty();
            $('#product_title').append('Product Wise '+date_range);
            $.ajax({
                url: "/dashboard/sales-return/product-pie-chart",
                type: "get",
                async: true,
                data: {
                    date_range: date_range,
                    agents: agents,
                    categories: categories,
                    return_type: return_type,
                    reuse_type: reuse_type,
                    period: period,
                    type: type,
                    end_product:end_product,
                    days: days,
                },
                success: function (data) {
                    let response = JSON.parse(data);
                    am4core.ready(function () {

                        am4core.useTheme(am4themes_animated);
                        var chart = am4core.create("product-pie-chart", am4charts.PieChart);


                        var pieSeries = chart.series.push(new am4charts.PieSeries());
                        pieSeries.dataFields.value = "qty";
                        pieSeries.dataFields.category = "name";

// Let's cut a hole in our Pie chart the size of 30% the radius
                        chart.innerRadius = am4core.percent(30);

// Put a thick white border around each Slice
                        pieSeries.slices.template.stroke = am4core.color("#fff");
                        pieSeries.slices.template.strokeWidth = 2;
                        pieSeries.slices.template.strokeOpacity = 1;
                        pieSeries.slices.template
                            // change the cursor on hover to make it apparent the object can be interacted with
                            .cursorOverStyle = [
                            {
                                "property": "cursor",
                                "value": "pointer"
                            }
                        ];

                        pieSeries.ticks.template.disabled = true;
                        pieSeries.alignLabels = false;
                        pieSeries.labels.template.text = "{value.percent.formatNumber('#.0')}%";
                        pieSeries.labels.template.radius = am4core.percent(-40);
                        pieSeries.labels.template.fill = am4core.color("white");

                        pieSeries.labels.template.adapter.add("radius", function(radius, target) {
                            if (target.dataItem && (target.dataItem.values.value.percent <= 5)) {
                                return 0;
                            }
                            return radius;
                        });

                        pieSeries.labels.template.adapter.add("fill", function(color, target) {
                            if (target.dataItem && (target.dataItem.values.value.percent > 1) && (target.dataItem.values.value.percent < 5)  ) {
                                return am4core.color("#000");
                            }
                            return color;
                        });

// Create a base filter effect (as if it's not there) for the hover to return to
                        var shadow = pieSeries.slices.template.filters.push(new am4core.DropShadowFilter);
                        shadow.opacity = 0;

// Create hover state
                        var hoverState = pieSeries.slices.template.states.getKey("hover"); // normally we have to create the hover state, in this case it already exists

// Slightly shift the shadow and make it more prominent on hover
                        var hoverShadow = hoverState.filters.push(new am4core.DropShadowFilter);
                        hoverShadow.opacity = 0.7;
                        hoverShadow.blur = 5;

// Add a legend
                        chart.legend = new am4charts.Legend();
                        chart.legend.position = "right";
                        chart.legend.scrollable = true;
                        chart.data = response

                    });
                }
            });
        }

        function agentDonutChart(date_range = '{{\Carbon\Carbon::now()->subMonths(3)->format('Y-m-d')}} - {{\Carbon\Carbon::now()->format('Y-m-d')}}', agents = null, categories = null, return_type = null, reuse_type = null, period = 'daily', type = 'qty-wise',end_product=null,days=null) {
            $('#agent_title').empty();
            $('#agent_title').append('Agent Wise '+date_range);
            $.ajax({
                url: "/dashboard/sales-return/agent-donut-chart",
                type: "get",
                async: true,
                data: {
                    date_range: date_range,
                    agents: agents,
                    categories: categories,
                    return_type: return_type,
                    reuse_type: reuse_type,
                    period: period,
                    type: type,
                    end_product:end_product,
                    days:days
                },
                success: function (data) {
                    let response = JSON.parse(data);
                    am4core.ready(function () {
                        am4core.useTheme(am4themes_animated);

                        var chart = am4core.create("agent-donut-chart", am4charts.PieChart);


                        chart.data = response


                        var pieSeries = chart.series.push(new am4charts.PieSeries());
                        pieSeries.dataFields.value = "qty";
                        pieSeries.dataFields.category = "name_with_initials";
                        pieSeries.innerRadius = am4core.percent(50);
                        pieSeries.ticks.template.disabled = true;
                        pieSeries.labels.template.disabled = true;

                        var rgm = new am4core.RadialGradientModifier();
                        rgm.brightnesses.push(-0.8, -0.8, -0.5, 0, -0.5);
                        pieSeries.slices.template.fillModifier = rgm;
                        pieSeries.slices.template.strokeModifier = rgm;
                        pieSeries.slices.template.strokeOpacity = 0.4;
                        pieSeries.slices.template.strokeWidth = 0;


                        chart.legend = new am4charts.Legend();
                        chart.legend.position = "right";
                        chart.legend.scrollable = true;
                    });
                }
            });
        }

        function categoryDonutChart(date_range = '{{\Carbon\Carbon::now()->subMonths(3)->format('Y-m-d')}} - {{\Carbon\Carbon::now()->format('Y-m-d')}}', agents = null, categories = null, return_type = null, reuse_type = null, period = 'daily', type = 'qty-wise',end_product=null,days=null) {
            $('#category_title').empty();
            $('#category_title').append('Category Wise '+date_range);
            $.ajax({
                url: "/dashboard/sales-return/category-donut-chart",
                type: "get",
                async: true,
                data: {
                    date_range: date_range,
                    agents: agents,
                    categories: categories,
                    return_type: return_type,
                    reuse_type: reuse_type,
                    period: period,
                    type: type,
                    end_product:end_product,
                    days:days
                },
                success: function (data) {
                    let response = JSON.parse(data);
                    am4core.ready(function() {
                        am4core.useTheme(am4themes_animated);

                        var chart = am4core.create("category-chart", am4charts.PieChart);
                        chart.startAngle = 160;
                        chart.endAngle = 380;


                        chart.innerRadius = am4core.percent(60);

                        chart.data = response


                        var pieSeries2 = chart.series.push(new am4charts.PieSeries());
                        pieSeries2.dataFields.value = "qty";
                        pieSeries2.dataFields.category = "name";
                        pieSeries2.slices.template.stroke = new am4core.InterfaceColorSet().getFor("background");
                        pieSeries2.slices.template.strokeWidth = 1;
                        pieSeries2.slices.template.strokeOpacity = 1;
                        pieSeries2.slices.template.states.getKey("hover").properties.shiftRadius = 0.05;
                        pieSeries2.slices.template.states.getKey("hover").properties.scale = 1;

                        pieSeries2.labels.template.disabled = true;
                        pieSeries2.ticks.template.disabled = true;


                        var label = chart.seriesContainer.createChild(am4core.Label);
                        label.textAlign = "middle";
                        label.horizontalCenter = "middle";
                        label.verticalCenter = "middle";
                        label.adapter.add("text", function(text, target){
                            return "[font-size:18px]total[/]:\n[bold font-size:30px]" + pieSeries2.dataItem.values.value.sum.toFixed(2) + "[/]";
                        })

                        chart.legend = new am4charts.Legend();
                        chart.legend.position = "botom";
                        chart.legend.scrollable = true;

                    });
                }
            });
        }

        function reusableDonutChart(date_range = '{{\Carbon\Carbon::now()->subMonths(3)->format('Y-m-d')}} - {{\Carbon\Carbon::now()->format('Y-m-d')}}', agents = null, categories = null, return_type = null, reuse_type = null, period = 'daily', type = 'qty-wise',end_product=null,days=null) {
            $('#re_usable_title').empty();
            $('#re_usable_title').append('Re Usable Type Wise '+date_range);
            $.ajax({
                url: "/dashboard/sales-return/reusable-donut-chart",
                type: "get",
                async: true,
                data: {
                    date_range: date_range,
                    agents: agents,
                    categories: categories,
                    return_type: return_type,
                    reuse_type: reuse_type,
                    period: period,
                    type: type,
                    end_product:end_product,
                    days:days
                },
                success: function (data) {
                    let response = JSON.parse(data);
                    am4core.ready(function() {

                        am4core.useTheme(am4themes_material);

                        var chart = am4core.create("re-usable-chart", am4charts.PieChart);
                        chart.startAngle = 160;
                        chart.endAngle = 380;


                        chart.innerRadius = am4core.percent(60);


                        chart.data = response


                        var pieSeries2 = chart.series.push(new am4charts.PieSeries());
                        pieSeries2.dataFields.value = "qty";
                        pieSeries2.dataFields.category = "name";
                        pieSeries2.slices.template.stroke = new am4core.InterfaceColorSet().getFor("background");
                        pieSeries2.slices.template.strokeWidth = 1;
                        pieSeries2.slices.template.strokeOpacity = 1;
                        pieSeries2.slices.template.states.getKey("hover").properties.shiftRadius = 0.05;
                        pieSeries2.slices.template.states.getKey("hover").properties.scale = 1;

                        pieSeries2.labels.template.disabled = true;
                        pieSeries2.ticks.template.disabled = true;


                        var label = chart.seriesContainer.createChild(am4core.Label);
                        label.textAlign = "middle";
                        label.horizontalCenter = "middle";
                        label.verticalCenter = "middle";
                        label.adapter.add("text", function(text, target){
                            return "[font-size:18px]total[/]:\n[bold font-size:30px]" + pieSeries2.dataItem.values.value.sum.toFixed(2) + "[/]";
                        })

                        chart.legend = new am4charts.Legend();
                        chart.legend.position = "botom";
                        chart.legend.scrollable = true;

                    });
                }
            });
        }

        // document.getElementById('hour_country_wise_trend_chart').onclick = function (evt) {
        //     let activePoint = hour_country_wise_trend_chart.getElementAtEvent(evt)[0];
        //     let data = activePoint._chart.data;
        //     let datasetIndex = activePoint._datasetIndex;
        //     let label = data.datasets[datasetIndex].label;
        //     let value = data.datasets[datasetIndex].data[activePoint._index];
        //     let time = filter_by_country_data.last_hour[activePoint._index];
        //
        //
        //     hourly_selected_time = time;
        //     getHistoryCountryData(time, datasetIndex);
        // };
    </script>
@stop



