@extends('layouts.back.master')@section('title','Settle Notes')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <style>
        th {
            text-align: center !important;
        }

        td {
            text-align: right !important;
        }
    </style>
@stop

@section('content')

   @if(Auth::user()->hasRole(['Sales Manager','Owner','Accountant','Super Admin']) )
        <div class="row mb-2">
            @if((Sentinel::check()->roles[0]->slug == 'sales-ref'))
                <div class="form-group col-sm-2">
                    <input type="hidden" name="agent" id="agent" value="{{Sentinel::getUser()->id}}">

                </div>
            @else
                <div class="col-sm-2">
                    <div class="form-group">
                        {!! Form::select('agent', $agents , null , ['class' => 'form-control','placeholder'=>'Agents','id'=>'agent']) !!}
                    </div>
                </div>
            @endif

            <div class="col-sm-2">
                <div class="form-group">
                    {!! Form::select('type', [1=>'System Generated',2=>'Return'] , null , ['class' => 'form-control','placeholder'=>'Type','id'=>'type']) !!}
                </div>
            </div>
            <div class="col-md-3">
                {!! Form::select('status', [1=>'Pending',2=>'Accepting',3=>'Approving',4=>'Clearing',5=>'Settled'] , null , ['class' => 'form-control','placeholder'=>'Select Status','id'=>'status']) !!}
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <input type="text" name="date_range" id="date_range" class="form-control">
                </div>
            </div>
            <div class="col-md-2">
                <button class="btn btn-info " onclick="process_form(this)">Filter</button>
            </div>
        </div>

    @endif

    <div class="">
        <table id="esr_table" class="display text-center">
            <thead>
            <tr>
                <th>ID</th>
                <th>Date</th>
                <th>Debit Note Number</th>
                <th>Agent</th>
                <th>Total</th>
                <th>Return Discount Amount</th>
                <th>Hold Amount</th>
                <th>Final Amount</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>ID</th>
                <th>Date</th>
                <th>Debit Note Number</th>
                <th>Agent</th>
                <th class="text-right">Total</th>
                <th class="text-right">Return Discount Amount</th>
                <th class="text-right">Hold Amount</th>
                <th class="text-right">Final Amount</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
    </div>

@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script>
        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019",
            "maxDate":moment()
        });


        $('#date_range').val('')
        var total_sum;
        var dis_sum;
        var hold_sum;
        var final_sum;
        $(document).ready(function () {
            table = $('#esr_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "ajax": {
                    url: "{{url('admin/settle_notes/table/data')}}",
                    type: "get",
                    error: function () {
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    },
                    dataSrc: function (data) {
                        total_sum = data.total_sum;
                        dis_sum = data.dis_sum;
                        hold_sum = data.hold_sum;
                        final_sum = data.final_sum;
                        return data.data;
                    }
                },
                pageLength: 25,
                responsive: true,
                drawCallback: function (settings) {
                    var api = this.api();

                    $(api.column(4).footer()).html(
                        'Total : ' + total_sum
                    );
                    $(api.column(5).footer()).html(
                        'Return Discount Amount : ' + dis_sum
                    );
                    $(api.column(6).footer()).html(
                        'Hold Amount : ' + hold_sum
                    );
                    $(api.column(7).footer()).html(
                        'Final Amount : ' + final_sum
                    );
                }
            });
        });

        function process_form(e) {
            let status = $("#status").val();
            let agent = $("#agent").val();
            let type = $("#type").val();
            let date_range = $("#date_range").val();
            let table = $('#esr_table').DataTable();
            table.ajax.url('/admin/settle_notes/table/data?status=' + status + '&agent=' + agent + '&type=' + type + '&date_range=' + date_range + '&filter=' + true).load();
        }


        function process_form_reset() {
            $("#status").val('');
            $("#agent").val('');
            $("#date_range").val('');
            let table = $('#esr_table').DataTable();
            table.ajax.url('/admin/settle_notes/table/data').load();
        }

    </script>

@stop
