@extends('layouts.back.master')@section('title','Sales Returns')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .hide {
            display: none;
        }
    </style>
@stop
@section('content')
    <div class="app-wrapper">


        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">Last Non Approve Returns</h5>
            </div>


            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">
                    <div class="container">


                        <div class="projects-list">

                            @foreach($returns as $return)

                                @php $updateAt =$return->updated_at->toDateString();

                                $todayAt = Carbon\Carbon::now();
                                if($return->status == 5 && $updateAt !== $todayAt->toDateString()){}else{ @endphp
                                <div class="media">
                                    <div class="avatar avatar text-white avatar-md project-icon bg-primary"
                                         data-target="#project-1">
                                        <i class="fa fa-pause" aria-hidden="true"></i>
                                    </div>
                                    <div class="media-body">
                                        @if(empty($return->agent))
                                            <small class="project-detail">{{$return->debit_note_number}}</small>
                                        @else

                                          <h6 class="project-name"
                                              id="project-1">{{$return->agent->name_with_initials}}</h6>
                                         <small class="project-detail">{{$return->agent->territory}}</small><br>

                                        <small class="project-detail">{{$return->debit_note_number}}</small>
                                        @endif
                                    </div>
                                    <div class="media-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <small class="project-detail">{{number_format($return->total,2)}}
                                                    LKR</small>
                                            </div>
                                            <div class="col-md-3">
                                                @if($return->status == 1)
                                                    <a href="#">
                                                        <button class="btn ml-3 btn-danger" style="width:6rem">Sending
                                                        </button>
                                                    </a>
                                                @endif
                                                @if($return->status == 2)
                                                    <a href="#">
                                                        <button class="btn ml-3 btn-info" style="width:6rem">Accepting
                                                        </button>
                                                    </a>
                                                @endif
                                                @if($return->status == 3)
                                                    <a href="#">
                                                        <button class="btn ml-3 btn-warning" style="width:6rem">
                                                            Approving
                                                        </button>
                                                    </a>
                                                @endif


                                                @if($return->status == 4)
                                                    <a href="#">
                                                        <button class="btn ml-3 btn-success" style="width:6rem">
                                                            Clearing
                                                        </button>
                                                    </a>
                                                @endif
                                                @if($return->status == 5)
                                                    <a href="#">
                                                        <button class="btn ml-3 btn-primary" style="width:6rem">
                                                            Settled
                                                        </button>
                                                    </a>
                                            @endif
                                            <!-- @if($return->status == 1)
                                                <a href="{{'/admin/sales/return/print/collect/'.$return->id}}" class="btn ml-3 btn-outline-primary" style="width:6rem">Print To<br> Collect</a>
                                                  @else
                                                <a href="">
                                                  <button class="btn ml-3 btn-warning" style="width:6rem">Approving</button>
                                                </a>
@endif -->
                                            </div>
                                            <div class="col-md-3">
                                                @if($return->status == 5)
                                                    {!! Form::open(['route' => 'admin.sales_return_print', 'method' => 'post','target'=>'_blank']) !!}
                                                    <input type="hidden" name="sales_return_header"
                                                           value="{{$return->id}}">
                                                    <button type="submit" name="submit" value="print_and_approve"
                                                            class="btn ml-3 btn-outline-dark">
                                                        Print
                                                    </button>
                                                    {!! Form::close() !!}
                                                @endif
                                            </div>
                                            <div class="col-md-3">
                                                @if($return->status <3 && !$return->is_system_generated)
                                                    <a href="{{'/admin/sales/return/print/collect/'.$return->id}}"
                                                       class="btn btn-outline-primary mb-2" style="width:6rem">Print To<br>
                                                        Collect</a>
                                                @endif
                                                @if($return->status == 2)
                                                    <a href="{{'/admin/sales_return/'.$return->id}}">
                                                        <button class="btn btn-outline-success" style="width:6rem">
                                                            Accept
                                                        </button>
                                                    </a>
                                                        @if(Sentinel::check()->roles[0]->slug=='owner')

                                                        {!! Form::open(['route' => ['sales_return.admin-destroy',$return->id], 'method' => 'post']) !!}
                                                        <input type="hidden" name="sales_return_header"
                                                               value="{{$return->id}}">
                                                        <a href="{{'/admin/sales_return/delete/'.$return->id}}">
                                                            <button class="btn btn-danger" style="width:6rem">
                                                                Delete
                                                            </button>
                                                        </a>

                                                        {!! Form::close() !!}
                                                        @endif
                                                    @endif
                                            {{-- @if($return->status !== 1)
                                                @if($return->status == 3)
                                                    <a href="">
                                                      <button class="btn btn-success" style="width:6rem">Approved</button>
                                                    </a>
                                                       @else
                                                    <a href="{{'/admin/sales_return/'.$return->id}}">
                                                            <button class="btn btn-outline-success" style="width:6rem">Approve</button>
                                                          </a>
                                                      @endif
                                            @else
                                                @if($return->status == 1)
                                                    <a href="">
                                                      <button class="btn btn-danger" style="width:6rem">Sending</button>
                                                    </a>
                                                    @endif
                                            @endif  --}}
                                            </div>
                                            <!-- <button class="btn ml-3 btn-outline-primary" style="width:6rem">View</button> -->

                                            <!-- <button class="btn ml-3 btn-outline-dark" style="width:6rem">Print</button> -->
                                        </div>
                                    </div>
                                </div>
                                <hr class="m-0">
                                @php } @endphp
                            @endforeach
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $('.delete').click(function (e) {
            e.preventDefault();
            id = $(this).attr("value");
            confirmAlert(id);

        });

        function confirmAlert(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "delete",
                        url: id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            } else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

        @if(session('pdf'))
        var id = '{!! session('pdf.file') !!}'
        $.ajax({
            type: "POST",
            url: '/loading/' + id + '/pdf',
            success: function (response) {
                window.open(response);
                console.log(response);
                debugger
            }
        });
        @endif

    </script>
@stop
