@extends('layouts.back.master')@section('title','Sales Vs Return')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }
    </style>
@stop

@section('content')
    {{ Form::open(array('url' => '/esr/excel/data','id'=>'filter_form'))}}
    <div class="row">

        <div class="col-md-2">
            {!! Form::select('agent',$agents , null , ['class' => 'form-control','required','placeholder'=>'Select Agent','id'=>'agent']) !!}
        </div>

        <div class="col-md-1">
            {!! Form::select('category',$categories , null , ['class' => 'form-control','required','id'=>'category']) !!}
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                {!! Form::select('desc1', [1=>'Distributor',2=>'Show Room',3=>'Direct Sale'] , null , ['class' => 'form-control','placeholder'=>'Select Type','id'=>'desc1']) !!}
            </div>
        </div>

        <div class="col-sm-2">
            <div class="form-group">
                {!! Form::select('dtt', [1=>'New Data',2=>'Old Data'] , null , ['class' => 'form-control','id'=>'dtt']) !!}
            </div>
        </div>

        <div class="col-sm-3">
            <div class="form-group">
                <input type="text" autocomplete="off" name="date_range" id="date_range" class="form-control">
          {{--      {!! Form::text('date_range',null,['class' => 'form-control','id'=>'date_range','placeholder'=>'Select Date','id'=>'date_range']) !!}  --}}
            </div>
        </div>

        <div class="col-sm-2">
            <div class="form-group">
                <button type="button" class="btn btn-success" onclick="process_form()">Filter Now</button>
            </div>
        </div>

    </div>
    {{ Form::close() }}


    <div class="">
        <table id="esr_table" class="display text-center">
            <thead>
            <tr>
                <th>Item Name</th>
                <th>Sold Qty</th>
                <th>SR</th>
                <th>Net Qty</th>
                <th>Sold Value</th>
                <th>MR</th>
                <th>MR Value</th>
                <th>MR %</th>
                <th>Action</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Item Name</th>
                <th>Sold Qty</th>
                <th>SR</th>
                <th>Net Qty</th>
                <th>Sold Value</th>
                <th>MR</th>
                <th>MR Value</th>
                <th>MR %</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
    </div>

@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script>
        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "startDate": moment(),
            "endDate":moment(),
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019",
            "maxDate":moment()
        });

        $(document).ready(function () {
            table = $('#esr_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "ajax": {
                    url: "{{route('loading.sales-return-data')}}",
                    type: "get",
                    error: function () {  // error handling code
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    }
                },
                pageLength: 25,
                responsive: true
            });
        });



        function process_form() {

            let date_range = $("#date_range").val();
            let agent = $("#agent").val();
            let category = $("#category").val();
            let desc1 = $("#desc1").val();
            let dtt = $("#dtt").val();
            let table = $('#esr_table').DataTable();
            table.ajax.url('/sales/return/data?agent=' + agent + '&category=' + category + '&desc1=' + desc1 + '&date_range=' + date_range + '&dtt=' + dtt + '&filter=' + true).load();
            // table.ajax.url('/loading/table/data?date_range=' + date_range + '&filter=' + true).load();

        }


        function process_form_reset() {
            $("#date_range").val('');
            // $("#status").val('');
            // $("#semi").val('');
            let table = $('#esr_table').DataTable();
            table.ajax.url('/sales/return/data').load();

        }


    </script>

@stop