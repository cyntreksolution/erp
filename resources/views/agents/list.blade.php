@extends('layouts.back.master')@section('title','Buffer Stock | List')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }
    </style>
@stop

@section('content')
    {{ Form::open(array('url' => '/burden/excel/data','id'=>'filter_form'))}}
    <div class="row">
        @if (!Auth::user()->hasRole(['Sales Agent']))
            <div class="col-sm-3">
                <div class="form-group">
                    {!! Form::select('agent', $agents , null , ['id'=>'agent','class' => 'form-control','placeholder'=>'Select Agent']) !!}
                </div>
            </div>
        @endif
        <div class="col-sm-2">
            <div class="form-group">
                {!! Form::select('category',$categories , null , ['id'=>'category','class' => 'form-control','placeholder'=>'Select Category','id'=>'category']) !!}
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                {!! Form::select('day', \App\AgentBuffer::day , null , ['id'=>'day','class' => 'form-control','placeholder'=>'Select Day']) !!}
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                {!! Form::select('time', \App\AgentBuffer::time , null , ['id'=>'time','class' => 'form-control','placeholder'=>'Select Time']) !!}
            </div>
        </div>

        <div class="col-sm-3">
            <div class="form-group">
                <button type="button" class="btn btn-success" onclick="process_form()">Filter Now</button>
                <button type="button" class="btn btn-warning" onclick="process_form_reset()">Reset Filter</button>
                <button type="submit" name="action" value="download_data" class="btn btn-info"><i
                            class="fa fa-download"></i></button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
    <div class="">
        <table id="invoice_table" class="display text-center">
            <thead>
            <tr>
                <th>#</th>
                <th>Agent</th>
                <th>Category</th>
                <th>Day</th>
                <th>Time</th>
                <th>Action</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>#</th>
                <th>Agent</th>
                <th>Category</th>
                <th>Day</th>
                <th>Time</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
    </div>
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script>
        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019"
        });
        $('#date_range').val('')

        $(document).ready(function () {
            table = $('#invoice_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "ajax": {
                    url: "{{url('/buffer-stock/agents/table/data')}}", // json datasource
                    type: "get",
                    error: function () {  // error handling code
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    }
                },
                pageLength: 25,
                responsive: true
            });
        });

        function process_form() {
            let agent = $("#agent").val();
            let category = $("#category").val();
            let day = $("#day").val();
            let time = $("#time").val();
            let table = $('#invoice_table').DataTable();
            table.ajax.url('/buffer-stock/agents/table/data?agent=' + agent + '&category=' + category + '&day=' + day + '&time=' + time + '&filter=' + true).load();

        }

        function process_form_reset() {
            $("#agent").val('');
            $("#category").val('');
            $("#day").val('');
            $("#time").val('');
            let table = $('#invoice_table').DataTable();
            table.ajax.url('/buffer-stock/agents/table/data').load();

        }


    </script>
    <script>
        function deleteBurden(id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            swal({
                    title: "Are you sure ?",
                    text: "Do you want to Delete? This action cannot be recover !",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, Delete it!",
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: "post",
                            url: '../burden/delete/burden',
                            data: {burden_id: id},

                            success: function (response) {
                                if (response == 'true') {
                                    swal("Nice!", "You successfully Deleted the burning", "success");
                                    setTimeout(location.reload.bind(location), 900);
                                } else {
                                    swal("Error!", "Something went wrong", "error");
                                }
                            }
                        });


                        // });

                    } else {
                        swal("Cancelled", "You cancelled the finishing :)", "error");
                    }
                });


        }
    </script>
@stop