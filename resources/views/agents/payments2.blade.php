@extends('layouts.back.master')
@section('title','Payments | List ')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <style>
        th {
            text-align: right !important;
        }

        td {
            text-align: right !important;
        }
    </style>
@stop

@section('content')
    @if(Sentinel::check()->roles[0]->slug=='owner' || Sentinel::check()->roles[0]->slug=='accountant' )
        <div class="row mb-2">
            <div class="col-md-3">
                {!! Form::select('agent',$agents , null , ['class' => 'form-control','placeholder'=>'Select Agent','id'=>'agent']) !!}
            </div>
            <div class="col-md-3">
                {!! Form::select('status', [ 1 => 'Pending' , 2 => 'Processing' , 3 => 'Approved' ] , null , ['class' => 'form-control','placeholder'=>'Select Status','id'=>'status']) !!}
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <input type="text" name="date_range" id="date_range" class="form-control">
                </div>
            </div>
            <div class="col-md-2">
                <button class="btn btn-info " onclick="process_form(this)">Filter</button>
            </div>
        </div>
    @endif
    <div class="">
        <table id="invoice_table" class="display text-center">
            <thead>
            <tr>
                <th>Date</th>
                <th>Serial</th>
                <th>Agent Name</th>
                <th>Agent Outstand</th>
                <th>Cheques</th>
                <th>Cash</th>
                <th>Total</th>
                <th>Status</th>

            </tr>
            </thead>
            <tbody>
{{--            @foreach($burdens as $burden)--}}
{{--                <tr>--}}
{{--                    <td>{{$burden->serial}}</td>--}}
{{--                    <td>{{$burden->endProduct->name}}</td>--}}
{{--                    <td>{{$burden->burden_size}}</td>--}}
{{--                    <td>{{$burden->expected_end_product_qty	}}</td>--}}
{{--                    <td>{{$burden->recipe->name}}</td>--}}
{{--                    <td>--}}
{{--                        <button class="btn btn-sm btn-primary"><i class="fa fa-eye"></i></button>--}}
{{--                    </td>--}}
{{--                </tr>--}}
{{--            @endforeach--}}
            </tbody>
            <tfoot>
            <tr>
                <th>Date</th>
                <th>Serial</th>
                <th>Agent Name</th>
                <th>Agent Outstand</th>
                <th>Cheques</th>
                <th>Cash</th>
                <th>Total</th>
                <th>Status</th>

            </tr>
            </tfoot>
        </table>


    </div>
@stop


@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/Buttons-1.5.1/js/dataTables.buttons.js')}}"></script>
    <script src="{{asset('/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/JSZip-2.5.0/jszip.min.js')}}"></script>
    <script src="{{asset('js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/pdfmake-0.1.32/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/Buttons-1.5.1/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('/js/vfs_fonts.js')}}"></script>

    <script>
        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019",
            "maxDate":moment()
        });
        $('#date_range').val('')

        function process_form(e) {

            let agent = $("#agent").val();
            let status = $("#status").val();
            let date_range = $("#date_range").val();
            let table = $('#invoice_table').DataTable();
            table.ajax.url('/agent-payment/table/data2?agent=' + agent + '&status=' + status +  '&date_range=' + date_range + '&filter=' + true).load();
        }


        function process_form_reset() {


            $("#agent").val('');
            $("#status").val('');
            $("#date_range").val('');
            let table = $('#invoice_table').DataTable();
            table.ajax.url('/agent-payment2').load();
        }


        var paid_chq;
        var paid_cash;
        var net_rem;
        $(document).ready(function () {
            table = $('#invoice_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                dom: 'lfBrtip',
                buttons: [
                    {
                        extend: 'copy',
                        className: 'btn btn-xs btn-info ml-3 sml assets-export-btn export-copy ttip',
                    },

                    {
                        extend: 'excel',
                        className: 'btn btn-xs btn-success ml-3 sml assets-export-btn export-copy ttip',
                    },
                    {
                        extend: 'pdf',
                        className: 'btn btn-xs btn-danger ml-3 sml assets-export-btn export-copy ttip',
                    },

                    {
                        extend: 'print',
                        className: 'btn btn-xs btn-primary ml-3 sml assets-export-btn export-copy ttip',
                    },
                ],
                "ajax": {
                    url: "{{url('/agent-payment/table/data2')}}",
                    type: "get",
                    error: function () {  // error handling code
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    },
                    dataSrc: function (data) {
                        paid_chq = data.paid_chq;
                        paid_cash = data.paid_cash;
                        net_rem = data.net_rem;
                        return data.data;
                    }
                },
                drawCallback: function (settings) {
                    var api = this.api();
                   /* $(api.column(4).footer()).html(
                        'Cheques Amount : ' + paid_chq
                    );
                    $(api.column(5).footer()).html(
                        'Cash Amount : ' + paid_cash
                    );*/
                    $(api.column(6).footer()).html(
                        'Net Remain : ' + net_rem
                    );
                },
                pageLength: 25,
                responsive: true
            });
        });


    </script>

@stop
