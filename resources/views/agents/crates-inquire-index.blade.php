@extends('layouts.back.master')@section('title','Crates Inquire')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.wizard.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/modal.css')}}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/demos.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/css/site.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/packages/recipe/recipe.css')}}">
@stop


@section('content')
    <div class="app-wrapper">
        <div class="app-panel" id="app-panel">
            <div class="app-search">
                <h5 class="app-main-heading text-center">Agents</h5>
            </div>

            <div class="scroll-container">
                <div class="app-panel-inner">

                    <div class="p-3">
                      <select id="agent_id" name="agent_id" class="form-control">
                        <option value="Null">Select</option>
                        @foreach($agents as $agent)
                          <option value="{{$agent->id}}">{{$agent->email}}</option>
                        @endforeach
                      </select>
                    </div>

                    <div class="p-3">
                        <button class="btn btn-success py-3 btn-block" id="printBTN" onclick="redirectToPrint()">PRINT
                        </button>
                    </div>
                    <div class="p-3">
                        <button class="btn btn-success py-3 btn-block" id="printInvoiceBTN" onclick="redirectToPrintInvoice()">PRINT INVOICE
                        </button>
                    </div>
                    <hr class="m-0">

                </div>
            </div>
            <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show"><i
                        class="fa fa-chevron-right"></i>
                <i class="fa fa-chevron-left"></i>
            </a>
        </div>
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">Details</h5>
            </div>

            <div class="scroll-container">
                <div class="app-main-content">
                    <div class="media-list">
                      <div class="container"  id="all_Details">
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    {{--    <script src="{{asset('assets/examples/js/apps/projects.js')}}"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('assets/packages/semi_finish/semi_finish.js')}}"></script>
    <script src="{{asset('assets/packages/recipe/end-recipe.js')}}"></script>

    <script>

    $(document).ready(function () {
        $("#printBTN").hide();
        $("#printInvoiceBTN").hide();
    });
        $("#agent_id").change(function () {
          let agent = $("#agent_id option:selected").text();
          let agentId = $("#agent_id option:selected").val();
          if(agentId == 'Null'){
            $('#all_Details').empty();
            $("#printBTN").hide();
            $("#printInvoiceBTN").hide();
          }else{
            $("#printBTN").show();
            $("#printInvoiceBTN").show();
            $.ajax({
                url: "crates-inquire/data/" + agentId,
                type: "GET",
                success: function (data) {
                  console.log(data);
                    appendRawList(data);
                },
                error: function (xhr, status) {

                }
            });
          }

        }).trigger("change");


        function appendRawList(rawList) {
            let flag = 0;
            let today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();
            today = yyyy + '-' + mm + '-' + dd;
            $('#all_Details').empty();
            $('#all_Details').append('' +
                '<div class="task-name-wrap">\n' +
                '        <div class="row mt-2">\n' +
                '            <div class="col-md-6"><b>Agent Name</b></div>\n' +
                '            <div class="col-md-6"><b>'+rawList[1][0]['agent_name']+'</b></div>\n' +
                '        </div>\n' +
                '</div>\n'+
                ' <div class="task-name-wrap">\n' +
                '                                            <div class="row mt-2">\n' +
                '                                                <div class="col-md-6"><b>Territory</b></div>\n' +
                '                                                <div class="col-md-6"><b>'+rawList[1][0]['agent_territory']+'</b></div>\n' +
                '                                            </div>\n' +
                '                                        </div>\n'+
                '<div class="task-name-wrap">\n' +
                '        <div class="row mt-2">\n' +
                '            <div class="col-md-6"><b>Date</b></div>\n' +
                '            <div class="col-md-6"><b>'+ today+'</b></div>\n' +
                '        </div>\n' +
                '</div><hr>'
              )

                $('#all_Details').append('' +
                    ' <div class="task-name-wrap">\n' +
                    '                                            <div class="row mt-2">\n' +
                    '                                                <div class="col-md-2 text-right"><b>Descrip.</b></div>\n' +
                    '                                                <div class="col-md-10 text-center"><b>Crates Types</b></div>\n' +
                    '                                            </div>\n' +
                    '                                        </div>')

                                    $('#all_Details').append('' +
                                        ' <div class="task-name-wrap">\n' +
                                        '                                            <div class="row mt-2">\n' +
                                        '                                                <div class="col-md-2 text-right"><b></b></div>\n' +
                                        '                                                <div class="col-md-2 text-right"><b>BC-L</b></div>\n' +
                                        '                                                <div class="col-md-2 text-right"><b>BC-S</b></div>\n' +
                                        '                                                <div class="col-md-2 text-right"><b>CR-L</b></div>\n' +
                                        '                                                <div class="col-md-2 text-right"><b>CR-L</b></div>\n' +
                                        '                                                <div class="col-md-2 text-right"><b>LID</b></div>\n' +
                                        '                                            </div>\n' +
                                        '                                        </div><hr>')


                let openingStock = '<div class="task-name-wrap"><div class="row mt-2"><div class="col-md-2"><b>Opening Stock</b></div>';
                //     //loadingHeader
                    let totalLoadingHeaderAmount = 0;
                    if(rawList[2].length != 0){
                    for (var i = 0; i < rawList[2].length; i++) {
                      openingStock = openingStock+'<div class="col-md-2 text-right"><b>'+rawList[2][i]+'</b></div>';
                          // totalLoadingHeaderAmount = totalLoadingHeaderAmount+ parseFloat(rawList[0]['loading_header'][i]['net_amount']);
                    }
                  }
                  openingStock = openingStock+'</div></div>';
                    $('#all_Details').append(openingStock);

                    let todaySendReturn = '<div class="task-name-wrap"><div class="row mt-2"><div class="col-md-4"><b>Today<br>Send Crates</b></div><div class="col-md-2 text-right"></div><div class="col-md-2 text-right"></div><div class="col-md-2 text-right"></div></div></div><div class="task-name-wrap"><div class="row mt-2">';
                    //     //loadingHeader
                        if(rawList[4].length != 0){
                        for (var i = 0; i < rawList[4].length; i++) {
                          todaySendReturn = todaySendReturn+'<div class="col-md-2">'+rawList[4][i]['category']+'|'+rawList[4][i]['loading_header_note']+'</div>';
                            todaySendReturn = todaySendReturn+'<div class="col-md-2 text-right"><b>'+rawList[4][i]['totalsendCrates193']+'</b></div>';
                              todaySendReturn = todaySendReturn+'<div class="col-md-2 text-right"><b>'+rawList[4][i]['totalsendCrates194']+'</b></div>';
                                todaySendReturn = todaySendReturn+'<div class="col-md-2 text-right"><b>'+rawList[4][i]['totalsendCrates195']+'</b></div>';
                                  todaySendReturn = todaySendReturn+'<div class="col-md-2 text-right"><b>'+rawList[4][i]['totalsendCrates196']+'</b></div>';
                                    todaySendReturn = todaySendReturn+'<div class="col-md-2 text-right"><b>'+rawList[4][i]['totalsendCrates197']+'</b></div>';
                              // totalLoadingHeaderAmount = totalLoadingHeaderAmount+ parseFloat(rawList[0]['loading_header'][i]['net_amount']);
                        }
                      }
                      todaySendReturn = todaySendReturn+'</div></div>';
                        $('#all_Details').append(todaySendReturn);
                        let todayReturnCrate = '<div class="task-name-wrap"><div class="row mt-2"><div class="col-md-4"><b>Today<br>Return Crates</b></div><div class="col-md-2 text-right"></div><div class="col-md-2 text-right"></div><div class="col-md-2 text-right"></div></div></div><div class="task-name-wrap"><div class="row mt-2">';
                        //     //loadingHeader
                            if(rawList[3].length != 0){
                            for (var i = 0; i < rawList[3].length; i++) {
                              todayReturnCrate = todayReturnCrate+'<div class="col-md-2">'+rawList[3][i]['category']+'|'+rawList[3][i]['loading_header_note']+'</div>';
                                todayReturnCrate = todayReturnCrate+'<div class="col-md-2 text-right"><b>'+rawList[3][i]['totalreturnCrates193']+'</b></div>';
                                  todayReturnCrate = todayReturnCrate+'<div class="col-md-2 text-right"><b>'+rawList[3][i]['totalreturnCrates194']+'</b></div>';
                                    todayReturnCrate = todayReturnCrate+'<div class="col-md-2 text-right"><b>'+rawList[3][i]['totalreturnCrates195']+'</b></div>';
                                      todayReturnCrate = todayReturnCrate+'<div class="col-md-2 text-right"><b>'+rawList[3][i]['totalreturnCrates196']+'</b></div>';
                                        todayReturnCrate = todayReturnCrate+'<div class="col-md-2 text-right"><b>'+rawList[3][i]['totalreturnCrates197']+'</b></div>';
                                  // totalLoadingHeaderAmount = totalLoadingHeaderAmount+ parseFloat(rawList[0]['loading_header'][i]['net_amount']);
                            }
                          }
                          todayReturnCrate = todayReturnCrate+'</div></div>';
                            $('#all_Details').append(todayReturnCrate);
                //   if(totalLoadingHeaderAmount != 0){
                //         $('#all_Details').append('' +
                //             ' <hr><div class="task-name-wrap">\n' +
                //             '                                            <div class="row mt-2">\n' +
                //             '                                                <div class="col-md-6"></div>\n' +
                //             '                                                <div class="col-md-3 text-right">' + Number(totalLoadingHeaderAmount).toFixed(3) + '</div>\n' +
                //             '                                                <div class="col-md-3 text-right"></div>\n' +
                //             '                                            </div>\n' +
                //             '                                        </div>');
                //   }
                //       let totalReduceAmount = 0;
                //   if(rawList[0]['sales_return'].length != 0){
                //     for (var i = 0; i < rawList[0]['sales_return'].length; i++) {
                //       $('#all_Details').append('' +
                //           ' <div class="task-name-wrap">\n' +
                //           '                                            <div class="row mt-2">\n' +
                //           '                                                <div class="col-md-6"><i class="fa fa-check-circle" aria-hidden="true"></i>  ' + rawList[0]['sales_return'][i]['sales_return_id'] + '</div>\n' +
                //           '                                                <div class="col-md-3 text-right"></div>\n' +
                //           '                                                <div class="col-md-3 text-right">(' + Number(rawList[0]['sales_return'][i]['final_amount']).toFixed(3) + ')</div>\n' +
                //           '                                            </div>\n' +
                //           '                                        </div>');
                //           totalReduceAmount = totalReduceAmount+ parseFloat(rawList[0]['sales_return'][i]['final_amount']);
                //         }
                //   }
                //   if(rawList[0]['agent_payment'].length != 0){
                //     for (var i = 0; i < rawList[0]['agent_payment'].length; i++) {
                //     $('#all_Details').append('' +
                //         ' <div class="task-name-wrap">\n' +
                //         '                                            <div class="row mt-2">\n' +
                //         '                                                <div class="col-md-6"><i class="fa fa-check-circle" aria-hidden="true"></i>  '+rawList[0]['agent_payment'][i]['agent_payment_id'] +'</div>\n' +
                //         '                                                <div class="col-md-3 text-right"></div>\n' +
                //         '                                                <div class="col-md-3 text-right">(' + Number(rawList[0]['agent_payment'][i]['total_amount']).toFixed(3) + ')</div>\n' +
                //         '                                            </div>\n' +
                //         '                                        </div>');
                //         totalReduceAmount = totalReduceAmount+parseFloat(rawList[0]['agent_payment'][i]['total_amount']);
                //     }
                //   }
                //
                //     let final_amount = (totalLoadingHeaderAmount+parseFloat(rawList[0]['due_balance']))-(totalReduceAmount);
                // $('#all_Details').append('' +
                //     ' <hr><div class="task-name-wrap">\n' +
                //     '                                            <div class="row mt-2">\n' +
                //     '                                                <div class="col-md-6">Final Amount</div>\n' +
                //     '                                                <div class="col-md-3 text-right"></div>\n' +
                //     '                                                <div class="col-md-3 text-right">' + parseFloat(final_amount).toFixed(3) + '</div>\n' +
                //     '                                            </div>\n' +
                //     '                                        </div><hr>')
                //

        }

        function redirectToPrint()
        {
        let agentId = $("#agent_id option:selected").val();
        window.location.href = "crates-inquire/print/"+agentId;
        }

        function redirectToPrintInvoice()
        {
        let agentId = $("#agent_id option:selected").val();
        window.location.href = "/crates-inquire/print/invoice/"+agentId;
        }

    </script>
@stop
