@extends('layouts.back.master')@section('title','approved preview')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}">


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .app-wrapper {
            font-family: Raleway, sans-serif;
            width: 100%;
            border: 1px solid #eee;
        }
    </style>
@stop
@section('content')
    <div class="app-wrapper">
        {{--        <div class="profile-section-user" id="app-panel">--}}
        {{--@foreach($po as $p)--}}
        {{--<div class="profile-cover-img"><img src="{{asset($p['supplier']->image_path.$p['supplier']->image)}}"--}}
        {{--alt="">--}}
        {{--</div>--}}
        {{--<div class="profile-info-brief p-3"><img class="img-fluid user-profile-avatar"--}}
        {{--src="{{asset($p['supplier']->image_path.$p['supplier']->image)}}"--}}
        {{--alt="">--}}
        {{--<div class="text-center"><h5 class="text-uppercase mb-1">{{$p['supplier']->supplier_name}}</h5>--}}
        {{--<div class="hidden-sm-down ">--}}
        {{--<div>{{$p['supplier']->mobile}}</div>--}}
        {{--<div>NO #{{$p->id}}</div>--}}
        {{--<div>{{$p->created_at}}</div>--}}
        {{--<button class="btn btn-outline-primary btn-sm m-2 btn-rounded " disabled>--}}
        {{--<i class="fa fa-pause px-1" aria-hidden="true"></i>--}}
        {{--PENDING--}}
        {{--</button>--}}
        {{--</div>--}}
        {{--<hr class="m-0 py-2">--}}
        {{--<div class="d-flex justify-content-center flex-wrap p-2">--}}
        {{--<a href="{{$p->id}}/edit" class="btn btn-block btn-success btn-sm m-2 text-white"> RECIEVE</a>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show"><i--}}
        {{--class="fa fa-chevron-right"></i>--}}
        {{--<i class="fa fa-chevron-left"></i>--}}
        {{--</a>--}}
        {{--@endforeach--}}
        {{--        </div>--}}
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">ORDERED PRODUCTS</h5>
            </div>
            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">
                    <div class="container">
                        <input type="hidden" name="refOrder" value="true">

                        <input type="hidden" name="id" value="{{$order->id}}">
                        <div class="projects-list">
                            @foreach($order->items as $product)
                                @if ($product->qty>0)
                                    <input type="hidden" name="item_id[]" value="{{$product->id}}">
                                    <input type="hidden" name="product_id[]" value="{{$product->end_product_id}}">
                                    <div class="card-body d-flex  align-items-center p-0">
                                        <div class="col-md-5">
                                            <div class="text-primary">
                                                <h5 class="mt-3 mx-3 {{($product->qty!=$product->agent_approved_qty)?'text-danger':null}}">   {{$product->endProduct->name}}</h5>
                                            </div>
                                        </div>
                                        <div class="col-md-4  text-center px-5">
                                            <div class="text-primary">
                                                <input type="hidden" name="unit_price[]" class="form-control"
                                                       value="{{$product->endProduct->distributor_price}}">
                                                <h6 class="{{($product->qty!=$product->agent_approved_qty)?'text-danger':null}}">{{number_format((float)$product->endProduct->distributor_price, 2, '.', '')}}
                                                    LKR</h6>
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-center px-5">
                                            <div class="text-primary">
                                                <h6 class="{{($product->qty!=$product->agent_approved_qty)?'text-danger':null}}">{{$product->qty}}</h6>
                                                <input type="hidden" name="qty[]" value="{{$product->qty}}">
                                            </div>
                                        </div>

                                        <div class="col-md-2 text-center px-5">
                                            <div class="text-primary {{($product->qty!=$product->agent_approved_qty)?'text-danger':null}}">
                                                {{$product->agent_approved_qty}}
                                            </div>
                                        </div>

                                    </div>
                                    <hr class="m-1">

                                @endif
                            @endforeach

                            @foreach($order->variants as $product)
                                @if ($product->qty>0)

                                    <div class="card-body d-flex  align-items-center p-0">
                                        <div class="col-md-5">
                                            <div class="text-primary">
                                                <h5 class="mt-3 mx-3 {{($product->weight!=$product->agent_approved_qty)?'text-danger':null}}">   {{$product->variant->product->name}}</h5>
                                            </div>
                                        </div>
                                        <div class="col-md-4  text-center px-5">
                                            <div class="text-primary">
                                                <h6 class="{{($product->weight!=$product->agent_approved_qty)?'text-danger':null}}">{{number_format((float)$product->variant->product->distributor_price, 2, '.', '')}}
                                                    LKR</h6>
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-center px-5">
                                            <div class="text-primary">
                                                <h6 class="{{($product->weight!=$product->agent_approved_qty)?'text-danger':null}}">{{$product->weight}}</h6>
                                            </div>
                                        </div>

                                        <div class="col-md-2 text-center px-5">
                                            <div class="text-primary {{($product->weight!=$product->agent_approved_qty)?'text-danger':null}}">
                                                {{$product->agent_approved_qty}}
                                            </div>
                                        </div>

                                    </div>
                                    <hr class="m-1">

                                @endif
                            @endforeach

                            @if(!empty($order->rawItems))
                                @foreach($order->rawItems as $product)
                                    <input type="hidden" name="item_id_r[]" value="{{$product->id}}">
                                    <input type="hidden" name="raw_id[]" value="{{$product->raw_material_id}}">
                                    <div class="card-body d-flex  align-items-center p-0 row">
                                        <div class="col-md-6">
                                            <div class="text-primary">
                                                <h5 class="mt-3 {{($product->qty!=$product->agent_approved_qty)?'text-danger':null}}">   {{\RawMaterialManager\Models\RawMaterial::withoutGlobalScope('type')->whereRawMaterialId($product->raw_material_id)->first()->name}}</h5>
                                            </div>
                                        </div>

                                        <div class="col-md-6 float-right text-right">
                                            <div class="text-primary  text-right{{($product->qty!=$product->agent_approved_qty)?'text-danger':null}}">
                                                <h6 class="text-right">{{$product->qty}}</h6>
                                            </div>
                                        </div>



                                    </div>
                                    <hr class="m-1">
                                @endforeach
                            @endif

                            <div id="appendDiv"></div>
                            <div class="mt-5 mb-5">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- <button type="submit" id="btnSave" name="submit"
                                                class="btn btn-success btn-lg btn-block"
                                                value="stream"> Approve
                                        </button> -->
                                        <a href="{{route('order.list')}}" class="btn btn-success btn-lg btn-block">Back</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script>

        $(document).keypress(function (e) {
            // alert(e.keyCode);
            if (e.keyCode == 78 || e.keyCode == 110) {
                $("#btnNew").click();
            }
            if (e.keyCode == 76 || e.keyCode == 108) {
                $("#btnSave").click();
            }
            if (e.keyCode == 69 || e.keyCode == 101) {
                $("#endProduct").focus();
            }
            if (e.keyCode == 67 || e.keyCode == 99) {
                $("#btnClose").click();
            }
        });

        $("#myModal").on('shown', function () {
            $("#endProduct").focus();
        });

        $("#endProduct").change(function () {
            $("#qty").focus();
        });

        $("#qty").keyup(function (event) {
            if (event.keyCode === 13) {
                $("#btnAdd").click();
            }
        });

        $('#myModalt').on('shown', function () {
            alert(1)
        })

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        async function addProduct() {
            let product = document.getElementById("endProduct");
            let qty = $('#qty').val();
            let product_id = $(product).children("option:selected").val();
            let product_name = $(product).children("option:selected").text();
            let price = 0;
            await $.ajax({
                type: "post",
                data: {id: product_id},
                url: '/end_product/get/endpro',
                success: function (response) {
                    price = response.distributor_price;
                }
            });
            var $newdiv1 = $("<input type=\"hidden\" name=\"product_id[]\" value=\"" + product_id + "\">" +
                " <div class=\"card-body d-flex  align-items-center p-0\">\n" +
                "                                    <div class=\"d-flex mr-auto\">\n" +
                "                                        <div>\n" +
                "                                            <div class=\"avatar avatar text-white avatar-md project-icon bg-primary\">\n" +
                "                                            </div>\n" +
                "                                        </div>\n" +
                "                                        <h5 class=\"mt-3 mx-3\"> " + product_name + "</h5>\n" +
                "                                    </div>\n" +
                "                                    <div class=\"d-flex activity-counters justify-content-between\">\n" +
                "<input type=\"hidden\" name=\"unit_price[]\" class=\"form-control\"\n" +
                "                                                       value=\"" + parseFloat(price).toFixed(2) + "\">" +
                "                                        <div class=\"text-center px-5\">\n" +
                "                                            <div class=\"text-primary\">\n" +
                "                                                <h6>" + parseFloat(price).toFixed(2) + " LKR</h6>\n" +
                "                                            </div>\n" +
                "                                        </div>\n" +
                "<div class=\"text-center px-5\">\n" +
                "                                            <div class=\"text-primary\">\n" +
                "                                                <h6>" + qty + "</h6>\n" +
                "                                            </div>\n" +
                "                                        </div>\n" +
                "                                        <div class=\"text-center px-5\">\n" +
                "                                            <div class=\"text-primary\">\n" +
                "                                                <input type=\"number\" name=\"qty[]\" class=\"form-control\"\n" +
                "                                                       value=\"" + qty + "\">\n" +
                "                                            </div>\n" +
                "                                        </div>\n" +
                "                                    </div>\n" +
                "                                </div> " +
                " <hr class=\"m-1\">"),
                newdiv2 = document.createElement("div"),
                existingdiv1 = document.getElementById("appendDiv");

            $(existingdiv1).append($newdiv1, [newdiv2, existingdiv1]);

        }

        $('.delete').click(function (e) {
            e.preventDefault();
            id = $(this).attr("value");
            confirmAlert(id);

        });


        // $(document).keypress(function ( e ) {
        //     // You may replace `c` with whatever key you want
        //     if ((e.metaKey || e.ctrlKey) && ( String.fromCharCode(e.which).toLowerCase() === 'c') ) {
        //         console.log( "You pressed CTRL + C" );
        //     }
        // });

        function confirmAlert(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "delete",
                        url: id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            } else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

    </script>
@stop
