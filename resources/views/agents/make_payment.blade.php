@extends('layouts.back.master')@section('title','Make Payment')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
@stop

@section('content')
    <div class="app-wrapper">

        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">Make Payment</h5>
            </div>


            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">
                    <div class="container">
                        <div class="projects-list mb-3">

                            {!! Form::open(['route' => 'agent-payment.store', 'method' => 'post']) !!}
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="row mt-3">
                                        <div class="col-md-4 text-right">
                                            <p> Notes </p>
                                        </div>
                                        <div class="col-md-4">
                                            <p class="text-right"> Qty </p>
                                        </div>
                                        <div class="col-md-4">
                                            <p class="text-right"> Amount </p>
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-4 text-right">
                                            <p class="mt-2"> 5000 x </p>
                                        </div>
                                        <div class="col-md-4">
                                            <input min="0" type="text" name="5000" onkeyup="lineValue(5000,'#n5000','#v5000')"
                                                 value="0" required  id="n5000" class="form-control text-right">
                                        </div>
                                        <div class="col-md-4" style="text-align:right">
                                            <p class="mt-2" id="v5000"></p>
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-4 text-right">
                                            <p class="mt-2"> 2000 x  </p>
                                        </div>
                                        <div class="col-md-4">
                                            <input min="0" type="text" name="2000" id="n2000" value="0" required
                                                   onkeyup="lineValue(2000,'#n2000','#v2000')"
                                                   class="form-control text-right">
                                        </div>
                                        <div class="col-md-4" style="text-align:right">
                                            <p class="mt-2" id="v2000"></p>
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-4 text-right">
                                            <p class="mt-2"> 1000 x  </p>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" min="0" name="1000" id="n1000" value="0" required
                                                   onkeyup="lineValue(1000,'#n1000','#v1000')"
                                                   class="form-control text-right">
                                        </div>
                                        <div class="col-md-4" style="text-align:right">
                                            <p class="mt-2" id="v1000"></p>
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-4 text-right">
                                            <p class="mt-2"> 500 x  </p>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" min="0" name="500" id="n500" value="0" required
                                                   onkeyup="lineValue(500,'#n500','#v500')"
                                                   class="form-control text-right">
                                        </div>
                                        <div class="col-md-4"  style="text-align:right">
                                            <p class="mt-2" id="v500"></p>
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-4 text-right">
                                            <p class="mt-2"> 100 x  </p>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" min="0" name="100" id="n100" value="0" required
                                                   onkeyup="lineValue(100,'#n100','#v100')"
                                                   class="form-control text-right">
                                        </div>
                                        <div class="col-md-4" style="text-align:right">
                                            <p class="mt-2" id="v100"></p>
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-4 text-right">
                                            <p class="mt-2"> 50 x  </p>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" min="0" name="50" id="n50" onkeyup="lineValue(50,'#n50','#v50')"
                                                   value="0" required class="form-control text-right">
                                        </div>
                                        <div class="col-md-4" style="text-align:right">
                                            <p class="mt-2" id="v50"></p>
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-4 text-right">
                                            <p class="mt-2"> 20 x  </p>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" min="0" name="20" id="n20" onkeyup="lineValue(20,'#n20','#v20')"
                                                   value="0" required  class="form-control text-right">
                                        </div>
                                        <div class="col-md-4" style="text-align:right">
                                            <p class="mt-2" id="v20"></p>
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-4 text-right">
                                            <p class="mt-2"> 10 x  </p>
                                        </div>
                                        <div class="col-md-4">
                                            <input min="0" type="text" name="10" onkeyup="lineValue(10,'#n10','#v10')" id="n10"
                                                   value="0" required   class="form-control text-right">
                                        </div>
                                        <div class="col-md-4" style="text-align:right">
                                            <p class="mt-2" id="v10"></p>
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-4 text-right">
                                            <p class="mt-2"> coins </p>
                                        </div>
                                        <div class="col-md-4">
                                            <input min="0"  type="text" name="coins" id="ncoins" value="0" required
                                                   step="0.01"  onkeyup="lineValue(1,'#ncoins','#vcoins')"
                                                   class="form-control text-right">
                                        </div>
                                        <div class="col-md-4" style="text-align:right">
                                            <p class="mt-2" id="vcoins"></p>
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-4 text-right">
                                            <p class="mt-2" > Cheques Value </p>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" min="0" value="0" required  step="0.01" onkeyup="lineValue(1,'#ncheque','#vcheque')" id="ncheque" name="cheque_amount_value" class="form-control text-right">
                                        </div>

                                    </div>

                                </div>


                                <div class="col-md-2">

                                </div>


                            </div>

                            <input type="hidden" name="cash_amount" id="cash_amount_value">
                            <input type="hidden" name="total_amount" id="total_amount_value">

                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">Total Cash Amount :</div>
                                        <div class="col-md-6"><p id="total_amount_cash"></p></div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">Total Cheques Amount :</div>
                                        <div class="col-md-6"><p class="mt-2" id="vcheque"></p></div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">Total Amount : </div>
                                        <div class="col-md-6"><p id="total_amount"></p></div>
                                    </div>
                                    @if(Sentinel::check()->roles[0]->slug != 'sales-ref')
                                    <div class="row">
                                        <div class="col-md-6">Agent : </div>
                                        <div class="col-md-6">
                                          <!-- {!! Form::select('agent_id', $agents , null , ['class' => 'form-control']) !!} -->
                                          <select id="agent_id" name="agent_id" class="form-control" onchange="checkValue(this.value)">
                                            <option value="Null">Select</option>
                                            @foreach($agents as $agent)
                                              <option value="{{$agent->id}}">{{$agent->email}}</option>
                                            @endforeach
                                          </select>
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-12">
                                            <button class="btn btn-success btn-block" type="submit" id="SubmitButton">Pay</button>
                                        </div>
                                    </div>
                                    @endif
                                    @if(Sentinel::check()->roles[0]->slug == 'sales-ref')
                                    <div class="row mt-4">
                                        <div class="col-md-12">
                                            <button class="btn btn-success btn-block" type="submit">Pay</button>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>

                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>

            </div>
        </div>
        @stop

        @section('js')
            <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
            <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
            <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
            <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
            <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
            <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
            <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
            <script type="text/javascript"
                    src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js"></script>
            <script>
                // let total = 0;

                        $(document).ready(function () {
                            $("#SubmitButton").hide();
                        });
                function lineValue(value, field1, field2) {
                    let qty = $(field1).val();
                    $(field2).empty();
                    $(field2).append((qty * value).toFixed(2) + ' LKR');
                    totalAmount((qty * value).toFixed(2));
                    totalCash((qty * value).toFixed(2));
                }

                function totalCash(row_value) {
                    var fieldID = ["#n5000", "#n2000", "#n1000", "#n500", "#n100", "#n50", "#n20", "#n10", "#ncoins"];
                    var fieldValue = [5000, 2000, 1000, 500, 100, 50, 20, 10, 1];
                    let total = 0;
                    var i;
                    var value = 0;
                    fLen = fieldID.length;
                    for (i = 0; i < fLen; i++) {
                        value = 0;
                        if ($(fieldID[i]).val() != '') {
                            value = parseFloat($(fieldID[i]).val()) * fieldValue[i];
                            total = total + value;
                        }
                    }
                    $('#total_amount_cash').empty();
                    $('#cash_amount_value').val(total.toFixed(2));
                    $('#total_amount_cash').append(total.toFixed(2) + ' LKR');
                }

                function totalAmount(row_value) {
                    var fieldID = ["#n5000", "#n2000", "#n1000", "#n500", "#n100", "#n50", "#n20", "#n10", "#ncoins","#ncheque"];
                    var fieldValue = [5000, 2000, 1000, 500, 100, 50, 20, 10, 1,1];
                    let total = 0;
                    var i;
                    var value = 0;
                    fLen = fieldID.length;
                    for (i = 0; i < fLen; i++) {
                        value = 0;
                        if ($(fieldID[i]).val() != '') {
                            value = parseFloat($(fieldID[i]).val()) * fieldValue[i];
                            total = total + value;
                        }
                    }
                    $('#total_amount').empty();
                    $('#total_amount_value').val(total.toFixed(2));
                    $('#total_amount').append(total.toFixed(2) + ' LKR');
                }

                function checkValue(value)
                {
                  if(value != "Null")
                  {
                    $("#SubmitButton").show();
                  }else{
                    $("#SubmitButton").hide();
                  }
                }

            </script>

@stop
