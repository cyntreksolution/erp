<html>
<head>
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/invoice.css')}}" rel="stylesheet">
    <style media="all">

        @page {
            margin: 30px 5px 30px 5px;
            size: 75mm 999mm;
        }
    </style>
</head>

<body style="margin-bottom: 100px">

<div class="row">

    <div class="col-xs-6">
        <img src="{{asset('assets/img/buntak_grace.jpeg')}}" style="width: 100px">
    </div>

    <div class="col-xs-6 text-right">
        <h6>
            <b>Ranjanas Holdings [Pvt] Ltd</b><br>
            Katukurunda<br>
            Habaraduwa<br>
            Hotline : +9477 330 4678<br>
        </h6>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <hr>
        <div class="task-name-wrap">
            <div class="row mt-2" style="display: inline">
                <div class="col-md-4 col-xs-4"><b>Agent Name</b></div>
                <div class="col-md-6 col-xs-6 text-left " ><b>{{$agent->type.' '.$agent->name_with_initials}}</b></div>

            </div>
        </div>
        <div class="task-name-wrap">
            <div class="row mt-2" style="display: inline">
                <div class="col-md-4 col-xs-4"><b>Territory</b></div>
                <div class="col-md-6 col-xs-6 text-left"><b>{{$agent->territory}}</b></div>
            </div>
        </div>

        <div class="task-name-wrap">
            <div class="row mt-2" style="display: inline">
                <div class="col-md-4 col-xs-4"><b>Printed At</b></div>
                <div class="col-md-6 col-xs-6 text-left"><b>{{date('Y-m-d H:i:s')}}</b></div>
            </div>
        </div>

        @foreach($agent_records as $record)
            <div class="task-name-wrap">
                <div class="row mt-2" style="display: inline">
                    <div class="col-md-4 col-xs-4"><b>Current Outstanding</b></div>
                    <div class="col-md-6 col-xs-6 text-left"><b>{{number_format($record->first()->start_balance+$agent_init_value,2)}}</b></div>
                </div>
            </div>
            @break
        @endforeach
        <hr style="height:1px;background-color: #00a5bb">

        <table class="table">
            <thead>
            <tr>
                <th>Description</th>
                <th class="text-center">Receipts</th>
                <th class="text-right">Payments</th>
            </tr>
            </thead>
            <tbody>
            @foreach($agent_records as $record)

                <tr>
                    <td class="col-md-1"><b>Date</b></td>
                    <td class="col-md-2 text-left"><b>{{$record->first()->created_at->format('Y-M-d')}}</b></td>
                    <td class="col-md-1"><b></b></td>
                </tr>

                {{--            <div class="task-name-wrap">--}}
                {{--                <div class="row mt-2">--}}
                {{--                    <div class="col-md-6"><b>Due Balance</b></div>--}}
                {{--                    <div class="col-md-6"><b>{{number_format($record->first()->start_balance+$agent_init_value,2)}}</b></div>--}}
                {{--                </div>--}}
                {{--            </div>--}}
                {{--            <hr>--}}
                @foreach($record as $data)
                    <tr>

      {{--                  <td>{{$data->reference_code}} - @if(!empty($data->reference_category['name'])){{$data->reference_category['name']}}@endif   --}}
                        <td>
                            {{$data->sub_desc}}

                        </td>

                        <td class="text-center">{{($data->amount>=0)?number_format($data->amount,2):'-'}}</td>
                        <td class="text-right">{{($data->amount<0)?number_format($data->amount,2):'-'}}</td>

                    </tr>
                @endforeach

                <td>Final Amount</td>
                <td class="col-md-3 text-right"></td>
                <td class="text-right"> {{number_format($record->last()->end_balance+$agent_init_value,2)}}</td>
                {{--           <td> <hr style="height:2px;background-color: #501310"></td>--}}

            @endforeach

            </tbody>
        </table>
    </div>
</div>
<br>


<footer>
    <div class="row" style="margin-top: 20px;">
        <h4 align="center">
            <!--h6 class="text-muted">Powered by Cyntrek Solutions<br>
                <h7 class="num">+9471 122 5489 / +9471 532 2262</h7>
            </h6-->
        </h4>
    </div>

    <p style="margin-top: 150px">--------------------------------------------------------------</p>
</footer>

</body>

</html>
