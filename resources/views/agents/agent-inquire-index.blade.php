@extends('layouts.back.master')@section('title','Agent Inquire')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.wizard.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/modal.css')}}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/demos.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/css/site.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/packages/recipe/recipe.css')}}">
@stop


@section('content')
    <div class="app-wrapper">
        <div class="app-panel" id="app-panel">
            <div class="app-search">
                <h5 class="app-main-heading text-center">Agents</h5>
            </div>

            <div class="scroll-container">
                <div class="app-panel-inner">

                    <div class="p-3">
                        <select id="agent_id" name="agent_id" class="form-control">
                            <option value="Null">Select</option>
                            @foreach($agents as $agent)
                                <option value="{{$agent->id}}">{{$agent->email}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="p-3">
                        <button class="btn btn-success py-3 btn-block" id="printBTN" onclick="redirectToPrint()">PRINT
                        </button>
                    </div>
                    <hr class="m-0">

                </div>
            </div>
            <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show"><i
                        class="fa fa-chevron-right"></i>
                <i class="fa fa-chevron-left"></i>
            </a>
        </div>
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">Details</h5>
            </div>

            <div class="scroll-container">
                <div class="app-main-content">
                    <div class="media-list">
                        <div class="container"  id="all_Details">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    {{--    <script src="{{asset('assets/examples/js/apps/projects.js')}}"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('assets/packages/semi_finish/semi_finish.js')}}"></script>
    <script src="{{asset('assets/packages/recipe/end-recipe.js')}}"></script>

    <script>

        $(document).ready(function () {
            $("#printBTN").hide();
        });
        $("#agent_id").change(function () {
            let agent = $("#agent_id option:selected").text();
            let agentId = $("#agent_id option:selected").val();
            if(agentId == 'Null'){
                $('#all_Details').empty();
                $("#printBTN").hide();
            }else{
                $("#printBTN").show();
                $.ajax({
                    url: "agent-inquire/data/" + agentId,
                    type: "GET",
                    success: function (data) {
                        // console.log(data);
                        appendRawList(data);
                    },
                    error: function (xhr, status) {

                    }
                });
            }

        }).trigger("change");


        function appendRawList(rawList) {
            let flag = 0;
            let today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();
            today = yyyy + '-' + mm + '-' + dd;
            $('#all_Details').empty();
            $('#all_Details').append('' +
                '<div class="task-name-wrap">\n' +
                '        <div class="row mt-2">\n' +
                '            <div class="col-md-6"><b>Agent Name</b></div>\n' +
                '            <div class="col-md-6"><b>'+rawList[0]['agent_name']+'</b></div>\n' +
                '        </div>\n' +
                '</div>\n'+
                ' <div class="task-name-wrap">\n' +
                '                                            <div class="row mt-2">\n' +
                '                                                <div class="col-md-6"><b>Territory</b></div>\n' +
                '                                                <div class="col-md-6"><b>'+rawList[0]['agent_territory']+'</b></div>\n' +
                '                                            </div>\n' +
                '                                        </div>\n'+
                '<div class="task-name-wrap">\n' +
                '        <div class="row mt-2">\n' +
                '            <div class="col-md-6"><b>Date</b></div>\n' +
                '            <div class="col-md-6"><b>'+ today+'</b></div>\n' +
                '        </div>\n' +
                '</div>\n'+
                '<div class="task-name-wrap">\n' +
                '        <div class="row mt-2">\n' +
                '            <div class="col-md-6"><b>Due Balance</b></div>\n' +
                '            <div class="col-md-6"><b>'+ Number(rawList[0]['due_balance']).toFixed(3)+'</b></div>\n' +
                '        </div>\n' +
                '</div><hr>'
            )

            $('#all_Details').append('' +
                ' <div class="task-name-wrap">\n' +
                '                                            <div class="row mt-2">\n' +
                '                                                <div class="col-md-6"><b>Description</b></div>\n' +
                '                                                <div class="col-md-3 text-right"><b>Receipts</b></div>\n' +
                '                                                <div class="col-md-3 text-right"><b>Payments</b></div>\n' +
                '                                            </div>\n' +
                '                                        </div><hr>')
            //loadingHeader
            let totalLoadingHeaderAmount = 0;
            if(rawList[0]['loading_header'].length != 0){
                for (var i = 0; i < rawList[0]['loading_header'].length; i++) {
                    $('#all_Details').append('' +
                        ' <div class="task-name-wrap">\n' +
                        '                                            <div class="row mt-2">\n' +
                        '                                                <div class="col-md-6"><i class="fa fa-check-circle" aria-hidden="true"></i> ' + rawList[0]['loading_header'][i]['category'] + ' | ' + rawList[0]['loading_header'][i]['loading_id'] + '</div>\n' +
                        '                                                <div class="col-md-3 text-right">' + Number(rawList[0]['loading_header'][i]['net_amount']).toFixed(3) + '</div>\n' +
                        '                                                <div class="col-md-3 text-right"></div>\n' +
                        '                                            </div>\n' +
                        '                                        </div>');
                    totalLoadingHeaderAmount = totalLoadingHeaderAmount+ parseFloat(rawList[0]['loading_header'][i]['net_amount']);
                }
            }
            if(totalLoadingHeaderAmount != 0){
                $('#all_Details').append('' +
                    ' <hr><div class="task-name-wrap">\n' +
                    '                                            <div class="row mt-2">\n' +
                    '                                                <div class="col-md-6"></div>\n' +
                    '                                                <div class="col-md-3 text-right">' + Number(totalLoadingHeaderAmount).toFixed(3) + '</div>\n' +
                    '                                                <div class="col-md-3 text-right"></div>\n' +
                    '                                            </div>\n' +
                    '                                        </div>');
            }
            let totalReduceAmount = 0;
            if(rawList[0]['sales_return'].length != 0){
                for (var i = 0; i < rawList[0]['sales_return'].length; i++) {
                    $('#all_Details').append('' +
                        ' <div class="task-name-wrap">\n' +
                        '                                            <div class="row mt-2">\n' +
                        '                                                <div class="col-md-6"><i class="fa fa-check-circle" aria-hidden="true"></i>  ' + rawList[0]['sales_return'][i]['sales_return_id'] + '</div>\n' +
                        '                                                <div class="col-md-3 text-right"></div>\n' +
                        '                                                <div class="col-md-3 text-right">(' + Number(rawList[0]['sales_return'][i]['final_amount']).toFixed(3) + ')</div>\n' +
                        '                                            </div>\n' +
                        '                                        </div>');
                    totalReduceAmount = totalReduceAmount+ parseFloat(rawList[0]['sales_return'][i]['final_amount']);
                }
            }
            if(rawList[0]['agent_payment'].length != 0){
                for (var i = 0; i < rawList[0]['agent_payment'].length; i++) {
                    $('#all_Details').append('' +
                        ' <div class="task-name-wrap">\n' +
                        '                                            <div class="row mt-2">\n' +
                        '                                                <div class="col-md-6"><i class="fa fa-check-circle" aria-hidden="true"></i>  '+rawList[0]['agent_payment'][i]['agent_payment_id'] +'</div>\n' +
                        '                                                <div class="col-md-3 text-right"></div>\n' +
                        '                                                <div class="col-md-3 text-right">(' + Number(rawList[0]['agent_payment'][i]['total_amount']).toFixed(3) + ')</div>\n' +
                        '                                            </div>\n' +
                        '                                        </div>');
                    totalReduceAmount = totalReduceAmount+parseFloat(rawList[0]['agent_payment'][i]['total_amount']);
                }
            }

            let final_amount = (totalLoadingHeaderAmount+parseFloat(rawList[0]['due_balance']))-(totalReduceAmount);
            $('#all_Details').append('' +
                ' <hr><div class="task-name-wrap">\n' +
                '                                            <div class="row mt-2">\n' +
                '                                                <div class="col-md-6">Final Amount</div>\n' +
                '                                                <div class="col-md-3 text-right"></div>\n' +
                '                                                <div class="col-md-3 text-right">' + parseFloat(final_amount).toFixed(3) + '</div>\n' +
                '                                            </div>\n' +
                '                                        </div><hr>')


        }

        function redirectToPrint()
        {
            let agentId = $("#agent_id option:selected").val();
            window.location.href = "agent-inquire/print/"+agentId;
        }

    </script>
@stop
