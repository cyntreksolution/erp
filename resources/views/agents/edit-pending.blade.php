@extends('layouts.back.master')
@section('title','Order Template')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .hide {
            display: none;
        }

        .app-main-content {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .portlet-placeholder {
            border: 1px dotted black;
            margin: 0 1em 1em 0;
            height: 100%;
        }

        #projects-task-modal .modal-bg {
            padding: 50px;
        }

        #projects-task-modal .modal-content {
            box-shadow: none;
            border: none
        }

        #projects-task-modal .close {
            border: none;
            outline: none;
            box-shadow: none;
            position: absolute;
            right: 1rem;
            top: 1rem;
            width: 25px;
            height: 25px;
            line-height: 25px;
            text-align: center;
            font-size: 1.13rem;
            border-radius: 500px;
            border: 1px solid #868e96
        }

        #floating-buttongtfr {
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #db4437;
            position: fixed;
            top: 13rem;
            right: 90px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .plus {
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }


    </style>
@stop
@section('content')
    <div class="app-wrapper">

        <div class="app-main">
            @if (Auth::user()->hasRole(['Sales Agent']))
                {{ Form::open(array('url' => route('buffer-stock.agent-request-update',$ab->id),'method'=>'POST'))}}
            @else
                {{ Form::open(array('url' => route('buffer-stock.agent-request-approve',$ab->id),'method'=>'POST'))}}
            @endif

            <div class="app-main-header">
                <div class="row">
                    {{--                    <div class="col-md-3">--}}
                    {{--                        <h5 class="aa app-main-heading text-left"><span>NEW SALES RETURN</span></h5>--}}
                    {{--                    </div>--}}
                    <div class="col-md-3 ">
                        {{$ab->salesRep->first_name}}
                    </div>
                    <div class="col-md-3 ">
                        {{$ab->template->category->name}}
                    </div>
                    <div class="col-md-3 ">
                        {{$ab->template->day}}
                    </div>
                    <div class="col-md-3 ">
                        {{$ab->template->time}}
                    </div>
                </div>
            </div>

            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">
                    <div class="container">
                        <input type="hidden" name="supplier_id" id="supplier_id">
                        <div class="projects-list abc">
                            @foreach(json_decode($ab->dataset) as $item)
                                @if(isset($item->end_product_id) && !empty(\EndProductManager\Models\EndProduct::whereId($item->end_product_id)->first()))
                                    @if((sizeof(\EndProductManager\Models\EndProduct::find($item->end_product_id)->variants) == 0))
                                        @php $idDiff = ($item->current_qty != $item->qty) ?true :false ;@endphp
                                        <div class=" {{($idDiff)?'text-danger':null}} card-body d-flex  align-items-center p-0 row w-100">
                                            <div class="col-md-8 activity-counters justify-content-between">
                                                <h6>{{\EndProductManager\Models\EndProduct::whereId($item->end_product_id)->first()->name}} </h6>
                                            </div>
                                            <div class="col-md-2 activity-counters justify-content-between">
                                                <h6>{{ empty(ltrim($item->current_qty, '0'))?0:ltrim($item->current_qty, '0')}} </h6>
                                            </div>
                                            <div class="col-md-2 mt-2 activity-counters justify-content-between">
                                                @if (Auth::user()->hasRole(['Sales Agent']))
                                                    <input type="text" name="qty[]" class="form-control"
                                                           value="{{$item->qty}}">
                                                @else
                                                    <h6>{{$item->qty}}</h6>
                                                @endif
                                                <input type="hidden" name="end_product[]" value="{{\EndProductManager\Models\EndProduct::whereId($item->end_product_id)->first()->id}}">
                                                <input type="hidden" name="current_end_qty[]" value="{{ empty(ltrim($item->current_qty, '0'))?0:ltrim($item->current_qty, '0')}}">
                                            </div>
                                        </div>
                                    @endif
                                @endif

                                @if(isset($item->variant_id) && !empty(\App\Variant::whereId($item->variant_id)->first()))
                                    @php $variant = \App\Variant::whereId($item->variant_id)->first();@endphp
                                    @php $idDiff = ($item->current_qty != $item->qty) ?true :false ;@endphp
                                    <div class=" {{($idDiff)?'text-danger':null}} card-body d-flex  align-items-center p-0 row w-100">
                                        <div class="col-md-8 activity-counters justify-content-between">
                                            <h6> {{$variant->product->name.' '. $variant->variant }} </h6>
                                        </div>
                                        <div class="col-md-2 activity-counters justify-content-between">
                                            <h6>{{ empty(ltrim($item->current_qty, '0'))?0:ltrim($item->current_qty, '0')}} </h6>
                                        </div>
                                        <div class="col-md-2 mt-2 activity-counters justify-content-between">
                                            @if (Auth::user()->hasRole(['Sales Agent']))
                                                <input type="text" name="variants_qty[]" class="form-control"
                                                       value="{{$item->qty}}">
                                            @else
                                                <h6>{{$item->qty}}</h6>
                                            @endif
                                            <input type="hidden" name="variants[]" value="{{$variant->id}}">
                                                <input type="hidden" name="current_variant_qty[]" value="{{ empty(ltrim($item->current_qty, '0'))?0:ltrim($item->current_qty, '0')}}">
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                            <hr>
                            @foreach($newItems as $item)
                                <div class=" card-body d-flex  align-items-center p-0 row w-100">
                                    <div class="col-md-1">
                                        <div>
                                            <div class="avatar avatar text-white avatar-md project-icon bg-primary">
                                                <img class="card-img-top" src="" alt="">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 activity-counters justify-content-between">
                                        <h6>{{\EndProductManager\Models\EndProduct::find($item)->name}} </h6>
                                    </div>
                                    <div class="col-md-3 activity-counters justify-content-between">
                                        <input type="text" name="qty[]" class="form-control" value="0">
                                        <input type="hidden" name="end_product[]" value="{{$item}}">
                                    </div>
                                </div>
                            @endforeach
                            @if (Auth::user()->hasRole(['Sales Agent']))
                                <button type="submit" class="btn btn-primary btn-lg btn-block mt-5 mb-5">Update Order
                                    Template Edit Request
                                </button>
                            @else
                                <button type="submit" class="btn btn-primary btn-lg btn-block mt-5 mb-5">Approve Order
                                    Template Edit Request
                                </button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            {!!Form::close()!!}
        </div>
    </div>

@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $('#category').on('change', function (e) {
            let optionSelected = $("option:selected", this);
            let valueSelected = this.value;

            let url = '/category/' + valueSelected + '/products';
            $.ajax({
                type: "get",
                url: url,
                success: function (response) {
                    $('#end_product').empty();
                    $.each(response, function (key, value) {
                        $('#end_product')
                            .append($("<option></option>")
                                .attr("value", key)
                                .text(value));
                    });
                    appendEndProducts(response)
                }
            });
        });

        function appendEndProducts(response) {
            $(".abc").empty();
            $.each(response, function (key, value) {
                $(".abc").append(' <div class=" card-body d-flex  align-items-center p-0 row w-100">' +
                    '                                <div class="col-md-1">' +
                    '                                    <div>' +
                    '                                        <div class="avatar avatar text-white avatar-md project-icon bg-primary">' +
                    '                                            <img class="card-img-top" src="" alt="">' +
                    '                                        </div>' +
                    '                                    </div>' +
                    '                                </div>' +
                    '                                <div class="col-md-8 activity-counters justify-content-between">' +
                    '                                    <h6>' + value + ' </h6>' +
                    '                                </div>' +
                    '' +
                    '                                <div class="col-md-3 activity-counters justify-content-between">\'' +
                    '                                    <input type="text" name="qty[]" class="form-control" value="0">' +
                    '                                    <input type="hidden" name="end_product[]" value="' + key + '">' +
                    '                                </div>' +
                    '                            </div>');
            });
        }

        $("#floating-button").click(function () {
            let end_product = $("#end_product option:selected").val();
            let qty = $('#qty').val();
            let type = $('#type').val();
            let type_text = $("#type option:selected").text();
            let total = parseFloat($('#total_value').text()).toFixed(2);


            if (end_product == 0 || qty == '') {
                Command: toastr["error"]("Select End Product and Quantity")
                return false;
            }


            $.ajax({
                type: "post",
                url: '/end_product/get/endpro',
                data: {id: end_product},
                success: function (response) {
                    let value = qty * response['selling_price'];
                    total = parseFloat(total) + parseFloat(value);
                    $(".abc").append('' +
                        '<div class=" card-body d-flex  align-items-center p-0" >' +
                        '<div class="col-md-1">' +
                        '<div>' +
                        '<div class="avatar avatar text-white avatar-md project-icon bg-primary">' +
                        '<img class="card-img-top" src="" alt="">' +
                        '</div>' +
                        '</div>' +
                        '<input type="hidden" name="end_product_ids[]" value=" ' + response['id'] + ' "/>' +
                        '<input type="hidden" name="unit_values[]" value=" ' + response['selling_price'] + ' "/>' +
                        '<input type="hidden" name="qty[]" value=" ' + qty + ' "/>' +
                        '</div>' +
                        '<div class="col-md-4 activity-counters justify-content-between">' +
                        '<div class="text-left">' +
                        '<div class="text-primary"><h6 >' + response['name'] + '</h6>' +
                        '</div>' +
                        '</div>' +
                        '</div> ' +
                        '<div class="col-md-3 activity-counters justify-content-between">' +
                        '<div class="text-left">' +
                        '<div class="text-primary"><h6 >' + type_text + '</h6>' +
                        '</div>' +
                        '</div>' +
                        '</div> ' +
                        '<div class="col-md-1 activity-counters justify-content-between">' +
                        '<div class="text-right">' +
                        '<div class="text-primary"><h6 >' + response['selling_price'] + '</h6>' +
                        '</div>' +
                        '</div>' +
                        '</div> ' +
                        '<div class="col-md-1 activity-counters justify-content-between">' +
                        '<div class="text-right">' +
                        '<div class="text-primary"><h6 >' + qty + '</h6>' +
                        '</div>' +
                        '</div>' +
                        '</div> ' +
                        '<div class="col-md-2 activity-counters justify-content-between">' +
                        '<div class="text-right">' +
                        '<div class="text-primary"><h6 >' + value + ' LKR </h6>' +
                        '</div>' +
                        '</div>' +
                        '</div> ' +
                        '</div>');
                    $("#total_value").html("");
                    document.getElementById("total_value").append(parseFloat(total).toFixed(2));
                    document.getElementById("hidden_total_value").value = parseFloat(total).toFixed(2);
                }
            });


            reset();
        });

        function reset() {
            document.getElementById("end_product").value = 0;
            document.getElementById("qty").value = "";
        }


    </script>
@stop

