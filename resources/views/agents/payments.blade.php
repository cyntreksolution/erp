@extends('layouts.back.master')
@section('title','Payments | List ')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <style>
        th {
            text-align: right !important;
        }

        td {
            text-align: right !important;
        }
    </style>
@stop

@section('content')
    <div class="">
        <div class="col-md-12 mt-5 float-right">
            <div class="row">
                <div class="col-md-6 float-right">
                  @if(Sentinel::check()->roles[0]->slug == 'sales-ref' || Sentinel::check()->roles[0]->slug == 'accountant' || Sentinel::check()->roles[0]->slug == 'owner')
                    <a href="{{route('agent-payment.create')}}" class="btn btn-block btn-success" name="saveButton"
                     value="saveSubmit">Create New Payment </a>
                  @endif
                </div>
            </div>

        </div>
        <div></div>
        <br>
        <table id="invoice_table" class="display text-center">
            <thead>
            <tr>
                <th>Date</th>
                <th>Serial</th>
                <th>Agent Name</th>
                <th>Agent Outstands</th>
                <th>Cheques</th>
                <th>Cash</th>
                <th>Total</th>
                <th>Status</th>
                @if(Auth::user()->hasRole(['Sales Agent','Owner','Super Admin']))
                    <th>Action</th>
                @endif
            </tr>
            </thead>
            <tbody>
{{--            @foreach($burdens as $burden)--}}
{{--                <tr>--}}
{{--                    <td>{{$burden->serial}}</td>--}}
{{--                    <td>{{$burden->endProduct->name}}</td>--}}
{{--                    <td>{{$burden->burden_size}}</td>--}}
{{--                    <td>{{$burden->expected_end_product_qty	}}</td>--}}
{{--                    <td>{{$burden->recipe->name}}</td>--}}
{{--                    <td>--}}
{{--                        <button class="btn btn-sm btn-primary"><i class="fa fa-eye"></i></button>--}}
{{--                    </td>--}}
{{--                </tr>--}}
{{--            @endforeach--}}
            </tbody>
            <tfoot>
            <tr>
                <th>Date</th>
                <th>Serial</th>
                <th>Agent Name</th>
                <th>Agent Outstands</th>
                <th>Cheques</th>
                <th>Cash</th>
                <th>Total</th>
                <th>Status</th>
                @if(Auth::user()->hasRole(['Sales Agent','Owner','Super Admin']))
                    <th>Action</th>
                @endif
            </tr>
            </tfoot>
        </table>


    </div>
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
    <script type="text/javascript"
            src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js"></script>
    <script>

        $(".addItem").click(function (event) {
            event.preventDefault();
            $("#qtyChangeForm").attr('action', '/end-burden/edit/qty/' + this.value);
            $('#ChangeQty').modal('show')
        });

        function process_form(e) {
            let category = $("#category").val();
            let day = $("#day").val();
            let time = $("#time").val();
            let date = $("#date").val();
            let agent = $("#agent").val();
            let table = $('#invoice_table').DataTable();
            table.ajax.url('/sr/table/data?category=' + category + '&day=' + day + '&date=' + date + '&agent=' + agent + '&time=' + time + '&filter=' + true).load();
        }


        function process_form_reset() {
            $("#category").val('');
            $("#day").val('');
            $("#time").val('');
            $("#date").val('');
            $("#agent").val('');
            let table = $('#invoice_table').DataTable();
            table.ajax.url('/sr/table/data').load();
        }

        $(document).ready(function () {
            table = $('#invoice_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "ajax": {
                    url: "{{url('/agent-payment/table/data')}}",
                    type: "get",
                    error: function () {  // error handling code
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    }
                },
                pageLength: 250,
                responsive: true
            });
        });

    </script>

@stop
