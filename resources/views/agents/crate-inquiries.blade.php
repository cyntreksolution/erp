@extends('layouts.back.master')@section('title','Crates Inquiries')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.wizard.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/modal.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/demos.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/css/site.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">
@stop


@section('content')
    <div class="app-wrapper">
        <div class="app-panel" id="app-panel">
            <div class="app-search">
                <h5 class="app-main-heading text-center">Agents</h5>
            </div>

            <div class="scroll-container">
                <div class="app-panel-inner">
                    @if(Auth::user()->hasRole(['Owner','Sales Agent','Super Admin']))
                        <div class="p-3">
                            <select id="agent_id" name="agent_id" class="form-control">
                                <option value="Null">Select</option>
                                @foreach($agents as $agent)
                                    <option value="{{$agent->id}}">{{$agent->email}}</option>
                                @endforeach
                            </select>
                        </div>
                    @else
                        <div class="p-3">
                            <input type="hidden" name="agent_id" id="agent_id" value="{{Auth::user()->id}}">

                        </div>
                    @endif

                    <hr class="m-0">
                    <div class="p-3">
                        <input type="text" name="date" id="date_range" class="form-control" placeholder="Select Date" required>

                    </div>

                    <div class="p-3">
                        <button onclick="showData()" class="btn btn-success py-3 btn-block" id="showBTN"><i
                                    class="fa fa-file"></i> SHOW
                        </button>
                    </div>
                    <hr class="m-0">
                        @if(Auth::user()->hasRole(['Owner','Sales Agent','Super Admin']))
                            <div class="p-3">
                            <button onclick="download()" class="btn btn-info py-3 btn-block" id="printBTN"><i class="fa fa-print"></i>PRINT
                            </button>

                        </div>
                    @endif

                </div>
            </div>


            <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show"><i
                        class="fa fa-chevron-right"></i>
                <i class="fa fa-chevron-left"></i>
            </a>

        </div>
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">Crates Movement</h5>
            </div>

            <div class="scroll-container">
                <div class="app-main-content">
                    <div class="media-list">
                        <div class="container" id="all_Details">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>

    <script>

        function download()

        {

            let agent = $('#agent_id').val();
            let date = $('#date_range').val();
            if (agent != '') {
                window.location.href = "/crate/inquiries/download/" + agent + "/" + date;
            }
// $('#all_Details').empty();
//
// $.ajaxSetup({
//     headers: {
//         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//     }
// });
// $.ajax({
//     type: "POST",
//     url: "/agent-inquiries/download",
//     data: {agent: agent, date: datex},
//     success: function (response) {
//
//     },
//     failure: function (errMsg) {
//
//     }
// });
        }
        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019",
            "maxDate":moment()
        });
        function showData() {
            let agent = $('#agent_id').val();
            let datex = $('#date_range').val();
            $('#all_Details').empty();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: "/crate/inquiries/render/data",
                data: {agent: agent, date: datex},
                success: function (response) {
                    $('#all_Details').append(response);
                },
                failure: function (errMsg) {

                }
            });
        }

        $(document).ready(function(){
            var date = new Date();

            var day = ("0" + date.getDate()).slice(-2); var month = ("0" + (date.getMonth() + 1)).slice(-2);

            var today = date.getFullYear()+"-"+(month)+"-"+(day) ;

            $('#date').val(today);
        });



    </script>
@stop
