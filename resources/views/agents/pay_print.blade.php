<html>
<head>
    <link rel="stylesheet" href="{{asset('assets/vendor/css/bootstrap.css')}}">
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/invoice.css')}}" rel="stylesheet">
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <style media="all">
        @page {
            margin: 30px 5px 30px 15px;
            size: 75mm 999mm;
        }
    </style>
</head>

<body style="margin-bottom: 100px">

<div class="row">

    <div class="col-xs-5">
        <img src="{{asset('assets/img/buntak_grace.jpeg')}}" style="width: 100px">
    </div>

    <div class="col-xs-7 text-left">
        <h6>
            <b>Ranjanas Holdings [Pvt] Ltd</b><br>
            Katukurunda<br>
            Habaraduwa<br>
            Hotline : +9477 330 4678<br>
            E-mail : buntalksl@icloud.com<br>
        </h6>
    </div>
</div>

<div class="row" style="margin-top: 15px;">

    <div class="col-xl-12 text-center">
        <h3><b>

                Payment Slip
            </b></h3>
    </div>



</div>

<div>
    <div class="row">
        <div class="col-xs-6 text-nowrap"><b>Pay Serial :</b></div>
        <div class="col-xs-6">{{$chkpay->serial}}</div>
    </div>

    <div class="row">
        <div class="col-xs-6 text-nowrap"><b>Agent Name :</b></div>
        <div class="col-xs-6">{{$agent->type.' '.$agent->name_with_initials}}</div>
    </div>
    <div class="row">
        <div class="col-xs-6 text-nowrap"><b>Territory :</b></div>
        <div class="col-xs-6">{{$agent->territory}}</div>
    </div>
    <div class="row">

        <div class="col-xs-6 text-nowrap"><b>Current Outstanding :</b></div>
        @if(empty($agent_init_value->amount))
            <div class="col-xs-6"><b>{{number_format(($creBal->end_balance),2)}}</b></div>
        @else
            <div class="col-xs-6"><b>{{number_format(($creBal->end_balance + $agent_init_value->amount),2)}}</b></div>
        @endif
    </div>
    <div class="row">
        <div class="col-xs-6 text-nowrap"><b>Paid Date with Time :</b></div>
        <div class="col-xs-6">{{$chkpay->created_at->format('Y-m-d H:i:s')}}</div>
    </div>

    <div class="row">
        <div class="col-xs-6 text-nowrap"><b>Print Date with Time :</b></div>
        <div class="col-xs-6">{{\Carbon\Carbon::now()->format('Y-m-d H:i:s')}}</div>
    </div>

</div>

<br>


<div class="row">
    <div class="col-xs-12">
        <table class="table">
            <thead>
            <tr>
                <th>Note</th>
                <th class="text-center">Qty</th>
                <th class="text-right">Amount</th>

            </tr>
            </thead>
            <tbody>

            @foreach(json_decode($chkpay->notes_data) as $key => $note)


                <tr>
                    <td> <h4 id="">{{$key}} x </h4></td>
                    <td align="center"> <h4 id=""> {{$note}}</h4></td>
                    @if($key == 'coins')
                    <td align="right"> <h4 id=""> {{number_format($note,2)}}</h4></td>
                    @else
                    <td align="right"> <h4 id=""> {{number_format(($note*$key),2)}}</h4></td>
                    @endif

                </tr>
            @endforeach
            <tr>
                <td> <h4 id="">Cheques Amount </h4></td><td></td><td align="right"> <h4 id=""> {{number_format($chkpay->cheque_amount_value,2)}}</h4></td>
            </tr>
            <tr>
                <td> <h4 id="">Total Paid</h4></td><td></td><td align="right"> <h4 id=""> {{number_format($chkpay->total_amount,2)}}</h4></td>
            </tr>
            </tbody>
        </table>
    </div>

</div>

<br>

<footer>


    <div class="row col-xs-12" style="margin-top: 60px;">

        <div class="text-center col-xs-12">
            <h5>
                .........................................<br>
               Name With Signature-Cash Collector
            </h5>
        </div>
    </div>
    <div class="row col-xs-12" style="margin-top: 60px;">
        <div class="text-center col-xs-12">
            <h5>
                .........................................<br>
                Signature- Agent
            </h5>
        </div>

    </div>
    <div class="row col-xs-12" style="margin-top: 60px;">
        <div class="text-center col-xs-12">
            <h5>
                .........................................<br>
                Signature-Accountant
            </h5>
        </div>

    </div>


</footer>

</body>

</html>

