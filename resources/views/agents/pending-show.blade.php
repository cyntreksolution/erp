@extends('layouts.back.master')@section('title','Purchasing order')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}">


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .app-wrapper {
            font-family: Raleway, sans-serif;
            width: 100%;
            border: 1px solid #eee;
        }

        .blinking {
            animation: blinkingText 0.8s infinite;
        }

        @keyframes blinkingText {
            0% {
                color: red;
            }
            49% {
                color: transparent;
            }
            50% {
                color: transparent;
            }
            99% {
                color: transparent;
            }
            100% {
                color: red;
            }
        }

        @media only screen and (max-width: 512px) {
            .form-control {
                /*width: 40% !important;*/
            }

            h5{
                font-size: 16px !important;
            }
        }
    </style>
@stop
@section('content')
    <div class="app-wrapper">

        <div class="app-main">
            <div class="row">
                <h5 class="p-3 border-b-1">ORDERED PRODUCTS</h5>
            </div>
                <div class="app-main-content">
                    <div class="container">
                        {!! Form::open(['route' => 'sales-request.store']) !!}
                        <input type="hidden" name="refOrder" value="true">
                        <input type="hidden" name="id" value="{{$order->id}}">

                            @foreach($order->items as $product)
{{--                                @if ($product->qty>0)--}}

                                    <input type="hidden" name="item_id[]" value="{{$product->id}}">

                                    <div class="row">
                                        <div class="col-xs-12 col-md-5">
                                            <input type="hidden" name="unit_price[]" class="form-control"
                                                   value="{{$product->endProduct->distributor_price}}">
                                                <h5 class="">   {{$product->endProduct->name}} ({{number_format((float)$product->endProduct->distributor_price, 2, '.', '')}})</h5>
                                        </div>
                                        <div class="col-xs-12 col-md-2 mb-2 ">
                                            <input type="text" disabled
                                                   class=" form-control"
                                                   value="{{$product->qty}}">
                                        </div>
                                        <div class="col-xs-12 col-md-4 ">
                                                <input type="text" step="0.01" required name="product_qty[]"
                                                       class=" form-control"
                                                       value="{{$product->qty}}">
                                        </div>
                                    </div>

{{--                                @endif--}}
                            @endforeach

                            @if(!empty($order->variants))
                                @foreach($order->variants as $product)
                                    <input type="hidden" name="variant_item_id[]" value="{{$product->id}}">
                                    @php $variant = \App\Variant::find($product->variant_id) @endphp
                                    @php $variant_end_product = \EndProductManager\Models\EndProduct::find($variant->end_product_id) @endphp

                                    <div class="row">
                                        <div class="col-md-5">

                                                <input type="hidden" name="unit_price[]" class="form-control"
                                                       value="{{$variant_end_product->distributor_price}}">
                                                <h5 class="">   {{$variant_end_product->name.' '.$variant->variant}} ({{number_format((float)$variant_end_product->distributor_price, 2, '.', '')}})</h5>

                                        </div>

                                        <div class="col-xs-12 col-md-2 mb-2 ">
                                            <input type="text" disabled
                                                   class=" form-control"
                                                   value="{{$product->weight}}">
                                        </div>

                                        <div class="col-xs-12 col-md-4 ">
                                                <input type="text" step="0.001" required name="variant_qty[]"
                                                       class=" form-control"
                                                       value="{{$product->weight}}">
                                        </div>

                                        <hr class="m-1">
                                    </div>
                                @endforeach
                            @endif


                            <br>
                            @if(!empty($order->rawItems))
                                @foreach($order->rawItems as $product)
                                    <input type="hidden" name="shop_items[]" value="{{$product->id}}">
                                    <input type="hidden" name="raw_id[]"
                                           value="{{$product->raw_material_id}}">

                                    <div class="card-body d-flex  align-items-center p-0 row">
                                        <div class="col-md-6">
                                            <div class="text-primary">
                                                <input type="hidden"
                                                       value="{{\RawMaterialManager\Models\RawMaterial::withoutGlobalScope('type')->whereRawMaterialId($product->raw_material_id)->first()->id}}">
                                                <h5 class="mt-3">   {{\RawMaterialManager\Models\RawMaterial::withoutGlobalScope('type')->whereRawMaterialId($product->raw_material_id)->first()->name}}</h5>
                                            </div>
                                        </div>

                                        <div class="col-md-3 text-center">
                                            <div class="text-primary">
                                                <h6>{{$product->qty}}</h6>
                                            </div>
                                        </div>
                                        {{--                                        <div class="col-md-3 text-center">--}}
                                        {{--                                            <div class="text-primary">--}}
                                        {{--                                                <input type="number" name="shop_items_qty[]"--}}
                                        {{--                                                       class=" form-control"--}}
                                        {{--                                                       value="{{$product->qty}}">--}}
                                        {{--                                            </div>--}}
                                        {{--                                        </div>--}}


                                    </div>
                                    <hr class="m-1">
                                @endforeach
                            @endif

                            @if(!empty($order->crates))

                                @foreach(json_decode($order->crates) as $product)
                                    <input type="hidden" name="item_id_r[]" value="{{$product->id}}">
                                    <input type="hidden" name="raw_id[]"
                                           value="{{$product->raw_material_id}}">
                                    <div class="card-body d-flex  align-items-center p-0 row">
                                        <div class="col-md-3 p-0">
                                            <div class="text-primary">
                                                <h5 class="mt-3">   {{\RawMaterialManager\Models\RawMaterial::withoutGlobalScope('type')->whereRawMaterialId($product->raw_material_id)->first()->name}}</h5>
                                            </div>
                                        </div>

                                        <div class="col-md-1 text-center">
                                            <div class="text-primary">
                                                <h6>{{$product->qty}}</h6>
                                            </div>
                                        </div>
                                        <div class="col-md-3 text-center">
                                            <div class="text-primary">
                                                {{$product->qty}}
                                            </div>
                                        </div>


                                    </div>
                                    <hr class="m-1">
                                @endforeach
                            @endif

                            <div class="col-md-12">
                                @foreach($crates as $crate)
                                    <?php $createName = \RawMaterialManager\Models\RawMaterial::withoutGlobalScopes()->whereRawMaterialId($crate['crate_id'])->get(); ?>

                                    <div class="card-body d-flex  align-items-center p-0 row">
                                        <div class="col-md-4">
                                            <div class="text-primary">
                                                <h5 class="mt-3"> {{$createName[0]->name}}</h5>
                                            </div>
                                        </div>

                                        <div class="col-md-3 text-center">
                                            <div class="text-primary">
                                                <input type="hidden" name="crate[]"
                                                       value="{{$crate['crate_id']}}">
                                                <input type="number" name="crate_qty[]" class="form-control"
                                                       value="{{$crate['qty']}}" disabled>
                                            </div>
                                        </div>
                                        {{--                                        <div class="col-md-3 text-center">--}}
                                        {{--                                            <div class="text-primary">--}}
                                        {{--                                                <input type="hidden" name="new_crate[]"--}}
                                        {{--                                                       value="{{$crate['crate_id']}}">--}}
                                        {{--                                                <input type="number" name="crate_qty_new[]"--}}
                                        {{--                                                       class="form-control"--}}
                                        {{--                                                       value="{{$crate['qty']}}">--}}
                                        {{--                                            </div>--}}
                                        {{--                                        </div>--}}
                                    </div>
                                @endforeach
                            </div>
                            <hr class="m-1">
                            <h6 class=" font-weight-bold mt-3">Before Accept Order Please Check Crates & Shop
                                Items Received Correctly.</h6>
                            <h6 class="font-weight-bold mt-3">If No - Contact Sales Department immediately
                                Before Accept Delevered Order</h6>
                            <h6 class="blinking  font-weight-bold mt-3">If Yes - Enter Return Crates Quantity
                                Correctly</h6>
                            <h5 class="mt-4"><b>Return Crates</b></h5>
                            <div class="col-md-12">
                                @foreach(\RawMaterialManager\Models\RawMaterial::withoutGlobalScopes()->whereType(5)->get() as $crate)
                                    @php
                                        $ag_id = \SalesOrderManager\Models\SalesOrder::where('id','=',$order->so_id)->first();


                                            $maxid =\App\CrateInquiryAgent::where('crate_id','=',$crate->raw_material_id)
                                                                        ->where('agent_id','=',$ag_id->created_by)
                                                                        ->orderByDesc('id')
                                                                        ->first();

                                            $cbal = \App\CrateInquiryAgent::where('id','=',$maxid->id)
                                                                        ->first();
                                             // dd($cbal);
                                    @endphp


                                    <div class="card-body d-flex  align-items-center p-0 row">
                                        <div class="col-md-4">
                                            <div class="text-primary">
                                                <h5 class="mt-3">{{$crate->name}}</h5>
                                            </div>
                                        </div>

                                        <div class="col-md-3 text-center">
                                            <div class="text-primary">
                                                <input type="hidden" name="return_crate[]"
                                                       value="{{$crate->raw_material_id}}">
                                                <input type="number" name="return_crate_qty[]"
                                                       value="0" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3 text-center">
                                            <div class="text-primary">
                                                <h6>Balance {{$crate->name}} = {{$cbal->end_balance}} </h6>
                                            </div>

                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <hr>
                            <div class="projects-list">
                                <div class="col-md-12 col-sm-12 my-4">
                                    <textarea name="description" class="form-control" rows="5"
                                              disabled>{{$order->extra_description}}</textarea>
                                </div>
                            </div>

                            <div id="appendDiv"></div>
                            <div class="mt-5 mb-5">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" id="btnSave" name="submit"
                                                class="btn btn-success btn-lg btn-block"
                                                value="stream"> Accept
                                        </button>
                                    </div>
                                </div>
                            </div>

                        {!!Form::close()!!}
                    </div>

                </div>


        </div>
    </div>
@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script>

        $(document).keypress(function (e) {
            // alert(e.keyCode);
            if (e.keyCode == 78 || e.keyCode == 110) {
                $("#btnNew").click();
            }
            if (e.keyCode == 76 || e.keyCode == 108) {
                $("#btnSave").click();
            }
            if (e.keyCode == 69 || e.keyCode == 101) {
                $("#endProduct").focus();
            }
            if (e.keyCode == 67 || e.keyCode == 99) {
                $("#btnClose").click();
            }
        });

        $("#myModal").on('shown', function () {
            $("#endProduct").focus();
        });

        $("#endProduct").change(function () {
            $("#qty").focus();
        });

        $("#qty").keyup(function (event) {
            if (event.keyCode === 13) {
                $("#btnAdd").click();
            }
        });

        $('#myModalt').on('shown', function () {
            alert(1)
        })

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        async function addProduct() {
            let product = document.getElementById("endProduct");
            let qty = $('#qty').val();
            let product_id = $(product).children("option:selected").val();
            let product_name = $(product).children("option:selected").text();
            let price = 0;
            await $.ajax({
                type: "post",
                data: {id: product_id},
                url: '/end_product/get/endpro',
                success: function (response) {
                    price = response.distributor_price;
                }
            });
            var $newdiv1 = $("<input type=\"hidden\" name=\"product_id[]\" value=\"" + product_id + "\">" +
                " <div class=\"card-body d-flex  align-items-center p-0\">\n" +
                "                                    <div class=\"d-flex mr-auto\">\n" +
                "                                        <div>\n" +
                "                                            <div class=\"avatar avatar text-white avatar-md project-icon bg-primary\">\n" +
                "                                            </div>\n" +
                "                                        </div>\n" +
                "                                        <h5 class=\"mt-3 mx-3\"> " + product_name + "</h5>\n" +
                "                                    </div>\n" +
                "                                    <div class=\"d-flex activity-counters justify-content-between\">\n" +
                "<input type=\"hidden\" name=\"unit_price[]\" class=\"form-control\"\n" +
                "                                                       value=\"" + parseFloat(price).toFixed(2) + "\">" +
                "                                        <div class=\"text-center px-5\">\n" +
                "                                            <div class=\"text-primary\">\n" +
                "                                                <h6>" + parseFloat(price).toFixed(2) + " LKR</h6>\n" +
                "                                            </div>\n" +
                "                                        </div>\n" +
                "<div class=\"text-center px-5\">\n" +
                "                                            <div class=\"text-primary\">\n" +
                "                                                <h6>" + qty + "</h6>\n" +
                "                                            </div>\n" +
                "                                        </div>\n" +
                "                                        <div class=\"text-center px-5\">\n" +
                "                                            <div class=\"text-primary\">\n" +
                "                                                <input type=\"number\" name=\"qty[]\" class=\"form-control\"\n" +
                "                                                       value=\"" + qty + "\">\n" +
                "                                            </div>\n" +
                "                                        </div>\n" +
                "                                    </div>\n" +
                "                                </div> " +
                " <hr class=\"m-1\">"),
                newdiv2 = document.createElement("div"),
                existingdiv1 = document.getElementById("appendDiv");

            $(existingdiv1).append($newdiv1, [newdiv2, existingdiv1]);

        }

        $('.delete').click(function (e) {
            e.preventDefault();
            id = $(this).attr("value");
            confirmAlert(id);

        });


        // $(document).keypress(function ( e ) {
        //     // You may replace `c` with whatever key you want
        //     if ((e.metaKey || e.ctrlKey) && ( String.fromCharCode(e.which).toLowerCase() === 'c') ) {
        //         console.log( "You pressed CTRL + C" );
        //     }
        // });

        function confirmAlert(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "delete",
                        url: id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            } else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

    </script>
@stop
