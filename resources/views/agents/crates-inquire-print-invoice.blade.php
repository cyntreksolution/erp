<html>
<head>
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/invoice.css')}}" rel="stylesheet">
    <style media="all">
        @page {
            margin: 30px 5px 30px 5px;
            size: 75mm 999mm;
        }
    </style>
</head>

<body style="margin-bottom: 100px">

<div class="row">

    <div class="col-xs-3">
        <img src="{{asset('assets/img/buntak_grace.jpeg')}}" style="width: 100px">
    </div>

    <div class="col-xs-6 text-right">
        <h6>
            <b>Ranjanas Holdings [Pvt] Ltd</b><br>
            Katukurunda<br>
            Habaraduwa<br>
            Hotline : +9477 330 4678<br>
        </h6>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-xs-3 text-nowrap">Sold Agent</div>
            <div class="col-xs-9">:{{$totalHeadersArray[0]['agent_name']}}</div>
        </div>
        <div class="row">
            <div class="col-xs-3 text-nowrap">Territory</div>
            <div class="col-xs-9">:{{$totalHeadersArray[0]['agent_territory']}}</div>
        </div>
        <div class="row">
            <div class="col-xs-3 text-nowrap">Date</div>
            <div class="col-xs-9">:{{date('Y-m-d H:i:s')}}</div>
        </div>
    </div>
</div>
<br>

@php
$totaldiffCrates193 = 0;
$totaldiffCrates194 = 0;
$totaldiffCrates195 = 0;
$totaldiffCrates196 = 0;
$totaldiffCrates197 = 0;
@endphp

<div class="row">
    <div class="col-xs-12">
        <table class="table">
            <thead>
              <tr>
                  <th rowspan="2" class="text-center">Description</th>
                  <th class="text-center" colspan="5">Crates Types</th>
              </tr>
              <tr>

                  <th class="text-right">BC-L</th>
                  <th class="text-right">BC-S</th>
                  <th class="text-right">CR-L</th>
                  <th class="text-right">CR-S</th>
                  <th class="text-right">LID</th>
              </tr>
            </thead>
            <tbody>
            <tbody>
              <tr>
                <td class="text-center">Opening Balance</td>
                @foreach($totalcratesArray as $key =>$totalcrate)
                @php
                switch($key)
                {
                  case 0:$totaldiffCrates193 = (int)$totalcrate;
                  break;
                  case 1:$totaldiffCrates194 = (int)$totalcrate;
                  break;
                  case 2:$totaldiffCrates195 = (int)$totalcrate;
                  break;
                  case 3:$totaldiffCrates196 = (int)$totalcrate;
                  break;
                  case 4:$totaldiffCrates197 = (int)$totalcrate;
                  break;
                }
                @endphp
                  <td class="text-right">{{$totalcrate}}</td>
                @endforeach
              </tr>
              <tr>

                  <td class="text-center"><b>Today Crates</b></td>
                  <td class="text-right"></td>
                  <td class="text-right"></td>
                  <td class="text-right"></td>
                  <td class="text-right"></td>
                  <td class="text-right"></td>
              </tr>
              @if(!empty($totalHeadersArray))
              @if(!empty($totalsendHeadersArray))
              <tr>

                  <td class="text-center"><u>Send Crates</u></td>
                  <td class="text-right"></td>
                  <td class="text-right"></td>
                  <td class="text-right"></td>
                  <td class="text-right"></td>
                  <td class="text-right"></td>
              </tr>
              @foreach($totalsendHeadersArray as $key => $totalcrate)
              <tr>
                @php
                $totaldiffCrates193 = $totaldiffCrates193+(int)$totalsendHeadersArray[$key]['totalsendCrates193'];
                $totaldiffCrates194 = $totaldiffCrates194+(int)$totalsendHeadersArray[$key]['totalsendCrates194'];
                $totaldiffCrates195 = $totaldiffCrates195+(int)$totalsendHeadersArray[$key]['totalsendCrates195'];
                $totaldiffCrates196 = $totaldiffCrates196+(int)$totalsendHeadersArray[$key]['totalsendCrates196'];
                $totaldiffCrates197 = $totaldiffCrates197+(int)$totalsendHeadersArray[$key]['totalsendCrates197'];
                @endphp
                <td class="text-center">{{$totalsendHeadersArray[$key]['category']." | ".$totalsendHeadersArray[$key]['loading_header_note']}}</td>
                <td class="text-right">{{$totalsendHeadersArray[$key]['totalsendCrates193']}}</td>
                <td class="text-right">{{$totalsendHeadersArray[$key]['totalsendCrates194']}}</td>
                <td class="text-right">{{$totalsendHeadersArray[$key]['totalsendCrates195']}}</td>
                <td class="text-right">{{$totalsendHeadersArray[$key]['totalsendCrates196']}}</td>
                <td class="text-right">{{$totalsendHeadersArray[$key]['totalsendCrates197']}}</td>
              </tr>
              @endforeach
              <tr>
                <td class="text-center"></td>
                <td class="text-right">{{$totaldiffCrates193}}</td>
                <td class="text-right">{{$totaldiffCrates194}}</td>
                <td class="text-right">{{$totaldiffCrates195}}</td>
                <td class="text-right">{{$totaldiffCrates196}}</td>
                <td class="text-right">{{$totaldiffCrates197}}</td>
              </tr>
              @endif
              @if(!empty($totalreturnHeadersArray))
              <tr>

                  <td class="text-center"><u>Return Crates</u></td>
                  <td class="text-right"></td>
                  <td class="text-right"></td>
                  <td class="text-right"></td>
                  <td class="text-right"></td>
                  <td class="text-right"></td>
              </tr>
              @foreach($totalreturnHeadersArray as $key => $totalcrate)
              <tr>
                @php
                $totaldiffCrates193 = $totaldiffCrates193-(int)$totalreturnHeadersArray[$key]['totalreturnCrates193'];
                $totaldiffCrates194 = $totaldiffCrates194-(int)$totalreturnHeadersArray[$key]['totalreturnCrates194'];
                $totaldiffCrates195 = $totaldiffCrates195-(int)$totalreturnHeadersArray[$key]['totalreturnCrates195'];
                $totaldiffCrates196 = $totaldiffCrates196-(int)$totalreturnHeadersArray[$key]['totalreturnCrates196'];
                $totaldiffCrates197 = $totaldiffCrates197-(int)$totalreturnHeadersArray[$key]['totalreturnCrates197'];
                @endphp
                <td class="text-center">{{$totalreturnHeadersArray[$key]['category']." | ".$totalreturnHeadersArray[$key]['loading_header_note']}}</td>
                <td class="text-right">{{$totalreturnHeadersArray[$key]['totalreturnCrates193']}}</td>
                <td class="text-right">{{$totalreturnHeadersArray[$key]['totalreturnCrates194']}}</td>
                <td class="text-right">{{$totalreturnHeadersArray[$key]['totalreturnCrates195']}}</td>
                <td class="text-right">{{$totalreturnHeadersArray[$key]['totalreturnCrates196']}}</td>
                <td class="text-right">{{$totalreturnHeadersArray[$key]['totalreturnCrates197']}}</td>
              </tr>
              @endforeach
              @endif
              @endif
              <tr>
                <td class="text-center">Final Quantity</td>
                <td class="text-right">{{$totaldiffCrates193}}</td>
                <td class="text-right">{{$totaldiffCrates194}}</td>
                <td class="text-right">{{$totaldiffCrates195}}</td>
                <td class="text-right">{{$totaldiffCrates196}}</td>
                <td class="text-right">{{$totaldiffCrates197}}</td>
              </tr>
            </tbody>
        </table>
    </div>

</div>
<br>

<footer>
    <div class="row" style="margin-top: 20px;">
        <h4 align="center">
            <h6 class="text-muted">Powered by Cyntrek Solutions<br>
                <h7 class="num">+9471 122 5489 / +9471 532 2262</h7>
            </h6>
        </h4>
    </div>

    <p style="margin-top: 150px">--------------------------------------------------------------</p>
</footer>

</body>

</html>
