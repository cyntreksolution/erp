@extends('layouts.back.master')@section('title','Crates Inquire')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.wizard.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/modal.css')}}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/demos.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/css/site.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/packages/recipe/recipe.css')}}">
@stop


@section('content')
    <div class="app-wrapper">
        <input type="hidden" id="agent_id" value="{{$agentId}}">
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">Details</h5>
            </div>

            <div class="scroll-container">
                <div class="app-main-content">
                    <div class="media-list">
                        <div class="container" id="all_Details">
                            <br>
                            <div class="task-name-wrap">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-4"><b>Agent Name</b></div>
                                        <div class="col-md-6"><b>{{$totalHeadersArray[0]['agent_name']}}</b></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4"><b>Agent Territory</b></div>
                                        <div class="col-md-6"><b>{{$totalHeadersArray[0]['agent_territory']}}</b></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4"><b>Date</b></div>
                                        <div class="col-md-6"><b>{{date('Y-m-d')}}</b></div>
                                    </div>
                                </div>
                            </div>
                            <br>

                            @php
                                $totaldiffCrates193 = 0;
                                $totaldiffCrates194 = 0;
                                $totaldiffCrates195 = 0;
                                $totaldiffCrates196 = 0;
                                $totaldiffCrates197 = 0;
                            @endphp

                            <div class="task-name-wrap mt-3">
                                <div class="row mt-3">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th rowspan="2" class="text-center">Description</th>
                                            <th class="text-center" colspan="5">Crates Types</th>
                                        </tr>
                                        <tr>

                                            <th class="text-right">Box Crate [ L ]</th>
                                            <th class="text-right">Box Crate [ S ]</th>
                                            <th class="text-right">Crate [ L ]</th>
                                            <th class="text-right">Crate [ S ]</th>
                                            <th class="text-right">Crate Lid</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <th class="text-center">Opening Balance</th>
                                            @foreach($totalcratesArray as $key =>$totalcrate)
                                                @php
                                                    switch($key)
                                                    {
                                                      case 0:$totaldiffCrates193 = (int)$totalcrate;
                                                      break;
                                                      case 1:$totaldiffCrates194 = (int)$totalcrate;
                                                      break;
                                                      case 2:$totaldiffCrates195 = (int)$totalcrate;
                                                      break;
                                                      case 3:$totaldiffCrates196 = (int)$totalcrate;
                                                      break;
                                                      case 4:$totaldiffCrates197 = (int)$totalcrate;
                                                      break;
                                                    }
                                                @endphp
                                                <th class="text-right">{{$totalcrate}}</th>
                                            @endforeach
                                        </tr>
                                        <tr>

                                            <th class="text-center"><b>Today Crates</b></th>
                                            <th class="text-right"></th>
                                            <th class="text-right"></th>
                                            <th class="text-right"></th>
                                            <th class="text-right"></th>
                                            <th class="text-right"></th>
                                        </tr>
                                        @if(!empty($totalHeadersArray))
                                            @if(!empty($totalsendHeadersArray))
                                                <tr>
                                                    <th class="text-center"><u>Send Crates</u></th>
                                                    <th class="text-right"></th>
                                                    <th class="text-right"></th>
                                                    <th class="text-right"></th>
                                                    <th class="text-right"></th>
                                                    <th class="text-right"></th>
                                                </tr>
                                                @foreach($totalsendHeadersArray as $key => $totalcrate)
                                                    <tr>
                                                        @php
                                                            $totaldiffCrates193 = $totaldiffCrates193+(int)$totalsendHeadersArray[$key]['totalsendCrates193'];
                                                            $totaldiffCrates194 = $totaldiffCrates194+(int)$totalsendHeadersArray[$key]['totalsendCrates194'];
                                                            $totaldiffCrates195 = $totaldiffCrates195+(int)$totalsendHeadersArray[$key]['totalsendCrates195'];
                                                            $totaldiffCrates196 = $totaldiffCrates196+(int)$totalsendHeadersArray[$key]['totalsendCrates196'];
                                                            $totaldiffCrates197 = $totaldiffCrates197+(int)$totalsendHeadersArray[$key]['totalsendCrates197'];
                                                        @endphp
                                                        <th class="text-center">{{$totalsendHeadersArray[$key]['category']." | ".$totalsendHeadersArray[$key]['loading_header_note']}}</th>
                                                        <th class="text-right">{{$totalsendHeadersArray[$key]['totalsendCrates193']}}</th>
                                                        <th class="text-right">{{$totalsendHeadersArray[$key]['totalsendCrates194']}}</th>
                                                        <th class="text-right">{{$totalsendHeadersArray[$key]['totalsendCrates195']}}</th>
                                                        <th class="text-right">{{$totalsendHeadersArray[$key]['totalsendCrates196']}}</th>
                                                        <th class="text-right">{{$totalsendHeadersArray[$key]['totalsendCrates197']}}</th>
                                                    </tr>
                                                @endforeach
                                                <tr>
                                                    <th class="text-center"></th>
                                                    <th class="text-right">{{$totaldiffCrates193}}</th>
                                                    <th class="text-right">{{$totaldiffCrates194}}</th>
                                                    <th class="text-right">{{$totaldiffCrates195}}</th>
                                                    <th class="text-right">{{$totaldiffCrates196}}</th>
                                                    <th class="text-right">{{$totaldiffCrates197}}</th>
                                                </tr>
                                            @endif
                                            @if(!empty($totalreturnHeadersArray))
                                                <tr>

                                                    <th class="text-center"><u>Return Crates</u></th>
                                                    <th class="text-right"></th>
                                                    <th class="text-right"></th>
                                                    <th class="text-right"></th>
                                                    <th class="text-right"></th>
                                                    <th class="text-right"></th>
                                                </tr>
                                                @foreach($totalreturnHeadersArray as $key => $totalcrate)
                                                    <tr>
                                                        @php
                                                            $totaldiffCrates193 = $totaldiffCrates193-(int)$totalreturnHeadersArray[$key]['totalreturnCrates193'];
                                                            $totaldiffCrates194 = $totaldiffCrates194-(int)$totalreturnHeadersArray[$key]['totalreturnCrates194'];
                                                            $totaldiffCrates195 = $totaldiffCrates195-(int)$totalreturnHeadersArray[$key]['totalreturnCrates195'];
                                                            $totaldiffCrates196 = $totaldiffCrates196-(int)$totalreturnHeadersArray[$key]['totalreturnCrates196'];
                                                            $totaldiffCrates197 = $totaldiffCrates197-(int)$totalreturnHeadersArray[$key]['totalreturnCrates197'];
                                                        @endphp
                                                        <th class="text-center">{{$totalreturnHeadersArray[$key]['category']." | ".$totalreturnHeadersArray[$key]['loading_header_note']}}</th>
                                                        <th class="text-right">{{$totalreturnHeadersArray[$key]['totalreturnCrates193']}}</th>
                                                        <th class="text-right">{{$totalreturnHeadersArray[$key]['totalreturnCrates194']}}</th>
                                                        <th class="text-right">{{$totalreturnHeadersArray[$key]['totalreturnCrates195']}}</th>
                                                        <th class="text-right">{{$totalreturnHeadersArray[$key]['totalreturnCrates196']}}</th>
                                                        <th class="text-right">{{$totalreturnHeadersArray[$key]['totalreturnCrates197']}}</th>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        @endif
                                        <tr>
                                            <th class="text-center">Final Quantity</th>
                                            <th class="text-right">{{$totaldiffCrates193}}</th>
                                            <th class="text-right">{{$totaldiffCrates194}}</th>
                                            <th class="text-right">{{$totaldiffCrates195}}</th>
                                            <th class="text-right">{{$totaldiffCrates196}}</th>
                                            <th class="text-right">{{$totaldiffCrates197}}</th>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    {{--    <script src="{{asset('assets/examples/js/apps/projects.js')}}"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('assets/packages/semi_finish/semi_finish.js')}}"></script>
    <script src="{{asset('assets/packages/recipe/end-recipe.js')}}"></script>

    <script>
        // $(document).ready(function () {
        //     let agentId = $("#agent_id").val();
        //     if(agentId == 'Null'){
        //       $('#all_Details').empty();
        //     }else{
        //       $.ajax({
        //           url: "/agent-inquire/data/" + agentId,
        //           type: "GET",
        //           success: function (data) {
        //             // console.log(data);
        //               appendRawList(data);
        //           },
        //           error: function (xhr, status) {
        //
        //           }
        //       });
        //     }
        // });
        //
        //
        //
        //
        //     function appendRawList(rawList) {
        //         let flag = 0;
        //         let today = new Date();
        //         var dd = String(today.getDate()).padStart(2, '0');
        //         var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        //         var yyyy = today.getFullYear();
        //         today = yyyy + '-' + mm + '-' + dd;
        //         $('#all_Details').empty();
        //         $('#all_Details').append('' +
        //             '<div class="task-name-wrap">\n' +
        //             '        <div class="row mt-2">\n' +
        //             '            <div class="col-md-6"><b>Agent Name</b></div>\n' +
        //             '            <div class="col-md-6"><b>'+rawList[0]['agent_name']+'</b></div>\n' +
        //             '        </div>\n' +
        //             '</div>\n'+
        //             ' <div class="task-name-wrap">\n' +
        //             '                                            <div class="row mt-2">\n' +
        //             '                                                <div class="col-md-6"><b>Territory</b></div>\n' +
        //             '                                                <div class="col-md-6"><b>'+rawList[0]['agent_territory']+'</b></div>\n' +
        //             '                                            </div>\n' +
        //             '                                        </div>\n'+
        //             '<div class="task-name-wrap">\n' +
        //             '        <div class="row mt-2">\n' +
        //             '            <div class="col-md-6"><b>Date</b></div>\n' +
        //             '            <div class="col-md-6"><b>'+ today+'</b></div>\n' +
        //             '        </div>\n' +
        //             '</div>\n'+
        //             '<div class="task-name-wrap">\n' +
        //             '        <div class="row mt-2">\n' +
        //             '            <div class="col-md-6"><b>Due Balance</b></div>\n' +
        //             '            <div class="col-md-6"><b>'+ Number(rawList[0]['due_balance']).toFixed(3)+'</b></div>\n' +
        //             '        </div>\n' +
        //             '</div><hr>'
        //           )
        //
        //             $('#all_Details').append('' +
        //                 ' <div class="task-name-wrap">\n' +
        //                 '                                            <div class="row mt-2">\n' +
        //                 '                                                <div class="col-md-6"><b>Description</b></div>\n' +
        //                 '                                                <div class="col-md-3 text-right"><b>Receipts</b></div>\n' +
        //                 '                                                <div class="col-md-3 text-right"><b>Payments</b></div>\n' +
        //                 '                                            </div>\n' +
        //                 '                                        </div><hr>')
        //                 //loadingHeader
        //                 let totalLoadingHeaderAmount = 0;
        //                 if(rawList[0]['loading_header'].length != 0){
        //                 for (var i = 0; i < rawList[0]['loading_header'].length; i++) {
        //                   $('#all_Details').append('' +
        //                       ' <div class="task-name-wrap">\n' +
        //                       '                                            <div class="row mt-2">\n' +
        //                       '                                                <div class="col-md-6"><i class="fa fa-check-circle" aria-hidden="true"></i> ' + rawList[0]['loading_header'][i]['category'] + ' | ' + rawList[0]['loading_header'][i]['loading_id'] + '</div>\n' +
        //                       '                                                <div class="col-md-3 text-right">' + Number(rawList[0]['loading_header'][i]['net_amount']).toFixed(3) + '</div>\n' +
        //                       '                                                <div class="col-md-3 text-right"></div>\n' +
        //                       '                                            </div>\n' +
        //                       '                                        </div>');
        //                       totalLoadingHeaderAmount = totalLoadingHeaderAmount+ rawList[0]['loading_header'][i]['net_amount'];
        //                 }
        //               }
        //               if(totalLoadingHeaderAmount != 0){
        //               $('#all_Details').append('' +
        //                   ' <hr><div class="task-name-wrap">\n' +
        //                   '                                            <div class="row mt-2">\n' +
        //                   '                                                <div class="col-md-6"></div>\n' +
        //                   '                                                <div class="col-md-3 text-right">' + Number(totalLoadingHeaderAmount).toFixed(3) + '</div>\n' +
        //                   '                                                <div class="col-md-3 text-right"></div>\n' +
        //                   '                                            </div>\n' +
        //                   '                                        </div>');
        //                 }
        //                   let totalReduceAmount = 0;
        //               if(rawList[0]['sales_return'].length != 0){
        //                 for (var i = 0; i < rawList[0]['sales_return'].length; i++) {
        //                   $('#all_Details').append('' +
        //                       ' <div class="task-name-wrap">\n' +
        //                       '                                            <div class="row mt-2">\n' +
        //                       '                                                <div class="col-md-6"><i class="fa fa-check-circle" aria-hidden="true"></i>  ' + rawList[0]['sales_return'][i]['sales_return_id'] + '</div>\n' +
        //                       '                                                <div class="col-md-3 text-right"></div>\n' +
        //                       '                                                <div class="col-md-3 text-right">(' + Number(rawList[0]['sales_return'][i]['final_amount']).toFixed(3) + ')</div>\n' +
        //                       '                                            </div>\n' +
        //                       '                                        </div>');
        //                       totalReduceAmount = totalReduceAmount+ rawList[0]['sales_return'][i]['final_amount'];
        //                     }
        //               }
        //               if(rawList[0]['agent_payment'].length != 0){
        //                 for (var i = 0; i < rawList[0]['agent_payment'].length; i++) {
        //                 $('#all_Details').append('' +
        //                     ' <div class="task-name-wrap">\n' +
        //                     '                                            <div class="row mt-2">\n' +
        //                     '                                                <div class="col-md-6"><i class="fa fa-check-circle" aria-hidden="true"></i>  '+rawList[0]['agent_payment'][i]['agent_payment_id'] +'</div>\n' +
        //                     '                                                <div class="col-md-3 text-right"></div>\n' +
        //                     '                                                <div class="col-md-3 text-right">(' + Number(rawList[0]['agent_payment'][i]['total_amount']).toFixed(3) + ')</div>\n' +
        //                     '                                            </div>\n' +
        //                     '                                        </div>');
        //                     totalReduceAmount = totalReduceAmount+rawList[0]['agent_payment'][i]['total_amount'];
        //                 }
        //               }
        //
        //                 let final_amount = (totalLoadingHeaderAmount+rawList[0]['due_balance'])-(totalReduceAmount);
        //             $('#all_Details').append('' +
        //                 ' <hr><div class="task-name-wrap">\n' +
        //                 '                                            <div class="row mt-2">\n' +
        //                 '                                                <div class="col-md-6">Final Amount</div>\n' +
        //                 '                                                <div class="col-md-3 text-right"></div>\n' +
        //                 '                                                <div class="col-md-3 text-right">' + parseFloat(final_amount).toFixed(3) + '</div>\n' +
        //                 '                                            </div>\n' +
        //                 '                                        </div><hr>')
        //
        //
        //     }

    </script>
@stop
