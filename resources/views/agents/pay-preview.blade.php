@extends('layouts.back.master')@section('title','Payment Preview')
@section('css')
@stop
@section('content')


    <div class="col-md-12">
        <div class="col-md-6"><h5> </h5></div>
    </div>
    <div class="col-md-12">
        <div class="col-md-6"><h5>  </h5></div>
    </div>

    <div class="col-md-12">
        <div class="col-md-12"><h5> Payment Details - {{$payment->serial}} [ {{$agent->name_with_initials}} ]</h5></div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="app-main-content px-3 py-4">
                <div class="content">
                    <div class="row ">

                        <div class="form-group col-md-4">
                            <label for="">Paid Date</label>
                        </div>
                        <div class="col-md-2 text-right">
                            <label class="text-right">Invoice Number</label>
                        </div>
                        <div class="col-md-2 text-right">
                            <label class="text-right">Due Amount</label>
                        </div>
                        <div class="col-md-2 text-right">
                            <label class="text-right">Paid Amount</label>
                        </div>
                        <div class="col-md-2 text-right">
                            <label class="text-right">Last Due</label>
                        </div>
                    </div>
                    @php   $totpaid = 0; @endphp

                            @foreach(json_decode($payment->invoice_data) as $key => $ids)
                                 @php

                                 $inv = \App\LoadingHeader::find($ids->loading_header_id);

                                 @endphp

                                        <div class="row ">

                                            <div class="form-group col-md-4">
                                                <label for="">{{$payment->created_at}}</label>
                                            </div>
                                            <div class="col-md-2 text-right">
                                                <label class="text-right">{{$inv->invoice_number}}</label>
                                            </div>

                                            <div class="col-md-2 text-right">
                                                <label class="text-right">{{number_format($ids->invoice_value,2)}}</label>
                                            </div>
                                            <div class="col-md-2 text-right">
                                                <label class="text-right">{{number_format($ids->settled_value,2)}}</label>
                                            </div>
                                            <div class="col-md-2 text-right">
                                                <label class="text-right">{{number_format((($ids->invoice_value)-($ids->settled_value)),2)}}</label>
                                            </div>

                                        </div>

                                        @php   $totpaid = $ids->settled_value + $totpaid ; @endphp

                            @endforeach


                    <div class="row ">

                        <div class="form-group col-md-4">
                            <label for=""></label>
                        </div>
                        <div class="col-md-2 text-right">
                            <label class="text-right">Total Paid</label>
                        </div>
                        <div class="col-md-2 text-right">
                            <label class="text-right"></label>
                        </div>
                        <div class="col-md-2 text-right">
                            <label class="text-right">{{number_format($totpaid,2)}}</label>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <a class="btn btn-block btn-warning" href="{{ url()->previous() }}">Back</a>
                    </div>

                </div>
            </div>
        </div>
    </div>


@stop
@section('js')


    <script>
        function checkItem(id) {
            let name = '#end_product_commission_' + id;
            let valu_name = '#end_product_commission_value_' + id;
            if ($(name).prop("checked") == true) {
                $(valu_name).removeAttr('disabled');
            } else if ($(name).prop("checked") == false) {
                $(valu_name).attr('disabled');
            }
        }
    </script>



@stop
