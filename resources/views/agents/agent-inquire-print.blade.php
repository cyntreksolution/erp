<html>
<head>
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/invoice.css')}}" rel="stylesheet">
    <style media="all">
        @page {
            margin: 30px 5px 30px 5px;
            size: 75mm 999mm;
        }
    </style>
</head>

<body style="margin-bottom: 100px">

<div class="row">

    <div class="col-xs-3">
        <img src="{{asset('assets/img/buntak_grace.jpeg')}}" style="width: 100px">
    </div>

    <div class="col-xs-6 text-right">
        <h6>
            <b>Ranjanas Holdings [Pvt] Ltd</b><br>
            Katukurunda<br>
            Habaraduwa<br>
            Hotline : +9477 330 4678<br>
        </h6>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-xs-3 text-nowrap">Sold Agent</div>
            <div class="col-xs-9">:{{$invoices[0]['agent_name']}}</div>
        </div>
        <div class="row">
            <div class="col-xs-3 text-nowrap">Territory</div>
            <div class="col-xs-9">:{{$invoices[0]['agent_territory']}}</div>
        </div>
        <div class="row">
            <div class="col-xs-3 text-nowrap">Date</div>
            <div class="col-xs-9">:{{date('Y-m-d H:i:s')}}</div>
        </div>
        <div class="row">
            <div class="col-xs-3 text-nowrap">Due Balance</div>
            <div class="col-xs-9">: {{number_format(($invoices[0]['due_balance']), 3, '.', '')}}</div>
        </div>


    </div>
</div>
<br>


<div class="row">
    <div class="col-xs-12">
        <table class="table">
            <thead>
            <tr>
                <th>Description</th>
                <th class="text-center">Receipts</th>
                <th class="text-right">Payments</th>
            </tr>
            </thead>
            <tbody>
            @php $counter= 0; $totalsum = 0; @endphp
            @foreach($invoices[0]['loading_header'] as $loading)
                    <tr>
                        <td>{{$loading['category']}} | {{$loading['loading_id']}}</td>
                        <td class="text-center">{{number_format((float)($loading['net_amount']), 3, '.', '')}}</td>
                        <td class="text-right"></td>
                    </tr>
                    @php  $counter = $counter+1 ; $totalsum = $totalsum + number_format((float)($loading['net_amount']), 3, '.', ''); @endphp
            @endforeach
            <!-- @if($totalsum != 0)
                    <tr>
                        <td></td>
                        <td class="text-center">{{ number_format((float)($totalsum), 3, '.', '')}}</td>
                        <td class="text-right"></td>
                    </tr>
            @endif -->
                    @php $counter= 0; $totaldecrease = 0; @endphp
                    @foreach($invoices[0]['sales_return'] as $salesReturn)
                            <tr>
                                <td>{{$salesReturn['sales_return_id']}}</td>
                                <td class="text-center"></td>
                                <td class="text-right">({{number_format((float)($salesReturn['final_amount']), 3, '.', '')}})</td>
                            </tr>
                            @php  $counter = $counter+1 ; $totaldecrease = $totaldecrease + number_format((float)($salesReturn['final_amount']), 3, '.', ''); @endphp
                    @endforeach
                    @php $counter= 0; @endphp
                    @foreach($invoices[0]['agent_payment'] as $agentPayment)
                            <tr>
                                <td>{{$agentPayment['agent_payment_id']}}</td>
                                <td class="text-center"></td>
                                <td class="text-right">({{number_format((float)($agentPayment['total_amount']), 3, '.', '')}})</td>
                            </tr>
                            @php  $counter = $counter+1 ; $totaldecrease = $totaldecrease + number_format((float)($agentPayment['total_amount']), 3, '.', ''); @endphp
                    @endforeach
                    <tr>
                        <td>Final Amount</td>
                        @php $finalAmount =(number_format(($invoices[0]['due_balance']), 3, '.', '')+$totalsum)-$totaldecrease; @endphp
                        <td class="text-center"></td>
                        <td class="text-right">{{ number_format((float)($finalAmount), 3, '.', '')}}</td>
                    </tr>
            </tbody>
        </table>
    </div>

</div>
<br>

<footer>
    <div class="row" style="margin-top: 20px;">
        <h4 align="center">
            <h6 class="text-muted">Powered by Cyntrek Solutions<br>
                <h7 class="num">+9471 122 5489 / +9471 532 2262</h7>
            </h6>
        </h4>
    </div>

    <p style="margin-top: 150px">--------------------------------------------------------------</p>
</footer>

</body>

</html>
