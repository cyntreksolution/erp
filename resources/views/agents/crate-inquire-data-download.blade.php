<html>
<head>
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/invoice.css')}}" rel="stylesheet">
    <style media="all">

        @page {
            margin: 30px 5px 30px 5px;
            size: 75mm 999mm;
        }
    </style>
</head>

<body style="margin-bottom: 100px">

<div class="row">

    <div class="col-xs-6">
        <img src="{{asset('assets/img/buntak_grace.jpeg')}}" style="width: 100px">
    </div>

    <div class="col-xs-6 text-right">
        <h6>
            <b>Ranjanas Holdings [Pvt] Ltd</b><br>
            Katukurunda<br>
            Habaraduwa<br>
            Hotline : +9477 330 4678<br>
        </h6>
    </div>
</div>
<div class="task-name-wrap">
    <div class="row mt-2">
        <div class="col-md-6"><b>Agent Name</b></div>
        <div class="col-md-6"><b>{{$agent->first_name.' '.$agent->last_name}}</b></div>
    </div>
</div>
<div class="task-name-wrap">
    <div class="row mt-2">
        <div class="col-md-6"><b>Territory</b></div>
        <div class="col-md-6"><b>{{$agent->territory}}</b></div>

    </div>
</div>

<div class="task-name-wrap">
    <div class="row mt-6">
        <div class="col-sm-2"><b>Desc</b></div>
        <div class="col-sm-2"><b>BC [L]</b></div>
        <div class="col-sm-2"><b>BC [S]</b></div>
        <div class="col-sm-2"><b>CR [L]</b></div>
        <div class="col-sm-2"><b>CR [S]</b></div>
        <div class="col-sm-2"><b>LID</b></div>

    </div>
    <hr>

    @php $total= $crate_records->count();$count=0;@endphp
    @foreach($crate_records as $key => $records)
        <div class="row mt-6">
            <div class="col-sm-2"><h6 class="-bold"><b><u>{{$key}}</u></b></h6></div>
            @php $dataset =[];$dataset[193]=0;$dataset[194]=0;$dataset[195]=0;$dataset[196]=0;$dataset[197]=0; @endphp
            @php $dataset_data =[];$dataset_data[193]=0;$dataset_data[194]=0;$dataset_data[195]=0;$dataset_data[196]=0;$dataset_data[197]=0; @endphp
            @php $dataset_end =[];$dataset_end[193]=0;$dataset_end[194]=0;$dataset_end[195]=0;$dataset_end[196]=0;$dataset_end[197]=0; @endphp
            @foreach($records->toArray() as $record)
                @php
                    // dd($record);
                         switch ($record['crate_id']){
                             case 193:
                                 if ($dataset[193]==0) {
                                    $dataset[193]=$record['start_balance'];
                                 }
                                 if ($dataset_end[193]==0) {
                                    $dataset_end[193]=$record['end_balance'];
                                 }

                               //  $dataset_data[$record['reference']][193]=$record['qty'];
                                 break;
                             case 194:
                                if ($dataset[194]==0) {
                                    $dataset[194]=$record['start_balance'];
                                 }
                                 if ($dataset_end[194]==0) {
                                    $dataset_end[194]=$record['end_balance'];
                                 }
                               //   $dataset_data[$record['reference']][194]=$record['qty'];
                                 break;
                             case 195:
                                if ($dataset[195]==0) {
                                    $dataset[195]=$record['start_balance'];
                                 }
                                 if ($dataset_end[195]==0) {
                                    $dataset_end[195]=$record['end_balance'];
                                 }
                               //   $dataset_data[$record['reference']][195]=$record['qty'];
                                 break;
                             case 196:
                               if ($dataset[196]==0) {
                                    $dataset[196]=$record['start_balance'];
                                 }
                                if ($dataset_end[196]==0) {
                                    $dataset_end[196]=$record['end_balance'];
                                 }
                              //  $dataset_data[$record['reference']][196]=$record['qty'];
                                 break;
                             case 197:
                                 if ($dataset[197]==0) {
                                    $dataset[197]=$record['start_balance'];
                                 }
                                  if ($dataset_end[197]==0) {
                                    $dataset_end[197]=$record['end_balance'];
                                 }
                              //     $dataset_data[$record['reference']][197]=$record['qty'];
                                 break;
                             }
                @endphp
            @endforeach
            <div class="col-sm-2"><h6><b>{{$dataset[193]}}</b></h6></div>
            <div class="col-sm-2"><h6><b>{{$dataset[194]}}</b></h6></div>
            <div class="col-sm-2"><h6><b>{{$dataset[195]}}</b></h6></div>
            <div class="col-sm-2"><h6><b>{{$dataset[196]}}</b></h6></div>
            <div class="col-sm-2"><h6><b>{{$dataset[197]}}</b></h6></div>
        </div>
        <hr>
</div>
<p>

@php $sum_193=$sum_194=$sum_195=$sum_196=$sum_197= 0; @endphp
@foreach($records->groupBy('reference','desc_id')->toArray() as $key => $record)
    @php
        $sd = 1;
        $up = 1;
        $rc = 1;
    @endphp
    @foreach($record as $ky => $rcd)
        @php
            //  dd($rcd['crate_id']);

               switch ($rcd['crate_id']){
                  case 193:
                      $dataset_data[$rcd['reference']][193]=$rcd['qty'];
                  break;
                  case 194:

                      $dataset_data[$rcd['reference']][194]=$rcd['qty'];
                  break;
                  case 195:

                      $dataset_data[$rcd['reference']][195]=$rcd['qty'];
                  break;
                  case 196:

                     $dataset_data[$rcd['reference']][196]=$rcd['qty'];
                  break;
                  case 197:
                      $dataset_data[$rcd['reference']][197]=$rcd['qty'];
                  break;
              }
        @endphp

        @if($rcd['desc_id'] == 1 && $sd == 1)
            <div class="row mt-6">
                @php
                    $invno = \App\LoadingHeader::where('id','=',$key)->first();

                @endphp
                @if(!empty($invno))
                    <div class="col-sm-2">SEND-{{$invno->invoice_number}}</div>
                @else
                    <div class="col-sm-2">SEND-{{$key}}</div>
                @endif
                @php
                    $sendbcl = !empty($dataset_data[$key][193])? $dataset_data[$key][193] : 0;
                    $sendbcs = !empty($dataset_data[$key][194])? $dataset_data[$key][194] : 0;
                    $sendcrl = !empty($dataset_data[$key][195])? $dataset_data[$key][195] : 0;
                    $sendcrs = !empty($dataset_data[$key][196])? $dataset_data[$key][196] : 0;
                    $sendlid = !empty($dataset_data[$key][197])? $dataset_data[$key][197] : 0;

                @endphp

                <div class="col-sm-2">{{$sendbcl}}</div>
                <div class="col-sm-2">{{$sendbcs}}</div>
                <div class="col-sm-2">{{$sendcrl}}</div>
                <div class="col-sm-2">{{$sendcrs}}</div>
                <div class="col-sm-2">{{$sendlid}}</div>
            </div>
            <hr>
            @php
                $sum_193 += $sendbcl;
                $sum_194 += $sendbcs;
                $sum_195 += $sendcrl;
                $sum_196 += $sendcrs;
                $sum_197 += $sendlid;
                $sd++;
            @endphp
        @elseif($rcd['desc_id'] == 3 && $up == 1)
            <div class="row mt-6">
                @php
                    $invno = \App\LoadingHeader::where('id','=',$key)->first();
                @endphp
                @if(!empty($invno))
                    <div class="col-sm-2">UPD-{{$invno->invoice_number}}</div>
                @else
                    <div class="col-sm-2">UPD-{{$key}}</div>
                @endif
                @php
                    $updbcl = !empty($dataset_data[$key][193])? $dataset_data[$key][193] : 0;
                    $updbcs = !empty($dataset_data[$key][194])? $dataset_data[$key][194] : 0;
                    $updcrl = !empty($dataset_data[$key][195])? $dataset_data[$key][195] : 0;
                    $updcrs = !empty($dataset_data[$key][196])? $dataset_data[$key][196] : 0;
                    $updlid = !empty($dataset_data[$key][197])? $dataset_data[$key][197] : 0;

                @endphp
                <div class="col-sm-2">{{$updbcl}}</div>
                <div class="col-sm-2">{{$updbcs}}</div>
                <div class="col-sm-2">{{$updcrl}}</div>
                <div class="col-sm-2">{{$updcrs}}</div>
                <div class="col-sm-2">{{$updlid}}</div>
            </div>
            <hr>
            @php
                $sum_193 += $updbcl;
                $sum_194 += $updbcs;
                $sum_195 += $updcrl;
                $sum_196 += $updcrs;
                $sum_197 += $updlid;
                $up++;
            @endphp
        @elseif($rcd['desc_id'] == 2 && $up == 1)
            <div class="row mt-6">
                @php
                    $invno = \App\LoadingHeader::where('id','=',$key)->first();
                @endphp
                @if(!empty($invno))
                    <div class="col-sm-2">RCV-{{$invno->invoice_number}}</div>
                @else
                    <div class="col-sm-2">RCV-{{$key}}</div>
                @endif
                @php

                    $rcbcl = !empty($dataset_data[$key][193])? $dataset_data[$key][193] : 0;
                    $rcbcs = !empty($dataset_data[$key][194])? $dataset_data[$key][194] : 0;
                    $rccrl = !empty($dataset_data[$key][195])? $dataset_data[$key][195] : 0;
                    $rccrs = !empty($dataset_data[$key][196])? $dataset_data[$key][196] : 0;
                    $rclid = !empty($dataset_data[$key][197])? $dataset_data[$key][197] : 0;

                @endphp

                <div class="col-sm-2">{{$rcbcl}}</div>
                <div class="col-sm-2">{{$rcbcs}}</div>
                <div class="col-sm-2">{{$rccrl}}</div>
                <div class="col-sm-2">{{$rccrs}}</div>
                <div class="col-sm-2">{{$rclid}}</div>
            </div>
            <hr>
            @php
                $sum_193 += $rcbcl;
                $sum_194 += $rcbcs;
                $sum_195 += $rccrl;
                $sum_196 += $rccrs;
                $sum_197 += $rclid;
                $up++;
            @endphp
        @elseif($rcd['desc_id'] == 21 && $sd == 1)
            <div class="row mt-6">

                <div class="col-sm-2">Admin Adjustment</div>
                @php
                    $adjbcl = !empty($dataset_data[$key][193])? $dataset_data[$key][193] : 0;
                    $adjbcs = !empty($dataset_data[$key][194])? $dataset_data[$key][194] : 0;
                    $adjcrl = !empty($dataset_data[$key][195])? $dataset_data[$key][195] : 0;
                    $adjcrs = !empty($dataset_data[$key][196])? $dataset_data[$key][196] : 0;
                    $adjlid = !empty($dataset_data[$key][197])? $dataset_data[$key][197] : 0;

                @endphp

                <div class="col-sm-2">{{$adjbcl}}</div>
                <div class="col-sm-2">{{$adjbcs}}</div>
                <div class="col-sm-2">{{$adjcrl}}</div>
                <div class="col-sm-2">{{$adjcrs}}</div>
                <div class="col-sm-2">{{$adjlid}}</div>
            </div>
            <hr>
            @php
                $sum_193 += $adjbcl;
                $sum_194 += $adjbcs;
                $sum_195 += $adjcrl;
                $sum_196 += $adjcrs;
                $sum_197 += $adjlid;
                $sd++;
            @endphp
        @endif


    @endforeach
@endforeach

@php $count++ @endphp
@if ($count == $total)
    <div class="row">
        <div class="col-sm-2"><h6 class="-bold"><b>Final Balance<u></u></b></h6></div>
        <div class="col-sm-2"><h6><b>{{$dataset[193]+$sum_193}}</b></h6></div>
        <div class="col-sm-2"><h6><b>{{$dataset[194]+$sum_194}}</b></h6></div>
        <div class="col-sm-2"><h6><b>{{$dataset[195]+$sum_195}}</b></h6></div>
        <div class="col-sm-2"><h6><b>{{$dataset[196]+$sum_196}}</b></h6></div>
        <div class="col-sm-2"><h6><b>{{$dataset[197]+$sum_197}}</b></h6></div>
    </div>
@endif



@endforeach

<div class="task-name-wrap">
    <div class="row mt-6">
        <div class="col-sm-2"><b>Start Bal.</b></div>
        <div class="col-sm-2"><b>BC [L]</b></div>
        <div class="col-sm-2"><b>BC [S]</b></div>
        <div class="col-sm-2"><b>CR [L]</b></div>
        <div class="col-sm-2"><b>CR [S]</b></div>
        <div class="col-sm-2"><b>LID</b></div>
        {{--         <div class="col-md-6"><b>{{number_format($record->first()->start_balance+$agent_init_value,2)}}</b></div> --}}
    </div>
</div>

<hr style="height:1px;background-color: #00a5bb">
<footer>
    <div class="row" style="margin-top: 20px;">
        <h4 align="center">
            <!--h6 class="text-muted">Powered by Cyntrek Solutions<br>
                <h7 class="num">+9471 122 5489 / +9471 532 2262</h7>
            </h6-->
        </h4>
    </div>

    <p style="margin-top: 150px">--------------------------------------------------------------</p>
</footer>

</body>

</html>

