@extends('layouts.back.master')@section('title','Pending Loading List')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }
    </style>
@stop

@section('content')
    {{ Form::open(array('url' => '/esr/excel/data','id'=>'filter_form'))}}
    <div class="row">
        {{--        <div class="col-sm-2">--}}
        {{--            <div class="form-group">--}}
        {{--                {!! Form::text('date',null,['class' => 'form-control','id'=>'date_range','placeholder'=>'Select Date','id'=>'date_range']) !!}--}}
        {{--            </div>--}}
        {{--        </div>--}}

        {{--        <div class="col-md-2">--}}
        {{--            {!! Form::select('agent',$agents , null , ['class' => 'form-control','required','placeholder'=>'Select Agent','id'=>'agent']) !!}--}}
        {{--        </div>--}}

        {{--        <div class="col-md-2">--}}
        {{--            {!! Form::select('category',$categories , null , ['class' => 'form-control','required','placeholder'=>'Select Category','id'=>'category']) !!}--}}
        {{--        </div>--}}

        {{--        <div class="col-md-2">--}}
        {{--            {!! Form::select('day', [1=>'Monday',2=>'Tuesday',3=>'Wednesday',4=>'Thursday',5=>'Friday',6=>'Saturday',7=>'Sunday',8=>'Long Weekend',9=>'Extra'] , null , ['class' => 'form-control','required','placeholder'=>'Select Day','id'=>'day']) !!}--}}
        {{--        </div>--}}
        {{--        <div class="col-md-2">--}}
        {{--            {!! Form::select('time', [1=>'Morning',2=>'Noon',3=>'Evening'] , null , ['class' => 'form-control','required','placeholder'=>'Select Time','id'=>'time']) !!}--}}
        {{--        </div>--}}

        {{--        <div class="col-sm-2">--}}
        {{--            <div class="form-group">--}}
        {{--                <button type="button" class="btn btn-success" onclick="process_form()">Filter Now</button>--}}
        {{--            </div>--}}
        {{--        </div>--}}

    </div>
    {{ Form::close() }}


    <div class="">
        <table id="esr_table" class="display text-center">
            <thead>
            <tr>
                <th>Name</th>
                <th>Requested QTY</th>
                <th>Current Stock</th>
                <th>QTY</th>
                <th>Action</th>
                <th>Reason</th>
            </tr>
            </thead>
            <tbody>
            @foreach($loading->rawItems as $item)
                <tr>
                    <td>{{\RawMaterialManager\Models\RawMaterial::withoutGlobalScopes(['type'])->where('raw_material_id','=',$item->raw_material_id)->first()->name}}</td>
                    <td>{{$item->qty}}</td>
                    <td>{{\RawMaterialManager\Models\RawMaterial::withoutGlobalScopes(['type'])->where('raw_material_id','=',$item->raw_material_id)->first()->available_qty}}</td>
                    <td><input type="number" step="0.001" {{($item->status ==0)? '' : 'disabled'}} name="qty" id="qty_{{$item->id}}" value="{{($item->status ==0)?$item->qty:$item->admin_approved_qty}}" class="form-control"></td>
                    <td>
                        <button class="btn ml-5 {{($item->status ==0)? 'btn-success' : 'btn-danger'}}"
                                id="raw_load_{{$item->id}}"
                                onclick="loadRaw({{$item->id}})">{{($item->status ==0)? 'Load' : 'Un Load'}}
                        </button>
                    </td>
                    <td></td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th>Name</th>
                <th>Requested QTY</th>
                <th>QTY</th>
                <th>Current Stock</th>
                <th>Action</th>
                <th>Reason</th>
            </tr>
            </tfoot>
        </table>
    </div>

@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script>
        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": false,
            "singleDatePicker": false,
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019"
        });

        $('#date_range').val('')

        $(document).ready(function () {
            table = $('#esr_table').DataTable({
                searching: true,
                pageLength: 25,
                responsive: true
            });
        });

        function process_form() {
            let date_range = $("#date_range").val();
            let agent = $("#agent").val();
            let category = $("#category").val();
            let day = $("#day").val();
            let time = $("#time").val();
            let table = $('#esr_table').DataTable();
            table.ajax.url('/shop-item-issue/table/data?agent=' + agent + '&category=' + category + '&day=' + day + '&time=' + time + '&date_range=' + date_range + '&filter=' + true).load();
            // table.ajax.url('/loading/table/data?date_range=' + date_range + '&filter=' + true).load();

        }

        function process_form_reset() {
            $("#date_range").val('');
            // $("#status").val('');
            // $("#semi").val('');
            let table = $('#esr_table').DataTable();
            table.ajax.url('/esr/table/data').load();

        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function loadRaw(rawId) {
            let element = '#raw_load_' + rawId;
            let qty_element = '#qty_' + rawId;
            let qty = $(qty_element).val();
            let url = '/shop-item-issue/' + rawId + '/load';
            $.ajax({
                type: "POST",
                url: url,
                data:{
                    qty:qty
                },
                success: function (response) {
                    if (response == 0) {
                        swal("Sorry!", "Not Enough Stock", "error");
                    } else if (response.status == 0) {
                        $(element).removeClass('btn-danger');
                        $(element).addClass('btn-success');
                        $(element).empty();
                        $(element).append('Load');
                        $(qty_element).removeClass('disabled');
                        $(qty_element).removeAttr('disabled');

                    } else {
                        $(element).removeClass('btn-success');
                        $(element).addClass('btn-danger');
                        $(element).empty();
                        $(element).append('Un Load');
                        $(qty_element).addClass('disabled');
                        $(qty_element).attr('disabled',true);
                    }
                }
            });
        }

    </script>

@stop