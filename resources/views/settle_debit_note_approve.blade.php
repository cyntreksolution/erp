@extends('layouts.back.master')@section('title','Settle Debit Note Approve')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .hide {
            display: none;
        }
    </style>
@stop
@section('content')
    <div class="app-wrapper">


        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">Debit Note Number : {{$return->debit_note_number}} [ {{$ur->name_with_initials}} ]</h5>
            </div>


                {{ Form::open(array('url' => 'admin/settle_notes/approve/','enctype'=>'multipart/form-data','id'=>'jq-validation-example-1'))}}
                <input type="hidden" value="{{$return->id}}" name="return_id">
            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">
                    <div class="container">
                        <div class="projects-list">
                            <div class="col-md-12">
                                <div class="" style="">
                                    <div class="card-body">
                                        <div class="row burden mt-2">
                                            <div class="col-md-6 font-weight-bold">
                                                Total
                                            </div>
                                            <div class="col-md-6 font-weight-bold">
                                                <input type="number" class="form-control" value="{{number_format($return->total, 2, '.', '')}}" step="0.01" readonly id="totalValue" name="totalValue">
                                            </div>
                                        </div>
                                        @if(Sentinel::check()->roles[0]->slug == 'stores-end-product')
                                            <div class="row burden mt-2">
                                                <div class="col-md-6 font-weight-bold">
                                                    Return Discount Amount
                                                </div>
                                                <div class="col-md-6 font-weight-bold">
                                                    <input type="number" class="form-control" value="{{number_format($return->return_discount, 2, '.', '')}}" step="0.01" id="returnDis" readonly name="returnDis" onkeyup="finalAmountCal()">
                                                </div>
                                            </div>
                                                <div class="row burden mt-2">
                                                    <div class="col-md-6 font-weight-bold">
                                                        Hold Amount
                                                    </div>
                                                    <div class="col-md-6 font-weight-bold">
                                                        <input type="number" class="form-control" value="0" step="0.01" id="holdAmount" readonly name="holdAmount" onkeyup="finalAmountCal()">
                                                    </div>
                                                </div>
                                        @else
                                            <div class="row burden mt-2">
                                                <div class="col-md-6 font-weight-bold">
                                                    Return Discount Amount
                                                </div>
                                                <div class="col-md-4 font-weight-bold">
                                                    <input type="number" class="form-control" value="{{number_format($return->return_discount, 2, '.', '')}}" readonly step="0.01" id="returnDis" name="returnDis" onkeyup="finalAmountCal()">
                                                </div>

                                                <div class="col-md-2 font-weight-bold">
                                                    <a href='{{ route('return.discount', $return->id) }}' class='ml-2 btn btn-sm btn-success'>Add Return Discount</a>
                                                </div>

                                            </div>
                                            <div class="row burden mt-2">
                                                <div class="col-md-6 font-weight-bold">
                                                    Hold Amount
                                                </div>
                                                <div class="col-md-6 font-weight-bold">
                                                    <input type="number" class="form-control" value="0" step="0.01" id="holdAmount" name="holdAmount" onkeyup="finalAmountCal()">
                                                </div>
                                            </div>
                                            @endif


                                                <div class="row burden mt-2">
                                                    <div class="col-md-6 font-weight-bold">
                                                        Final Amount
                                                    </div>
                                                    <div class="col-md-6 font-weight-bold">
                                                        <input type="number" class="form-control" step="0.01" value="{{number_format((($return->total)-($return->return_discount)), 2, '.', '')}}" readonly id="finalAmount" name="finalAmount">
                                                    </div>
                                                </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                              <div class="col-lg-6">
                                  <a href="{{route('admin.settle_notes')}}"
                                     class="btn mt-3 btn-success btn-lg btn-block"> Back</a>
                              </div>
                              <div class="col-lg-6">
                                  <button type="submit"
                                     class="btn mt-3 btn-primary btn-lg btn-block"> Approve</Button>
                              </div>

                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}

            </div>
        </div>


    </div>

@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        function finalAmountCal()
        {
          let totalValue = 0;
          let returnDis = 0;
          let holdAmount = 0;
              totalValue = parseFloat($("#totalValue").val());
              returnDis = parseFloat($("#returnDis").val());
              holdAmount = parseFloat($("#holdAmount").val());

              let finalAmount = totalValue - (returnDis + holdAmount);
              $("#finalAmount").val(Number(finalAmount).toFixed(3));
        }
    </script>
@stop
