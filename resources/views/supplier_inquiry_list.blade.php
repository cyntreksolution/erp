@extends('layouts.back.master')@section('title','Supplier Inquiry List')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }
    </style>
@stop

@section('content')

        <div class="row mb-2">

            <div class="col-md-2">
                {!! Form::select('supplier',$suppliers , null , ['class' => 'form-control','placeholder'=>'Select Supplier','id'=>'supplier']) !!}
            </div>
            <div class="col-md-3">
                {!! Form::select('reference_type',['PurchasingOrderManager\Model\PurchasingOrder'=>'Purchasing Order','PassBookManager\Models\Cheque'=>'Cheque Payment','CashLockerManager\Models\CashBookTransaction'=>'Cash Payment'], null , ['class' => 'form-control','placeholder'=>'Select Type','id'=>'reference_type']) !!}
            </div>
            <div class="col-md-3">
                <input type="text" name="date" id="date_range" class="form-control" placeholder="Select Date" required>
            </div>

            <div class="col-md-2">
                <button class="btn btn-info " onclick="process_form(this)">Filter</button>
            </div>

            <!-- loading view -->


        </div>

    <div class="table-responsive">
        <table id="supplier_table" class="display text-center ">
            <thead>
            <tr>
                <th>ID</th>
                <th>Supplier</th>
                <th>Start Balance</th>
                <th>Amount</th>
                <th>End Balance</th>
                <th>Description</th>
                <th>Reference</th>
                <th>Remark</th>
                <th>Action</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>ID</th>
                <th>Supplier</th>
                <th>Start Balance</th>
                <th>Amount</th>
                <th>End Balance</th>
                <th>Description</th>
                <th>Reference</th>
                <th>Remark</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
    </div>

@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script>
        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019",
            "maxDate":moment()
        });


        $('#date_range').val('')
        $(document).ready(function () {
            table = $('#supplier_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "ajax": {
                    url: "{{url('supplier-inquiry/list/table/data')}}",
                    type: "get",
                    error: function () {
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    }
                },
                pageLength: 100,
                responsive: true
            });
        });

        function process_form(e) {
            let supplier = $("#supplier").val();
            let table = $('#supplier_table').DataTable();
            let reference_type = $("#reference_type").val();
            let date_range = $("#date_range").val();

            table.ajax.url('/supplier-inquiry/list/table/data?supplier=' + supplier + '&reference_type=' + reference_type  + '&date_range=' + date_range  +'&filter=' + true).load();
        }


        function process_form_reset() {
            $("#supplier").val('');

            let table = $('#supplier_table').DataTable();
            table.ajax.url('supplier-inquiry/list/table/data').load();
        }

    </script>

@stop
