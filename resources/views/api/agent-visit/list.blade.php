@extends('layouts.back.master')@section('title','Agent Visit Shops')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">


    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/vendor/select2/css/select2.css')}}"/>

    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }

        .abc {
            width: 1000px;
            height: 1000px;
            /* background-color: black; */
            z-index: 8000;

        }

        .sweet-alert {
            z-index: 7999 !important;
        }

        .sweet-overlay {
            z-index: 7998 !important;
        }

    </style>
@stop

@section('content')

    <div id="wait" class="d-none abc" style=""><img src='{{asset('assets/img/4V0b.gif')}}' width="64" height="64"/><br>Loading..
    </div>


    <div class="">
        <button data-toggle="modal" data-target="#projects-task-modal" class="btn btn-success float-right mb-2"><i
                    class="fa fa-truck"></i> New Sales Route
        </button>
        <table id="table" class="display text-center">
            <thead>
            <tr>
                <th>Route Name</th>
                <th>Agent Name</th>
                <th>Shop Name</th>
                <th>Status</th>
                <th>Time</th>
            </tr>
            </thead>
            <tbody>

            @foreach($routes as $route)
                <tr>
                    <td>{{$route->agent_name}}</td>
                    <td>{{$route->shop_name}}</td>
                    <td>{{$route->visit_date_interval}}</td>
                    <td>{{$route->status}}</td>
                    <td>{{$route->time}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>



@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendor/select2/js/select2.js')}}"></script>


    <script>

        $('#table').DataTable({
            "autoWidth": false,
            "scrollX": true
        });
    </script>

@stop
