@extends('layouts.back.master')@section('title','Sales Routes')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">


    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/vendor/select2/css/select2.css')}}"/>

    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }

        .abc {
            width: 1000px;
            height: 1000px;
            /* background-color: black; */
            z-index: 8000;

        }

        .sweet-alert {
            z-index: 7999 !important;
        }

        .sweet-overlay {
            z-index: 7998 !important;
        }

    </style>
@stop

@section('content')

    <div id="wait" class="d-none abc" style=""><img src='{{asset('assets/img/4V0b.gif')}}' width="64" height="64"/><br>Loading..
    </div>


    <div class="">
        <button data-toggle="modal" data-target="#projects-task-modal" class="btn btn-success float-right mb-2"><i
                    class="fa fa-truck"></i> New Sales Route
        </button>
        <table id="table" class="display text-center">
            <thead>
            <tr>
                <th>Name</th>
                <th>Agent Name</th>
                <th>Description</th>
                <th>Visit Date Interval</th>
                <th>Status</th>
                <th>Shop Count</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>

            @foreach($routes as $route)
{{--                @php--}}
{{--                    $agent =!empty($route->agent) && sizeof($route->agent)>0?$route->agent[0]:null;--}}
{{--                @endphp--}}
                <tr>
                    <td>{{$route->name}}</td>
                    <td>{{optional($route->agent)->name_with_initials}}</td>
                    <td>{{$route->description}}</td>
                    <td>{{$route->visit_date_interval}}</td>
                    <td><i class="fa fa-check-circle {{$route->is_active? 'text-success':'text-danger'}}"></i></td>
                    <td>{{$route->shops_count}}</td>
                    <td>
                        <a href="{{route('sales-routes.shops',$route->id)}}" class="mr-2 ml-2 text-warning"><i
                                    class="fa fa-building "></i> </a>
                        <a onclick="editSalesRoute({{$route->id}},'{{$route->name}}','{{$route->description}}',{{$route->visit_date_interval}},{{$route->is_active}},{{optional($route->agent)->id}})"
                           class="mr-2 ml-2 text-info"><i class="fa fa-pencil "></i> </a>
                        @if ($route->shops_count == 0)
                            <a class="mr-2 ml-2 text-danger" onclick="deleteConfirm({{$route->id}})"><i
                                        class="fa fa-trash"></i> </a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>



    <div class="modal fade" id="projects-task-modal" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <form id="createForm">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="modal-body">
                        <label>Agent</label>
                        <div>
                            {!! Form::select('agent_id',$agents,null,['class'=>'form-control w-100 select2','placeholder'=>'Select Agent']) !!}
                        </div>
                        <div class="task-name-wrap">
                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                            <input class=" task-name-field" type="text" required name="name"
                                   placeholder="Name">
                        </div>
                        <hr>
                        <div class="task-desc-wrap">
                            <textarea class="task-desc-field" name="description"
                                      placeholder="Description"></textarea>
                        </div>
                        <hr>
                        <div class="task-name-wrap">
                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                            <input class="task-name-field" type="number" required name="visit_date_interval"
                                   placeholder="Visit Date Interval">
                        </div>
                        <hr>
                        <div class="task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            {!! Form::select('is_active',[1=>'Active',0=>'Inactive'],1,['class'=>'task-name-field' ,'required']) !!}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-success btn-block" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <form id="updateForm">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="id" id="id">
                    <div class="modal-body">
                        <label>Agent</label>
                        <div>
                            {!! Form::select('agent_id',$agents,null,['class'=>'form-control w-100 select2','placeholder'=>'Select Agent','id'=>'agent_id']) !!}
                        </div>
                        <div class="task-name-wrap">
                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                            <input class=" task-name-field" type="text" required name="name" id="name"
                                   placeholder="Name">
                        </div>
                        <hr>
                        <div class="task-desc-wrap">
                            <textarea class="task-desc-field" name="description" id="description"
                                      placeholder="Description"></textarea>
                        </div>
                        <hr>
                        <div class="task-name-wrap">
                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                            <input class="task-name-field" type="number" required name="visit_date_interval"
                                   id="visit_date_interval" placeholder="Visit Date Interval">
                        </div>
                        <hr>
                        <div class="task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            {!! Form::select('is_active',[1=>'Active',0=>'Inactive'],1,['class'=>'task-name-field' ,'required','id'=>'is_active']) !!}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-success btn-block" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>

@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendor/select2/js/select2.js')}}"></script>


    <script>
        $(".select2").select2();
        $(".select2").select2({
            width: 'resolve' // need to override the changed default
        });

        $('#table').DataTable({
            "autoWidth": false,
            "scrollX": true
        });


        $("#createForm").submit(function (event) {
            event.preventDefault();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            var formData = new FormData($(this)[0]);

            $.ajax({
                url: "/sales-routes/create",
                type: "post",
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    if (response == 'true') {
                        $('#projects-task-modal').modal('hide');
                        $('.modal-backdrop').removeClass('show');
                        // $('.modal-backdrop').removeClass('hide');
                        swal("Good job!", "Thanks for your time", "success");
                    } else {
                        $('#projects-task-modal').modal('hide');
                        $('.modal-backdrop').removeClass('show');
                        // $('.modal-backdrop').removeClass('hide');
                        swal("Error!", "Something went wrong", "error");
                    }
                    setTimeout(location.reload.bind(location), 900);
                }
            });
        });

        $("#updateForm").submit(function (event) {
            event.preventDefault();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            var formData = new FormData($(this)[0]);

            let id = $("#id").val();
            $.ajax({
                url: "/sales-routes/" + id,
                type: "post",
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    if (response == 'true') {
                        $('#editModal').modal('hide');
                        $('.modal-backdrop').removeClass('show');
                        // $('.modal-backdrop').removeClass('hide');
                        swal("Good job!", "Thanks for your time", "success");
                    } else {
                        $('#editModal').modal('hide');
                        $('.modal-backdrop').removeClass('show');
                        // $('.modal-backdrop').removeClass('hide');
                        swal("Error!", "Something went wrong", "error");
                    }
                    setTimeout(location.reload.bind(location), 900);
                }
            });
        });

        function deleteConfirm(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    });
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': '{{csrf_token()}}',
                            'Content-Type': 'application/json',
                        },
                        type: "DELETE",
                        url: '/sales-routes/' + id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            } else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

        function editSalesRoute(id, name, description, visit_date_interval, is_active,agent_id) {
            $("#id").val(id);
            $("#name").val(name);
            $("#description").val(description);
            $("#visit_date_interval").val(visit_date_interval);
            $("#is_active").val(is_active);
            $("#agent_id").val(agent_id).trigger('change');
            $('#editModal').modal('show');
        }

    </script>

@stop
