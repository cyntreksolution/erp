@extends('layouts.back.master')@section('title','Sales Shops')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" href="{{asset('css/select.dataTables.min.css')}}">


    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/vendor/select2/css/select2.css')}}" />
{{--    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/datatables.net/css/buttons.dataTables.min.css')}}">--}}

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.11.3/b-2.0.1/b-colvis-2.0.1/b-html5-2.0.1/b-print-2.0.1/datatables.min.css"/>


    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }

        .abc {
            width: 1000px;
            height: 1000px;
            /* background-color: black; */
            z-index: 8000;

        }

        .sweet-alert {
            z-index: 7999 !important;
        }

        .sweet-overlay {
            z-index: 7998 !important;
        }

    </style>
@stop

@section('content')

    <div id="wait" class="d-none abc" style=""><img src='{{asset('assets/img/4V0b.gif')}}' width="64" height="64"/><br>Loading..
    </div>


    <div class="">
        <button data-toggle="modal" data-target="#projects-task-modal" class="btn btn-success float-right ml-2 mb-2"><i
                    class="fa fa-building"></i> New Sales Shop
        </button>
        <button data-toggle="modal" data-target="#importModal" class="btn btn-primary float-right ml-2 mb-2"><i
                    class="fa fa-file-excel-o"></i> Import Excel
        </button>
{{--        <button onclick="getSelectedItems()" class="btn btn-outline-primary float-right ml-2 mb-2"><i--}}
{{--                    class="fa fa-cloud-download"></i> Batch QR Download--}}
{{--        </button>--}}
        <table id="table" class="display text-center">
            <thead>
            <tr>
{{--                <th><input type="checkbox" class="selectAll  select-checkbox"></th>--}}
                <th>ID</th>
                <th>Shop Key</th>
                <th>Route</th>
                <th>Name</th>
{{--                <th>Description</th>--}}
                <th> Address</th>
                <th>Mobile 1</th>
                <th>Mobile 2</th>
                <th>Last Visit Date</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($shops as $shop)
                <tr>
{{--                    <td></td>--}}
                    <td>{{$shop->id}}</td>
                    <td>{{$shop->shop_key}}</td>
                    <td>{{implode(' , ',collect($shop->routes)->pluck('name')->toArray())}}</td>
                    <td>{{$shop->name}}</td>
{{--                    <td>{{$shop->description}}</td>--}}
                    <td>{{$shop->address}}</td>
                    <td>{{$shop->mobile_number_1}}</td>
                    <td>{{$shop->mobile_number_2}}</td>
                    <td>{{$shop->last_visit_date}}</td>
                    <td>{{$shop->status}}</td>
                    <td>
                        <a onclick="editShop({{$shop->id}},{{collect($shop->routes)->pluck('id')}},'{{$shop->name}}','{{$shop->description}}','{{$shop->address}}','{{$shop->mobile_number_1}}','{{$shop->mobile_number_2}}','{{$shop->lat}}','{{$shop->long}}')"
                           class="mr-2 ml-2 text-info"><i class="fa fa-pencil "></i>
                        </a>
                        <a class="mr-2 ml-2 text-danger" onclick="deleteConfirm({{$shop->id}})">
                            <i class="fa fa-trash"></i>
                        </a>
                        <a class="mr-2 ml-2 text-info" onclick="showQR({{$shop->id}},'{{$shop->name}}')">
                            <i class="fa fa-qrcode"></i>
                        </a>
                        <a target="_blank" class="mr-2 ml-2" href="http://124.43.177.204:8080/qr/QR_{{$shop->id}}.png"
                           download>
                            <i class="fa fa-download"></i>
                        </a>
                        <a class="mr-2 ml-2 text-success" href="{{route('shop-orders.index')}}?shop_id={{$shop->id}}">
                            <i class="fa fa-first-order"></i>
                        </a>
                        @if (isset($shop->portal_enabled) && $shop->portal_enabled ==0)
                        <a class="mr-2 ml-2 text-warning" onclick="activeConfirm({{$shop->id}})">
                            <i class="fa fa-user-circle"></i>
                        </a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>



    <div class="modal fade" id="projects-task-modal" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <form id="createForm">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="modal-body">
                        <label>Select Route/s</label>
                        <div >
                            {!! Form::select('shop_route_id[]',$routes,request()->id,['class'=>'form-control w-100 select2','required','multiple']) !!}
                        </div>
                        <hr>
                        <div class="task-name-wrap">
                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                            <input class=" task-name-field" type="text" required name="name"
                                   placeholder="Name">
                        </div>
                        <hr>
                        <div class="task-desc-wrap">
                            <textarea class="task-desc-field" name="description"
                                      placeholder="Description"></textarea>
                        </div>
                        <hr>
                        <div class="task-desc-wrap">
                            <textarea class="task-desc-field" name="address"
                                      placeholder="Address"></textarea>
                        </div>
                        <hr>
                        <div class="task-name-wrap">
                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                            <input class="task-name-field" type="text" maxlength="10" minlength="10" required
                                   name="mobile_number_1"
                                   placeholder="Mobile Number">
                        </div>
                        <hr>
                        <div class="task-name-wrap">
                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                            <input class="task-name-field" type="text" maxlength="10" minlength="10"
                                   name="mobile_number_2"
                                   placeholder="Home Number">
                        </div>
                        <hr>
                        <div class="task-name-wrap">
                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                            <input class="task-name-field" type="text" required name="lat"
                                   placeholder="Latitude">
                        </div>
                        <hr>
                        <div class="task-name-wrap">
                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                            <input class="task-name-field" type="text" required
                                   name="long"
                                   placeholder="Longtude">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-success btn-block" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <form id="updateForm">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="id" id="id">
                    <div class="modal-body">
                        <label>Select Route/s</label>
                        <div >
                            {!! Form::select('shop_route_id[]',$routes,null,['class'=>'form-control w-100 select2','required','multiple','id'=>'shop_route_id']) !!}
                        </div>
                        <hr>
                        <div class="task-name-wrap">
                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                            <input class=" task-name-field" type="text" required name="update_file"
                                   placeholder="Name" id="name">
                        </div>
                        <hr>
                        <div class="task-desc-wrap">
                            <textarea class="task-desc-field" name="description"
                                      placeholder="Description" id="description"></textarea>
                        </div>
                        <hr>
                        <div class="task-desc-wrap">
                            <textarea class="task-desc-field" name="address"
                                      placeholder="Address" id="address"></textarea>
                        </div>
                        <hr>
                        <div class="task-name-wrap">
                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                            <input class="task-name-field" type="text" maxlength="10" minlength="10" required
                                   name="mobile_number_1"
                                   placeholder="Mobile Number" id="mobile_number_1">
                        </div>
                        <hr>
                        <div class="task-name-wrap">
                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                            <input class="task-name-field" type="text" maxlength="10" minlength="10"
                                   name="mobile_number_2"
                                   placeholder="Home Number" id="mobile_number_2">
                        </div>
                        <hr>
                        <div class="task-name-wrap">
                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                            <input class="task-name-field" type="text" required name="lat"
                                   placeholder="Latitude" id="lat">
                        </div>
                        <hr>
                        <div class="task-name-wrap">
                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                            <input class="task-name-field" type="text" required
                                   name="long"
                                   placeholder="Longtude" id="long">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-success btn-block" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <form id="bulkForm" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="modal-body">
                        <div class="task-name-wrap">
                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                            {!! Form::select('shop_route_id',$routes,null,['class'=>'task-name-field','required','placeholder'=>'Select Route']) !!}
                        </div>
                        <hr>
                        <div class="task-name-wrap">
                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                            <input class=" task-name-field" type="file" required name="update_file">
                        </div>
                        <hr>
                        <div class="task-name-wrap">
                            <a href="" download><i class="fa fa-download"></i>Download Sample Excel </a>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-success btn-block" value="Import">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="showQR" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="task-name-wrap">
                        <h1 class="title text-center" id="shopTitle"> SHop X</h1>
                    </div>
                    <div class="task-name-wrap" style="padding: 10px;border: 2px solid #501310;border-radius: 15px">
                        <img class="img-full" src="" id="qrImg">
                    </div>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-success btn-block" target="_blank" id="showDownloadButton" download="true"><i
                                class="fa fa-download"></i> Download </a>
                </div>

            </div>
        </div>
    </div>

@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
{{--    <script src="{{asset('js/dataTables.select.min.js')}}"></script>--}}
    <script src="{{asset('assets/vendor/select2/js/select2.js')}}"></script>
{{--    <script src="{{asset('assets/vendor/dtable/Buttons-1.5.1/js/dataTables.buttons.js')}}"></script>--}}


    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.11.3/b-2.0.1/b-colvis-2.0.1/b-html5-2.0.1/b-print-2.0.1/datatables.min.js"></script>

    <script>
        // $(".select2").select2();
        $(".select2").select2({
            width: 'resolve' // need to override the changed default
        });
        let dt = $('#table').DataTable({
            // select: {
            //     style:    'multi',
            //     selector: 'td:first-child'
            // },
            order: [[ 1, 'asc' ]],
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf'
            ]
        });

        // $(".selectAll").on("click", function (e) {
        //     if ($(this).is(":checked")) {
        //         dt.rows().select();
        //     } else {
        //         dt.rows().deselect();
        //     }
        // });

        function getSelectedItems() {
            let data = dt.rows({selected: true}).data();
            let id_list = []
            $.each(data, function (index, value) {
                id_list.push(value[1]);
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: "/sales-shop/bulk/qr/download",
                type: "post",
                data: {
                    "list": id_list,
                },
                success: function (response) {
                    {{--let link = "{{env('API_URL')}}/qr_zip/"+response;--}}
                    let link = "http://124.43.177.204:8080/"+response;
                    var a = $("<a target='_blank' />");
                    a.attr("download", response);
                    a.attr("href", link);
                    $("body").append(a);
                    a[0].click();
                    $("body").remove(a);
                }
            });
        }
        function getPDFSelectedItems() {
            let data = dt.rows({selected: true}).data();
            let id_list = []
            $.each(data, function (index, value) {
                id_list.push(value[1]);
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: "/sales-shop/bulk/qr/download/pdf",
                type: "post",
                data: {
                    "list": id_list,
                },
                success: function (response) {
                    {{--let link = "{{env('API_URL')}}/qr_zip/"+response;--}}
                    let link = "http://124.43.177.204:8080/"+response;
                    var a = $("<a target='_blank' />");
                    a.attr("download", response);
                    a.attr("href", link);
                    $("body").append(a);
                    a[0].click();
                    $("body").remove(a);
                }
            });

        }

        $("#bulkForm").submit(function (event) {
            event.preventDefault();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            var formData = new FormData(this);

            $.ajax({
                url: "/sales-shops/bulk/update",
                type: "post",
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    if (response == 'true') {
                        $('#projects-task-modal').modal('hide');
                        $('.modal-backdrop').removeClass('show');
                        // $('.modal-backdrop').removeClass('hide');
                        swal("Good job!", "Import Success");
                    } else {
                        $('#projects-task-modal').modal('hide');
                        $('.modal-backdrop').removeClass('show');
                        // $('.modal-backdrop').removeClass('hide');
                        swal("Error!", "Something went wrong", "error");
                    }
                    setTimeout(location.reload.bind(location), 900);
                }
            });
        });

        $("#createForm").submit(function (event) {
            event.preventDefault();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            var formData = new FormData($(this)[0]);

            $.ajax({
                url: "/sales-shops/create",
                type: "post",
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    if (response == 'true') {
                        $('#projects-task-modal').modal('hide');
                        $('.modal-backdrop').removeClass('show');
                        // $('.modal-backdrop').removeClass('hide');
                        swal("Good job!", "Thanks for your time", "success");
                    } else {
                        $('#projects-task-modal').modal('hide');
                        $('.modal-backdrop').removeClass('show');
                        // $('.modal-backdrop').removeClass('hide');
                        swal("Error!", "Something went wrong", "error");
                    }
                    setTimeout(location.reload.bind(location), 900);
                }
            });
        });

        $("#updateForm").submit(function (event) {
            event.preventDefault();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            var formData = new FormData($(this)[0]);

            let id = $("#id").val();
            $.ajax({
                url: "/sales-shops/" + id,
                type: "post",
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    if (response == 'true') {
                        $('#editModal').modal('hide');
                        $('.modal-backdrop').removeClass('show');
                        // $('.modal-backdrop').removeClass('hide');
                        swal("Good job!", "Thanks for your time", "success");
                    } else {
                        $('#editModal').modal('hide');
                        $('.modal-backdrop').removeClass('show');
                        // $('.modal-backdrop').removeClass('hide');
                        swal("Error!", "Something went wrong", "error");
                    }
                    setTimeout(location.reload.bind(location), 900);
                }
            });
        });

        function showQR(id, name) {
            let file = "http://124.43.177.204:8080/qr/" + 'QR_' + id + '.png';

            $("#qrImg").prop("src", file);
            $("#shopTitle").empty();
            $("#shopTitle").append(name);

            $("#showDownloadButton").prop("href", file);


            $('#showQR').modal('toggle');
        }

        function deleteConfirm(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    });
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': '{{csrf_token()}}',
                            'Content-Type': 'application/json',
                        },
                        type: "DELETE",
                        url: '/sales-shops/' + id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            } else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

        function editShop(id, shop_route_id, name, description, address, mobile_number_1, mobile_number_2, lat, long) {
            $("#id").val(id);
            $("#name").val(name);
            $("#shop_route_id").val(shop_route_id).trigger('change');
            $("#description").val(description);
            $("#address").val(address);
            $("#mobile_number_1").val(mobile_number_1);
            $("#mobile_number_2").val(mobile_number_2);
            $("#lat").val(lat);
            $("#long").val(long);
            $('#editModal').modal('show');
        }

        function activeConfirm(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to Active this User!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, active it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    });
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': '{{csrf_token()}}',
                            'Content-Type': 'application/json',
                        },
                        type: "POST",
                        url: '/sales-shops-active/' + id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Activated!", "Activated successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            } else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }
    </script>

@stop
