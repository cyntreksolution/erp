@extends('layouts.back.master')@section('title','Visited Shops')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" href="{{asset('css/select.dataTables.min.css')}}">


    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/vendor/select2/css/select2.css')}}"/>
    {{--    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/datatables.net/css/buttons.dataTables.min.css')}}">--}}

    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.11.3/b-2.0.1/b-colvis-2.0.1/b-html5-2.0.1/b-print-2.0.1/datatables.min.css"/>


    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }

        .abc {
            width: 1000px;
            height: 1000px;
            /* background-color: black; */
            z-index: 8000;

        }

        .sweet-alert {
            z-index: 7999 !important;
        }

        .sweet-overlay {
            z-index: 7998 !important;
        }

        .c-dashboardInfo {
            margin-bottom: 15px;
        }

        .c-dashboardInfo .wrap {
            background: #ffffff;
            box-shadow: 2px 10px 20px rgba(0, 0, 0, 0.1);
            border-radius: 7px;
            text-align: center;
            position: relative;
            overflow: hidden;
            padding: 40px 25px 20px;
            height: 100%;
        }

        .c-dashboardInfo__title,
        .c-dashboardInfo__subInfo {
            color: #6c6c6c;
            font-size: 1.18em;
        }

        .c-dashboardInfo span {
            display: block;
        }

        .c-dashboardInfo__count {
            font-weight: 600;
            font-size: 2.5em;
            line-height: 64px;
            color: #323c43;
        }

        .c-dashboardInfo .wrap:after {
            display: block;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 10px;
            content: "";
        }

        .c-dashboardInfo:nth-child(1) .wrap:after {
            background: linear-gradient(82.59deg, #00c48c 0%, #00a173 100%);
        }

        .c-dashboardInfo:nth-child(2) .wrap:after {
            background: linear-gradient(81.67deg, #0084f4 0%, #1a4da2 100%);
        }

        .c-dashboardInfo:nth-child(3) .wrap:after {
            background: linear-gradient(69.83deg, #0084f4 0%, #00c48c 100%);
        }

        .c-dashboardInfo:nth-child(4) .wrap:after {
            background: linear-gradient(81.67deg, #ff647c 0%, #1f5dc5 100%);
        }

        .c-dashboardInfo__title svg {
            color: #d7d7d7;
            margin-left: 5px;
        }

        .MuiSvgIcon-root-19 {
            fill: currentColor;
            width: 1em;
            height: 1em;
            display: inline-block;
            font-size: 24px;
            transition: fill 200ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
            user-select: none;
            flex-shrink: 0;
        }


    </style>
@stop

@section('content')

    <div id="wait" class="d-none abc" style=""><img src='{{asset('assets/img/4V0b.gif')}}' width="64" height="64"/><br>Loading..
    </div>


    <div class="">
        <form method="get">
            <div class="row mb-3 p-2">
                <div class="col-md-2">
                    <input value="{{$dateRange}}" type="text" name="date_range" id="date_range" class="form-control"
                           placeholder="Select Date Range">
                </div>



                @if((Sentinel::check()->roles[0]->slug != 'sales-ref'))
                    <div class="col-md-2">
                        {!! Form::select('route_id',$routes , $routeId , ['class' => 'form-control','placeholder'=>'Select Route','id'=>'route_id']) !!}
                    </div>

                    <div class="col-md-2">
                        {!! Form::select('agent_id',$agents , $agentId , ['class' => 'form-control','placeholder'=>'Select Agent','id'=>'agent_id']) !!}
                    </div>
                @else
                    <div class="p-3">
                        <input type="hidden" name="agent_id" id="agent_id" value="{{Sentinel::getUser()->id}}">

                    </div>
                @endif



                <div class="col-md-2">
                    <button class="btn btn-info ">Filter</button>
                </div>
            </div>
        </form>

        @if (!empty($analytics))
            <div class="row align-items-stretch">
                <div class="c-dashboardInfo col-lg-3 col-md-6">
                    <div class="wrap">
                        <h4 class="heading heading5 hind-font medium-font-weight c-dashboardInfo__title">Total Shops
                            <svg
                                    class="MuiSvgIcon-root-19" focusable="false" viewBox="0 0 24 24" aria-hidden="true"
                                    role="presentation">
                                <path fill="none" d="M0 0h24v24H0z"></path>
                                <path
                                        d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 15h-2v-6h2v6zm0-8h-2V7h2v2z">
                                </path>
                            </svg>
                        </h4>
                        <span class="hind-font caption-12 c-dashboardInfo__count">{{$analytics->total_shops}}</span>
                        <span
                                class="hind-font caption-12 c-dashboardInfo__subInfo">All shops count in route</span>
                    </div>
                </div>
                <div class="c-dashboardInfo col-lg-3 col-md-6">
                    <div class="wrap">
                        <h4 class="heading heading5 hind-font medium-font-weight c-dashboardInfo__title">Visited Shops
                            <svg
                                    class="MuiSvgIcon-root-19" focusable="false" viewBox="0 0 24 24" aria-hidden="true"
                                    role="presentation">
                                <path fill="none" d="M0 0h24v24H0z"></path>
                                <path
                                        d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 15h-2v-6h2v6zm0-8h-2V7h2v2z">
                                </path>
                            </svg>
                        </h4>
                        <span class="hind-font caption-12 c-dashboardInfo__count">{{$analytics->visited_shops}}</span><span
                                class="hind-font caption-12 c-dashboardInfo__subInfo">Visited Percentage: {{$analytics->visited_shops_percentage}}%</span>
                    </div>
                </div>
                <div class="c-dashboardInfo col-lg-3 col-md-6">
                    <div class="wrap">
                        <h4 class="heading heading5 hind-font medium-font-weight c-dashboardInfo__title">Remaining Shops
                            <svg
                                    class="MuiSvgIcon-root-19" focusable="false" viewBox="0 0 24 24" aria-hidden="true"
                                    role="presentation">
                                <path fill="none" d="M0 0h24v24H0z"></path>
                                <path
                                        d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 15h-2v-6h2v6zm0-8h-2V7h2v2z">
                                </path>
                            </svg>
                        </h4>
                        <span class="hind-font caption-12 c-dashboardInfo__count">{{$analytics->remaining_shops}}</span><span
                                class="hind-font caption-12 c-dashboardInfo__subInfo">Visited Percentage: {{$analytics->remaining_shops_percentage}}%</span>

                    </div>
                </div>
                <div class="c-dashboardInfo col-lg-3 col-md-6">
                    <div class="wrap">
                        <h4 class="heading heading5 hind-font medium-font-weight c-dashboardInfo__title">Productive Calls
                            <svg
                                    class="MuiSvgIcon-root-19" focusable="false" viewBox="0 0 24 24" aria-hidden="true"
                                    role="presentation">
                                <path fill="none" d="M0 0h24v24H0z"></path>
                                <path
                                        d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 15h-2v-6h2v6zm0-8h-2V7h2v2z">
                                </path>
                            </svg>
                        </h4>
                        <span class="hind-font caption-12 c-dashboardInfo__count">0</span><span
                                class="hind-font caption-12 c-dashboardInfo__subInfo">Invoice count</span>
                    </div>
                </div>
            </div>
        @endif

        <table id="table" class="display text-center">
            <thead>
            <tr>
                <th>#</th>
                <th>Shop Name</th>
                <th>Visited Status</th>
                <th>Last Visit Date</th>
                <th>Visited Count</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @if (!empty($visited_data))
                @foreach($visited_data->data as $key=> $item)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$item->shop_name}}</td>
                        <td>
                            <i class='{{!empty($item->visited_time)? 'text-success fa fa-check':'text-danger fa fa-close '}}'></i>
                        </td>
                        <td>{{!empty($item->visited_time)?$item->visited_time:'Not Visited'}}</td>
                        <td>{{!empty($item->visited_count)?$item->visited_count:'0'}}</td>
                        <td>
                            <a target="_blank" href="/sales-agent-visited-shops/filter?date_range={{$dateRange}}&route_id={{$routeId}}&agent_id={{$agentId}}&shop_id={{$item->shop_id}}" class="btn btn-outline-success"><i class="fa fa-building-o"></i></a>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>

    </div>


@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    {{--    <script src="{{asset('js/dataTables.select.min.js')}}"></script>--}}
    <script src="{{asset('assets/vendor/select2/js/select2.js')}}"></script>
    {{--    <script src="{{asset('assets/vendor/dtable/Buttons-1.5.1/js/dataTables.buttons.js')}}"></script>--}}
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript"
            src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.11.3/b-2.0.1/b-colvis-2.0.1/b-html5-2.0.1/b-print-2.0.1/datatables.min.js"></script>

    <script>
        $(".select2").select2({
            width: 'resolve' // need to override the changed default
        });

        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "2021-10-01",
            "maxDate": moment()
        });

        let dt = $('#table').DataTable({
            order: [[0, 'asc']],
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf'
            ]
        });

    </script>

@stop
