@extends('layouts.back.master')@section('title','Visited Shops')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" href="{{asset('css/select.dataTables.min.css')}}">


    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/vendor/select2/css/select2.css')}}"/>
    {{--    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/datatables.net/css/buttons.dataTables.min.css')}}">--}}

    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.11.3/b-2.0.1/b-colvis-2.0.1/b-html5-2.0.1/b-print-2.0.1/datatables.min.css"/>


    <style>
        #map {
            height: 500px;
            z-index: 9999999;
        }


        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }

        .abc {
            width: 1000px;
            height: 1000px;
            /* background-color: black; */
            z-index: 8000;

        }

        .sweet-alert {
            z-index: 7999 !important;
        }

        .sweet-overlay {
            z-index: 7998 !important;
        }

        .c-dashboardInfo {
            margin-bottom: 15px;
        }

        .c-dashboardInfo .wrap {
            background: #ffffff;
            box-shadow: 2px 10px 20px rgba(0, 0, 0, 0.1);
            border-radius: 7px;
            text-align: center;
            position: relative;
            overflow: hidden;
            padding: 40px 25px 20px;
            height: 100%;
        }

        .c-dashboardInfo__title,
        .c-dashboardInfo__subInfo {
            color: #6c6c6c;
            font-size: 1.18em;
        }

        .c-dashboardInfo span {
            display: block;
        }

        .c-dashboardInfo__count {
            font-weight: 600;
            font-size: 2.5em;
            line-height: 64px;
            color: #323c43;
        }

        .c-dashboardInfo .wrap:after {
            display: block;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 10px;
            content: "";
        }

        .c-dashboardInfo:nth-child(1) .wrap:after {
            background: linear-gradient(82.59deg, #00c48c 0%, #00a173 100%);
        }

        .c-dashboardInfo:nth-child(2) .wrap:after {
            background: linear-gradient(81.67deg, #0084f4 0%, #1a4da2 100%);
        }

        .c-dashboardInfo:nth-child(3) .wrap:after {
            background: linear-gradient(69.83deg, #0084f4 0%, #00c48c 100%);
        }

        .c-dashboardInfo:nth-child(4) .wrap:after {
            background: linear-gradient(81.67deg, #ff647c 0%, #1f5dc5 100%);
        }

        .c-dashboardInfo__title svg {
            color: #d7d7d7;
            margin-left: 5px;
        }

        .MuiSvgIcon-root-19 {
            fill: currentColor;
            width: 1em;
            height: 1em;
            display: inline-block;
            font-size: 24px;
            transition: fill 200ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
            user-select: none;
            flex-shrink: 0;
        }


    </style>
@stop

@section('content')

    <div id="wait" class="d-none abc" style=""><img src='{{asset('assets/img/4V0b.gif')}}' width="64" height="64"/><br>Loading..
    </div>


    <div class="">
        <form method="get">
            <div class="row mb-3 p-2">
                <div class="col-md-2">
                    <input value="{{$dateRange}}" type="text" name="date_range" id="date_range" class="form-control"
                           placeholder="Select Date Range">
                </div>

                <div class="col-md-2">
                    {!! Form::select('route_id',$routes , $routeId , ['class' => 'form-control','placeholder'=>'Select Route','id'=>'route_id']) !!}
                </div>
                <div class="col-md-2">
                    {!! Form::select('agent_id',$agents , $agentId , ['class' => 'form-control','placeholder'=>'Select Agent','id'=>'agent_id']) !!}
                </div>
                <div class="col-md-2">
                    {!! Form::select('shop_id',$shops , $shopId , ['class' => 'form-control','placeholder'=>'Select Shop','id'=>'shop_id']) !!}
                </div>


                <div class="col-md-2">
                    <button class="btn btn-info ">Filter</button>
                </div>
            </div>
        </form>

        @if (!empty($analytics))
            <div class="row align-items-stretch">
                <div class="c-dashboardInfo col-lg-3 col-md-6">
                    <div class="wrap">
                        <h4 class="heading heading5 hind-font medium-font-weight c-dashboardInfo__title">Total Visits
                            <svg
                                    class="MuiSvgIcon-root-19" focusable="false" viewBox="0 0 24 24" aria-hidden="true"
                                    role="presentation">
                                <path fill="none" d="M0 0h24v24H0z"></path>
                                <path
                                        d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 15h-2v-6h2v6zm0-8h-2V7h2v2z">
                                </path>
                            </svg>
                        </h4>
                        <span class="hind-font caption-12 c-dashboardInfo__count">0</span>
                        <span
                                class="hind-font caption-12 c-dashboardInfo__subInfo">visit count</span>
                    </div>
                </div>
                <div class="c-dashboardInfo col-lg-3 col-md-6">
                    <div class="wrap">
                        <h4 class="heading heading5 hind-font medium-font-weight c-dashboardInfo__title">Sales
                            <svg
                                    class="MuiSvgIcon-root-19" focusable="false" viewBox="0 0 24 24" aria-hidden="true"
                                    role="presentation">
                                <path fill="none" d="M0 0h24v24H0z"></path>
                                <path
                                        d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 15h-2v-6h2v6zm0-8h-2V7h2v2z">
                                </path>
                            </svg>
                        </h4>
                        <span class="hind-font caption-12 c-dashboardInfo__count">0</span><span
                                class="hind-font caption-12 c-dashboardInfo__subInfo">sales percentage: 0%</span>
                    </div>
                </div>
                <div class="c-dashboardInfo col-lg-3 col-md-6">
                    <div class="wrap">
                        <h4 class="heading heading5 hind-font medium-font-weight c-dashboardInfo__title">Sales Return
                            <svg
                                    class="MuiSvgIcon-root-19" focusable="false" viewBox="0 0 24 24" aria-hidden="true"
                                    role="presentation">
                                <path fill="none" d="M0 0h24v24H0z"></path>
                                <path
                                        d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 15h-2v-6h2v6zm0-8h-2V7h2v2z">
                                </path>
                            </svg>
                        </h4>
                        <span class="hind-font caption-12 c-dashboardInfo__count">0</span><span
                                class="hind-font caption-12 c-dashboardInfo__subInfo">sales return percentage: 0%</span>

                    </div>
                </div>
                <div class="c-dashboardInfo col-lg-3 col-md-6">
                    <div class="wrap">
                        <h4 class="heading heading5 hind-font medium-font-weight c-dashboardInfo__title">Invalid Locations
                            Calls
                            <svg
                                    class="MuiSvgIcon-root-19" focusable="false" viewBox="0 0 24 24" aria-hidden="true"
                                    role="presentation">
                                <path fill="none" d="M0 0h24v24H0z"></path>
                                <path
                                        d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 15h-2v-6h2v6zm0-8h-2V7h2v2z">
                                </path>
                            </svg>
                        </h4>
                        <span class="hind-font caption-12 c-dashboardInfo__count">0</span><span
                                class="hind-font caption-12 c-dashboardInfo__subInfo">invalid location count</span>
                    </div>
                </div>
            </div>
        @endif

        <table id="table" class="display text-center">
            <thead>
            <tr>
                <th>#</th>
                <th>Agent Name</th>
                <th>Route Name</th>
                <th>Shop Name</th>
                <th>Visited Date</th>
                <th>Visited Time</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @if (!empty($visited_data))
                @foreach($visited_data->data as $key=> $item)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{optional($item->agent)->name_with_initials}}</td>
                        <td>{{optional($item->route)->name}}</td>
                        <td>{{optional($item->shop)->name}}</td>
                        <td>{{\Carbon\Carbon::parse($item->visited_time)->format('Y-m-d H:i:s')}}</td>
                        <td>{{\Carbon\Carbon::parse($item->visited_time)->diffForHumans()}}</td>
                        <td>
                            <button class="btn btn-outline-info"
                                    onclick="showMap({{$item->id}},{{optional($item->shop)->lat}},{{optional($item->shop)->long}},{{$item->latitude}},{{$item->longitude}})">
                                <i class="fa fa-location-arrow"></i>
                            </button>

                            <a title="invoice" data-toggle="tooltips" class="ml-2 btn btn-outline-success"><i class="fa fa-dollar"></i></a>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>

    </div>

    <div class="modal fade" id="mapModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="map"></div>
                    <div class="row mt-2">
                        <div class="col-md-6">
                            <p class="mt-2" style="white-space: nowrap" id="distance_div"></p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    {{--    <script src="{{asset('js/dataTables.select.min.js')}}"></script>--}}
    <script src="{{asset('assets/vendor/select2/js/select2.js')}}"></script>
    {{--    <script src="{{asset('assets/vendor/dtable/Buttons-1.5.1/js/dataTables.buttons.js')}}"></script>--}}
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript"
            src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.11.3/b-2.0.1/b-colvis-2.0.1/b-html5-2.0.1/b-print-2.0.1/datatables.min.js"></script>
    <script
            src="https://maps.googleapis.com/maps/api/js?key={{env('MAP_API_KEY')}}&callback=initMap&v=weekly"
            async
    ></script>

    <script>
        $(".select2").select2({
            width: 'resolve' // need to override the changed default
        });

        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "2021-10-01",
            "maxDate": moment()
        });

        let dt = $('#table').DataTable({
            order: [[0, 'asc']],
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf'
            ]
        });

        function haversine_distance(mk1, mk2) {
            // var R = 3958.8; // Radius of the Earth in miles
            var R = 6371.0710; // Radius of the Earth in KM
            var rlat1 = mk1.position.lat() * (Math.PI / 180); // Convert degrees to radians
            var rlat2 = mk2.position.lat() * (Math.PI / 180); // Convert degrees to radians
            var difflat = rlat2 - rlat1; // Radian difference (latitudes)
            var difflon = (mk2.position.lng() - mk1.position.lng()) * (Math.PI / 180); // Radian difference (longitudes)

            var d = 2 * R * Math.asin(Math.sqrt(Math.sin(difflat / 2) * Math.sin(difflat / 2) + Math.cos(rlat1) * Math.cos(rlat2) * Math.sin(difflon / 2) * Math.sin(difflon / 2)));
            return d;
        }

        function initMap(shop, agent) {
            let distance_label = "#distance_div";
            const store = "{{asset('assets/img/store.png')}}";
            const man = "{{asset('assets/img/delivery-truck.png')}}";

            const map = new google.maps.Map(document.getElementById("map"), {
                zoom: 16,
                center: shop,
            });

            var marker = new google.maps.Marker({position: shop, map: map, icon: store});
            var marker2 = new google.maps.Marker({position: agent, map: map, icon: man});

            var cityCircle = new google.maps.Circle({
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#FF0000',
                fillOpacity: 0.35,
                map: map,
                center: shop,
                radius: 200
            });

            var line = new google.maps.Polyline({path: [shop, agent], map: map});
            var distance = haversine_distance(marker, marker2);

            $(distance_label).empty();
            if (distance > 0) {
                $(distance_label).append("<label class='label text-danger'>Distance : " + distance.toFixed(2) * 1000 + " M</label>");
            }

        }

        function showMap(id, shopLat, shopLang, agentLat, agentLang) {
            $('#exampleModalLongTitle').empty();
            $('#exampleModalLongTitle').append('#' + id);
            let shop = {lat: shopLat, lng: shopLang};
            let agent = {lat: agentLat, lng: agentLang};
            initMap(shop, agent)
            $('#mapModal').modal('show')
        }
    </script>

@stop
