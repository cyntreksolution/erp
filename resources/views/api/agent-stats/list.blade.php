@extends('layouts.back.master')@section('title','Agent Statistics')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">


    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/vendor/select2/css/select2.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">

@stop

@section('content')

    <div id="wait" class="d-none abc" style="">
        <img src='{{asset('assets/img/4V0b.gif')}}' width="64" height="64"/><br>Loading..
    </div>


    <div class="">
        {{ Form::open(array('route' => 'agent.stats','method'=>'GET'))}}

        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    {!! Form::text('date_range',$dateRange,['class' => 'form-control','id'=>'date','placeholder'=>'Select Date','autocomplete'=>'off']) !!}
                </div>
            </div>

            @if((Sentinel::check()->roles[0]->slug != 'sales-ref'))
                <div class="col-sm-3">
                    <div class="form-group">
                        {!! Form::select('agent_id',$agents,$agentIds,['class' => 'form-control','id'=>'agent_id','placeholder'=>'Select Agent','autocomplete'=>'off']) !!}
                    </div>
                </div>
            @else
                <div class="p-3">
                    <input type="hidden" name="agent_id" id="agent_id" value="{{Sentinel::getUser()->id}}">
                </div>
            @endif

            <div class="">
                <button type="submit" class="btn btn-success">Filter</button>
                <a href="{{route('agent.stats')}}" class="btn btn-info">Clear Filter</a>
            </div>

            {!! Form::close() !!}
        </div>

        <div class="row mt-3">
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-6"><h4>Not Visited Shops</h4></div>
                    <div class="col-md-6 text-danger"><h4>: {{$summery->not_visited_shops}}<a target="_blank" href="/agent/not-visited-shop-data?date_range={{$dateRange}}&agent_id={{$agentIds}}"> <i
                                        class="ml-2 fa fa-info-circle"></i></a></h4></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6 text-nowrap"><h4>Not Visited Percentage</h4></div>
                    <div class="col-md-6 text-danger"><h4>: {{number_format($summery->not_visited_percentage,2)}} %</h4></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6"><h4>Invalid Scans</h4></div>
                    <div class="col-md-6"><h4>: {{$summery->invalid_scan}}</h4></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6 text-nowrap"><h4>Invalid Scans Percentage</h4></div>
                    <div class="col-md-6"><h4>: {{number_format($summery->invalid_scan_percentage,2)}} %</h4></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-6"><h4>Visited Shops </h4></div>
                    <div class="col-md-6 text-success"><h4>: {{$summery->visited_shops}} <a target="_blank" href="/agent/visited-shop-data?date_range={{$dateRange}}&agent_id={{$agentIds}}"> <i
                                        class="ml-2 fa fa-info-circle"></i></a></h4></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6 text-nowrap"><h4>Visited Percentage</h4></div>
                    <div class="col-md-6 text-success"><h4>: {{number_format($summery->visited_percentage,2)}} %</h4></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6"><h4>Valid Scans</h4></div>
                    <div class="col-md-6"><h4>: {{$summery->valid_scan}}</h4></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6 text-nowrap"><h4>Valid Scans Percentage</h4></div>
                    <div class="col-md-6"><h4>: {{$summery->valid_scan_percentage}}</h4></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-6"><h4>Total Shops</h4></div>
                    <div class="col-md-6"><h4>: {{$summery->total_shops}}</h4></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6"><h4>Temporary Orders</h4></div>
                    <div class="col-md-6"><h4>: {{$summery->temporary_orders}}</h4></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6"><h4>Total Sales</h4></div>
                    <div class="col-md-6"><h4>: {{$summery->total_sales}}</h4></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6"><h4>Total QTY</h4></div>
                    <div class="col-md-6"><h4>: {{$summery->total_qty}}</h4></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6"><h4>Total Items</h4></div>
                    <div class="col-md-6"><h4>: {{$summery->total_items}}</h4></div>
                </div>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-md-1"></div>
            <div class="col-md-8">
                <div class="row ">
                    @for ($i = 1; $i < 5 ; $i++)
                        <div class="col-md-3 p-2">
                            <a target="_blank" href="/agent/visited-shop-count-data?date_range={{$dateRange}}&agent_id={{$agentIds}}&number={{$i}}">
                                <div style="border-radius: 15px;border: 1px solid black"
                                     class="{{($i<5?'bg-danger':'bg-success')}}">
                                    <div class="row">
                                        <div class="col-md-6 p-3">
                                            <h3 class="font-weight-bold m-1"> {{$i==9?'8+':$i}}
                                                <br>{{\Illuminate\Support\Str::plural('Day',$i)}} </h3>
                                        </div>
                                        <div class="col-md-6 p-4">
                                            <h1 class="font-weight-bold" id={{"visited_shop_count_$i"}}>00</h1>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endfor
                </div>

                <div class="row mt-3">
                    @for ($i = 5; $i < 9 ; $i++)
                        <div class="col-md-3 p-2">
                            <a target="_blank" href="/agent/visited-shop-count-data?date_range={{$dateRange}}&agent_id={{$agentIds}}&number={{$i}}">
                                <div style="border-radius: 15px;border: 1px solid black"
                                     class="{{($i<5?'bg-danger':'bg-success')}}">
                                    <div class="row">
                                        <div class="col-md-6 p-3">
                                            <h3 class="font-weight-bold m-1"> {{$i==9?'8+':$i}}
                                                <br>{{\Illuminate\Support\Str::plural('Day',$i)}} </h3>
                                        </div>
                                        <div class="col-md-6 p-4">
                                            <h1 class="font-weight-bold" id={{"visited_shop_count_$i"}}>00</h1>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endfor
                </div>
            </div>
            <div class="col-md-2 mt-2 p-2">
                <a target="_blank" href="/agent/visited-shop-count-data?date_range={{$dateRange}}&agent_id={{$agentIds}}&number=9">
                    <div style="border-radius: 15px;border: 1px solid black;height: 100%" class="bg-success">
                        <div class="row">
                            <div class="col-md-6 p-3">
                                <h3 class="font-weight-bold m-1"> 8+
                                    <br>Days </h3>
                            </div>
                            <div class="col-md-6 p-4">
                                <h1 class="font-weight-bold" id="visited_shop_count_9">00</h1>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-1"></div>
        </div>


    </div>



@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/select2/js/select2.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>


    <script>
        $('#date').daterangepicker({
            "showDropdowns": true,
            "timePicker": false,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "01/01/2022"
        });


        $(document).ready(function () {
            let agent_id = $("#agent_id").val() == '' ? 0 : $("#agent_id").val();
            let date_range = $("#date").val();
            if (agent_id != 0) {
                for (let i = 1; i < 10; i++) {
                    let shop_count = "#visited_shop_count_" + i;
                    $.ajax({
                        url: "/agent/shop/statistics/by-number",
                        data: {"agent_id": agent_id, 'number': i, date_range: date_range},
                        success: function (data) {
                            $(shop_count).empty();
                            $(shop_count).html(data.length);
                        }
                    });
                }
            }
        });
    </script>

@stop
