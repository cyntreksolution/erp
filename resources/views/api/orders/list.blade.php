@extends('layouts.back.master')@section('title','Orders')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" href="{{asset('css/select.dataTables.min.css')}}">


    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/vendor/select2/css/select2.css')}}" />
{{--    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/datatables.net/css/buttons.dataTables.min.css')}}">--}}

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.11.3/b-2.0.1/b-colvis-2.0.1/b-html5-2.0.1/b-print-2.0.1/datatables.min.css"/>


    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }

        .abc {
            width: 1000px;
            height: 1000px;
            /* background-color: black; */
            z-index: 8000;

        }

        .sweet-alert {
            z-index: 7999 !important;
        }

        .sweet-overlay {
            z-index: 7998 !important;
        }

    </style>
@stop

@section('content')

    <div id="wait" class="d-none abc" style=""><img src='{{asset('assets/img/4V0b.gif')}}' width="64" height="64"/><br>Loading..
    </div>


    <div class="">

        <table id="table" class="display text-center">
            <thead>
            <tr>
                <th>ID</th>
                <th>Shop</th>
                <th>Agent</th>
                <th>Invoice Number</th>
                <th>DateTime</th>
                <th>Value</th>
                <th>Item Count</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($orders as $order)
                <tr>
                    <td>{{$order->id}}</td>
                    <td>{{$order->shop->name}}</td>
                    <td>{{$order->agent->name_with_initials}}</td>
                    <td>{{$order->invoice_number}}</td>
                    <td>{{$order->ordered_time}}</td>
                    <td>{{number_format($order->net_amount,2)}}</td>
                    <td>{{sizeof($order->items)}}</td>
                    <td>
                        <a class="mr-2 ml-2 text-success" href="{{route('shop-order-items.index')}}?order_id={{$order->id}}">
                            <i class="fa fa-file-excel-o"></i>
                        </a>
                        <a class="mr-2 ml-2 text-warning" href="{{route('shop-order-return.index')}}?reference_shop_order_id={{$order->id}}">
                            <i class="fa fa-undo"></i>
                        </a>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>


@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
{{--    <script src="{{asset('js/dataTables.select.min.js')}}"></script>--}}
    <script src="{{asset('assets/vendor/select2/js/select2.js')}}"></script>
{{--    <script src="{{asset('assets/vendor/dtable/Buttons-1.5.1/js/dataTables.buttons.js')}}"></script>--}}


    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.11.3/b-2.0.1/b-colvis-2.0.1/b-html5-2.0.1/b-print-2.0.1/datatables.min.js"></script>

    <script>
        let dt = $('#table').DataTable({
            order: [[ 1, 'asc' ]],
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf'
            ]
        });

    </script>
@stop
