@extends('layouts.back.master')@section('title','Orders')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" href="{{asset('css/select.dataTables.min.css')}}">


    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/vendor/select2/css/select2.css')}}"/>
    {{--    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/datatables.net/css/buttons.dataTables.min.css')}}">--}}

    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.11.3/b-2.0.1/b-colvis-2.0.1/b-html5-2.0.1/b-print-2.0.1/datatables.min.css"/>


    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }

        .abc {
            width: 1000px;
            height: 1000px;
            /* background-color: black; */
            z-index: 8000;

        }

        .sweet-alert {
            z-index: 7999 !important;
        }

        .sweet-overlay {
            z-index: 7998 !important;
        }

    </style>
@stop

@section('content')

    <div id="wait" class="d-none abc" style=""><img src='{{asset('assets/img/4V0b.gif')}}' width="64" height="64"/><br>Loading..
    </div>
    {{ Form::open(array('route' => 'shop-orders.list','enctype'=>'multipart/form-data','method'=>'GET'))}}
    <div class="row">
        <div class="col-sm-3">
            <div class="form-group">
                {!! Form::text('date_range',$dateRange,['class' => 'form-control','id'=>'date','placeholder'=>'Select Date','autocomplete'=>'off']) !!}
            </div>
        </div>

        @if((Sentinel::check()->roles[0]->slug != 'sales-ref'))

            <div class="col-sm-3">
                <div class="form-group">
                    {!! Form::select('agent_id',$agents,$agentId,['class' => 'form-control','id'=>'agent_id','placeholder'=>'Select Agent','autocomplete'=>'off']) !!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    {!! Form::select('shop_id',$shops,$shopId,['class' => 'form-control','id'=>'shop_id','placeholder'=>'Select Shop','autocomplete'=>'off']) !!}
                </div>
            </div>
        @else
            <div class="p-3">
                <input type="hidden" name="agent_id" id="agent_id" value="{{Sentinel::getUser()->id}}">

            </div>
        @endif


        <div class="col-sm-3">
            <div class="form-group">
                <button type="submit" class="btn btn-success" >Filter</button>
                <a href="{{route('shop-orders.list')}}" class="btn btn-info" >Clear Filter</a>
            </div>
        </div>
    </div>

    {!! Form::close() !!}

    <div class="">

        <table id="table" class="display text-center">
            <thead>
            <tr>
                <th>ID</th>
                <th>Shop</th>
                <th>Agent</th>
                <th>Invoice Number</th>
                <th>DateTime</th>
                <th>Amount</th>
                <th>Return Value</th>
                <th>Net Amount</th>
                <th>Item Count</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>


            @foreach($orders as $order)
                {{--            @foreach($orderx as $order)--}}
                <tr>

                    <td>{{$order->id}}</td>
                    <td>{{optional($order->shop)->name}}</td>
                    <td>{{optional($order->agent)->name_with_initials}}</td>
                    <td>{{$order->invoice_number}}</td>
                    <td>{{$order->ordered_time}}</td>
                    <td>{{number_format($order->amount,2)}}</td>
                    <td>{{number_format($order->return_value,2)}}</td>
                    <td>{{number_format($order->net_amount,2)}}</td>
                    <td>{{sizeof($order->items)}}</td>
                    <td>
                        @if (empty($order->shop_id) && (Sentinel::check()->roles[0]->slug != 'sales-ref'))
                            <button class="mr-2 ml-2 text-danger btn btn-rounded"
                                  onclick="changeShop({{optional($order->agent)->id}},{{$order->id}})">
                                <i class="fa fa-pencil"></i>
                            </button>
                        @endif
                        <a class="mr-2 ml-2 text-success btn btn-rounded"
                           href="{{route('shop-order-items.index')}}?order_id={{$order->id}}">
                            <i class="fa fa-file-excel-o"></i>
                        </a>
                        <a class="mr-2 ml-2 text-warning btn btn-rounded"
                           href="{{route('shop-order-return.index')}}?reference_shop_order_id={{$order->id}}">
                            <i class="fa fa-undo"></i>
                        </a>

                    </td>
                </tr>
                {{--            @endforeach--}}
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th>ID</th>
                <th>Shop</th>
                <th>Agent</th>
                <th>Invoice Number</th>
                <th>DateTime</th>
                <th>Amount</th>
                <th>Return Value</th>
                <th>Net Amount</th>
                <th>Item Count</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>

    </div>

    <div class="modal fade" id="projects-task-modal" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="card">

                        {!! Form::open(['url'=>'/shops/change-shop-id','method'=>'POST']) !!}
                        <input type="hidden" name="order_id" id="order_id">
                            <div class="projects-list">
                                <div class=" row">
                                    <div class="col-sm-12">
                                        <div class="d-flex mr-auto">
                                            <div class="form-group">
                                                <label for="single">Shop</label>
                                                {!! Form::select('shop_id',$shops,null,['class'=>'form-control select2 w-100','Placeholder'=>'Select A Shop','required','id'=>"shops"]) !!}
                                            </div>
                                        </div>
                                    </div>


                                </div>

                                <button type="submit"  class="btn btn-success btn-lg btn-block">
                                    Change Shop
                                </button>

                            </div>

                      {!! Form::close() !!}

                    </div>

                </div>

            </div>
        </div>
    </div>


@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    {{--    <script src="{{asset('js/dataTables.select.min.js')}}"></script>--}}
    <script src="{{asset('assets/vendor/select2/js/select2.js')}}"></script>
    {{--    <script src="{{asset('assets/vendor/dtable/Buttons-1.5.1/js/dataTables.buttons.js')}}"></script>--}}


    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.11.3/b-2.0.1/b-colvis-2.0.1/b-html5-2.0.1/b-print-2.0.1/datatables.min.js"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.11.3/api/sum().js"></script>
    <script>
        let dt = $('#table').DataTable({
            order: [[4, 'desc']],
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf'
            ],
            drawCallback: function (settings) {
                var api = this.api();

                $(api.column(5).footer()).html(
                    'Total : ' + api.column(5).data().sum().toFixed(2)
                );
                $(api.column(6).footer()).html(
                    'Total Return: ' + api.column(6).data().sum().toFixed(2)
                );
                $(api.column(7).footer()).html(
                    'Total Net: ' + api.column(7).data().sum().toFixed(2)
                );

            },
        });

        $('#date').daterangepicker({
            "showDropdowns": true,
            "timePicker": false,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "01/01/2022"
        });

        function changeShop(agentId,orderId){
            var shops = $('#shops');
            $('#shops').empty();
            $('#order_id').val(orderId)

            $.ajax({
                type: 'get',
                url: 'shops-by-agent',
                data: {
                    'agent_id': agentId
                },
                success: function (response) {
                    var data = JSON.parse(response).data;
                    for (var i = 0; i < data.length; i++) {
                        shops.append('<option value=' + data[i].shop_id + '>' + data[i].shop_key + ' - '+  data[i].name+' </option>');
                    }
                }
            })
            $("#projects-task-modal").modal("show");
        }

    </script>
@stop
