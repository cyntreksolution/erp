<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>404 | Not Found</title>
    <meta name="description" content="Luxury is a premium adman dashboard template based on bootstrap">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <link rel="apple-touch-icon" href="apple-touch-icon.html"><!-- core plugins -->
    <link rel="stylesheet" href="{{asset('assets/vendor/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet"
          href="{{asset('')}}../assets/vendor/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.css">
    <!-- core plugins --><!-- site-wide stylesheets -->
    <link rel="stylesheet" href="{{asset('assets/css/site.css')}}"><!-- / site-wide stylesheets --><!-- styles for the current page -->
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/error.css')}}"><!-- / styles for the current page -->
    <style>@import 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,600';</style>
</head>
<body class="simple-page error-page error-page-404"><!--[if lt IE 10]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<div class="error-page-content bg-white">
    <div class="text-center">
        <div class="mb-3">
            <svg class="svg-bug-icon">
                <use xlink:href="http://spantags.com/luxury/assets/global/svg-sprite/sprite.svg#bug"/>
            </svg>
        </div>
        <h1 class="display-4 text-success mb-5 mt-0">No Access for Curent User</h1>
        <a href="{{route('home')}}" class="btn bg-faded text-primary border-a-1 py-3 px-5">
            <i class="fa fa-long-arrow-left mr-3"></i>Back To Home
        </a>
    </div>
</div><!-- /.error-page-content --><!-- core plugins -->
<script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/tether/dist/js/tether.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
</body>
</html>