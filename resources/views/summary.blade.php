@extends('layouts.back.master')@section('title','Summary')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.wizard.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">

    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.cs')}}s">

@stop
@section('content')
    <div class="app-wrapper">
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">Summary at {{$work_date.' of '.$employee->full_name}}</h5>
            </div>


            <div class="table-responsive">
                <table id="esr_table" class="display text-center ">
                    <thead>
                    <tr bgcolor="#b8860b">
                        <th class="text-center">Serial</th>
                        <th class="text-center">Size</th>
                        <th class="text-center">Group</th>
                        <th class="text-center">Members</th>
                        <th class="text-center">A</th>
                        <th class="text-center">B</th>
                        <th class="text-center">C</th>
                        <th class="text-center">D</th>
                    </tr>
                    </thead>
                    <tbody>



                    @php
                        $gdaSum= 0;$gdbSum= 0;$gdcSum= 0;$gddSum= 0;
                        $dayCommis=0;
                    @endphp
                    @if (!empty($semi_finish_maker) && sizeof($semi_finish_maker)>0)


                        <tr>
                            <td colspan="8" bgcolor="#dfccff">Semi Finish Product Stage 1</td>
                        </tr>
                        @php  $sumsemi_a1 = 0; $sumsemi_b1 = 0; $sumsemi_c1 = 0; $sumsemi_d1 = 0; @endphp

                        @foreach($semi_finish_maker as $burden)
                            <tr>
                                <td><a target='_blank' href='{{route('burden.historyView',[$burden->id])}}'>{{$burden->serial}} {{$burden->semiProdcuts->name}}</a></td>
                                <td>{{$burden->burden_size}}</td>

                                @php  $groups = \EmployeeManager\Models\Group::find($burden->employee_group); @endphp
                                <td>{{$groups->name}}</td>

                                <td>{{$commission->getMembers($burden->employee_group)}}</td>
                                @php
                                    $members=0;
                                    $members1=$commission->getMembers($burden->employee_group);
                                    if($members1 > 0){
                                        $members=$members1;
                                    }else{
                                        dd("No Members in Semi Finish Product Stage 1 : ".$burden->serial);
                                    }

                                    $product_id=$burden->semi_finish_product_id;
                                    $stage_id=1;
                                    $pro_type=1;
                                    $payment=$commission->getPay($product_id,$stage_id,$pro_type);

                                    if(empty($payment)){
                                        dd("No Payment Asign For this : ".$burden->serial.$burden->semiProdcuts->name);
                                        $grade_a = 0;
                                    }else{
                                        $grade_a = $payment->grade_a;
                                    }

                                    if(empty($payment)){
                                        $grade_b = 0;
                                    }else{
                                        $grade_b = $payment->grade_b;
                                    }

                                    if(empty($payment)){
                                        $grade_c = 0;
                                    }else{
                                        $grade_c = $payment->grade_c;
                                    }

                                    if(empty($payment)){
                                        $grade_d = 0;
                                    }else{
                                        $grade_d = $payment->grade_d;
                                    }



                    $a=0;$b=0;$c=0;$d=0;
                    foreach ($burden->grade as $grade){

                    if($burden->responce==0)
                        {
                        if ($grade->grade_id==1) {
                            $a=$grade->burden_qty;
                        }

                        if ($grade->grade_id==2) {
                            $b=$grade->burden_qty;
                        }

                        if ($grade->grade_id==3) {
                            $c=$grade->burden_qty;
                        }

                        if ($grade->grade_id==4) {
                            $d=$grade->burden_qty;
                        }
                        }else
                            {
                             $a=$burden->expected_semi_product_qty;
                             $b=0;
                             $c=0;
                             $d=0;
                            }

                    }
                            $rawsum = 0;

                                 if($burden->burden_size != 0){
                                    $gda=($a / $burden->expected_semi_product_qty);
                                    $gdb=($b / $burden->expected_semi_product_qty);
                                    $gdc=($c / $burden->expected_semi_product_qty);
                                    $gdd=($d / $burden->expected_semi_product_qty);

                                    $gdaTotal=$gda*(($burden->burden_size*$grade_a)/$members);
                                    $gdaSum= $gdaSum+$gdaTotal;
                                    $sumsemi_a1 = $sumsemi_a1 + $gdaTotal;
                                    $gdbTotal=$gdb*(($burden->burden_size*$grade_b)/$members);
                                    $gdbSum= $gdbSum+$gdbTotal;
                                    $sumsemi_b1 = $sumsemi_b1 + $gdbTotal;

                                    $gdcTotal=$gdc*(($burden->burden_size*$grade_c)/$members);
                                    $gdcSum= $gdcSum+$gdcTotal;
                                    $sumsemi_c1 = $sumsemi_c1 + $gdcTotal;

                                    if( $d < 0 )
                                        {

                                        $gddTotal=$gdd*(($burden->burden_size*$grade_a)/$members);
                                        }else{

                                    $gddTotal=$gdd*(($burden->burden_size*$grade_d)/$members);

                                    }
                                    $rawsum = $gdaTotal + $gdbTotal + $gdcTotal + $gddTotal;
                                    $sumsemi_d1 = $sumsemi_d1 + $gddTotal;
                                    $gddSum= $gddSum+$gddTotal;
                                    }

                                @endphp
                                @if($rawsum == 0 )
                                    <td bgcolor="orange"></td><td bgcolor="orange"></td><td bgcolor="orange"></td><td bgcolor="orange"></td>

                                @else

                                <td>{{!empty($a)? number_format($gdaTotal,2)  :0}}
                                    - ({{!empty($a)?$a:0}})

                                </td>
                                <td>{{!empty($b)? number_format($gdbTotal,2)  :0}}
                                    - ({{!empty($b)?$b:0}})
                                </td>
                                <td>{{!empty($c)? number_format($gdcTotal,2)  :0}}
                                    - ({{!empty($c)?$c:0}})
                                </td>
                                <td>{{!empty($d)? number_format($gddTotal,2)  :0}}
                                    - ({{!empty($d)?$d:0}})
                                </td>
                                @endif
                            </tr>

                        @endforeach

                        <tr class="bg-success">
                            <td class="text-left">Total Earn of Semi Finish Stage One</td>
                            <td ></td>
                            <td ></td>
                            <td></td>
                            <td class="text-center">{{number_format($sumsemi_a1,2)}}</td>
                            <td class="text-center">{{number_format($sumsemi_b1,2)}}</td>
                            <td class="text-center">{{number_format($sumsemi_c1,2)}}</td>
                            <td class="text-center">{{number_format($sumsemi_d1,2)}}</td>
                        </tr>
                    @endif
                    @if (!empty($cooking_request_maker) && sizeof($cooking_request_maker)>0)
                        <tr>
                            <td colspan="8" bgcolor="#dfccff">Cooking Request Stage 1</td>
                        </tr>
                        @php  $sumck_a1 = 0;  @endphp
                        @foreach($cooking_request_maker as $burden)
                            <tr>
                                <td><a target='_blank' href='{{route('shorteats.historyView',[$burden->id])}}'>{{$burden->serial}} {{$burden->semiProdcuts->name}}</a></td>
                                <td>{{$burden->burden_size}}</td>
                                @php  $groups = \EmployeeManager\Models\Group::find($burden->employee_group); @endphp
                                <td>{{$groups->name}}</td>


                                <td>{{ $commission->getMembers($burden->employee_group)}}</td>
                                @php
                                    $members=0;
                                    $members1=$commission->getMembers($burden->employee_group);
                                    if($members1 > 0){
                                        $members=$members1;
                                    }else{
                                         dd("No Members in Cooking Request Stage 1 : ".$burden->serial);
                                    }

                                    $product_id=$burden->semi_finish_product_id;
                                    $stage_id=1;
                                    $pro_type=1;
                                    $payment=$commission->getPay($product_id,$stage_id,$pro_type);

                                     if(empty($payment)){

                                         dd("No Payment Asign For this : ".$burden->serial.$burden->semiProdcuts->name);
                                         $grade_a = 0;
                                         $a = 0;
                                    }else{
                                         $grade_a = $payment->grade_a;
                                        $a=$burden->expected_semi_product_qty;
                                    }
                                $gdaTotal= 0;
                                if($burden->responce == 0){
                                       if($burden->burden_size != 0){
                                        $gda=($a / $burden->expected_semi_product_qty);
                                        $gdaTotal=$gda*(($burden->burden_size*$grade_a)/$members);
                                        $sumck_a1 = $sumck_a1 + $gdaTotal;
                                        $gdaSum= $gdaSum + $gdaTotal;
                                        }
                                   }
                                @endphp
                                @if($gdaTotal == 0)
                                    <td bgcolor="orange"></td><td bgcolor="orange"></td><td bgcolor="orange"></td><td bgcolor="orange"></td>

                                @else
                                    <td>{{number_format($gdaTotal,2)}}- ({{$burden->expected_semi_product_qty}})</td>
                                    <td>0 (0)</td>
                                    <td>0 (0)</td>
                                    <td>0 (0)</td>
                                @endif
                            </tr>
                        @endforeach
                        <tr class="bg-success">
                            <td class="text-left">Total Earn of Cooking Request Stage One</td>
                            <td ></td>
                            <td ></td>
                            <td></td>
                            <td class="text-center">{{number_format($sumck_a1,2)}}</td>
                            <td class="text-center">{{0}}</td>
                            <td class="text-center">{{0}}</td>
                            <td class="text-center">{{0}}</td>
                        </tr>
                    @endif
                    @php
                   // dd($end_product_maker);

                    @endphp

                    @if (!empty($end_product_maker) && sizeof($end_product_maker)>0)
                        <tr>
                            <td colspan="8" bgcolor="#dfccff">End Product Stage 1</td>
                        </tr>

                        @php  $sumend_a1 = 0; $sumend_b1 = 0; $sumend_c1 = 0; $sumend_d1 = 0; @endphp

                        @foreach($end_product_maker as $burden)
                            <tr>
                                <td><a target='_blank' href='{{route('end-burden.show',[$burden->id])}}'>{{$burden->serial}} {{$burden->endProduct->name}}</a></td>

                     {{--           <td><a target='_blank' href='/view-history/{{$burden->id}}'>{{$burden->serial}} {{$burden->endProduct->name}}</a></td>  --}}
                                <td>{{$burden->burden_size}}</td>
                                @php  $groups = \EmployeeManager\Models\Group::find($burden->employee_group); @endphp
                                <td>{{$groups->name}}</td>

                                <td>{{ $commission->getMembers($burden->employee_group)}}</td>
                                @php
                                    $members=0;
                                    $members1=$commission->getMembers($burden->employee_group);


                                    if($members1 > 0){
                                        $members=$members1;
                                    }else{
                                        dd("No Members in End Product Burden Stage 1 : ".$burden->serial);
                                    }

                                   $product_id=$burden->end_product_id;
                                   $stage_id=1;
                                   $pro_type=2;
                                   $payment=$commission->getPay($product_id,$stage_id,$pro_type);

                                     if(empty($payment)){
                                        dd("No Payment Asign For this : ".$burden->serial.$burden->endProduct->name);
                                        $grade_a = 0;
                                    }else{
                                        $grade_a = $payment->grade_a;
                                    }

                                    if(empty($payment)){
                                        $grade_b = 0;
                                    }else{
                                        $grade_b = $payment->grade_b;
                                    }

                                    if(empty($payment)){
                                        $grade_c = 0;
                                    }else{
                                        $grade_c = $payment->grade_c;
                                    }

                                    if(empty($payment)){
                                        $grade_d = 0;
                                    }else{
                                        $grade_d = $payment->grade_d;
                                    }


                    $a=0;$b=0;$c=0;$d=0;
                    foreach ($burden->grade as $grade){
                    if($burden->response==0)
                        {
                        if ($grade->grade_id==1) {
                            $a=$grade->burden_qty;
                        }

                        if ($grade->grade_id==2) {
                            $b=$grade->burden_qty;
                        }

                        if ($grade->grade_id==3) {
                            $c=$grade->burden_qty;
                        }

                        if ($grade->grade_id==4) {
                            $d=$grade->burden_qty;
                        }
                        }else
                            {
                            $a=$burden->expected_end_product_qty;
                             $b=0;
                             $c=0;
                             $d=0;
                        }
                    }

                                    $rawsum = 0;

                                   if($burden->burden_size != 0){

                                        $gda=($a / $burden->expected_end_product_qty);
                                        $gdb=($b / $burden->expected_end_product_qty);
                                        $gdc=($c / $burden->expected_end_product_qty);
                                        $gdd=($d / $burden->expected_end_product_qty);

                                        $gdaTotal=$gda*(($burden->burden_size*$grade_a)/$members);
                                        $gdaSum= $gdaSum+$gdaTotal;
                                        $sumend_a1 = $sumend_a1 + $gdaTotal;
                                        $gdbTotal=$gdb*(($burden->burden_size*$grade_b)/$members);
                                        $gdbSum= $gdbSum+$gdbTotal;
                                        $sumend_b1 = $sumend_b1 + $gdbTotal;
                                        $gdcTotal=$gdc*(($burden->burden_size*$grade_c)/$members);
                                        $gdcSum= $gdcSum+$gdcTotal;
                                        $sumend_c1 = $sumend_c1 + $gdcTotal;


                                        if( $d < 0 )
                                            {
                                        $gddTotal=$gdd*(($burden->burden_size*$grade_a)/$members);
                                            }else
                                                {
                                              $gddTotal=$gdd*(($burden->burden_size*$grade_d)/$members);
                                                }
                                        $gddSum= $gddSum+$gddTotal;
                                        $sumend_d1 = $sumend_d1 + $gddTotal;
                                         $rawsum = $gdaTotal + $gdbTotal + $gdcTotal + $gddTotal;

                                    }
                                @endphp
                                @if($rawsum == 0)
                                    <td bgcolor="orange"></td><td bgcolor="orange"></td><td bgcolor="orange"></td><td bgcolor="orange"></td>

                                @else
                                    <td>{{!empty($a)? number_format($gdaTotal,2)  :0}}
                                        - ({{!empty($a)?$a:0}})
                                    </td>
                                    <td>{{!empty($b)? number_format($gdbTotal,2)  :0}}
                                        - ({{!empty($b)?$b:0}})
                                    </td>
                                    <td>{{!empty($c)? number_format($gdcTotal,2)  :0}}
                                        - ({{!empty($c)?$c:0}})
                                    </td>
                                    <td>{{!empty($d)? number_format($gddTotal,2)  :0}}
                                        - ({{!empty($d)?$d:0}})
                                    </td>
                                @endif
                            </tr>
                        @endforeach

                        <tr class="bg-success">
                            <td class="text-left">Total Earn of End Burden Stage One</td>
                            <td ></td>
                            <td ></td>
                            <td></td>
                            <td class="text-center">{{number_format($sumend_a1,2)}}</td>
                            <td class="text-center">{{number_format($sumend_b1,2)}}</td>
                            <td class="text-center">{{number_format($sumend_c1,2)}}</td>
                            <td class="text-center">{{number_format($sumend_d1,2)}}</td>
                        </tr>
                    @endif

                    @if (!empty($semi_finish_baker) && sizeof($semi_finish_baker)>0)

                        <tr>
                            <td colspan="8" bgcolor="#dfccff">Semi Finish Product Stage 2</td>
                        </tr>
                        @php  $sumsemi_a2 = 0; $sumsemi_b2 = 0; $sumsemi_c2 = 0; $sumsemi_d2 = 0; @endphp


                        @foreach($semi_finish_baker as $burden)
                            <tr>
                                <td><a target='_blank' href='{{route('burden.historyView',[$burden->id])}}'>{{$burden->serial}} {{$burden->semiProdcuts->name}}</a></td>
                                <td>{{$burden->burden_size}}</td>
                                @php  $groups = \EmployeeManager\Models\Group::find($burden->baker); @endphp
                                <td>{{$groups->name}}</td>

                                <td>{{ $commission->getMembers($burden->baker)}}</td>
                                @php
                                    $members=0;
                                    $members1=$commission->getMembers($burden->baker);
                                    if($members1 > 0){
                                        $members=$members1;
                                    }else{
                                        dd("No Members in Semi Finish Stage 2 : ".$burden->serial);

                                    }

                                    $product_id=$burden->semi_finish_product_id;
                                    $stage_id=2;
                                    $pro_type=1;
                                    $payment=$commission->getPay($product_id,$stage_id,$pro_type);

                                   if(empty($payment)){
                                         dd("No Payment Asign For this : ".$burden->serial.$burden->semiProdcuts->name);
                                        $grade_a = 0;
                                    }else{
                                        $grade_a = $payment->grade_a;
                                    }

                                    if(empty($payment)){
                                        $grade_b = 0;
                                    }else{
                                        $grade_b = $payment->grade_b;
                                    }

                                    if(empty($payment)){
                                        $grade_c = 0;
                                    }else{
                                        $grade_c = $payment->grade_c;
                                    }

                                    if(empty($payment)){
                                        $grade_d = 0;
                                    }else{
                                        $grade_d = $payment->grade_d;
                                    }



                    $a=0;$b=0;$c=0;$d=0;
                    foreach ($burden->grade as $grade){
                     if($burden->responce==1)
                        {
                        if ($grade->grade_id==1) {
                            $a=$grade->burden_qty;
                        }

                        if ($grade->grade_id==2) {
                            $b=$grade->burden_qty;
                        }

                        if ($grade->grade_id==3) {
                            $c=$grade->burden_qty;
                        }

                        if ($grade->grade_id==4) {
                            $d=$grade->burden_qty;
                        }
                        }else
                            {
                             $a=$burden->expected_semi_product_qty;
                             $b=0;
                             $c=0;
                             $d=0;
                            }
                    }

                                $rawsum = 0;
                                 if($burden->burden_size != 0){
                                    $gda=($a / $burden->expected_semi_product_qty);
                                    $gdb=($b / $burden->expected_semi_product_qty);
                                    $gdc=($c / $burden->expected_semi_product_qty);
                                    $gdd=($d / $burden->expected_semi_product_qty);
                                    $gdaTotal=$gda*(($burden->burden_size*$grade_a)/$members);
                                    $gdaSum= $gdaSum+$gdaTotal;
                                    $sumsemi_a2 = $sumsemi_a2 + $gdaTotal;
                                    $gdbTotal=$gdb*(($burden->burden_size*$grade_b)/$members);
                                    $gdbSum= $gdbSum+$gdbTotal;
                                    $sumsemi_b2 = $sumsemi_b2 + $gdbTotal;
                                    $gdcTotal=$gdc*(($burden->burden_size*$grade_c)/$members);
                                    $gdcSum= $gdcSum+$gdcTotal;
                                    $sumsemi_c2 = $sumsemi_c2 + $gdcTotal;
                                    if( $d < 0 )
                                        {
                                    $gddTotal=$gdd*(($burden->burden_size*$grade_a)/$members);
                                    }else
                                        {
                                         $gddTotal=$gdd*(($burden->burden_size*$grade_d)/$members);
                                        }
                                    $gddSum= $gddSum+$gddTotal;
                                    $rawsum = $gdaTotal + $gdbTotal + $gdcTotal +$gddTotal;
                                    $sumsemi_d2 = $sumsemi_d2 + $gddTotal;
                                    }

                                @endphp
                                @if($rawsum == 0)
                                    <td bgcolor="orange"></td><td bgcolor="orange"></td><td bgcolor="orange"></td><td bgcolor="orange"></td>

                                @else
                                <td>{{!empty($a)? number_format($gdaTotal,2)  :0}}
                                    - ({{!empty($a)?$a:0}})
                                </td>
                                <td>{{!empty($b)? number_format($gdbTotal,2)  :0}}
                                    - ({{!empty($b)?$b:0}})
                                </td>
                                <td>{{!empty($c)? number_format($gdcTotal,2)  :0}}
                                    - ({{!empty($c)?$c:0}})
                                </td>
                                <td>{{!empty($d)? number_format($gddTotal,2)  :0}}
                                    - ({{!empty($d)?$d:0}})
                                </td>
                                @endif
                            </tr>
                        @endforeach
                        <tr class="bg-success">
                            <td class="text-left">Total Earn of Semi Finish Stage Two</td>
                            <td ></td>
                            <td ></td>
                            <td></td>
                            <td class="text-center">{{number_format($sumsemi_a2,2)}}</td>
                            <td class="text-center">{{number_format($sumsemi_b2,2)}}</td>
                            <td class="text-center">{{number_format($sumsemi_c2,2)}}</td>
                            <td class="text-center">{{number_format($sumsemi_d2,2)}}</td>
                        </tr>

                    @endif
                    @if (!empty($cooking_request_baker) && sizeof($cooking_request_baker)>0)
                        <tr>
                            <td colspan="8" bgcolor="#dfccff">Cooking Request Stage 2</td>
                        </tr>
                        @php  $sumck_a2 = 0;  @endphp
                        @foreach($cooking_request_baker as $burden)
                            <tr>
                                <td><a target='_blank' href='{{route('shorteats.historyView',[$burden->id])}}'>{{$burden->serial}} {{$burden->semiProdcuts->name}}</a></td>
                                <td>{{$burden->burden_size}}</td>
                                @php  $groups = \EmployeeManager\Models\Group::find($burden->baker); @endphp
                                <td>{{$groups->name}}</td>

                                <td>{{ $commission->getMembers($burden->baker)}}</td>
                                @php
                                    $members=0;
                                    $members1=$commission->getMembers($burden->baker);
                                    if($members1 > 0){
                                        $members=$members1;
                                    }else{
                                        dd("No Members in Cooking Request Stage 2 : ".$burden->serial);
                                    }


                                    $product_id=$burden->semi_finish_product_id;
                                    $stage_id=2;
                                    $pro_type=1;
                                    $payment=$commission->getPay($product_id,$stage_id,$pro_type);


                                    if(empty($payment)){
                                        dd("No Payment Asign For this : ".$burden->serial.$burden->semiProdcuts->name);
                                        $a=0;
                                        $grade_a = 0;
                                    }else{
                                    $grade_a = $payment->grade_a;
                                    $a=$burden->expected_semi_product_qty;
                                    }

                                    $gdaTotal= 0;

                                    if($burden->responce == 1){
                                        if($burden->burden_size != 0){
                                            $gda=($a / $burden->expected_semi_product_qty);
                                            $gdaTotal=$gda*(($burden->burden_size*$grade_a)/$members);
                                            $sumck_a2 = $sumck_a2 + $gdaTotal;
                                            $gdaSum= $gdaSum+$gdaTotal;
                                        }
                                    }

                                @endphp
                                @if($gdaTotal == 0)
                                    <td bgcolor="orange"></td><td bgcolor="orange"></td><td bgcolor="orange"></td><td bgcolor="orange"></td>

                                @else
                                <td>{{number_format($gdaTotal,2)}}- ({{$burden->expected_semi_product_qty}})</td>
                                <td>0 (0)</td>
                                <td>0 (0)</td>
                                <td>0 (0)</td>
                                @endif
                            </tr>
                        @endforeach

                        <tr class="bg-success">
                            <td class="text-left">Total Earn of Cooking Request Stage Two</td>
                            <td ></td>
                            <td ></td>
                            <td></td>
                            <td class="text-center">{{number_format($sumck_a2,2)}}</td>
                            <td class="text-center">{{0}}</td>
                            <td class="text-center">{{0}}</td>
                            <td class="text-center">{{0}}</td>
                        </tr>
                    @endif

                    @if (!empty($end_product_baker) && sizeof($end_product_baker)>0)
                        <tr>
                            <td colspan="8" bgcolor="#dfccff">End Product Stage 2</td>
                        </tr>
                        @php  $sumend_a2 = 0; $sumend_b2 = 0; $sumend_c2 = 0; $sumend_d2 = 0; @endphp

                        @foreach($end_product_baker as $burden)
                            <tr>
                                <td><a target='_blank' href='{{route('end-burden.show',[$burden->id])}}'>{{$burden->serial}} {{$burden->endProduct->name}}</a></td>

                          {{--      <td><a target='_blank' href='/view-history/{{$burden->id}}'>{{$burden->serial}} {{$burden->endProduct->name}}</a></td>  --}}
                                <td>{{$burden->burden_size}}</td>
                                @php  $groups = \EmployeeManager\Models\Group::find($burden->baker); @endphp
                                <td>{{$groups->name}}</td>

                                <td>{{ $commission->getMembers($burden->baker)}}</td>
                                @php
                                    $members=0;
                                    $members1=$commission->getMembers($burden->baker);
                                    if($members1 > 0){
                                        $members=$members1;
                                    }else{
                                        dd("No Members in End Product Burden Stage 2 : ".$burden->serial);
                                    }


                                   $product_id=$burden->end_product_id;
                                   $stage_id=2;
                                   $pro_type=2;
                                   $payment=$commission->getPay($product_id,$stage_id,$pro_type);


                                   if(empty($payment)){
                                         dd("No Payment Asign For this : ".$burden->serial.$burden->endProduct->name);
                                        $grade_a = 0;
                                    }else{
                                        $grade_a = $payment->grade_a;
                                    }

                                    if(empty($payment)){
                                        $grade_b = 0;
                                    }else{
                                        $grade_b = $payment->grade_b;
                                    }

                                    if(empty($payment)){
                                        $grade_c = 0;
                                    }else{
                                        $grade_c = $payment->grade_c;
                                    }

                                    if(empty($payment)){
                                        $grade_d = 0;
                                    }else{
                                        $grade_d = $payment->grade_d;
                                    }


                    $a=0;$b=0;$c=0;$d=0;
                    foreach ($burden->grade as $grade){
                    if($burden->response==1)
                        {
                        if ($grade->grade_id==1) {
                            $a=$grade->burden_qty;
                        }

                        if ($grade->grade_id==2) {
                            $b=$grade->burden_qty;
                        }

                        if ($grade->grade_id==3) {
                            $c=$grade->burden_qty;
                        }

                        if ($grade->grade_id==4) {
                            $d=$grade->burden_qty;
                        }
                        }else
                            {
                            $a=$burden->expected_end_product_qty;
                             $b=0;
                             $c=0;
                             $d=0;
                        }
                    }
                            $rawsum = 0;
                             if($burden->burden_size != 0){
                                    $gda=($a / $burden->expected_end_product_qty);
                                    $gdb=($b / $burden->expected_end_product_qty);
                                    $gdc=($c / $burden->expected_end_product_qty);
                                    $gdd=($d / $burden->expected_end_product_qty);
                                    $gdaTotal=$gda*(($burden->burden_size*$grade_a)/$members);
                                    $gdaSum= $gdaSum+$gdaTotal;
                                    $sumend_a2 = $sumend_a2 + $gdaTotal;
                                    $gdbTotal=$gdb*(($burden->burden_size*$grade_b)/$members);
                                    $gdbSum= $gdbSum+$gdbTotal;
                                    $sumend_b2 = $sumend_b2 + $gdbTotal;
                                    $gdcTotal=$gdc*(($burden->burden_size*$grade_c)/$members);
                                    $gdcSum= $gdcSum+$gdcTotal;
                                    $sumend_c2 = $sumend_c2 + $gdcTotal;
                                    if( $d < 0 )
                                        {
                                    $gddTotal=$gdd*(($burden->burden_size*$grade_a)/$members);
                                        }else
                                            {
                                            $gddTotal=$gdd*(($burden->burden_size*$grade_d)/$members);
                                            }
                                    $gddSum= $gddSum+$gddTotal;
                                    $sumend_d2 = $sumend_d2 + $gddTotal;
                                    $rawsum = $gdaTotal + $gdbTotal + $gdcTotal + $gddTotal;
                                    }
                                @endphp
                                @if($rawsum == 0)
                                    <td bgcolor="orange"></td><td bgcolor="orange"></td><td bgcolor="orange"></td><td bgcolor="orange"></td>

                                @else
                                <td>{{!empty($a)? number_format($gdaTotal,2)  :0}}
                                    - ({{!empty($a)?$a:0}})
                                </td>
                                <td>{{!empty($b)? number_format($gdbTotal,2)  :0}}
                                    - ({{!empty($b)?$b:0}})
                                </td>
                                <td>{{!empty($c)? number_format($gdcTotal,2)  :0}}
                                    - ({{!empty($c)?$c:0}})
                                </td>
                                <td>{{!empty($d)? number_format($gddTotal,2)  :0}}
                                    - ({{!empty($d)?$d:0}})
                                </td>
                                @endif
                            </tr>
                        @endforeach

                        <tr class="bg-success">
                            <td class="text-left">Total Earn of End Burden Stage Two</td>
                            <td ></td>
                            <td ></td>
                            <td></td>
                            <td class="text-center">{{number_format($sumend_a2,2)}}</td>
                            <td class="text-center">{{number_format($sumend_b2,2)}}</td>
                            <td class="text-center">{{number_format($sumend_c2,2)}}</td>
                            <td class="text-center">{{number_format($sumend_d2,2)}}</td>
                        </tr>
                    @endif

                    </tbody>
                    <tfoot>
                    <tr bgcolor="#b8860b">
                        <th class="text-center">0</th>
                        <th class="text-center">0</th>
                        <th class="text-center">0</th>
                        <th class="text-center">0</th>
                        <th class="text-right">{{number_format($gdaSum,2)}}</th>
                        <th class="text-center">{{number_format($gdbSum,2)}}</th>
                        <th class="text-center">{{number_format($gdcSum,2)}}</th>
                        <th class="text-center">{{number_format($gddSum,2)}}</th>
                        @php
                            $dayCommis=$gdaSum+$gdbSum+$gdcSum+$gddSum;
                        @endphp


                    </tr>
                    <tr bgcolor="#b8860b">
                        <th class="text-center">Serial</th>
                        <th class="text-center">Size</th>
                        <th class="text-center">Group</th>
                        <th class="text-center">Members</th>
                        <th class="text-center">A</th>
                        <th class="text-center">B</th>
                        <th class="text-center">C</th>
                        <th class="text-center">D</th>
                    </tr>

                    <tr>
                        <th class="text-center" >Total Day Commis is </th>
                        <th class="text-center"><h3><b>{{number_format($dayCommis,2)}}</b></h3></th><th></th><th></th><th></th><th></th><th></th><th></th>
                    </tr>
                    </tfoot>
                </table>
                {!! Form::open(['route' => 'daycommis.store']) !!}
                <div class="row">
                    <div class="col-md-6 mt-5 ">
                        <a href="{{ url()->previous() }}" class="btn btn-block btn-primary" >Back
                        </a>

                    </div>
                    <input type="hidden" name="emp_id" value="{{$employee->id}}">
                    <input type="hidden" name="comis_date" value="{{$work_date}}">
                    <input type="hidden" name="day_comis" value="{{$dayCommis}}">

                    <div class="col-md-6 mt-5 ">
                    @if(Auth::user()->hasRole(['Super Admin', 'Accountant']))
                        <button type="submit" name="submit" onclick="this.form.submit();"
                            class="btn btn-block btn-warning">Add Day Commission
                        </button>
                    @endif
                    </div>
                </div>
                {!!Form::close()!!}
            </div>


        </div>
    </div>



@stop
@section('js')
<script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
<script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>

<script>
$(document).ready(function () {
table = $('#esr_table').DataTable({
"bProcessing": true,
searching: true,
pageLength: 10000,
responsive: true
});
});

</script>
@stop
