@extends('layouts.back.master')@section('title','Sales order')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .hide {
            display: none;
        }

        .app-main-content {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .portlet-placeholder {
            border: 1px dotted black;
            margin: 0 1em 1em 0;
            height: 100%;
        }

        #projects-task-modal .modal-bg {
            padding: 50px;
        }

        #projects-task-modal .modal-content {
            box-shadow: none;
            border: none
        }

        #projects-task-modal .close {
            border: none;
            outline: none;
            box-shadow: none;
            position: absolute;
            right: 1rem;
            top: 1rem;
            width: 25px;
            height: 25px;
            line-height: 25px;
            text-align: center;
            font-size: 1.13rem;
            border-radius: 500px;
            border: 1px solid #868e96
        }

        #floating-button {
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #db4437;
            position: fixed;
            top: 13rem;
            right: 90px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .plus {
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }

        #container {
            position: relative;
            padding: 0px;
            width: 100%;
            height: 200px;
            overflow: auto;
        }

        #container .content {
            width: 100%;
            height: 100%;
        }

    </style>
@stop
@section('content')
    <div class="app-wrapper">
        <div class="col-md-12" id="app-panel">
          <input type="hidden" id="hdnSessionType" value="{{session('error')}}" />
            <div class="col-md-12">
                <div class="app-main-header">
                    <div class="row">
                    <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-3">DATE :</div>
                                <div class="col-md-9">{{$salesHeader['order_date']}}</div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-3">CATEGORY :</div>
                                <div class="col-md-9"> {{$bf->category->name}}</div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-3">DAY :</div>
                                <div class="col-md-9">{{$bf->day}}</div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-3">TIME :</div>
                                <div class="col-md-9">{{$bf->time}}</div>
                            </div>
                        </div>


                    </div>
                </div>


                <div class="app-main-content">
                    <div class="container">
                    <form action="/order/update/@php echo $id; @endphp" method="POST">
                    {{ csrf_field() }}
                        <div class="projects-list">
                            @foreach($items as $item)
                                @if(!empty($item->end_product_end_product_id))
{{--                                    {{dd(EndProductManager\Models\EndProduct::find($item->end_product_end_product_id)->variants)}}--}}
                                    @if((sizeof(\EndProductManager\Models\EndProduct::find($item->end_product_end_product_id)->variants) == 0))
                                        <div class="adiv panel-item media my-2 px-2">
                                            <div class="row w-100">

                                                <div class="col-md-1">
                                                    <div>
                                                        <div class="avatar avatar text-white avatar-md project-icon bg-primary">
                                                            <img class="card-img-top" src="" alt="">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-4 px-3">
                                                    <div class="text-primary">
                                                        <h5 class="mt-3 mx-3"> {{\EndProductManager\Models\EndProduct::find($item->end_product_end_product_id)->name}} </h5>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 px-3">
                                                    <label>Default Order</label>
                                                    <div class="text-primary">
                                                        <h4>{{$item->qty}} </h4>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 px-3">
                                                    <label>Extra Order</label>
                                                    <div class="text-primary">
                                                      <h4>{{$item->extra_qty}}</h4>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 px-3">
                                                    <label>Available Quantity</label>
                                                    <div class="text-primary">
                                                      <h4>{{$item->available_qty}}</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endif
                            @endforeach
                            <hr>
                                @foreach($salesHeader->salesOrderVariants as $item)
                            <div class="adiv panel-item media my-2 px-2">
                                        <div class="row w-100">

                                            <div class="col-md-1">
                                                <div>
                                                    <div class="avatar avatar text-white avatar-md project-icon bg-primary">
                                                        <img class="card-img-top" src="" alt="">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4 px-3">
                                                <div class="text-primary">
                                                    <h5 class="mt-3 mx-3">@php $variant = \App\Variant::find($item->variant_id);  @endphp {{$variant->product->name.' '.$variant->variant}} </h5>
                                                </div>
                                            </div>
                                            <div class="col-md-3 px-3">
                                                <label>Default Order</label>
                                                <div class="text-primary">
                                                    <h4>{{$item->qty}} </h4>
                                                </div>
                                            </div>
                                            <div class="col-md-2 px-3">
                                                <label>Extra Order</label>
                                                <div class="text-primary">
                                                    <h4>{{$item->extra_qty}} </h4>
                                                </div>
                                            </div>
                                            <div class="col-md-2 px-3">
                                                <label>Available Quantity</label>
                                                <div class="text-primary">
                                                    <h4>{{$item->available_qty}} </h4>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    @endforeach
                        </div>


                        <div class="projects-list">
                            <hr>
                            @if($raw_materials->isNotEmpty())
                            <div class="app-main-content">
                                <div class="container">
                                    @php $orderQtyError @endphp
                                    <div class="projects-list">
                                        <table class="table">
                                            <tbody>
                                            @foreach($raw_materials as $raw)
                                            <tr>
                                                <th>
                                                @php $raw_materialName = RawMaterialManager\Models\RawMaterial::withoutGlobalScope('type')->whereType(4)->where('raw_material_id',$raw->raw_material_id)->first(); @endphp
                                                            {{$raw_materialName->name}}
                                                </th>
                                                <th class="text-right">{{$raw->qty}}</th>
                                            </tr>
                                                @endforeach
                                            </tbody>
                                            </table>
                                            </div>
                                            </div>
                            </div>
                            @endif
                        <div class="projects-list">
                            <div class="col-md-12 col-sm-12 my-4">
                                <textarea name="description" class="form-control" rows="5" disabled>{{$salesHeader->description}}</textarea>
                            </div>
                        </div>
                        <a href="/order/list/table" class="btn btn-success btn-lg btn-block mb-5">Back</a>
                    </div>
                </div>


            </div>
        </div>

        @stop
        @section('js')
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
            <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
            <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
            <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>

            <script>


                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });

                $('.delete').click(function (e) {
                    e.preventDefault();
                    id = $(this).attr("value");
                    confirmAlert(id);

                });

                function confirmAlert(id) {
                    swal({
                            title: "Are you sure?",
                            text: "Your will not be able to recover this imaginary file!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "Yes, delete it!",
                            closeOnConfirm: false
                        },
                        function () {
                            $.ajax({
                                type: "delete",
                                url: id,
                                success: function (response) {
                                    if (response == 'true') {
                                        swal("Deleted!", "Deleted successfully", "success");
                                        setTimeout(location.reload.bind(location), 900);
                                    } else {
                                        swal("Error!", "Something went wrong", "error");
                                    }
                                }
                            });

                        }
                    );

                }

                var sessionValueType= document.getElementById("hdnSessionType").value;
                $(document).ready(function () {
                    $("#unit").hide();
                    $('#pj-task-1').change(function () {
                        if (this.checked)
                            $("#unit").show();
                        else
                            $("#unit").hide();

                    });
                    if(sessionValueType !== ''){
                    if(sessionValueType == 'Yes'){
                      confirmAlertPreview("Are you sure?","Request has been processing","warning");
                    }
                    if(sessionValueType == 'No'){
                      confirmAlertPreview("Success!!","Request has been updated","success");
                    }
                  }
                });

                function confirmAlertPreview(sessionValueTitle,sessionValueMessage,type){
                  swal({
                          title: sessionValueTitle,
                          text: sessionValueMessage,
                          type: type,
                          showCancelButton: false,
                          confirmButtonColor: '#3085d6',
                          confirmButtonText: "Confirm",
                          closeOnConfirm: false
                      },
                      function () {
                          window.location = "/order/list/table";

                      }
                  );
                }
                $("#add").click(function () {

                    var a = $("#single option:selected").val();
                    var qty = $('#qty').val();

                    var o = validateEnd();
                    if (o) {
                        $.ajax({
                            type: "post",
                            url: '../end_product/get/endpro',
                            data: {id: a},

                            success: function (response) {
                                $(".end-product-list").append(' <div class="adiv panel-item media my-2 px-2"  >' +
                                    '<div class="d-flex mr-auto">' +
                                    '<div>' +
                                    '<div class="avatar avatar text-white avatar-md project-icon bg-primary">' +
                                    '<img class="card-img-top" src="" alt="">' +
                                    '</div>' +
                                    '</div>' +
                                    '<h5 class="mt-3 mx-3" >' + response['name'] + '</h5>' +
                                    '<input type="hidden" name="extra_end_product[]" value=" ' + response['id'] + ' "/>' +
                                    '<input type="hidden" name="extra_qty[]" value=" ' + qty + ' "/>' +
                                    '<input type="hidden" name="extra_unit_price[]" value=" ' + response['selling_price'] + ' "/>' +
                                    '</div>' +
                                    '<div class="d-flex activity-counters justify-content-between">' +
                                    '<div class="text-center px-3">' +
                                    '<div class="text-primary"><h4 >' + qty + '</h4>' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="text-center px-3">' +
                                    '<div class="text-primary"><h4 >' + response['selling_price'] + ' LKR </h4>' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="text-center px-3">' +
                                    '<div class="text-primary"><h4 >' + parseFloat(qty) * parseFloat(response['selling_price']) + 'LKR </h4>' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="section-2">' +
                                    '<div onclick="removediv(this)" class="delete btn btn-light btn-sm ml-2" value=" ' + response['id'] + ' " data-toggle="tooltip" data-placement="left" title="DELETE"> ' +
                                    '<i class="zmdi zmdi-close"></i>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div> ' +
                                    '</div>');
                            }

//                    success: function (response) {
//
//                        $(".abc").append('<div class="panel-item media my-2">' +
//                            '<div class="avatar avatar-sm project-icon bg-primary"data-plugin="firstLitter"data-target="#raw-4">' +
//                            '</div>' +
//                            '<div class="media-body">' +
//                            '<h6 class="media-heading my-1">' +
//                            '<h6 id="raw-4">' + response['name'] + '</h6></h6>' +
//                            '<div class="d-flex activity-counters justify-content-between">' +
//                           '<div class="text-center px-5">' +
//                           '<div class="text-primary" style="background-color: #00bcd4"><h4 >' + qty + '</h4>' +
//                           '</div>' +
//                           '</div>' +
//                           '<div class="section-2">' +
//                            '<div onclick="removediv(this)" class="delete btn btn-light btn-sm ml-2" value=" ' + response['id'] + ' " data-toggle="tooltip" data-placement="left" title="DELETE"> ' +
//                           '<i class="zmdi zmdi-close"></i>' +
//                            '</div>' +
//                           '</div>' +
//                            '</div> ' +
//                            '</div>');
//                    }
                        });

                        reset();
                    }


                });

                $("#raw-add").click(function () {

                    var a = $("#raw-material option:selected").val();
                    var qty = $('#r-qty').val();

                    var o = validateRaw();
                    if (o) {
                        $.ajax({
                            type: "post",
                            url: '/raw_material/get/raw',
                            data: {id: a},

                            success: function (response) {
                                $(".raw-list").append(' <div class="adiv panel-item media my-2 px-2"  >' +
                                    '<div class="d-flex mr-auto">' +
                                    '<div>' +
                                    '<div class="avatar avatar text-white avatar-md project-icon bg-primary">' +
                                    '<img class="card-img-top" src="" alt="">' +
                                    '</div>' +
                                    '</div>' +
                                    '<h5 class="mt-3 mx-3" >' + response['name'] + '</h5>' +
                                    '<input type="hidden" name="raw_materials[]" value=" ' + response['raw_material_id'] + ' "/>' +
                                    '<input type="hidden" name="r_qty[]" value=" ' + qty + ' "/>' +
                                    '</div>' +
                                    '<div class="d-flex activity-counters justify-content-between">' +
                                    '<div class="text-center px-3">' +
                                    '<div class="text-primary"><h4 >' + qty + '</h4>' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="section-2">' +
                                    '<div onclick="removediv(this)" class="delete btn btn-light btn-sm ml-2" value=" ' + response['raw_material_id'] + ' " data-toggle="tooltip" data-placement="left" title="DELETE"> ' +
                                    '<i class="zmdi zmdi-close"></i>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div> ' +
                                    '</div>');
                            }

//                    success: function (response) {
//
//                        $(".abc").append('<div class="panel-item media my-2">' +
//                            '<div class="avatar avatar-sm project-icon bg-primary"data-plugin="firstLitter"data-target="#raw-4">' +
//                            '</div>' +
//                            '<div class="media-body">' +
//                            '<h6 class="media-heading my-1">' +
//                            '<h6 id="raw-4">' + response['name'] + '</h6></h6>' +
//                            '<div class="d-flex activity-counters justify-content-between">' +
//                           '<div class="text-center px-5">' +
//                           '<div class="text-primary" style="background-color: #00bcd4"><h4 >' + qty + '</h4>' +
//                           '</div>' +
//                           '</div>' +
//                           '<div class="section-2">' +
//                            '<div onclick="removediv(this)" class="delete btn btn-light btn-sm ml-2" value=" ' + response['id'] + ' " data-toggle="tooltip" data-placement="left" title="DELETE"> ' +
//                           '<i class="zmdi zmdi-close"></i>' +
//                            '</div>' +
//                           '</div>' +
//                            '</div> ' +
//                            '</div>');
//                    }
                        });

                        reset_raw();
                    }


                });

                function removediv(e) {
                    $(e).closest(".adiv").remove();

                }

                function validateEnd() {
                    var ing = document.getElementById('single').value;
                    var qty = document.getElementById('qty').value;

                    if (ing == "") {
                        swal('Select End Product')
                        return false;
                    }

                    if (qty == "") {
                        swal('Enter Quantity')
                        return false;
                    } else {
                        return true;
                    }

                }

                function validateRaw() {
                    var ing = document.getElementById('raw-material').value;
                    var qty = document.getElementById('r-qty').value;

                    if (ing == "") {
                        swal('Select Outelet Item');
                        return false;
                    }

                    if (qty == "") {
                        swal('Enter Quantity');
                        return false;
                    } else {
                        return true;
                    }

                }

                function reset() {
                    document.getElementById("single").value = "";
                    document.getElementById("qty").value = "";
                }

                function reset_raw() {
                    document.getElementById("raw-material").value = "";
                    document.getElementById("r-qty").value = "";
                }

            </script>
@stop
