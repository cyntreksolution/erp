@extends('layouts.back.master')@section('title','Invoice Preview')
@section('css')

@stop


@section('content')
    <div class="container">
        <div class="app-wrapper">
            <div class="app-main">
                <div class="row">
                    <div class="col-md-12">
                        <div class="app-main-content px-3 py-4">
                            <div class="content">
                                <div class="row ">
                                    <div class="form-group col-md-1">
                                        <label for="">#</label>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="">Name</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Qty</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Price</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Total</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Commission</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Unit Discount</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Total Discount</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Amount</label>
                                    </div>
                                </div>
                                @php $count = 0;
                                $amount =0;$qty =0;
                                @endphp

                                @foreach($loading->items as $data)
                                    @if($data->qty != 0)
                                        @php
                                            $count = $count+1;
                                        $amount = $amount + $data->endProduct->selling_price * $data->qty;
                                        $qty = $qty + $data->qty;
                                        @endphp
                                        <div class="row ">
                                            <div class="form-group col-md-1">
                                                <label for="">{{ $count }}</label>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="">{{ $data->endProduct->name }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ $data->qty }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ number_format($data->endProduct->selling_price,2) }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ number_format($data->endProduct->selling_price * $data->qty,2) }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ number_format($data->commission,2) }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ number_format($data->unit_discount,2) }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ number_format($data->total_discount,2) }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ number_format($data->row_value,2) }}</label>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                                @foreach($loading->variants as $data)
                                    @if($data->weight != 0)
                                        @php $count = $count+1;
$amount = $amount + $data->variant->product->selling_price * $data->weight;
                                        $qty = $qty + $data->weight;
                                        @endphp
                                        <div class="row ">
                                            <div class="form-group col-md-1">
                                                <label for="">{{ $count }}</label>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="">{{ $data->variant->product->name.' '.$data->variant->variant }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ $data->weight }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ number_format($data->variant->product->selling_price,2) }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ number_format($data->variant->product->selling_price * $data->weight ,2) }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ number_format($data->commission,2) }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ number_format($data->unit_discount,2) }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ number_format($data->total_discount,2) }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ number_format($data->row_value,2) }}</label>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                                <div class="row ">
                                    <div class="form-group col-md-5">
                                        <label for="">Total</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">{{number_format($qty,2)}}</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right"></label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">{{number_format($amount,2)}}</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                    </div>
                                    <div class="col-md-1 text-right">
                                    </div>
                                    <div class="col-md-1 text-right">
                                    </div>
                                    <div class="col-md-1 text-right">
                                    </div>
                                </div>

                                <div class="row ">
                                    <div class="form-group col-md-1">
                                        <label for="">#</label>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="">Name</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Qty</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Price</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Total</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Commission</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Unit Discount</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Total Discount</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Amount</label>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <a class="btn btn-block btn-warning" href="{{ url()->previous() }}">Back</a>
            </div>
        </div>
    </div>

@stop
@section('js')


    <script>
        function checkItem(id) {
            let name = '#end_product_commission_' + id;
            let valu_name = '#end_product_commission_value_' + id;
            if ($(name).prop("checked") == true) {
                $(valu_name).removeAttr('disabled');
            } else if ($(name).prop("checked") == false) {
                $(valu_name).attr('disabled');
            }
        }
    </script>

    {{--    <script type="text/javascript">--}}
    {{--        var maxcommission = [];--}}
    {{--        //max price array--}}
    {{--        var maxtotal = [];--}}

    {{--        function countCommission(e, id) {--}}
    {{--            $('.checkbox_' + id).change(function () {--}}
    {{--                //variables--}}
    {{--                let qty = $(".checkbox_" + id).data('id');--}}
    {{--                let price = $('.checkbox_' + id).data('price');--}}
    {{--                let commission = $(".checkbox_" + id).data('info');--}}

    {{--                var billAmount = price * qty;--}}
    {{--                maxtotal.push(billAmount);--}}
    {{--                var maxprice = 0;--}}
    {{--                for (var i = 0; i < maxtotal.length; i++) {--}}
    {{--                    maxprice += maxtotal[i] << 0;--}}
    {{--                }--}}

    {{--                var total = qty * commission;--}}
    {{--                maxcommission.push(total);--}}
    {{--                var totalx = 0;--}}
    {{--                for (var i = 0; i < maxcommission.length; i++) {--}}
    {{--                    totalx += maxcommission[i] << 0;--}}
    {{--                }--}}
    {{--                // alert(totalx);--}}
    {{--                $('#billAmount').html(maxprice);--}}
    {{--                $('#commission').html(totalx);--}}
    {{--            });--}}
    {{--        }--}}

    {{--        var totalDiscount = [];--}}

    {{--        function countDiscount(e, id) {--}}
    {{--            $(".discount_" + id).focusout(function () {--}}
    {{--                // alert(id);--}}
    {{--                let discount = $(this).val();--}}
    {{--                // alert(discount);--}}
    {{--                totalDiscount.push(discount);--}}
    {{--                //discount--}}
    {{--                var discountx = 0;--}}
    {{--                for (var i = 0; i < totalDiscount.length; i++) {--}}
    {{--                    discountx += totalDiscount[i] << 0;--}}
    {{--                }--}}
    {{--                //total commission--}}
    {{--                var totalx = 0;--}}
    {{--                for (var i = 0; i < maxcommission.length; i++) {--}}
    {{--                    totalx += maxcommission[i] << 0;--}}
    {{--                }--}}
    {{--                //total price--}}
    {{--                var maxprice = 0;--}}
    {{--                for (var i = 0; i < maxtotal.length; i++) {--}}
    {{--                    maxprice += maxtotal[i] << 0;--}}
    {{--                }--}}
    {{--                // alert(totalx);--}}
    {{--                var finalTotal = maxprice - (totalx + discountx);--}}
    {{--                // alert(finalTotal);--}}
    {{--                $('#Disc').html(discountx);--}}
    {{--                $('#finalTOT').html(finalTotal);--}}
    {{--            });--}}
    {{--        }--}}

    {{--        function checkOrderStatus(order) {--}}
    {{--            let url = 'item/loading/check/' + order;--}}
    {{--            $.ajax({--}}
    {{--                type: "get",--}}
    {{--                url: url,--}}
    {{--                success: function (response) {--}}
    {{--                    // console.log(response)--}}
    {{--                    if (response == 'true') {--}}
    {{--                        $('#btnSave').removeClass('d-none');--}}
    {{--                    } else {--}}
    {{--                        $('#btnSave').addClass('d-none');--}}
    {{--                    }--}}
    {{--                }--}}
    {{--            });--}}
    {{--        }--}}
    {{--    </script>--}}


@stop
