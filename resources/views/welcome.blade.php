@extends('layouts.back.master')@section('title','Buntalk')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">

    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.css')}}">
    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .hide {
            display: none;
        }

        .app-main-content {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .portlet-placeholder {
            border: 1px dotted black;
            margin: 0 1em 1em 0;
            height: 100%;
        }

        #projects-task-modal .modal-bg {
            padding: 50px;
        }

        #projects-task-modal .modal-content {
            box-shadow: none;
            border: none
        }

        #projects-task-modal .close {
            border: none;
            outline: none;
            box-shadow: none;
            position: absolute;
            right: 1rem;
            top: 1rem;
            width: 25px;
            height: 25px;
            line-height: 25px;
            text-align: center;
            font-size: 1.13rem;
            border-radius: 500px;
            border: 1px solid #868e96
        }

        #floating-button {
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #db4437;
            position: fixed;
            top: 13rem;
            right: 90px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .plus {
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }

        #container {
            position: relative;
            padding: 0px;
            width: 100%;
            height: 200px;
            overflow: auto;
        }

        #container .content {
            width: 100%;
            height: 100%;
        }

        .jqstooltip {
            position: absolute;
            left: 0px;
            top: 0px;
            visibility: hidden;
            background: rgb(0, 0, 0) transparent;
            background-color: rgba(0, 0, 0, 0.6);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);
            -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";
            color: white;
            font: 10px arial, san serif;
            text-align: left;
            white-space: nowrap;
            padding: 5px;
            border: 1px solid white;
            z-index: 10000;
        }

        .jqsfield {
            color: white;
            font: 10px arial, san serif;
            text-align: left;
        }
    </style>
@stop
@section('content')

        <main class="site-man">
            <div class="row">

                <div class="col-md-4">
                    <div class="card p-2">
                        <h5 class="p-1 title">2020-03-30</h5>
                        <a href="{{route('shop-item-issue.index')}}"
                           class="btn-block btn btn-primary btn-block  text-white">issue shop items</a>
                        <a href="{{route('loading.stable')}}" class="btn-block btn btn-primary text-white">loading</a>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card p-2">
                        <h5 class="p-1 title">2020-04-02</h5>
                        <a disabled class="btn-block btn btn-primary btn-block text-white disabled">Cash Collection</a>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card p-2">
                        <h5 class="p-1 title">2020-04-03</h5>
                        <a disabled class="btn-block btn btn-primary btn-block text-white disabled">Agent Target </a>
                        <a disabled class="btn-block btn btn-primary btn-block text-white disabled">Day Transaction </a>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card p-2">
                        <h5 class="p-1 title">2020-04-04</h5>
                        <a disabled class="btn-block btn btn-primary btn-block text-white disabled">End Product Current Stock </a>
                        <a disabled class="btn-block btn btn-primary btn-block text-white disabled">Crates Inquiry </a>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card p-2">
                        <h5 class="p-1 title">2020-04-05</h5>
                        <a disabled class="btn-block btn btn-primary btn-block text-white disabled">Burden Inquiry </a>
                        <a disabled class="btn-block btn btn-primary btn-block text-white disabled">Bug Fixing </a>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card p-2">
                        <h5 class="p-1 title">2020-04-06</h5>
                        <a disabled class="btn-block btn btn-primary btn-block text-white disabled">Bug Fixing</a>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card p-2">
                        <h5 class="p-1 title">xxxx-xx-xx</h5>
                        <a href="{{route('buffer-stock.list-view')}}" class="btn-block btn btn-primary btn-block text-white">Pending Request Update</a>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card p-2">
                        <h5 class="p-1 title">xxxx-xx-xx</h5>
                        <a href="{{route('crates-return.index')}}" class="btn-block btn btn-primary btn-block text-white">Crates Return</a>
                    </div>
                </div>

            </div>
        </main>

@stop
