<html>
<head>
{{--    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">--}}
{{--    <link href="{{asset('assets/css/invoice.css')}}" rel="stylesheet">--}}
</head>
<body>
<div class="row">
    <h4 class="inv">
        BUFFER STOCK SUMMARY
    </h4>
</div>
<div class="row">
    <table>
        <tr>
            <th>Raw material</th>
            <th>Buffer stock</th>
            <th>Available qty</th>
            <th>Priority</th>
            <th>Last bill</th>
        </tr>
        @foreach($records as $record)
            @php
                $label_text = "Enabled";
                if ($record->enabled == 0) {
                    $label_text = "Disabled";
                }
            @endphp
            <tr>
                <td>{{$record->name}}</td>
                <td>{{$record->buffer_stock}}</td>
                <td>{{$record->available_qty}}</td>
                <td>{{$label_text}}</td>
                <td>{{0}}</td>
            </tr>
        @endforeach
    </table>
</div>
<footer>
    <div class="row" style="margin-top: 20px;">
        <h4 align="center">
            <h6 class="text-muted">powered by cyntrek<br>
                <h7 class="num">+9471 122 5489 / +9471 532 2262</h7>
            </h6>
        </h4>

    </div>
</footer>
</body>

</html>