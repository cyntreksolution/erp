@extends('layouts.back.master')@section('title','Buffer stock summary')
@section('css')
    <style type="text/css">

        .datatable-btn
        {
            float: right;
            margin: 0 10px;
            padding: 9px;
        }

        #floating-button {
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #00C851;
            position: fixed;
            bottom: 80px;
            right: 30px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .plus {
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }

        .sml {
            height: 30px;
            width: 50px;
            text-align: center;
            font-size: 0.75rem;
        }
    </style>
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/plugins/datatables.net-buttons/dataTables.buttons.js')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/datatables.net/css/buttons.dataTables.min.css')}}">
@stop
@section('content')

    {{ Form::open(array('url' => '/buffer_stock_summary/download','id'=>'filter_form'))}}
    {{ Form::close() }}

    <div class="">
        <table id="buffer_stock_summary_table" class="display text-center">
            <thead>
            <tr>
                <th>Raw material</th>
                <th>Buffer stock</th>
                <th>Available qty</th>
                <th>Priority</th>
                <th>Last bill</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Raw material</th>
                <th>Buffer stock</th>
                <th>Available qty</th>
                <th>Priority</th>
                <th>Last bill</th>
            </tr>
            </tfoot>
        </table>
    </div>
@stop
@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/plugins/datatables.net-buttons/dataTables.buttons.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/Buttons-1.5.1/js/dataTables.buttons.js')}}"></script>
    <script src="{{asset('/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/JSZip-2.5.0/jszip.min.js')}}"></script>
    <script src="{{asset('js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/pdfmake-0.1.32/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/Buttons-1.5.1/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('/js/vfs_fonts.js')}}"></script>
    <script>

        function download()
        {
            $("#filter_form").submit();
        }

        $(document).ready(function () {

            $('#buffer_stock_summary_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                "ajax": {
                    url: "{{url('/buffer_stock_summary/datatable')}}", // json datasource
                    type: "get",
                    error: function () {  // error handling code
                        $("#buffer_stock_summary_processing").css("display", "none");
                    }
                },
                pageLength: 25,
                responsive: true,
                dom: 'Bfrtip',
                "autoWidth": false,
                buttons: [
                    {
                        text: '',
                        className: 'btn btn-default fa fa-download datatable-btn',
                        action: function ( e, dt, node, config ) {
                            download();
                        },
                    },
                    {
                        extend: 'copy',
                        className: 'btn btn-xs btn-info ml-3 sml assets-export-btn export-copy ttip',
                    },

                    {
                        extend: 'excel',
                        className: 'btn btn-xs btn-success ml-3 sml assets-export-btn export-copy ttip',
                    },
                    {
                        extend: 'pdf',
                        className: 'btn btn-xs btn-danger ml-3 sml assets-export-btn export-copy ttip',
                    },

                    {
                        extend: 'print',
                        className: 'btn btn-xs btn-primary ml-3 sml assets-export-btn export-copy ttip',
                    },
                ]
            });

        });

    </script>
@stop