@extends('layouts.back.master')@section('title','Sales Returns')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .hide {
            display: none;
        }
    </style>
@stop
@section('content')
    <div class="app-wrapper">
        <div class="app-panel" id="app-panel">
            <div class="app-search">
                <input type="search" class="search-field" placeholder="Search"> <i class="search-icon fa fa-search"></i>
            </div>
            <div class="scroll-container" id="scroll-container">
                <div class="app-panel-inner">
                    <div class="scroll-container ps-container">
                        <div class="p-3">
                            {{--                            <a class="btn btn-success py-3 btn-block btn-lg text-white"--}}
                            {{--                               href="{{route('sales_return.create')}}">NEW SALES RETURN--}}
                            {{--                            </a>--}}

                            <a class="btn btn-success py-3 btn-block btn-lg text-white"
                               href="{{route('agent.return')}}">NEW SALES RETURN
                            </a>

                        </div>
                        <hr class="m-0">
                        <div class="people-list d-flex justify-content-start flex-wrap p-3">
                            {{--@foreach($supliers as $suplier)--}}
                            {{--@if(isset($suplier->image))--}}
                            {{--<a href="#" class="avatar avatar-circle avatar-sm">--}}
                            {{--<img src="{{asset($suplier->image_path.$suplier->image)}}" alt="">--}}
                            {{--</a>--}}
                            {{--@else--}}
                            {{--@php $color=array("blue","danger","purple","yellow","success","info","red") @endphp--}}
                            {{--<a href="#"--}}
                            {{--class="avatar avatar-circle avatar-sm bg-{{$color[$suplier->colour]}} "--}}
                            {{--data-plugin="firstLitter"--}}
                            {{--data-target="#sup-{{$suplier->id*87}}">--}}
                            {{--</a>--}}
                            {{--<h6 class="hide"--}}
                            {{--id="sup-{{$suplier->id*87}}">{{$suplier->supplier_name}}</h6>--}}
                            {{--@endif--}}
                            {{--@endforeach--}}

                            {{--<button class="avatar-sm avatar-circle fz-base font-weight-bold add-people-btn"--}}
                            {{--data-toggle="tooltip" data-placement="right" title=""--}}
                            {{--data-original-title="New Suplier">--}}
                            {{--<i class="zmdi zmdi-plus"></i>--}}
                            {{--</button>--}}
                        </div>
                        <hr class="m-0">
                        <div class="media-list">
                            {{--@foreach($supliers as $suplier)--}}
                            {{--<div class="media">--}}
                            {{--@if(isset($suplier->image))--}}
                            {{--<a href="#" class="avatar avatar-circle avatar-sm">--}}
                            {{--<img src="{{asset($suplier->image_path.$suplier->image)}}" alt="">--}}
                            {{--</a>--}}
                            {{--@else--}}
                            {{--@php $color=array("blue","danger","purple","yellow","success","info","red") @endphp--}}
                            {{--<a href="#"--}}
                            {{--class="avatar avatar-circle avatar-sm bg-{{$color[$suplier->colour]}} "--}}
                            {{--data-plugin="firstLitter"--}}
                            {{--data-target="#sup-{{$suplier->id*874}}">--}}
                            {{--</a>--}}
                            {{--@endif--}}

                            {{--<div class="media-body">--}}
                            {{--<h6 class="project-name"--}}
                            {{--id="sup-{{$suplier->id*874}}">{{$suplier->supplier_name}}</h6>--}}
                            {{--<small class="project-detail">{{$suplier->address}}</small>--}}
                            {{--<br>--}}
                            {{--<small class="project-detail">{{$suplier->mobile}}</small>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--@endforeach--}}

                        </div>

                    </div>
                </div>
            </div>
            <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show">
                <i class="fa fa-chevron-right"></i>
                <i class="fa fa-chevron-left"></i>
            </a>

        </div>

        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">Last 5 returns</h5>
            </div>


            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">
                    <div class="container">


                        <div class="projects-list">

                            @foreach($returns as $return)
                                @php $updateAt =$return->updated_at->toDateString();
                                $todayAt = Carbon\Carbon::now();
                                if($return->status == 5 && $updateAt !== $todayAt->toDateString()){}else{ @endphp
                                {{--                                <a href="{{'sales_return/'.$return->id}}">--}}
                                <div class="media">
                                    <div class="row w-100">

                                        <div class="{{($return->is_system_generated == 1)?'col-md-5':'col-md-3'}}">
                                            <div class="media-body mt-2">
                                                <h6 class="project-name"
                                                    id="project-1">{{$return->time}}</h6>
                                                <small class="project-detail">{{$return->debit_note_number}}</small>
                                            </div>
                                        </div>

                                        <div class="col-md-2 mt-2">
                                            <small class="project-1 mt-2">{{number_format($return->total,2)}}
                                                LKR</small>
                                        </div>


                                        <div class="{{($return->is_system_generated == 1)?'col-md-3':'col-md-5'}}">
                                            <div class="row">
                                                <div class="{{($return->is_system_generated == 1)?'col-12':'col-md-3 col-xs-12'}}">
                                                    @can('sales_return-adminIndex')
                                                    <a href="{{'sales_return/'.$return->id}}"
                                                       class="btn btn-outline-primary w-100 mt-2"
                                                    >View</a>
                                                    @endcan
                                                </div>
                                                @if($return->is_system_generated != 1)
                                                    @if($return->status == 1)
                                                        <div class="col-md-3 col-xs-12">
                                                            @can('sales_return-destroy')
                                                            {!! Form::open(['route' => ['sales_return.destroy',$return->id], 'method' => 'delete']) !!}
                                                            <button href="{{'sales_return/'.$return->id}}"
                                                                    class="btn btn-outline-danger w-100 mt-2">Delete
                                                            </button>
                                                            {!! Form::close() !!}
                                                            @endcan
                                                        </div>
                                                        <div class="col-md-3 col-xs-12">
                                                            @can('sales_return-sendReturn')
                                                            <a href="{{route('sales_return.send',$return->id)}}"
                                                               class="btn btn-outline-success w-100 mt-2">SEND</a>
                                                            @endcan
                                                        </div>
                                                        <div class="col-md-3 col-xs-12">
                                                            @can('sales_return-edit')
                                                            <a href="{{'sales_return/'.$return->id.'/edit'}}"
                                                               class="btn btn-outline-primary w-100 mt-2">Edit</a>
                                                            @endcan
                                                        </div>
                                                    @endif
                                                @endif

                                            </div>

                                        </div>


                                        <div class="col-md-2">
                                            @if($return->is_system_generated == 1)
                                                <a class="mt-2 btn btn-sm btn-dark text-white w-100">System
                                                    Generated</a>
                                            @else
                                                @if($return->status == 1)
                                                    <a class="btn-sm mt-2 btn btn-danger w-100">Collecting</a>
                                                @elseif ($return->status == 2)
                                                    <a class="btn-sm mt-2 btn btn-info w-100">Accepting</a>
                                                @elseif ($return->status == 3)
                                                    <a class="btn-sm mt-2 btn btn-warning w-100">Approving</a>
                                                @elseif ($return->status == 4)
                                                    <a class="btn-sm mt-2 btn btn-success w-100">Clearing</a>
                                                @elseif ($return->status == 5)
                                                    <a class="btn-sm mt-2 btn btn-primary text-white w-100">Settled</a>
                                                @endif
                                            @endif
                                        </div>
                                    </div>


                                </div>
                                <hr class="m-0">
                                {{--                                </a>--}}
                                @php } @endphp
                            @endforeach
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $('.delete').click(function (e) {
            e.preventDefault();
            id = $(this).attr("value");
            confirmAlert(id);

        });

        function confirmAlert(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "delete",
                        url: id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            } else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

        @if(session('pdf'))
        var id = '{!! session('pdf.file') !!}'
        $.ajax({
            type: "POST",
            url: '/loading/' + id + '/pdf',
            success: function (response) {
                window.open(response);
                console.log(response);
                debugger
            }
        });
        @endif

    </script>
@stop
