@extends('layouts.back.master')@section('title','End Product Stock Report')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">

    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/Buttons-1.5.1/css/buttons.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/datatables.net/css/buttons.dataTables.min.css')}}">
    <style>
        th {
            text-align: right !important;
        }

        td {
            text-align: right !important;
        }
    </style>
@stop

@section('content')
    {{ Form::open(array('url' => '/esr/excel/data','id'=>'filter_form'))}}
        <div class="row">
            <div class="col-md-2">
                {!! Form::select('category',$category , null , ['class' => 'form-control','placeholder'=>'Select Category','id'=>'category']) !!}
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    {!! Form::text('date',null,['class' => 'form-control','id'=>'date_range','placeholder'=>'Select Date']) !!}
                </div>
            </div>

{{--            <div class="col-sm-3">--}}
{{--                <div class="form-group">--}}
{{--                    {!! Form::select('status', [1=>'pending',2=>'received',3=>'creating',4=>'burning',5=>'grade',6=>'finished',] , null , ['class' => 'form-control','placeholder'=>'Status','id'=>'status']) !!}--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-sm-3">--}}
{{--                <div class="form-group">--}}
{{--                    <input type="text" name="date_range" id="date_range" class="form-control">--}}
{{--                </div>--}}
{{--            </div>--}}

            <div class="col-sm-3">
                <div class="form-group">
                    <button type="button" class="btn btn-success" onclick="process_form()">Filter Now</button>
                    <button type="button" class="btn btn-warning" onclick="process_form_reset()">Reset Filter</button>
                    <button type="submit" name="action" value="download_data" class="btn btn-info"><i
                                class="fa fa-download"></i></button>
                </div>
            </div>
        </div>
    {{ Form::close() }}
    <div class="">
        <table id="esr_table" class="display text-center">
            <thead>
            <tr>
                <th>ID</th>
                <th>End Product</th>
                <th>QTY</th>
                <th>Weight</th>
                <th>Value</th>
                <th>Action</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>ID</th>
                <th>End Product</th>
                <th>QTY</th>
                <th>Weight</th>
                <th>Value</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
    </div>
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script src="{{asset('assets/vendor/dtable/Buttons-1.5.1/js/dataTables.buttons.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/buttons/1.5.1/js/buttons.jqueryui.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/JSZip-2.5.0/jszip.min.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/pdfmake-0.1.32/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/Buttons-1.5.1/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js')}}"></script>


    <script>
        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": false,
            "singleDatePicker": true,
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019"
        });
        $('#date_range').val('')

        $(document).ready(function () {
            var total_qty=0;
            var total_price=0;
            var total_weight=0;
            table = $('#esr_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                dom: 'lfBrtip',
                "ajax": {
                    url: "{{url('/esr/table/data')}}",
                    type: "get",
                    error: function () {  // error handling code
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    },
                    dataSrc: function (data) {
                        total_qty = data.qty;
                        total_price = data.price;
                        total_weight = data.weight;
                        return data.data;
                    }
                },
                pageLength: 25,
                responsive: true,
                drawCallback: function (settings) {
                    var api = this.api();

                    $(api.column(4).footer()).html(
                        'Total Price : ' + total_price
                    );
                    $(api.column(3).footer()).html(
                        'Total Weight : ' + total_weight
                    );
                    $(api.column(2).footer()).html(
                        'Total QTY : ' + total_qty
                    );
                },
                buttons: [
                    {
                        extend: 'copy',
                        className: 'btn btn-xs btn-info ml-2 sml assets-export-btn export-copy ttip',
                    },

                    {
                        extend: 'excel',
                        className: 'btn btn-xs btn-success ml-2 sml assets-export-btn export-copy ttip',
                    },
                    {
                        extend: 'pdfHtml5',
                        download: 'open',
                        className: 'btn btn-xs btn-danger ml-2 sml assets-export-btn export-copy ttip',
                        titleAttr: 'Pdf',
                        filename: 'End_product_balance_report '+moment().format('DD_MM_YYYY'),
                        extension: '.pdf',
                        orientation: 'landscape',
                        title: "Buntalk End Product Balance Report "+moment().format('HH:mm:ss DD-MM-YYYY'),
                        footer: true,
                        exportOptions: {
                            orthogonal: "Export-pdf",
                        },
                    },
                    {
                        extend: 'print',
                        orientation: 'landscape',
                        className: 'btn btn-xs btn-primary ml-2 sml assets-export-btn export-copy ttip',
                    },

                ]
            });
        });

        function process_form() {
            let date_range = $("#date_range").val();
            let category = $("#category").val();
            // let status = $("#status").val();
            // let semi = $("#semi").val();
            // let table = $('#burden_table').DataTable();
            // table.ajax.url('/burden/table/data?semi=' + semi + '&status=' + status + '&date_range=' + date_range + '&filter=' + true).load();
            table.ajax.url('/esr/table/data?category=' + category + '&date_range=' + date_range + '&filter=' + true).load();

        }

        function process_form_reset() {
            $("#date_range").val('');
            $("#category").val('');
            // $("#status").val('');
            // $("#semi").val('');
            let table = $('#esr_table').DataTable();
            table.ajax.url('/esr/table/data').load();

        }


    </script>

@stop