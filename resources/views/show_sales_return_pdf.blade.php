@extends('layouts.back.master')@section('title','Sales Return')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}">


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .app-wrapper {
            font-family: Raleway, sans-serif;
            width: 100%;
            border: 1px solid #eee;
        }
    </style>
@stop
@section('content')
    <div class="app-wrapper">
        <div class="app-main">
            <div class="app-main-header">
                <div class="row">
                    <div class="col-md-6">
                        <h5 class=text-left">SALES RETURN - {{ $invoice->time}} - {{$invoice->debit_note_number}}</h5>
                        @if ($invoice->status==1)
                            <button class="btn btn-outline-danger btn-sm mr-1">pending</button>
                        @elseif ($invoice->status==2)
                            <button class="btn btn-outline-warning btn-sm mr-1">accepted</button>
                        @elseif ($invoice->status==3)
                            <button class="btn btn-outline-success btn-sm mr-1">billed</button>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <h5 class="text-right">{{$invoice->agent->first_name.' '.$invoice->agent->last_name}}</h5>
                        <h6 class="text-right">Total : {{number_format($invoice->total,'2')}} LKR</h6>
                    </div>
                </div>
            </div>
            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">
                    <div class="container">
                        {!! Form::open(['route' => 'sales_return.store']) !!}
                        <div class="projects-list">
                            @foreach($items as $product)
                                <input type="hidden" name="product_id[]" value="{{$product->endProduct->id}}">
                                <div class="card-body d-flex  align-items-center p-0">
                                    <div class="d-flex mr-auto">
                                        <div>
                                            <div class="avatar avatar text-white avatar-md project-icon bg-primary">
                                            </div>
                                        </div>
                                        <h5 class="mt-3 mx-3">{{$product->endProduct->name}}</h5>
                                    </div>
                                    <div class="d-flex activity-counters justify-content-between">
                                        <div class="text-left px-5">
                                            <div class="text-left">
                                                <input type="hidden" name="unit_price[]" class="form-control"
                                                       value="{{$product->type}}">
                                                <h6>{{$product->type}}</h6>
                                            </div>
                                        </div>
                                        <div class="text-center px-5">
                                            <div class="text-primary">
                                                <input type="hidden" name="unit_price[]" class="form-control"
                                                       value="{{$product->unit_value}}">
                                                <h6>{{number_format((float)$product->unit_value, 2, '.', '')}}LKR</h6>
                                            </div>
                                        </div>
                                        <div class="text-center px-5">
                                            <div class="text-primary">
                                                <h6>{{$product->qty}}</h6>
                                            </div>
                                        </div>
                                        <div class="text-center px-5">
                                            <div class="text-primary">
                                                <h6>{{number_format($product->qty * $product->unit_value,2)}} LKR</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="m-1">
                            @endforeach
                        </div>
                        {!!Form::close()!!}
                    </div>
                </div>

            </div>

            {!! Form::open(['route' => 'admin.sales_return_accept', 'method' => 'post']) !!}
            <input type="hidden" name="sales_return_header" value="{{$invoice->id}}">
            <div class="row m-5">
                <div class="col-md-6">
                    <button type="submit" name="submit" value="print_and_approve" class="btn btn-block btn-danger">
                        Print and Approve
                    </button>
                </div>
                <div class="col-md-6">
                    <button type="submit" name="submit" value="approve" class="btn btn-success btn-block">
                        Approve
                    </button>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>

@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

    </script>
@stop