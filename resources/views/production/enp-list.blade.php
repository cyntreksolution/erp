@extends('layouts.back.master')@section('title','Production |End Product Merged List')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }

        .abc {
            width: 1000px;
            height: 1000px;
            /* background-color: black; */
            z-index: 8000;

        }

        .sweet-alert {
            z-index: 7999 !important;
        }

        .sweet-overlay {
            z-index: 7998 !important;
        }

    </style>
@stop

@section('content')

    <div id="wait" class="d-none abc" style=""><img src='{{asset('assets/img/4V0b.gif')}}' width="64" height="64"/><br>Loading..

    </div>

    {!! Form::open(['route' => 'ep.list', 'method' => 'get']) !!}
    <div class="app-main-content mb-2">
        <div class="row mx-2">
            <div class="col-md-2">
                {!! Form::select('status',['created'=>'Pending','finished'=>'Finished','approved'=>'Approved'] , null , ['class' => 'form-control','placeholder'=>'Select Status','id'=>'status']) !!}
            </div>
            <div class="col-md-2">
                <input type="text" name="date" id="date_range" class="form-control" placeholder="Select Date">
            </div>
            <div class="col-md-1">
                <button class="btn btn-info " onclick="process_form(this)">Filter</button>
            </div>
            <div class="col-md-1">
                <a href="{{route('ep.list')}}" class="btn btn-success " >Reset</a>
            </div>
        </div>
    </div>

    {!! Form::close() !!}


    <div class="">
        <table id="invoice_table" class="display text-center">
            <thead>
            <tr>
                <th>Merge Created Date</th>
                <th>Serial</th>
                <th>Issue Raw</th>
                <th>Issue Semi</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>

            @foreach($merged as$key=> $merge)
                <tr>
                    <td>{{\Carbon\Carbon::parse($merge->created_at)->format('Y-m-d H:i:s')}}</td>
                    <td>{{$merge->serial}}</td>
                    <td>
                        @if ($merge->issue_raw)
                            <i class="fa fa-check"></i>
                        @else
                            <i class="fa fa-close"></i>
                        @endif
                    </td>
                    <td>
                        @if ($merge->issue_semi)
                            <i class="fa fa-check"></i>
                        @else
                            <i class="fa fa-close"></i>
                        @endif
                    </td>
                    <td>
                        @if ($merge->merge_status=='created')
                            <button class="btn btn-sm btn-warning">pending</button>
                        @elseif ($merge->merge_status=='finished')
                            <button class="btn btn-sm btn-success">finished</button>
                        @elseif ($merge->merge_status=='approved')
                            <button class="btn btn-sm btn-success">approved</button>
                        @endif
                    </td>
                    <td>
                        @if ($merge->issue_semi==0)
                            <a class="btn btn-sm btn-info" href="{{route('issue.semi',['id'=>$merge->id])}}">Issue
                                Semi</a>
                        @else
                            <a class="btn btn-sm btn-warning text-right"
                               href="{{route('pro.loading',['id'=>$merge->id])}}">Order Status</a>
                            <a class="btn btn-sm btn-danger text-right"
                               href="{{route('production.diff',['id'=>$merge->id])}}">Wastage</a>
                        @endif


                            <a href="{{route('merge.raw',['id'=>$merge->id])}}" target="_blank" class="btn btn-sm btn-primary" style="width:6rem">
                                <i class="fa fa-print" aria-hidden="true"></i>
                                Raw Materials
                            </a>

                    </td>
                </tr>
            @endforeach

            </tbody>
            <tfoot>
            <tr>
                <th>Merge Created Date</th>
                <th>Serial</th>
                <th>Issue Raw</th>
                <th>Issue Semi</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
    </div>
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script>
        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019"
        });
        $('#date_range').val('');
        $(document).ready(function () {
            table = $('#invoice_table').DataTable({
                searching: true,
                'columnDefs': [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                "order": [[0, "desc"]],
                'select': {
                    'style': 'multi'
                },
                dom: 'Bfrtip',
                pageLength: 50,
                responsive: true
            });
        });
    </script>

@stop
