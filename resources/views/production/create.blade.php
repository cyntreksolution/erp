@extends('layouts.back.master')@section('title','Production | Sales order')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .hide {
            display: none;
        }

        .app-main-content {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .portlet-placeholder {
            border: 1px dotted black;
            margin: 0 1em 1em 0;
            height: 100%;
        }

        #projects-task-modal .modal-bg {
            padding: 50px;
        }

        #projects-task-modal .modal-content {
            box-shadow: none;
            border: none
        }

        #projects-task-modal .close {
            border: none;
            outline: none;
            box-shadow: none;
            position: absolute;
            right: 1rem;
            top: 1rem;
            width: 25px;
            height: 25px;
            line-height: 25px;
            text-align: center;
            font-size: 1.13rem;
            border-radius: 500px;
            border: 1px solid #868e96
        }

        #floating-button {
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #db4437;
            position: fixed;
            top: 13rem;
            right: 90px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .plus {
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }


    </style>
@stop
@section('content')
    <div class="app-wrapper">
        <div class="app-panel show" id="app-panel">
            <div class="app-search"><input type="search" class="search-field" placeholder="Search">
                <i class="search-icon fa fa-search"></i>
            </div><!-- /.app-search -->
            <div class="app-panel-inner">
                <div class="scroll-container ps-container ps-theme-default"
                     data-ps-id="dd5b112a-b6dd-36cb-37c4-c6b5e2d1bfdf">

                    <div class="projects-list">

                    </div><!-- /.projects-list -->

                    <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;">
                        <div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;">

                        </div>
                    </div>
                    <div class="ps-scrollbar-y-rail" style="top: 0px; right: 0px;">
                        <div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                    </div>
                </div><!-- /.scroll-container -->
            </div>
            <!-- /.app-panel-inner  --><!-- panel-toggle button -->
            <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show">
                <i class="fa fa-chevron-right"></i> <i class="fa fa-chevron-left"></i></a></div>

        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">SALES ORDERS</h5>
            </div>


            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">
                    <div class="container">
                        {{ Form::open(array('url' => 'so'))}}
                        <div class="media-list">
                            @if (!empty($orderedProducts))
                                @foreach($orderedProducts as $key => $item)
                                    {{--<a href="63/edit">--}}
                                    <div class="media">
                                        <div class="avatar avatar avatar-md project-icon bg-info"
                                             data-plugin="firstLitter" data-target="#47502">R
                                        </div>
                                        <div class="media-body">
                                            <h6 class="project-name"
                                                id="47502">{{$item['product']->name}}</h6>

                                            <span class="text-danger">{{$item['qty']}}</span>


                                        </div>
                                    </div>

                                    {{--</a>--}}
                                @endforeach
                            @endif
                        </div>
                        <input type="submit" class="btn btn-success btn-lg btn-block" value="Request Now">
                        {!!Form::close()!!}
                    </div>
                </div>

            </div>
        </div>
    </div>

@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $('.delete').click(function (e) {
            e.preventDefault();
            id = $(this).attr("value");
            confirmAlert(id);

        });

        function confirmAlert(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "delete",
                        url: id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            }
                            else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

        $(document).ready(function () {
            $("#unit").hide();
            $('#pj-task-1').change(function () {
                if (this.checked)
                    $("#unit").show();
                else
                    $("#unit").hide();

            });
        });

        //        function selectIngrediants(sel) {
        //            $abc = sel.options[sel.selectedIndex].text
        //            $id = sel.options[sel.selectedIndex].value
        //        }

        $("#floating-button").click(function () {

            var a = $("#single option:selected").val();
            var qty = $('#qty').val();

            $.ajax({
                type: "post",
                url: '/erp/end_product/getendpro',
                data: {id: a},

                success: function (response) {

                    $(".abc").append(' <div class=" card-body d-flex  align-items-center p-0" >' +
                        '<div class="d-flex mr-auto">' +
                        '<div>' +
                        '<div class="avatar avatar text-white avatar-md project-icon bg-primary">' +
                        '<img class="card-img-top" src="" alt="">' +
                        '</div>' +
                        '</div>' +
                        '<h5 class="mt-3 mx-3" >' + response['name'] + '</h5>' +
                        '<input type="hidden" name="end_product[]" value=" ' + response['id'] + ' "/>' +
                        '<input type="hidden" name="qty[]" value=" ' + qty + ' "/>' +
                        '</div>' +
                        '<div class="d-flex activity-counters justify-content-between">' +
                        '<div class="text-center px-5">' +
                        '<div class="text-primary"><h6 >' + qty + '</h6>' +
                        '</div>' +
                        '</div>' +
                        '</div> ' +
                        '</div>');
                }
            });

//
//            $("#selecting").append('<a>' +
//                '<div class="media">' +
//                ' <div class="avatar avatar-circle avatar-md project-icon" data-plugin="firstLitter" data-target="#' + $id + '"></div>' +
//                ' <div class="media-body"> ' +
//                ' <h6 class="project-name" id=" ' + $id + ' "> ' + $abc + '</h6>' +
//                ' <small class="project-detail">' + $("#qty").val() + '</small>' +
//                ' </div>' +
//                '</div>' +
//                '<input type="hidden" name="ingredients[]" value=" ' + $id + ' ">' +
//                '<input type="hidden" name="qty[]" value=" ' + $("#qty").val() + ' ">' +
//                '</a>');


            reset();
        });

        function reset() {

            document.getElementById("single").value = "";
            document.getElementById("qty").value = "";
        }

    </script>
@stop
