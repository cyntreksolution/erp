@extends('layouts.back.master')@section('title','Production | Order List')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }
    </style>
@stop

@section('content')

    {{--    @if((Sentinel::check()->roles[0]->slug=='owner') || (Sentinel::check()->roles[0]->slug == 'sales-manager'))--}}
    {{--        <div class="row mb-2">--}}

    {{--            <div class="col-md-2">--}}
    {{--                {!! Form::select('agent',$agents , null , ['class' => 'form-control','placeholder'=>'Select Agent','id'=>'agent']) !!}--}}
    {{--            </div>--}}
    {{--            <div class="col-md-1">--}}
    {{--                {!! Form::select('category',$categories , null , ['class' => 'form-control','placeholder'=>'Select Category','id'=>'category']) !!}--}}
    {{--            </div>--}}
    {{--            <div class="col-md-2">--}}
    {{--                {!! Form::select('day', ['Monday'=>'Monday','Tuesday'=>'Tuesday','Wednesday'=>'Wednesday','Thursday'=>'Thursday','Friday'=>'Friday','Saturday'=>'Saturday','Sunday'=>'Sunday','Long Weekend'=>'Long Weekend','Extra'=>'Extra'] , null , ['class' => 'form-control','placeholder'=>'Select Day','id'=>'day']) !!}--}}
    {{--            </div>--}}
    {{--            <div class="col-md-1">--}}
    {{--                {!! Form::select('time', ['Morning'=>'Morning','Noon'=>'Noon','Evening'=>'Evening'] , null , ['class' => 'form-control','placeholder'=>'Select Time','id'=>'time']) !!}--}}
    {{--            </div>--}}
    {{--            <div class="col-md-2">--}}
    {{--                {!! Form::select('status', [1=>'Pending',2=>'Processing',3=>'Accepting',4=>'Approving',5=>'Approved'] , null , ['class' => 'form-control','placeholder'=>'Select Status','id'=>'status']) !!}--}}
    {{--            </div>--}}
    {{--            <div class="col-md-2">--}}
    {{--                <input type="text" name="date" id="date_range" class="form-control" placeholder="Select Date">--}}
    {{--            </div>--}}

    {{--            <div class="col-md-2">--}}
    {{--                <button class="btn btn-info " onclick="process_form(this)">Filter</button>--}}
    {{--            </div>--}}

    {{--            <!-- loading view -->--}}


    {{--        </div>--}}
    {{--    @endif--}}



    <div class="table-responsive">
        <table id="esr_table" class="display text-center ">
            <thead>
            <tr>
                <th>ID</th>
                <th>Date</th>
                <th>Category</th>
                <th>Day - Time</th>
                <th>Status</th>
                <th>Agent</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @php $approvedFlag =true; @endphp
            @foreach($sales_orders as $order)

                <tr>
                    <td>{{$order->id}}</td>
                    <td>{{$order->created_at}}</td>
                    <td>{{$order->salesOrder->template->category->name}}</td>
                    <td>{{$order->salesOrder->template->day.' - '.$order->salesOrder->template->time}}</td>
                    <td>
                        @switch($order->salesOrder->status)
                            @case(1)
                            <button class="btn btn-sm btn-warning">Pending</button>
                            @php $approvedFlag=false @endphp
                            @break

                            @case(2)
                            <button class='btn btn-sm btn-info'>Processing</button>
                            @php $approvedFlag=false @endphp
                            @break

                            @case(3)
                            <button class='btn btn-sm btn-secondary'>Accepting</button>
                            @php $approvedFlag=false @endphp
                            @break

                            @case(4)
                            <button class='btn btn-sm btn-primary'>Approving</button>
                            @php $approvedFlag=false @endphp
                            @break

                            @case(5)
                            <button class='btn btn-sm btn-success'>Approved</button>
                            @break
                        @endswitch
                    </td>
                    <td>{{$order->salesOrder->salesRep->name_with_initials}}</td>
                    <td></td>

                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th>ID</th>
                <th>Date</th>
                <th>Category</th>
                <th>Day - Time</th>
                <th>Status</th>
                <th>Agent</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>

        <div class="row mt-2">
            <div class="col-md-3">
                <a href="{{route('team.index')}}" target="_blank" class="btn btn-warning btn-sm btn-block">Create
                    Workers Group</a>
            </div>
            <div class="col-md-3">
                <a href="{{route('semi.bulk',['id'=>$sales_merge_header_id])}}" target="_blank"
                   class="btn btn-info btn-sm btn-block">Semi Finish Workers Team</a>
            </div>
            <div class="col-md-2">
                <a href="{{route('end.bulk',['id'=>$sales_merge_header_id])}}" target="_blank"
                   class="btn btn-dark btn-sm btn-block">End Product Workers Team</a>
            </div>

            <div class="col-md-2">
                <a href="{{route('adjust.semi',['id'=>$sales_merge_header_id])}}" target="_blank"
                   class="btn btn-danger btn-sm btn-block">Adjust Semi Finish</a>
            </div>

            <div class="col-md-2">
                @if ($issue_condition)
                    <button class="btn btn-outline-danger btn-sm btn-block" disabled>Check Burden Stages !</button
                @elseif ($approvedFlag)
                    <a href="{{route('production.vs',['id'=>$sales_merge_header_id])}}" target='_blank' }}
                       class="btn btn-success btn-sm btn-block">Production Vs Accepted</a>
                @else
                    <button class="btn btn-outline-danger btn-sm btn-block" disabled>First Approve Orders !</button>
                @endif

            </div>
        </div>
    </div>

@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script>
        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019",
            "maxDate": moment()
        });


        $('#date_range').val('')

        $(document).ready(function () {
            table = $('#esr_table').DataTable({
                searching: true,
                pageLength: 100,
                responsive: true
            });
        });


    </script>

@stop
