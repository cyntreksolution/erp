@extends('layouts.back.master')@section('title','Production | Merged Request List')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }

        .abc {
            width: 1000px;
            height: 1000px;
            /* background-color: black; */
            z-index: 8000;

        }

        .sweet-alert {
            z-index: 7999 !important;
        }

        .sweet-overlay {
            z-index: 7998 !important;
        }

    </style>
@stop

@section('content')

    <div class="">
        <table id="invoice_table" class="display text-center">
            <thead>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>Order Date</th>
                <th>Category</th>
                <th>Day</th>
                <th>Time</th>
                <th>Agent</th>
                <th>Territory</th>
            </tr>
            </thead>
            <tbody>
            @foreach($so_list as $key=> $so)
                <tr>
                    <td>{{++$key}}</td>
                    <td>{{$so->id}}</td>
                    <td>{{\Carbon\Carbon::parse($so->order_date)->format('Y-m-d H:i:s')}}</td>
                    <td>{{$so->template->category->name}}</td>
                    <td>{{$so->template->day}}</td>
                    <td>{{$so->template->time}}</td>
                    <td>{{$so->salesRep->name_with_initials}}</td>
                    <td>{{$so->salesRep->territory}}</td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>Order Date</th>
                <th>Category</th>
                <th>Day</th>
                <th>Time</th>
                <th>Agent</th>
                <th>Territory</th>
            </tr>
            </tfoot>
        </table>

    </div>
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>

    <script>
        $(document).ready(function () {
            table = $('#invoice_table').DataTable({
                searching: true,
                'columnDefs': [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                'select': {
                    'style': 'multi'
                },
                dom: 'Bfrtip',
                pageLength: 10,
                responsive: true
            });
        });
    </script>

@stop
