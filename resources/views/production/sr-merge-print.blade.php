<html>
<head>
    <title>Production | Sales Order Merge Preview</title>
    <link rel="stylesheet" href="{{asset('assets/vendor/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }

        .lead {
            font-size: 18px;
            font-weight: 800;
            margin: 10px;
        }

        caption {
            caption-side: top;
            margin: 10px 0 0 0;
            padding: 0;
        }

        table.dataTable tbody td {
            padding: 3px !important;
        }

        table.dataTable thead td {
            padding: 3px !important;;
        }

        /*table > tbody > tr:last-child {*/
        /*    color: white;*/
        /*    background: grey;*/
        /*}*/
    </style>
</head>

<body>

<div class="col-sm-12">

    <div class="col-md-12">
        @include('SalesRequestManager::sales_request.sr-merge-print-table')
    </div>
    <div class="col-md-12 text-center mt-5 mb-5">
        <div class="row">
            <div class="col-md-2">
                <a href="{{url()->previous() }}" class="btn btn-block btn-danger">Back</a>
            </div>
            @if($category->loading_type == 'to_burden')
                <div class="col-md-8">
                    {!! Form::open(['action'=>['App\Http\Controllers\ProductionController@salesRequestsToBurden'],'method'=>'post']) !!}
                    <input type="hidden" value="{{$mr->serial}}" name="merge_serial">
                    <input type="hidden" value="{{implode(',',$orders)}}" name="order">
                    <button type="submit" onclick="this.form.submit();this.disabled = true;" class="btn btn-block btn-success">To Burden</button>
                    {!! Form::close() !!}
                </div>
            @endif
            <div class="col-md-2">
                {!! Form::open(['action'=>['SalesRequestManager\Controllers\SalesRequestController@salesReportExcelExport','orders'=>implode(',',$orders)],'method'=>'post']) !!}
                <button type="submit" class="btn btn-block btn-primary">Download Excel</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>


<script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/popper.js/dist/umd/popper.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>

<script>
    $(document).ready(function () {
        table = $('.table').DataTable({
            searching: false,
            paging: false,
            info: false,
            responsive: true,
            ordering: false,
            order: [1, 'asc'],
        });
    });
</script>


</body>

</html>
