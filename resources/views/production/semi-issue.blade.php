@extends('layouts.back.master')@section('title','Production | Semi Issue')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    {{--    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">--}}
    {{--    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/css/dataTables.checkboxes.css" rel="stylesheet"/>--}}

    <style>
        th {
            text-align: right !important;
        }

        td {
            text-align: right !important;
        }

        .abc {
            width: 1000px;
            height: 1000px;
            /* background-color: black; */
            z-index: 8000;

        }

        .sweet-alert {
            z-index: 7999 !important;
        }

        .sweet-overlay {
            z-index: 7998 !important;
        }

    </style>
@stop

@section('content')

    <div id="wait" class="d-none abc" style=""><img src='{{asset('assets/img/4V0b.gif')}}' width="64" height="64"/><br>Loading..
    </div>

    {{--    <div class="app-main-content mb-2">--}}
    {{--        <div class="row mx-2">--}}
    {{--            <div class="col-md-2">--}}
    {{--                {!! Form::select('agent',$agents , null , ['class' => 'form-control','placeholder'=>'Select Agent','id'=>'agent']) !!}--}}
    {{--            </div>--}}
    {{--            <div class="col-md-2">--}}
    {{--                {!! Form::select('category',$categories , null , ['class' => 'form-control','placeholder'=>'Select Category','id'=>'category']) !!}--}}
    {{--            </div>--}}
    {{--            <div class="col-md-2">--}}
    {{--                {!! Form::select('day', ['Monday'=>'Monday','Tuesday'=>'Tuesday','Wednesday'=>'Wednesday','Thursday'=>'Thursday','Friday'=>'Friday','Saturday'=>'Saturday','Sunday'=>'Sunday','Long Weekend'=>'Long Weekend','Extra'=>'Extra'] , null , ['class' => 'form-control','placeholder'=>'Select Day','id'=>'day']) !!}--}}
    {{--            </div>--}}
    {{--            <div class="col-md-2">--}}
    {{--                {!! Form::select('time', ['Morning'=>'Morning','Noon'=>'Noon','Evening'=>'Evening'] , null , ['class' => 'form-control','placeholder'=>'Select Time','id'=>'time']) !!}--}}
    {{--            </div>--}}
    {{--            <div class="col-md-2">--}}
    {{--                <input type="text" name="date" id="date" class="form-control" placeholder="Select Date">--}}
    {{--            </div>--}}
    {{--            <div class="col-md-2">--}}
    {{--                <button class="btn btn-info " onclick="process_form(this)">Filter</button>--}}
    {{--            </div>--}}

    {{--            <!-- loading view -->--}}
    {{--        </div>--}}
    {{--    </div>--}}

    {{ Form::open(array('url' => '/production/semi/issue/store','id'=>'mergerForm','method'=>'POST'))}}
    <input type="hidden" name="merge_serial" value="{{$data['burden_id']}}">
    <div class="">
        <table id="invoice_table" class="display text-center">
            <thead>
            <tr>
                <th>Semi finish product</th>
                <th>Available QTY</th>
                <th>Expected QTY</th>
                <th>Requested QTY</th>
                <th>Requested Burden Size</th>
                <th>Issued QTY</th>
                <th>Action</th>

            </tr>
            </thead>
            <tbody>

            @php $flag =false @endphp
            @foreach($data['items'] as  $item)
                <tr>
                    <td>{{$item['semi_name']}}</td>
                    <td>{{number_format($item['available_qty'],3)}}</td>
                    <td>{{number_format($item['qty'],3)}}</td>
                    <td>
                        {{!empty($item['requested_qty'])?number_format($item['requested_qty'],2):null}}
                    </td>
                    <td>
                        {{!empty($item['requested_qty'])?number_format($item['burden_size'],2):null}}
                    </td>
                    <td>

                        @php
                            $round_qty =$item['qty'];
$semi_p = \SemiFinishProductManager\Models\SemiFinishProduct::find($item['id']);
                                if ($semi_p->is_fractional==0){
                                    $round_qty = ceil($item['qty']);
                                }

                        @endphp
                        <input type="hidden" name="semi[]" value="{{$item['id']}}">
                        <input type="hidden" name="expected[]" value="{{$item['qty']}}">
                        <input type="number" readonly name="qty[]" autocomplete="off"
                               class="form-control text-right w-100" step="0.001"
                               max="{{number_format($item['available_qty'],3,'.','')}}"
                               min="{{number_format($item['qty'],3,'.','')}}"
                               value="{{number_format($round_qty,3,'.','')}}">
                    </td>
                    <td>

                        @if (number_format($item['available_qty'],3,'.','') < number_format($item['qty'],3,'.',''))
                            @php $flag =true; @endphp

                            @if ($item['is_bake']==1)
                                @php $active_burdens = \BurdenManager\Models\Burden::whereSemiFinishProductId($item['id'])->whereStatus(1)->get() ; @endphp
                                @php $active_burdens_issued = \BurdenManager\Models\Burden::whereSemiFinishProductId($item['id'])->whereStatus(2)->get() ; @endphp

                                @if (!empty($active_burdens) && $active_burdens->count()>0)
                                    <button type="button"
                                            class="btn btn-info btn-sm">Pending Raw Material
                                    </button>
                                @elseif (!empty($active_burdens_issued) && $active_burdens_issued->count()>0)
                                    <button type="button"
                                            class="btn btn-danger btn-sm"
                                            onclick="makeSemiFinish({{$item['id']}},{{$item['is_bake']}},{{$data['burden_id']}})"> Make Semi
                                        Finish
                                    </button>
                                @else
                                    <button type="button"
                                            onclick="addSemiFinishBurden({{$item['id']}},{{$item['burden_size']}},{{$item['is_bake']}})"
                                            class="btn btn-warning btn-sm">Make Burden
                                    </button>
                                @endif


                            @else
                                @php $active_burdens = \ShortEatsManager\Models\CookingRequest::whereSemiFinishProductId($item['id'])->whereStatus(1)->get() ; @endphp
                                @php $active_burdens_issued = \ShortEatsManager\Models\CookingRequest::whereSemiFinishProductId($item['id'])->whereStatus(2)->get() ; @endphp

                                @if (!empty($active_burdens) && $active_burdens->count()>0)
                                    <button type="button"
                                            class="btn btn-info btn-sm">Pending Raw Material
                                    </button>
                                @elseif (!empty($active_burdens_issued) && $active_burdens_issued->count()>0)
                                    <button type="button"
                                            class="btn btn-danger btn-sm"
                                            onclick="makeSemiFinish({{$item['id']}},{{$item['is_bake']}},{{$data['burden_id']}})"> Make Cooking
                                        Request
                                        Finish
                                    </button>
                                @else
                                    <button type="button"
                                            onclick="addSemiFinishBurden({{$item['id']}},{{$item['burden_size']}},{{$item['is_bake']}})"
                                            class="btn btn-warning btn-sm">Create Cooking Request
                                    </button>
                                @endif


                            @endif
                        @endif
                    </td>


                </tr>
            @endforeach

            </tbody>
            <tfoot>
            <tr>
                <th>Semi finish product</th>
                <th>Available QTY</th>
                <th>Expected QTY</th>
                <th>Requested QTY</th>
                <th>Requested Burden Size</th>
                <th>Issued QTY</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
        <div class="row">

          @php  $isin = \EndProductBurdenManager\Models\EndProductBurdenHeader::whereMergeStatus('created')->whereIssueRaw(1)->whereIssueSemi(1)->get(); @endphp

            @if ($data['raw_issue'])
                @if (!$flag)
                    @if(empty($isin[0]->id))
                        <div class="col-md-12 mt-5 ">
                            <button type="submit" name="submit" onclick="this.form.submit();this.disabled = true;"
                                    class="btn btn-block btn-warning">Issue Semi Finish
                            </button>
                        </div>
                    @else

                        @if(Sentinel::check()->roles[0]->slug=='owner')
                            <div class="col-md-12 mt-5 ">
                                <button type="submit" name="submit" onclick="this.form.submit();this.disabled = true;"
                                        class="btn btn-block btn-warning">Issue Semi Finish
                                </button>
                            </div>
                        @else

                        <div class="col-md-12 mt-5 ">
                            <button type="button" class="btn btn-block btn-danger">Previous Merge Burdens Still Have to Finish.....</button>
                        </div>
                        @endif
                    @endif


                @endif
            @else
                <div class="col-md-12 mt-5 ">
                    <button type="button" class="btn btn-block btn-danger">Raw Material Not Issued Yet ! Merge Serial
                        : {{$data['serial']}}</button>
                </div>
            @endif

        </div>
        {{ Form::close() }}
    </div>

    <div class="modal fade" id="ChangeQty" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(['url'=>'','id'=>'qtyChangeForm']) }}
                <input type="hidden" value="{{$data['burden_id']}}" name="mid">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">

                        <div class="row task-name-wrap">

                            <div class="col-md-6">
                                <span>Quantity</span>
                            </div>
                            <div class="col-md-6">
                                <select name="qty" id="qty" class="form-control" required>
                                </select>
                                {{--                                <input name="qty" type="number" id="qty" step="0.01" class="task-name-field" value="">--}}
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="Create Burden">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>--}}
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    {{--    <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>--}}
    {{--    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js"></script>--}}



    <script>
        $(document).ready(function () {
            table = $('#invoice_table').DataTable({
                searching: true,
                'columnDefs': [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                'select': {
                    'style': 'multi'
                },
                dom: 'Bfrtip',
                pageLength: 250,
                responsive: true
            });
        });

        function addSemiFinishBurden(id, qty, type) {
            if (type == 1) {
                $("#qtyChangeForm").attr('action', '/production/make/semi/' + id);
            } else {
                $("#qtyChangeForm").attr('action', '/production/make/cooking/' + id);
            }

            populateBurdenSizes(id);


            $('#ChangeQty').modal('show')
        }

        function makeSemiFinish(id, type, mid) {
            let url = null;
            if (type == 1) {
                url = '/production/create/semi/' + id + '?mid=' + mid;
            } else {
                url = '/production/create/cooking/' + id + '?mid=' + mid;
            }
            swal({
                    title: "Are you sure?",
                    text: "Merge Serial Number is :" + mid,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, Make Semi Finish!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "post",
                        url: url,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Created !", "Semi finish created successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            } else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );
        }

        function populateBurdenSizes(semi) {
            $.ajax({
                url: "/get/semi/burden/size/" + semi,
                type: "GET",
                success: function (data) {
                    var $dropdown = $("#qty");
                    $dropdown.empty();
                    $.each(data, function (i, k) {
                        $dropdown.append($("<option />").val(i).text(k));
                    });
                },
                error: function (xhr, status) {

                }
            });
        }
    </script>

@stop
