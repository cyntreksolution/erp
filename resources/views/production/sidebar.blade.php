
<div class="profile-section-user">
    <div class="profile-cover-img profile-pic">
        <img src="{{asset('assets\img\semi2.jpg')}}" alt="">
    </div>
    <div class="profile-info-brief p-3">
        @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
        <div class="user-profile-avatar avatar avatar-circle avatar-md project-iconr bg-{{$color[$burden->colour]}}"
             data-plugin="firstLitter"
             data-target="#{{$burden->id*754}}"></div>

        <div class="text-center">
            <h5 class="text-uppercase ">{{$burden->serial}}</h5>
            <h5 class="text-uppercase mb-1" id="{{$burden->id*754}}">{{$burden['endProdcuts']->name}}</h5>
            <h6 class="text mb-lg-1">{{'Burden Size : '.$burden->burden_size}} </h6>
            <br>
        </div>

        <hr class="m-0">

        <div class="py-3">
            {{'Active Recipe : '.$burden['recipe']->recipe_id}}<br>
            <hr class="m-1">
            {{'Expected Quantity : '.$burden->expected_semi_product_qty}} units<br>

            @php
                $g_count = \BurdenManager\Models\BurdenGrade::whereBurdenId($burden->id)->sum('burden_qty');
                $g_availble_count = \BurdenManager\Models\BurdenGrade::whereBurdenId($burden->id)->sum('used_qty');
                $g_used_count = $g_count-$g_availble_count;
            @endphp
            {{'Graded Quantity : '.$g_count}} <br>
            {{'Used Quantity : '.$g_used_count}} <br>
            {{'Available Quantity : '.$g_availble_count}} <br>


            <hr class="m-1">
            <b class="font-weight-bold">First Stage
                - {{!empty($burden->employeeGroup) && $burden->employeeGroup->count()>0 ?$burden->employeeGroup[0]->name:'-'}} </b>
            @if($role = Sentinel::check()->roles[0]->slug=='owner')
                <i data-toggle="modal" data-target="#exampleModal2"
                   class="fa fa-pencil float-right text-right"></i>
            @endif
            <br>

            @if (!empty($burden->employeeGroup))
                @foreach ($burden->employeeGroup as $gr)
                    @foreach ($gr->employees as $user)
                        <small class="text-left">{{$user->full_name}}</small><br>
                    @endforeach
                @endforeach
            @endif

            <hr class="m-1">
            <b class="font-weight-bold">Second Stage

                <?php $bakers=$burdenClass->getBakers($burden->baker);
                ?>

                - {{!empty($bakers) && $bakers->count()>0 ?$bakers[0]->name:'-'}} </b>
            @if($role = Sentinel::check()->roles[0]->slug=='owner')
                <i data-toggle="modal" data-target="#exampleModal"
                   class="fa fa-pencil float-right text-right"></i>
            @endif
            <br>

            @if (!empty($bakers))
                @foreach ($bakers as $gr)
                    @foreach ($gr->employees as $user)
                        <small class="text-left">{{$user->full_name}}</small><br>
                    @endforeach
                @endforeach
            @endif

        </div>
        <hr class="m-0">

        <div class="py-3">
            <b class="font-weight-bold">Responsible Stage

                @if ($burden->responce === 0)
                    {!! Form::open(['route' => ['update-first-stage'], 'method' => 'post']) !!}
                    <input type="hidden" name="burden_id" value="{{$burden->id}}">
                    <button type="submit" class="btn btn-warning">First Stage</button>
                    {!! Form::close() !!}
                @else
                    {!! Form::open(['route' => ['update-second-stage'], 'method' => 'post']) !!}
                    <input type="hidden" name="burden_id" value="{{$burden->id}}">
                    <button type="submit" class="btn btn-danger">Second Stage</button>
            {!! Form::close() !!}
            @endif
        </div>
    </div>
</div>
