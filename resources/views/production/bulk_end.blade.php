@extends('layouts.back.master')@section('title','Production | Bulk Burden Create')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }
    </style>
@stop

@section('content')
    {{ Form::close() }}
    {!! Form::open(['route' => 'production_end.store', 'method' => 'post']) !!}
    <div class="">
        <table id="esr_table" class="display text-center">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Serial</th>
                <th>Size</th>
                <th>Expected Qty</th>
                <th>Raw Material Issued</th>
                <th>Semi Finish Issued</th>
                <th>Stage 1</th>
                <th>Stage 2</th>
            </tr>
            </thead>
            <tbody>
            @foreach($end_burdens  as $record)
                @php
                  $flag =  $record->stage_1 && $record->stage_2;
                @endphp
                <tr>
                    <td>
                        @if (!$flag)
                            @if($record->issue_raw && $record->issue_semi)
                                <input type='checkbox' name='id[]' value='{{$record->id}}'/>
                            @endif
                        @endif
                    </td>
                    <td>{{$record->endProduct->name}}</td>
                    <td>{{$record->serial}}</td>
                    <td>{{$record->burden_size}}</td>
                    <td>{{$record->expected_end_product_qty}}</td>
                    <td>
                        @if($record->issue_raw)
                            <i class='fa fa-check text-center'/>
                        @else
                            <i class='text-center fa fa-close'/>
                        @endif
                    </td>
                    <td>
                        @if($record->issue_semi)
                            <i class='fa fa-check text-center'/>
                        @else
                            <i class='text-center fa fa-close'/>
                        @endif
                    </td>
                    <td>
                        @if($record->stage_1)
                            <i class='text-success fa fa-check text-center'/>
                        @else
                            <i class='text-danger text-center fa fa-close'/>
                        @endif
                    </td>
                    <td>
                        @if($record->stage_2)
                            <i class='text-success fa fa-check text-center'/>
                        @else
                            <i class='text-danger text-center fa fa-close'/>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Serial</th>
                <th>Size</th>
                <th>Expected Qty</th>
                <th>Raw Material Issued</th>
                <th>Semi Finish Issued</th>
                <th>Stage 1</th>
                <th>Stage 2</th>
            </tr>
            </tfoot>
        </table>
        <div class="row">
            <div class="col-md-6">
                <select class="form-control mt-3" name="team" placeholder="Select Team">
                    @foreach($teams as $team)
                        <option value="{{$team->id}}">{{ str_replace(str_split('\\/:*?"<>|[]'),'',$team->employees->pluck('full_name')) }}</option>
                    @endforeach
                        @foreach($employees as $employee)
                            <option value="{{$employee->id}}">{{ str_replace(str_split('\\/:*?"<>|[]'),'',$employee->employees->pluck('full_name')) }}</option>
                        @endforeach
                </select>
            </div>
            <div class="col-md-6">
                <button class="btn btn-block btn-success m-3" name="type" value="make" onclick="confirmAlert()">MAKE BURDEN</button>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                {{--!! Form::select('employee', $employees , null , ['class' => 'form-control mt-3','placeholder'=>'Select Employee']) !!--}}
                <select class="form-control mt-3" name="employee" placeholder="Select Team">
                    @foreach($teams as $team)
                        <option value="{{$team->id}}">{{ str_replace(str_split('\\/:*?"<>|[]'),'',$team->employees->pluck('full_name')) }}</option>
                    @endforeach
                        @foreach($employees as $employee)
                            <option value="{{$employee->id}}">{{ str_replace(str_split('\\/:*?"<>|[]'),'',$employee->employees->pluck('full_name')) }}</option>
                        @endforeach
                </select>
            </div>
            <div class="col-md-6">
                <button class="btn btn-block btn-warning m-3" name="type" value="bake" onclick="confirmAlert()">BAKE BURDEN</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script>
        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": false,
            "singleDatePicker": true,
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019"
        });
        $('#date_range').val('')

        $(document).ready(function () {
            table = $('#esr_table').DataTable({
                searching: true,
                pageLength: 250,
                responsive: true
            });
        });


    </script>

@stop