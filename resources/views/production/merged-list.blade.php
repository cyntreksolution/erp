@extends('layouts.back.master')@section('title','Production |Merged Sales Request')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
    {{--    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/css/dataTables.checkboxes.css" rel="stylesheet"/>--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }

        .abc {
            width: 1000px;
            height: 1000px;
            /* background-color: black; */
            z-index: 8000;

        }

        .sweet-alert {
            z-index: 7999 !important;
        }

        .sweet-overlay {
            z-index: 7998 !important;
        }

    </style>
@stop

@section('content')

    <div id="wait" class="d-none abc" style=""><img src='{{asset('assets/img/4V0b.gif')}}' width="64" height="64"/><br>Loading..
    </div>

    {{--    <div class="app-main-content mb-2">--}}
    {{--        <div class="row mx-2">--}}
    {{--            <div class="col-md-2">--}}
    {{--                {!! Form::select('agent',$agents , null , ['class' => 'form-control','placeholder'=>'Select Agent','id'=>'agent']) !!}--}}
    {{--            </div>--}}
    {{--            <div class="col-md-2">--}}
    {{--                {!! Form::select('category',$categories , null , ['class' => 'form-control','placeholder'=>'Select Category','id'=>'category']) !!}--}}
    {{--            </div>--}}
    {{--            <div class="col-md-2">--}}
    {{--                {!! Form::select('day', ['Monday'=>'Monday','Tuesday'=>'Tuesday','Wednesday'=>'Wednesday','Thursday'=>'Thursday','Friday'=>'Friday','Saturday'=>'Saturday','Sunday'=>'Sunday','Long Weekend'=>'Long Weekend','Extra'=>'Extra'] , null , ['class' => 'form-control','placeholder'=>'Select Day','id'=>'day']) !!}--}}
    {{--            </div>--}}
    {{--            <div class="col-md-2">--}}
    {{--                {!! Form::select('time', ['Morning'=>'Morning','Noon'=>'Noon','Evening'=>'Evening'] , null , ['class' => 'form-control','placeholder'=>'Select Time','id'=>'time']) !!}--}}
    {{--            </div>--}}
    {{--            <div class="col-md-2">--}}
    {{--                <input type="text" name="date" id="date" class="form-control" placeholder="Select Date">--}}
    {{--            </div>--}}
    {{--            <div class="col-md-2">--}}
    {{--                <button class="btn btn-info " onclick="process_form(this)">Filter</button>--}}
    {{--            </div>--}}

    {{--            <!-- loading view -->--}}
    {{--        </div>--}}
    {{--    </div>--}}

    {{ Form::open(array('url' => '/production/sr/end/burden/list','id'=>'mergerForm','method'=>'GET'))}}
    <div class="">
        <table id="invoice_table" class="display text-center">
            <thead>
            <tr>
                <th>#</th>
                <th>Merge Created Date</th>
                <th>Serial</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>

            @foreach($merged as$key=> $merge)
                <tr>
                    <td><input type="checkbox" name="merge_ids[{{$merge->id}}]"></td>
                    <td>{{\Carbon\Carbon::parse($merge->created_at)->format('Y-m-d H:i:s')}}</td>
                    <td>{{$merge->serial}}</td>
                    <td>
                        @can('production_manager-mergedSalesRequestList')
                        <a href="{{route('merged.list',['id'=>$merge->id])}}" class="btn btn-sm btn-success mr-2"
                           target="_blank">Merged Requests</a>
                        @endcan
{{--                        @if($role = Sentinel::check()->roles[0]->slug=='owner')--}}
                            @can('production_manager-mergedSalesHeaderDelete')
                            <a href="{{route('merged.delete',['id'=>$merge->id])}}" class="btn btn-sm btn-danger mr-2"
                            ><i class="fa fa-recycle"></i> </a>
                            @endcan
{{--                        @endif--}}

                    </td>
                </tr>
            @endforeach

            </tbody>
            <tfoot>
            <tr>
                <th>#</th>
                <th>Merge Created Date</th>
                <th>Serial</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
        @can('production_manager-endBurdenList')
        <div class="row">
            <div class="col-md-12 mt-5 ">
                <button type="submit" name="submit" onclick="this.form.submit();this.disabled = true;"
                        class="btn btn-block btn-warning">Next
                </button>
            </div>
        </div>
        @endcan
        {{ Form::close() }}
    </div>
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    {{--    <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>--}}
    {{--    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js"></script>--}}



    <script>
        $(document).ready(function () {
            table = $('#invoice_table').DataTable({
                searching: true,
                'columnDefs': [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                'select': {
                    'style': 'multi'
                },
                dom: 'Bfrtip',
                pageLength: 30,
                responsive: true,
                "order": [[1, "desc"]]
            });
        });
    </script>

@stop
