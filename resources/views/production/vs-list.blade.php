@extends('layouts.back.master')@section('title','Production Vs Accepted')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/datatables.net/css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/datatables.net/css/buttons.dataTables.min.css')}}">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        th {
            text-align: right !important;
        }

        td {
            text-align: right !important;
        }

        .abc {
            width: 1000px;
            height: 1000px;
            /* background-color: black; */
            z-index: 8000;

        }

        .sweet-alert {
            z-index: 7999 !important;
        }

        .sweet-overlay {
            z-index: 7998 !important;
        }

    </style>
@stop

@section('content')

    <div id="wait" class="d-none abc" style=""><img src='{{asset('assets/img/4V0b.gif')}}' width="64" height="64"/><br>Loading..
    </div>

    {{--    <div class="app-main-content mb-2">--}}
    {{--        <div class="row mx-2">--}}
    {{--            <div class="col-md-2">--}}
    {{--                {!! Form::select('agent',$agents , null , ['class' => 'form-control','placeholder'=>'Select Agent','id'=>'agent']) !!}--}}
    {{--            </div>--}}
    {{--            <div class="col-md-2">--}}
    {{--                {!! Form::select('category',$categories , null , ['class' => 'form-control','placeholder'=>'Select Category','id'=>'category']) !!}--}}
    {{--            </div>--}}
    {{--            <div class="col-md-2">--}}
    {{--                {!! Form::select('day', ['Monday'=>'Monday','Tuesday'=>'Tuesday','Wednesday'=>'Wednesday','Thursday'=>'Thursday','Friday'=>'Friday','Saturday'=>'Saturday','Sunday'=>'Sunday','Long Weekend'=>'Long Weekend','Extra'=>'Extra'] , null , ['class' => 'form-control','placeholder'=>'Select Day','id'=>'day']) !!}--}}
    {{--            </div>--}}
    {{--            <div class="col-md-2">--}}
    {{--                {!! Form::select('time', ['Morning'=>'Morning','Noon'=>'Noon','Evening'=>'Evening'] , null , ['class' => 'form-control','placeholder'=>'Select Time','id'=>'time']) !!}--}}
    {{--            </div>--}}
    {{--            <div class="col-md-2">--}}
    {{--                <input type="text" name="date" id="date" class="form-control" placeholder="Select Date">--}}
    {{--            </div>--}}
    {{--            <div class="col-md-2">--}}
    {{--                <button class="btn btn-info " onclick="process_form(this)">Filter</button>--}}
    {{--            </div>--}}

    {{--            <!-- loading view -->--}}
    {{--        </div>--}}
    {{--    </div>--}}

    <div class="">
        {!! Form::open(['route' => 'production.vss', 'method' => 'post']) !!}

        <table id="invoice_table" class="display text-center">
            <thead>
            <tr>
                <th class="">End Product</th>
                <th class="">Sold QTY</th>
                <th class="">Approved QTY</th>
                <th class="">Production QTY</th>
                <th class="">Difference QTY</th>
                <th class="">Grade A</th>
                <th class="">Grade B</th>
                <th class="">Grade C</th>
                <th class="">Grade D</th>
                <th class="">Grade D Value</th>
                <th class="">Difference Value</th>
                <th class="">Grade C QTY</th>
            </tr>
            </thead>
            <tbody>
            @php  $merged = \EndProductBurdenManager\Models\EndProductBurdenHeader::find($end_product_burden_header); @endphp

            @php

                    $loading_qty =0;
                    $req_qty =0;
                    $production_qty =0;
                    $dif_qty =0;
                    $dif_value =0;
                    $g2 =0;
                    $g3 =0;
                    $g4 =0;
                    $g4_v=0;
            @endphp
            @foreach($loadingData as $key=> $loading_item)
                @foreach($end_pro as $key2=> $production_item)
                    @if ($loading_item->end_product_id == $production_item['end_product_id'])
                        <tr>
                            <td>{{$loading_item->endProduct->name}}</td>
                            <td>{{$loading_item->requested_qty}}</td>
                            <td>{{$loading_item->qty}}</td>
                            <td>{{$production_item['qty'] + $production_item['grade_2_qty'] + $production_item['grade_3_qty'] +$production_item['grade_4_qty']}}</td>
                            @php
                            $np =$production_item['qty'] + $production_item['grade_2_qty'] + $production_item['grade_3_qty'] +$production_item['grade_4_qty'];
                            $dif = $np - $loading_item->qty ;
                                //$dif =  $production_item['qty'] -$loading_item->qty ;
                                $clz = "";
                            if ($dif>0){
                                $clz ="text-danger";
                            }elseif($dif==0){
                                 $clz ="text-success";
                            }
                            @endphp
                            <td class="{{$clz}}">{{$dif}}</td>
                            <td>{{$production_item['qty']}}</td>
                            <td>{{$production_item['grade_2_qty']}}</td>
                            <td>{{$production_item['grade_3_qty']}}</td>
                            <td>{{$production_item['grade_4_qty']}}</td>
                            <td>{{number_format($production_item['grade_4_qty'] * $loading_item->endProduct->selling_price,2) }}</td>
                            <td class="{{$clz}}">{{number_format($dif * $loading_item->endProduct->selling_price,2) }}</td>
                            <td>
                                @if (!$merged->merge_status =='finished')
                                    @if ($loading_item->endProduct->reusable_type >1)
                                        <input type="number" step="0.001" min="0.00" class="form-control"
                                               name="grade_c[{{$loading_item->end_product_id}}]" value="0">
                                    @endif
                                @endif

                            </td>

                            <input type="hidden" name="end_product[]" value="{{$loading_item->end_product_id}}">
                            <input type="hidden" name="burden_id[]" value="{{$production_item['burden_id']}}">
                            <input type="hidden" name="grade_id[]" value="{{$production_item['grade_id']}}">
                            <input type="hidden" name="difference[]" value="{{$dif}}">
                            <input type="hidden" name="end_product_burden_header"
                                   value="{{$end_product_burden_header}}">

                        </tr>


                        @php
                            $g2 = $g2+$production_item['grade_2_qty'];
                            $g3 = $g3+$production_item['grade_3_qty'];
                            $g4 = $g4+$production_item['grade_4_qty'];
                                $loading_qty = $loading_qty + $loading_item->qty ;
                                $production_qty = $production_qty + $production_item['qty'];
                                $dif_qty = $dif_qty + $dif;
                                $req_qty = $req_qty + $loading_item->requested_qty;
                                $dif_value = $dif_value + ($dif * $loading_item->endProduct->selling_price);
                                $g4_v =$g4_v+$production_item['grade_4_qty'] * $loading_item->endProduct->selling_price;
                        @endphp
                    @endif
                @endforeach
            @endforeach
            <tr>
                <td>Total</td>
                <td>{{$req_qty}}</td>
                <td>{{$loading_qty}}</td>
                <td>{{$production_qty+$g2+$g3+$g4}}</td>
                @php
                    $clz = "";
                if ($dif_qty>0){
                    $clz ="text-danger";
                }elseif($dif_qty==0){
                     $clz ="text-success";
                }
                @endphp
                <td class="{{$clz}}">{{$dif_qty}}</td>
                <td>{{$production_qty}}</td>
                <td>{{$g2}}</td>
                <td>{{$g3}}</td>
                <td>{{$g4}}</td>
                <td>{{$g4_v}}</td>
                <td class="{{$clz}}">{{number_format($dif_value,2) }}</td>
                <td>

                </td>
            </tr>

            </tbody>
            <tfoot>
            <tr>
                <th class="">End Product</th>
                <th class="">Sold QTY</th>
                <th class="">Approved QTY</th>
                <th class="">Production QTY</th>
                <th class="">Difference QTY</th>
                <th class="">Grade A</th>
                <th class="">Grade B</th>
                <th class="">Grade C</th>
                <th class="">Grade D</th>
                <th class="">Grade D Value</th>
                <th class="">Difference Value</th>
                <th class="">Grade C QTY</th>
            </tr>
            </tfoot>
        </table>

        <div class="col-md-12 mt-3">
            @if ($merged->merge_status =='finished')
                @if (Sentinel::check()->roles[0]->slug == 'owner' ||  Sentinel::check()->roles[0]->slug == 'assistant')
                    <a href="{{route('production.approve',['id'=>$end_product_burden_header])}}"
                       class="btn btn-block btn-warning">Approve Admin</a>
                @endif
            @endif
            @if ($merged->merge_status =='created')
                @if ($returnNoteCount > 0)
                    <button class="btn btn-block btn-danger">Please Approve System Generated Return Notes First</button>
                @else
                    <button class="btn btn-block btn-success">Next</button>
                @endif

            @endif
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>

    <script>
        $(document).ready(function () {
            table = $('#invoice_table').DataTable({
                searching: true,
                dom: 'Bfrtip',
                buttons: [
                    'pdf'
                ],
                "pagingType": "full_numbers",
                "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
                "ordering": false,
                pageLength: 250,
                responsive: true
            });
        });
    </script>

@stop
