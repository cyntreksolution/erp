@extends('layouts.back.master')@section('title','Production | Wastage')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }

        .abc {
            width: 1000px;
            height: 1000px;
            /* background-color: black; */
            z-index: 8000;

        }

        .sweet-alert {
            z-index: 7999 !important;
        }

        .sweet-overlay {
            z-index: 7998 !important;
        }

    </style>
@stop

@section('content')

    <div id="wait" class="d-none abc" style=""><img src='{{asset('assets/img/4V0b.gif')}}' width="64" height="64"/><br>Loading..
    </div>

    {{--    <div class="app-main-content mb-2">--}}
    {{--        <div class="row mx-2">--}}
    {{--            <div class="col-md-2">--}}
    {{--                {!! Form::select('agent',$agents , null , ['class' => 'form-control','placeholder'=>'Select Agent','id'=>'agent']) !!}--}}
    {{--            </div>--}}
    {{--            <div class="col-md-2">--}}
    {{--                {!! Form::select('category',$categories , null , ['class' => 'form-control','placeholder'=>'Select Category','id'=>'category']) !!}--}}
    {{--            </div>--}}
    {{--            <div class="col-md-2">--}}
    {{--                {!! Form::select('day', ['Monday'=>'Monday','Tuesday'=>'Tuesday','Wednesday'=>'Wednesday','Thursday'=>'Thursday','Friday'=>'Friday','Saturday'=>'Saturday','Sunday'=>'Sunday','Long Weekend'=>'Long Weekend','Extra'=>'Extra'] , null , ['class' => 'form-control','placeholder'=>'Select Day','id'=>'day']) !!}--}}
    {{--            </div>--}}
    {{--            <div class="col-md-2">--}}
    {{--                {!! Form::select('time', ['Morning'=>'Morning','Noon'=>'Noon','Evening'=>'Evening'] , null , ['class' => 'form-control','placeholder'=>'Select Time','id'=>'time']) !!}--}}
    {{--            </div>--}}
    {{--            <div class="col-md-2">--}}
    {{--                <input type="text" name="date" id="date" class="form-control" placeholder="Select Date">--}}
    {{--            </div>--}}
    {{--            <div class="col-md-2">--}}
    {{--                <button class="btn btn-info " onclick="process_form(this)">Filter</button>--}}
    {{--            </div>--}}

    {{--            <!-- loading view -->--}}
    {{--        </div>--}}
    {{--    </div>--}}

    <div class="">
        <table id="invoice_table" class="display text-center">
            <thead>
            <tr>
                <th>Semi Finish Product</th>
                <th class="text-right">Wastage Volume</th>
                <th class="text-right">Price </th>
                <th class="text-center">Action</th>
            </tr>
            </thead>
            <tbody>
            @php
                $semi_vol = 0;
                $semi_tot = 0;
            @endphp


            @foreach($data_b as$key=> $mergeB)
                @if ( $mergeB->qty != 0 )

                    @php

                        $semi_name = \SemiFinishProductManager\Models\SemiFinishProduct::where('semi_finish_product_id', '=', $mergeB->semi_id)->first();

                        $semi_vol = $semi_vol+$mergeB->qty;
                        $semi_tot = $semi_tot+$mergeB->amount;
                    @endphp
                    <tr>
                        <td>{{$semi_name->name}}</td>
                        <td class="text-right">{{number_format(($mergeB->qty)*(-1),3)}}</td>
                        <td class="text-right">{{number_format(($mergeB->amount)*(-1),2)}}</td>
                        <td class="text-right"></td>
                    </tr>
                @endif
            @endforeach
            @foreach($data_c as$key=> $mergeC)
                @if ( $mergeC->qty != 0 )
                    @php

                            $semi_name = \SemiFinishProductManager\Models\SemiFinishProduct::where('semi_finish_product_id', '=', $mergeC->semi_id)->first();

                            $semi_vol = $semi_vol+$mergeC->qty;
                            $semi_tot = $semi_tot+$mergeC->amount;
                    @endphp

                    <tr>
                        <td>{{$semi_name->name}}</td>
                        <td class="text-right">{{number_format(($mergeC->qty)*(-1),3)}}</td>
                        <td class="text-right">{{number_format(($mergeC->amount)*(-1),2)}}</td>
                        <td class="text-right"></td>
                    </tr>
                @endif
            @endforeach

            </tbody>
            <tfoot>
            <tr>
                <th>Total</th>
                <th class="text-right">{{number_format(($semi_vol)*(-1),3)}}</th>
                <th class="text-right">{{number_format(($semi_tot)*(-1),2)}} </th>
                <th class="text-center"></th>
            </tr>
            <tr>
                <th>Semi Finish Product</th>
                <th class="text-right">Wastage Volume</th>
                <th class="text-right">Price </th>
                <th class="text-center">Action</th>
            </tr>
            </tfoot>
        </table>
    </div>
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>

    <script>
        $(document).ready(function () {
            table = $('#invoice_table').DataTable({
                searching: true,
                'columnDefs': [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                'select': {
                    'style': 'multi'
                },
                dom: 'Bfrtip',
                pageLength: 10,
                responsive: true
            });
        });
    </script>

@stop
