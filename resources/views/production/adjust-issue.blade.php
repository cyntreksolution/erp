@extends('layouts.back.master')@section('title','Production | Adjust Semi Issue')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
    {{--    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/css/dataTables.checkboxes.css" rel="stylesheet"/>--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        th {
            text-align: right !important;
        }

        td {
            text-align: right !important;
        }

        .abc {
            width: 1000px;
            height: 1000px;
            /* background-color: black; */
            z-index: 8000;

        }

        .sweet-alert {
            z-index: 7999 !important;
        }

        .sweet-overlay {
            z-index: 7998 !important;
        }

    </style>
@stop

@section('content')

    <div id="wait" class="d-none abc" style=""><img src='{{asset('assets/img/4V0b.gif')}}' width="64" height="64"/><br>Loading..
    </div>

    {{--    <div class="app-main-content mb-2">--}}
    {{--        <div class="row mx-2">--}}
    {{--            <div class="col-md-2">--}}
    {{--                {!! Form::select('agent',$agents , null , ['class' => 'form-control','placeholder'=>'Select Agent','id'=>'agent']) !!}--}}
    {{--            </div>--}}
    {{--            <div class="col-md-2">--}}
    {{--                {!! Form::select('category',$categories , null , ['class' => 'form-control','placeholder'=>'Select Category','id'=>'category']) !!}--}}
    {{--            </div>--}}
    {{--            <div class="col-md-2">--}}
    {{--                {!! Form::select('day', ['Monday'=>'Monday','Tuesday'=>'Tuesday','Wednesday'=>'Wednesday','Thursday'=>'Thursday','Friday'=>'Friday','Saturday'=>'Saturday','Sunday'=>'Sunday','Long Weekend'=>'Long Weekend','Extra'=>'Extra'] , null , ['class' => 'form-control','placeholder'=>'Select Day','id'=>'day']) !!}--}}
    {{--            </div>--}}
    {{--            <div class="col-md-2">--}}
    {{--                {!! Form::select('time', ['Morning'=>'Morning','Noon'=>'Noon','Evening'=>'Evening'] , null , ['class' => 'form-control','placeholder'=>'Select Time','id'=>'time']) !!}--}}
    {{--            </div>--}}
    {{--            <div class="col-md-2">--}}
    {{--                <input type="text" name="date" id="date" class="form-control" placeholder="Select Date">--}}
    {{--            </div>--}}
    {{--            <div class="col-md-2">--}}
    {{--                <button class="btn btn-info " onclick="process_form(this)">Filter</button>--}}
    {{--            </div>--}}

    {{--            <!-- loading view -->--}}
    {{--        </div>--}}
    {{--    </div>--}}

    {{ Form::open(array('url' => '/production/adjust/semi/issue/store','id'=>'mergerForm','method'=>'POST'))}}
    <input type="hidden" name="merge_serial" value="{{$data['burden_id']}}">
    <div class="">
        <table id="invoice_table" class="display text-center">
            <thead>
            <tr>
                <th>Semi finish product</th>
                <th>Available QTY</th>
                <th>Issued QTY</th>


            </tr>
            </thead>
            <tbody>

            @php $flag =false @endphp
            @foreach($data['items'] as  $item)
                @if ($item['available_qty']>0)
                    <tr>
                        <td>{{$item['semi_name']}}</td>
                        <td>{{number_format($item['available_qty'],3)}}</td>
                        <td>
                            <input type="hidden" name="semi[]" value="{{$item['id']}}">
                            <input type="number" name="qty[]" autocomplete="off"
                                   class="form-control text-right w-100" step="0.001"
                                   max="{{number_format($item['available_qty'],3,'.','')}}" min="0"
                                   value="0">
                        </td>
                    </tr>
                @endif

            @endforeach

            </tbody>
            <tfoot>
            <tr>
                <th>Semi finish product</th>
                <th>Available QTY</th>
                <th>Issued QTY</th>

            </tr>
            </tfoot>
        </table>
        <div class="row">
            <div class="col-md-12 mt-5 ">
                <button type="submit" name="submit" onclick="this.form.submit();this.disabled = true;"
                        class="btn btn-block btn-warning">Adjust Semi Finish
                </button>
            </div>
        </div>
        {{ Form::close() }}
    </div>

    <div class="modal fade" id="ChangeQty" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(['url'=>'','id'=>'qtyChangeForm']) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            <div class="col-md-6">
                                <span>Quantity</span>
                            </div>
                            <div class="col-md-6">
                                <input name="qty" type="number" id="qty" step="0.01" class="task-name-field" value="">
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="Create Burden">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    {{--    <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>--}}
    {{--    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js"></script>--}}



    <script>
        $(document).ready(function () {
            table = $('#invoice_table').DataTable({
                searching: true,
                'columnDefs': [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                'select': {
                    'style': 'multi'
                },
                dom: 'Bfrtip',
                pageLength: 250,
                responsive: true
            });
        });

        function addSemiFinishBurden(id, qty, type) {
            if (type == 1) {
                $("#qtyChangeForm").attr('action', '/production/make/semi/' + id);
            } else {
                $("#qtyChangeForm").attr('action', '/production/make/cooking/' + id);
            }

            $("#qty").attr({
                "min": parseFloat(qty).toFixed(2)
            });

            $('#ChangeQty').modal('show')
        }


        function makeSemiFinish(id, type) {
            let url = null;
            if (type == 1) {
                url = '/production/create/semi/' + id;
            } else {
                url = '/production/create/cooking/' + id;
            }
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this action",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, Make Semi Finish!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "post",
                        url: url,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Created !", "Semi finish created successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            } else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );
        }
    </script>

@stop
