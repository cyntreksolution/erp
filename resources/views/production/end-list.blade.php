@extends('layouts.back.master')@section('title','Production End Product')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        th {
            text-align: right !important;
        }

        td {
            text-align: right !important;
        }

        .abc {
            width: 1000px;
            height: 1000px;
            /* background-color: black; */
            z-index: 8000;

        }

        .sweet-alert {
            z-index: 7999 !important;
        }

        .sweet-overlay {
            z-index: 7998 !important;
        }

    </style>
@stop

@section('content')

    <div id="wait" class="d-none abc" style=""><img src='{{asset('assets/img/4V0b.gif')}}' width="64" height="64"/><br>Loading..
    </div>

    <div class="">
        {!! Form::open(['route' => 'production.cuBal', 'method' => 'post']) !!}
        <input type="hidden" name="end_product_burden_header" value="{{$end_product_burden_header}}">
        <table id="invoice_table" class="display text-center">
            <thead>
            <tr>
                <th>End Product</th>
                <th>Available QTY</th>
                <th>Destroy QTY</th>
                <th>Reuse QTY</th>
                <th>Lost QTY</th>
            </tr>
            </thead>
            <tbody>


            @foreach($end_products as $key2=> $product)
{{--                @if($product->available_qty)--}}
                    <tr>
                        <td>{{$product->name}}</td>
                        <td><label id="available_qty_text_{{$product->id}}">{{$product->available_qty}}</label></td>
                        <input type="hidden" name="end_product_id[]" value="{{$product->id}}">
                        <input type="hidden" name="available_qty[]" id="available_qty_{{$product->id}}"
                               value="{{$product->available_qty}}">
                        <td>
                            <input {{($product->available_qty<0) ?'readonly':null}}  type="number" class="form-control qty" {{$product->available_qty >0 ?"min=0":null }} data-id="{{$product->id}}"
                                   {{($product->available_qty<0) ?'readonly':null}}  max="{{$product->available_qty}}" step="0.001" style="width: 90%" value="0"
                                   id="destroy_qty_{{$product->id}}" name="destroy_qty[]"></td>
                        <td>
                            <input {{($product->available_qty<0) ?'readonly':null}}  {{$product->reusable_type==1?'readonly':null}} type="number" class="form-control qty"
                                    data-id="{{$product->id}}"
                                   max="{{$product->available_qty}}" step="0.001" style="width: 90%" value="0"
                                   id="re_use_qty_{{$product->id}}" name="re_use_qty[]"></td>
                        <td>
                            <input type="number" class="form-control qty"  {{$product->available_qty >0 ?"min=0":null }} data-id="{{$product->id}}"
                                   max="{{$product->available_qty}}" step="0.001" style="width: 90%" value="0"
                                   id="lost_qty_{{$product->id}}" name="lost_qty[]"></td>
                    </tr>
{{--                @endif--}}
            @endforeach


            </tbody>
            <tfoot>
            <tr>
                <th>End Product</th>
                <th>Available QTY</th>
                <th>Destroy QTY</th>
                <th>Reuse QTY</th>
                <th>Lost QTY</th>
            </tr>
            </tfoot>
        </table>

        <div class="row mt-3">
            <div class="col-md-4">
                {!! Form::select('employee', $employees , null , ['class' => 'form-control','required','placeholder'=>'select employee']) !!}
            </div>
            @php  $merged = \EndProductBurdenManager\Models\EndProductBurdenHeader::find($end_product_burden_header); @endphp
            @if ($merged->merge_status =='finished')
                <div class="col-md-8">
                    <button type="button" class="btn btn-block btn-danger">Already Finished</button>
                </div>
            @else
                <div class="col-md-8">
                    <button class="btn btn-block btn-success">Finish</button>
                </div>
            @endif
        </div>
    </div>
    {!! Form::close() !!}
    </div>
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>

    <script>
        $(document).ready(function () {
            table = $('#invoice_table').DataTable({
                searching: true,
                'columnDefs': [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                'select': {
                    'style': 'multi'
                },
                "ordering": false,
                dom: 'Bfrtip',
                pageLength: 250,
                responsive: true
            });
        });


        $('.qty').keyup(function () {
            let dInput = this.value;
            let id = $(this).data("id")
            let available_qty_id = '#available_qty_' + id
            let destroy_qty_id = '#destroy_qty_' + id
            let re_use_qty_id = '#re_use_qty_' + id
            let lost_qty_id = '#lost_qty_' + id
            let available_qty_text = '#available_qty_text_' + id


            let available_qty = $(available_qty_id).val();
            let destroy_qty = $(destroy_qty_id).val();
            let re_use_qty = $(re_use_qty_id).val();
            let lost_qty = $(lost_qty_id).val();

            let qty = available_qty - destroy_qty - re_use_qty - lost_qty;
            if (available_qty>0){
                if (qty < 0) {
                    swal("Sorry!", "Available quantity cannot be negative value", "error");
                    $(this).val(0);

                    available_qty = $(available_qty_id).val();
                    destroy_qty = $(destroy_qty_id).val();
                    re_use_qty = $(re_use_qty_id).val();
                    lost_qty = $(lost_qty_id).val();
                    qty = available_qty - destroy_qty - re_use_qty - lost_qty
                    $(available_qty_text).empty();
                    $(available_qty_text).text(qty);
                } else {
                    $(available_qty_text).empty();
                    $(available_qty_text).text(qty);
                }
            }else {
                if (qty > 0) {
                    swal("Sorry!", "Available quantity cannot be greater than zero", "error");
                    $(this).val(0);

                    available_qty = $(available_qty_id).val();
                    destroy_qty = $(destroy_qty_id).val();
                    re_use_qty = $(re_use_qty_id).val();
                    lost_qty = $(lost_qty_id).val();
                    qty = available_qty - destroy_qty - re_use_qty - lost_qty
                    $(available_qty_text).empty();
                    $(available_qty_text).text(qty);
                } else {
                    $(available_qty_text).empty();
                    $(available_qty_text).text(qty);
                }
            }


        });
    </script>

@stop
