@extends('layouts.back.master')
@section('title','Cash Book Inquiry ')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/datatables.net/css/buttons.dataTables.min.css')}}">
    <style>
        th {
            text-align: right !important;
        }

        td {
            text-align: right !important;
        }
    </style>
@stop

@section('content')
    @if(Sentinel::check()->roles[0]->slug=='owner' ||  Sentinel::check()->roles[0]->slug=='accountant' )
        <div class="row sm-12">

            <div class="col-sm-2">
                {!! Form::select('maindesc',$main_descs , null , ['class' => 'form-control','placeholder'=>'Select Main Description','id'=>'maindesc']) !!}

{{--                <select name="maindesc" id="maindesc" required--}}
{{--                    class="form-control select2 ">--}}
{{--                <option value="">Select Main Description</option>--}}
{{--                <option value="AgentsPayments">Agents Payments</option>--}}
{{--                <option value="SupplierPayments">Supplier Payments</option>--}}
{{--                <option value="Wastage">Wastage</option>--}}
{{--                <option value="Salary & Wadges">Salary & Wadges</option>--}}
{{--                <option value="Capital">Capital</option>--}}
{{--                <option value="Commission">Commission</option>--}}
{{--                <option value="Maintanance">Maintenance</option>--}}
{{--                <option value="Utility Bills">Utility Bills</option>--}}
{{--                <option value="Rent">Rent</option>--}}
{{--                <option value="Running Cost">Running Cost</option>--}}
{{--                <option value="Taxes">Taxes</option>--}}
{{--                <option value="Private">Private</option>--}}
{{--                <option value="Private">Responsibility</option>--}}
{{--                <option value="Other">Other</option>--}}
{{--            </select>--}}
            </div>
            <div class="col-sm-1">
                {!! Form::select('paytype',['CashIn'=>'Cash In','CashOut'=>'Cash Out'] , null , ['class' => 'form-control','placeholder'=>'Select Payment Type','id'=>'paytype']) !!}
            </div>
            <div class="col-sm-2">
                {!! Form::select('sub_desc',$datas , null , ['class' => 'form-control','placeholder'=>'Select Sub Description','id'=>'sub_desc']) !!}

{{--                <select name="sub_desc" id="sub_desc" required class="form-control select2 ">--}}
{{--                    <option value="">Select Sub Description</option>--}}

                    {{--@foreach($datas as $key => $data)

                        <option value="">{{$data[$key]->sub_desc}}</option>
                    @endforeach--}}
{{--                </select>--}}
            </div>
            <div class="col-sm-1">
                {!! Form::select('category',$categories , null , ['class' => 'form-control','placeholder'=>'Select Category','id'=>'category']) !!}


            </div>

            <div class="col-sm-1">
                {!! Form::select('desc1',$data2 , null , ['class' => 'form-control','placeholder'=>'Select Description','id'=>'desc1']) !!}

            </div>

            <div class="col-sm-3">
                <div class="form-group">
                    <input type="text" autocomplete="off" name="date_range" id="date_range" class="form-control">
                </div>
            </div>


            <div class="col-md-3">
                <button class="btn btn-info " onclick="process_form(this)">Filter</button>

                <a onclick="process(this)" class="btn btn-danger" id="printBTN" target="_blank">Summery</a>


            </div>
        </div>
    @endif


    <div class="">
        <table id="invoice_table" class="display text-center mt-3">
            <thead>
            <tr>
                <th>Date</th>
                <!--th>Main Description</th>
                <th>Sub Description</th-->
                <th>Description</th>
                <th>Credit</th>
                <th>Debit</th>
                <th>Balance</th>

            </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <tr>
                <th>Date</th>
                <!--th>Main Description</th>
                <th>Sub Description</th-->
                <th>Description</th>
                <th>Credit</th>
                <th>Debit</th>
                <th>Balance</th>
            </tr>
            </tfoot>
        </table>
    </div>
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/Buttons-1.5.1/js/dataTables.buttons.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/JSZip-2.5.0/jszip.min.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/pdfmake-0.1.32/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/Buttons-1.5.1/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js')}}"></script>

    <script>



        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            "startDate": moment(),
            "endDate":moment(),
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019",
            "maxDate":moment()
        });
        // $('#date_range').val('')

        function process_form(e) {
            let date_range = $("#date_range").val();
            let maindesc = $("#maindesc").val();
            let sub_desc = $("#sub_desc").val();
            let paytype = $("#paytype").val();
            let category = $("#category").val();
            let desc1 = $("#desc1").val();
            let table = $('#invoice_table').DataTable();
            table.ajax.url('/cash/inquiry/table/data?date_range=' + date_range + '&maindesc=' +maindesc+ '&sub_desc=' +sub_desc+ '&paytype=' +paytype + '&category=' +category+ '&desc1=' +desc1+ '&filter=' + true).load();
        }




        function process()
        {
            let date_range = $("#date_range").val();
            window.location.href = "/cash/inquiry/table/summery?date_range=" + date_range;
        }



        function process_form_reset() {
            $("#status").val('');
            $("#agent").val('');
            $("#date_range").val('');
            let table = $('#invoice_table').DataTable();
            table.ajax.url('/cash/inquiry/table/data').load();
        }


        var outcome_sum;
        var income_sum;
        var balance ;

        $(document).ready(function () {
            table = $('#invoice_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "lengthMenu": [[10, 25, 50,100,250, -1], [10, 25, 50,100,250, "All"]],
                dom: 'lfBrtip',
                buttons: [
                    {
                        extend: 'copy',
                        className: 'btn btn-xs btn-info ml-3 sml assets-export-btn export-copy ttip',
                    },

                    {
                        extend: 'excel',
                        className: 'btn btn-xs btn-success ml-3 sml assets-export-btn export-copy ttip',
                    },
                    {
                        extend: 'pdf',
                        className: 'btn btn-xs btn-danger ml-3 sml assets-export-btn export-copy ttip',
                    },

                    {
                        extend: 'print',
                        className: 'btn btn-xs btn-primary ml-3 sml assets-export-btn export-copy ttip',
                    },
                ],
                "ajax": {
                    url: "{{url('/cash/inquiry/table/data')}}",
                    type: "get",
                    error: function () {  // error handling code
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    },
                    dataSrc: function (data) {
                        income_sum = data.income_sum;
                        outcome_sum = data.outcome_sum;
                        balance = data.balance;
                        return data.data;
                    }
                },
                drawCallback: function (settings) {
                    var api = this.api();
                    $(api.column(2).footer()).html(
                        'Credit : ' + income_sum
                    );
                    $(api.column(3).footer()).html(
                        'Debit : ' + outcome_sum
                    );
                    $(api.column(4).footer()).html(
                        'Transaction Balance : ' + balance
                    );
                },
                pageLength: 25,
                responsive: true
            });
        });

        $('#maindesc').on('change', function (e) {
            var optionSelected = $("option:selected", this);
            var valueSelected = this.value;

            var sel = $("#sub_desc");
            $.ajax({
                type: "get",
                url: '/get/cash/category',
                data: {category_name: valueSelected},

                success: function (response) {
                    sel.empty();
                    sel.append('<option value="">Select Sub Category</option>');
                    for (var i=0; i<response.length; i++) {
                        sel.append('<option value="' + response[i].id + '">' + response[i].sub_desc + '</option>');
                    }
                }
            });

        });

        $('#paytype').on('change', function (e) {
            var optionSelected = $("option:selected", this);
            var valueSelected = this.value;
            var category_name = $('#maindesc').val();

            var sel = $("#sub_desc");
            $.ajax({
                type: "get",
                url: '/get/cash/category',
                data: {category_name: category_name,cash_flow:valueSelected},

                success: function (response) {
                    sel.empty();
                    sel.append('<option value="">Select Sub Category</option>');
                    for (var i=0; i<response.length; i++) {
                        sel.append('<option value="' + response[i].id + '">' + response[i].sub_desc + '</option>');
                    }
                }
            });

        });

        $('#sub_desc').on('change', function (e) {
            var optionSelected = $("option:selected", this);
            var valueSelected = this.value;
            var category_name = $('#maindesc').val();

            var sel = $("#category");
            $.ajax({
                type: "get",
                url: '/get/cash/subdesc/category',
                data: {subdesc: valueSelected},

                success: function (response) {
                    sel.empty();
                    sel.append('<option value="">Select Category</option>');
                    for (var i=0; i<response.length; i++) {
                        sel.append('<option value="' + response[i].category + '">' + response[i].category + '</option>');
                    }
                }
            });

        });


     /*   $(document).ready(function () {
            $("#emp_id").hide();

        });*/


        $('#category').on('change', function (e) {
            var optionSelected = $("option:selected", this);
            var valueSelected = this.value;
           // var category_name = $('#maindesc').val();

            var dc = $("#desc1");
            $.ajax({
                type: "get",
                url: '/get/cash/subdesc/desc1',
                data: {cat: valueSelected},

                success: function (response) {
                    dc.empty();
                    dc.append('<option value="">Select Description</option>');
                  //  for (var i=0; i<response.length; i++) {
                  //      dc.append('<option value="' + response[i].index + '">' + response[i].value + '</option>');
                  //  }
                    $.each(response, function (index, value) {
                        $('#desc1').append('<option value="' + index + '">' + value + '</option>');
                    })

                }
            });


        }).trigger("change");




    </script>

@stop
