@extends('layouts.back.master')
@section('title','Supplier Outstanding | List ')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/datatables.net/css/buttons.dataTables.min.css')}}">
    <style>
        th {
            text-align: right !important;
        }

        td {
            text-align: right !important;
        }
    </style>
@stop

@section('content')
    @if(Auth::user()->hasRole(['Owner','Super Admin','Accountant']))
        <div class="row mb-2">

            <div class="col-md-3">
                {!! Form::select('agent',$suppliers , null , ['class' => 'form-control','placeholder'=>'Select Supplier','id'=>'supplier']) !!}
            </div>


            <div class="col-md-2">
                {!! Form::select('status',[1=>'Pending Orders',3=>'Received Orders'] , null , ['class' => 'form-control','placeholder'=>'Select Status','id'=>'status']) !!}
            </div>


            <div class="col-md-2">
                {!! Form::select('status',[1=>'Paid',2=>'Unpaid'] , null , ['class' => 'form-control','placeholder'=>'Select Payment Status','id'=>'payments']) !!}
            </div>

            <div class="col-sm-3">
                <div class="form-group">
                    <input type="text" name="date_range" id="date_range"  autocomplete="off" class="form-control">
                </div>
            </div>
            <div class="col-md-2">
                <button class="btn btn-info " onclick="process_form(this)">Filter</button>
            </div>
        </div>
    @endif


    <div class="">
        <table id="invoice_table" class="display text-center mt-3">
            <thead>
            <tr>
                <th>#</th>
                <th>Date</th>
                <th>Supplier Name</th>
                <th>Invoice Number</th>
                <th>Total</th>
                <th>Age</th>
                <th>Status</th>
                <th>Remark</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <tr>
                <th>#</th>
                <th>Date</th>
                <th>Supplier Name</th>
                <th>Invoice Number</th>
                <th>Total</th>
                <th>Age</th>
                <th>Status</th>
                <th>Remark</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
    </div>
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/Buttons-1.5.1/js/dataTables.buttons.js')}}"></script>
    <script src="{{asset('/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/JSZip-2.5.0/jszip.min.js')}}"></script>
    <script src="{{asset('js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/pdfmake-0.1.32/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/Buttons-1.5.1/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('/js/vfs_fonts.js')}}"></script>

    <script>
        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019",
            "maxDate":moment()
        });
        $('#date_range').val('')


        function process_form(e) {
            let supplier = $("#supplier").val();
            let date_range = $("#date_range").val();
            let payments = $("#payments").val();
            let status = $("#status").val();
            let table = $('#invoice_table').DataTable();
            table.ajax.url('/supplier/outstanding/table/data?supplier=' + supplier + '&date_range=' + date_range +'&payments=' +  status +'&status=' + payments + '&filter=' + true).load();
        }


        function process_form_reset() {
            $("#supplier").val('');
            $("#date_range").val('');
            let table = $('#invoice_table').DataTable();
            table.ajax.url('/supplier/outstanding/table/data').load();
        }


        var total_sum;
        $(document).ready(function () {
            table = $('#invoice_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                dom: 'lfBrtip',
                buttons: [
                    {
                        extend: 'copy',
                        className: 'btn btn-xs btn-info ml-3 sml assets-export-btn export-copy ttip',
                    },

                    {
                        extend: 'excel',
                        className: 'btn btn-xs btn-success ml-3 sml assets-export-btn export-copy ttip',
                    },
                    {
                        extend: 'pdf',
                        className: 'btn btn-xs btn-danger ml-3 sml assets-export-btn export-copy ttip',
                    },

                    {
                        extend: 'print',
                        className: 'btn btn-xs btn-primary ml-3 sml assets-export-btn export-copy ttip',
                    },
                ],
                "ajax": {
                    url: "{{url('/supplier/outstanding/table/data')}}",
                    type: "get",
                    error: function () {  // error handling code
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    },
                    dataSrc: function (data) {
                        total_sum = data.total_sum;
                        return data.data;
                    }
                },
                drawCallback: function (settings) {
                    var api = this.api();
                    $(api.column(4).footer()).html(
                        'Total Amount : ' + total_sum
                    );
                },
                pageLength: 25,
                responsive: true
            });
        });


    </script>

@stop
