<html>
<head>
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/invoice.css')}}" rel="stylesheet">
    <style media="all">

        @page {
            margin: 30px 5px 30px 5px;
            size: 75mm 999mm;
        }
    </style>
</head>

<body style="margin-bottom: 100px">

<div class="row">

    <div class="col-xs-6">
        <img src="{{asset('assets/img/buntak_grace.jpeg')}}" style="width: 100px">
    </div>

    <div class="col-xs-6 text-right">
        <h6>
            <b>Ranjanas Holdings [Pvt] Ltd</b><br>
            Katukurunda<br>
            Habaraduwa<br>
            Hotline : 077 330 4678<br>
        </h6>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <hr>

        <div class="task-name-wrap">
            <div class="row mt-12" style="display: inline">
                <div class="col-md-6 col-xl-6"><b>Document :</b></div>
                <div class="col-md-6 col-xl-6 text-left"><b>Cash Book Summery</b></div>
            </div>
        </div>

        <div class="task-name-wrap">
            <div class="row mt-12" style="display: inline">
                <div class="col-md-6 col-xl-6"><b>Date :</b></div>
                <div class="col-md-6 col-xl-6 text-left"><b>{{$date_range}}</b></div>
            </div>
        </div>

        <div class="task-name-wrap">
            <div class="row mt-12" style="display: inline">
                <div class="col-md-6 col-xl-6"><b>Printed At</b></div>
                <div class="col-md-6 col-xl-6 text-left"><b>{{date('Y-m-d H:i:s')}}</b></div>
            </div>
        </div>

  {{--      @foreach($datas as $data)
            <div class="task-name-wrap">
                <div class="row mt-2" style="display: inline">
                    <div class="col-md-4 col-xs-4"><b>Current Outstanding</b></div>
                    <div class="col-md-6 col-xs-6 text-left"><b>{{number_format($record->first()->start_balance+$agent_init_value,2)}}</b></div>
                </div>
            </div>
            @break
        @endforeach  --}}
        <hr style="height:1px;background-color: #00a5bb">

        <table class="table">
            <thead>
            <tr>
                <th class="text-left">Main Desc</th>
                <th class="text-left">Sub Desc</th>
                <th class="text-right">Credit</th>
                <th class="text-right">Debit</th>
            </tr>
            </thead>
            <tbody>
            @php @endphp

                <tr>
                    <td class="col-md-4 text-left"><b>Start Balance</b></td>
                    <td class="col-md-4 text-left"><b></b></td>
                    <td class="col-md-2 text-right"><b>{{number_format($opBal,2)}}</b></td>
                    <td class="col-md-2 text-right"><b></b></td>
                </tr>


            @foreach($datas as $data)
                    <tr>
                        <td class="col-md-4 text-left">{{$data[0]}}</td>
                        <td class="col-md-4 text-left">{{$data[1]}}</td>
                        <td class="col-md-2 text-right">{{number_format(floatval($data[2]),2)}}</b></td>
                        <td class="col-md-2 text-right">{{number_format(floatval($data[3]),2)}}</td>

                    </tr>
                @endforeach

            <tr>
                <td class="col-md-4 text-left"><b>Final Balance</b></td>
                <td class="col-md-4 text-left"><b></b></td>
                <td class="col-md-2 text-right"><b></b></td>
                <td class="col-md-2 text-right"><b>{{number_format($endBal,2)}}</b></td>

            </tr>

            <tr>
                <td class="col-md-4 text-left"><b>Cash In Hand</b></td>
                <td class="col-md-4 text-left"><b></b></td>
                <td class="col-md-2 text-right"><b></b></td>
                <td align="right">
                    <div style="width:150px;height:30px;border:1px solid #000;"></div>
                </td>

            </tr>

            <tr>
                <td class="col-md-4 text-left"><b>Difference</b></td>
                <td class="col-md-4 text-left"><b></b></td>
                <td class="col-md-2 text-right"><b></b></td>
                <td align="right">
                    <div style="width:150px;height:30px;border:1px solid #000;"></div>
                </td>

            </tr>


            </tbody>
        </table>
    </div>
</div>
<br>


<footer>
    <footer>


        <div class="row col-xs-12" style="margin-top: 60px;">

            <div class="text-center col-xs-12">
                <h5>
                    .........................................<br>
                    Name & Signature-Accountant
                </h5>
            </div>
        </div>
        <div class="row col-xs-12" style="margin-top: 60px;">
            <div class="text-center col-xs-12">
                <h5>
                    .........................................<br>
                    Signature-Checked By
                </h5>
            </div>

        </div>



    </footer>

</footer>

</body>

</html>
