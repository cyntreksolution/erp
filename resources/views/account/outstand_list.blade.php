@extends('layouts.back.master')
@section('title','Agent Outstanding | List ')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/datatables.net/css/buttons.dataTables.min.css')}}">
    <style>
        th {
            text-align: right !important;
        }

        td {
            text-align: right !important;
        }
    </style>
@stop

@section('content')
    @if (Auth::user()->hasRole(['Owner','Sales Manager','Accountant']) )
       <div class="row mb-2">
            <div class="col-md-3">
                {!! Form::select('agent',$agents , null , ['class' => 'form-control','placeholder'=>'Select Agent','id'=>'agent']) !!}
            </div>
            <div class="col-md-2">
                {!! Form::select('category', $categories , null , ['class' => 'form-control','placeholder'=>'Select Category','id'=>'category']) !!}
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    {!! Form::select('desc1', [1=>'Distributor',2=>'Show Room',3=>'Direct Sale'] , null , ['class' => 'form-control','placeholder'=>'Select Type','id'=>'desc1']) !!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <input type="text" name="date_range" id="date_range" class="form-control">
                </div>
            </div>
            <div class="col-md-2">
                <button class="btn btn-info " onclick="process_form(this)">Filter</button>
            </div>
        </div>
    @endif


    <div class="">
        <table id="invoice_table" class="display text-center mt-3">
            <thead>
            <tr>
                <th>#</th>
                <th>Date</th>
                <th>Agent Name</th>
                <th>Category</th>
                <th>Invoice Number</th>
                <th>Net Amount</th>
                <th>Paid Amount</th>
                <th>Remain</th>
                <th>Age</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            <tr>
                <th>#</th>
                <th>Date</th>
                <th>Agent Name</th>
                <th>Category</th>
                <th>Invoice Number</th>
                <th>Net Amount</th>
                <th>Paid Amount</th>
                <th>Remain</th>
                <th>Age</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
    </div>
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/Buttons-1.5.1/js/dataTables.buttons.js')}}"></script>
    <script src="{{asset('/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/JSZip-2.5.0/jszip.min.js')}}"></script>
    <script src="{{asset('js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/pdfmake-0.1.32/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/Buttons-1.5.1/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('/js/vfs_fonts.js')}}"></script>

    <script>
        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019",
            "maxDate":moment()
        });
        $('#date_range').val('')
        function process_form(e) {
            let category = $("#category").val();
            let agent = $("#agent").val();
            let desc1 = $("#desc1").val();
            let date_range = $("#date_range").val();
            let table = $('#invoice_table').DataTable();
            table.ajax.url('/agent/outstanding/table/data?category=' + category + '&agent=' + agent +  '&date_range=' + date_range + '&desc1=' + desc1 + '&filter=' + true).load();
        }


        function process_form_reset() {
            $("#status").val('');
            $("#agent").val('');
            $("#date_range").val('');
            let table = $('#invoice_table').DataTable();
            table.ajax.url('/agent/outstanding/table/data').load();
        }


        var paid_sum;
        var net_sum;
        var net_rem;
        $(document).ready(function () {
            table = $('#invoice_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "lengthMenu": [[10, 25, 50,100,250, -1], [10, 25, 50,100,250, "All"]],
                dom: 'lfBrtip',
                buttons: [
                    {
                        extend: 'copy',
                        className: 'btn btn-xs btn-info ml-3 sml assets-export-btn export-copy ttip',
                    },

                    {
                        extend: 'excel',
                        className: 'btn btn-xs btn-success ml-3 sml assets-export-btn export-copy ttip',
                    },
                    {
                        extend: 'pdf',
                        className: 'btn btn-xs btn-danger ml-3 sml assets-export-btn export-copy ttip',
                    },

                    {
                        extend: 'print',
                        className: 'btn btn-xs btn-primary ml-3 sml assets-export-btn export-copy ttip',
                    },
                ],
                "ajax": {
                    url: "{{url('/agent/outstanding/table/data')}}",
                    type: "get",
                    error: function () {  // error handling code
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    },
                    dataSrc: function (data) {
                        paid_sum = data.paid_sum;
                        net_sum = data.net_sum;
                        net_rem = data.net_rem;
                        return data.data;
                    }
                },
                drawCallback: function (settings) {
                    var api = this.api();
                    $(api.column(5).footer()).html(
                        'Net Amount : ' + net_sum
                    );
                    $(api.column(6).footer()).html(
                        'Paid Amount : ' + paid_sum
                    );
                    $(api.column(7).footer()).html(
                        'Net Remain : ' + net_rem
                    );
                },
                pageLength: 25,
                responsive: true
            });
        });


    </script>

@stop
