<html>
<head>
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/invoice.css')}}" rel="stylesheet">
</head>
<body>
<div class="row">
    <h4 class="inv">
        SUPPLIER WISE RAW MATERIALS
    </h4>
</div>
<div class="row">
    <table>
        <tr>
            <th>Name</th>
            <th>Total qty</th>
            <th>Buffer stock</th>
            <th>Supplier</th>
            <th>Last price</th>
            <th>Last bill qty</th>
        </tr>
        @foreach($records as $record)
            <tr>
                <td>{{$record->raw_material_name}}</td>
                <td>{{$record->available_qty}}</td>
                <td>{{$record->buffer_stock}}</td>
                <td>{{$record->supplier_name}}</td>
                <td>{{$record->ordered_price}}</td>
                <td>{{$record->recieved_qty}}</td>
            </tr>
        @endforeach
    </table>
</div>
<footer>
    <div class="row" style="margin-top: 20px;">
        <h4 align="center">
            <h6 class="text-muted">powered by cyntrek<br>
                <h7 class="num">+9471 122 5489 / +9471 532 2262</h7>
            </h6>
        </h4>

    </div>
</footer>
</body>

</html>