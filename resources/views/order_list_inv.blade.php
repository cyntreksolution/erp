@extends('layouts.back.master')@section('title','Invoice Preview')
@section('css')

@stop


@section('content')
    <div class="container">
        <div class="app-wrapper">
            <div class="app-main">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    @php
                                   // dd($loading);

                                    $order = \SalesOrderManager\Models\SalesOrder::whereId($loading->so_id)->first();


                                    $pay = \App\AgentPayment::where('agent_id','=',$order->salesRep->id)
                                                    ->where('updated_at','>=',\Carbon\Carbon::parse($order->created_at)->format('Y-m-d'))
                                                    ->where('updated_at','<=',\Carbon\Carbon::parse($loading->updated_at)->addDay()->format('Y-m-d'))
                                                    ->get();



                                    $pay_debit = \App\SalesReturnHeader::where('agent_id','=',$order->salesRep->id)
                                                    ->where('updated_at','>=',\Carbon\Carbon::parse($order->created_at)->format('Y-m-d'))
                                                    ->where('updated_at','<=',\Carbon\Carbon::parse($loading->updated_at)->addDay()->format('Y-m-d'))
                                                    ->get();



                                    @endphp

                                    <div class="col-md-6">Category:</div>
                                    <div class="col-md-6"> {{$order->template->category->name}}</div>
                                </div>
                                @if($order->template->category->loading_type == 'to_loading')
                                    <input type="hidden" name="man_id" value=1>
                                @endif


                                <div class="row">
                                    <div class="col-md-6">Day:</div>
                                    <div class="col-md-6"> {{$order->template->day}}</div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">Time:</div>
                                    <div class="col-md-6"> {{$order->template->time}}</div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">Agent:</div>
                                    <div class="col-md-6">{{$order->salesRep->first_name.' '.$order->salesRep->last_name}}</div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">Date:</div>
                                    <div class="col-md-6"> {{$order->created_at}}</div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">Order ID:</div>
                                    <div class="col-md-6"> {{$order->id}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="app-main-content px-3 py-4">
                            <div class="content">
                                <div class="row ">
                                    <div class="form-group col-md-1">
                                        <label for="">#</label>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="">Name</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Qty</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Price</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Total</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Commission</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Unit Discount</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Total Discount</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Amount</label>
                                    </div>
                                </div>
                                @php $count = 0; @endphp
                                @foreach($loading->items as $data)
                                    @if($data->qty != 0)
                                        @php $count = $count+1; @endphp
                                        <div class="row ">
                                            <div class="form-group col-md-1">
                                                <label for="">{{ $count }}</label>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="">{{ $data->endProduct->name }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ $data->qty }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ number_format($data->endProduct->selling_price,2) }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ number_format($data->endProduct->selling_price * $data->qty,2) }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ number_format($data->commission,2) }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ number_format($data->unit_discount,2) }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ number_format($data->total_discount,2) }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ number_format($data->row_value,2) }}</label>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                                @foreach($loading->variants as $data)
                                    @if($data->weight != 0)
                                        @php $count = $count+1; @endphp
                                        <div class="row ">
                                            <div class="form-group col-md-1">
                                                <label for="">{{ $count }}</label>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="">{{ $data->variant->product->name.' '.$data->variant->variant }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ $data->weight }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ number_format($data->variant->product->selling_price,2) }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ number_format($data->variant->product->selling_price * $data->weight ,2) }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ number_format($data->commission,2) }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ number_format($data->unit_discount,2) }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ number_format($data->total_discount,2) }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ number_format($data->row_value,2) }}</label>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                                <div class="row ">
                                    <div class="form-group col-md-5">
                                        <label for="">Total</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right"></label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right"></label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">{{number_format($loading->total,2)}}</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">{{number_format($loading->commission,2)}}</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">{{number_format($loading->unit_discount,2)}}</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">{{number_format($loading->total_discount,2)}}</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">{{number_format($loading->net_amount,2)}}</label>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="form-group col-md-1">
                                        <label for="">#</label>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="">Name</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Qty</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Price</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Total</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Commission</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Unit Discount</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Total Discount</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Amount</label>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="col-md-12">
                <a class="btn btn-block btn-warning" href="{{ url()->previous() }}">Back</a>
            </div>
        </div>
    </div>

    @if((\Cartalyst\Sentinel\Laravel\Facades\Sentinel::check()->roles[0]->slug == 'accountant') || (\Cartalyst\Sentinel\Laravel\Facades\Sentinel::check()->roles[0]->slug == 'owner'))
    <div class="col-md-12">
        <div class="col-md-6"><h5> </h5></div>
    </div>
    <div class="col-md-12">
        <div class="col-md-6"><h5>  </h5></div>
    </div>

    <div class="col-md-12">
        <div class="col-md-6"><h5> Payment History </h5></div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="app-main-content px-3 py-4">
                <div class="content">
                    <div class="row ">

                        <div class="form-group col-md-4">
                            <label for="">Paid Date</label>
                        </div>
                        <div class="col-md-2 text-right">
                            <label class="text-right">Serial</label>
                        </div>
                        <div class="col-md-2 text-right">
                            <label class="text-right">Due Amount</label>
                        </div>
                        <div class="col-md-2 text-right">
                            <label class="text-right">Paid Amount</label>
                        </div>
                        <div class="col-md-2 text-right">
                            <label class="text-right">Description</label>
                        </div>
                    </div>


                    @php   $totpaid = 0; @endphp
               @if($loading->paid_amount != 0)

                        @foreach($pay_debit as $data_debit)

                            @if(!empty($data_debit->paid_invoices))

                                     @foreach(json_decode($data_debit->paid_invoices) as $debit_ids)
                                    @php // dd($debit_ids);     @endphp
                                         @if(!empty($debit_ids))


                                             @if($debit_ids->loading_header_id == $loading->id)

                                                 <div class="row ">

                                                     <div class="form-group col-md-4">
                                                         <label for="">{{$data_debit->created_at}}</label>
                                                     </div>
                                                     <div class="col-md-2 text-right">
                                                         <label class="text-right">{{$data_debit->debit_note_number}}</label>
                                                     </div>

                                                     <div class="col-md-2 text-right">
                                                         <label class="text-right">{{number_format($debit_ids->invoice_value,2)}}</label>
                                                     </div>

                                                     <div class="col-md-2 text-right">
                                                         <label class="text-right">{{number_format($debit_ids->settled_value,2)}}</label>
                                                     </div>
                                                     <div class="col-md-2 text-right">
                                                         <label class="text-right">DEBIT NOTE</label>
                                                     </div>

                                                 </div>

                                                 @php   $totpaid = $debit_ids->settled_value + $totpaid ; @endphp
                                             @endif

                                         @endif


                                     @endforeach
                                 @endif

                                 @endforeach

                            <div class="row ">
                            <div class="col-md-12 text-center">
                                <label class="text-center">------------------------------------------------------------------------------------------------</label>
                            </div>
                            </div>

                    @foreach($pay as $data)

                        @if(!empty($data->invoice_data) )

                            @foreach(json_decode($data->invoice_data) as $ids)
                                @php // dd($ids);    @endphp
                                @if(!empty($ids))


                                    @if($ids->loading_header_id == $loading->id)

                                        <div class="row ">
                                            @php   @endphp
                                            <div class="form-group col-md-4">
                                                <label for="">{{$data->created_at}}</label>
                                            </div>
                                            <div class="col-md-2 text-right">
                                                <label class="text-right">{{$data->serial}}</label>
                                            </div>

                                            <div class="col-md-2 text-right">
                                                <label class="text-right">{{number_format($ids->invoice_value,2)}}</label>
                                            </div>
                                            <div class="col-md-2 text-right">
                                                <label class="text-right">{{number_format($ids->settled_value,2)}}</label>
                                            </div>
                                            <div class="col-md-2 text-right">
                                                <label class="text-right">CASH-CHEQUE</label>
                                            </div>

                                        </div>

                                        @php   $totpaid = $ids->settled_value + $totpaid ; @endphp
                                    @endif
                                @endif
                            @endforeach
                        @endif
                        @endforeach

                   @endif




                    <div class="row ">

                        <div class="form-group col-md-4">
                            <label for=""></label>
                        </div>
                        <div class="col-md-2 text-right">
                            <label class="text-right">Total Paid</label>
                        </div>
                        <div class="col-md-2 text-right">
                            <label class="text-right"></label>
                        </div>
                        <div class="col-md-2 text-right">
                            <label class="text-right">{{number_format($totpaid,2)}}</label>
                        </div>
                    </div>
                    <div class="row ">

                        <div class="form-group col-md-4">
                            <label for=""></label>
                        </div>
                        <div class="col-md-2 text-right">
                            <label class="text-right">Balance Credit</label>
                        </div>
                        <div class="col-md-2 text-right">
                            <label class="text-right"></label>
                        </div>
                        <div class="col-md-2 text-right">
                            <label class="text-right">{{(int)(($loading->net_amount)-($totpaid))}}</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

@stop
@section('js')


    <script>
        function checkItem(id) {
            let name = '#end_product_commission_' + id;
            let valu_name = '#end_product_commission_value_' + id;
            if ($(name).prop("checked") == true) {
                $(valu_name).removeAttr('disabled');
            } else if ($(name).prop("checked") == false) {
                $(valu_name).attr('disabled');
            }
        }
    </script>

    {{--    <script type="text/javascript">--}}
    {{--        var maxcommission = [];--}}
    {{--        //max price array--}}
    {{--        var maxtotal = [];--}}

    {{--        function countCommission(e, id) {--}}
    {{--            $('.checkbox_' + id).change(function () {--}}
    {{--                //variables--}}
    {{--                let qty = $(".checkbox_" + id).data('id');--}}
    {{--                let price = $('.checkbox_' + id).data('price');--}}
    {{--                let commission = $(".checkbox_" + id).data('info');--}}

    {{--                var billAmount = price * qty;--}}
    {{--                maxtotal.push(billAmount);--}}
    {{--                var maxprice = 0;--}}
    {{--                for (var i = 0; i < maxtotal.length; i++) {--}}
    {{--                    maxprice += maxtotal[i] << 0;--}}
    {{--                }--}}

    {{--                var total = qty * commission;--}}
    {{--                maxcommission.push(total);--}}
    {{--                var totalx = 0;--}}
    {{--                for (var i = 0; i < maxcommission.length; i++) {--}}
    {{--                    totalx += maxcommission[i] << 0;--}}
    {{--                }--}}
    {{--                // alert(totalx);--}}
    {{--                $('#billAmount').html(maxprice);--}}
    {{--                $('#commission').html(totalx);--}}
    {{--            });--}}
    {{--        }--}}

    {{--        var totalDiscount = [];--}}

    {{--        function countDiscount(e, id) {--}}
    {{--            $(".discount_" + id).focusout(function () {--}}
    {{--                // alert(id);--}}
    {{--                let discount = $(this).val();--}}
    {{--                // alert(discount);--}}
    {{--                totalDiscount.push(discount);--}}
    {{--                //discount--}}
    {{--                var discountx = 0;--}}
    {{--                for (var i = 0; i < totalDiscount.length; i++) {--}}
    {{--                    discountx += totalDiscount[i] << 0;--}}
    {{--                }--}}
    {{--                //total commission--}}
    {{--                var totalx = 0;--}}
    {{--                for (var i = 0; i < maxcommission.length; i++) {--}}
    {{--                    totalx += maxcommission[i] << 0;--}}
    {{--                }--}}
    {{--                //total price--}}
    {{--                var maxprice = 0;--}}
    {{--                for (var i = 0; i < maxtotal.length; i++) {--}}
    {{--                    maxprice += maxtotal[i] << 0;--}}
    {{--                }--}}
    {{--                // alert(totalx);--}}
    {{--                var finalTotal = maxprice - (totalx + discountx);--}}
    {{--                // alert(finalTotal);--}}
    {{--                $('#Disc').html(discountx);--}}
    {{--                $('#finalTOT').html(finalTotal);--}}
    {{--            });--}}
    {{--        }--}}

    {{--        function checkOrderStatus(order) {--}}
    {{--            let url = 'item/loading/check/' + order;--}}
    {{--            $.ajax({--}}
    {{--                type: "get",--}}
    {{--                url: url,--}}
    {{--                success: function (response) {--}}
    {{--                    // console.log(response)--}}
    {{--                    if (response == 'true') {--}}
    {{--                        $('#btnSave').removeClass('d-none');--}}
    {{--                    } else {--}}
    {{--                        $('#btnSave').addClass('d-none');--}}
    {{--                    }--}}
    {{--                }--}}
    {{--            });--}}
    {{--        }--}}
    {{--    </script>--}}


@stop
