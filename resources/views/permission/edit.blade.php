<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Permission Group</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::select('permission_group', $permissionGroups,null, ['class' => 'form-control select3','placeholder'=>'Enter Permission Group','autocomplete'=>'off','id'=>'permission_group','required']) !!}
        </div>
    </div>
</div>
<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Name</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('name', null, ['class' => 'form-control','placeholder'=>'Enter Name','autocomplete'=>'off','id'=>'name','required']) !!}
        </div>
    </div>
</div>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script>
    $(document).ready(function() {

        $(".select3").select2({
            dropdownParent: $("#editModal"),

        });
    });
</script>

