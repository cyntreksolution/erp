@extends('layouts.back.master')@section('title','Bulk Burden Bake')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }
    </style>
@stop

@section('content')

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group">
                {!! Form::select('category', $categories , null , ['class' => 'form-control','placeholder'=>'Select Category','id'=>'category']) !!}
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group">
                {!! Form::select('end_product', $end_products , null , ['class' => 'form-control','placeholder'=>'Select End Product','id'=>'end_product']) !!}
            </div>
        </div>

        <div class="col-sm-4">
            <div class="form-group">
                <button type="button" class="btn btn-success" onclick="process_form()">Filter Now</button>
                <button type="button" class="btn btn-warning" onclick="process_form_reset()">Reset Filter</button>
            </div>
        </div>
    </div>

    {!! Form::open(['route' => 'bulk_burden_loading.store', 'method' => 'post']) !!}
    <div class="">
        <table id="esr_table" class="display text-center">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Serial</th>
                <th>Size</th>
                <th>Grade A</th>
                <th>Grade B</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Serial</th>
                <th>Size</th>
                <th>Grade A</th>
                <th>Grade B</th>
            </tr>
            </tfoot>
        </table>
        <div class="row">
            <div class="col-md-4">
                <input type="text" disabled value="0" class="form-control m-3" id="totalQty">
            </div>
            <div class="col-md-8">
                <button class="btn btn-block btn-warning m-3">For Loading</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script>
        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": false,
            "singleDatePicker": true,
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019"
        });
        $('#date_range').val('')

        $(document).ready(function () {
            table = $('#esr_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "ajax": {
                    url: "{{url('/bulk-burden-loading/table/data')}}",
                    type: "get",
                    error: function () {  // error handling code
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    }
                },
                "ordering": false,
                pageLength: 250,
                responsive: true
            });
        });

        function process_form() {
            let category = $("#category").val();
            let end_product = $("#end_product").val();
            let table = $('#esr_table').DataTable();
            $('#totalQty').val(0);
            table.ajax.url('/bulk-burden-loading/table/data?category=' + category + '&end_product=' + end_product + '&filter=' + true).load();
        }

        function process_form_reset() {
         $("#category").val('');
           $("#end_product").val('');
            $('#totalQty').val(0);
            let table = $('#esr_table').DataTable();
            table.ajax.url('/bulk-burden-loading/table/data').load();
        }


        function showTotal(box) {
            let qty = box.dataset["qty"];
            let totalQty = $('#totalQty').val();
            let newVal = 0;
            if (box.checked == true) {
                newVal = parseFloat(totalQty) + parseFloat(qty);
                $('#totalQty').val(newVal)
            } else {
                if (totalQty != 0) {
                    newVal =parseFloat(totalQty) - parseFloat(qty);
                    $('#totalQty').val(newVal)
                }
            }
        }

    </script>

@stop