@extends('layouts.back.master')@section('title','Database Backup')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
    {{--    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/css/dataTables.checkboxes.css" rel="stylesheet"/>--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }

        .abc {
            width: 1000px;
            height: 1000px;
            /* background-color: black; */
            z-index: 8000;

        }

        .sweet-alert {
            z-index: 7999 !important;
        }

        .sweet-overlay {
            z-index: 7998 !important;
        }

    </style>
@stop

@section('content')

    <div id="wait" class="d-none abc" style=""><img src='{{asset('assets/img/4V0b.gif')}}' width="64" height="64"/><br>Loading..
    </div>

    <div class="app-main-content mb-2">
        <div class="row mx-2">
                        <div class="col-md-10">
{{--                            {!! Form::select('shedule',['1'=>'Hourly','2'=>'Weekly','3'=>'Monthly'] , $shedule->status , ['class' => 'form-control','placeholder'=>'Select Shedule','id'=>'shedule']) !!}--}}
                        </div>
            <div class="col-md-2 p-0 float-right">
                <a href="{{route('db-backup.create')}}" class="btn btn-info  float-right">New Backup</a>

                <button class="btn btn-success float-right mr-2" data-toggle="modal"
                        data-target="#projects-task-modal"><i class='fa fa-cog'></i>
                </button>
            </div>



            <!-- loading view -->
        </div>
    </div>


    <div class="">
        <table id="invoice_table" class="display text-center">
            <thead>
            <tr>
                <th>#</th>
                <th>Created At</th>
                <th>File Size</th>
                <th>Downloads</th>
                <th>Action</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>#</th>
                <th>Created At</th>
                <th>File Size</th>
                <th>Downloads</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
        <div class="row">
            <div class="col-md-12 mt-5 ">
                <button type="submit" name="submit" value="summary" onclick="this.form.submit();this.disabled = true;"
                        class="d-none btn btn-block btn-warning" id="btn_sumbit">Merge Show Summary
                </button>
            </div>
            {{--            <div class="col-md-6 mt-5 float-right">--}}
            {{--                <button type="submit" name="submit"  value="merge" class="btn btn-block btn-success">Merge Selected</button>--}}
            {{--            </div>--}}
        </div>

    </div>

    <div class="modal fade" id="projects-task-modal" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="card">
                        <div class="projects-list">

                            <div class=" row">

                                <div class="col-sm-6">
                                    <div class="d-flex mr-auto">
                                        <div class="form-group">
                                            <label for="single">Set a Backup Schedule</label>

                                            {!! Form::select('shedule',['1'=>'Hourly','2'=>'Daily','3'=>'Weekly','4'=>'Monthly'] , $shedule->status , ['class' => 'form-control','placeholder'=>'Select Schedule','id'=>'shedule','style'=>"width: 15rem"]) !!}
                                            <span class="error"><p id="schedule_error"></p></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6" id="st_times">
                                    <div class="d-flex activity-counters justify-content-between">

                                        <div class="text-center px-6">

                                            <label for="single">Set Time</label>
                                            <input class="form-control" name="burden_count"
                                                   placeholder="" type="time" id="time" value="{{$shedule->time}}" style="width:300px">
                                            <span class="error"><p id="time_error"></p></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="d-flex mr-auto">
                                        <div class="form-group">
                                            <label for="single">Set a Delete Schedule</label>

                                            {!! Form::select('delete_schedule',['1'=>'1 Day Before','2'=>'3 Days Before','3'=>'Week Before','4'=>'Month Before','5'=>'3 Months Before'] , $shedule->delete_status , ['class' => 'form-control','placeholder'=>'Select Delete Schedule','id'=>'delete_schedule','style'=>"width: 15rem"]) !!}
                                            <span class="error"><p id="delete_schedule_error"></p></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <button id="sub_btn" class="btn btn-success btn-lg btn-block">
                                Save
                            </button>

                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>
    <input type="hidden" value="{{ session('msgbackup')? session('msgbackup'):'false'}}" id="msgbackup">
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    {{--    <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>--}}
    {{--    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js"></script>--}}

    <script>
        if ($('#msgbackup').val() == 'true') {
            swal("", "Database backup is starting. Please check after 5 min.")
        }
        $("#shedule").change(function () {

            var sheduleID = $(this).val();
            if (sheduleID == 2) {
                $("#st_times").show();
            } else {
                $("#st_times").hide();
            }
            {{--$.ajax({--}}
            {{--    url: "{{route('shedule.update')}}",--}}
            {{--    data : {sheduleID : sheduleID}--}}

            {{--}).done(function(response) {--}}
            {{--    console.log(response)--}}
            {{--});--}}
        });

        $("#sub_btn").click(function () {

            var shedule = $("#shedule").val();
            var time = $("#time").val();
            var delete_schedule = $("#delete_schedule").val();

            if (shedule !== null || shedule !== "") {
                if (shedule == 2) {
                    if (time == null || time === "") {
                        nameError = "Please enter Time";
                        document.getElementById("time_error").innerHTML = nameError;
                        return false;
                    } else {
                        if (delete_schedule == 2 || delete_schedule == 3 || delete_schedule == 4 || delete_schedule == 5) {
                            $.ajax({
                                url: "{{route('shedule.update')}}",
                                data: {sheduleID: shedule, time: time, delete_schedule: delete_schedule}

                            }).done(function (response) {
                                location.reload();
                                console.log(response)
                            });
                        } else {
                            nameError = "Please Select Delete duration more than a day";
                            document.getElementById("delete_schedule_error").innerHTML = nameError;
                            return false;
                        }

                    }
                } else if (shedule == 3) {
                    if (delete_schedule == 4 || delete_schedule == 5) {
                        $.ajax({
                            url: "{{route('shedule.update')}}",
                            data: {sheduleID: shedule, delete_schedule: delete_schedule}

                        }).done(function (response) {
                            location.reload();
                            console.log(response)
                        });
                    } else {
                        nameError = "Please Select Delete duration more than a week";
                        document.getElementById("delete_schedule_error").innerHTML = nameError;
                        return false;
                    }

                } else if (shedule == 4) {
                    if (delete_schedule == 5) {
                        $.ajax({
                            url: "{{route('shedule.update')}}",
                            data: {sheduleID: shedule, delete_schedule: delete_schedule}

                        }).done(function (response) {
                            location.reload();
                            console.log(response)
                        });
                    } else {
                        nameError = "Please Select Delete duration more than a month";
                        document.getElementById("delete_schedule_error").innerHTML = nameError;
                        return false;
                    }

                } else {

                    if (delete_schedule==1 || delete_schedule==2 || delete_schedule==3 || delete_schedule==4 || delete_schedule == 5) {
                        $.ajax({
                            url: "{{route('shedule.update')}}",
                            data: {sheduleID: shedule, delete_schedule: delete_schedule}

                        }).done(function (response) {
                            location.reload();
                            console.log(response)
                        });
                    }
                }
            } else {
                nameError = "Please Select Schedule";
                document.getElementById("schedule_error").innerHTML = nameError;
                return false;
            }
        });
    </script>

    <script>


        $(document).ready(function () {
            var sheduleID = $('#shedule').val();
            if (sheduleID == 2) {
                $("#st_times").show();
            } else {
                $("#st_times").hide();
            }
            table = $('#invoice_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "ajax": {
                    url: "{{url('/db-backup/dbBackup/request')}}", // json datasource
                    type: "get",
                    error: function () {  // error handling code
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    }
                },
                'columnDefs': [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                // 'select': {
                //     'style': 'multi'
                // },

                dom: 'Bfrtip',
                buttons: [
                    {
                        text: '',
                        className: 'btn btn-default icon-md icon-trash datatable-btn',
                        action: function (e, dt, node, config) {

                        }
                    }
                ],
                pageLength: 100,
                responsive: true
            });
        });

        function dataTableLoad() {
            table = $('#invoice_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "ajax": {
                    url: "{{url('/db-backup/dbBackup/request')}}", // json datasource
                    type: "get",
                    error: function () {  // error handling code
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    }
                },
                'columnDefs': [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                // 'select': {
                //     'style': 'multi'
                // },

                dom: 'Bfrtip',
                buttons: [
                    {
                        text: '',
                        className: 'btn btn-default icon-md icon-trash datatable-btn',
                        action: function (e, dt, node, config) {

                        }
                    }
                ],
                pageLength: 25,
                responsive: true
            });
        }


    </script>
    <script>
        function deleteDBbackup(backup) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "post",
                        url: '/db-backup/delete',
                        data: {
                            backup_id: backup,
                        },
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            } else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                });
        }
    </script>
@stop
