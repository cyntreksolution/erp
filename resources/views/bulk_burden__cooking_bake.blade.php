@extends('layouts.back.master')@section('title','Bulk Cooking Bake')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }
    </style>
@stop

@section('content')
    {{ Form::open(array('url' => '/esr/excel/data','id'=>'filter_form'))}}
    <div class="row">

        {{--            <div class="col-sm-3">--}}
        {{--                <div class="form-group">--}}
        {{--                    {!! Form::text('date',null,['class' => 'form-control','id'=>'date_range','placeholder'=>'Select Date']) !!}--}}
        {{--                </div>--}}
        {{--            </div>--}}

        {{--            <div class="col-sm-3">--}}
        {{--                <div class="form-group">--}}
        {{--                    {!! Form::select('status', [1=>'pending',2=>'received',3=>'creating',4=>'burning',5=>'grade',6=>'finished',] , null , ['class' => 'form-control','placeholder'=>'Status','id'=>'status']) !!}--}}
        {{--                </div>--}}
        {{--            </div>--}}
        {{--            <div class="col-sm-3">--}}
        {{--                <div class="form-group">--}}
        {{--                    <input type="text" name="date_range" id="date_range" class="form-control">--}}
        {{--                </div>--}}
        {{--            </div>--}}

        {{--            <div class="col-sm-3">--}}
        {{--                <div class="form-group">--}}
        {{--                    <button type="button" class="btn btn-success" onclick="process_form()">Filter Now</button>--}}
        {{--                    <button type="button" class="btn btn-warning" onclick="process_form_reset()">Reset Filter</button>--}}
        {{--                    <button type="submit" name="action" value="download_data" class="btn btn-info"><i--}}
        {{--                                class="fa fa-download"></i></button>--}}
        {{--                </div>--}}
        {{--            </div>--}}
    </div>
    {{ Form::close() }}
    {!! Form::open(['route' => 'short_bulk_burden_bake.store', 'method' => 'post']) !!}
    <div class="">
        <table id="esr_table" class="display text-center">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Serial</th>
                <th>Size</th>
                <th>Expected Qty</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Serial</th>
                <th>Size</th>
                <th>Expected Qty</th>
            </tr>
            </tfoot>
        </table>
        <div class="row">
            <div class="col-md-6">
               {{--!! Form::select('employee', $employees , null , ['class' => 'form-control mt-3','placeholder'=>'Select Employee','required']) !!--}}
                <select class="form-control mt-3" name="employee" placeholder="Select Team" required>
                    @foreach($teams as $team)
                        <option value="{{$team->id}}">{{ str_replace(str_split('\\/:*?"<>|[]'),'',$team->employees->pluck('full_name')) }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-6">
                @can('bulk_short_burden_bake-create')
                <button class="btn btn-block btn-warning m-3" onclick="confirmAlert()">BAKE SHORT EATS</button>
                @endcan
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script>
        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": false,
            "singleDatePicker": true,
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019"
        });
        $('#date_range').val('')

        $(document).ready(function () {
            table = $('#esr_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "ajax": {
                    url: "{{url('/short-bulk-burden-bake/table/data')}}",
                    type: "get",
                    error: function () {  // error handling code
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    }
                },
                pageLength: 250,
                responsive: true
            });
        });
    </script>

@stop