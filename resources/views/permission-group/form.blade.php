<div class="form-group row">
    <label class="col-form-label text-right col-lg-2 col-sm-12">Name</label>
    <div class="col-lg-10 col-md-10 col-sm-12">
        <div class="input-group">
            {!! Form::text('name', null, ['class' => 'form-control','placeholder'=>'Enter Name','autocomplete'=>'off','id'=>'name','required']) !!}
        </div>
    </div>
</div>