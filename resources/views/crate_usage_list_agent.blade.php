
@extends('layouts.back.master')@section('title','Agents Crates Inquiry')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }
    </style>


    <style type="text/css">

        #floating-button{
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #00C851;
            position: fixed;
            bottom: 80px;
            right: 30px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index:2
        }

        .plus{
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }
        .sml{
            height: 30px;
            width:50px;
            text-align: center;
            font-size: 0.75rem;
        }
    </style>

    {{--<link rel="stylesheet" href="{{asset('assets/vendor/bower_components/datatables.net/css/jquery.dataTables.min.css')}}">--}}
    {{--<link rel="stylesheet" href="{{asset('assets/vendor/dtable/Buttons-1.5.1/css/buttons.dataTables.css')}}">--}}
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/Buttons-1.5.1/css/buttons.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    {{--<link rel="stylesheet" href="{{asset('assets/vendor/dtable/DataTables-1.10.16/css/dataTables.bootstrap4.css')}}">--}}
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/datatables.net/css/buttons.dataTables.min.css')}}">
@stop

@section('content')

    {{-- @if((Sentinel::check()->roles[0]->slug=='owner') || (Sentinel::check()->roles[0]->slug == 'stores-end-product')) --}}
    <div class="row">
        @php
        $dt = \Carbon\Carbon::today()->format('Y-m-d');

        @endphp
        <div class="col-md-3">
            <input type="text" name="date" id="date_range" class="form-control" required value="{{$dt}}-{{$dt}}">
        </div>
        @if(Auth::user()->hasRole(['Sales Agent','Super Admin']) )
            <div class="form-group col-sm-2">
                <input type="hidden" name="agent" id="agent" value="{{Auth::user()->id}}">

            </div>
            <div class="form-group col-sm-2">
                <input type="hidden" name="referenceType" id="referenceType" value="">

            </div>
        @else
        <div class="col-md-2">
            {!! Form::select('agent',$agents , null , ['class' => 'form-control','placeholder'=>'Select Agent','id'=>'agent']) !!}
        </div>


        <div class="col-md-2">
            <select name="referenceType" id="referenceType" class="form-control select2 ">
                <option value="">Select Move Type</option>
                <option value="100">Opening Stock</option>
                <option value="200">Day End Balance Stock</option>
                <option value="1">Creates issue with invoice</option>
                <option value="2">Approved Return Creates</option>
                <option value="3">Update Invoiced Crates</option>
                <option value="21">Admin Crates Adjustment</option>
            </select>
        </div>
        @endif


        <div class="col-md-4">
            <button class="btn btn-info " id="filterBTN" onclick="process_form(this)"><i class="fa fa-filter"></i></button>
            <!--button onclick="cratePrint()" target="_blank" class="btn btn-warning" id="printBTN"><i class="fa fa-print"></i>
            </button-->
            <a onclick="cratePrint()" class="btn btn-warning" id="printBTN" target="_blank"><i class="fa fa-print"></i> </a>
        </div>

        <!-- loading view -->


    </div>


    <div class="table-responsive">
        <table id="raw_table" class="display text-center ">
            <thead>
            <tr>
                <th>Date & Time</th>
                <th>Agent with Refference</th>
                <th>BC[L]</th>
                <th>BC[S]</th>
                <th>CR[L]</th>
                <th>CR[S]</th>
                <th>LID</th>
                <th>Move Type</th>
            </tr>
            </thead>
            <tfoot>
            <tr>

                <th>Date & Time</th>
                <th>Agent with Refference</th>
                <th>BC[L]</th>
                <th>BC[S]</th>
                <th>CR[L]</th>
                <th>CR[S]</th>
                <th>LID</th>
                <th>Move Type</th>

            </tr>
            </tfoot>
        </table>
    </div>

@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/Buttons-1.5.1/js/dataTables.buttons.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/JSZip-2.5.0/jszip.min.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/pdfmake-0.1.32/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/Buttons-1.5.1/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js')}}"></script>
    <script>


        var table;
        $(document).ready(function () {
            table = $('#example').DataTable({
                responsive: true,
                "ajax": '{{url('/crates-inquiry-agent/list/table/data')}}',
                dom: 'lfBrtip',

                buttons: [
                    {
                        extend: 'copy',
                        className: 'btn btn-xs btn-info ml-3 sml assets-export-btn export-copy ttip',
                    },

                    { extend: 'excel',
                        className: 'btn btn-xs btn-success ml-3 sml assets-export-btn export-copy ttip',
                    },
                    { extend: 'pdf',
                        className: 'btn btn-xs btn-danger ml-3 sml assets-export-btn export-copy ttip',
                    },

                    { extend: 'print',
                        className: 'btn btn-xs btn-primary ml-3 sml assets-export-btn export-copy ttip',
                    },
                ]

            });

            $('#example tbody').on( 'click', 'td button', function (e) {
                // var data = table.row( $(this).parents('tr') ).data();
                e.preventDefault();
                var id = $(this).attr("value");
                confirmAlert(id);

            } );
        });



        function confirmAlert(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "delete",
                        url: id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            }
                            else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }



        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019",
            "maxDate": moment()
        });


      //  $('#date_range').val('')
        $(document).ready(function () {
            table = $('#raw_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "ajax": {
                    url: "{{url('/crates-inquiry-agent/list/table/data')}}",
                    type: "get",
                    error: function () {
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    }
                },
                pageLength: 100,
                responsive: true
            });
        });




        $(document).ready(function () {
            $("#printBTN").hide();

        });
        $("#agent").change(function () {
            let agent = $('#agent').val();
            let date = $('#date_range').val();


            if (agent == '') {

                $("#printBTN").hide();

            }else {
                if (date != '') {
                    $("#printBTN").show();

                    $.ajax({
                        url: "/crates-inquiry-agent/print/download/" + agent + "/" + date,
                        type: "GET",
                        success: function (data) {
                            console.log(data);
                            appendRawList(data);
                        },
                        error: function (xhr, status) {

                        }
                    });
                }else{
                    $("#printBTN").hide();
                }
            }

        }).trigger("change");



        function process_form(e) {
            let raw_material = $("#raw_material").val();
            let date_range = $("#date_range").val();
            let referenceType = $("#referenceType").val();
            let agent = $("#agent").val();
            let table = $('#raw_table').DataTable();
            table.ajax.url('/crates-inquiry-agent/list/table/data?crate=' + raw_material + '&date_range=' + date_range + '&agent=' + agent + '&reference_type=' + referenceType + '&filter=' + true).load();
        }


            function cratePrint()
            {
                let date_range = $("#date_range").val();
                let ref = $("#referenceType").val();
                let agent = $("#agent").val();
                window.location.href = "/crates-inquiry-agent/print/download?agent="+ agent + "&date=" + date_range + "&ref=" + ref + "&filter=" + true;
            }


        function process_form_reset() {
            $("#raw_material").val('');

            let table = $('#raw_table').DataTable();
            table.ajax.url('/crates-inquiry-agent/list/table/data').load();
        }



    </script>

@stop

