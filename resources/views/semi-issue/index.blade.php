@extends('layouts.back.master')@section('title','Issue Semi Finish Products')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.wizard.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/modal.css')}}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/demos.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/css/site.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/packages/recipe/recipe.css')}}">
@stop
@php
    use App\SemiInquiry;
@endphp

@section('content')
    <div class="app-wrapper">
        <div class="app-panel" id="app-panel">
            <div class="app-search">
                <input type="search" class="search-field" placeholder="Search" disabled>
                <i class="search-icon fa fa-search"></i>
            </div>

            <div class="scroll-container">
                <div class="app-panel-inner">
                    @can('semi_issue-create')
                    <div class="p-3">
                        <button class="btn btn-success py-3 btn-block btn-lg" data-toggle="modal"
                                data-target="#projects-task-modal">Issue Semi Finish Products
                        </button>
                    </div>
                    @endcan
                    <hr class="m-0">
                        @can('semi_issue-reUsableWestage')
                    <div class="p-3">
                        <button class="btn btn-warning py-3 btn-block btn-lg" data-toggle="modal"
                                data-target="#projects-task-modal-2">Re Usable Wastage
                        </button>
                    </div>
                        @endcan
                    <hr class="m-0">
                    <div class="people-list d-flex justify-content-start flex-wrap p-3">

                    </div>
                    <hr class="m-0">
                    <div class="media-list" id="container">


                    </div>
                </div>
            </div>
            <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show"><i
                        class="fa fa-chevron-right"></i>
                <i class="fa fa-chevron-left"></i>
            </a>
        </div>
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">Today Issued Semi Finish List</h5>
            </div>

            <div class="scroll-container">
                <div class="app-main-content">
                    <div class="media-list" id="rcontainer">
                        @foreach($transaction as $t)
                            @php

                                $semi_name = \SemiFinishProductManager\Models\SemiFinishProduct::where('semi_finish_product_id','=',$t->semi_id)->first();
                                if($semi_name->is_bake == 1)
                                    {
                                        $bid = \BurdenManager\Models\Burden::where('id','=',$t->burden_id)->first();
                                    }else{
                                        $bid = \ShortEatsManager\Models\CookingRequest::where('id','=',$t->burden_id)->first();
                                    }

                            @endphp
                            @if($t->type==1)
                                <div class="media">
                                    <div class="avatar avatar-sm bg-success">+
                                    </div>
                                    <div class="media-body">

                                        <h6 class="media-heading" id="media-list-item-3">{{$semi_name->name}}
                                            =>{{$t->description}}</h6>
                                        <small>{{$t->created_at->format('d - M - Y @ h :i: s')}}</small>
                                        <h6>{{$bid->serial}}</h6>
                                    </div>
                                    <h6 class="media-heading">{{$t->qty}}</h6>
                                </div>
                            @else
                                @if($t->category==0)
                                    <div class="media">
                                        <div class="avatar avatar-sm bg-warning">-
                                        </div>
                                        <div class="media-body">
                                            <h6 class="media-heading"
                                                id="media-list-item-3">{{$semi_name->name}}=>{{$t->description}}</h6>
                                            <small>{{$t->created_at->format('d - M - Y @ h :i: s')}}</small>
                                            <h6>{{$bid->serial}}</h6>
                                        </div>
                                        <h6 class="media-heading">{{$t->qty}}</h6>
                                    </div>
                                @elseif($t->category==1)
                                    <div class="media">
                                        <div class="avatar avatar-sm bg-danger">-
                                        </div>
                                        <div class="media-body">
                                            <h6 class="media-heading"
                                                id="media-list-item-3">{{$semi_name->name}}=>{{$t->description}}</h6>
                                            <small>{{$t->created_at->format('d - M - Y @ h :i: s')}}</small>
                                            <h6>{{$bid->serial}}</h6>
                                        </div>
                                        <h6 class="media-heading">{{$t->qty}}</h6>
                                    </div>
                                @endif
                            @endif
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="projects-task-modal" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12">
                        <div class="card">

                            {{ Form::open(array('url' => 'semi-issue','id'=>'end-recipe-form')) }}
                            <div class="wizard p-4" id="bootstrap-wizard-1">
                                <ul class="nav nav-tabs vertices keep-prefix-suffix" role="tablist">
                                    <li class="nav-item">
                                        <a disabled="true" class=" nav-link active" data-toggle="tab" href="#ex5-step-1"
                                           role="tab">
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#ex5-step-2"
                                           role="tab">
                                        </a>
                                    </li>

                                </ul>
                                <div class="tab-content">
                                    {{--<div class="tab-pane active" id="ex5-step-1" role="tabpanel">--}}
                                    <div class="tab-pane active" id="ex5-step-1" role="tabpanel">
                                        <div class="task-name-wrap">

                                                @if (Auth::user()->hasRole(['Owner','Production Manager']) )
                                            {!! Form::select('type', ['End Product Burden Request','Production Purpose','Customize Cake','Destroy and Return','Admin Adjustment'] , null , ['placeholder'=>'Select One','required','class' => 'form-control select2','id'=>"type"]) !!}
                                            @else
                                                {!! Form::select('type', ['End Product Burden Request','Production Purpose','Customize Cake','Destroy and Return'] , null , ['placeholder'=>'Select One','required','class' => 'form-control select2','id'=>"type"]) !!}

                                            @endif
                                        </div>

                                        <hr>
                                        <div class="task-name-wrap d-none" id="div_merge_serial">
                                            @php// dd($merges);  @endphp
                                            {!! Form::select('merge_serial', $merges , null , ['placeholder'=>'Select Merge  Serial','class' => 'form-control select2','required','id'=>"merge_serial"]) !!}
                                        </div>

                                        <div id="div_description" class="d-none">
                                            <div class="row">
                                                <div class="col-md-6 mb-2">
                                                    <div class="task-name-wrap">
                                                        {!! Form::select('semi_finish',$semi_finish , null , ['placeholder'=>'Select Semi Finish Product','class' => 'form-control select2' ,'id'=>'semi_finish']) !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="task-name-wrap">
                                                        {!! Form::text('qty',null, ['placeholder'=>'Quantity','class' => 'form-control select2','required','id'=>'qty','onKeyUp'=>'updateQty()']) !!}
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="task-name-wrap">
                                                {!! Form::textarea('description', null, ['class' => 'form-control','id'=>'description','required','placeholder'=>'Description Here...']) !!}
                                            </div>
                                        </div>

                                        <div id="div_description_customize_cake" class="mb-2 d-none div-desc">
                                            <div class="row mb-2">
                                                <div class="col-md-12">
                                                    {!! Form::select('semi_finish_c', $semi_finish , null , ['placeholder'=>'Select Semi Finish','required','class' => 'form-control select2','id'=>'semi_finish_c']) !!}
                                                </div>
                                            </div>


                                            <div class="row mb-2">
                                                <div class="col-md-8">
                                                    {!! Form::select('end_bid', $custo1 , null , ['placeholder'=>'Select End Burden','required','class' => 'form-control select2','id'=>'end_bid']) !!}
                                                </div>
                                                <div class="col-md-4">
                                                {!! Form::text('customized_qty[]', 0 , ['id'=>'qty','class' => 'form-control cc-qty','required','placeholder'=>'Enter Quantity','onKeyUp'=>'updateQTYC()']) !!}   {{--  --}}
                                                </div>
                                            </div>

                                           <div class="row mb-2" id="customize_cake_list">
                                                @foreach($custo as $item)
                                                    <div class="col-md-6">
                                                        <p>{{optional($item->endProduct)->name}}</p>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <p>{{$item->serial}}</p>
                                                        <input type="hidden" name="customized_burden_id[]" value="{{$item->id}}">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="col-md-4">
                                                            <p>{{$item->burden_size}}</p>
                                                        </div>
                                                  {{--      {!! Form::text('customized_qty[]', $item->burden_size, ['id'=>'qty','class' => 'form-control cc-qty','required','placeholder'=>'Enter Quantity','readonly']) !!}   {{-- 'onKeyUp'=>'updateQTYC()' --}}
                                                    </div>
                                                @endforeach

                                                <div class="col-md-4">
                                                    <p>Total QTY</p>
                                                </div>
                                                <div class="col-md-4">
                                                    <p id="total_update_qty">0.00</p>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                    <div class="tab-pane" id="ex5-step-2" role="tabpanel">
                                        <div id="row_material_serial"></div>
                                        <hr>
                                        <div class="task-name-wrap" id="header_list">
                                            <div class="row">
                                                <div class="col-md-4">Semi Finish Product</div>
                                                <div class="col-md-2 text-nowrap text-right">Available Qty</div>
                                                <div class="col-md-3 text-right">Expected Qty</div>
                                                <div class="col-md-3 text-right">Issued Qty</div>
                                            </div>
                                        </div>
                                        <div id="row_material_list"></div>
                                    </div>

                                </div>
                                <div class="pager d-flex justify-content-center mt-3">
                                    <input type="submit" id="finish-btn" class="finish d-none btn btn-success w-50"
                                           value="Issue"/>

                                    <button type="button" id="next-btn" class="next btn btn-success w-50">Next
                                    </button>
                                </div>
                            </div>
                            {{ Form::close() }}

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="projects-task-modal-2" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12">
                        <div class="card">

                            {{ Form::open(array('url' => 'semi-issue/reusable/wastage','id'=>'end-recipe-form')) }}
                            <div class="wizard p-4" id="bootstrap-wizard-1">
                                <ul class="nav nav-tabs vertices keep-prefix-suffix" role="tablist">
                                    <li class="nav-item">
                                        <a disabled="true" class=" nav-link active" data-toggle="tab" href="#ex5-step-1"
                                           role="tab">
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    {{--<div class="tab-pane active" id="ex5-step-1" role="tabpanel">--}}
                                    <div class="tab-pane active" id="ex5-step-1" role="tabpanel">
                                        <div class="row">
                                            <div class="col-md-6 mb-2">
                                                <div class="task-name-wrap">
                                                    {!! Form::select('semi_finish',$semi_finish_reuse , null , ['placeholder'=>'Select Semi Finish Product','class' => 'form-control select2' ,'id'=>'semi_finish']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="task-name-wrap">
                                                    {!! Form::text('qty',null, ['placeholder'=>'Quantity','class' => 'form-control select2','required','id'=>'qty','onKeyUp'=>'updateQTY()']) !!}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="task-name-wrap">
                                            {!! Form::textarea('description', null, ['class' => 'form-control','id'=>'description','required','placeholder'=>'Description Here...']) !!}
                                        </div>
                                    </div>


                                </div>

                            </div>
                            <div class="pager d-flex justify-content-center mt-3">
                                <input type="submit" class="finish btn btn-success w-50" value="Issue"/>
                            </div>
                        </div>
                        {{ Form::close() }}

                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>
@stop
@section('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    {{--    <script src="{{asset('assets/examples/js/apps/projects.js')}}"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('assets/packages/semi_finish/semi_finish.js')}}"></script>
    <script src="{{asset('assets/packages/recipe/end-recipe.js')}}"></script>

    <script>

        function updateQTYC(){
            let semi = $("#semi_finish_c option:selected").val();

            let semiLoadedName = '#semi_loaded_qty_' + semi;
            let semiLoadedLabel = '#label_' + semi;

            console.log(semi)
            let total = 0;
            $('.cc-qty').each(function(){
                total += parseFloat(this.value);
            });
            $('#total_update_qty').empty();
            $('#total_update_qty').append(total);

            $(semiLoadedName).val(total)


            let labelRemain = '#label_remain_' + semi;
            $(labelRemain).text(total);
            $(semiLoadedLabel).text(total);
        }

        $("#type").change(function () {
            let type = $("#type option:selected").text();
            let typeVal = $("#type option:selected").val();

            if (typeVal == 0) {
                $('#div_merge_serial').removeClass('d-none');
                $('#div_description').addClass('d-none');
                $('#div_description_customize_cake').addClass('d-none');
            } else if (typeVal == 2) {
                $('#header_list').addClass('d-none');
                $('.div-desc').addClass('d-none');
                $('#div_merge_serial').addClass('d-none');
                $('#div_description').addClass('d-none');
                $('#div_description_customize_cake').removeClass('d-none');
            } else {
                $('#header_list').addClass('d-none');
                $('#div_description_customize_cake').addClass('d-none');
                $('#div_description').removeClass('d-none');
                $('#div_merge_serial').addClass('d-none');
            }

        }).trigger("change");

        $("#merge_serial").change(function () {
            let type = $("#merge_serial option:selected").text();
            let typeVal = $("#merge_serial option:selected").val();
            $('#row_material_list').empty();

            $.ajax({
                url: "/issue/semi/merged/burden/" + typeVal,
                type: "GET",
                success: function (data) {
                    appendRawList(data, type);
                },
                error: function (xhr, status) {

                }
            });
        }).trigger("change");


        function updateQty() {
            let semi = $("#semi_finish option:selected").val();

            let qty = $("#qty").val();

            let semiLoadedName = '#semi_loaded_qty_' + semi;
            let semiLoadedLabel = '#label_' + semi;


            $(semiLoadedName).val(qty)


            let labelRemain = '#label_remain_' + semi;
            $(labelRemain).text(qty);
            $(semiLoadedLabel).text(qty);

        }

        $("#semi_finish").change(function () {
            let type = $("#merge_serial option:selected").text();
            let typeVal = $("#merge_serial option:selected").val();


            let semi = $("#semi_finish option:selected").val();
            // let qty = $("#qty").val();


            $('#row_material_list').empty();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            $.ajax({
                url: "/semi_finish_product/semi/burden/available/list/" + semi,
                type: "GET",
                success: function (data) {
                    appendAvailableSemi(data, type);
                },
                error: function (xhr, status) {

                }
            });
        }).trigger("change");
        $("#semi_finish_c").change(function () {
            let type = $("#merge_serial option:selected").text();
            let typeVal = $("#merge_serial option:selected").val();


            let semi = $("#semi_finish_c option:selected").val();
            // let qty = $("#qty").val();


            $('#row_material_list').empty();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            $.ajax({
                url: "/semi_finish_product/semi/burden/available/list/" + semi,
                type: "GET",
                success: function (data) {
                    appendAvailableSemi(data, type);
                },
                error: function (xhr, status) {

                }
            });
        }).trigger("change");


        function appendAvailableSemi(semi, type) {
            $('#row_material_serial').empty();
            $('#row_material_serial').append(semi);
        }


        function appendRawList(rawList, type) {
            let flag = 0;
            $('#row_material_serial').empty();
            $('#row_material_serial').append('' +
                ' <div class="task-name-wrap">\n' +
                '                                            <div class="row mt-2">\n' +
                '                                                <div class="col-md-6">Merge Serial</div>\n' +
                '                                                <div class="col-md-6 text-right">' + type + '</div>\n' +
                '                                            </div>\n' +
                '                                        </div>')


            $.each(rawList, function (index, value) {
                let available = parseFloat(value.available_qty).toFixed(3)
                let qty = parseFloat(value.qty).toFixed(3)
                let err = parseFloat(available) < parseFloat(qty) ? 1 : 0;

                let dis = (value.loading_type == 'to_loading') ? 1 : 0;
                if (err) {
                    $('#row_material_list').append('' +
                        ' <div class="task-name-wrap">\n' +
                        '                                            <div class="row mt-2">\n' +
                        '                                                <div class="col-md-4 text-nowrap text-danger">' + value.semi_name + '<input type="hidden" name="semi[]" value="' + value.id + '"></div>\n' +
                        '                                                <div class="col-md-2 text-nowrap text-danger text-right">' + Number(value.available_qty).toFixed(3) + '</div>\n' +
                        '                                                <div class="col-md-3 text-nowrap text-danger text-right">' + Number(value.qty).toFixed(3) + '</div>\n' +
                        // '                                                <div class="col-m text-nowrapd-3 text-danger text-right"><input disabled type="number" name="qty[]" value="' + value.qty + '" class="form-control"> </div>\n' +
                        '                                                <div class="col-md-3 text-nowrap text-danger text-right"><input readonly type="text" name="qty[]" step="0.001" value="' + Number(value.qty).toFixed(3) + '" class="form-control text-right"> </div>\n' +
                        '                                            </div>\n' +
                        '                                        </div>')
                    flag = 1;
                } else {
                    if (dis) {
                        $('#row_material_list').append('' +
                            ' <div class="task-name-wrap">\n' +
                            '                                            <div class="row mt-2">\n' +
                            '                                                <div class="col-md-4 text-nowrap">' + value.semi_name + '<input type="hidden" name="semi[]" value="' + value.id + '"></div>\n' +
                            '                                                <div class="col-md-2 text-nowrap text-right"> ' + Number(value.available_qty).toFixed(3) + '</div>\n' +
                            '                                                <div class="col-md-3 text-nowrap text-right">' + Number(value.qty).toFixed(3) + '</div>\n' +
                            '                                                <div class="col-md-3 text-nowrap text-danger text-right"><input readonly type="text" name="qty[]" step="0.001" min="' + Number(value.qty).toFixed(3) + '" value="' + Number(value.qty).toFixed(3) + '" class="form-control text-right"></div>\n' +
                            '                                            </div>\n' +
                            '                                        </div>')
                    } else {
                        $('#row_material_list').append('' +
                            ' <div class="task-name-wrap">\n' +
                            '                                            <div class="row mt-2">\n' +
                            '                                                <div class="col-md-4 text-nowrap">' + value.semi_name + '<input type="hidden" name="semi[]" value="' + value.id + '"></div>\n' +
                            '                                                <div class="col-md-2 text-nowrap text-right"> ' + Number(value.available_qty).toFixed(3) + '</div>\n' +
                            '                                                <div class="col-md-3 text-nowrap text-right">' + Number(value.qty).toFixed(3) + '</div>\n' +
                            '                                                <div class="col-md-3 text-nowrap text-danger text-right"><input type="text" name="qty[]" step="0.001" min="' + Number(value.qty).toFixed(3) + '" value="' + Number(value.qty).toFixed(3) + '" class="form-control text-right"></div>\n' +
                            '                                            </div>\n' +
                            '                                        </div>')
                    }

                }
            });
            if (flag) {
                $('#finish-btn').addClass('d-none');
            } else {
                $('#finish-btn').removeClass('d-none');
            }
        }


        function checkBoxClick(box) {
            let semi = box.dataset["semi"];
            let grade = box.dataset["grade"];
            let type = box.dataset['typex'];

            let inputBoxBurdenName = '#burden_request_qty_' + grade
            let inputBoxCookingName = '#cooking_request_qty_' + grade

            let burdenQTYName = '#burden_expected_qty_' + grade;
            let cookingQTYName = '#cooking_expected_qty_' + grade;

            let burdenQTY = parseFloat($(burdenQTYName).text());
            let cookingQTY = parseFloat($(cookingQTYName).text());


            let neededQtyName = '#semi_requested_qty_' + semi;
            let neededQty = $(neededQtyName).val();


            let semiLoadedName = '#semi_loaded_qty_' + semi;
            let semiLoaded = parseFloat($(semiLoadedName).val());

            let LblRemainName = '#label_remain_' + semi;
            let LblRemain = $(LblRemainName).text();

            if (box.checked) {
                if (type == 'burden_request') {
                    if (semiLoaded > 0) {
                        $(inputBoxBurdenName).attr('readonly', true)
                        if (semiLoaded >= burdenQTY) {
                            burdenQTY = parseFloat(burdenQTY).toFixed(3);
                            $(inputBoxBurdenName).val(burdenQTY)
                            $(semiLoadedName).val(semiLoaded - burdenQTY);
                        } else {
                            $(inputBoxBurdenName).val(semiLoaded)
                            $(semiLoadedName).val(semiLoaded - semiLoaded);
                        }
                    } else {
                        swal("Sorry!", "Quantity is Zero", "warning");
                        box.checked = false;
                    }
                } else {
                    if (semiLoaded > 0) {
                        $(inputBoxCookingName).attr('readonly', true)
                        if (semiLoaded >= cookingQTY) {
                            cookingQTY = parseFloat(cookingQTY).toFixed(3);
                            $(inputBoxCookingName).val(cookingQTY)
                            $(semiLoadedName).val(semiLoaded - cookingQTY);
                        } else {
                            $(inputBoxCookingName).val(semiLoaded)
                            $(semiLoadedName).val(semiLoaded - semiLoaded);
                        }
                    } else {
                        swal("Sorry!", "Quantity is Zero", "warning");
                        box.checked = false;
                    }
                }
            } else {
                if (type == 'burden_request') {
                    $(inputBoxBurdenName).attr('readonly', false)
                    let textVal = parseFloat($(inputBoxBurdenName).val());
                    $(inputBoxBurdenName).val(0)
                    $(semiLoadedName).val(semiLoaded + textVal);
                } else {
                    $(inputBoxCookingName).attr('readonly', false)
                    let textVal = parseFloat($(inputBoxCookingName).val());
                    $(inputBoxCookingName).val(0)
                    $(semiLoadedName).val(semiLoaded + textVal);
                }
            }

            semiLoaded = parseFloat($(semiLoadedName).val()).toFixed(3);
            $(LblRemainName).empty();
            $(LblRemainName).append(semiLoaded);


            let inputs = $('.semi-loaded-class');
            let arr = [];
            for (var i = 0; i < inputs.length; i++) {
                arr[i] = $(inputs[i]).val();
            }

            var new_arr = arr.filter(function (x) {
                return x > 0;
            });

            console.log(new_arr)
            if (new_arr.length == 0) {
                $('#finish-btn').removeClass('d-none');
            } else {
                $('#finish-btn').addClass('d-none');
            }

        }
    </script>
@stop
