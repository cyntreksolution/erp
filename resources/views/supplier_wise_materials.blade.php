@extends('layouts.back.master')@section('title','Supplier Wise Materials')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }
    </style>
@stop
@section('content')
    {{ Form::open(array('url' => '/supplier_wise_materials/download','id'=>'filter_form'))}}
    <div class="row">
        <div class="col-sm-3">
            <div class="form-group">
                {!! Form::select('suppliers',$suppliers , null , ['class' => 'form-control','placeholder'=>'Select supplier','id'=>'supplier']) !!}
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                {!! Form::select('item', $items , null , ['class' => 'form-control','placeholder'=>'Select Item','id'=>'item']) !!}
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <input type="text" name="date_range" id="date_range" class="form-control">
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <button type="button" class="btn btn-success" onclick="process_form('#filter_form')">Filter data</button>
                <button type="submit" name="action" value="download_data" class="btn btn-info"><i class="fa fa-download"></i></button>
            </div>
        </div>


    </div>
    {{ Form::close() }}
    <div class="">
        <table id="supplier_wise_materials_table" class="display text-center">
            <thead>
            <tr>
                <th>Order Date</th>
                <th>Invoice No.</th>
                <th>Item Name</th>
                <th>Supplier Name</th>
                <th>Order Price</th>
                <th>Invoice Price</th>
                <th>Ordered Qty</th>
                <th>Received Qty</th>
                <th>Value</th>
                <th>Remark</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Order Date</th>
                <th>Invoice No.</th>
                <th>Item Name</th>
                <th>Supplier Name</th>
                <th>Order Price</th>
                <th>Invoice Price</th>
                <th>Ordered Qty</th>
                <th>Received Qty</th>
                <th>Value</th>
                <th>Remark</th>
            </tr>
            </tfoot>
        </table>
    </div>
@stop
@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script>
        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019"
        });
        $('#date_range').val('')


        
        var supplier_id = 'all';
        var start_date = moment().subtract(1, 'days').format("YYYY-MM-DD");
        var end_date = moment().format("YYYY-MM-DD");
        $(document).ready(function () {

            table = $('#supplier_wise_materials_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                "ajax": {
                    url: "{{url('/supplier_wise_materials/datatable')}}", // json datasource
                    type: "get",
                    error: function () {  // error handling code
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    }
                },
                pageLength: 25,
                responsive: true
            });

            $("#filter_form").validate({});

        });

       /* function process_form(form_id) {
            if ($(form_id).valid()) {
                var date = $("#start_date").val();
                var vend_date = $("#end_date").val();
                var vsupplier_id = $("#supplier_id").val();
                var table = $('#supplier_wise_materials_table').DataTable();
                table.ajax.url('supplier_wise_materials/get_new_rows?supplier_id='+vsupplier_id+'&start_date='+vstart_date+'&end_date='+vend_date).load();
            }
        }*/

        function process_form(e) {
            let item = $("#item").val();
            let supplier = $("#supplier").val();
            let date_range = $("#date_range").val();
            let table = $('#supplier_wise_materials_table').DataTable();
            table.ajax.url('/supplier_wise_materials/datatable?item=' + item + '&supplier=' + supplier +  '&date_range=' + date_range + '&filter=' + true).load();
        }

    </script>
@stop