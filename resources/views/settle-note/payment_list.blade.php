@extends('layouts.back.master')
@section('title','Payments | List ')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <style>
        th {
            text-align: right !important;
        }

        td {
            text-align: right !important;
        }
    </style>
@stop

@section('content')
    <div class="">
        {!! Form::open(['route' => ['settleNote-payment.process','id'=>$payment->id], 'method' => 'post']) !!}
        <input type="hidden" name="payment_type" value="{{$payment_type}}">
        <input type="hidden" name="return_id" value="{{$payment->id}}">
        <div class="col-md-12 mb-2 float-right">
            <div class="row">
                <div class="col-md-4">
                    Agent : {{ $payment->agent->name_with_initials.' - '.$payment->agent->territory}}
                </div>
                <div class="col-md-4">
                    <!-- Value : <label id="value">{{number_format($payment->final_amount, 2)}}</label> -->
                    Value : <label id="value">{{$payment->final_amount}}</label>
                </div>
                <div class="col-md-4">
                    Remain : <label id="remainValue">{{$payment->final_amount}}</label>
                </div>
            </div>
        </div>

        <table id="invoice_table" class="display text-center mt-3">
            <thead>
            <tr>
                <th>#</th>
                <th>Date</th>
                <th>Invoice Number</th>
                <th>Amount</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($invoices as $invoice)
                <tr>
                    <td><input value="{{!empty($invoice->net_amount)?$invoice->net_amount- $invoice->paid_amount:$invoice->total- $invoice->paid_amount}}" onclick="updateRemainValue(this)"
                               data-amount="{{!empty($invoice->net_amount)?$invoice->net_amount- $invoice->paid_amount:$invoice->total- $invoice->paid_amount}}"
                               type="checkbox" data-id="{{$invoice->id}}" name="invoices[{{$invoice->id}}]" class="form-control chk"></td>
                    <td>{{ \Carbon\Carbon::parse($invoice->created_at)->format('Y-m-d H:i:s')}}</td>
                    <td>{{$invoice->invoice_number}}</td>
                    <td>{{!empty($invoice->net_amount)?number_format($invoice->net_amount - $invoice->paid_amount,2):number_format($invoice->total - $invoice->paid_amount,2)}}</td>
                    <td>
                        <a href="/order/{{$invoice->id}}/preview" class="btn btn-sm btn-primary"><i
                                    class="fa fa-eye"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th>#</th>
                <th>Date</th>
                <th>Invoice Number</th>
                <th>Amount</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
        <div class="col-md-12">
            <button type="submit" class="btn btn-block btn-success d-none" id="btnMakePayment">Make Payment</button>
        </div>
        <input type="hidden" name="invoice_ids" id="invoice_ids">
        {!! Form::close() !!}
    </div>
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
    <script type="text/javascript"
            src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js"></script>
    <script>
        var selectedColors = [];
        $('.chk').change(function () {
            let id = this.dataset.id;
            const clickedColor = $(this).attr('data-id');
            if (this.checked) {
                selectedColors =  [...selectedColors, clickedColor];
            } else {
                selectedColors = selectedColors.filter(color => color != clickedColor);
            }
            $('#invoice_ids').val(selectedColors.join(','));
        });

        $(".addItem").click(function (event) {
            event.preventDefault();
            $("#qtyChangeForm").attr('action', '/end-burden/edit/qty/' + this.value);
            $('#ChangeQty').modal('show')
        });

        function process_form(e) {
            let category = $("#category").val();
            let day = $("#day").val();
            let time = $("#time").val();
            let date = $("#date").val();
            let agent = $("#agent").val();
            let table = $('#invoice_table').DataTable();
            table.ajax.url('/sr/table/data?category=' + category + '&day=' + day + '&date=' + date + '&agent=' + agent + '&time=' + time + '&filter=' + true).load();
        }


        function process_form_reset() {
            $("#category").val('');
            $("#day").val('');
            $("#time").val('');
            $("#date").val('');
            $("#agent").val('');
            let table = $('#invoice_table').DataTable();
            table.ajax.url('/sr/table/data').load();
        }

        $(document).ready(function () {
            table = $('#invoice_table').DataTable({
                searching: true,
                pageLength: 100,
                responsive: true
            });
        });

        function updateRemainValue(chk) {
            let amount = parseFloat(chk.dataset['amount']);
            let remain = parseFloat($('#remainValue').text());
            let value = parseFloat($('#value').text());
            let new_remain_value = 0;
            if (chk.checked) {
              // if ((remain <= 0 && value >0) || (remain >= 0 && value <0)){
              // alert(remain <= (2*value) && value <0);
              // if ((remain < 0 && value >=0) || (remain <= (2*value) && value <0)){
              //     alert("Payment Enough!!!");
              //     chk.checked = false;
              //     return false;
              // }
              if(value >=0 && remain <0)
              {
                alert("Payment Enough!!!");
                    chk.checked = false;
                    return false;
              }
              if(value <0 && remain<=(2*value)){
                alert("Payment Enough!!!");
                    chk.checked = false;
                    return false;
              }
                new_remain_value = remain - amount;
            } else {
                new_remain_value = remain + amount;
            }
            $('#remainValue').empty();
            $('#remainValue').append(new_remain_value);

            if (new_remain_value <= 0) {
                $('#btnMakePayment').removeClass('d-none')
            } else {
                $('#btnMakePayment').addClass('d-none')
            }
        }

    </script>

@stop
