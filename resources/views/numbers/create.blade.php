@extends('layouts.back.master')@section('title','Numbers')
@section('css')

    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.wizard.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/modal.css')}}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/demos.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/css/site.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/packages/recipe/recipe.css')}}">


    <style>
        .hide {
            display: none;
        }

        .floating-button {
            width: 35px;
            height: 35px;
            border-radius: 50%;
            background: #60c84c;
            position: fixed;

            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .plus {
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 35px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }


    </style>
@stop
@section('content')
@section('current','Numbers')
@section('current_url',route('numbers.index'))
<div class="app-wrapper">
    <div class="app-panel" id="app-panel">

        <div class="app-panel-inner">
            <div class="scroll-container">
                <div class="p-3">
                    <button class="btn btn-warning py-3 btn-block btn-lg" data-toggle="modal"
                            data-target="#cat">NEW NUMBER
                    </button>
                </div>
                <hr class="m-0">
                <div class="p-3">
                    <button class="btn btn-outline-danger py-3 btn-block btn-lg" data-toggle="modal"
                            data-target="#assign">Assign NUMBER
                    </button>
                </div>
                <hr class="m-0">
                <div class="projects-list">
                    @foreach($numbers as $number)
                        <a>
                            <div class="panel-item media p-2 mb-2">
                                <div class="media-body">
                                    <h6 class="media-heading my-1">
                                        {{$number->number}}
{{--                                        <a class="float-right" href="/edit/number/{{$number->id}}"> <i class="fa fa-pencil mr-2 text-indigo"></i></a>--}}
                                    </h6>
                                    <i class="qty-icon fa fa-archive"></i>
                                    <small>{{$number->description}}</small>
                                    <a class="float-right" href="/delete/number/{{$number->id}}"> <i class="fa fa-close  mr-2 text-danger"></i></a>
                                    <br>
                                    </div>
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
        <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show"><i
                    class="fa fa-chevron-right"></i>
            <i class="fa fa-chevron-left"></i>
        </a>
    </div>

    <div class="app-main">
        <div class="app-main-header">
            <h5 class="app-main-heading text-center">Numbers</h5>
        </div>
        <div class="app-main-content px-3 py-4" style="background-color: white" id="container">
            <div class="content ">
                <div class="contacts-list">
                    @foreach($list as $item)
                        @php
                            $btn_cls= $item->status?'btn-success':'btn-danger';
$btn_text =$item->status?'Active':'Disabled'
                        @endphp
                        <div class="panel-item media mb-2">
                            <div class="media-body">
                                <div class="row">
                                    <div class="col-md-3">{{optional($item->number)->number}}</div>
                                    <div class="col-md-3">{{!empty(App\User::find($item->user_id))?\App\User::find($item->user_id)->email:'User Deleted'}}</div>
                                    <div class="col-md-2">
                                        <button class="btn  btn-sm {{$btn_cls}}">{{$btn_text}}</button>
                                    </div>
                                    <div class="col-md-2">
                                        <input type="checkbox" class="one text-center" onclick="changeStatus(this,{{$item->id}})" data-size="medium" value="{{($item->status==1)?1:0 }}" {{($item->status==1) ?'checked':null }} >
                                    </div>
                                    <div class="col-md-2 text-right"><a href="/delete/user/number/{{$item->id}}"> <i class="fa fa-close text-danger"></i></a></div>
                                </div>
                                <br>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="cat" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                    <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-lg-12">
                    <div class="card">
                        {{ Form::open(array('url' => route('numbers.store'),'enctype'=>'multipart/form-data'))}}
                        <div class="wizard p-4" id="bootstrap-wizard-1">
                            <ul class="nav nav-tabs vertices keep-prefix-suffix" role="tablist">
                                <li class="nav-item">
                                    <a class=" nav-link active" data-toggle="tab" href="#ex5-step-1"
                                       role="tab">
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="ex5-step-1" role="tabpanel">


                                    <div class="task-name-wrap">
                                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                        <input class="task-name-field" type="number" name="number"
                                               placeholder="Number">
                                    </div>

                                    <hr>
                                    <div class="task-name-wrap">
                                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                        <input class="task-name-field" type="text" name="description"
                                               placeholder="Description">
                                    </div>

                                </div>
                            </div>
                            <div class="pager d-flex justify-content-center">
                                <input type="submit" id="finish-btn" class="finish btn btn-success btn-block"
                                       value="Finish">
                            </div>
                        </div>
                        {!!Form::close()!!}
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="assign" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                    <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-lg-12">
                    <div class="card">
                        {{ Form::open(array('url' => route('numbers.assign'),'enctype'=>'multipart/form-data'))}}
                        <div class="wizard p-4" id="bootstrap-wizard-1">
                            <ul class="nav nav-tabs vertices keep-prefix-suffix" role="tablist">
                                <li class="nav-item">
                                    <a class=" nav-link active" data-toggle="tab" href="#ex5-step-1"
                                       role="tab">
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="ex5-step-1" role="tabpanel">


                                    <div class="task-name-wrap">
                                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                        {!! Form::select('user', $users , null , ['class' => 'task-name-field','placeholder'=>'Select User']) !!}
                                    </div>

                                    <hr>
                                    <div class="task-name-wrap">
                                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                        {!! Form::select('number', $number_list , null , ['class' => 'task-name-field','placeholder'=>'Select Number']) !!}
                                    </div>

                                </div>
                            </div>
                            <div class="pager d-flex justify-content-center">
                                <input type="submit" id="finish-btn" class="finish btn btn-success btn-block"
                                       value="Finish">
                            </div>
                        </div>
                        {!!Form::close()!!}
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


@stop
@section('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    {{--    <script src="{{asset('assets/examples/js/apps/projects.js')}}"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('assets/packages/semi_finish/semi_finish.js')}}"></script>
    <script src="{{asset('assets/packages/recipe/end-recipe.js')}}"></script>
    <script>
        function changeStatus(element,id){
            let type =null;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

            if($(element).prop("checked") == true){
                type =1
            }
            else if($(element).prop("checked") == false){
                type =0;
            }

            $.ajax({
                url: "/change/user/number/"+id,
                data: {
                    "type": type,
                },
                cache: false,
                type: "POST",
                success: function(response) {

                },
                error: function(xhr) {

                }
            });
        }
    </script>
@stop