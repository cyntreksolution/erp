<html>
<head>
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/invoice.css')}}" rel="stylesheet">
</head>
<body>
<div class="row">
    <h4 class="inv">
        PRODUCTION ORDER
    </h4>
</div>
<div class="row">
    <h5 class="inv">
        End products
    </h5>
</div>
<div class="row">
    <table>
        <tr>
            <th>Order ID</th>
            <th>Product</th>
            <th>Total</th>
        </tr>
        @foreach($end_product_data as $key => $end_product)
            <tr>
                <td>{{$end_product[0]}}</td>
                <td>{{$end_product[1]}}</td>
                <td>{{$end_product[2]}}</td>
            </tr>
        @endforeach
    </table>
</div>
<div class="row">
    <h5 class="inv">
        Semi products
    </h5>
</div>
<div class="row">
    <table>
        <tr>
            <th>Product</th>
            <th>Total</th>
        </tr>
        @foreach($semi_product_data as $key => $end_product)
            <tr>
                <td>{{$end_product[0]}}</td>
                <td>{{$end_product[1]}}</td>
            </tr>
        @endforeach
    </table>
</div>
<footer>
    <div class="row" style="margin-top: 20px;">
        <h4 align="center">
            <h6 class="text-muted">powered by cyntrek<br>
                <h7 class="num">+9471 122 5489 / +9471 532 2262</h7>
            </h6>
        </h4>

    </div>
</footer>
</body>

</html>