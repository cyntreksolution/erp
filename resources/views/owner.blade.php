@extends('layouts.back.master')@section('title','Buntalk')
@section('css')

    {{--<link rel="stylesheet" href="{{asset('assets/examples/css/demos/demos.css')}}"/>--}}
    {{--<link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>--}}
    {{--<link rel="stylesheet" href="{{asset('assets/examples/css/theme-customizer.css')}}"/>--}}
    {{--<script src="{{asset('assets/examples/js/theme-customizer.js')}}"></script>--}}
    {{--<link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>--}}
    {{--<link rel="stylesheet" href="{{asset('assets/vendor/bower_components/chartist/dist/chartist.min.css')}}"/>--}}
    {{--<link rel="stylesheet" href="{{asset('assets/vendor/owl-carousel/owl.carousel.css')}}"/>--}}
   {{----}}

@stop


@section('content')
    {{--<div class="row">--}}
        {{--<div class="col-lg-8">--}}

            {{--<div class="card">--}}
                {{--<header class="card-header"><h6 class="card-heading">Recent Orders</h6><ul class="card-toolbar">--}}
                        {{--<li><a href="javascript:void(0)"><i class="zmdi zmdi-refresh"></i></a>--}}
                        {{--</li><li class="dropdown">--}}
                            {{--<a href="javascript:void(0)" data-toggle="dropdown" aria-expanded="false">--}}
                                {{--<i class="zmdi zmdi-more-vert"></i></a>--}}
                            {{--<div class="dropdown-menu float-right">--}}
                                {{--<a class="dropdown-item" href="#">Option 1</a>--}}
                                {{--<a class="dropdown-item" href="#">Option 2</a>--}}
                                {{--<a class="dropdown-item" href="#">Option 3</a></div>--}}
                        {{--</li>--}}
                    {{--</ul><!-- /.card-toolbar --></header>--}}
                {{--<div><div class="d-flex justify-content-end p-4 mb-4">--}}
                        {{--<label><span class="mr-2">Live</span>--}}
                            {{--<input type="checkbox" data-plugin="switchery" data-color="#60c84c" data-size="small" checked="checked">--}}
                        {{--</label></div>--}}
                    {{--<div id="dash1-flotchart-1" style="width: 100%; height: 320px">--}}

                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div><!-- /.col -->--}}
        {{--<div class="col-lg-4">--}}
            {{--<div class="card">--}}
                {{--<header class="card-header"><h6 class="card-heading">New Customers</h6>--}}
                    {{--<ul class="card-toolbar"><li>--}}
                            {{--<a href="javascript:void(0)">--}}
                                {{--<i class="zmdi zmdi-refresh">--}}

                                {{--</i></a></li><li class="dropdown">--}}
                            {{--<a href="javascript:void(0)" data-toggle="dropdown" aria-expanded="false">--}}
                                {{--<i class="zmdi zmdi-more-vert"></i></a>--}}
                            {{--<div class="dropdown-menu float-right">--}}
                                {{--<a class="dropdown-item" href="#">Option 1</a>--}}
                                {{--<a class="dropdown-item" href="#">Option 2</a>--}}
                                {{--<a class="dropdown-item" href="#">Option 3</a>--}}
                            {{--</div></li></ul><!-- /.card-toolbar -->--}}
                {{--</header><div class="card-body d-flex">--}}
                    {{--<h2 class="text-primary mr-auto">--}}
                        {{--<span data-plugin="counterUp">40</span></h2>--}}
                    {{--<div class="text-primary">--}}
                        {{--<span class="dash1-peity-chart" data-peity='{"stroke": "#501310" }'>0,1,2,0,2,0,3</span>--}}
                    {{--</div></div><!-- /.card-body -->--}}
            {{--</div><!-- /.card -->--}}
            {{--<div class="card">--}}
                {{--<header class="card-header">--}}
                    {{--<h6 class="card-heading">Total Profit</h6>--}}
                    {{--<ul class="card-toolbar"><li>--}}
                            {{--<a href="javascript:void(0)">--}}
                                {{--<i class="zmdi zmdi-refresh"></i>--}}
                            {{--</a></li>--}}
                        {{--<li class="dropdown"><a href="javascript:void(0)" data-toggle="dropdown" aria-expanded="false">--}}
                                {{--<i class="zmdi zmdi-more-vert"></i></a>--}}
                            {{--<div class="dropdown-menu float-right">--}}
                                {{--<a class="dropdown-item" href="#">Option 1</a>--}}
                                {{--<a class="dropdown-item" href="#">Option 2</a>--}}
                                {{--<a class="dropdown-item" href="#">Option 3</a>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                    {{--</ul><!-- /.card-toolbar -->--}}
                {{--</header>--}}
                {{--<div class="card-body d-flex">--}}
                    {{--<h2 class="text-success mr-auto">Rs. <span data-plugin="counterUp">2,050,000</span></h2>--}}
                    {{--<div class="text-success">--}}
                        {{--<span class="dash1-peity-chart" data-peity='{"stroke": "#60c84c" }'>0,1,3,0,2,0,2</span>--}}
                    {{--</div>--}}
                {{--</div><!-- /.card-body --></div><!-- /.card -->--}}
            {{--<div class="card">--}}
                {{--<header class="card-header"><h6 class="card-heading">New Orders</h6>--}}
                    {{--<ul class="card-toolbar"><li><a href="javascript:void(0)"><i class="zmdi zmdi-refresh"></i></a></li><li class="dropdown"><a href="javascript:void(0)" data-toggle="dropdown" aria-expanded="false"><i class="zmdi zmdi-more-vert"></i></a><div class="dropdown-menu float-right"><a class="dropdown-item" href="#">Option 1</a> <a class="dropdown-item" href="#">Option 2</a> <a class="dropdown-item" href="#">Option 3</a></div></li></ul><!-- /.card-toolbar --></header><div class="card-body d-flex"><h2 class="text-danger mr-auto"><span data-plugin="counterUp">262</span></h2><div class="text-danger"><span class="dash1-peity-chart" data-peity='{"stroke": "#ff7473" }'>0,1,2,0,2,0,1</span></div></div><!-- /.card-body --></div><!-- /.card -->--}}
        {{--</div><!-- /.col -->--}}
    {{--</div>--}}

    {{--<div class="row">--}}
        {{--<div class="col-lg-6 col-sm-6">--}}
            {{--<div class="card">--}}
                {{--<header class="card-header"><h6 class="card-heading">Income</h6>--}}
                    {{--<ul class="card-toolbar"><li><a href="javascript:void(0)">--}}
                                {{--<i class="zmdi zmdi-refresh"></i></a></li>--}}
                        {{--<li class="dropdown"><a href="javascript:void(0)" data-toggle="dropdown" aria-expanded="false">--}}
                                {{--<i class="zmdi zmdi-more-vert"></i></a><div class="dropdown-menu float-right">--}}
                                {{--<a class="dropdown-item" href="#">Option 1</a>--}}
                                {{--<a class="dropdown-item" href="#">Option 2</a>--}}
                                {{--<a class="dropdown-item" href="#">Option 3</a>--}}
                            {{--</div></li></ul><!-- /.card-toolbar -->--}}
                {{--</header><div class="card-body d-flex px-3">--}}
                    {{--<div class="mr-auto text-primary">--}}
                        {{--<h5>Rs. <span data-plugin="counterUp">3,886,200</span>--}}
                        {{--</h5><span>Total Income</span></div>--}}
                    {{--<div><a href="#" class="btn btn-sm btn-primary">Monthly</a>--}}
                        {{--<div class="mt-3 text-center"><span class="mr-2">98% </span>--}}
                            {{--<i class="fa fa-long-arrow-up text-success"></i>--}}
                        {{--</div></div></div><!-- /.card-body -->--}}
            {{--</div><!-- /.card -->--}}
        {{--</div><!-- /.col -->--}}
        {{--<div class="col-lg-6 col-sm-6">--}}
            {{--<div class="card">--}}
                {{--<header class="card-header"><h6 class="card-heading">Orders</h6><ul class="card-toolbar">--}}
                        {{--<li><a href="javascript:void(0)">--}}
                                {{--<i class="zmdi zmdi-refresh"></i></a></li>--}}
                        {{--<li class="dropdown"><a href="javascript:void(0)" data-toggle="dropdown" aria-expanded="false">--}}
                                {{--<i class="zmdi zmdi-more-vert"></i></a><div class="dropdown-menu float-right">--}}
                                {{--<a class="dropdown-item" href="#">Option 1</a>--}}
                                {{--<a class="dropdown-item" href="#">Option 2</a>--}}
                                {{--<a class="dropdown-item" href="#">Option 3</a>--}}
                            {{--</div></li></ul><!-- /.card-toolbar --></header>--}}
                {{--<div class="card-body d-flex px-3"><div class="mr-auto text-primary">--}}
                        {{--<h5 data-plugin="counterUp">46128</h5><span>Total orders</span>--}}
                    {{--</div>--}}
                    {{--<div>--}}
                        {{--<a href="#" class="btn btn-sm btn-success">Annual</a>--}}
                        {{--<div class="mt-3 text-center"><span class="mr-2">20% </span>--}}
                            {{--<i class="fa fa-long-arrow-up text-success"></i>--}}
                        {{--</div></div></div><!-- /.card-body -->--}}
            {{--</div><!-- /.card -->--}}
        {{--</div><!-- /.col -->--}}
    {{--</div><!-- /.row -->--}}

    {{--<div class="row">--}}
        {{--<div class="col-lg-7">--}}
            {{--<div class="card" id="dash1-widget-performance">--}}
                {{--<header class="card-header">--}}
                    {{--<h6 class="card-heading">Performance</h6>--}}
                    {{--<ul class="card-toolbar">--}}
                        {{--<li><a href="javascript:void(0)">--}}
                                {{--<i class="zmdi zmdi-refresh"></i>--}}
                            {{--</a></li>--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="javascript:void(0)" data-toggle="dropdown" aria-expanded="false">--}}
                                {{--<i class="zmdi zmdi-more-vert"></i></a>--}}
                            {{--<div class="dropdown-menu float-right">--}}
                                {{--<a class="dropdown-item" href="#">Option 1</a>--}}
                                {{--<a class="dropdown-item" href="#">Option 2</a>--}}
                                {{--<a class="dropdown-item" href="#">Option 3</a>--}}
                            {{--</div></li></ul><!-- /.card-toolbar --></header>--}}
                {{--<div class="card-body">--}}
                    {{--<div class="d-flex justify-content-between flex-sm-row flex-column mb-4">--}}
                        {{--<div class="media mb-4"><div class="mr-3">--}}
                                {{--<span class="circle circle-sm bg-primary">--}}
                                    {{--<svg class="svg-eye-2-icon"><use xlink:href="http://spantags.com/luxury/assets/global/svg-sprite/sprite.svg#eye-2"/></svg></span>--}}
                            {{--</div>--}}
                            {{--<div class="media-body">--}}
                                {{--<h6 class="my-1 fz-sm">New Customers</h6>--}}
                                {{--<small>40 </small>--}}
                            {{--</div>--}}
                        {{--</div><!-- /.media -->--}}
                        {{--<div class="media mb-4">--}}
                            {{--<div class="mr-3">--}}
                                {{--<span class="circle circle-sm bg-success">--}}
                                    {{--<svg class="svg-eye-2-icon"><use xlink:href="http://spantags.com/luxury/assets/global/svg-sprite/sprite.svg#eye-2"/></svg></span>--}}
                            {{--</div><div class="media-body">--}}
                                {{--<h6 class="my-1 text-success fz-sm">New Orders</h6>--}}
                                {{--<small>262</small></div></div><!-- /.media -->--}}

                    {{--</div><!-- /.performance-counters -->--}}
                    {{--<div id="dash1-flotchart-2" style="width: 100%; height: 296px">--}}

                    {{--</div></div><!-- /.card-body -->--}}
            {{--</div><!-- /.card -->--}}

        {{--</div><!-- /.col -->--}}
        {{--<div class="col-lg-5">--}}
            {{--<div class="card" style="height: 492px">--}}
                {{--<header class="card-header"><h6 class="card-heading">Analyzing</h6>--}}
                    {{--<ul class="card-toolbar"><li>--}}
                            {{--<a href="javascript:void(0)">--}}
                                {{--<i class="zmdi zmdi-refresh"></i>--}}
                            {{--</a></li><li class="dropdown">--}}
                            {{--<a href="javascript:void(0)" data-toggle="dropdown" aria-expanded="false">--}}
                                {{--<i class="zmdi zmdi-more-vert"></i>--}}
                            {{--</a><div class="dropdown-menu float-right">--}}
                                {{--<a class="dropdown-item" href="#">Option 1</a>--}}
                                {{--<a class="dropdown-item" href="#">Option 2</a>--}}
                                {{--<a class="dropdown-item" href="#">Option 3</a>--}}
                            {{--</div></li></ul><!-- /.card-toolbar -->--}}
                {{--</header><div class="card-body bg-faded">--}}
                    {{--<div class="text-center">--}}
                        {{--<div class="btn-group btn-group-sm">--}}
                            {{--<a href="#" class="btn btn-success">Customer</a>--}}
                            {{--<a href="#" class="btn btn-success">Order</a>--}}
                        {{--</div></div>--}}
                    {{--<div class="mt-4">--}}
                        {{--<div id="dash1-flotchart-3" style="width: 100%; height: 234px"></div>--}}
                    {{--</div>--}}
                {{--</div><!-- /.card-body --></div>   <!-- /.card -->--}}

        {{--</div>--}}
        {{--<!-- /.col -->--}}
    {{--</div>--}}

@stop

@section('js')

    {{--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--}}
    {{--<script src="{{asset('assets/examples/js/apps/projects.js')}}"></script>--}}
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>--}}

    {{--<script src="{{asset('assets/vendor/bower_components/chartist/dist/chartist.min.js')}}"></script>--}}
    {{--<script src="{{asset('assets/vendor/jvectormap/jquery-jvectormap.min.js')}}"></script>--}}
    {{--<script src="{{asset('assets/vendor/jvectormap/maps/jquery-jvectormap-world-mill.js')}}"></script>--}}
    {{--<script src="{{asset('assets/vendor/flot/jquery.flot.min.js')}}"></script>--}}
    {{--<script src="{{asset('assets/vendor/flot/jquery.flot.resize.min.js')}}"></script>--}}
    {{--<script src="{{asset('assets/vendor/flot/jquery.flot.tooltip.min.js')}}"></script>--}}
    {{--<script src="{{asset('assets/vendor/flot/jquery.flot.categories.min.js')}}"></script>--}}
    {{--<script src="{{asset('assets/vendor/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js')}}"></script>--}}
    {{--<script src="{{asset('assets/vendor/bower_components/peity/jquery.peity.min.js')}}"></script>--}}
    {{--<script src="{{asset('assets/vendor/owl-carousel/owl.carousel.min.js')}}"></script>--}}
    {{--<script src="{{asset('assets/examples/js/dashboards/dashboard.v1.js')}}"></script>--}}

    {{--<script>--}}
        {{--$(document).ready(function () {--}}
            {{--var o = [[0, 6], [1, 4.5], [2, 6], [3, 5], [4, 6.5], [5, 5], [6, 6], [7, 7], [8, 6], [9, 5], [10, 6], [11, 7], [12, 5.5]],--}}
                {{--t = [[0, 5], [1, 4], [2, 5], [3, 4.5], [4, 5.5], [5, 4], [6, 5], [7, 6], [8, 5], [9, 4], [10, 5], [11, 6], [12, 6]];--}}
            {{--$.plot($("#dash1-flotchart-1"), [{data: o, label: "Orders"}, {--}}
                {{--data: t,--}}
                {{--label: "Sales"--}}
            {{--}], {--}}
                {{--series: {--}}
                    {{--lines: {show: !0, zero: !0, fill: !0, fillColor: {colors: [{opacity: 0}, {opacity: .2}]}},--}}
                    {{--points: {show: !0, radius: 4},--}}
                    {{--highlightColor: $.colors.warning--}}
                {{--},--}}
                {{--colors: ["#71ce60", "#415e8c"],--}}
                {{--grid: {show: !0, hoverable: !0, borderWidth: 0, margin: {left: 0, right: 0, bottom: 0, top: 0}},--}}
                {{--xaxis: {show: !1},--}}
                {{--yaxis: {show: !1},--}}
                {{--legend: {--}}
                    {{--position: "nw", margin: [10, -90], labelFormatter: function (o, t) {--}}
                        {{--return "<div class='ml-1' style='color: " + t.color + ";'>" + o + "</div>"--}}
                    {{--}--}}
                {{--},--}}
                {{--tooltip: {--}}
                    {{--show: !0,--}}
                    {{--content: "%s",--}}
                    {{--cssClass: "flotTip-with-arrow",--}}
                    {{--defaultTheme: !1,--}}
                    {{--shifts: {x: -45, y: -60}--}}
                {{--}--}}
            {{--});--}}
        {{--})--}}
    {{--</script>--}}

@stop


