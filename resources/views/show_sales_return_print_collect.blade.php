<html>
<head>
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/invoice.css')}}" rel="stylesheet">
    <style media="all">
        @page {
            margin: 30px 5px 30px 5px;
            size: 75mm 999mm;
        }
    </style>
</head>

<body style="margin-bottom: 100px">

<div class="row">

    <div class="col-xs-5">
        <img src="{{asset('assets/img/buntak_grace.jpeg')}}" style="width: 100px">
    </div>

    <div class="col-xs-7 text-right">
        <h6>
            <b>Ranjanas Holdings [Pvt] Ltd</b><br>
            Katukurunda<br>
            Habaraduwa<br>
            Hotline : +9477 330 4678<br>
        </h6>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-xs-3 text-nowrap">Sold Agent</div>
            <div class="col-xs-9">:{{$invoice->agent->type.' '.$invoice->agent->name_with_initials}}</div>
        </div>
        <div class="row">
            <div class="col-xs-3 text-nowrap">Territory</div>
            <div class="col-xs-9">:{{$invoice->agent->territory}}</div>
        </div>
        <div class="row">
            <div class="col-xs-3 text-nowrap">Return Note Number</div>
            <div class="col-xs-9">:{{$invoice->debit_note_number}}</div>
        </div>
        <div class="row">
            <div class="col-xs-3 text-nowrap">Day</div>
            <div class="col-xs-9">:{{$invoice->created_at}}</div>
        </div>
        <div class="row">
            <div class="col-xs-3 text-nowrap">time</div>
            <div class="col-xs-9">:{{$invoice->time}}</div>
        </div>


    </div>
</div>
<br>


<div class="row">
    <div class="col-xs-12">
        <table class="table">
            <thead>
            <tr>
                <th>Item with price</th>
                <th class="text-center">QTY</th>
                <th class="text-right">Accepted Qty</th>
            </tr>
            </thead>
            <tbody>
            @php $counter= 0; @endphp
            @foreach($items as $product)
                @if ($product->qty>0)
                    <tr>
                        <td>{{$product->endProduct->name." (".number_format((float)$product->unit_value, 2, '.', '').")"}}</td>
                        <td class="text-center">{{$product->qty}}</td>
                        <td align="right">
                            <div style="width:150px;height:30px;border:1px solid #000;"></div>
                        </td>
                    </tr>
                @endif
                @php  $counter = $counter+1 ;@endphp
            @endforeach
            </tbody>
        </table>
    </div>

</div>
<div class="row">
    <div class="col-xs-12 pull-right text-right">
        <h4> Number Of Items : {{$counter}} </h4>
    </div>
</div>
<br>

<footer>
    <div class="row" style="margin-top: 20px;">
        <h4 align="center">
            <h6 class="text-muted">Powered by Cyntrek Solutions<br>
                <h7 class="num">+9471 122 5489 / +9471 532 2262</h7>
            </h6>
        </h4>
    </div>

    <p style="margin-top: 150px">--------------------------------------------------------------</p>
</footer>

</body>

</html>
