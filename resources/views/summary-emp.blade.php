@extends('layouts.back.master')@section('title','Salary')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }
    </style>
@stop

@section('content')

    <div class="row mb-2">

        <div class="col-md-2">
            {!! Form::select('employee',$employees , null , ['class' => 'form-control','placeholder'=>'Select Employee','id'=>'employee']) !!}
        </div>

        <div class="col-md-2">
            <input type="text" name="date" id="date_range" class="form-control" placeholder="Select Date">
        </div>

        <div class="col-md-2">
            <button class="btn btn-info " onclick="process_form(this)">Filter</button>
        </div>




    </div>



    <div class="table-responsive">
        <table id="esr_table" class="display text-center ">
            <thead>
            <tr>
                <th>Date</th>
                <th>Name</th>
                <th>Work Hours</th>
                <th>Commission</th>
                <th>Action</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Date</th>
                <th>Name</th>
                <th>Work Hours</th>
                <th>Commission</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
    </div>


@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script>
        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019",
            "maxDate":moment()
        });


        $('#date_range').val('')
        $(document).ready(function () {
            table = $('#esr_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "ajax": {
                    url: "{{url('/salary/list/table/data')}}",
                    type: "get",
                    error: function () {
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    }
                },
                pageLength: 100,
                responsive: true
            });


        });

        function deleteCommis(id){
            var commis_id=$('.delete-commis').attr("data-id");
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "post",
                url: "{{ url('/salary/destroy/commission') }}",
                data: {commis_id: commis_id},
                success: function (response) {
                    table.ajax.reload();
                }
            });

        }

        function process_form(e) {
            let employee = $("#employee").val();
            let table = $('#esr_table').DataTable();
            let date_range = $("#date_range").val();


            table.ajax.url('/salary/list/table/data?employee=' + employee + '&date_range=' + date_range +'&filter=' + true).load();
        }


        function process_form_reset() {
            $("#category").val('');
            $("#day").val('');
            $("#time").val('');
            $("#status").val('');
            $("#agent").val('');
            let table = $('#esr_table').DataTable();
            table.ajax.url('order/list/table/data').load();
        }

    </script>

@stop
