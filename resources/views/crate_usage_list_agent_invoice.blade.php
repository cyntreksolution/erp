<html>
<head>
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/invoice.css')}}" rel="stylesheet">
    <style media="all">

        @page {
            margin: 30px 5px 30px 5px;
            size: 75mm 999mm;
        }
    </style>
</head>

<!--body style="margin-bottom: 100px"-->
<table>
<tr class="row mt-2">

    <td class="col-xs-6">
        <img src="{{asset('assets/img/buntak_grace.jpeg')}}" style="width: 100px">
    </td>

    <td class="col-xs-6 text-right">
        <h6>
            <b>Ranjanas Holdings [Pvt] Ltd</b><br>
            Katukurunda<br>
            Habaraduwa<br>
            Hotline : +9477 330 4678<br>
        </h6>
    </td>
</tr>
</table>
<table class="-text-height"><tr><td class="text-center">--------------------------------------------------------------</td></tr></table>
<table>
    <tr class="row mt-2">
        <td class="col-sm-4"><b>Agent Name :</b></td>
       <td class="col-sm-8"><b>{{$agents->name_with_initials}}</b></td>
    </tr>

    <tr class="row mt-2">
        <td class="col-sm-4"><b>Territory :</b></td>
          <td class="col-sm-8"><b>{{$agents->territory}}</b></td>

      </tr>

    <tr class="row mt-2">
        <td class="col-sm-4"><b>Printed At :</b></td>
        <td class="col-sm-8"><b>{{date('Y-m-d H:i:s')}}</b></td>

    </tr>

</table>

<table>
    <tr class="row mt-6">
        <th class="col-sm-7"><b><u>Desc</u></b></th>
        <th class="col-sm-1 text-center"><b><small><u>BL</u></small></b></th>
        <th class="col-sm-1 text-center"><b><small><u>BS</u></small></b></th>
        <th class="col-sm-1 text-center"><b><small><u>CL</u></small></b></th>
        <th class="col-sm-1 text-center"><b><small><u>CS</u></small></b></th>
        <th class="col-sm-1 text-center"><b><small><u>L</u></small></b></th>
    </tr>
<br>
    @foreach($data as $records)

            <tr class="row mt-6">
                <td class="col-sm-7"><h6><b>{{$records[0]}}</b></h6><h6><b>{{$records[1]}}</b></h6><h6><b>{{$records[2]}}</b></h6></td>
                <td class="col-sm-1 text-center">{{$records[3]}}</td>
                <td class="col-sm-1 text-center">{{$records[4]}}</td>
                <td class="col-sm-1 text-center">{{$records[5]}}</td>
                <td class="col-sm-1 text-center">{{$records[6]}}</td>
                <td class="col-sm-1 text-center">{{$records[7]}}</td>

            </tr>


    @endforeach

    <br>

    <tr class="row mt-6">
        <th class="col-sm-7"><b>Desc</b></th>
        <th class="col-sm-1 text-center"><b><small>BL</small></b></th>
        <th class="col-sm-1 text-center"><b><small>BS</small></b></th>
        <th class="col-sm-1 text-center"><b><small>CL</small></b></th>
        <th class="col-sm-1 text-center"><b><small>CS</small></b></th>
        <th class="col-sm-1 text-center"><b><small>L</small></b></th>
    </tr>


    <hr style="height:1px;background-color: #00a5bb">
    <tr class="row mt-6">

        <th class="col-sm-7"><b>Return Crates</b></th>
        <th class="col-sm-1 text-center"><div style="width:40px;height:30px;border:1px solid #000000;"></div></th>
        <th class="col-sm-1 text-center"><div style="width:40px;height:30px;border:1px solid #000;"></div></th>
        <th class="col-sm-1 text-center"><div style="width:40px;height:30px;border:1px solid #000;"></div></th>
        <th class="col-sm-1 text-center"><div style="width:40px;height:30px;border:1px solid #000;"></div></th>
        <th class="col-sm-1 text-center"><div style="width:40px;height:30px;border:1px solid #000;"></div></th>

    </tr>
</table>



<hr style="height:1px;background-color: #00a5bb">
<footer>
    <table align="center" style="margin-top: 120px;">
    <tr>
        <td >

            <h6 class="text">------------------------------------------------<br>
                <h6 class="text-center">( Signature )</h6>
            </h6>

        </td>
    </tr>
    </table>

</footer>

</body>

</html>

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script>

      /*  function process_form(e) {
            let date_range = $("#date").val();
            let ref = $("#ref").val();
            let agent = $("#agents").val();
            alert('ok');
            window.location.href = "/crates-inquiry-agent/list/table/print?agent="+ agent + "&date=" + date_range + "&ref=" + ref + "&filter=" + true;
        }*/


    </script>

@stop