@extends('layouts.back.master')@section('title','End Inquiry List')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <style>
        th {
            text-align: right !important;
        }

        td {
            text-align: right !important;
        }
    </style>
@stop

@section('content')

{{--    @if((Sentinel::check()->roles[0]->slug=='owner') || (Sentinel::check()->roles[0]->slug == 'sales-manager'))--}}
{{--        <div class="row mb-2">--}}
{{--            <div class="form-group col-sm-2">--}}
{{--                {!! Form::select('category',$categories , null , ['class' => 'form-control','placeholder'=>'Select Category','id'=>'category']) !!}--}}
{{--            </div>--}}
{{--            <div class="col-md-2">--}}
{{--                {!! Form::select('end_product',$end_products , null , ['class' => 'form-control','placeholder'=>'Select End Product','id'=>'end_product']) !!}--}}
{{--            </div>--}}
{{--            <div class="col-md-3">--}}
{{--                {!! Form::select('reference_type',['APP\EndProductBurdenGrade'=>'End Product Burden Grade','App\SalesReturnHeader'=>'Sales Return Header'], null , ['class' => 'form-control','placeholder'=>'Select Type','id'=>'reference_type']) !!}--}}
{{--            </div>--}}
{{--            <div class="col-md-3">--}}
{{--                <input type="text" name="date" id="date_range" class="form-control" placeholder="Select Date" required>--}}
                <input type="hidden" id="datefiil" value="{{$datefiil}}">
{{--            </div>--}}

{{--            <div class="col-md-2">--}}
{{--                <button class="btn btn-info " onclick="process_form(this)">Filter</button>--}}
{{--            </div>--}}

            <!-- loading view -->


{{--        </div>--}}
{{--    @endif--}}

<div class="col-md-12">
    <h3 class="text-center"> {{\EndProductManager\Models\EndProduct::find($id)->name}}</h3>
</div>
    <div class="table-responsive">
        <table id="endyes_table" class="display text-center ">
            <thead>

            <tr>
                <th>ID</th>
                <th>Date & Time</th>
                <th>Serial No</th>
                <th>Description</th>
                <th>Remark</th>
                <th>Qty</th>
                <th>Balance Qty</th>
            </tr>
            </thead>

        </table>
        <hr style="height:1px;background-color:#00a5bb">
        <table id="endi_table" class="display text-center ">
            <thead>
            <tr>
                <th>ID</th>
                <th>Date & Time</th>
                <th>Serial No</th>
                <th>Description</th>
                <th>Remark</th>
                <th>Qty</th>
                <th>Balance Qty</th>
            </tr>
            </thead>

        </table>

    </div>

@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script>
        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019",
            "maxDate":moment()
        });


        $('#date_range').val('')
        var id = {!! $id !!};
        var datefiil = $("#datefiil").val();
        var openning_balance;

        $(document).ready(function () {
            table = $('#endi_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "order":[0,'ASC'],
                "ajax": {
                    url: "{{url('/end-inquiry-new/list/table/data')}}/"+id+ '/' +moment(datefiil).format('YYYY-MM-DD'),
                    data: {'id':id,'date':datefiil},
                    type: "get",
                    error: function () {
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    },

                },
                pageLength: 100,
                responsive: true,

            });
        });

        $(document).ready(function () {
            table = $('#endyes_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "order":[0,'ASC'],
                "ajax": {
                    url: "{{url('/end-inquiry-new/list/table/data')}}/"+id+ '/' +moment(datefiil). add(-1,'days'). format('YYYY-MM-DD'),
                    data: {'id':id,'date':datefiil},
                    type: "get",
                    error: function () {
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    },
                    dataSrc: function (data) {
                        openning_balance = data.openning_balance;
                        return data.data;
                    },

                },
                pageLength: 100,
                responsive: true,
                drawCallback: function (settings) {

                    $("#openning_balance").empty();
                    $('#openning_balance').append(openning_balance);
                }
            });
        });

        $(document).ready(function () {
            table = $('#endtomorrow_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "order":[0,'ASC'],
                "ajax": {
                    url: "{{url('/end-inquiry-new/list/table/data')}}/"+id+ '/' +moment(datefiil). add(1,'days'). format('YYYY-MM-DD'),
                    data: {'id':id,'date':datefiil},
                    type: "get",
                    error: function () {
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    },

                },
                pageLength: 100,
                responsive: true,
            });
        });

        function process_form(e) {
            let end_product = $("#end_product").val();
            let table = $('#endi_table').DataTable();
            let reference_type = $("#reference_type").val();
            let date_range = $("#date_range").val();

            table.ajax.url('/end-inquiry/list/table/data?end_product=' + end_product + '&reference_type=' + reference_type  + '&date_range=' + date_range  +'&filter=' + true).load();
        }


        function process_form_reset() {
            $("#end_product").val('');

            let table = $('#endi_table').DataTable();
            table.ajax.url('end-inquiry/list/table/data').load();
        }

    </script>

@stop
