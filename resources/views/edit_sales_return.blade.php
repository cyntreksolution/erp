@extends('layouts.back.master')
@section('title','Sales Returns')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .hide {
            display: none;
        }

        .app-main-content {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .portlet-placeholder {
            border: 1px dotted black;
            margin: 0 1em 1em 0;
            height: 100%;
        }

        #projects-task-modal .modal-bg {
            padding: 50px;
        }

        #projects-task-modal .modal-content {
            box-shadow: none;
            border: none
        }

        #projects-task-modal .close {
            border: none;
            outline: none;
            box-shadow: none;
            position: absolute;
            right: 1rem;
            top: 1rem;
            width: 25px;
            height: 25px;
            line-height: 25px;
            text-align: center;
            font-size: 1.13rem;
            border-radius: 500px;
            border: 1px solid #868e96
        }

        #floating-buttongtfr {
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #db4437;
            position: fixed;
            top: 13rem;
            right: 90px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .plus {
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }

        @media only screen and (max-width: 600px) {
            .row-clz {
                width: 80% !important;
            }

            .btn-clz{
                width: 75% !important;
            }
        }
    </style>
@stop
@section('content')
    <div class="app-wrapper">

        <div class="app-main">
            {!! Form::open(['route' => ['sales_return.update',$invoice->id],'method'=>'patch']) !!}
            <input type="hidden" name="time" id="time" value="{{$invoice->time}}">
            <div class="row p-3 row-clz">
                <div class="col-md-6">
                    <h5 class="aa app-main-heading text-left"><span>UPDATE SALES RETURN-{{$invoice->debit_note_number}}</span></h5>
                </div>
                <div class="col-md-6 float-right">
                    <h5 class="aa app-main-heading text-left"><span>TIME-{{$invoice->time}}</span></h5>
                    </div>
            </div>

            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">
                    <div class="container">
                        <input type="hidden" name="supplier_id" id="supplier_id">
                        <div class="projects-list">
                            <div class="row mb-2 row-clz" >
                                <div class="col-md-2">
                                    {{--                                    Total Return--}}
                                </div>
                                <div class="col-md-2">

                                </div>
                                <div class="col-md-4">Product Name</div>
                                <div class="col-md-2">
                                    {{--                                    Sending Return--}}
                                </div>
                                <div class="col-md-2">

                                </div>
                            </div>
                            <hr>
                            @foreach($end_products as $endproduct)
                                @php
                                    $data = \App\SalesReturnData::where('sales_return_header_id','=',$invoice->id)
                                                                ->where('end_product_id','=',$endproduct->id)
                                                                ->first();
                                    $agent_category =\SalesRepManager\Models\AgentCategory::where('sales_ref_id','=',Sentinel::getUser()->id)->get();
                                    $end_product_category = $endproduct->category_id;
                                    $accept_status = null;
                                    foreach ($agent_category as $ag){
                                        if ($ag->category_id == $end_product_category) {
                                            $return_typ = $ag->return_mode; //1 = reuse ,2 =all

                                            if ($return_typ==1) {
                                                if ($endproduct->reusable_type != 1) {
                                                     $accept_status = 1;
                                                }else{
                                                     $accept_status = 0;
                                                }
                                            }else{
                                                $accept_status = 1;
                                            }

                                        }
                                    }


                                            $variant_size=0;
                                            $v = \App\Variant::where('end_product_id','=',$endproduct->id)->where('return_accepted','=',1)->first();
                                      if (!empty($v) && $v->count()>0) {
                                            $variant_size =$v->size;
                                            $unitPr = ($endproduct->distributor_price)*($variant_size);
                                      }else
                                          {
                                            $unitPr = ($endproduct->distributor_price);
                                          }



                                @endphp
                                <input type="hidden" name="end_product_ids[]" value="{{$endproduct->id}}">
                                <input type="hidden" name="unit_values[]" value="{{$unitPr}}">
                                <input type="hidden" name="type[]" value="{{$endproduct->reusable_type}} "/>
                                <div class="row mb-3 p-3 row-clz" style="background-color: antiquewhite;border: 1px solid black;border-radius: 10px;">
                                    <div class="col-md-2">
                                        <label>Total Return</label>
                                        <input type="text" required min="0" step="0.001" id="qty_{{$endproduct->id}}"
                                               onkeyup="updateValue({{$endproduct->id}},{{$endproduct->distributor_price}},{{$variant_size}})"
                                               class="form-control" name="qty[]" value="{{$data->all_qty}}">
                                    </div>
                                    <div class="col-md-2 tot "><label id="total_value_{{$endproduct->id}}">{{(($data->all_qty)*($unitPr))}}</label>
                                    </div>
                                    <div class="col-md-4"><h5 class="font-weight-bold text-nowrap">{{$endproduct->name}}</h5></div>

                                    <div class="col-md-2">
                                        <label>Sending Return</label>
                                        <input type="text" required min="0" {{($accept_status ==0 ) ? 'readonly':''}} step="0.001"
                                               id="send_qty_{{$endproduct->id}}"
                                               onkeyup="updateSendValue({{$endproduct->id}},{{$endproduct->distributor_price}},{{$variant_size}})"
                                               class="form-control" name="send_qty[]" value="{{$data->qty}}">
                                    </div>
                                    <div class="col-md-2 send_tot" id="send_value_{{$endproduct->id}}">
                                        <label>{{(($data->qty)*($unitPr))}}</label></div>
                                </div>
                                <hr>
                            @endforeach


                            <div class="row row-clz" >
                                <div class="col-md-2">
                                    <p>Total Value :</p>
                                </div>
                                <div class="col-md-2">
                                    <p id="total_valuex" class="">{{$invoice->all_total}}</p>
                                </div>
                                <div class="col-md-4"></div>
                                <div class="col-md-2">
                                    <p>Send Value :</p>
                                </div>
                                <div class="col-md-2 ">
                                    <p id="send_total_value" class="">{{$invoice->total}}</p>
                                </div>
                            </div>

                        </div>

                        <hr>


                        <input type="hidden" name="total_value" id="hidden_total_value">
                        <input type="hidden" name="all_total_value" id="hidden_all_total_value">

                        <!--button type="submit" onclick="this.form.submit();this.disabled = true;" class="btn btn-clz btn-primary btn-lg btn-block mb-5">Update Return Request-(Will take sevaral time.Please wait.....)
                        </button-->

                        <button type="submit" class="btn btn-clz btn-primary btn-lg btn-block mb-5" >Update Return Request-(Will take sevaral time.Please wait.....)
                        </button>

                    </div>
                </div>
            </div>
            {!!Form::close()!!}
        </div>
    </div>

@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });


        $("#floating-button").click(function () {
            let end_product = $("#end_product option:selected").val();
            let qty = $('#qty').val();
            let type = $('#type').val();
            let type_text = $("#type option:selected").text();
            let total = parseFloat($('#total_value').text()).toFixed(2);


            if (end_product == 0 || qty == '') {
                Command: toastr["error"]("Select End Product and Quantity")
                return false;
            }
        });


        function updateValue(id, price, v_size) {
            let qty_name = '#qty_' + id;
            let total_value = '#total_value_' + id;
            let send_value = '#send_value_' + id;
            let send_qty = '#send_qty_' + id;

            let qty = $(qty_name).val()

            let total_price = 0;

            if (v_size != 0) {
                total_price = (qty * v_size) * price;
            } else {
                total_price = qty * price;
            }

            if ($(send_qty).is('[readonly]')) {

            } else {
                $(send_qty).val(qty)
                $(send_value).empty();
                $(send_value).append(total_price.toFixed(2));
            }

            $(total_value).empty();
            $(total_value).append(total_price.toFixed(2));
            changeTotalValue();
            changeSendTotalValue();
        }


        function updateSendValue(id, price, v_size) {
            let qty_name = '#qty_' + id;
            let total_value = '#total_value_' + id;
            let send_value = '#send_value_' + id;
            let send_qty = '#send_qty_' + id;

            let qty_t = $(qty_name).val()
            let qty = $(send_qty).val()


            if (parseFloat(qty) > parseFloat(qty_t)) {
                swal("Sorry!", "Send quantity must be lower than or equal to total quantity", "error");
                $(send_qty).val(qty_t);
            }


            if (v_size != 0) {
                total_price = (qty * v_size) * price;
            } else {
                total_price = qty * price;
            }

            $(send_value).empty();
            $(send_value).append(total_price.toFixed(2));
            changeSendTotalValue();
        }


        function changeTotalValue() {
            var sum = 0;
            $('.tot').each(function () {
                sum += parseFloat($(this).text());  // Or this.innerHTML, this.innerText
            })

            $('#total_valuex').empty();
            $('#hidden_all_total_value').val(sum);
            $('#total_valuex').append(sum.toFixed(2))

        }

        function changeSendTotalValue() {
            var sum = 0;
            $('.send_tot').each(function () {
                sum += parseFloat($(this).text());  // Or this.innerHTML, this.innerText
            })

            $('#send_total_value').empty();
            $('#hidden_total_value').val(sum);
            $('#send_total_value').append(sum.toFixed(2))
        }

    </script>
@stop
