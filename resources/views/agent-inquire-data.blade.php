<div class="task-name-wrap">
    <div class="row mt-2">
        <div class="col-md-6"><b>Agent Name</b></div>
        <div class="col-md-6"><b>{{$agent->first_name.' '.$agent->last_name}}</b></div>
    </div>
</div>
<div class="task-name-wrap">
    <div class="row mt-2">
        <div class="col-md-6"><b>Territory</b></div>
        <div class="col-md-6"><b>{{$agent->territory}}</b></div>
    </div>
</div>
@foreach($agent_records as $record)
    <div class="task-name-wrap">
        <div class="row mt-2">
            <div class="col-md-6"><b>Current Outstanding</b></div>
   {{--         <div class="col-md-6"><b>{{number_format($record->first()->start_balance+$agent_init_value,2)}}</b></div>  --}}
        </div>
    </div>
    @break
@endforeach

{{--<div class="task-name-wrap">--}}
{{--    <div class="row mt-2">--}}
{{--        <div class="col-md-6"><b>Init Value</b></div>--}}
{{--        <div class="col-md-6"><b>{{number_format($agent_init_value,2)}}</b></div>--}}
{{--    </div>--}}
{{--</div>--}}
<hr style="height:1px;background-color: #00a5bb">


<div class="task-name-wrap">
    <div class="row mt-2">
        <div class="col-md-3"><b>Invoice No.</b></div>
        <div class="col-md-3"><b>BC [ L ]</b></div>
        <div class="col-md-3"><b>BC [ S ]</b></div>
        <div class="col-md-3"><b>CR [ L ]</b></div>
        <div class="col-md-3"><b>CR [ S ]</b></div>
        <div class="col-md-3"><b>LID</b></div>
    </div>
</div>

{{-- @foreach($agent_records as $record) --}}
    {{--    <hr>--}}
    {{--                <div class="task-name-wrap">--}}
    {{--                    <div class="row mt-2">--}}
    {{--                        <div class="col-md-6"><b>Current Outstanding</b></div>--}}
    {{--                        <div class="col-md-6"><b>{{number_format($record->first()->start_balance+$agent_init_value,2)}}</b></div>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <hr>--}}
   {{-- @foreach($record as $data) --}}
        <div class="task-name-wrap">

            <div class="row mt-2">
        {{--        <div class="col-md-3 text-left">{{$data->created_at}}</div>  --}}
                <div class="col-md-3"><i class="fa fa-check-circle" aria-hidden="true"></i>
       {{--             {{$data->reference_code}} - @if(!empty($data->reference_category['name'])){{$data->reference_category['name']}}@endif  --}}
                    {{--$data->sub_desc--}}
                </div>

         {{--       <div class="col-md-3 text-right">{{($data->amount>=0)?number_format($data->amount,2):' '}}</div>
                <div class="col-md-3 text-right">{{($data->amount<0)?number_format($data->amount,2):' '}}</div>

                --}}
            </div>
        </div>
        <hr>
 {{--   @endforeach  --}}



    <div class="task-name-wrap">
        <div class="row mt-2">
            <div class="col-md-6">Final Amount</div>
            <div class="col-md-3 text-right"></div>
{{--            <div class="col-md-3 text-right"> {{number_format($record->last()->end_balance+$agent_init_value,2)}}</div>  --}}
        </div>
    </div>
    <hr>
    <hr style="height:2px;background-color: #501310">

{{-- @endforeach --}}