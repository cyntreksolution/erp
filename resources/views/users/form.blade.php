<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Name</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('first_name',null, ['class' => 'form-control','placeholder'=>'Enter First Name','autocomplete'=>'off','id'=>'first_name','required']) !!}
        </div>
    </div>
</div>
<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Second Name</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('second_name',null, ['class' => 'form-control','placeholder'=>'Enter Second Name','autocomplete'=>'off','id'=>'second_name']) !!}
        </div>
    </div>
</div>
<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Email</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('email', null, ['class' => 'form-control','placeholder'=>'Enter Email','autocomplete'=>'off','id'=>'email','required']) !!}
        </div>
    </div>
</div>
<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Telephone</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('telephone1', null, ['class' => 'form-control','placeholder'=>'Enter Telephone','autocomplete'=>'off','id'=>'telephone1']) !!}
        </div>
    </div>
</div>
<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Address</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('address', null, ['class' => 'form-control','placeholder'=>'Enter Address','autocomplete'=>'off','id'=>'address']) !!}
        </div>
    </div>
</div>
<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">OTP Status</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">

            <input type="checkbox" name="otp_status" class="check" value="1" autocomplete="off" id="otp_status">
{{--            {!! Form::checkbox('otp_status', null, ['class' => 'form-control check','placeholder'=>'Status','autocomplete'=>'off','id'=>'otp_status']) !!}--}}

        </div>
    </div>
</div>
{{--<div class="form-group row">--}}
{{--    <label class="col-form-label text-right col-lg-3 col-sm-12">Password</label>--}}
{{--    <div class="col-lg-9 col-md-9 col-sm-12">--}}
{{--        <div class="input-group">--}}
{{--            {!! Form::password('password', array('placeholder' => 'Enter Password','id'=>'password','class' => 'form-control')) !!}--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
{{--<div class="form-group row">--}}
{{--    <label class="col-form-label text-right col-lg-3 col-sm-12">Confirm Password</label>--}}
{{--    <div class="col-lg-9 col-md-9 col-sm-12">--}}
{{--        <div class="input-group">--}}
{{--            {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control')) !!}--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Role</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">

            {!! Form::select('roles[]', $roles,null, ['class' => 'form-control','id'=>'roles','required']) !!}
        </div>
    </div>
</div>



