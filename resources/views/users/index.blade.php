@extends('layouts.back.master')@section('title','User | List')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.css')}}">

    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }
        #floating-button {
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #db4437;
            position: fixed;
            bottom: 50px;
            right: 30px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .plus {
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }
        input[type="checkbox"] {
            transform: scale(1.5);
            -webkit-transform: scale(1.5);
        }
    </style>

@stop

@section('content')
    @can('role-create')
        <a id="floating-button" data-toggle="modal" data-placement="left" data-original-title="Create"
           data-target="#createModal">
            <p class="plus">+</p>
        </a>
    @endcan
    <div class="app-main-content mb-2">
        <div class="row mx-2">
            <div class="col-md-2">
                {!! Form::select('roles',$roles , null , ['class' => 'form-control','placeholder'=>'Select Role','id'=>'roles']) !!}
            </div>
            <div class="col-md-2">
                <button class="btn btn-info " onclick="process_form(this)">Filter</button>
            </div>

            <!-- loading view -->
        </div>
    </div>
    <div class="">
        <table id="permissiongroup_table" class="display text-center">
            <thead>
            <tr>
                <th>id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Telephone</th>
                <th>Address</th>
                <th>Role</th>
                <th>OTP</th>
                <th>Active</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            {{--                        @foreach($burdens as $burden)--}}
            {{--                            <tr>--}}
            {{--                                <td><input type="checkbox" name="burden_id[]" value="{{$burden->id}}"></td>--}}
            {{--                                <td>{{$burden->serial}}</td>--}}
            {{--                                <td>{{$burden->endProduct->name}}</td>--}}
            {{--                                <td>{{$burden->burden_size}}</td>--}}
            {{--                                <td>{{$burden->expected_end_product_qty	}}</td>--}}
            {{--                                <td>{{$burden->recipe->name}}</td>--}}
            {{--                                <td>--}}
            {{--                                    <button class="btn btn-sm btn-primary"><i class="fa fa-truck"></i></button>--}}
            {{--                                    <button value="{{$burden->id}}" class="addItem btn btn-sm btn-warning ml-1">--}}
            {{--                                        <i class="fa fa-hand-grab-o"></i></button>--}}
            {{--                                </td>--}}
            {{--                            </tr>--}}
            {{--                        @endforeach--}}
            </tbody>
            <tfoot>
            <tr>
                <th>id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Telephone</th>
                <th>Address</th>
                <th>Role</th>
                <th>OTP</th>
                <th>Active</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
        <div class="col-md-12 mt-5 float-right">
            <div class="row">
                <div class="col-md-6">
                    <button class="btn btn-block  d-none btn-primary" id="btn_sumbit1" name="showButton" value="showSubmit">Merge Selected Show
                    </button>
                </div>
                <div class="col-md-6">
                    <button class="btn btn-block d-none  btn-success" id="btn_sumbit2" name="saveButton" value="saveSubmit">Merge Save</button>
                </div>
            </div>

        </div>

    </div>

    <div class="modal fade" id="createModal" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => 'users.store', 'method' => 'post','id'=>'createForm', 'files' => true]) !!}
                <div class="modal-body">
                    @include('users.form')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button
                            type="submit" class="btn btn-primary">Save changes</button>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>

    <div class="modal fade" id="editModal" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => 'users.store', 'method' => 'PATCH','id'=>'editForm', 'files' => true]) !!}
                <div class="modal-body">
                    @include('users.form')

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button
                            type="submit" class="btn btn-primary">Save changes</button>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>



@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
    <script type="text/javascript"
            src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.js')}}"></script>
    <script>

        function chengeotpStatus(row,id) {

            var status_id = $('#myonoffswitch_'+row).prop('checked') == true ? 1 : 0;

            $.ajax({
                type: "GET",
                dataType: "json",
                url: '/user/otpstates',
                data: {'status': status_id, 'id': id},
                success: function(data){
                    toastr.success('successfully');
                    console.log(data.success)


                },
                error: function (msg) {
                    toastr.error( 'Inconceivable!');
                }
            });

        }

        $(document).ready(function () {
            table = $('#permissiongroup_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "ajax": {
                    url: "{{url('/users/table/data')}}",
                    type: "get",
                    error: function () {
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    }
                },
                pageLength: 10,
                responsive: true
            });
        });

        function process_form(e) {

            let roles = $("#roles").val();

            table.ajax.url('/users/table/data?roles=' + roles+ '&filter=' + true).load();
        }
        function deleteuser(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "delete",
                        url: '/users/destroy',
                        data: {
                            id: id,
                        },
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            } else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                });
        }


        function edit(item) {
            let id = item.dataset.id;
            let first_name = item.dataset.first_name;
            let second_name = item.dataset.second_name;
            let email = item.dataset.email;
            let role = item.dataset.role;
            let telephone1 = item.dataset.telephone1;
            let address = item.dataset.address;
            let otp_status = item.dataset.otp_status;

            if (otp_status == 1) {
                $("#editForm").find('#otp_status').prop("checked", true);
            } else {

                $("#editForm").find('#otp_status').prop("checked", false);
            }
            $("#editForm").find('#first_name').val(first_name);
            $("#editForm").find('#second_name').val(second_name);
            $("#editForm").find('#email').val(email);
            $("#editForm").find('#telephone1').val(telephone1);
            $("#editForm").find('#address').val(address);
            $("#editForm").find('#role').val(role);
            $("#editForm").find('#roles').val(role).trigger("change");

            $("#editForm").attr('action', '/users/' + id);

            $('#editModal').modal('show')
        }

    </script>

@stop
