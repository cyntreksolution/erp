@extends('layouts.back.master')@section('title','Issue Raw Materials')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.wizard.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/modal.css')}}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/demos.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/css/site.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/packages/recipe/recipe.css')}}">
@stop


@section('content')
    <div class="app-wrapper">
        <div class="app-panel" id="app-panel">
            <div class="app-search">
                <input type="search" class="search-field" placeholder="Search" disabled>
                <i class="search-icon fa fa-search"></i>
            </div>

            <div class="scroll-container">
                <div class="app-panel-inner">

                    <div class="p-3">
                        @if(Auth::user()->hasRole(['Owner','Production Manager','Super Admin']))
                        <button class="btn btn-success py-3 btn-block btn-lg" data-toggle="modal"
                                data-target="#projects-task-modal">Request Raw Materials
                        </button>

                        @endif
                    </div>
                    <hr class="m-0">
                    <div class="people-list d-flex justify-content-start flex-wrap p-3">

                    </div>
                    <hr class="m-0">
                    <div class="media-list" id="container">


                    </div>
                </div>
            </div>
            <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show"><i
                        class="fa fa-chevron-right"></i>
                <i class="fa fa-chevron-left"></i>
            </a>
        </div>
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">Raw Materials Request List</h5>
            </div>

            <div class="scroll-container">
                <div class="app-main-content">
                    <div class="media-list" id="rcontainer">
                        @if(Auth::user()->hasRole(['Owner','Stock Manager','Super Admin']))
                            @foreach ($items as $key => $ref)

                                @php

                                    $refdata = \RawMaterialManager\Models\RawRequestTable::where('ref','=',$ref->ref)->first();
                                    $raw_name = \RawMaterialManager\Models\RawMaterial::where('raw_material_id','=',$refdata->raw_id)->where('type','!=',5)->first();

                                    $trans_stores = \RawMaterialManager\Models\RawRequestTable::select(DB::raw('sum(qty) as qty'))->where('ref','=',$ref->ref)->first();

                                @endphp

                                @if($refdata->type==1 || $refdata->type==3)

                                    <div class="media">
                                        <div class="avatar avatar-sm bg-success">+
                                        </div>
                                        <div class="media-body">

                                            <h6 class="media-heading" id="media-list-item-3">{{$raw_name->name}} {{$trans_stores}}</h6>
                                            <small>{{$refdata->created_at->format('d - M - Y @ h :i: s')}}</small>
                                            <h6>{{$refdata->ref}}-{{$refdata->description}}</h6>

                                        </div>
                                        @if($refdata->status == -1)
                                        <a class="btn btn-sm btn-warning" target="_blank" href="{{'request/print/'.$refdata->id}}" > <i class="fa fa-print"></i></a> {{--href="{{route('ppcc.show',$refdata)}}" --}}
                                         @else
                                        <button data-toggle="modal"  data-target="#issueRaw{{$refdata->ref,$refdata->raw_id,$trans_stores->qty,$refdata->type,$raw_name->name,$raw_name->available_qty}}" class="btn btn-sm btn-danger">Issue</button>
                                        @endif
                                   {{--         <!--h6 class="media-heading">{{$t->qty}}</h6-->  --}}

                                    </div>
                                @else
                                    <div class="media">
                                        <div class="avatar avatar-sm bg-warning">+
                                        </div>
                                        <div class="media-body">

                                            <h6 class="media-heading" id="media-list-item-3">{{$raw_name->name}} {{$trans_stores}}</h6>
                                            <small>{{$refdata->created_at->format('d - M - Y @ h :i: s')}}</small>
                                            <h6>{{$refdata->ref}}-Issued For Customized Cake</h6>

                                        </div>
                                        @if($refdata->status == -1)
                                            <a class="btn btn-sm btn-warning" target="_blank" href="{{'request/print/'.$refdata->id}}"> <i class="fa fa-print"></i></a>
                                        @else
                                        <button data-toggle="modal"  data-target="#issueRaw{{$refdata->ref,$refdata->raw_id,$trans_stores->qty,$refdata->type,$raw_name->name,$raw_name->available_qty}}" class="btn btn-sm btn-danger">Issue</button>
                                        @endif
                                        {{--         <!--h6 class="media-heading">{{$t->qty}}</h6-->  --}}

                                    </div>
                                @endif

                                <div class="modal fade" id="issueRaw{{$refdata->ref,$refdata->raw_id,$trans_stores->qty,$refdata->type,$raw_name->name,$raw_name->available_qty}}" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
                                     aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            {!! Form::open(['route' => ['raw-issue.stores'], 'method' => 'get','style'=>'width:100%']) !!}
                                            <input name="ref" type="hidden" value="{{$refdata->ref}}">
                                            <input name="raw_id" type="hidden" value="{{$refdata->raw_id}}">
                                            <input name="qty" type="hidden" value="{{$trans_stores->qty}}">
                                            <input name="type" type="hidden" value="{{$refdata->type}}">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                                                    <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                                                </button>
                                            </div>
                                            <div class="modal-body">


                                                <div class="task-name-wrap">
                                                    <label class="col-8 col-form-label"><b>{{$raw_name->name}}</b> - Available Qty : {{$raw_name->available_qty}} </label>
                                                    <div>
                                                        <div><label class="col-8 col-form-label">Requested Qty</label></div>
                                                    <input class="form-control" type="text" name="exqty"
                                                           value="{{$trans_stores->qty}}" readonly>

                                                    </div>
                                                    <br>
                                                    <div class="task-name-wrap measurer">
                                                        {!! Form::select('emp', $employees , null , ['placeholder'=>'Select Measurer','required','class' => 'form-control select2','id'=>'emp']) !!}
                                                    </div>
                                                </div>
                                                <br>

                                                @if($trans_stores->qty < $raw_name->available_qty)
                                                <div class="pager d-flex justify-content-center">
                                                    <input type="submit" id="issue" class="finish btn btn-success btn-block"
                                                           value="Issue">
                                                </div>
                                                @else
                                                   <div>
                                                       <label class="col-8 col-form-label"><b>Insufficient Available Stock........ </b></label>

                                                   </div>
                                                @endif

                                            </div>





                                            {!!Form::close()!!}
                                        </div>
                                    </div>
                                </div>



                            @endforeach
                        @else
                            @foreach($transaction as $t)
                                @php
                                    $raw_name = \RawMaterialManager\Models\RawMaterial::where('raw_material_id','=',$t->raw_id)->where('type','!=',5)->first();


                                  if($t->type==2){
                                    $serial = \EndProductBurdenManager\Models\EndProductBurden::where('id','=',$t->description)->first();
                                }
                                @endphp
                                @if($t->type==1 || $t->type==3)
                                    <div class="media">
                                        <div class="avatar avatar-sm bg-success">+
                                        </div>
                                        <div class="media-body">

                                            <h6 class="media-heading" id="media-list-item-3">{{$raw_name->name}}</h6>
                                            <small>{{$t->created_at->format('d - M - Y @ h :i: s')}}</small>
                                            <h6>{{$t->ref}}-{{$t->description}}</h6>

                                        </div>

                                        <h6 class="media-heading">{{$t->qty}}</h6>

                                    </div>
                                @else
                                    <div class="media">
                                        <div class="avatar avatar-sm bg-warning">+
                                        </div>
                                        <div class="media-body">
                                            <h6 class="media-heading" id="media-list-item-3">{{$raw_name->name}} [ Volume- {{$t->qty}} ]</h6>
                                            <small>{{$t->created_at->format('d - M - Y @ h :i: s')}}</small>
                                            <h6>{{$t->ref}} | {{$serial->serial}}</h6>
                                        </div>

                                        <h6 class="media-heading">{{$t->qty}}</h6>

                                    </div>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="projects-task-modal" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12">
                        <div class="card">

                            {!! Form::open(['route' => 'raw-request.store','id'=>'end-recipe-form', 'method' => 'get']) !!}
                            {{-- Form::open(array('url' => 'issue','id'=>'end-recipe-form')) --}}
                            <div class="wizard p-4" id="bootstrap-wizard-1">
                                <ul class="nav nav-tabs vertices keep-prefix-suffix" role="tablist">
                                    <li class="nav-item">
                                        <a class=" nav-link active" data-toggle="tab" href="#ex5-step-1"
                                           role="tab">
                                        </a>
                                    </li>
                                    <li class="nav-item" id="xx2">
                                        <a class="nav-link" data-toggle="tab" href="#ex5-step-2"
                                           role="tab">
                                        </a>
                                    </li>

                                </ul>
                                <div class="tab-content">

                                    {{--<div class="tab-pane active" id="ex5-step-1" role="tabpanel">--}}
                                    <div class="tab-pane active" id="ex5-step-1" role="tabpanel">
                                        <div class="task-name-wrap">
                                            {!! Form::select('type', ['Production Purpose','Customize Cake','Refill Deep Fryers'] , null , ['required','placeholder'=>'Select One','class' => 'form-control select2','id'=>"type"]) !!}
                                        </div>

                                    <br>

                                        <div id="div_description" class="mb-2 d-none div-desc">
                                            <div class="row mb-2">
                                                <div class="col-md-6">
                                                    {!! Form::select('raw_material', $raw_materials , null , ['placeholder'=>'Select Raw Material','required','class' => 'form-control select2','id'=>'raw_material']) !!}
                                                </div>
                                                <div class="col-md-6">
                                                    {!! Form::number('qty', null, ['id'=>'qty','class' => 'form-control','required','placeholder'=>'Enter Quantity','onKeyUp'=>'updateData()']) !!}
                                                </div>
                                            </div>

                                            <div class="task-name-wrap">
                                                {!! Form::textarea('description', null, ['class' => 'form-control','required','id'=>'description','placeholder'=>'Description Here...']) !!}
                                            </div>
                                        </div>


                                        <div id="div_description_customize_cake" class="mb-2 d-none div-desc">
                                            <div class="row mb-2">
                                                <div class="col-md-12">
                                                    {!! Form::select('raw_material_c', $raw_materials , null , ['placeholder'=>'Select Raw Material','required','class' => 'form-control select2','id'=>'raw_material2']) !!}
                                                </div>
                                            </div>

                                            <div class="row mb-2" id="customize_cake_list">
                                                @foreach($custo as $item)
                                                    <div class="col-md-4">
                                                        <p>{{optional($item->endProduct)->name}}</p>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <p>{{$item->serial}}-[{{$item->burden_size}}]</p>
                                                        <input type="hidden" name="customized_burden_id[]" value="{{$item->id}}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        {!! Form::text('customized_qty[]', 0, ['id'=>'qty','class' => 'form-control cc-qty','required','placeholder'=>'Enter Quantity','onKeyUp'=>'updateQTY()']) !!}
                                                    </div>
                                                @endforeach
                                                <div class="col-md-4"></div>
                                                <div class="col-md-4">
                                                    <p>Total QTY</p>
                                                </div>
                                                <div class="col-md-4">
                                                    <p id="total_update_qty">0.00</p>
                                                </div>

                                            </div>

                                            <input type="submit" id="finish-btn" class="finish btn btn-warning w-50"
                                                   value="Request"/>
                                        </div>


                                    </div>

                                    <!--div class="tab-pane" id="ex5-step-2" role="tabpanel">
                                        <div id="row_material_serial"></div>
                                        <hr>
                                        <div class="task-name-wrap">
                                            <div class="row">
                                                <div class="col-md-6 text-nowrap">Raw Material</div>

                                                <div class="col-md-3 text-nowrap text-right">Available Qty</div>

                                                <div class="col-md-3 text-nowrap text-right">QTY</div>
                                            </div>
                                        </div>
                                        <div id="row_material_list"></div>
                                        <hr>

                                    </div-->

                                </div>
                                <div class="pager d-flex justify-content-center mt-3">
                                    <input type="submit" id="finish-btn" class="finish btn btn-success w-50"
                                           value="Issue"/>
                                    <input type="submit" id="finish-btn2" class="d-none btn btn-success w-50"
                                           value="Issue2"/>

                                    <!--button type="button" id="next-btn" class="next btn btn-success w-50">Next
                                    </button-->
                                </div>
                            </div>
                            {{ Form::close() }}

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    {{--    <script src="{{asset('assets/examples/js/apps/projects.js')}}"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('assets/packages/semi_finish/semi_finish.js')}}"></script>
    <script src="{{asset('assets/packages/recipe/end-recipe.js')}}"></script>

    <script>
        function updateQTY(){
            let total = 0;
            $('.cc-qty').each(function(){
                total += parseFloat(this.value);
            });
            $('#total_update_qty').empty();
            $('#total_update_qty').append(total);
        }


        $("#type").change(function () {
            let type = $("#type option:selected").text();
            let typeVal = $("#type option:selected").val();

            if (typeVal == 1){
                $('.div-desc').addClass('d-none');
                $('#div_description_customize_cake').removeClass('d-none');
                $('#finish-btn').removeClass('d-none');
            }
            else {
                $('#div_description').removeClass('d-none')
               /* $('#div_merge_serial').addClass('d-none')
                $('#div_description1').addClass('d-none')
                $('#div_description2').addClass('d-none')
                $('#div_description3').addClass('d-none')
                $('#div_description4').addClass('d-none')*/
                $('#div_description_customize_cake').addClass('d-none');
                $('#finish-btn').removeClass('d-none');
            }

        }).trigger("change");



        function updateData() {
            let raw_material = $("#raw_material option:selected").val();
            let qty = $("#qty").val();

            $('#row_material_list').empty();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "/raw_material/get/raw",
                type: "POST",
                data: {
                    id: raw_material
                },
                success: function (data) {
                    appendRaw(data, qty);
                },
                error: function (xhr, status) {

                }
            });
        }


        $("#raw_material").change(function () {
            let raw_material = $("#raw_material option:selected").val();
            let qty = $("#qty").val();

            $('#row_material_list').empty();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "/raw_material/get/raw",
                type: "POST",
                data: {
                    id: raw_material
                },
                success: function (data) {
                    appendRaw(data, qty);

                },
                error: function (xhr, status) {

                }
            });
        }).trigger("change");


    </script>
@stop

