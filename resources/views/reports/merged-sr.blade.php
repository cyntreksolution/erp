@extends('layouts.back.master')@section('title','Repot')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }
    </style>
@stop

@section('content')

    {{--    <div class="row">--}}
    {{--        <div class="col-sm-4">--}}
    {{--            <div class="form-group">--}}
    {{--                {!! Form::select('category', $categories , null , ['class' => 'form-control','placeholder'=>'Select Category','id'=>'category']) !!}--}}
    {{--            </div>--}}
    {{--        </div>--}}

    {{--        <div class="col-sm-4">--}}
    {{--            <div class="form-group">--}}
    {{--                {!! Form::select('end_product', $end_products , null , ['class' => 'form-control','placeholder'=>'Select End Product','id'=>'end_product']) !!}--}}
    {{--            </div>--}}
    {{--        </div>--}}

    {{--        <div class="col-sm-4">--}}
    {{--            <div class="form-group">--}}
    {{--                <button type="button" class="btn btn-success" onclick="process_form()">Filter Now</button>--}}
    {{--                <button type="button" class="btn btn-warning" onclick="process_form_reset()">Reset Filter</button>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}

    {{--    {!! Form::open(['route' => 'mgvsl.items', 'method' => 'post']) !!}--}}
    <div class="row">
        <div class="col-md-12">
            <h4 class="ml-2 p-2">Summary</h4>
            <table id="merge_table" class="display text-center">
                <thead>
                <tr>
                    <th>End Product</th>
                    <th>Burden Data</th>
                    <th>Invoice Data</th>
                    <th>Burden QTY</th>
                    <th>Invoice QTY</th>
                </tr>
                </thead>
                <tbody>
                @foreach($row as $item)
                    <tr>
                        <td>{{$item['name']}}</td>
                        <td>
                            @if ($item['burden_total']>0)
                                <a class="btn btn-primary" data-toggle="collapse" href="#ID_{{$item['id']}}"
                                   aria-expanded="false">
                                    Burden Details
                                </a>
                            @endif

                        </td>
                        <td>
                            @if ($item['loading_total']>0)
                                <a class="btn btn-primary" data-toggle="collapse" href="#ID_inv_{{$item['id']}}"
                                   aria-expanded="false">
                                    Invoice Details
                                </a>
                            @endif
                        </td>
                        <td>{{$item['burden_total']}}</td>
                        <td>{{$item['loading_total']}}</td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <div class="collapse" id="ID_{{$item['id']}}">
                                <div class="well">
                                    <table class="table table-hover table-bordered"
                                           style="background:#fff; margin-bottom:10px;">
                                        <thead>
                                        <tr class="inverse">
                                            <th>Burden Id</th>
                                            <th>QTY</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($item['burden_data'] as $i)
                                            <tr>
                                                <td>{{$i['burden_serial']}}</td>
                                                <td>{{$i['qty']}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <div class="collapse" id="ID_inv_{{$item['id']}}">
                                <div class="well">
                                    <table class="table table-hover table-bordered"
                                           style="background:#fff; margin-bottom:10px;">
                                        <thead>
                                        <tr class="inverse">
                                            <th>Invoice Id</th>
                                            <th>QTY</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($item['loading_data'] as $i)
                                            <tr>
                                                <td>{{$i['invoice_id']}}</td>
                                                <td>{{$i['qty']}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>


    {{--    {!! Form::close() !!}--}}
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script>
        // $(".btnx").click(function() {
        //     if($("#collapseme").hasClass("out")) {
        //         $("#collapseme").addClass("in");
        //         $("#collapseme").removeClass("out");
        //     } else {
        //         $("#collapseme").addClass("out");
        //         $("#collapseme").removeClass("in");
        //     }
        // });

        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": false,
            "singleDatePicker": true,
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019"
        });
        $('#date_range').val('')

        $(document).ready(function () {
            table = $('#merge_table').DataTable({
                searching: true,
                "ordering": false,
                pageLength: 25,
                responsive: true
            });
        });

        $(document).ready(function () {
            table = $('#loading_table').DataTable({
                searching: true,
                "ordering": false,
                pageLength: 25,
                responsive: true
            });
        });


    </script>

@stop