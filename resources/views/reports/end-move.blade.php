@extends('layouts.back.master')@section('title','End Product Movements')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <style>
        th {
            text-align: right !important;
        }

        td {
            text-align: right !important;
        }
    </style>
@stop

@section('content')


    @if(Auth::user()->hasRole(['Production Manager','Owner','Super Admin']))

            <div class="row mb-3 p-2">
                <div class="col-md-2">
                    {!! Form::select('end_products',$end_products , null , ['class' => 'form-control','placeholder'=>'Select End Product','id'=>'end_product']) !!}
                </div>
                <div class="col-md-2">
                    {!! Form::select('grade',[1=>'Grade A',2=>'Grade B',3=>'Grade C',4=>'Grade D',9 =>'To Loading'] , null , ['class' => 'form-control','placeholder'=>'Select Grade','id'=>'grade']) !!}
                </div>

                <div class="col-md-2">
                    {!! Form::select('category',$categories, null , ['class' => 'form-control','placeholder'=>'Select Category','id'=>'category']) !!}
                </div>

                <div class="col-md-2">
                    <input type="text" name="date" id="date_range" class="form-control" placeholder="Select Date Range">
                </div>

                <div class="col-md-2">
                    <button class="btn btn-info " onclick="process_form(this)">Filter</button>
                </div>
            </div>
        @elseif(Auth::user()->hasRole(['Sales Agent','Owner','Super Admin']))

            <div class="row mb-3 p-2">
                <div class="col-md-2">
                    {!! Form::select('end_products',$end_products , null , ['class' => 'form-control','placeholder'=>'Select End Product','id'=>'end_product_sales']) !!}
                </div>
                <div class="col-md-2">
                    {!! Form::select('agent_sales',$agents->pluck('name_with_initials','id'), null , ['class' => 'form-control','placeholder'=>'Select Agent','id'=>'agent_sales']) !!}
                </div>

                <div class="col-md-2">
                    {!! Form::select('category',$categories, null , ['class' => 'form-control','placeholder'=>'Select Category','id'=>'category_sales']) !!}
                </div>

                <div class="col-md-2">
                    {!! Form::select('item_type',['1'=>'Non-Variant','2'=>'Variant'], null , ['class' => 'form-control','placeholder'=>'Select Type','id'=>'item_type']) !!}
                </div>

                <div class="col-md-2">
                    <input type="text" name="date" id="date_range_sales" class="form-control"
                           placeholder="Select Date Range">
                </div>

                <div class="col-md-1 mr-2">
                    <button class="btn btn-success -align-right text-right" onclick="process_form_sales(this)">Filter Sales
                    </button>
                </div>
            </div>
            <div class="row mb-3 p-2">
                <div class="col-md-2">
                    {!! Form::select('end_products',$end_products , null , ['class' => 'form-control','placeholder'=>'Select End Product','id'=>'end_product_return']) !!}
                </div>

                <div class="col-md-2">
                    <select class="form-control" placeholder="Select Agents" id="agent_return">
                        <option value="null">Select Agent</option>
                        @foreach($agents as $agent)
                            <option value="{{$agent->id}}">{{$agent->name_with_initials}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-md-1">
                    {!! Form::select('category',$categories, null , ['class' => 'form-control','placeholder'=>'Category','id'=>'category_return']) !!}
                </div>

                <div class="col-md-2">
                    {!! Form::select('type',[2=>'By System Generated',3=>'By Agent',4=>'EP Merge Removals',5=>'Stores Return'], null , ['class' => 'form-control','placeholder'=>'Select Type','id'=>'type']) !!}
                </div>

                <div class="col-md-1">
                    {!! Form::select('reuse_type',['re_use'=>'Re Use','destroy'=>'Destroy','stock_rotation'=>'Stock Rotation','factory_fault'=>'Factory Fault','lost'=>'Lost QTY'], null , ['class' => 'form-control','placeholder'=>'Select Type','id'=>'reuse_type']) !!}
                </div>

                <div class="col-md-2">
                    <input type="text" name="date" id="date_range_return" class="form-control"
                           placeholder="Select Date Range">
                </div>

                <div class="col-md-1">
                    <button class="btn btn-danger " onclick="process_form_return(this)">Filter Return</button>
                </div>
            </div>

        @else
            <div class="row mb-3 p-2">
                <div class="col-md-2">
                    {!! Form::select('end_products',$end_products , null , ['class' => 'form-control','placeholder'=>'Select End Product','id'=>'end_product']) !!}
                </div>
                <div class="col-md-2">
                    {!! Form::select('grade',[1=>'Grade A',2=>'Grade B',3=>'Grade C',4=>'Grade D',9 =>'To Loading'] , null , ['class' => 'form-control','placeholder'=>'Select Grade','id'=>'grade']) !!}
                </div>

                <div class="col-md-2">
                    {!! Form::select('category',$categories, null , ['class' => 'form-control','placeholder'=>'Select Category','id'=>'category']) !!}
                </div>

                <div class="col-md-2">
                    <input type="text" name="date" id="date_range" class="form-control" placeholder="Select Date Range">
                </div>

                <div class="col-md-2">
                    <button class="btn btn-info " onclick="process_form(this)">Filter</button>
                </div>
            </div>

            <div class="row mb-3 p-2">
                <div class="col-md-2">
                    {!! Form::select('end_products',$end_products , null , ['class' => 'form-control','placeholder'=>'Select End Product','id'=>'end_product_sales']) !!}
                </div>
                <div class="col-md-2">
                    {!! Form::select('agent_sales',$agents->pluck('name_with_initials','id'), null , ['class' => 'form-control','placeholder'=>'Select Agent','id'=>'agent_sales']) !!}
                </div>

                <div class="col-md-2">
                    {!! Form::select('category',$categories, null , ['class' => 'form-control','placeholder'=>'Select Category','id'=>'category_sales']) !!}
                </div>

                <div class="col-md-2">
                    {!! Form::select('item_type',['1'=>'Non-Variant','2'=>'Variant'], null , ['class' => 'form-control','placeholder'=>'Select Type','id'=>'item_type']) !!}
                </div>

                <div class="col-md-2">
                    <input type="text" name="date" id="date_range_sales" class="form-control"
                           placeholder="Select Date Range">
                </div>

                <div class="col-md-1 mr-2">
                    <button class="btn btn-success -align-right text-right" onclick="process_form_sales(this)">Filter Sales
                    </button>
                </div>
            </div>
            <div class="row mb-3 p-2">
                <div class="col-md-2">
                    {!! Form::select('end_products',$end_products , null , ['class' => 'form-control','placeholder'=>'Select End Product','id'=>'end_product_return']) !!}
                </div>

                <div class="col-md-2">
                    <select class="form-control" placeholder="Select Agents" id="agent_return">
                        <option value="null">Select Agent</option>
                        @foreach($agents as $agent)
                            <option value="{{$agent->id}}">{{$agent->name_with_initials}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-md-1">
                    {!! Form::select('category',$categories, null , ['class' => 'form-control','placeholder'=>'Category','id'=>'category_return']) !!}
                </div>

                <div class="col-md-2">
                    {!! Form::select('type',[2=>'By System Generated',3=>'By Agent',4=>'EP Merge Removals',5=>'Stores Return'], null , ['class' => 'form-control','placeholder'=>'Select Type','id'=>'type']) !!}
                </div>

                <div class="col-md-1">
                    {!! Form::select('reuse_type',['re_use'=>'Re Use','destroy'=>'Destroy','stock_rotation'=>'Stock Rotation','factory_fault'=>'Factory Fault','lost'=>'Lost QTY'], null , ['class' => 'form-control','placeholder'=>'Select Type','id'=>'reuse_type']) !!}
                </div>

                <div class="col-md-2">
                    <input type="text" name="date" id="date_range_return" class="form-control"
                           placeholder="Select Date Range">
                </div>

                <div class="col-md-1">
                    <button class="btn btn-danger " onclick="process_form_return(this)">Filter Return</button>
                </div>
            </div>


        @endif


        <div class="table-responsive">
            <table id="esr_table" class="display text-center ">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th class="text-right">QTY</th>
                    <th class="text-right">Agent Accepted QTY</th>
                    <th class="text-right">Price</th>
                    <th>Remark</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>Date</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th class="text-right">QTY</th>
                    <th class="text-right">Agent Accepted QTY</th>
                    <th class="text-right">Price</th>
                    <th>Remark</th>
                    <th>Action</th>
                </tr>
                </tfoot>
            </table>
        </div>

@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script>
        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019",
            "maxDate": moment()
        });

        $('#date_range_sales').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019",
            "maxDate": moment()
        });

        $('#date_range_return').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019",
            "maxDate": moment()
        });


        var total_sum;
        var total_amount;
        $(document).ready(function () {
            table = $('#esr_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "ajax": {
                    url: "{{url('/reports/end-product/movements/table/data')}}",
                    type: "get",
                    error: function () {
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    },
                    dataSrc: function (data) {

                        total_sum = data.total_qty;
                        total_amount = data.total_amount;

                        return data.data;

                    }
                },

                drawCallback: function (settings) {
                    var api = this.api();
                    $(api.column(3).footer()).html(
                        'Total Qty : ' + total_sum
                    );

                    $(api.column(5).footer()).html(
                        'Total Amount : ' + total_amount
                    );
                },
                pageLength: 25,
                responsive: true
            });

            var column = table.column(4);
            column.visible(false);
        });

        function process_form(e) {
            let grade = $("#grade").val();
            let end_product = $("#end_product").val();
            let date_range = $("#date_range").val();
            let category = $("#category").val();
            var column = table.column(4);
            column.visible(false);
            table.ajax.url('/reports/end-product/movements/table/data?end_product=' + end_product + '&date_range=' + date_range + '&grade=' + grade + '&category=' + category + '&filter=' + true).load();
        }

        function process_form_sales(e) {
            let agent = $("#agent_sales").val();
            let end_product = $("#end_product_sales").val();
            let date_range = $("#date_range_sales").val();
            let category = $("#category_sales").val();
            let item_type = $("#item_type").val();
            /* var column = table.column(5);
             column.visible(true);
             var column = table.column(5);
             column.visible(false);*/
            table.ajax.url('/reports/end-product/movements/table/sales/data?end_product=' + end_product + '&date_range=' + date_range + '&item_type=' + item_type + '&agent=' + agent + '&category=' + category + '&filter=' + true).load();
        }

        function process_form_return(e) {
            let agent = $("#agent_return").val();
            let end_product = $("#end_product_return").val();
            let date_range = $("#date_range_return").val();
            let category = $("#category_return").val();
            let type = $("#type").val();
            let reuse_type = $("#reuse_type").val();
            var column = table.column(4);
            column.visible(false);
            table.ajax.url('/reports/end-product/movements/table/return/data?end_product=' + end_product + '&date_range=' + date_range + '&type=' + type + '&reuse_type=' + reuse_type + '&agent=' + agent + '&category=' + category + '&filter=' + true).load();
        }


    </script>

@stop
