@extends('layouts.back.master')@section('title','End Inquiry List')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }
    </style>
@stop

@section('content')

    @if((Sentinel::check()->roles[0]->slug=='owner') || (Sentinel::check()->roles[0]->slug == 'sales-manager') || (Sentinel::check()->roles[0]->slug == 'assistant'))
        <div class="row mb-1">
            <div class="col-md-1">
                {!! Form::select('category',$categories , null , ['class' => 'form-control','placeholder'=>'Select Category','id'=>'category']) !!}
            </div>
            <div class="col-md-2">
                {!! Form::select('end_product',$end_products , null , ['class' => 'form-control','placeholder'=>'Select End Product','id'=>'end_product']) !!}
            </div>
            <div class="col-md-2">

                {!! Form::select('full_name',$agents , null , ['class' => 'form-control','placeholder'=>'Select Agent','id'=>'agent']) !!}

            </div>
            <div class="col-sm-1">
                <select name="type" id="type" required class="form-control select2 ">


                    <!--option value="1">Order Vs. Sold - (Non Variants)</option>
                    <option value="11">Order Vs. Sold - (Variants)</option>
                    <option value="2">Order Vs. Accepted - (Non Variants)</option>
                    <option value="22">Order Vs. Accepted - (Variants)</option>
                    <option value="3">Sold Vs. Accepted - (Non Variants)</option>
                    <option value="33">Sold Vs. Accepted - (Variants)</option-->
                    <option value="6">Return Vs. Sent</option>
                    <!--option value="4">Order Vs. Return</option>
                    <option value="5">Accepted Vs. Return</option-->

                </select>
            </div>

            <div class="col-md-2">
                <input type="text" name="date" id="date_range" class="form-control" placeholder="Select Date" required>
            </div>

            <div class="col-md-2">
                <button class="btn btn-info " onclick="process_form(this)">Filter</button>
            </div>

            <!-- loading view -->


        </div>
    @endif

    <div class="table-responsive">
        <table id="endi_table" class="display text-center ">
            <thead>
            <tr>
                <th>Date</th>
                <th>Product Name</th>
                <th>Agent Name</th>
                <th>Type 1 Qty</th>
                <th>Type 1 Value</th>
                <th>Type 2 Qty</th>
                <th>Type 2 Value</th>
                <th>Difference</th>
                <th>Amount</th>
                <th>%</th>
                <th>Action</th>
            </tr>
            </thead>
            <tfoot>
            <tr>

                <th>Date</th>
                <th>Product Name</th>
                <th>Agent Name</th>
                <th>Type 1 Qty</th>
                <th>Type 1 Value</th>
                <th>Type 2 Qty</th>
                <th>Type 2 Value</th>
                <th>Difference</th>
                <th>Amount</th>
                <th>%</th>
                <th>Action</th>

            </tr>
            </tfoot>
        </table>
    </div>

@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script>
        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019",
            "maxDate":moment()
        });


        $('#date_range').val('')
        $(document).ready(function () {
            table = $('#endi_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "ajax": {
                    url: "{{url('/sales/comparison/sales/return')}}",
                    type: "get",
                    error: function () {
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    }
                },
                pageLength: 100,
                responsive: true
            });
        });

        function process_form(e) {
            let end_product = $("#end_product").val();
            let table = $('#endi_table').DataTable();
            let category = $("#category").val();
            let agent_id = $("#agent").val();
            let type = $("#type").val();
            let date_range = $("#date_range").val();

            table.ajax.url('/sales/comparison/sales/return?end_product=' + end_product + '&date_range=' + date_range  + '&category=' + category  + '&agent_id=' + agent_id  + '&type=' + type  + '&filter=' + true).load();
        }


        function process_form_reset() {
            $("#end_product").val('');

            let table = $('#endi_table').DataTable();
            table.ajax.url('/sales/comparison/sales/return').load();
        }

    </script>

@stop
