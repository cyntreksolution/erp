@extends('layouts.back.master')@section('title','Role | List')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }
        #floating-button {
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #db4437;
            position: fixed;
            bottom: 50px;
            right: 30px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .plus {
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }
        input[type="checkbox"] {
            transform: scale(1.5);
            -webkit-transform: scale(1.5);
        }
    </style>

@stop

@section('content')
    @can('role-create')
        <a id="floating-button" data-toggle="modal" data-placement="left" data-original-title="Create"
           data-target="#createModal">
            <p class="plus">+</p>
        </a>
    @endcan
    <div class="">
        <table id="permissiongroup_table" class="display text-center">
            <thead>
            <tr>
                <th>id</th>
                <th>Name</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
            <tfoot>
            <tr>
                <th>id</th>
                <th>Name</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
        <div class="col-md-12 mt-5 float-right">
            <div class="row">
                <div class="col-md-6">
                    <button class="btn btn-block  d-none btn-primary" id="btn_sumbit1" name="showButton" value="showSubmit">Merge Selected Show
                    </button>
                </div>
                <div class="col-md-6">
                    <button class="btn btn-block d-none  btn-success" id="btn_sumbit2" name="saveButton" value="saveSubmit">Merge Save</button>
                </div>
            </div>

        </div>

    </div>

    <div class="modal fade" id="createModal" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New Role</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => 'roles.store', 'method' => 'post','id'=>'createForm', 'files' => true]) !!}
                <div class="modal-body" style = "height:700px; line-height: 3em; overflow:scroll;  padding: 5px;">
                    @include('roles.form')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button
                            type="submit" class="btn btn-primary">Save changes</button>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>

    <div class="modal fade" id="editModal" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Role</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => 'roles.store', 'method' => 'PATCH','id'=>'editForm', 'files' => true]) !!}
                <div class="modal-body" id="all_Details" style = "height:700px; line-height: 3em; overflow:scroll;  padding: 5px;">
{{--                    @include('roles.form')--}}

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button
                            type="submit" class="btn btn-primary">Save changes</button>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>



@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
    <script type="text/javascript"
            src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js"></script>


    <script>


        $(document).ready(function () {
            table = $('#permissiongroup_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "ajax": {
                    url: "{{url('/roles/table/data')}}",
                    type: "get",
                    error: function () {
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    }
                },
                pageLength: 250,
                responsive: true
            });
        });

        function deleterole(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "delete",
                        url: '/roles/destroy',
                        data: {
                            id: id,
                        },
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            } else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                });
        }


        function edit(item) {
            let id = item.dataset.id;
            let name = item.dataset.name;
            let permission = item.dataset.permission;


            $("#editForm").find('#name').val(name);
            $("#editForm").find('#permission').val(permission);

            $('#all_Details').empty();
            $("#editForm").attr('action', '/roles/' + id);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({

                type: "post",
                url: '/roles/details',
                data: {
                    id: id,
                },
                success: function (response) {
                    $('#editModal').modal('show')
                    $('#all_Details').append(response);
                    $("#editForm").find('#name').val(name);

                }
            });

        }

    </script>

@stop
