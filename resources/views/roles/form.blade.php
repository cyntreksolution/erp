<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Name</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('name', null, ['class' => 'form-control','placeholder'=>'Enter Name','autocomplete'=>'off','id'=>'name','required']) !!}
        </div>
    </div>
</div>
<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Name</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="row">

            @foreach($permissionGroups as $key=> $value)

                <div class="col-lg-6 col-md-6 col-sm-12">
                <ol>
                    <li style="list-style-type: none; ">
                        <label style="font-weight: bold;color: #0a6aa1">
                            <input type="checkbox" id="permission_group" class="selectall" name="permission_group[]" data-group="{{$key}}">
{{--                            {{ Form::checkbox('permission_group[]', null, array('data-group'=>"$key",'class' => 'selectall','id' => 'permission_group')) }}--}}
                            {{ $value->name }}</label>
                        <br/>
                    </li>
                    <ul><li style="list-style-type: none;">
                            @foreach($value->permissions as $permission)
                                <label>{{ Form::checkbox('permission[]', $permission->id, in_array($permission->id, $rolePermissions) ? true : false, array('class' => 'name child','id' => 'permission','data-group'=>"$key")) }}
                                    {{ $permission->name }}</label>
                                <br/>
                            @endforeach
                        </li></ul>
                </ol>
                </div>

            @endforeach
        </div>

    </div>
</div>

<script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script>
    (function($){
        $('.child').change(function(){
            // create var for parent .checkall and group
            var group = $(this).data('group'),
                checkall = $('.selectall[data-group="'+group+'"]');

            // do we have some checked? Some unchecked? Store as boolean variables
            var someChecked = $('.child[data-group="'+group+'"]:checkbox:checked').length > 0;
            var someUnchecked = $('.child[data-group="'+group+'"]:checkbox:not(:checked)').length > 0;

            // if we have some checked and unchecked, set .checkall, of the correct group, to indeterminate.
            // If all are checked, set .checkall to checked
            checkall.prop("indeterminate", someChecked && someUnchecked);
            checkall.prop("checked", someChecked || !someUnchecked);

            // fire change() when this loads to ensure states are updated on page load
        }).change();

        // clicking .checkall will check all children in the same group.
        $('.selectall').click(function() {
            var group = $(this).data('group');
            $('.child[data-group="'+group+'"]').prop('checked', this.checked).change();
        });
    }(window.jQuery));
</script>



