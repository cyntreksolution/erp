@extends('layouts.back.master')@section('title','Roles Create')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Create New Role</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('roles.index') }}"> Back</a>
            </div>
        </div>
    </div>


    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    {!! Form::open(array('route' => 'roles.store','method'=>'POST')) !!}
    {{ csrf_field() }}
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Permission:</strong>
                <br/>
                @foreach($permissionGroups as $value)
                    <ol>
                        <li style="list-style-type: none; ">
                            <label style="font-weight: bold;color: #0a6aa1">{{ Form::checkbox('permission_group[]', $value->id,false, array('class' => 'name permission_group'.$value->id,'id' => 'permission_group')) }}
                                {{ $value->name }}</label>
                            <br/>
                        </li>
                        <ul><li style="list-style-type: none;">
                                @foreach($value->permissions as $permission)
                                    <label>{{ Form::checkbox('permission[]', $permission->id,false, array('class' => 'name','id' => 'permission')) }}
                                        {{ $permission->name }}</label>
                                    <br/>
                                @endforeach
                            </li></ul>
                    </ol>
                @endforeach
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center" >
            <button type="submit" class="btn btn-primary" style="float: right">Submit</button>
        </div>
    </div>
    {!! Form::close() !!}
@stop
