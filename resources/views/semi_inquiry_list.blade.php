@extends('layouts.back.master')@section('title','Semi Inquiry List')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }
    </style>
@stop

@section('content')

    <div class="row mb-2">

        <div class="col-md-2">
            {!! Form::select('semi_product',$semi_products , null , ['class' => 'form-control','placeholder'=>'Select Semi Product','id'=>'semi_product']) !!}
        </div>
        <div class="col-md-3">
            {{--!! Form::select('reference_type',[
    'a'=>'Burden Request',
1=>'Burden Request - Production',
2=>'Cooking Request - Production',
3=>'Cooking Request',
4=>'Customize Cake',
5=>'Destroy and Return',
6=>'Burden Grade',
7=>'Grade A - Production',
8=>'Grade A',
9=>'Grade B',
10=>'Grade C',
11=>'Grade D',
14=>'Cooking Request Grade',
15=>'Cooking Request Grade - Production',
12=>'Wastage - Burden',
13=>'Wastage - Cooking Request'
14=>'Admin Adjustment'] , null , ['class' => 'form-control','placeholder'=>'Select Type','id'=>'reference_type']) !!--}}

            {!! Form::select('meta',[
            1=>'Grade Burden Request',
            2=>'Grade Cooking Request',
            3=>'End Product Burden Request',
            4=>'Production Purpose',
            5=>'Customize Cake',
            6=>'Destroy and Return',
            7=>'Admin Adjustment',
            8=>'Burden Grade-Production',
            9=>'Cooking Request Grade-Production',
            10=>'Issued For Burden Request-Production',
            11=>'Issued For Cooking Request-Production',
            12=>'Burden Wastage-Production',
            13=>'Cooking Request Wastage-Production',
            14=>'Allocate For Make End Product'] , null , ['class' => 'form-control','placeholder'=>'Select Type','id'=>'meta']) !!}



            {{--                {!! Form::select('reference_type',['BurdenManager\Models\BurdenGrade'=>'Burden Grade','EndProductBurdenManager\Models\BurdenSemi'=>'Burden Semi','ShortEatsManager\Models\CookingRequest'=>'Cooking Request','App\SalesMergeCookingRequest'=>'Sales Merge Cooking Request'], null , ['class' => 'form-control','placeholder'=>'Select Type','id'=>'reference_type']) !!}--}}
        </div>
        <div class="col-md-2">
            {!! Form::select('semi_type',[

            1=>'Burden Request',
            2=>'Cooking Request'] , null , ['class' => 'form-control','placeholder'=>'Select Type','id'=>'semi_type']) !!}



        </div>
<div class="col-md-3">
    <input type="text" name="date" id="date_range" class="form-control" placeholder="Select Date" required>
</div>

<div class="col-md-2">
    <button class="btn btn-info " onclick="process_form(this)">Filter</button>
</div>

<!-- loading view -->


</div>

<div class="table-responsive">
<table id="semi_table" class="display text-center ">
    <thead>
    <tr>
        <th width="5%">Opening Balance:</th>
        <th colspan="10" style="text-align: left">
            <div id="openning_balance"></div>
        </th>
    </tr>
    <tr>

        <th>Date & Time</th>
        <th>Semi Product</th>
        <th>Movement Type</th>
        <th>Start Volume</th>
        <th>Volume</th>
        <th>Amount</th>
        <th>Balance Volume</th>
        <th>Action</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th>Date & Time</th>
        <th>Semi Product</th>
        <th>Movement Type</th>
        <th>Start Volume</th>
        <th>Volume</th>
        <th>Amount</th>
        <th>Balance Volume</th>
        <th>Action</th>

    </tr>
    </tfoot>
</table>
</div>

@stop

@section('js')
<script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
<script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


<script>
$('#date_range').daterangepicker({
    "showDropdowns": true,
    "timePicker": true,
    "timePicker24Hour": true,
    ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    "locale": {
        "format": "YYYY-MM-DD",
    },
    "minDate": "11/01/2019",
    "maxDate": moment()
});


$('#date_range').val('')
var amount_sum;
var qty_sum;
var openning_balance;

$(document).ready(function () {
    table = $('#semi_table').DataTable({
        "bProcessing": true,
        "serverSide": true,
        searching: true,
        "ajax": {
            url: "{{url('/semi-inquiry/list/table/data')}}",
            type: "get",
            error: function () {
                $("#supplier_wise_materials_table_processing").css("display", "none");
            },
            dataSrc: function (data) {
                amount_sum = data.amount_sum;
                qty_sum = data.qty_sum;
                openning_balance = data.openning_balance;
                return data.data;
            }
        },
        pageLength: 25,
        responsive: true,
        drawCallback: function (settings) {
            var api = this.api();
            $(api.column(4).footer()).html(
               'Volume : ' + qty_sum

            );
            $(api.column(5).footer()).html(
               'Amount : ' + amount_sum

            );

            $("#openning_balance").empty();
            $('#openning_balance').append(openning_balance);
        }
    });
});

function process_form(e) {
    let semi_product = $("#semi_product").val();
    let table = $('#semi_table').DataTable();
    let reference_type = $("#meta").val();
    let date_range = $("#date_range").val();
    let semi_type = $("#semi_type").val();

    table.ajax.url('/semi-inquiry/list/table/data?semi_product=' + semi_product + '&meta=' + reference_type + '&date_range=' + date_range + '&semi_type=' + semi_type + '&filter=' + true).load();
}


function process_form_reset() {
    $("#semi_product").val('');

    let table = $('#semi_table').DataTable();
    table.ajax.url('semi-inquiry/list/table/data').load();
}

</script>

@stop
