@extends('layouts.back.master')@section('title','End Product Loading')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .hide {
            display: none;
        }

    </style>
@stop
@section('content')
    <div class="app-wrapper">

        <div class="app-main">

            <div class="app-main-header">
                <h5 class="app-main-heading text-center">Return Crates</h5>
            </div>


            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">
                    <div class="container">
                     <div class="projects-list">
                            @if (!empty($loadings))
                                @foreach($loadings as $crate_return)
                                <div class="container">
                                   <!--  <a href="{{route('crates-request.cratesRequestShow',[$crate_return->id])}}"> -->
                                        <div class="media">
                                            <div class="avatar avatar text-white avatar-md project-icon bg-primary"
                                                 data-target="#project-1">
                                                <i class="fa fa-pause" aria-hidden="true"></i>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="project-name"
                                                    id="project-1">{{\Carbon\Carbon::parse($crate_return->created_at)->format('y-m-d')}}</h6>
                                                @php 
                                                $so_id= $crate_return->loading['so_id'];
                                                $sales_order = SalesOrderManager\Models\SalesOrder::find($so_id);
                                                $template = $sales_order['template_id'];
                                                $templateData = App\AgentBuffer::find($template);
                                                $category = '';
                                                $category = EndProductManager\Models\Category::find($templateData['category_id']);
                                                @endphp
                                                    <table class="table table-borderless" style="padding-right:20px;">
                                                      <tbody>
                                                        <tr>
                                                          <td>{{$crate_return->loading['invoice_number']}}</td>
                                                          <td>{{$crate_return->loading['created_at']." | ".$templateData['day']." | ".$templateData['time']}}</td>
                                                        </tr>
                                                        <tr>
                                                          <td>{{$category['name']}}</td>
                                                          <td>{{$crate_return->agent->name_with_initials}}</td>
                                                        </tr>
                                                      </tbody>
                                                      </table>
                                                    </div>
                                            <!-- <small class="project-detail">{{$crate_return->loading['invoice_number']}}</small><br>
                                                <small class="project-detail">{{$category['name']}}</small><br>
                                                <small class="project-detail">{{$crate_return->loading['created_at']." | ".$templateData['day']." | ".$templateData['time']}}</small><br>
                                                <small class="project-detail">{{$crate_return->agent->name_with_initials}}</small><br>
                                            </div> -->
                                            @if(empty($crate_return->crates_approve))
                                                @can('pending_crates-notApprove')
                                          <a href="{{route('cratesShowPending.cratesShowRequest',[$crate_return->id])}}" class="btn btn-danger">Not Approve</a>
                                                @endcan
                                            @can('pending_sales_request-cratesShow')
                                            <a href="{{route('crates-request.cratesRequestShow',[$crate_return->id])}}" class="btn btn-outline-primary" style="width:6rem;margin-left:20px;">View</a>
                                                    @endcan
                                                    @else
                                                <button class="btn btn-primary">Approve</button>
                                          @ENDIF
                                        </div>
                                        <hr class="m-0">
                                    <!-- </a> -->
                                            </div>
                                @endforeach
                            @endif

                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>

@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $('.delete').click(function (e) {
            e.preventDefault();
            id = $(this).attr("value");
            confirmAlert(id);

        });

        function confirmAlert(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "delete",
                        url: id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            } else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

                @if(session('pdf'))
        var id = '{!! session('pdf.file') !!}'
        $.ajax({
            type: "POST",
            url: '/loading/' + id + '/pdf',
            success: function (response) {
                window.open(response);
                console.log(response);
                debugger
            }
        });
        @endif

    </script>
@stop
