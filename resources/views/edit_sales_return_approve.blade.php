@extends('layouts.back.master')@section('title','Sales Return')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}">


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .app-wrapper {
            font-family: Raleway, sans-serif;
            width: 100%;
            border: 1px solid #eee;
        }
    </style>
@stop
@section('content')
    <div class="app-wrapper">
        <div class="app-main">
            {!! Form::open(['route' => ['admin.sales_return_accepted'],'method'=>'post']) !!}
            <input type="hidden" name="sales_return_header" value="{{$invoice->id}}">
            <div class="app-main-header">
                <div class="row">
                    <div class="col-md-6">
                        <h5 class=text-left">SALES RETURN - {{ $invoice->time}} - {{$invoice->debit_note_number}}</h5>
                        <!-- @if ($invoice->status==1)
                            <button class="btn btn-outline-danger btn-sm mr-1">pending</button>
                        @elseif ($invoice->status==2)
                            <button class="btn btn-outline-warning btn-sm mr-1">received</button>
                        @elseif ($invoice->status==3)
                            <button class="btn btn-outline-success btn-sm mr-1">billed</button>
                        @endif -->
                    </div>
                    <div class="col-md-6">
                        <h5 class="text-right">{{$invoice->agent->first_name.' '.$invoice->agent->last_name}}</h5>
                        <h6 id="grand_total" class="text-right">Total : {{number_format($invoice->total,'2')}} LKR</h6>
                        <input type="hidden" name="grand_total" id="hidden_grand_total" value="{{$invoice->total}}">
                    </div>
                </div>
            </div>
            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">

                    <div class="projects-list">
                        @foreach($invoice->items as $product)
                            @if ($product->qty!=0)
                            <input type="hidden" name="id[]" value="{{$product->id}}">
                            <input type="hidden" name="reusable_type_end_product[]" value="{{$product->endProduct->reusable_type}}">
                            <input type="hidden" id="endProductWeight-{{$product->id}}" value="{{$product->endProduct->weight}}">
                            <div class="card-body col-md-12  align-items-center p-0">
                                <div class="row">


                                    <div class="col-md-2">
                                        <div class="text-left">
                                            <h5 class="mt-3 mx-3 text-nowrap">{{$product->endProduct->name}} </h5>
                                            <h5 class="mt-3 mx-3 text-nowrap">{{$product->qty}} units</h5>
                                            <input type="hidden" value="{{$product->endProduct->id}}" name="end_product_ids[]"></div>
                                    </div>


                                    <!-- <div class="col-md-2">
                                        <h6 class="text-nowrap text-right">Reuse Bun & Cake</h6>
                                        <div class="text-primary  mt-3 mx-3">

                                            <input type="text" class="form-control mt-3 mx-3"
                                                   name="reusable_bun_and_cake[]"
                                                   style="width: 100%" value="0" readonly>
                                        <center><small><u>weight (g) </u></small></center>
                                            <input type="text" class="form-control mt-3 mx-3"
                                                   name="reusable_bun_and_cake_weight[]"
                                                   style="width: 100%;" value="0" readonly>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <h6 class="text-nowrap text-right">Reuse bread</h6>
                                        <div class="text-primary  mt-3 mx-3">
                                            <input type="text" class="form-control mt-3 mx-3"
                                                   name="reusable_bread[]"
                                                   style="width: 100%" value="0" readonly>
                                        <center><small><u>weight (g) </u></small></center>
                                            <input type="text" class="form-control mt-3 mx-3"
                                                   name="reusable_bread_weight[]"
                                                   style="width: 100%" value="0" readonly>
                                        </div>
                                    </div> -->
                                    <div class="col-md-2">
                                        <h6 class="text-nowrap text-right">Reuse qty</h6>
                                        <div class="text-primary  mt-3 mx-3">
                                            <input type="number" step="0.001" class="form-control mt-3 mx-3"
                                                   name="reusable_qty[]" id="reusable_qty-{{$product->id}}" style="width: 100%" value="0" required
                                                  {{-- @if($product->endProduct->reusable_type == 1)
                                                    readonly
                                                  @endif --}}
                                                   onkeyup="changeLostQty('reusable_qty',{{$product->id}})" >
                                        <center><small><u>weight (g) </u></small></center>
                                            <input type="number" step="0.001" class="form-control mt-3 mx-3"
                                                   name="reusable_weight[]" id="reusable_weight-{{$product->id}}"
                                                   style="width: 100%" value="0" readonly required>
                                                 {{--  @if($product->endProduct->reusable_type == 1)
                                                    readonly
                                                  @endif>--}}
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <h6 class="text-nowrap text-right">Destroy qty</h6>
                                        <div class="text-primary  mt-3 mx-3">
                                            <input type="number" step="0.001" class="form-control mt-3 mx-3"
                                                   name="destroy_qty[]" id="destroy_qty-{{$product->id}}"
                                                   style="width: 100%" value="0" onkeyup="changeLostQty('destroy_qty',{{$product->id}})" required>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <h6 class="text-nowrap text-right">SR qty</h6>
                                        <div class="text-primary  mt-3 mx-3">
                                            <input type="number" step="0.001" class="form-control mt-3 mx-3" required
                                                   name="stock_rotation_qty[]" id="stock_rotation_qty-{{$product->id}}"
                                                   style="width: 100%" value="0" onkeyup="changeLostQty('stock_rotation_qty',{{$product->id}})">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <h6 class="text-nowrap text-right">Factory Fault</h6>
                                        <div class="text-primary  mt-3 mx-3">
                                            <input type="number" step="0.001" class="form-control mt-3 mx-3" required
                                                   name="factory_fault_qty[]" id="factory_fault_qty-{{$product->id}}"
                                                   style="width: 100%" value="0" onkeyup="changeLostQty('factory_fault_qty',{{$product->id}})">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <h6 class="text-nowrap text-right">Lost Qty</h6>
                                        <div class="text-primary  mt-3 mx-3">
                                            <input type="text" class="form-control mt-3 mx-3"
                                                   name="lost_qty[]" id="lost_qty-{{$product->id}}"
                                                   style="width: 100%" value="{{$product->qty}}" readonly>
                                          <input type="hidden" id="all_qty-{{$product->id}}" value="{{$product->qty}}" readonly>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <hr class="m-2">
                            @endif
                        @endforeach

                        <div class="card-body col-md-12  align-items-center p-0 mt-3">
                            <div class="row">
                                  <div class="coll-md-8" style="margin-right:80px;margin-left:20px;">
                                    <h5 class="text-nowrap text-right">Collected By :</h5>
                                  </div>
                                  <div class="coll-md-4">
                                    {!! Form::select('employee', $employees , null , ['class' => 'form-control','placeholder'=>'Select Employee','required','name'=>'collectBy']) !!}
                                  </div>
                            </div>
                        </div>
                        <hr class="m-3">
                        <button type="submit" class="btn btn-primary btn-lg btn-block mt-3 mb-5">Accept</button>
                    </div>
                </div>

            </div>
            {!!Form::close()!!}
        </div>
    </div>

@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        // function changeDeleteReturn(id,value){
        //     var deletefieldID = 'destroy_qty'+id;
        //     var x = document.getElementById("deletefieldID").value;
        //     console.log(deletefieldID);
        // }


        function changeTotal(val) {
            let id_name = val.id;
            let id = id_name.substr(4);

            let qty = val.value;
            let unit_value = document.getElementById('unit_value-' + id).textContent;
            let row_total = parseFloat(unit_value) * parseFloat(qty);

            let total_id = 'total-' + id;
            $("#" + total_id + "").html("");
            $("#" + total_id + "").append(row_total.toFixed(2));
            $("#" + total_id + "").append(" LKR");

            let total_value = 0;
            $(".total").each(function () {
                total_value = total_value + parseFloat((this.textContent).replace(',', ''))
            });

            $("#grand_total").html("");
            $("#grand_total").append(total_value.toFixed(2));
            $("#grand_total").append(" LKR");

            $("#hidden_grand_total").val(total_value);
        }

        function changeLostQty(type,value)
        {

          if(type == 'reusable_qty')
          {
            if(document.getElementById('reusable_qty-' + value).value != '')
            {
              let endProductWeight = parseFloat(document.getElementById('endProductWeight-' + value).value);
              let reUseableQty = parseFloat(document.getElementById('reusable_qty-' + value).value);

              document.getElementById('reusable_weight-' + value).value = endProductWeight*reUseableQty;
            }else{
              document.getElementById('reusable_weight-' + value).value = 0;
            }
          }

          let lostqty = document.getElementById('lost_qty-' + value).value;
          let reUseQty = document.getElementById('reusable_qty-' + value).value;
          let destroyQty = document.getElementById('destroy_qty-' + value).value;
          let stockRotationQty = document.getElementById('stock_rotation_qty-' + value).value;
          let factoryFaultQty = document.getElementById('factory_fault_qty-' + value).value;
          let allQty = document.getElementById('all_qty-' + value).value;

            if((reUseQty != '' && destroyQty != '' && stockRotationQty != '' && factoryFaultQty != ''))
            {
              let nowValue = parseFloat(reUseQty)+parseFloat(destroyQty)+parseFloat(stockRotationQty)+parseFloat(factoryFaultQty);
              // if(nowValue > parseFloat(allQty))
              // {
              //   // document.getElementById('reusable_qty-' + value).value = 0;
              //   // document.getElementById('destroy_qty-' + value).value = 0;
              //   // document.getElementById('stock_rotation_qty-' + value).value = 0;
              //   // document.getElementById('factory_fault_qty-' + value).value = 0;
              //   // document.getElementById('lost_qty-' + value).value = parseFloat(allQty);
              // }else{
                document.getElementById('lost_qty-' + value).value = parseFloat(allQty)-nowValue;
              // }
            }
        }
    </script>
@stop
