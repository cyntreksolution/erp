<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>

    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">

    <link rel="stylesheet" href="{{asset('assets/examples/css/theme-customizer.css')}}">
    <script src="{{asset('assets/examples/js/theme-customizer.js')}}"></script><!-- core plugins -->
    <link rel="stylesheet" href="{{asset('assets/vendor/css/hamburgers.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.cs')}}s">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.css')}}">
    <!-- site-wide styles -->

    <link rel="stylesheet" href="{{asset('assets/vendor/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/site.css')}}"><!-- plugins for the current page -->
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/chartist/dist/chartist.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/jvectormap/jquery-jvectormap.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/owl-carousel/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/toastr/toastr.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/material-design-iconic-font/css/material-design-iconic-font.min.css')}}">


    <script src="{{asset('assets/vendor/bower_components/breakpoints.js/dist/breakpoints.min.js')}}"></script>
    @yield('css')
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">--}}
    <script>Breakpoints({
            xs: {min: 0, max: 575},
            sm: {min: 576, max: 767},
            md: {min: 768, max: 991},
            lg: {min: 992, max: 1199},
            xl: {min: 1200, max: Infinity}
        });</script>
</head>

<body class="menubar-left menubar-dark dashboard dashboard-v1" style="font-family: sans-serif">
<!--[if lt IE 10]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a>
    to improve your experience.</p>
<![endif]-->

<nav class="site-navbar navbar fixed-top navbar-expand-md navbar-light bg-blue text-right">

<div class="col-md-10">

</div>
    <div class="collapse navbar-collapse" id="site-navbar-collapse">


        <ul class="navbar-nav">

            <li id="navbar-search-toggler" class="nav-item hidden-xl-up hidden-sm-down"><a class="nav-link" href="#"
                                                                                           data-toggle="navbar-search"><span
                            class="zmdi zmdi-hc-lg zmdi-search"></span></a></li>
            <li class="nav-item dropdown">
                <a class="nav-link site-user dropdown-toggle" href="#" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    <img class="nav-img"
                         src="{{asset('assets/global/images/user-img.png')}}"
                         alt=""> <span
                            class="nav-text hidden-sm-down ml-2">@if(isset($user)){{$user->first_name.' '.$user->last_name}}@endif</span>
                    <i class="nav-caret hidden-sm-down zmdi zmdi-hc-sm zmdi-chevron-down"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right p-0" data-plugin="dropdownCaret"><a
                            class="dropdown-item dropdown-menu-cap"></a>
                    <a class="dropdown-item" href="{{route('user.profile')}}"><i class="fa fa-user-o mr-3"></i>
                        <span>My Profile</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    {{--<a class="dropdown-item" href="#">--}}
                    {{--<i class="fa fa-file-o mr-3"></i>--}}
                    {{--<span>Lock</span>--}}
                    {{--</a>--}}
                    <a class="dropdown-item" href="{{route('user.logout')}}">
                        <i class="fa fa-power-off mr-3"></i>
                        <span>Logout</span>
                    </a>
                </div>
            </li>

        </ul>
        <!-- /.navbar-nav -->
    </div>

</nav>
<!-- /.site-navbar -->


    <!-- /.site-menubar -->

    {{--<main class="site-main">--}}
        {{--<header class="site-header">--}}
            {{--<div class="breadcrumb">--}}
                {{--<ol class="breadcrumb-tree">--}}
                    {{--<li class="breadcrumb-item">--}}
                        {{--<a href="{{route('home')}}"><span class="zmdi zmdi-home mr-1"></span> <span>Home</span></a>--}}
                    {{--</li>--}}
                    {{--<li class="breadcrumb-item " aria-current="page">--}}
                        {{--<a href=" @yield('current_url') ">  @yield('current') </a>--}}
                    {{--</li>--}}
                    {{--@if(View::hasSection('current_url2'))--}}
                        {{--<li class="breadcrumb-item active" aria-current="page">--}}
                            {{--<a href=" @yield('current_url2') ">  @yield('current2') </a>--}}
                        {{--</li>--}}
                    {{--@endif--}}
                {{--</ol>--}}
            {{--</div>--}}
        {{--</header>--}}
        {{--<div class="site-content">--}}
            {{--@yield('content')--}}
        {{--</div>--}}

    {{--</main>--}}
<div class="site-content">
    @yield('content')
</div>
    <!-- /.site-main -->
<footer class="site-footer text-center">
    <div class="mr-auto">
        <p class="text-primary mb-0">Powered By <i class="fa fa-heart text-danger"></i>
            <a href="http://www.cyntrek.com">Cyntrek</a>
        </p>
    </div>
</footer>

<script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/popper.js/dist/umd/popper.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/waypoints/lib/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/waypoints/lib/shortcuts/sticky.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/counterup/jquery.counterup.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/toastr/toastr.js')}}"></script>
<script src="{{asset('assets/vendor/js/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('assets/global/js/plugins/dropdown-caret.j')}}s"></script>
<script src="{{asset('assets/global/js/plugins/firstlitter.js')}}"></script>
<script src="{{asset('assets/global/js/plugins/video-modal.js')}}"></script>
<!-- plugins for the current page -->
<script src="{{asset('assets/vendor/bower_components/chartist/dist/chartist.min.j')}}s"></script>
<script src="{{asset('assets/vendor/jvectormap/jquery-jvectormap.min.js')}}"></script>
<script src="{{asset('assets/vendor/jvectormap/maps/jquery-jvectormap-world-mill.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/peity/jquery.peity.min.js')}}"></script>
<script src="{{asset('assets/vendor/owl-carousel/owl.carousel.min.js')}}"></script>


<!-- site-wide scripts -->
<script src="{{asset('assets/global/js/main.js')}}"></script>
<script src="{{asset('assets/js/site.')}}js"></script>
<script src="{{asset('assets/js/navbar.j')}}s"></script>
<script src="{{asset('assets/js/menubar.js')}}"></script>

<script>
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }

    @if(session('success'))
        Command: toastr["success"]("{{session('success.message')}} ", "{{session('success.title')}}")
    @elseif(session('error'))
        Command: toastr["error"]("{{session('error.message')}} ", "{{session('error.title')}}")
    @elseif(session('warning'))
        Command: toastr["warning"]("{{session('warning.message')}} ", "{{session('warning.title')}}")
    @elseif(session('info'))
        Command: toastr["info"]("{{session('info.message')}} ", "{{session('info.title')}}")
    @endif
</script>
@yield('js')
<!-- current page scripts -->
</body>

</html>