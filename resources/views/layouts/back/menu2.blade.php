<li>
    <a href="{{route('esr.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">End Product Stock Report</span>
    </a>
</li>

<li>
    <a href="{{route('driverCodeGenerate.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Driver Code</span>
    </a>
</li>
<li>
    <a href="{{route('admin.return')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Pending returns</span>
    </a>
</li>

<li class="menu-section-heading">END PRODUCT TESTING ONLY</li>
<li>
    <a href="{{route('bulk_burden.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">End Product Bulk Burden Make</span>
    </a>
</li>
<li>
    <a href="{{route('bulk_burden_bake.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">End Product Bulk Burden Burn</span>
    </a>
</li>
<li>
    <a href="{{route('burden_grade.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">End Product Burden Grade</span>
    </a>
</li>
<li>
    <a href="{{route('bulk_burden_loading.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Bulk Burden To Loading</span>
    </a>
</li>
<li class="menu-section-heading">SEMI FINISH BULK</li>
<li>
    <a href="{{route('semi_bulk_burden.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Bulk Burden Make</span>
    </a>
</li>
<li>
    <a href="{{route('semi_bulk_burden_bake.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Bulk Burden Burn</span>
    </a>
</li>

<li class="menu-section-heading">SHORT EATS BULK</li>
<li>
    <a href="{{route('short_bulk_burden.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Cooking Request Bulk Burden Make</span>
    </a>
</li>
<li>
    <a href="{{route('short_bulk_burden_bake.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Cooking Request Bulk Burden Burn</span>
    </a>
</li>


<li class="menu-section-heading">Stage 01</li>
<li>
    <a href="#">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Agents Payments</span>
    </a>
</li>
<li>
    <a href="#">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Day Transactions</span>
    </a>
</li>

<li>
    <a href="{{route('crates-inquire.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Crates Inquiry</span>
    </a>
</li>
<li>
    <a href="{{route('agent-inquire.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Agent Inquires</span>
    </a>
</li>
<li class="menu-section-heading">Stage xx</li>
<li>
    <a href="{{route('buffer-stock.list-view')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Pending Request Update</span>
    </a>
</li>

<li>
    <a href="{{route('shop-item-issue.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Issue Shop Items</span>
    </a>
</li>

<li>
    <a href="{{route('loading.stable')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Loading</span>
    </a>
</li>
<li>
    <a href="{{route('order.list')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Order List</span>
    </a>
</li>

<li>
    <a href="{{route('agent-payment.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Agents' Payments</span>
    </a>
</li>

<li>
    <a href="{{route('admin.settle_notes')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Settle Debit Note</span>
    </a>
</li>
