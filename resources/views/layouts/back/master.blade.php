<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">

    <link rel="stylesheet" href="{{asset('assets/examples/css/theme-customizer.css')}}">
    <script src="{{asset('assets/examples/js/theme-customizer.js')}}"></script><!-- core plugins -->
    <link rel="stylesheet" href="{{asset('assets/vendor/css/hamburgers.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.cs')}}s">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.css')}}">
    <!-- site-wide styles -->

    <link rel="stylesheet" href="{{asset('assets/vendor/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/site.css')}}"><!-- plugins for the current page -->
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/chartist/dist/chartist.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/jvectormap/jquery-jvectormap.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/owl-carousel/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/select2/css/select2.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/toastr/toastr.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/material-design-iconic-font/css/material-design-iconic-font.min.css')}}">

    <style>
        .select2-container {
            width: 100% !important;
        }
    </style>

    <script src="{{asset('assets/vendor/bower_components/breakpoints.js/dist/breakpoints.min.js')}}"></script>
    @yield('css')
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">--}}
    <script>Breakpoints({
            xs: {min: 0, max: 575},
            sm: {min: 576, max: 767},
            md: {min: 768, max: 991},
            lg: {min: 992, max: 1199},
            xl: {min: 1200, max: Infinity}
        });</script>
</head>

<body class="menubar-left menubar-dark dashboard dashboard-v1" style="font-family: sans-serif">
<!--[if lt IE 10]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a>
to improve your experience.</p>
<![endif]-->

<nav class="site-navbar navbar fixed-top navbar-expand-md navbar-light bg-blue">
    <div class="navbar-header">
        <a class="navbar-brand" href="/">
            <svg class="flip-y" xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 32 32">
                <path fill="currentColor"
                      d="M30.063 2.585c-.253-1.023-.758-1.754-1.5-2.17-3.28-1.842-9.02 3.577-11.05 6.88-.65 1.06-1.275 2.358-1.792 3.718-1.486-.21-2.95-.098-4.366.337C6.954 12.694 4 16.975 4 22v2c0 4.337 3.663 8 8 8h1.98c5.31 0 9.803-3.664 10.682-8.714.33-1.89.142-3.807-.54-5.585 1.26-1.2 2.43-2.587 3.268-3.886 1.646-2.554 3.46-8.062 2.673-11.23zM12 23c-1.105 0-2-.895-2-2s.895-2 2-2 2 .895 2 2-.895 2-2 2z"/>
                <path data-color="color-2" fill="#52c03b"
                      d="M10.77 9.437c1.14-.35 2.32-.527 3.506-.527h.148c.424-.954.888-1.846 1.37-2.633-1.106-2.466-2.56-4.72-4.01-5.71-.7-.477-1.387-.656-2.04-.528-.442.086-1.08.37-1.594 1.23C7 3.19 6.89 7.465 7.457 11.06c1-.7 2.108-1.255 3.312-1.623z"/>
            </svg>
            <span class="brand-name hidden-fold">BUNTALK</span>
        </a>
        <a href="javascript:void(0)" class="hidden-fold hidden-sm-down"
           data-toggle="dropdown" aria-haspopup="true"
           aria-expanded="false">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                <g transform="translate(.5 .5)" fill="none" stroke-linecap="square" stroke-miterlimit="10">
                    <path stroke="currentColor"
                          d="M23 12c0-1.105-.895-2-2-2h-1.262c-.19-.732-.477-1.422-.852-2.058l.892-.892c.78-.78.78-2.047 0-2.828-.78-.78-2.047-.78-2.828 0l-.892.892c-.636-.375-1.326-.663-2.058-.852V3c0-1.104-.895-2-2-2s-2 .895-2 2v1.262c-.732.19-1.422.477-2.058.852l-.892-.892c-.78-.78-2.047-.78-2.828 0-.78.78-.78 2.047 0 2.828l.892.892C4.74 8.578 4.45 9.268 4.262 10H3c-1.104 0-2 .895-2 2s.895 2 2 2h1.262c.19.732.477 1.422.852 2.058l-.892.892c-.78.78-.78 2.047 0 2.828.78.78 2.047.78 2.828 0l.892-.892c.635.375 1.326.663 2.058.852V21c0 1.104.895 2 2 2s2-.895 2-2v-1.262c.732-.19 1.422-.477 2.058-.852l.892.892c.78.78 2.047.78 2.828 0 .78-.78.78-2.047 0-2.828l-.892-.892c.375-.635.663-1.326.852-2.058H21c1.104 0 2-.895 2-2z"/>
                    <circle data-color="color-2" stroke="#52c03b" cx="12" cy="12" r="3"/>
                </g>
            </svg>
        </a>
        <button data-toggle="menubar" class="mr-auto hidden-md-up hamburger hamburger--collapse js-hamburger"
                type="button">
            <span class="hamburger-box">
                <span class="hamburger-inner"></span>
            </span>
        </button>
        <button type="button" class="navbar-toggler hidden-md-up collapsed" data-toggle="navbar-search">
            <span class="sr-only">Toggle navigation</span> <span class="zmdi zmdi-hc-lg zmdi-search"></span>
        </button>
        <button type="button" class="navbar-toggler hidden-md-up collapsed" data-toggle="collapse"
                data-target="#site-navbar-collapse" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="zmdi zmdi-hc-lg zmdi-more"></span>
        </button>
    </div>
    <!-- /.navbar-header -->
    <div class="collapse navbar-collapse" id="site-navbar-collapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item hidden-sm-down px-3 d-flex align-items-center">
                <button data-toggle="menubar-fold" class="hamburger hamburger--arrowalt is-active js-hamburger"
                        type="button">
                    <span class="hamburger-box"><span class="hamburger-inner"></span></span></button>
            </li>


            {{--<li class="nav-item dropdown">--}}
            {{--<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"--}}
            {{--aria-haspopup="true" aria-expanded="false">--}}
            {{--<i class="nav-icon fa fa-bell-o"></i>--}}
            {{--<span class="badge badge-circle badge-danger">4</span>--}}
            {{--</a>--}}
            {{--<div class="media-list dropdown-menu p-0" data-plugin="dropdownCaret">--}}
            {{--<div class="dropdown-item dropdown-menu-cap d-flex">--}}
            {{--<span class="mr-auto my-1">You Have 4 Unread Notifications</span>--}}
            {{--<a href="#" class="btn btn-sm btn-light my-1">See all</a>--}}
            {{--</div>--}}
            {{--<div class="scroll-container"><a href="javascript:void(0)" class="media dropdown-item">--}}
            {{--<span class="avatar bg-success" data-plugin="firstLitter" data-target="#message-1"></span>--}}
            {{--<div class="media-body"><h6 class="media-heading" id="message-1">Mohamed Ali</h6>--}}
            {{--<small>Lorem ipsum dolor sit amet, Lorem ipsum dolor.</small>--}}
            {{--</div>--}}
            {{--</a>--}}
            {{--<a href="javascript:void(0)" class="media dropdown-item">--}}
            {{--<span class="avatar bg-warning" data-plugin="firstLitter" data-target="#message-2"></span>--}}
            {{--<div class="media-body"><h6 class="media-heading" id="message-2">Sophia Smith</h6>--}}
            {{--<small>Lorem ipsum dolor sit amet, Lorem ipsum dolor.</small>--}}
            {{--</div>--}}
            {{--</a>--}}
            {{--<a href="javascript:void(0)" class="media dropdown-item">--}}
            {{--<span class="avatar bg-primary" data-plugin="firstLitter" data-target="#message-3"></span>--}}
            {{--<div class="media-body">--}}
            {{--<h6 class="media-heading" id="message-3">Sarah Adams</h6>--}}
            {{--<small>Lorem ipsum dolor sit amet, Lorem ipsum dolor.</small>--}}
            {{--</div>--}}
            {{--</a>--}}
            {{--<a href="javascript:void(0)" class="media dropdown-item">--}}
            {{--<span class="avatar bg-danger" data-plugin="firstLitter" data-target="#message-4"></span>--}}
            {{--<div class="media-body">--}}
            {{--<h6 class="media-heading" id="message-4">John Doe</h6>--}}
            {{--<small>Lorem ipsum dolor sit amet, Lorem ipsum dolor.</small>--}}
            {{--</div>--}}
            {{--</a>--}}
            {{--</div><!-- /.scroll-container -->--}}
            {{--</div><!-- /.media-list -->--}}
            {{--</li>--}}
            {{----}}
            {{--<li class="nav-item dropdown">--}}
            {{--<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true"--}}
            {{--aria-expanded="false">--}}
            {{--<i class="nav-icon fa fa-envelope-o"></i>--}}
            {{--<span class="badge badge-circle badge-success">3</span>--}}
            {{--</a>--}}

            {{--<div class="media-list dropdown-menu p-0" data-plugin="dropdownCaret">--}}
            {{--<div class="dropdown-item dropdown-menu-cap d-flex">--}}
            {{--<span class="mr-auto my-1">You Have 3 Unread Messages</span>--}}
            {{--<a href="#" class="btn btn-sm btn-light my-1">See all</a>--}}
            {{--</div>--}}
            {{--<div class="scroll-container">--}}
            {{--<a href="javascript:void(0)" class="media dropdown-item">--}}
            {{--<div class="avatar">--}}
            {{--<img src="{{asset('assets/global/images/203.jpg')}}" alt="">--}}
            {{--<span class="badge badge-success">5</span>--}}
            {{--</div>--}}
            {{--<div class="media-body">--}}
            {{--<h6 class="media-heading">Ahmed Gamal</h6>--}}
            {{--<small>Lorem ipsum dolor sit amet, Lorem ipsum dolor.</small>--}}
            {{--</div>--}}
            {{--</a>--}}
            {{--<a href="javascript:void(0)" class="media dropdown-item">--}}
            {{--<div class="avatar">--}}
            {{--<img src="{{asset('assets/global/images/101.jpg')}}" alt="">--}}
            {{--<span class="badge badge-success">9</span>--}}
            {{--</div>--}}
            {{--<div class="media-body">--}}
            {{--<h6 class="media-heading">Nick Pettit</h6>--}}
            {{--<small>Lorem ipsum dolor sit amet, Lorem ipsum dolor.</small>--}}
            {{--</div>--}}
            {{--</a>--}}
            {{--<a href="javascript:void(0)" class="media dropdown-item">--}}
            {{--<div class="avatar"><img src="{{asset('assets/global/images/202.jpg')}}" alt="">--}}
            {{--<span class="badge badge-success">1</span>--}}
            {{--</div>--}}
            {{--<div class="media-body"><h6 class="media-heading">Media heading</h6>--}}
            {{--<small>Lorem ipsum dolor sit amet, Lorem ipsum dolor.</small>--}}
            {{--</div>--}}
            {{--</a>--}}
            {{--</div>--}}
            {{--<!-- /.scroll-container -->--}}
            {{--</div>--}}
            {{--<!-- /.media-list -->--}}
            {{--</li>--}}
        </ul>

        <ul class="navbar-nav">
            <li class="nav-item">
                <div id="navbar-search" class="navbar-search">
                    <form class="form-inline navbar-search-form"><input class="form-control navbar-search-field"
                                                                        type="text" placeholder="Search">
                        <button type="submit" class="navbar-search-submit">
                            <svg class="svg-search-icon">
                                {{--<use xlink:href="http://spantags.com/luxury/assets/global/svg-sprite/sprite.svg#search"/>--}}
                            </svg>
                        </button>
                        <button class="navbar-search-close" data-toggle="navbar-search"><i class="zmdi zmdi-close"></i>
                        </button>
                    </form>
                    <div class="navbar-search-backdrop" data-toggle="navbar-search"></div>
                </div><!-- /.navbar-search --></li>
            <li id="navbar-search-toggler" class="nav-item hidden-xl-up hidden-sm-down"><a class="nav-link" href="#"
                                                                                           data-toggle="navbar-search"><span
                            class="zmdi zmdi-hc-lg zmdi-search"></span></a></li>
            <li class="nav-item dropdown">
                <a class="nav-link site-user dropdown-toggle" href="#" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    <img class="nav-img"
                         src="{{asset('assets/global/images/user-img.png')}}"
                         alt=""> <span
                            class="nav-text hidden-sm-down ml-2">@if(Auth::check()){{Auth::user()->first_name.' '.Auth::user()->last_name}}@endif</span>
                    <i class="nav-caret hidden-sm-down zmdi zmdi-hc-sm zmdi-chevron-down"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right p-0" data-plugin="dropdownCaret"><a
                            class="dropdown-item dropdown-menu-cap"></a>
                    <a class="dropdown-item" href="{{route('user.profile')}}"><i class="fa fa-user-o mr-3"></i>
                        <span>My Profile</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a target="_blank" class="dropdown-item" href="https://mail.buntalk.lk/"><i
                                class="fa fa-mail-reply mr-3"></i>
                        <span>MailBox</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    {{--<a class="dropdown-item" href="#">--}}
                    {{--<i class="fa fa-file-o mr-3"></i>--}}
                    {{--<span>Lock</span>--}}
                    {{--</a>--}}
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        <i class="fa fa-power-off mr-3"></i>
                        <span>Logout</span>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
{{--                    <a class="dropdown-item" href="{{route('user.logout')}}">--}}
{{--                        <i class="fa fa-power-off mr-3"></i>--}}
{{--                        <span>Logout</span>--}}
{{--                    </a>--}}
                </div>
            </li>

        </ul>
        <!-- /.navbar-nav -->
    </div>
    <!-- /.navbar-collapse -->
</nav>
<!-- /.site-navbar -->

<div class="site-wrapper">
    <aside class="site-menubar">
        <div class="site-menubar-inner">
            <ul class="site-menu">
{{--                @include('layouts.menu.store')--}}
{{--                @include('layouts.menu.production')--}}
{{--                @include('layouts.menu.packing')--}}
{{--                @include('layouts.menu.hr')--}}
{{--                @include('layouts.menu.sales')--}}
{{--                @include('layouts.menu.accounts')--}}
{{--                @include('layouts.menu.agent')--}}
                @if(Auth::user()->hasRole(['Owner','Super Admin']))
                    @include('layouts.back.menu')
                @elseif(Auth::user()->hasRole(['Sales Agent']))
                    @include('layouts.menu.agent')
                @elseif(Auth::user()->hasRole(['Production Manager']))
                    @include('layouts.menu.production')
                @elseif(Auth::user()->hasRole(['Stock Manager']))
                    @include('layouts.menu.store')
                @elseif(Auth::user()->hasRole(['Sales Manager']))
                    @include('layouts.menu.sales')
                @elseif(Auth::user()->hasRole(['Packing Manager']))
                    @include('layouts.menu.packing')
                @elseif(Auth::user()->hasRole(['Human Resource']))
                    @include('layouts.menu.hr')
                @elseif(Auth::user()->hasRole(['Accountant']))
                    @include('layouts.menu.accounts')
                @elseif(Auth::user()->hasRole(['Stores End Product']))
                    @include('layouts.menu.store_end')
                @elseif(Auth::user()->hasRole(['Assistant']))
                    @include('layouts.menu.assistant')
                @endif


{{--                @if($role = Sentinel::check()->roles[0]->slug=='stock-manager')--}}
{{--                    @include('layouts.menu.store')--}}
{{--                @endif--}}
{{--                @if($role = Sentinel::check()->roles[0]->slug=='production-manager')--}}
{{--                    @include('layouts.menu.production')--}}
{{--                @endif--}}
{{--                @if($role = Sentinel::check()->roles[0]->slug=='packing')--}}
{{--                    @include('layouts.menu.packing')--}}
{{--                @endif--}}
{{--                @if($role = Sentinel::check()->roles[0]->slug=='human-resource')--}}
{{--                    @include('layouts.menu.hr')--}}
{{--                @endif--}}
{{--                @if($role = Sentinel::check()->roles[0]->slug=='sales-manager')--}}
{{--                    @include('layouts.menu.sales')--}}
{{--                @endif--}}
{{--                @if($role = Sentinel::check()->roles[0]->slug=='accountant')--}}
{{--                    @include('layouts.menu.accounts')--}}
{{--                @endif--}}
{{--                @if($role = Sentinel::check()->roles[0]->slug=='sales-ref')--}}
{{--                    @include('layouts.menu.agent')--}}
{{--                @endif--}}
{{--                @if($role = Sentinel::check()->roles[0]->slug=='owner')--}}
{{--                    @include('layouts.back.menu')--}}
{{--                @endif--}}
{{--                @if($role = Sentinel::check()->roles[0]->slug=='superadministrator')--}}
{{--                    @include('layouts.back.menu')--}}
{{--                @endif--}}
{{--                @if($role = Sentinel::check()->roles[0]->slug=='stores-end-product')--}}
{{--                    @include('layouts.menu.store_end')--}}
{{--                @endif--}}
            </ul>

        </div>
        <!-- /.site-menubar-inner -->
    </aside>
    <!-- /.site-menubar -->
    <main class="site-main">
        <header class="site-header">
            <div class="breadcrumb">
                <ol class="breadcrumb-tree">
                    <li class="breadcrumb-item">
                        {{--                        <a href="{{route('home')}}"><span class="zmdi zmdi-home mr-1"></span> <span>Home</span></a>--}}
                    </li>
                    <li class="breadcrumb-item " aria-current="page">
                        <a href=" @yield('current_url') ">  @yield('current') </a>
                    </li>
                    @if(View::hasSection('current_url2'))
                        <li class="breadcrumb-item active" aria-current="page">
                            <a href=" @yield('current_url2') ">  @yield('current2') </a>
                        </li>
                    @endif
                </ol>
            </div>
        </header>
        <div class="site-content">
            @yield('content')
        </div>


        <footer class="site-footer text-center">
            <div class="mr-auto">
                <p class="text-primary mb-0">Powered By <i class="fa fa-heart text-danger"></i>
                    <a href="http://www.cyntrek.com">Cyntrek Solutions</a>
                </p>
            </div>
        </footer>
        <!-- /.site-footer -->
    </main>
    <!-- /.site-main -->
</div><!-- /.site-warpper -->
<!-- theme customizer -->
{{--<div id="theme-customizer">--}}
{{--<header class="p-4"><a href="https://themeforest.net/item/kiwi-responsive-web-app-kit/20562688?s_rank=1"--}}
{{--class="btn btn-block btn-lg btn-primary fz-base">BUY Luxury NOW!</a>--}}
{{--<hr>--}}
{{--<div class="d-flex justify-content-between customizer-action-btns">--}}
{{--<button id="customizerResetButton" class="btn btn-outline-danger">Reset</button>--}}
{{--<button id="customizerSaveButton" class="btn btn-outline-success">Save</button>--}}
{{--</div>--}}
{{--</header>--}}
{{--<div class="theme-customizer-body">--}}
{{--<ul class="nav nav-tabs" role="tablist">--}}
{{--<li class="nav-item"><a class="nav-link active" id="navbar-tab" data-toggle="tab" href="#navbar" role="tab"--}}
{{--aria-controls="navbar" aria-expanded="true">Navbar</a></li>--}}
{{--<li class="nav-item"><a class="nav-link" id="menubar-tab" data-toggle="tab" href="#menubar" role="tab"--}}
{{--aria-controls="menubar" aria-expanded="true">Menubar</a></li>--}}
{{--</ul>--}}
{{--<div class="tab-content" id="myTabContent">--}}
{{--<div class="tab-pane p-4 fade show active" id="navbar" role="tabpanel" aria-labelledby="navbar-tab">--}}
{{--<div><input id="navbarDark" data-toggle="navbarDark" type="checkbox" data-plugin="switchery"--}}
{{--data-size="small"><label for="navbarDark" class="ml-3">Navbar Dark</label></div>--}}
{{--<hr>--}}
{{--<h6 class="mb-4">Navbar Skin</h6>--}}
{{--<div class="mb-3">--}}
{{--<div class="radio radio-indigo"><input type="radio" id="nb-skin-2" name="navbar-skin-option"--}}
{{--data-toggle="navbarSkin" data-skin="bg-indigo"><label--}}
{{--for="nb-skin-2">Indigo</label></div>--}}
{{--</div>--}}
{{--<div class="mb-3">--}}
{{--<div class="radio radio-blue"><input type="radio" id="nb-skin-4" name="navbar-skin-option"--}}
{{--data-toggle="navbarSkin" data-skin="bg-blue"><label--}}
{{--for="nb-skin-4">Blue</label></div>--}}
{{--</div>--}}
{{--<div class="mb-3">--}}
{{--<div class="radio radio-cyan"><input type="radio" id="nb-skin-6" name="navbar-skin-option"--}}
{{--data-toggle="navbarSkin" data-skin="bg-cyan"><label--}}
{{--for="nb-skin-6">Cyan</label></div>--}}
{{--</div>--}}
{{--<div class="mb-3">--}}
{{--<div class="radio radio-orange"><input type="radio" id="nb-skin-5" name="navbar-skin-option"--}}
{{--data-toggle="navbarSkin" data-skin="bg-orange"><label--}}
{{--for="nb-skin-5">Orange</label></div>--}}
{{--</div>--}}
{{--<div class="mb-3">--}}
{{--<div class="radio radio-red"><input type="radio" id="nb-skin-7" name="navbar-skin-option"--}}
{{--data-toggle="navbarSkin" data-skin="bg-red"><label--}}
{{--for="nb-skin-7">Red</label></div>--}}
{{--</div>--}}
{{--<div class="mb-3">--}}
{{--<div class="radio radio-pink"><input type="radio" id="nb-skin-13" name="navbar-skin-option"--}}
{{--data-toggle="navbarSkin" data-skin="bg-pink"><label--}}
{{--for="nb-skin-13">Pink</label></div>--}}
{{--</div>--}}
{{--<div class="mb-3">--}}
{{--<div class="radio radio-purple"><input type="radio" id="nb-skin-12" name="navbar-skin-option"--}}
{{--data-toggle="navbarSkin" data-skin="bg-purple"><label--}}
{{--for="nb-skin-12">Purple</label></div>--}}
{{--</div>--}}
{{--<div class="mb-3">--}}
{{--<div class="radio radio-green"><input type="radio" id="nb-skin-8" name="navbar-skin-option"--}}
{{--data-toggle="navbarSkin" data-skin="bg-green"><label--}}
{{--for="nb-skin-8">Green</label></div>--}}
{{--</div>--}}
{{--<div class="mb-3">--}}
{{--<div class="radio radio-teal"><input type="radio" id="nb-skin-9" name="navbar-skin-option"--}}
{{--data-toggle="navbarSkin" data-skin="bg-teal"><label--}}
{{--for="nb-skin-9">Teal</label></div>--}}
{{--</div>--}}
{{--<div class="mb-3">--}}
{{--<div class="radio radio-brown"><input type="radio" id="nb-skin-10" name="navbar-skin-option"--}}
{{--data-toggle="navbarSkin" data-skin="bg-brown"><label--}}
{{--for="nb-skin-10">Brown</label></div>--}}
{{--</div>--}}
{{--<div>--}}
{{--<div class="radio radio-gray"><input type="radio" id="nb-skin-11" name="navbar-skin-option"--}}
{{--data-toggle="navbarSkin" data-skin="bg-gray"><label--}}
{{--for="nb-skin-11">Gray</label></div>--}}
{{--</div>--}}
{{--</div><!-- /.tab-pane -->--}}
{{--<div class="tab-pane fade p-4" id="menubar" role="tabpanel" aria-labelledby="menubar-tab">--}}
{{--<div><input id="menubarDark" data-toggle="menubarDark" type="checkbox" data-plugin="switchery"--}}
{{--data-size="small"><label for="menubarDark" class="ml-3">Menubar Dark</label></div>--}}
{{--<hr>--}}
{{--<div id="menubar-fold-wrap" class="hidden-top hidden-sm-down"><input id="menubarFold"--}}
{{--data-toggle="menubarFold"--}}
{{--type="checkbox"--}}
{{--data-plugin="switchery"--}}
{{--data-size="small"><label--}}
{{--for="menubarFold" class="ml-3">Menubar Folded</label></div>--}}
{{--</div>--}}
{{--<!-- /.tab-pane -->--}}
{{--</div>--}}
{{--<!-- /.tab-content -->--}}
{{--<div id="theme-customizer-toggler" data-toggle="class" data-target="#theme-customizer" data-class="show"><i--}}
{{--class="fa fa-gear fa-2x"></i></div>--}}
{{--</div>--}}
{{--<!-- /.theme-customizer-body -->--}}
{{--</div>--}}
<!-- /.theme-customizer -->
<!-- core plugins -->


<script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/popper.js/dist/umd/popper.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/waypoints/lib/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/waypoints/lib/shortcuts/sticky.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/counterup/jquery.counterup.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/toastr/toastr.js')}}"></script>
<script src="{{asset('assets/vendor/js/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('assets/global/js/plugins/dropdown-caret.j')}}s"></script>
<script src="{{asset('assets/global/js/plugins/firstlitter.js')}}"></script>
<script src="{{asset('assets/global/js/plugins/video-modal.js')}}"></script>
<!-- plugins for the current page -->
<script src="{{asset('assets/vendor/bower_components/chartist/dist/chartist.min.j')}}s"></script>
<script src="{{asset('assets/vendor/jvectormap/jquery-jvectormap.min.js')}}"></script>
<script src="{{asset('assets/vendor/jvectormap/maps/jquery-jvectormap-world-mill.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/peity/jquery.peity.min.js')}}"></script>
<script src="{{asset('assets/vendor/owl-carousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/vendor/select2/js/select2.js')}}"></script>


<!-- site-wide scripts -->
<script src="{{asset('assets/global/js/main.js')}}"></script>
<script src="{{asset('assets/js/site.')}}js"></script>
<script src="{{asset('assets/js/navbar.j')}}s"></script>
<script src="{{asset('assets/js/menubar.js')}}"></script>

<script>
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }

    @if(session('success'))
        Command: toastr["success"]("{{session('success.message')}} ", "{{session('success.title')}}")
    @elseif(session('error'))
        Command: toastr["error"]("{{session('error.message')}} ", "{{session('error.title')}}")
    @elseif(session('warning'))
        Command: toastr["warning"]("{{session('warning.message')}} ", "{{session('warning.title')}}")
    @elseif(session('info'))
        Command: toastr["info"]("{{session('info.message')}} ", "{{session('info.title')}}")
    @endif


            @if(!View::hasSection('deniedBack'))
    if (history.pushState != undefined) {
        history.pushState(null, null, location.href);
    }
    history.back();
    history.forward();
    window.onpopstate = function () {
        history.go(1);
    };

    @endif

</script>
@yield('js')
<!-- current page scripts -->
</body>

</html>