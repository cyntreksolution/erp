<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">

    <link rel="stylesheet" href="{{asset('assets/examples/css/theme-customizer.css')}}">
    <script src="{{asset('assets/examples/js/theme-customizer.js')}}"></script><!-- core plugins -->
    <link rel="stylesheet" href="{{asset('assets/vendor/css/hamburgers.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.cs')}}s">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.css')}}">
    <!-- site-wide styles -->

    <link rel="stylesheet" href="{{asset('assets/vendor/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/site.css')}}"><!-- plugins for the current page -->
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/chartist/dist/chartist.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/jvectormap/jquery-jvectormap.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/owl-carousel/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/select2/css/select2.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/toastr/toastr.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/material-design-iconic-font/css/material-design-iconic-font.min.css')}}">

    <style>
        .select2-container {
            width: 100% !important;
        }
    </style>

    <script src="{{asset('assets/vendor/bower_components/breakpoints.js/dist/breakpoints.min.js')}}"></script>
    @yield('css')
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">--}}
    <script>Breakpoints({
            xs: {min: 0, max: 575},
            sm: {min: 576, max: 767},
            md: {min: 768, max: 991},
            lg: {min: 992, max: 1199},
            xl: {min: 1200, max: Infinity}
        });</script>
</head>

<body class="menubar-left menubar-dark dashboard dashboard-v1" style="font-family: sans-serif">


<div class="container-fluid">

    @yield('content')


    <footer class="site-footer text-center">
        <div class="mr-auto">
            <p class="text-primary mb-0">Powered By <i class="fa fa-heart text-danger"></i>
                <a href="http://www.cyntrek.com">Cyntrek Solutions</a>
            </p>
        </div>
    </footer>

</div>


<script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/popper.js/dist/umd/popper.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/waypoints/lib/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/waypoints/lib/shortcuts/sticky.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/counterup/jquery.counterup.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/toastr/toastr.js')}}"></script>
<script src="{{asset('assets/vendor/js/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('assets/global/js/plugins/dropdown-caret.j')}}s"></script>
<script src="{{asset('assets/global/js/plugins/firstlitter.js')}}"></script>
<script src="{{asset('assets/global/js/plugins/video-modal.js')}}"></script>
<!-- plugins for the current page -->
<script src="{{asset('assets/vendor/bower_components/chartist/dist/chartist.min.j')}}s"></script>
<script src="{{asset('assets/vendor/jvectormap/jquery-jvectormap.min.js')}}"></script>
<script src="{{asset('assets/vendor/jvectormap/maps/jquery-jvectormap-world-mill.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/peity/jquery.peity.min.js')}}"></script>
<script src="{{asset('assets/vendor/owl-carousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/vendor/select2/js/select2.js')}}"></script>


<!-- site-wide scripts -->
<script src="{{asset('assets/global/js/main.js')}}"></script>
<script src="{{asset('assets/js/site.')}}js"></script>
<script src="{{asset('assets/js/navbar.j')}}s"></script>
<script src="{{asset('assets/js/menubar.js')}}"></script>

<script>
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }

    @if(session('success'))
        Command: toastr["success"]("{{session('success.message')}} ", "{{session('success.title')}}")
    @elseif(session('error'))
        Command: toastr["error"]("{{session('error.message')}} ", "{{session('error.title')}}")
    @elseif(session('warning'))
        Command: toastr["warning"]("{{session('warning.message')}} ", "{{session('warning.title')}}")
    @elseif(session('info'))
        Command: toastr["info"]("{{session('info.message')}} ", "{{session('info.title')}}")
    @endif


            @if(!View::hasSection('deniedBack'))
    if (history.pushState != undefined) {
        history.pushState(null, null, location.href);
    }
    history.back();
    history.forward();
    window.onpopstate = function () {
        history.go(1);
    };

    @endif

</script>
@yield('js')
<!-- current page scripts -->
</body>

</html>