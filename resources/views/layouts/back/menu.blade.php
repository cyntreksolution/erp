






<!--li class="menu-section-heading">STAGE 01</li-->
{{--@include('layouts.back.menu2')--}}
<!--li class="menu-section-heading">END STAGE 01</li-->









<!--li>check this below link

    <a href="{{route('bake.list')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Bulk Bake Burden</span>
    </a>
</li>
<li>
    <a href="{{route('sr.merged')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Merged sales requests</span>
    </a>
</li>
<li>
    <a href="{{route('sales_return.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Sales return</span>
    </a>
</li>
<li>
    <a href="{{route('loading.all')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">loaded</span>
    </a>
</li>
<li>
    <a href="{{route('packing-po.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">packing order </span>
    </a>
</li>
<li>
    <a href="{{route('end-product-merge-list.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">End Product Burden Merge</span>
    </a>
</li>

<li>
    <a href="{{route('eburden.mergedlist')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">End Product Merged Burden</span>
    </a>
</li-->






<div class="side-content text-center">
    <img class="mb-5" style="width: 175px" src="{{asset('assets/img/buntalk_home.png')}}" alt="">
    <div id="login-page-slider" data-plugin="owlCarousel">
        <!--div class="item">
            <h4 style="color:#501310;font-weight: 900; ">RANJANAS HOLDINGS (PVT) LTD </h4>
            <h5 style="color:#501310;font-weight: 900; ">   katukurunda,Habaraduwa,Sri Lanka<br>
                buntalksl@icloud.com<br>
                +94 77 330 4678</h5>
        </div-->
    </div>
</div>
{{--{{dd(\Illuminate\Support\Facades\Auth::user()->hasAnyPermission('k'))}}--}}
@if(Auth::user()->hasAnyPermission(['Production-list','SalesRequests-list','EndProductMerge-list','EndProductBurden-list','BulkBurdenMake-list',
                   'BulkBurdenBurn-list','ProductBurdenGrade-list','ProductAddStores-list','BulkBurdenMake-list','BulkBurdenBurn-list',
                    'BurdenGrade-list','NewBurden-list','SemiFinishProducts-list','RequestRawMaterials-list','NewFillingRequest-list','FillingBulkBurdenMake-list',
                    'FillingBulkBurdenBurn-list',]))
<li>
    <a href="javascript:void(0)" class="submenu-toggle bg-warning">
        <i class="menu-icon fa fa-users zmdi-hc-lg"></i>
        <span class="menu-text text-dark">PRODUCTION</span>
        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
    </a>
    <ul class="submenu" style="display: block;">
        <li class="menu-heading"><a href="javascript:void(0)">PRODUCTION</a></li>
        @can('Production-list')
        <li>
            <a href="{{route('production.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Production</span>
            </a>
        </li>
        @endcan
        @can('SalesRequests-list')
        <li>
            <a href="{{route('production.merged')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Sales Request Merge List</span>
            </a>
        </li>
        @endcan
        @can('EndProductMerge-list')
        <li>
            <a href="{{route('ep.list')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">End Product Merge List</span>
            </a>
        </li>
        @endcan


        <li class="menu-section-heading">END PRODUCT</li>
        @can('EndProductBurden-list')
        <li>
            <a href="{{route('eburden.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">New End Product Burdens</span>
            </a>

        </li>
        @endcan
        @can('BulkBurdenMake-list')
        <li>
            <a href="{{route('bulk_burden.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">End Product Bulk Burden Make</span>
            </a>
        </li>
        @endcan
        @can('BulkBurdenBurn-list')
        <li>
            <a href="{{route('bulk_burden_bake.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">End Product Bulk Burden Burn</span>
            </a>
        </li>
        @endcan
        @can('ProductBurdenGrade-list')
        <li>
            <a href="{{route('burden_grade.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">End Product Burden Grade</span>
            </a>
        </li>
        @endcan
        @can('ProductAddStores-list')
        <li>
            <a href="{{route('bulk_burden_loading.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">End Product Add Stores</span>
            </a>
        </li>
        @endcan
        <li class="menu-section-heading">SEMI FINISH BULK</li>
        @can('BulkBurdenMake-list')
        <li>
            <a href="{{route('semi_bulk_burden.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Bulk Burden Make</span>
            </a>
        </li>
        @endcan
        @can('BulkBurdenBurn-list')
        <li>
            <a href="{{route('semi_bulk_burden_bake.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Bulk Burden Burn</span>
            </a>
        </li>
        @endcan
        @can('BurdenGrade-list')
        <li>
            <a href="{{route('burden.create')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Burden Grade</span>
            </a>
        </li>
        @endcan
        @can('NewBurden-list')
        <li>
            <a href="{{route('proOrder.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">New Burden</span>
            </a>
        </li>
        @endcan
        @can('SemiFinishProducts-list')
        <li>
            <a href="{{route('semi-issue.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Issue Semi Finish Products </span>
            </a>
        </li>
        @endcan
        @can('RequestRawMaterials-list')
        <li>
            <a href="{{route('raw-request.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Request Raw Materials </span>
            </a>
        </li>
        @endcan
        <li class="menu-section-heading">FILLIN BULK</li>
        @can('NewFillingRequest-list')
        <li>
            <a href="{{route('shorteats.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">New Filling Request</span>
            </a>
        </li>
        @endcan
        @can('FillingBulkBurdenMake-list')
        <li>
            <a href="{{route('short_bulk_burden.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Filling Bulk Burden Make</span>
            </a>
        </li>
        @endcan
        @can('FillingBulkBurdenBurn-list')
        <li>
            <a href="{{route('short_bulk_burden_bake.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Filling Bulk Burden Burn</span>
            </a>
        </li>
        @endcan


    </ul>
</li>
@endif

@if(Auth::user()->hasAnyPermission(['ReusableWastage-list','EditPriceChange-list','IssueRawMaterialsEndProduct-list','IssueShopItems-list']))
<li>
    <a href="javascript:void(0)" class="submenu-toggle bg-warning">
        <i class="menu-icon fa fa-users zmdi-hc-lg"></i>
        <span class="menu-text text-dark">STORES</span>
        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
    </a>
    <ul class="submenu" style="display: block;">
        <li class="menu-heading"><a href="javascript:void(0)">STORES</a></li>
        @can('ReusableWastage-list')
        <li>
            <a href="{{route('reusable.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Reusable Wastage</span>
            </a>
        </li>
        @endcan
        @can('EditPriceChange-list')
        <li>
            <a href="{{route('po.approve-price')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Edit Price Change </span>
            </a>
        </li>
        @endcan
        @can('IssueRawMaterialsEndProduct-list')
        <li>
            <a href="{{route('issue.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Issue Raw Materials For End Product</span>
            </a>
        </li>
        @endcan
        @can('IssueShopItems-list')
        <li>
            <a href="{{route('shop-item-issue.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Issue Shop Items</span>
            </a>
        </li>
        @endcan

    </ul>
</li>
@endif
@if(Auth::user()->hasAnyPermission(['PendingCrates-list','ApproveAgentsReceivedOrder-list','PendingReturns-list','loading-list',
                'OrderList-list','RemoveStoresReturn-list']))

<li>
    <a href="javascript:void(0)" class="submenu-toggle bg-warning">
        <i class="menu-icon fa fa-users zmdi-hc-lg"></i>
        <span class="menu-text text-dark">SALES</span>
        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
    </a>
    <ul class="submenu" style="display: block;">
        <li class="menu-heading"><a href="javascript:void(0)">SALES</a></li>
        @can('PendingCrates-list')
        <li>
            <a href="{{route('crates-return.allcrates')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Pending Crates</span>
            </a>
        </li>
        @endcan
        @can('ApproveAgentsReceivedOrder-list')
        <li>
            <a href="{{route('approve-list.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Approve Agents Received Order</span>
            </a>
        </li>
        @endcan
        @can('PendingReturns-list')
        <li>
            <a href="{{route('admin.return')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Pending returns</span>
            </a>
        </li>
        @endcan
        @can('loading-list')
        <li>
            <a href="{{route('loading.stable')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Loading</span>
            </a>
        </li>
        @endcan
        @can('OrderList-list')
        <li>
            <a href="{{route('order.list')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Order List</span>
            </a>
        </li>
        @endcan
        @can('RemoveStoresReturn-list')
        <li>
            <a href="{{route('end.remove')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Remove Stores Return</span>
            </a>
        </li>
        @endcan

    </ul>
</li>
@endif
@if(Auth::user()->hasAnyPermission(['AgentPayment-list','SettleDebitNote-list','SettlePurchasing-list','CashLocker-list','passBook-list',
            'salary-list','allowance-list','DayPaySlip-list']))
<li>
    <a href="javascript:void(0)" class="submenu-toggle bg-warning">
        <i class="menu-icon fa fa-users zmdi-hc-lg"></i>
        <span class="menu-text text-dark">FINANCE</span>
        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
    </a>
    <ul class="submenu" style="display: block;">
        <li class="menu-heading"><a href="javascript:void(0)">FINANCE</a></li>
        @can('AgentPayment-list')
        <li>
            <a href="{{route('agent-payment.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Agents' Payments</span>
            </a>
        </li>
        @endcan
        @can('SettleDebitNote-list')
        <li>
            <a href="{{route('admin.settle_notes')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Settle Debit Note</span>
            </a>
        </li>
        @endcan


        @can('SettlePurchasing-list')
        <li>
            <a href="{{route('po.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Settle Purchasing</span>
            </a>
        </li>
        @endcan

        @can('CashLocker-list')
        <li>
            <a href="{{route('cash_locker.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Cash Locker</span>
            </a>
        </li>
        @endcan
        @can('passBook-list')
        <li>
            <a href="{{route('pass_book.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Passbook </span>
            </a>
        </li>
        @endcan
        @can('salary-list')
        <li>
            <a href="{{route('salary.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Salary & Wadges </span>
            </a>
        </li>
        @endcan
        @can('allowance-list')
        <li>
            <a href="{{route('salwages.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Allowances </span>
            </a>
        </li>
        @endcan


        @can('DayPaySlip-list')
        <li>
            <a href="{{route('a.g')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Day Payslips</span>
            </a>
        </li>
        @endcan


    </ul>
</li>
@endif
@if(Auth::user()->hasAnyPermission(['Commission-list']))
<li>
    <a href="javascript:void(0)" class="submenu-toggle bg-warning">
        <i class="menu-icon fa fa-users zmdi-hc-lg"></i>
        <span class="menu-text text-dark">HUMAN RESOURCE</span>
        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
    </a>
    <ul class="submenu" style="display: block;">
        <li class="menu-heading"><a href="javascript:void(0)">HUMAN RESOURCE</a></li>
        @can('Commission-list')
        <li>
            <a href="{{route('salary-comis.index')}}">
                <i class="menu-icon fa fa-user zmdi-hc-lg">
                </i><span class="menu-text">Commission</span>
            </a>
        </li>
        @endcan
    </ul>
</li>
@endif
@if(Auth::user()->hasAnyPermission(['AgentOutstanding-list','upplierWiseMaterial-list','SupplierOutstanding-list','BufferStockSummary-list',
        'CratesInquiryOld-list','CashBookInquiry-list','CratesInquiry-list','AgentInquire-list']))
<li>
    <a href="javascript:void(0)" class="submenu-toggle bg-warning">
        <i class="menu-icon fa fa-users zmdi-hc-lg"></i>
        <span class="menu-text text-dark">INQUIRIES</span>
        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
    </a>
    <ul class="submenu" style="display: block;">
        <li class="menu-heading"><a href="javascript:void(0)">INQUIRIES</a></li>
        @can('AgentOutstanding-list')
        <li>
            <a href="{{route('outstanding.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Agent Outstanding</span>
            </a>
        </li>
        @endcan
        @can('SupplierWiseMaterial-list')
        <li>
            <a href="{{route('supplier_wise_materials.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Supplier Wise Raw Materials</span>
            </a>
        </li>
        @endcan
        @can('SupplierOutstanding-list')
        <li>
            <a href="{{route('supplier-outstanding.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Supplier Outstanding</span>
            </a>
        </li>
        @endcan
        @can('BufferStockSummary-list')
        <li>
            <a href="{{route('buffer_stock_summary.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Buffer stock summary</span>
            </a>
        </li>
        @endcan
        @can('CratesInquiryOld-list')
        <li>
            <a href="{{route('crates-inquire.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Crates Inquiry Old</span>
            </a>
        </li>
        @endcan

        @can('CashBookInquiry-list')
        <li>
            <a href="{{route('cash-inquiry.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Cashbook Inquiry</span>
            </a>
        </li>
        @endcan

        @can('CratesInquiry-list')
        <li>
            <a href="{{route('crates.indexnew')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Crates Inquires</span>
            </a>
        </li>
        @endcan
        @can('AgentInquire-list')
        <li>
            <a href="{{route('agents.inquiries')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Agents Inquiries</span>
            </a>
        </li>
        @endcan

        <li>
            <a href="{{route('reusable.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Reusable Wastage</span>
            </a>
        </li>

        <li>
            <a href="{{route('a.g')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Day Payslips</span>
            </a>
        </li>

    </ul>
</li>
@endif
@if(Auth::user()->hasAnyPermission(['EndProductMovement-list','EndProductUsage-list','SemiFinishUsage-list','RawMaterialStockUsage-list',
        'SupplierInquiry-list','CratesInquiryStore-list','CratesInquiryAgent-list','CratesUsage-list','ShopItemMovement-list',
        'SupplierWiseMaterial-list','DebitNoteHistory-list','SalesReturnComparison-list','AgentOutstanding-list','SupplierOutstanding-list',
        'CashBookInquiry-list','BurdenGrade-list','NewFillingRequest-list','EndProductBurdenHistory-list','SalesHistory-list']))
<li>
    <a href="javascript:void(0)" class="submenu-toggle bg-warning">
        <i class="menu-icon fa fa-users zmdi-hc-lg"></i>
        <span class="menu-text text-dark">REPORTS</span>
        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
    </a>
    <ul class="submenu" style="display: block;">
        <li class="menu-heading"><a href="javascript:void(0)">REPORTS</a></li>
        @can('EndProductMovement-list')
        <li>
            <a href="{{route('reports.end-move')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg"></i>
                <span class="menu-text">End Product Movement</span>
            </a>
        </li>
        @endcan
        @can('EndProductUsage-list')
        <li>
            <a href="{{route('esr.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">End Product Usage</span>
            </a>
        </li>
        @endcan
        @can('SemiFinishUsage-list')
        <li>
            <a href="{{route('semi_finish_product.list')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg"></i>
                <span class="menu-text">Semi Finish Usage</span>
            </a>
        </li>
        @endcan
        @can('RawMaterialStockUsage-list')
        <li>
            <a href="{{route('stock_usage.index')}}">

                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Raw Materials Stock Usage</span>
            </a>
        </li>
        @endcan
        @can('SupplierInquiry-list')
        <li>
            <a href="{{route('sales-return.list')}}">

                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Sales Vs. Return</span>
            </a>
        </li>
        @endcan
        @can('CratesInquiryStore-list')

        <li>
            <a href="{{route('creates-stores.list')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Crates Inquiry - Stores </span>
            </a>
        </li>
        @endcan
        @can('CratesInquiryAgent-list')
        <li>
            <a href="{{route('creates-agent.list')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Crates Inquiry - Agent </span>
            </a>
        </li>
        @endcan
        @can('CratesUsage-list')
        <li>
            <a href="{{route('crate_usage.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg"></i>
                <span class="menu-text">Crates Usage</span>
            </a>
        </li>
        @endcan
        @can('ShopItemMovement-list')
        <li>
            <a href="{{route('shopitemlist.shopitem')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg"></i>
                <span class="menu-text">Shop Items Movement</span>
            </a>
        </li>
        @endcan
        @can('SupplierWiseMaterial-list')
        <li>
            <a href="{{route('supplier_wise_materials.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Supplier Wise Raw Materials</span>
            </a>
        </li>
        @endcan
        @can('DebitNoteHistory-list')
        <li>
            <a href="{{route('debit-note.index')}}">

                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Debit Notes History</span>
            </a>
        </li>
        @endcan
        @can('SalesReturnComparison-list')
        <li>
            <a href="{{route('sales-return.comparison')}}">

                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Sales & Return comparison</span>
            </a>
        </li>
        @endcan
        @can('AgentOutstanding-list')
        <li>
            <a href="{{route('outstanding.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Agent Outstanding</span>
            </a>
        </li>
        @endcan



        @can('SupplierOutstanding-list')
        <li>
            <a href="{{route('supplier-outstanding.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Supplier Outstanding</span>
            </a>
        </li>
        @endcan

        @can('CashBookInquiry-list')
        <li>
            <a href="{{route('cash-inquiry.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Cashbook Inquiry</span>
            </a>
        </li>
        @endcan


        @can('BurdenGrade-list')

        <li>
            <a href="{{route('burden.list')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Burdens History</span>
            </a>
        </li>
        @endcan
        @can('NewFillingRequest-list')
        <li>
            <a href="{{route('shorteats.list')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Cooking Request History</span>
            </a>
        </li>
        @endcan
        @can('EndProductBurdenHistory-list')
        <li>
            <a href="{{route('end-product.history')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">End Products Burden History</span>
            </a>
        </li>
        @endcan
        @can('SalesHistory-list')
        <li>
            <a href="{{route('sales.list')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Sales History</span>
            </a>
        </li>
        @endcan



    </ul>
</li>
@endif
@if(Auth::user()->hasAnyPermission(['Description-list','AddLoanLeasing-list','AddEquipments-list','AddUtilityBill-list','AddOutlet-list',
        'number-list','ApprovePrice-list','DriverCodeGenerate-list','EndProductPrice-list','UpdateOrderTemplates-list','CreateOrderTemplates-list',
        'PendingTemplateUpdateRequest-list','DatabaseBackup-list','user-list','role-list','permission_group-list','permission-list',
        'burden_payment_details-list']))
<li>
    <a href="javascript:void(0)" class="submenu-toggle bg-warning">
        <i class="menu-icon fa fa-users zmdi-hc-lg"></i>
        <span class="menu-text text-dark">MASTER FILES</span>
        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
    </a>
    <ul class="submenu" style="display: block;">
        <li class="menu-heading"><a href="javascript:void(0)">MASTER FILES</a></li>
        @can('Description-list')
        <li>
            <a href="{{route('cash_locker.mdesc')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Add Description</span>
            </a>
        </li>
        @endcan
        @can('AddLoanLeasing-list')
        <li>
            <a href="{{route('cash_locker.loan')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Add Loan & Leasing</span>
            </a>
        </li>
        @endcan
        @can('AddEquipments-list')
        <li>
            <a href="{{route('cash_locker.equip')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Add Equipments</span>
            </a>
        </li>
        @endcan
        @can('AddUtilityBill-list')
        <li>
            <a href="{{route('utility.bill')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Add Utility Bills</span>
            </a>
        </li>
        @endcan
        @can('AddOutlet-list')
        <li>
            <a href="{{route('outlet.add')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Add Outlets</span>
            </a>
        </li>
        @endcan
        @can('number-list')
        <li>
            <a href="{{route('numbers.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Number List</span>
            </a>
        </li>
        @endcan
        @can('ApprovePrice-list')
        <li>
            <a href="{{route('po.approve-price')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Approve Price </span>
            </a>
        </li>
        @endcan
        @can('DriverCodeGenerate-list')
        <li>
            <a href="{{route('driverCodeGenerate.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Driver Code Generate</span>
            </a>
        </li>
        @endcan
        @can('EndProductPrice-list')
        <li>
            <a href="{{route('endrecipe.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">End Product Recipe</span>
            </a>
        </li>
        @endcan
        @can('UpdateOrderTemplates-list')
        <li>
            <a href="{{route('agents.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Update Order Templates</span>
            </a>
        </li>
        @endcan
        @can('CreateOrderTemplates-list')
        <li>
            <a href="{{route('agents.create')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Create Order Templates</span>
            </a>
        </li>
        @endcan
        @can('PendingTemplateUpdateRequest-list')
        <li>
            <a href="{{route('buffer-stock.list-view')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Pending Template Update Request</span>
            </a>
        </li>
        @endcan

        @can('DatabaseBackup-list')
        <li>
            <a href="{{route('db-backup.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">DB Backup</span>
            </a>
        </li>
        @endcan
        @can('user-list')
        <li>
            <a href="{{route('users.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Users</span>
            </a>
        </li>
        @endcan
        @can('role-list')
        <li>
            <a href="{{route('roles.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Roles</span>
            </a>
        </li>
        @endcan
        @can('permission_group-list')
        <li>
            <a href="{{route('permission-groups.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Permission Group</span>
            </a>
        </li>
        @endcan
        @can('permission-list')
        <li>
            <a href="{{route('permissions.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Permissions</span>
            </a>
        </li>
        @endcan
        @can('burden_payment_details-list')
        <li>
            <a href="{{route('semi_finish_product.payburden')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Burden Payment Details</span>
            </a>
        </li>
        @endcan

        @if(isset($menu))
            {!!$menu!!}
        @endif
    </ul>
</li>
@endif

<li>
    <a href="javascript:void(0)" class="submenu-toggle bg-warning">
        <i class="menu-icon fa fa-users zmdi-hc-lg"></i>
        <span class="menu-text text-dark">MOBILE APP</span>
        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
    </a>
    <ul class="submenu" style="display: block;">
        <li class="menu-heading"><a href="javascript:void(0)">MOBILE APP</a></li>

        <li>
            <a href="{{route('sales-routes.all')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Sales Routes</span>
            </a>
        </li>
        <li>
            <a href="{{route('sales-shops.list')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Shops</span>
            </a>
        </li>
        <li>
            <a href="{{route('sales-shops.visited')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Visited Routes</span>
            </a>
        </li>

        <li>
            <a href="{{route('sales-shops.filter')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Visited Shops</span>
            </a>
        </li>
        <li>
            <a href="{{route('shop-orders.list')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Shop Order List</span>
            </a>
        </li>
        <li>
            <a href="{{route('agent.stats')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Agent Statistics</span>
            </a>
        </li>
    </ul>
</li>

<li>
    <a href="javascript:void(0)" class="submenu-toggle bg-warning">
        <i class="menu-icon fa fa-users zmdi-hc-lg"></i>
        <span class="menu-text text-dark">DASH BOARD</span>
        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
    </a>
    <ul class="submenu" style="display: block;">
        <li class="menu-heading"><a href="javascript:void(0)">DASH BOARD</a></li>

        <li>
            <a href="/dashboard/sales-return">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Sales Return Charts</span>
            </a>
        </li>

    </ul>
</li>


<!-- MAIN NAVIGATION SECTION >
<li class="menu-section-heading">MAIN NAVIGATION</li-->
{{--@if(isset($menu))
    {!!$menu!!}
@endif--}}
