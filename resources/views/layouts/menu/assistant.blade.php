<li class="menu-section-heading" style="color:yellow"><b>TO APPROVE</b></li>

<li>
    <a href="{{route('sr.index')}}">
        <i class="menu-icon fa fa-user zmdi-hc-lg">
        </i><span class="menu-text">Sales Request (Distributors)</span>
    </a>
</li>

<li>
    <a href="{{route('production.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Sales Request (Show Rooms)</span>
    </a>
</li>

<li>
    <a href="{{route('raw_materials.proindex')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Raw Materials Available Current Stock</span>
    </a>
</li>

<li>
    <a href="{{route('agents.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Update Order Templates</span>
    </a>
</li>
<li>
    <a href="{{route('buffer-stock.list-view')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Pending Template Update Request</span>
    </a>
</li>

<li>
    <a href="{{route('end.remove')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Remove Stores Return</span>
    </a>
</li>


//main topic
<li class="menu-section-heading" style="color:yellow"><b>INQUIRES</b></li>
<li>
    <a href="{{route('agents.inquiries')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Agents Inquiries</span>
    </a>
</li>

<li>
    <a href="{{route('salary-comis.index')}}">
        <i class="menu-icon fa fa-user zmdi-hc-lg">
        </i><span class="menu-text">Day Commission</span>
    </a>
</li>

<li>
    <a href="{{route('ep.list')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">End Product Merge List</span>
    </a>
</li>

<li>
    <a href="{{route('proOrder.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Semi Finish Status</span>
    </a>
</li>
<li>
    <a href="{{route('semi_finish_products.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Semi Finish Stock</span>
    </a>
</li>
<li>
    <a href="{{route('creates-stores.list')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Crates Inquiry - Stores </span>
    </a>
</li>

<li>
    <a href="{{route('creates-agent.list')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Crates Inquiry - Agent </span>
    </a>
</li>

<li>
    <a href="{{route('shopitemlist.shopitem')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg"></i>
        <span class="menu-text">Shop Items Movement</span>
    </a>
</li>
<li>
    <a href="{{route('stock_usage.index')}}">

        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Raw Materials Stock Usage</span>
    </a>
</li>

<li>
    <a href="{{route('semi_finish_product.list')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg"></i>
        <span class="menu-text">Semi Finish Usage</span>
    </a>
</li>

<li>
    <a href="{{route('esr.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">End Product Usage</span>
    </a>
</li>

<li>
    <a href="{{route('reports.end-move')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">End Product Movements</span>
    </a>
</li>

<li>
    <a href="{{route('supplier_wise_materials.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Supplier Wise Raw Materials</span>
    </a>
</li>


<li class="menu-section-heading" style="color:yellow"><b>REPORTS</b></li>
<li>
    <a href="{{route('sales-return.comparison')}}">

        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Sales & Return comparison</span>
    </a>
</li>

<li>
    <a href="{{route('outstanding.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Agent Outstanding</span>
    </a>
</li>
<li>
    <a href="{{route('burden.list')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Burden History</span>
    </a>
</li>
<li>
    <a href="{{route('shorteats.list')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Cooking Request History</span>
    </a>
</li>

<li>
    <a href="{{route('end-product.history')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">End Products Burden History</span>
    </a>
</li>


<li>
    <a href="{{route('reusable.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Reusable Wastage Movement</span>
    </a>
</li>

<li>
    <a href="{{route('buffer_stock_summary.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Raw Material stock</span>
    </a>
</li>

<li>
    <a href="{{route('admin.settle_notes')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Settle Debit Note</span>
    </a>
</li>
<li>
    <a href="{{route('debit-note.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Debit Notes History</span>
    </a>
</li>
<li>
    <a href="{{route('sales.list')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Sales History</span>
    </a>
</li>

<li>
    <a href="{{route('creates-stores.list')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Crates Movement </span>
    </a>
</li>

<li>
    <a href="javascript:void(0)" class="submenu-toggle bg-warning">
        <i class="menu-icon fa fa-users zmdi-hc-lg"></i>
        <span class="menu-text text-dark">DASH BOARD</span>
        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
    </a>
    <ul class="submenu" style="display: block;">
        <li class="menu-heading"><a href="javascript:void(0)">DASH BOARD</a></li>

        <li>
            <a href="/dashboard/sales-return">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Sales Return Charts</span>
            </a>
        </li>

    </ul>
</li>


    <!--/ul-->

