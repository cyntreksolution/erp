<li class="menu-section-heading">MAIN NAVIGATION</li>
<li>
    <a href="{{route('agent-payment.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Agents' Payments</span>
    </a>
</li>

<li>
    <a href="{{route('admin.settle_notes')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Settle Debit Note</span>
    </a>
</li>

<li>
    <a href="{{route('salary-comis.index')}}">
        <i class="menu-icon fa fa-user zmdi-hc-lg">
        </i><span class="menu-text">Commission</span>
    </a>
</li>

<li>
    <a href="{{route('sales.list')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Sales History</span>
    </a>
</li>

<li>
    <a href="{{route('po.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Settle Purchasing</span>
    </a>
</li>


<li>
    <a href="{{route('cash_locker.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Cash Locker</span>
    </a>
</li>

<li>
    <a href="{{route('pass_book.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Passbook </span>
    </a>
</li>

<li>
    <a href="{{route('salwages.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Salary & Wadges </span>
    </a>
</li>

<li>
    <a href="{{route('a.g')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Day Payslips</span>
    </a>
</li>



<li>
    <a href="{{route('outstanding.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Agent Outstanding</span>
    </a>
</li>




<li>
    <a href="{{route('supplier-outstanding.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Supplier Outstanding</span>
    </a>
</li>


<li>
    <a href="{{route('cash-inquiry.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Cashbook Inquiry</span>
    </a>
</li>


<li>
    <a href="{{route('agents.inquiries')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Agents Inquiries</span>
    </a>
</li>
<li>
    <a href="{{route('debit-note.index')}}">

        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Debit Notes History</span>
    </a>
</li>