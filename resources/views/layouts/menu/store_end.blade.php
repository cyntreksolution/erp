{{--@if(Auth::user()->hasAnyPermission(['sales_request-list','SettleDebitNote-list','OrderList-list','loading-list',--}}
{{--'sales_return-list','sales_orders-list','ShopItemMovement-list','BulkBurdenLoading-list','PendingCratesm-list']))--}}
<li class="menu-section-heading" style="color:yellow"><b>MAIN NAVIGATION</b></li>
{{--@can('sales_request-list')--}}
<li>
    <a href="{{route('sr.index')}}">
        <i class="menu-icon fa fa-user zmdi-hc-lg">
        </i><span class="menu-text">Sales Request</span>
    </a>
</li>
{{--@endcan--}}
<!--li>
    <a href="{{route('approve-list.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Approve Agents Received Order</span>
    </a>
</li-->
{{--@can('SettleDebitNote-list')--}}
<li>
    <a href="{{route('admin.return')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Pending returns</span>
    </a>
</li>
{{--@endcan--}}
<!--li>
    <a href="{{route('admin.settle_notes')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Settle Debit Note</span>
    </a>
</li-->
{{--@can('OrderList-list')--}}
<li>
    <a href="{{route('order.list')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Order List</span>
    </a>
</li>
{{--@endcan--}}
{{--@can('loading-list')--}}
<li>
    <a href="{{route('loading.stable')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Loading</span>
    </a>
</li>
{{--@endcan--}}

<!--li>
    <a href="{{route('end-product-merge-list.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">End Product Burden Merge</span>
    </a>
</li-->
{{--@can('sales_return-list')--}}
<li>
    <a href="{{route('sales_return.index')}}">
        <i class="menu-icon fa fa-user zmdi-hc-lg">
        </i><span class="menu-text">Sales Return</span>
    </a>
</li>
{{--@endcan--}}
{{--@can('sales_orders-list')--}}
<li>
    <a href="{{route('so.create')}}">
        <i class="menu-icon fa fa-user zmdi-hc-lg">
        </i><span class="menu-text">Sales Order</span>
    </a>
</li>
{{--@endcan--}}
{{--@can('ShopItemMovement-list')--}}
<li>
    <a href="{{route('shopitemlist.shopitem')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg"></i>
        <span class="menu-text">Shop Items Movement</span>
    </a>
</li>
{{--@endcan--}}
{{--@can('BulkBurdenLoading-list')--}}
<li>
    <a href="{{route('bulk_burden_loading.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Bulk Burden To Loading</span>
    </a>
</li>
{{--@endcan--}}
{{--@can('PendingCratesm-list')--}}
<li>
    <a href="{{route('crates-return.allcrates')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Pending Crates</span>
    </a>
</li>
{{--@endcan--}}
{{--@endif--}}
//main topic
{{--@if(Auth::user()->hasAnyPermission(['AgentInquireReport-list','CratesInquiryAgentReport-list','EndProductStockReport-list','SettleDebitNoteReport-list',--}}
{{--        'EndProductMovementReport-list','EndProductUsageReport-list','CratesInquiryStoreReport-list']))--}}
<li class="menu-section-heading" style="color:yellow"><b>REPORTS</b></li>

{{--@can('AgentInquireReport-list')--}}
<li>
    <a href="{{route('agents.inquiries')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Agents Inquiries</span>
    </a>
</li>
{{--@endcan--}}
{{--@can('CratesInquiryAgentReport-list')--}}
<li>
    <a href="{{route('creates-agent.list')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Crates Inquiry - Agent </span>
    </a>
</li>
{{--@endcan--}}
<!--li>
    <a href="{{route('crates-inquire.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Crates Inquiry</span>
    </a>
</li>
<li>
    <a href="{{route('agent-inquire.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Agent Inquires</span>
    </a>
</li-->
<!--li>
    <a href="{{route('sr.merged')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Merged sales requests</span>
    </a>
</li-->
{{--@can('EndProductStockReport-list')--}}
<li>
    <a href="{{route('esr.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">End Product Stock Report</span>
    </a>
</li>
{{--@endcan--}}
<!--li>
    <a href="#">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Day Transactions</span>
    </a>
</li-->

<!-- temparary add -->
{{--@can('SettleDebitNoteReport-list')--}}
<li>
    <a href="{{route('admin.settle_notes')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Settle Debit Note</span>
    </a>
</li>
{{--@endcan--}}
<!--li>
    <a href="{{route('debit-note.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Debit Notes ***New Feature***</span>
    </a>
</li>
<li>
    <a href="{{route('sales.list')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Sales ***New Feature***</span>
    </a>
</li-->

<!--li class="">
    <a href="javascript:void(0)" class="submenu-toggle">
        <i class="menu-icon fa fa-users zmdi-hc-lg"></i>
        <span class="menu-text">Reports</span>
        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
    </a>
    <ul class="submenu" style="display: block;">
        <li class="menu-heading"><a href="javascript:void(0)">Reports</a></li-->
{{--@can('EndProductMovementReport-list')--}}
        <li>
            <a href="{{route('reports.end-move')}}">
                <i class="menu-icon fa fa- zmdi-hc-lg"></i>
                <span class="menu-text">End Product Movements</span>
            </a>
        </li>
{{--@endcan--}}

        {{--        <li>--}}
        {{--            <a href="{{route('end_products.index')}}">--}}
        {{--                <i class="menu-icon fa fa- zmdi-hc-lg"></i>--}}
        {{--                <span class="menu-text">End Products</span>--}}
        {{--            </a>--}}
        {{--        </li>--}}
{{--@can('EndProductUsageReport-list')--}}
        <li>
            <a href="{{route('esr.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">End Product Usage</span>
            </a>
        </li>
{{--@endcan--}}
{{--@can('CratesInquiryStoreReport-list')--}}
        <li>
            <a href="{{route('creates-stores.list')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Crates Inquiry - Stores </span>
            </a>
        </li>
{{--@endcan--}}



    <!--/ul-->
</li>
{{--@endif--}}