<li class="menu-section-heading" style="color:yellow"><b>MAIN NAVIGATION</b></li>

<!--li>
    <a href="{{route('sr.index')}}">
        <i class="menu-icon fa fa-user zmdi-hc-lg">
        </i><span class="menu-text">Sales Request</span>
    </a>
</li>
<li>
    <a href="{{route('approve-list.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Approve Agents Received Order</span>
    </a>
</li>
<li>
    <a href="{{route('admin.return')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Pending returns</span>
    </a>
</li>
<li>
    <a href="{{route('order.list')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Order List</span>
    </a>
</li>
<li>
    <a href="{{route('loading.stable')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Loading</span>
    </a>
</li>

<li>
    <a href="{{route('end-product-merge-list.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">End Product Burden Merge</span>
    </a>
</li>
<li>
    <a href="{{route('sales_return.index')}}">
        <i class="menu-icon fa fa-user zmdi-hc-lg">
        </i><span class="menu-text">Sales Return</span>
    </a>
</li>
<li>
    <a href="{{route('so.create')}}">
        <i class="menu-icon fa fa-user zmdi-hc-lg">
        </i><span class="menu-text">Sales Order</span>
    </a>
</li>

<li>
    <a href="{{route('bulk_burden_loading.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Bulk Burden To Loading</span>
    </a>
</li>
<li>
    <a href="{{route('crates-return.allcrates')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Pending Crates</span>
    </a>
</li>
<li>
    <a href="{{route('agents.inquiries')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Agents Inquiries</span>
    </a>
</li>

<li>
    <a href="{{route('agents.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Create Order Templates</span>
    </a>
</li-->
<li>
    <a href="{{route('sr.index')}}">
        <i class="menu-icon fa fa-user zmdi-hc-lg">
        </i><span class="menu-text">Sales Request</span>
    </a>
</li>
<li>
    <a href="{{route('agents.create')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Update Order Templates</span>
    </a>
</li>
<li>
    <a href="{{route('buffer-stock.list-view')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Pending Template Update Request</span>
    </a>
</li>

//main topic
<li class="menu-section-heading" style="color:yellow"><b>REPORTS</b></li>
<!--li>
    <a href="{{route('crates-inquire.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Crates Inquiry</span>
    </a>
</li-->
<li>
    <a href="{{route('agents.inquiries')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Agents Inquiries</span>
    </a>
</li>

<li>
    <a href="{{route('esr.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">End Product Stock Report</span>
    </a>
</li>


<!-- temparary add -->

<!--li>
    <a href="{{route('debit-note.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Debit Notes History</span>
    </a>
</li-->
<li>
    <a href="{{route('admin.settle_notes')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Settle Debit Note</span>
    </a>
</li>
<li>
    <a href="{{route('sales.list')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Sales History</span>
    </a>
</li>
<li>
    <a href="{{route('sales-return.comparison')}}">

        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Sales & Return comparison</span>
    </a>
</li>

<li>
    <a href="{{route('reports.end-move')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg"></i>
        <span class="menu-text">End Product Movements</span>
    </a>
</li>
<li>
    <a href="{{route('sales-return.list')}}">

        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Sales Vs. Return</span>
    </a>
</li>

<!--li class="">
    <a href="javascript:void(0)" class="submenu-toggle">
        <i class="menu-icon fa fa-users zmdi-hc-lg"></i>
        <span class="menu-text">Reports</span>
        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
    </a>
    <ul class="submenu" style="display: block;">
        <li class="menu-heading"><a href="javascript:void(0)">Reports</a></li>

        <li>
            <a href="{{route('reports.end-move')}}">
                <i class="menu-icon fa fa- zmdi-hc-lg"></i>
                <span class="menu-text">End Product Movements</span>
            </a>
        </li>

{{--        <li>--}}
{{--            <a href="{{route('end_products.index')}}">--}}
{{--                <i class="menu-icon fa fa- zmdi-hc-lg"></i>--}}
{{--                <span class="menu-text">End Products</span>--}}
{{--            </a>--}}
{{--        </li>--}}
        <li>
            <a href="{{route('esr.index')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">End Product Usage</span>
            </a>
        </li>

        <li>
            <a href="{{route('creates-stores.list')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Crates Inquiry - Stores </span>
            </a>
        </li>

        <li>
            <a href="{{route('creates-agent.list')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Crates Inquiry - Agent </span>
            </a>
        </li>

    </ul>
</li-->

<li>
    <a href="javascript:void(0)" class="submenu-toggle bg-warning">
        <i class="menu-icon fa fa-users zmdi-hc-lg"></i>
        <span class="menu-text text-dark">MOBILE APP</span>
        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
    </a>
    <ul class="submenu" style="display: block;">
        <li class="menu-heading"><a href="javascript:void(0)">MOBILE APP</a></li>

        <li>
            <a href="{{route('sales-shops.visited')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Visited Routes</span>
            </a>
        </li>

        <li>
            <a href="{{route('sales-shops.filter')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Visited Shops</span>
            </a>
        </li>
        <li>
            <a href="{{route('shop-orders.list')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Shop Order List</span>
            </a>
        </li>
        <li>
            <a href="{{route('agent.stats')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Agent Statistics</span>
            </a>
        </li>
        {{--        <li>--}}
        {{--            <a href="{{route('shop-orders.list')}}">--}}
        {{--                <i class="menu-icon fa fa-first-order zmdi-hc-lg">--}}
        {{--                </i><span class="menu-text">Shop Order Item List</span>--}}
        {{--            </a>--}}
        {{--        </li>--}}
    </ul>
</li>
<li>
    <a href="javascript:void(0)" class="submenu-toggle bg-warning">
        <i class="menu-icon fa fa-users zmdi-hc-lg"></i>
        <span class="menu-text text-dark">DASH BOARD</span>
        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
    </a>
    <ul class="submenu" style="display: block;">
        <li class="menu-heading"><a href="javascript:void(0)">DASH BOARD</a></li>

        <li>
            <a href="/dashboard/sales-return">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Sales Return Charts</span>
            </a>
        </li>

    </ul>
</li>
