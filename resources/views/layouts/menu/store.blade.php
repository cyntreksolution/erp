<li class="menu-section-heading" style="color:yellow"><b>MAIN NAVIGATION</b></li>
<li>
    <a href="{{route('po.index')}}">
        <i class="menu-icon fa fa-truck zmdi-hc-lg">
        </i><span class="menu-text">Purchasing Order</span>
    </a>
</li>
<li>
    <a href="{{route('burden_order.index')}}">
        <i class="menu-icon fa fa-paper-plane-o zmdi-hc-lg">
        </i><span class="menu-text">Issue Raw Materials For Semi Finish</span>
    </a>
</li>
<li>
    <a href="{{route('reusable.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Reusable Wastage</span>
    </a>
</li>
<li>
    <a href="{{route('raw_materials.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Raw Materials</span>
    </a>
</li>

<li>
    <a href="{{route('po.approve-price')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Edit Price Change </span>
    </a>
</li>

<li>
    <a href="{{route('issue.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Issue Raw Materials For End Product</span>
    </a>
</li>
<li>
    <a href="{{route('shop-item-issue.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Issue Shop Items</span>
    </a>
</li>
<li>
    <a href="{{route('raw-request.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Request Raw Materials From Production</span>
    </a>
</li>
<!--li>
    <a href="{{route('loading.all')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">loaded</span>
    </a>
</li>
<li>
    <a href="{{route('crates-return.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Pending Crates</span>
    </a>
</li-->

<li class="menu-section-heading" style="color:yellow"><b>INQUARIES</b></li>
<li>
    <a href="{{route('buffer_stock_summary.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Raw Material stock</span>
    </a>
</li>

<li>
    <a href="{{route('eburden.mergelist')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">End Product Burden Merge</span>
    </a>
</li>
<li>
    <a href="{{route('supplier_wise_materials.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Supplier wise materials</span>
    </a>
</li>

<li>
    <a href="{{route('stock_usage.index')}}">

        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Raw Materials Stock Usage</span>
    </a>
</li>

<!--li>
    <a href="{{route('supplier.list')}}">

        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Supplier Inquiry</span>
    </a>
</li-->
<li>
    <a href="{{route('shopitemlist.shopitem')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg"></i>
        <span class="menu-text">Shop Items Movement</span>
    </a>
</li>

<li>
    <a href="{{route('supplier_wise_materials.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Supplier Wise Raw Materials</span>
    </a>
</li>
