
<br>
<br>
<br>

<li>
    <a href="javascript:void(0)" class="submenu-toggle bg-warning">
        <i class="menu-icon fa fa-users zmdi-hc-lg"></i>
        <span class="menu-text text-dark">MAIN NAVIGATION</span>
        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
    </a>
    <ul class="submenu" style="display: block;">
        <li class="menu-heading"><a href="javascript:void(0)">MAIN NAVIGATION</a></li>


<li>
    <a href="{{route('so.create')}}">
        <i class="menu-icon fa fa-user zmdi-hc-lg">
        </i><span class="menu-text">Sales Request</span>
    </a>
</li>

<li>
    <a href="{{route('sales_return.index')}}">
        <i class="menu-icon fa fa-user zmdi-hc-lg">
        </i><span class="menu-text">Sales Return</span>
    </a>
</li>

<li>
    <a href="{{route('crates-request.cratesRequestList')}}">
        <i class="menu-icon fa fa-truck zmdi-hc-lg">
        </i><span class="menu-text">Return crates</span>
    </a>
</li>

<li>
    <a href="{{route('agent-payment.index')}}">
        <i class="menu-icon fa fa-truck zmdi-hc-lg">
        </i><span class="menu-text">Make Payment</span>
    </a>
</li>

<li>
    <a href="{{route('agents.index')}}">
        <i class="menu-icon fa fa-truck zmdi-hc-lg">
        </i><span class="menu-text">Order Templates</span>
    </a>
</li>

<li>
    <a href="{{route('buffer-stock.list-view')}}">
        <i class="menu-icon fa fa-truck zmdi-hc-lg">
        </i><span class="menu-text">Pending Order Template Request</span>
    </a>
</li>

<li>
    <a href="{{route('order.list')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Order List</span>
    </a>
</li>
    </ul></li>


<li>
    <a href="javascript:void(0)" class="submenu-toggle bg-warning">
        <i class="menu-icon fa fa-users zmdi-hc-lg"></i>
        <span class="menu-text text-dark">INQUIRIES & REPORTS</span>
        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
    </a>
<ul class="submenu" style="display: block;">
    <li class="menu-heading"><a href="javascript:void(0)">REPORTS</a></li>
<li>
    <a href="{{route('agents.inquiries')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Agent's Inquiries</span>
    </a>
</li>

<li>
    <a href="{{route('sales.list')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Sales History</span>
    </a>
</li>

<!--li>
    <a href="{{route('debit-note.index')}}">

        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Debit Notes History</span>
    </a>
</li-->
    <li>
        <a href="{{route('admin.settle_notes')}}">
            <i class="menu-icon fa fa-first-order zmdi-hc-lg">
            </i><span class="menu-text">Settle Debit Note</span>
        </a>
    </li>
<li>
    <a href="{{route('shopitemlist.shopitem')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg"></i>
        <span class="menu-text">Shop Items Movement</span>
    </a>
</li>

<li>
    <a href="{{route('creates-agent.list')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Crates Inquiry</span>
    </a>
</li>

</ul>
</li>


<li>
    <a href="javascript:void(0)" class="submenu-toggle bg-warning">
        <i class="menu-icon fa fa-users zmdi-hc-lg"></i>
        <span class="menu-text text-dark">MOBILE APP</span>
        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
    </a>
    <ul class="submenu" style="display: block;">
        <li class="menu-heading"><a href="javascript:void(0)">MOBILE APP</a></li>

        <li>
            <a href="{{route('sales-shops.visited')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Visited Routes</span>
            </a>
        </li>

        <li>
            <a href="{{route('shop-orders.list')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">Shop Order List</span>
            </a>
        </li>
    </ul>
</li>



