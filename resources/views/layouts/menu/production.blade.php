
<li class="menu-section-heading" style="color:yellow"><b>SEMI FINISH PRODUCT</b></li>
<li>
    <a href="{{route('burden.create')}}">
        <i class="menu-icon fa fa-free-code-camp zmdi-hc-lg">
        </i><span class="menu-text">Burdens Inquiry</span>
    </a>
</li>
<li>
    <a href="{{route('proOrder.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Production Order</span>
    </a>
</li>

<li>
    <a href="{{route('shorteats.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">New Filling</span>
    </a>
</li>

<li>
    <a href="{{route('semi-issue.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Issue Semi Finish</span>
    </a>
</li>
<li>
    <a href="{{route('semi_bulk_burden.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Burden Make</span>
    </a>
</li>
<li>
    <a href="{{route('semi_bulk_burden_bake.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Burden Burn</span>
    </a>
</li>
<li>
    <a href="{{route('short_bulk_burden.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Filling Make</span>
    </a>
</li>
<li>
    <a href="{{route('short_bulk_burden_bake.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Filling Burn</span>
    </a>
</li>
<li>
    <a href="{{route('semi_finish_products.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Semi Finish Stock</span>
    </a>
</li>

<li>
    <a href="{{route('raw-request.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Request Raw Materials </span>
    </a>
</li>

<li class="menu-section-heading" style="color:yellow"><b>END PRODUCTS</b></li>
<li>
    <a href="{{route('production.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Production</span>
    </a>
</li>
<li>
    <a href="{{route('production.merged')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Sales Request Merge List</span>
    </a>
</li>
<li>
    <a href="{{route('ep.list')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">End Product Merge List</span>
    </a>
</li>
<li>
    <a href="{{route('eburden.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">New Burdens</span>
    </a>
</li>


<li>
    <a href="{{route('end-product-merge-list.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">End Product Burden Merge</span>
    </a>
</li>
<li>
    <a href="{{route('bulk_burden.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Make</span>
    </a>
</li>
<li>
    <a href="{{route('bulk_burden_bake.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Burn</span>
    </a>
</li>
<li>
    <a href="{{route('burden_grade.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">End Product Burden Grade</span>
    </a>
</li>



<li class="menu-section-heading" style="color:yellow"><b>HUMAN RESOURCE</b></li>
<li>
    <a href="{{route('attendance.index')}}">
        <i class="menu-icon fa fa-user zmdi-hc-lg">
        </i><span class="menu-text">Attendance</span>
    </a>
</li><li>
    <a href="{{route('team.index')}}">
        <i class="menu-icon fa fa-user zmdi-hc-lg">
        </i><span class="menu-text">Workers Team</span>
    </a>
</li>
<li>
    <a href="{{route('salary-comis.index')}}">
        <i class="menu-icon fa fa-user zmdi-hc-lg">
        </i><span class="menu-text">Commission</span>
    </a>
</li>
<li class="menu-section-heading" style="color:yellow"><b>HISTORY REPORTS</b></li>
<li>
    <a href="{{route('burden.list')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Semi Finish Burdens</span>
    </a>
</li>
<li>
    <a href="{{route('shorteats.list')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Filling</span>
    </a>
</li>
<li>
    <a href="{{route('eburden.mergedlist')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Merged End Product</span>
    </a>
</li>
<li>
    <a href="{{route('esr.index')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">End Product Stock</span>
    </a>
</li>
<li>
    <a href="{{route('burden.list')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Burden History</span>
    </a>
</li>
<li>
    <a href="{{route('shorteats.list')}}">
        <i class="menu-icon fa fa-first-order zmdi-hc-lg">
        </i><span class="menu-text">Cooking Request History</span>
    </a>
</li>
<li class="">
    <a href="javascript:void(0)" class="submenu-toggle">
        <i class="menu-icon fa fa-users zmdi-hc-lg"></i>
        <span class="menu-text">Reports</span>
        <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
    </a>
    <ul class="submenu" style="display: block;">
        <li class="menu-heading"><a href="javascript:void(0)">Reports</a></li>
        <li>
            <a href="{{route('reports.end-move')}}">
                <i class="menu-icon fa fa- zmdi-hc-lg"></i>
                <span class="menu-text">End Product Movements</span>
            </a>
        </li>
        <li>
            <a href="{{route('end-product.history')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg">
                </i><span class="menu-text">End Products Burden History</span>
            </a>
        </li>

        <li>
            <a href="{{route('semi_finish_product.list')}}">
                <i class="menu-icon fa fa-first-order zmdi-hc-lg"></i>
                <span class="menu-text">Semi Finish Usage</span>
            </a>
        </li>
    </ul>
</li>

