<html>
<head>
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/invoice.css')}}" rel="stylesheet">
    <style media="all">
        @page {
            margin: 30px 5px 30px 5px;
            size: 75mm 999mm;
        }
    </style>
</head>

<body style="margin-bottom: 100px">

<div class="row">

    <div class="col-xs-3">
        <img src="{{asset('assets/img/buntak_grace.jpeg')}}" style="width: 100px">
    </div>

    <div class="col-xs-6 text-right">
        <h6>
            <b>Ranjanas Holdings [Pvt] Ltd</b><br>
            Katukurunda<br>
            Habaraduwa<br>
            Hotline : +9477 330 4678<br>
        </h6>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <table class="table">
            <thead>
            <tr>
                <th>Item with price</th>
                <th class="text-center">QTY</th>
                <th class="text-right">Accepted Qty</th>
            </tr>
            </thead>
            <tbody>
  {{--          @php $counter= 0; @endphp
            @foreach($items as $product)
                @if ($product->qty>0)
                    <tr>
                        <td>{{$product->endProduct->name." (".number_format((float)$product->unit_value, 2, '.', '').")"}}</td>
                        <td class="text-center">{{$product->qty}}</td>
                        <td align="right">
                            <div style="width:150px;height:30px;border:1px solid #000;"></div>
                        </td>
                    </tr>
                @endif
                @php  $counter = $counter+1 ;@endphp
            @endforeach  --}}

            </tbody>
        </table>
    </div>

</div>

<br>

<footer>
    <div class="row" style="margin-top: 20px;">
        <h4 align="center">
            <h6 class="text-muted">Powered by Cyntrek Solutions<br>
                <h7 class="num">+9471 122 5489 / +9471 532 2262</h7>
            </h6>
        </h4>
    </div>

    <p style="margin-top: 150px">--------------------------------------------------------------</p>
</footer>

</body>

</html>



{{--@extends('layouts.back.master')@section('title','Raw Inquiry Multiple Entry List')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }
    </style>
@stop

@section('content')



    <div class="table-responsive">
        <table id="rawi_table" class="display text-center ">
            <thead>

            <tr>

                <th>Date & Time</th>
                <th>Raw Material</th>
                <th>Movement Type & Description</th>
                <th>Start Balance</th>
                <th>Qty</th>
                <th>Amount</th>
                <th>Balance Qty</th>
                <th>Action</th>
            </tr>
            </thead>

            @php  dd($datas);  @endphp



            <tfoot>
            <tr>

                <th>Date & Time</th>
                <th>Raw Material</th>
                <th>Movement Type & Description</th>
                <th>Start Balance</th>
                <th>Qty</th>
                <th>Amount</th>
                <th>Balance Qty</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
    </div>

@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script>
        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019",
            "maxDate":moment()
        });


        $('#date_range').val('')
        var amount_sum;
        var qty_sum;
        var openning_balance;
        $(document).ready(function () {
            table = $('#rawi_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "ajax": {
                    url: "{{url('/raw-inquiry/list/table/data')}}",
                    type: "get",
                    error: function () {
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    },
                    dataSrc: function (data) {
                        amount_sum = data.amount_sum;
                        qty_sum = data.qty_sum;
                        openning_balance = data.openning_balance;
                        return data.data;
                    }
                },
                pageLength: 100,
                responsive: true,
                drawCallback: function (settings) {
                    var api = this.api();
                    $(api.column(4).footer()).html(
                        'qty : ' + qty_sum
                    );
                    $(api.column(5).footer()).html(
                        'amount : ' + amount_sum
                    );

                    $("#openning_balance").empty();
                    $('#openning_balance').append(openning_balance);
                }
            });
        });

        $(document).ready(function () {
            $("#emp_id").hide();

        });
        $("#desc_id").change(function () {
            let descid = $('#desc_id').val();
            let empid = $('#emp_id').val();


            if (descid != 6) {

                $("#emp_id").hide();

            }else {

                $("#emp_id").show();

                $.ajax({
                    url: "/raw-inquiry/list/table/data" + raw_material + "/" + reference_type + "/" + date_range + "/" + emp_id,
                    type: "GET",
                    success: function (data) {
                        console.log(data);
                        appendRawList(data);
                    },
                    error: function (xhr, status) {

                    }
                });

            }

        }).trigger("change");




        function process_form(e) {
            let raw_material = $("#raw_material").val();
            let table = $('#rawi_table').DataTable();
            let reference_type = $("#desc_id").val();
            let date_range = $("#date_range").val();
            let emp_id = $("#emp_id").val();

            table.ajax.url('/raw-inquiry/list/table/data?raw_material=' + raw_material + '&reference_type=' + reference_type + '&date_range=' + date_range + '&emp_id=' + emp_id +'&filter=' + true).load();
        }

        function process()
        {
            let raw_material = $("#raw_material").val();

            window.location.href = "/raw-inquiry/list/table/data_multiple?raw_material=" + raw_material;
        }


        function process_form_reset() {
            $("#raw_material").val('');

            let table = $('#rawi_table').DataTable();
            table.ajax.url('raw-inquiry/list/table/data').load();
        }

    </script>

@stop
--}}
