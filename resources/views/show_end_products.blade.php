@extends('layouts.back.master')@section('title','End Products Burden | Details')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/lightbox2/dist/css/lightbox.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}"/>

    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">

    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }


        .profile-pic {
            position: relative;
            /*display: inline-block;*/
        }

        .profile-pic:hover .edit {
            display: block;
        }

        .edit {
            padding-top: 7px;
            padding-right: 7px;
            position: absolute;
            right: 0;
            top: 0;
            display: none;
        }

        .edit a {
            color: #000;
        }

        #container {
            position: relative;
            padding: 0px;
            width: 100%;
            height: 330px;
            overflow: auto;
        }

        #container .content {
            width: 100%;
            height: 25%;
        }
    </style>

@stop

@section('content')
    <div class="app-wrapper">

        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">{{$burden->created_at->format('d l F Y').' - '.$burden['endProdcuts']->name}}
                    Burden</h5>
            </div>
            <div class="text-center">
                <h5 class="text-uppercase ">{{$burden->serial}}</h5>
                <h5 class="text-uppercase mb-1" id="{{$burden->id*754}}">{{$burden['endProdcuts']->name}}</h5>
                <h6 class="text mb-lg-1">{{'Burden Size : '.$burden->burden_size}} </h6>
                <br>
            </div>
            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content" class="col-md-12">
                    <div class="media-list" id="rcontainer">

                        <div class="media content">
                            @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                            <div class="avatar avatar avatar-md project-icon bg-{{$color[2]}}"
                                 class="col-md-2"
                                 data-plugin="firstLitter"
                                 data-target="#90"></div>
                            <div class="media-body">
                                <div class="col-md-7">
                                    <h6 class="project-name"
                                        id="90">Grade</h6>
                                </div>
                                <br>
                            </div>
                            <div class="col-md-3">
                                <button class="btn btn-outline-primary" type="button" data-toggle="collapse"
                                        data-target="#multiCollapseExample1"
                                        aria-expanded="false"
                                        aria-controls="multiCollapseExample1"
                                        style="width: 100%; border-color: transparent">
                                                <span>
                                                <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                </span> Quantity
                                </button>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="" id="multiCollapseExample1" style="">
                                <div class="card-body">
                                    @foreach($burden['grade'] as $r)
                                        <div class="row burden">
                                            <div class="col-md-2">
                                                <span><i class="fa fa-shopping-bag"></i> </span>
                                            </div>
                                            <div class="col-md-7">
                                                {{$r['grade']->name}}

                                            </div>
                                            <div class="col-md-3">
                                                {{$r->burden_qty}}
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

        <div class="profile-section-user">
            <div class="profile-cover-img profile-pic">
                <!--img src="{{asset('assets\img\semi2.jpg')}}" alt=""-->
            </div>
            <div class="profile-info-brief p-3">
               {{-- @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                <div class="user-profile-avatar avatar avatar-circle avatar-md project-iconr bg-{{$color[$burden->colour]}}"
                     data-plugin="firstLitter"
                     data-target="#{{$burden->id*754}}"></div>--}}





                <div class="py-3">
                    {{'Active Recipe : '.$burden['recipe']->name}}<br>
                    <hr class="m-1">

                    {{'Expected Quantity : '.$burden->expected_end_product_qty}} units<br>


                    @php
                        $makers=$burdenClass->getMakers($burden->employee_group);
                        $gg = \Carbon\Carbon::today();
                        $tom = \EndProductBurdenManager\Models\EndProductBurden::where('id','=',$burden->id)->where('created_start_time', '>', $gg->subDays(2)->format('Y-m-d') . '%')->first();


                    @endphp

                    <hr class="m-1">
                    <b class="font-weight-bold">First Stage</b><br>
                    -Start At : {{(!empty($burden->created_start_time)) ?\Carbon\Carbon::parse($burden->created_start_time)->format('d-m-Y H:m:s'):'-'}} <br>
                    -Work Group :{{!empty($burden->employeeGroup) && $makers->count()>0 ?$makers[0]->name:'-'}} <br>
                    @if(($role = Sentinel::check()->roles[0]->slug=='owner') || (!empty($tom)))
                        <i data-toggle="modal" data-target="#exampleModal2"
                           class="fa fa-pencil float-right text-right"></i>
                    @endif
                    <br>


                    @if (!empty($burden->employeeGroup))
                        @foreach ($makers as $gr)

                            @foreach ($gr->employees as $user)
                                <small class="text-left">{{$user->full_name}}</small><br>
                            @endforeach
                        @endforeach
                    @endif

                    @php
                        $bakers=$burdenClass->getBakers($burden->baker);
                        $gg = \Carbon\Carbon::today();
                        $tob = \EndProductBurdenManager\Models\EndProductBurden::where('id','=',$burden->id)->where('bake_start_time', '>', $gg->subDays(2)->format('Y-m-d') . '%')->first();

                    @endphp
                    <hr class="m-1">
                    <b class="font-weight-bold">Second Stage</b><br>
                    -Start At : {{(!empty($burden->bake_start_time)) ?\Carbon\Carbon::parse($burden->bake_start_time)->format('d-m-Y H:m:s'):'-'}} <br>
                    -Work Group :{{!empty($burden->baker) && $bakers->count()>0 ?$bakers[0]->name:'-'}} <br>
                    @if(($role = Sentinel::check()->roles[0]->slug=='owner') || (!empty($tob)))
                        <i data-toggle="modal" data-target="#exampleModal"
                           class="fa fa-pencil float-right text-right"></i>
                    @endif
                    <br>

                    @if (!empty($burden->baker))
                        @foreach ($bakers as $gr)
                            @foreach ($gr->employees as $user)
                                <small class="text-left">{{$user->full_name}}</small><br>
                            @endforeach
                        @endforeach
                    @endif
                    <hr class="m-1">


                    @if (!empty($burden->baker()->get()) && sizeof($burden->baker()->get())>0)
                        @foreach($burden->baker()->get() as $baker)
                            <b class="font-weight-bold">Baker</b> {{$baker->first_name.' '.$baker->lastt_name}}
                            @if($role = Sentinel::check()->roles[0]->slug=='owner')
                                <i data-toggle="modal" data-target="#exampleModal"
                                   class="fa fa-pencil float-right text-right"></i>
                            @endif
                            <br>
                        @endforeach
                    @endif

                    <div class="py-3">


                        @if($burden->response==0)

                        {{'Current Responsibility : '}}-First Stage <br>
                            <b class="font-weight-bold">Change Responsible Stage

                                {!! Form::open(['route' => ['update-first-stage'], 'method' => 'post']) !!}
                                <input type="hidden" name="burden_id" value="{{$burden->id}}">
                                <button type="submit" class="btn btn-danger">To Second Stage</button>
                                {!! Form::close() !!}
                        @else
                        {{'Current Responsibility : '}}-Second Stage <br>
                            <b class="font-weight-bold">Change Responsible Stage
                                {!! Form::open(['route' => ['update-second-stage'], 'method' => 'post']) !!}
                            <input type="hidden" name="burden_id" value="{{$burden->id}}">
                            <button type="submit" class="btn btn-warning">To First Stage</button>
                            {!! Form::close() !!}
                        @endif



                    </div>
                </div>
            </div>
        </div>



    <div class="app-wrapper">

        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">Semi Finish Allocation</h5>
            </div>
            <table class="text-center">

                @php

                $totval = 0;

                $bd = 'burden';
                $cr = 'cooking_request';
                $allb = \BurdenManager\Models\AllocateSemiFinish::where('end_product_burden_id','=',$burden->id)->where('burden_type','=',$bd)->OrderBy('id')->get();
                $allc = \BurdenManager\Models\AllocateSemiFinish::where('end_product_burden_id','=',$burden->id)->where('burden_type','=',$cr)->OrderBy('id')->get();


                @endphp
                <tr><td>
                <h5 class="text">Semi Finish Name</h5>
                </td><td class="text-center">
                <h5 class="text mb-1">Burden Serial</h5>
                </td><td class="text-center">
                <h6 class="text mb-lg-1">Qty</h6>
                    </td><td class="text-right">
                        <h6 class="text mb-lg-1">Value</h6>
                    </td></tr>

                @foreach($allb as $burd)

                    @php
                        $b = \BurdenManager\Models\Burden::find($burd->semi_finish_burden_id);
                        $bname = \SemiFinishProductManager\Models\SemiFinishProduct::find($b->semi_finish_product_id);

                   $totval = $totval + (($burd->allocate_qty)*($b->value/$b->expected_semi_product_qty));
                    @endphp
                    <tr><td>
                            <h5 class="text">{{$bname->name}}</h5>
                        </td><td class="text-center">
                            <h5 class="text mb-1">{{$b->serial}}</h5>
                        </td><td class="text-center">
                            <h6 class="text mb-lg-1">{{number_format($burd->allocate_qty,3)}}</h6>
                        </td>
                        @if((Sentinel::check()->roles[0]->slug=='owner') || (Sentinel::check()->roles[0]->slug == 'assistant'))

                        <td class="text-right">
                            <h6 class="text mb-lg-1">{{number_format(($burd->allocate_qty)*($b->value/$b->expected_semi_product_qty),2)}}</h6>
                        </td>
                        @endif
                    </tr>
                @endforeach


                @foreach($allc as $burd)

                    @php
                        $b = \ShortEatsManager\Models\CookingRequest::find($burd->semi_finish_burden_id);
                        $bname = \SemiFinishProductManager\Models\SemiFinishProduct::find($b->semi_finish_product_id);

                    $totval = $totval + ($burd->allocate_qty)*($b->value/($b->expected_semi_product_qty));
                    @endphp
                    <tr><td>
                            <h5 class="text">{{$bname->name}}</h5>
                        </td><td class="text-center">
                            <h5 class="text mb-1">{{$b->serial}}</h5>
                        </td><td class="text-center">
                            <h6 class="text mb-lg-1">{{number_format($burd->allocate_qty,3)}}</h6>
                        </td>
                        @if((Sentinel::check()->roles[0]->slug=='owner') || (Sentinel::check()->roles[0]->slug == 'assistant'))

                        <td class="text-right">
                            <h6 class="text mb-lg-1">{{number_format(($burd->allocate_qty)*($b->value/($b->expected_semi_product_qty)),2)}}</h6>
                        </td>
                        @endif
                    </tr>
                @endforeach

            </table>
        </div>
    </div>

    <div class="app-wrapper">

        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">Raw Material Allocation</h5>
            </div>
            <table class="text-center">
                @php

                        $allraw = \App\RawInquiry::where('desc_id','=',4)->where('issued_ref_id','=',$burden->id)->get();
                        $allpack = \App\RawInquiry::where('desc_id','=',5)->where('issued_ref_id','=',$burden->id)->get();
                       $mid = \EndProductBurdenManager\Models\EndProductBurden::find($burden->id);

                        $ms = \EndProductBurdenManager\Models\EndProductBurdenHeader::find($mid->merge_request_id);

                @endphp
                <tr><td>
                        <h5 class="text">Raw Material Name</h5>

                    </td><td class="text-center">

                        <h6 class="text mb-lg-1">Merged Serial</h6>
                    </td><td class="text-center">

                        <h6 class="text mb-lg-1">Qty</h6>
                    </td><td class="text-right">

                        <h6 class="text mb-lg-1">Value</h6>
                    </td></tr>


                <tr><td>
                <hr>
                </td></tr>
                @foreach($allraw as $raw)

                    @php

                        $rawname = \RawMaterialManager\Models\RawMaterial::find($raw->raw_material_id);

                    $totval = $totval + ($raw->price * (-($raw->qty)));
                    @endphp
                    <tr><td>
                            <h5 class="text">{{$rawname->name}}</h5>
                        </td><td class="text-center">
                            @if(empty($ms))
                                <h6 class="text mb-lg-1"></h6>
                            @else
                                <h6 class="text mb-lg-1">{{$ms->serial}}</h6>
                            @endif
                        </td><td class="text-center">

                            <h6 class="text mb-lg-1">{{number_format(-($raw->qty),3)}}</h6>
                        </td>
                        @if((Sentinel::check()->roles[0]->slug=='owner') || (Sentinel::check()->roles[0]->slug == 'assistant'))

                        <td class="text-right">

                            <h6 class="text mb-lg-1">{{number_format((-($raw->qty) * $raw->price),2)}}</h6>
                        </td>
                        @endif
                    </tr>
                @endforeach
            </table>

        </div>
    </div>

            <div class="app-wrapper">

                <div class="app-main">
                    <div class="app-main-header">
                        <h5 class="app-main-heading text-center">Packing Item Allocation</h5>
                    </div>
                    <table class="text-center">

            <tr><td>
                    <h5 class="text">Packing Item Name</h5>
                </td><td class="text-center">

                    <h6 class="text mb-lg-1">Merged Serial</h6>
                </td><td class="text-center">

                    <h6 class="text mb-lg-1">Qty</h6>
                </td><td class="text-right">

                    <h6 class="text mb-lg-1">Value</h6>
                </td></tr>

            @foreach($allpack as $raw)

                @php

                    $rawname = \RawMaterialManager\Models\RawMaterial::find($raw->raw_material_id);

                    $totval = $totval + ($raw->price * -($raw->qty));
                @endphp
                <tr><td>
                        <h5 class="text">{{$rawname->name}}</h5>
                    </td><td class="text-center">
                        @if(!empty($ms))
                            <h6 class="text mb-lg-1">{{$ms->serial}}</h6>
                        @endif
                    </td><td class="text-center">

                        <h6 class="text mb-lg-1">{{number_format(-$raw->qty,3)}}</h6>
                    </td>
                    @if((Sentinel::check()->roles[0]->slug=='owner') || (Sentinel::check()->roles[0]->slug == 'assistant'))

                    <td class="text-right">

                        <h6 class="text mb-lg-1">{{number_format(($raw->price * -($raw->qty)),2)}}</h6>
                    </td>
                    @endif
                </tr>
                @endforeach


                </table>
        </div>
    </div>

    <div class="app-wrapper">

        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">Customized - Semi Finish</h5>
            </div>
            <table class="text-center">
                @php


                    $semiCusto = \App\SemiBurdenIssueData::where('end_product_burden_header_id','=',$burden->id)->where('type','=',2)->get();

                @endphp
                <tr><td>
                        <h5 class="text">Semi Finish Name</h5>
                    </td><td class="text-center">
                        <h5 class="text mb-1">Burden Serial</h5>
                    </td><td class="text-center">
                        <h6 class="text mb-lg-1">Qty</h6>
                    </td><td class="text-right">
                        <h6 class="text mb-lg-1">Value</h6>
                    </td></tr>

                @foreach($semiCusto as $scusto)

                    @php
                    $b = null;
                    $c = null;
                    if(!empty($scusto->end_product_id)){
                        $bname = \SemiFinishProductManager\Models\SemiFinishProduct::find($scusto->end_product_id);
                        if($bname->is_bake == 1){
                            $b = \BurdenManager\Models\Burden::find($scusto->reference);
                        }else{
                            $c = \ShortEatsManager\Models\CookingRequest::find($scusto->reference);
                        }
                    }
                    @endphp
            @if(!empty($scusto->end_product_id))
                @if($bname->is_bake == 1)
                    <tr><td>
                            <h5 class="text">{{$bname->name}}</h5>
                        </td><td class="text-center">
                            <h5 class="text mb-1">{{$b->serial}}</h5>
                        </td><td class="text-center">
                            <h6 class="text mb-lg-1">{{number_format($scusto->used_qty,3)}}</h6>
                        </td>
                        @if((Sentinel::check()->roles[0]->slug=='owner') || (Sentinel::check()->roles[0]->slug == 'assistant'))

                        <td class="text-right">
                            <h6 class="text mb-lg-1">{{number_format(($scusto->used_qty)*($b->value/$b->expected_semi_product_qty),2)}}</h6>
                        </td>
                        @endif
                    </tr>

                    @php
                        $totval = $totval + ($scusto->used_qty)*($b->value/$b->expected_semi_product_qty);
                    @endphp
                    @else

                        <tr><td>
                                <h5 class="text">{{$bname->name}}</h5>
                            </td><td class="text-center">
                                <h5 class="text mb-1">{{$c->serial}}</h5>
                            </td><td class="text-center">
                                <h6 class="text mb-lg-1">{{number_format($scusto->used_qty,3)}}</h6>
                            </td>
                            @if((Sentinel::check()->roles[0]->slug=='owner') || (Sentinel::check()->roles[0]->slug == 'assistant'))

                            <td class="text-right">
                                <h6 class="text mb-lg-1">{{number_format(($scusto->used_qty)*($c->value/($c->expected_semi_product_qty)),2)}}</h6>
                            </td>
                            @endif
                        </tr>
                    @php
                        $totval = $totval + ($scusto->used_qty)*($c->value/($c->expected_semi_product_qty));
                    @endphp

                    @endif
                    @endif
                @endforeach

            </table>
        </div>
    </div>

    <div class="app-wrapper">

        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">Customized - Raw Materials</h5>
            </div>
            <table class="text-center">
                @php

                    $rawCusto = \RawMaterialManager\Models\RawRequestTable::where('description','=',$burden->id)->where('type','=',2)->where('status','=',1)->get();



                @endphp
                <tr><td>
                        <h5 class="text">Raw Name</h5>
                    </td><td class="text-center">
                        <h5 class="text mb-1">Request ID</h5>
                    </td><td class="text-center">
                        <h6 class="text mb-lg-1">Qty</h6>
                    </td><td class="text-right">
                        <h6 class="text mb-lg-1">Value</h6>
                    </td></tr>

                @foreach($rawCusto as $rcusto)

                    @if(!empty($rcusto->ref))

                        @php
                        $rawprice = \App\RawInquiry::where('issued_ref_id','=',$rcusto->ref)->where('raw_material_id','=',$rcusto->raw_id)->first();
                       $rawnm = \RawMaterialManager\Models\RawMaterial::find($rcusto->raw_id);


                                $totval = $totval + ($rcusto->qty)*($rawnm->price/$rawnm->units_per_packs);
                        @endphp
                            <tr><td>
                                    <h5 class="text">{{$rawnm->name}}</h5>
                                </td><td class="text-center">
                                    <h5 class="text mb-1">{{$rcusto->ref}}</h5>
                                </td><td class="text-center">
                                    <h6 class="text mb-lg-1">{{number_format($rcusto->qty,3)}}</h6>
                                </td>
                                @if((Sentinel::check()->roles[0]->slug=='owner') || (Sentinel::check()->roles[0]->slug == 'assistant'))

                                <td class="text-right">
                                    <h6 class="text mb-lg-1">{{number_format(($rcusto->qty)*($rawnm->price/$rawnm->units_per_packs),2)}}</h6>
                                </td>
                                @endif
                            </tr>

                    @endif
                @endforeach


            </table>
        </div>
    </div>

    @if((Sentinel::check()->roles[0]->slug=='owner') || (Sentinel::check()->roles[0]->slug == 'assistant'))

        <div class="app-wrapper">

        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">Costing</h5>
            </div>
            <table class="text-center">
                <tr><td>
                        <h3 class="text"><b>Cost Of Burden</b></h3>
                    </td><td class="text-center">
                        <h5 class="text mb-1"></h5>
                    </td><td class="text-center">
                        <h6 class="text mb-lg-1"></h6>
                    </td><td class="text-right">
                        <h3 class="text mb-lg-1"><b>{{number_format($totval,2)}}</b></h3>
                    </td></tr>
                <tr><td>
                        <h3 class="text"><b>Cost Of Unit</b></h3>
                    </td><td class="text-center">
                        <h5 class="text mb-1"></h5>
                    </td><td class="text-center">
                        <h6 class="text mb-lg-1"></h6>
                    </td><td class="text-right">
                        <h3 class="text mb-lg-1"><b>{{number_format($totval/$burden->burden_size,2)}}</b></h3>
                    </td></tr>
                @php
                $upr = \EndProductManager\Models\EndProduct::find($burden->end_product_id);
                @endphp

                <tr><td>
                        <h3 class="text"><b>Current Selling Price Rs.</b></h3>
                    </td><td class="text-center">
                        <h5 class="text mb-1"></h5>
                    </td><td class="text-center">
                        <h6 class="text mb-lg-1"></h6>
                    </td><td class="text-right">
                        <h3 class="text mb-lg-1"><b>{{number_format($upr->selling_price,2)}}</b></h3>
                    </td></tr>
                <tr><td>
                        <h3 class="text"><b>Cost vs Current Price</b></h3>
                    </td><td class="text-center">
                        <h5 class="text mb-1"></h5>
                    </td><td class="text-center">
                        <h6 class="text mb-lg-1"></h6>
                    </td><td class="text-right">
                        <h3 class="text mb-lg-1"><b>{{number_format((($totval/$burden->burden_size)*100/($upr->selling_price)),2)}}%</b></h3>
                    </td></tr>


            </table>
        </div>
    </div>
        @endif

    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit First Stage</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => ['update-endgroup',$burden->id], 'method' => 'post']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            Employee Groups
                        </div>
                        <div class="col-md-6">
                            <select required name="employee_group" class="form-control">
                                @foreach($employee_groups as $group)
                                    <option value="{{$group->id}}">{{$group->employees->pluck('first_name')}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary">Save changes</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>


    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Second Stage</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => ['update-end_baker',$burden->id], 'method' => 'post']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            Baker
                        </div>
                        <div class="col-md-6">
                            {{--!! Form::select('baker', $bakers , null , ['class' => 'form-control','required']) !!--}}
                            <select required name="baker" class="form-control">
                                @foreach($baker as $group)
                                    <option value="{{$group->id}}">{{$group->employees->pluck('first_name')}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary">Save changes</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>


@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/waypoints/lib/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/waypoints/lib/shortcuts/sticky.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/counterup/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/js/jquery.sparkline.min.js')}}"></script>
    <script src="{{asset('js/site.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>


    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>

    <script>
        var ps = new PerfectScrollbar('#container');

    </script>
@stop
