@extends('layouts.back.master')@section('title','Crates Return')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }
    </style>
@stop

@section('content')
{{--    {{ Form::open(array('url' => '/esr/excel/data','id'=>'filter_form'))}}--}}
{{--    <div class="row">--}}
{{--        <div class="col-sm-2">--}}
{{--            <div class="form-group">--}}
{{--                {!! Form::text('date',null,['class' => 'form-control','id'=>'date_range','placeholder'=>'Select Date','id'=>'date_range']) !!}--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="col-md-2">--}}
{{--            {!! Form::select('agent',$agents , null , ['class' => 'form-control','required','placeholder'=>'Select Agent','id'=>'agent']) !!}--}}
{{--        </div>--}}

{{--        <div class="col-md-2">--}}
{{--            {!! Form::select('category',$categories , null , ['class' => 'form-control','required','placeholder'=>'Select Category','id'=>'category']) !!}--}}
{{--        </div>--}}

{{--        <div class="col-md-2">--}}
{{--            {!! Form::select('day', [1=>'Monday',2=>'Tuesday',3=>'Wednesday',4=>'Thursday',5=>'Friday',6=>'Saturday',7=>'Sunday',8=>'Long Weekend',9=>'Extra'] , null , ['class' => 'form-control','required','placeholder'=>'Select Day','id'=>'day']) !!}--}}
{{--        </div>--}}
{{--        <div class="col-md-2">--}}
{{--            {!! Form::select('time', [1=>'Morning',2=>'Noon',3=>'Evening'] , null , ['class' => 'form-control','required','placeholder'=>'Select Time','id'=>'time']) !!}--}}
{{--        </div>--}}

{{--        <div class="col-sm-2">--}}
{{--            <div class="form-group">--}}
{{--                <button type="button" class="btn btn-success" onclick="process_form()">Filter Now</button>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--    </div>--}}
{{--    {{ Form::close() }}--}}


    <div class="">
        <table id="esr_table" class="display text-center">
            <thead>
            <tr>
                <th>ID</th>
                <th>Date</th>
                <th>Agent</th>
                <th>Category</th>
                <th>Day - Time</th>
                <th>Invoice Number</th>
                <th>Action</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>ID</th>
                <th>Date</th>
                <th>Agent</th>
                <th>Category</th>
                <th>Day - Time</th>
                <th>Invoice Number</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
    </div>

@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script>
        // $('#date_range').daterangepicker({
        //     "showDropdowns": true,
        //     "timePicker": false,
        //     "singleDatePicker": false,
        //     "locale": {
        //         "format": "YYYY-MM-DD",
        //     },
        //     "minDate": "11/01/2019"
        // });
        //
        // $('#date_range').val('')

        $(document).ready(function () {
            table = $('#esr_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "ajax": {
                    url: "{{route('crates-return.table')}}",
                    type: "get",
                    error: function () {  // error handling code
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    }
                },
                pageLength: 25,
                responsive: true
            });
        });

        // function process_form() {
        //     let date_range = $("#date_range").val();
        //     let agent = $("#agent").val();
        //     let category = $("#category").val();
        //     let day = $("#day").val();
        //     let time = $("#time").val();
        //     let table = $('#esr_table').DataTable();
        //     table.ajax.url('/shop-item-issue/table/data?agent=' + agent + '&category=' + category + '&day=' + day + '&time=' + time + '&date_range=' + date_range + '&filter=' + true).load();
        //     // table.ajax.url('/loading/table/data?date_range=' + date_range + '&filter=' + true).load();
        //
        // }
        //
        // function process_form_reset() {
        //     $("#date_range").val('');
        //     // $("#status").val('');
        //     // $("#semi").val('');
        //     let table = $('#esr_table').DataTable();
        //     table.ajax.url('/esr/table/data').load();
        //
        // }


    </script>

@stop