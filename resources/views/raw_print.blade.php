<html>
<head>
    <link rel="stylesheet" href="{{asset('assets/vendor/css/bootstrap.css')}}">
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/invoice.css')}}" rel="stylesheet">
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <style media="all">
        @page {
            margin: 30px 5px 30px 15px;
            size: 75mm 999mm;
        }
    </style>
</head>

<body style="margin-bottom: 100px">

<div class="row">

    <div class="col-xs-5">
        <img src="{{asset('assets/img/buntak_grace.jpeg')}}" style="width: 95px">
    </div>

    <div class="col-xs-7 text-left">
        <h6>
            <b>Ranjanas Holdings [Pvt] Ltd</b><br>
            Katukurunda<br>
            Habaraduwa<br>
            Hotline : +9477 330 4678<br>
            E-mail : buntalksl@icloud.com<br>
        </h6>
    </div>
</div>
<div>
    <div class="row">
        <div class="col-xs-6 text-nowrap"><b>Merge Serial :</b></div>
        <div class="col-xs-6">{{$merge->serial}}</div>
    </div>

    <div class="row">
        <div class="col-xs-6 text-nowrap"><b>Date with Time :</b></div>
        <div class="col-xs-6">{{\Carbon\Carbon::now()->format('Y-m-d H:i:s')}}</div>
    </div>

</div>

<br>

<div class="row">
    <div class="col-xs-12">
        <table class="table">
            <thead>
            <tr>
                <th>Item Name</th>
                <th class="text-center">Volume</th>
                <th class="text-right">Checked</th>

            </tr>
            </thead>
            <tbody>
            @foreach($items as $item)

            @php  $rawMaterial = \RawMaterialManager\Models\RawMaterial::find($item->raw_material_id);  @endphp

                <tr>
                    <td><h4 id="{{$rawMaterial->raw_material_id}}">{{$rawMaterial->name}}/<font face="FMMalithi x"><b> {{$rawMaterial->sin_name}}</b></font> -[{{$rawMaterial->scale_key}}]</h4></td>

                    <td class="text-center"><h4>
                            @if($rawMaterial->measurement =='gram')
                                @if($item->qty > 999)
                                    {{($item->qty/1000)}}
                                @else
                                    {{($item->qty)}}
                                @endif

                            @elseif($rawMaterial->measurement =='milliliter')
                                @if($item->qty > 999)
                                    {{($item->qty/1000)}}
                                @else
                                    {{($item->qty)}}
                                @endif

                            @elseif($rawMaterial->measurement =='unit')
                                {{($item->qty)}}

                            @endif

                            @if($rawMaterial->measurement =='gram')
                                @if($item->qty > 999)
                                    kg
                                @else
                                    g
                                @endif

                            @elseif($rawMaterial->measurement =='milliliter')
                                @if($item->qty > 999)
                                    litre
                                @else
                                    milliliter
                                @endif

                            @elseif($rawMaterial->measurement =='unit')
                                units
                            @endif
                        </h4></td>
                    <td align="right">
                        <div style="width:20px;height:20px;border:1px solid #000;"></div>
                    </td>





                </tr>
            @endforeach

            </tbody>
        </table>
    </div>

</div>

<footer>


    <div class="row col-xs-12" style="margin-top: 60px;">

        <div class="text-center col-xs-12">
            <h5>
                .........................................<br>
                Signature-Stores Superviser
            </h5>
        </div>
    </div>
    <div class="row col-xs-12" style="margin-top: 60px;">
        <div class="text-center col-xs-12">
            <h5>
                .........................................<br>
                Signature-Manager
            </h5>
        </div>

    </div>
    <div class="row col-xs-12" style="margin-top: 60px;">
        <div class="text-center col-xs-12">
            <h5>
                .........................................<br>
                Name with Signature-Accepted By
            </h5>
        </div>

    </div>


</footer>
</body>

</html>
