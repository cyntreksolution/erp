<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">

    <link rel="stylesheet" href="{{asset('assets/examples/css/theme-customizer.css')}}">
    <script src="{{asset('assets/examples/js/theme-customizer.js')}}"></script><!-- core plugins -->
    <link rel="stylesheet" href="{{asset('assets/vendor/css/hamburgers.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.cs')}}s">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.css')}}">
    <!-- site-wide styles -->

    <link rel="stylesheet" href="{{asset('assets/vendor/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/site.css')}}"><!-- plugins for the current page -->
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/chartist/dist/chartist.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/jvectormap/jquery-jvectormap.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/owl-carousel/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/select2/css/select2.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/toastr/toastr.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/material-design-iconic-font/css/material-design-iconic-font.min.css')}}">

    <style>
        .select2-container {
            width: 100% !important;
        }

        .vertical-center {
            top: 50%;
        }
    </style>

    <script src="{{asset('assets/vendor/bower_components/breakpoints.js/dist/breakpoints.min.js')}}"></script>
    @yield('css')
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">--}}
    <script>Breakpoints({
            xs: {min: 0, max: 575},
            sm: {min: 576, max: 767},
            md: {min: 768, max: 991},
            lg: {min: 992, max: 1199},
            xl: {min: 1200, max: Infinity}
        });</script>
</head>

<body>

<div class="row">

    <div class="col-sm-12 col-md-5 col-lg-5 vertical-center p-5" style="margin-top: 10%">
        <a href="{{asset(env('APP_LOCATION'))}}" download class="text-center center-block -align-center">
            <img width="100%" class="text-center center-block -align-center"
                 src="{{asset('/assets/img/download-here.png')}}"></a>
        <h2 class="mt-3 p-5 text-center">Download And Install Now</h2>
    </div>
    <div class="col-sm-12 col-md-7 col-lg-7 vertical-center">
        <img src="{{asset('/assets/buntalk-app.PNG')}}">
    </div>
</div>
</body>

</html>