@extends('layouts.back.master')
@section('title','Sales Returns')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .hide {
            display: none;
        }

        .app-main-content {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .portlet-placeholder {
            border: 1px dotted black;
            margin: 0 1em 1em 0;
            height: 100%;
        }

        #projects-task-modal .modal-bg {
            padding: 50px;
        }

        #projects-task-modal .modal-content {
            box-shadow: none;
            border: none
        }

        #projects-task-modal .close {
            border: none;
            outline: none;
            box-shadow: none;
            position: absolute;
            right: 1rem;
            top: 1rem;
            width: 25px;
            height: 25px;
            line-height: 25px;
            text-align: center;
            font-size: 1.13rem;
            border-radius: 500px;
            border: 1px solid #868e96
        }

        #floating-buttongtfr {
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #db4437;
            position: fixed;
            top: 13rem;
            right: 90px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .plus {
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }


    </style>
@stop
@section('content')
    <div class="app-wrapper">

        <div class="app-main">
            {{ Form::open(array('url' => 'sales_return'))}}

            <div class="row p-3">
                <div class="col-md-6">
                    <h5 class="aa app-main-heading text-left"><span>NEW SALES RETURN</span></h5>
                </div>
                <div class="col-md-6 float-right">
                    {!! Form::select('time', [1=>'Morning',2=>'Noon',3=>'Evening'] , null , ['class' => 'form-control','required','placeholder'=>'Select Time']) !!}
                </div>
            </div>

            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">
                    <div class="container">
                        <input type="hidden" name="supplier_id" id="supplier_id">
                        <div class="projects-list">
                            <div class="row">
                                <div class="col-md-8 mt-1">
                                    {{--                                    <label for="end_product">End Product</label>--}}
                                    <select id="end_product" name="end_product_id"
                                            class="form-control select2 p-2 " ui-jp="select2"
                                            style="width: 100%"
                                            ui-options="{theme: 'bootstrap'}">
                                        <option value="0">Select End Product</option>
                                        @foreach($productsArray as $product)
                                            <option value="{{$product->id}}">{{$product->name}}</option>
                                        @endforeach

                                    </select>
                                </div>

                                <div class="col-md-2 p-2">
                                    <input class="form-control" id="qty" name="qty"
                                           placeholder="Quantity" type="text" style="width:90px" >
                                </div>
                                <div class="col-md-2">
                                    <div class="btn btn-outline-primary my-4" id="floating-button">
                                        add
                                    </div>
                                </div>

                            </div>
                            <hr>

                            <div class="abc">

                            </div>

                        </div>

                        <hr>

                        <div class="col-md-6 float-right text-right mb-3">
                            <div class="row">
                                <div class="col-md-6">
                                    <p>Total Value :</p>
                                </div>
                                <div class="col-md-6 text-right">
                                    <div class="col-md-12">
                                        <p id="total_value" class="text-right">0.00</p>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="total_value" id="hidden_total_value">

                        <button type="submit" class="btn btn-primary btn-lg btn-block mb-5">Create Return Request
                        </button>
                    </div>
                </div>
            </div>
            {!!Form::close()!!}
        </div>
    </div>

@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });


        $("#floating-button").click(function () {
            let end_product = $("#end_product option:selected").val();
            let qty = $('#qty').val();
            let type = $('#type').val();
            let type_text = $("#type option:selected").text();
            let total = parseFloat($('#total_value').text()).toFixed(2);


            if (end_product == 0 || qty == '') {
                Command: toastr["error"]("Select End Product and Quantity")
                return false;
            }


            $.ajax({
                type: "post",
                url: '../end_product/get/endpro/return',
                data: {id: end_product},
                success: function (response) {
                    let value = qty * response['selling_price'];
                    total = parseFloat(total) + parseFloat(value);
                    $(".abc").append('' +
                        '<div class=" card-body d-flex  align-items-center p-0" >' +
                        '<input type="hidden" name="end_product_ids[]" value=" ' + response['id'] + ' "/>' +
                        '<input type="hidden" name="unit_values[]" value=" ' + response['selling_price'] + ' "/>' +
                        '<input type="hidden" name="type[]" value=" ' + type + ' "/>' +
                        '<input type="hidden" name="qty[]" value=" ' + qty + ' "/>' +
                        '<div class="col-md-3 activity-counters justify-content-between">' +
                        '<div class="text-left">' +
                        '<div class="text-primary"><h6 >' + response['name'] + '</h6>' +
                        '</div>' +
                        '</div>' +
                        '</div> ' +
                        '<div class="col-md-3 activity-counters justify-content-between">' +
                        '<div class="text-left">' +
                        '<div class="text-primary"><h6 >' + type_text + '</h6>' +
                        '</div>' +
                        '</div>' +
                        '</div> ' +
                        '<div class="col-md-1 activity-counters justify-content-between">' +
                        '<div class="text-right">' +
                        '<div class="text-primary"><h6 >' + response['selling_price'] + '</h6>' +
                        '</div>' +
                        '</div>' +
                        '</div> ' +
                        '<div class="col-md-1 activity-counters justify-content-between">' +
                        '<div class="text-right">' +
                        '<div class="text-primary"><h6 >' + qty + '</h6>' +
                        '</div>' +
                        '</div>' +
                        '</div> ' +
                        '<div class="col-md-2 activity-counters justify-content-between">' +
                        '<div class="text-right">' +
                        '<div class="text-primary"><h6 >' + parseFloat(value).toFixed(2) + '</h6>' +
                        '</div>' +
                        '</div>' +
                        '</div> ' +
                        '<div class="col-md-1 activity-counters justify-content-between">' +
                        '<div class="text-right">' +
                        '<div class="text-primary"><i onclick="deleteItem(this,' + parseFloat(value).toFixed(2) + ')" class="fa fa-close"></i>' +
                        '</div>' +
                        '</div>' +
                        '</div> ' +
                        '</div>');
                    $("#total_value").html("");
                    document.getElementById("total_value").append(parseFloat(total).toFixed(2));
                    document.getElementById("hidden_total_value").value = parseFloat(total).toFixed(2);
                }
            });


            reset();
        });

        function reset() {
            document.getElementById("end_product").value = 0;
            // document.getElementById("type").value = 1;
            document.getElementById("qty").value = "";
        }

        function deleteItem(row, delval) {
            row.closest('.card-body').remove();
            let nowValue = parseFloat($('#total_value').text()).toFixed(2) - parseFloat(delval).toFixed(2);
            document.getElementById("total_value").innerHTML = parseFloat(nowValue).toFixed(2);
            document.getElementById("hidden_total_value").value = parseFloat(nowValue).toFixed(2)
        }


    </script>
@stop
