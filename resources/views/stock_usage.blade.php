@extends('layouts.back.master')@section('title','Stock usage')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/bootstrap-datepicker/bootstrap-datepicker3.css')}}"/>
    <style type="text/css">
        #floating-button {
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #00C851;
            position: fixed;
            bottom: 80px;
            right: 30px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .plus {
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }

        .sml {
            height: 30px;
            width: 50px;
            text-align: center;
            font-size: 0.75rem;
        }
    </style>
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
@stop
@section('content')
    {{ Form::open(array('url' => '/stock_usage/download','id'=>'filter_form'))}}
    <div class="row">
        <div class="col-sm-3">
            <div class="form-group">
                <select id="raw_material_id" name="raw_material_id" class="form-control" required>
                    <option value="" selected disabled>Select raw material</option>
                    <option value="all">All</option>
                    @foreach($raw_materials as $raw_material)
                        <option value="{{$raw_material->raw_material_id}}">{{$raw_material->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-sm-6 input-daterange">
            <div class="row">
                <div class="col-sm-6">
                    <input type="text" class="form-control" placeholder="Start date" name="start_date" id="start_date" required style="text-align: left;">
                </div>
                <div class="col-sm-6">
                    <input type="text" class="form-control" placeholder="End date" name="end_date" id="end_date" required style="text-align: left;">
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <button type="button" class="btn btn-success" onclick="process_form('#filter_form')">Filter data</button>
                <button type="submit" name="action" value="download_data" class="btn btn-info"><i class="fa fa-download"></i></button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
    <div class="">
        <table id="stock_usage_table" class="display text-center">
            <thead>
            <tr>
                <th>Raw material</th>
                <th>Date</th>
                <th>Out time</th>
                <th>Quantity</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Raw material</th>
                <th>Date</th>
                <th>Out time</th>
                <th>Quantity</th>
            </tr>
            </tfoot>
        </table>
    </div>
@stop
@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="{{asset('assets/vendor/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
    <script>

        var today = new Date();
        var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        $('.input-daterange').datepicker({
            format: "yyyy-mm-dd",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            endDate: "today",
            maxDate: date
        });

        var start_date = moment().subtract(1, 'days').format("YYYY-MM-DD");
        var end_date = moment().format("YYYY-MM-DD");
        $(document).ready(function () {

            table = $('#stock_usage_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                "ajax": {
                    url: "{{url('/stock_usage/datatable')}}", // json datasource
                    type: "get",
                    error: function () {  // error handling code
                        $("#stock_usage_processing").css("display", "none");
                    }
                },
                pageLength: 25,
                responsive: true
            });

            $("#filter_form").validate({});

        });

        function process_form(form_id) {
            if ($(form_id).valid()) {
                var vstart_date = $("#start_date").val();
                var vend_date = $("#end_date").val();
                var vraw_material_id = $("#raw_material_id").val();
                var table = $('#stock_usage_table').DataTable();
                table.ajax.url('stock_usage/get_new_rows?raw_material_id='+vraw_material_id+'&start_date='+vstart_date+'&end_date='+vend_date).load();
            }
        }

    </script>
@stop