@extends('layouts.back.master')@section('title','Order List')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }
    </style>
@stop

@section('content')

        <div class="row mb-2">

            <div class="col-md-2">
                {!! Form::select('agent',$agents , null , ['class' => 'form-control','placeholder'=>'Select Agent','id'=>'agent']) !!}
            </div>
            <div class="col-md-1">
                {!! Form::select('category',$categories , null , ['class' => 'form-control','placeholder'=>'Select Category','id'=>'category']) !!}
            </div>
            <div class="col-md-2">
                {!! Form::select('day', ['Monday'=>'Monday','Tuesday'=>'Tuesday','Wednesday'=>'Wednesday','Thursday'=>'Thursday','Friday'=>'Friday','Saturday'=>'Saturday','Sunday'=>'Sunday','Long Weekend'=>'Long Weekend','Extra'=>'Extra'] , null , ['class' => 'form-control','placeholder'=>'Select Day','id'=>'day']) !!}
            </div>
            <div class="col-md-1">
                {!! Form::select('time', ['Morning'=>'Morning','Noon'=>'Noon','Evening'=>'Evening'] , null , ['class' => 'form-control','placeholder'=>'Select Time','id'=>'time']) !!}
            </div>
            <div class="col-md-2">
                {!! Form::select('status', [1=>'Pending',2=>'Processing',3=>'Accepting',4=>'Approving',5=>'Approved'] , null , ['class' => 'form-control','placeholder'=>'Select Status','id'=>'status']) !!}
            </div>
            <div class="col-md-2">
                <input type="text" name="date" id="date_range" class="form-control" placeholder="Select Date">
            </div>

            <div class="col-md-2">
                <button class="btn btn-info " onclick="process_form(this)">Filter</button>
            </div>

            <!-- loading view -->


        </div>



        <div class="table-responsive">
            <table id="esr_table" class="display text-center ">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Date</th>
                    <th>Category</th>
                    <th>Day - Time</th>
                    <th>Status</th>
                    <th>Agent</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Date</th>
                    <th>Category</th>
                    <th>Day - Time</th>
                    <th>Status</th>
                    <th>Agent</th>
                    <th>Action</th>
                </tr>
                </tfoot>
            </table>
        </div>



        <div class="table-responsive">
            <table id="esr_table" class="display text-center ">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Date</th>
                    <th>Category</th>
                    <th>Day - Time</th>
                    <th>Action</th>
                    <th>Status</th>
                    <th>Agent</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Date</th>
                    <th>Category</th>
                    <th>Day - Time</th>
                    <th>Action</th>
                    <th>Status</th>
                    <th>Agent</th>
                </tr>
                </tfoot>
            </table>
        </div>

@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script>
        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019",
            "maxDate":moment()
        });


        $('#date_range').val('')
        $(document).ready(function () {
            table = $('#esr_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "ajax": {
                    url: "{{url('/order/list/table/data')}}",
                    type: "get",
                    error: function () {
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    }
                },
                pageLength: 100,
                responsive: true
            });
        });

        function process_form(e) {
            let category = $("#category").val();
            let day = $("#day").val();
            let time = $("#time").val();
            let status = $("#status").val();
            let agent = $("#agent").val();
            let table = $('#esr_table').DataTable();
            let date_range = $("#date_range").val();
            if (category != null && category != '') {
                $('#btn_sumbit').removeClass('d-none');
            } else {
                $('#btn_sumbit').addClass('d-none');
            }

            table.ajax.url('/order/list/table/data?category=' + category + '&date_range=' + date_range + '&day=' + day + '&status=' + status + '&agent=' + agent + '&time=' + time + '&filter=' + true).load();
        }


        function process_form_reset() {
            $("#category").val('');
            $("#day").val('');
            $("#time").val('');
            $("#status").val('');
            $("#agent").val('');
            let table = $('#esr_table').DataTable();
            table.ajax.url('order/list/table/data').load();
        }

    </script>

@stop
