@extends('layouts.back.master')@section('title','Debit Note | List')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }
    </style>
@stop

@section('content')
    {{ Form::open(array('url' => '/burden/excel/data','id'=>'filter_form'))}}
        <div class="row">

                @if(Auth::user()->hasRole(['Owner','Sales Agent','Super Admin']))
                <div class="form-group col-sm-2">
                    <input type="hidden" name="agent" id="agent" value="{{Auth::user()->id}}">

                </div>
            @else
            <div class="col-sm-2">
                <div class="form-group">
                    {!! Form::select('agent', $agents , null , ['class' => 'form-control','placeholder'=>'Agents','id'=>'agent']) !!}
                </div>
            </div>
            @endif

            <div class="col-sm-2">
                <div class="form-group">
                    {!! Form::select('type', [1=>'System Generated',2=>'Return'] , null , ['class' => 'form-control','placeholder'=>'Type','id'=>'type']) !!}
                </div>
            </div>
            <div class="col-sm-1">
                <div class="form-group">
                    {!! Form::select('time', ['Morning'=>'Morning','Noon'=>'Noon','Evening'=>'Evening'] , null , ['class' => 'form-control','placeholder'=>'Time','id'=>'time']) !!}
                </div>
            </div>
            <div class="col-sm-1">
                <div class="form-group">
                    {!! Form::select('status', [1=>'Pending',2=>'Accepting',3=>'Approving',4=>'Clearing',5=>'Settled'] , null , ['class' => 'form-control','placeholder'=>'Select Status','id'=>'status']) !!}
                </div>

            </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        {!! Form::select('desc1', [1=>'Distributor',2=>'Show Room',3=>'Direct Sale'] , null , ['class' => 'form-control','placeholder'=>'Select Type','id'=>'desc1']) !!}
                    </div>
                </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <input type="text" name="date_range" id="date_range" class="form-control">
                </div>
            </div>

            <div class="col-sm-2">
                <div class="form-group">
                    <button type="button" class="btn btn-success" onclick="process_form()">Filter Now</button>
                    <button type="button" class="btn btn-warning" onclick="process_form_reset()">Reset Filter</button>
                    <button type="submit" name="action" value="download_data" class="btn btn-info"><i
                                class="fa fa-download"></i></button>
                </div>
            </div>
        </div>


    {{ Form::close() }}
    <div class="">
        <table id="invoice_table" class="display text-center">
            <thead>
            <tr>
                <th>#</th>
                <th>Date</th>
                <th>Time</th>
                <th>Agent</th>
                <th>Total Return</th>
                <th>Sent Amount</th>
                <th>Return Discount</th>
                <th>Hold Amount</th>
                <th>Settled Amount</th>
                <th>Action</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>#</th>
                <th>Date</th>
                <th>Time</th>
                <th>Agent</th>
                <th>Total Return</th>
                <th>Sent Amount</th>
                <th>Return Discount</th>
                <th>Hold Amount</th>
                <th>Settled Amount</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
    </div>
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script>
        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019"
        });
        $('#date_range').val('')

        var total_sum;
        var sent_sum;
        var dis_sum;
        var hold_sum;
        var final_sum;
        $(document).ready(function () {
            table = $('#invoice_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "ajax": {
                    url: "{{url('/debit-note/list')}}",
                    type: "get",
                    error: function () {  // error handling code
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    },
                    dataSrc: function (data) {
                        total_sum = data.total_sum;
                        sent_sum = data.sent_sum;
                        dis_sum = data.dis_sum;
                        hold_sum = data.hold_sum;
                        final_sum = data.final_sum;
                        return data.data;
                    }
                },
                pageLength: 25,
                responsive: true,
                drawCallback: function (settings) {
                    var api = this.api();

                    $(api.column(4).footer()).html(
                        'Total Return : ' + total_sum
                    );
                    $(api.column(5).footer()).html(
                        'Sent Amount : ' + sent_sum
                    );
                    $(api.column(6).footer()).html(
                        'Return Discount : ' + dis_sum
                    );
                    $(api.column(7).footer()).html(
                        'Hold Amount : ' + hold_sum
                    );
                    $(api.column(8).footer()).html(
                        'Settled Amount : ' + final_sum
                    );
                }

            });
        });

        function process_form() {
            let date_range = $("#date_range").val();
            let agent = $("#agent").val();
            let status = $("#status").val();
            let type = $("#type").val();
            let desc1 = $("#desc1").val();
            let time = $("#time").val();
            let table = $('#invoice_table').DataTable();
            table.ajax.url('{{url('/debit-note/list')}}?agent=' + agent + '&status=' + status + '&date_range=' + date_range + '&type=' + type +'&time=' + time + '&desc1=' + desc1 + '&filter=' + true).load();

        }

        function process_form_reset() {
            $("#date_range").val('');
            $("#status").val('');
            $("#agent").val('');
            $("#type").val('');
            $("#desc1").val('');
            $("#time").val('');
            let table = $('#invoice_table').DataTable();
            table.ajax.url('/debit-note/list').load();

        }


    </script>
    <script>
        function deleteBurden(id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            swal({
                    title: "Are you sure ?",
                    text: "Do you want to Delete? This action cannot be recover !",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, Delete it!",
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: "post",
                            url: '../burden/delete/burden',
                            data: {burden_id: id},

                            success: function (response) {
                                if (response == 'true') {
                                    swal("Nice!", "You successfully Deleted the burning", "success");
                                    setTimeout(location.reload.bind(location), 900);
                                } else {
                                    swal("Error!", "Something went wrong", "error");
                                }
                            }
                        });


                        // });

                    } else {
                        swal("Cancelled", "You cancelled the finishing :)", "error");
                    }
                });


        }
    </script>
@stop