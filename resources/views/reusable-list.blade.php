@extends('layouts.back.master')@section('title','Reusable Stock')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">

    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }

        .abc {
            width: 1000px;
            height: 1000px;
            /* background-color: black; */
            z-index: 8000;

        }

        .sweet-alert {
            z-index: 7999 !important;
        }

        .sweet-overlay {
            z-index: 7998 !important;
        }

    </style>
@stop

@section('content')

    <div id="wait" class="d-none abc" style=""><img src='{{asset('assets/img/4V0b.gif')}}' width="64" height="64"/><br>Loading..
    </div>
    {{ Form::open(array('url' => 'reusable/stock/list/','id'=>'mergerForm','method'=>'GET'))}}
        <div class="app-main-content mb-2">
            <div class="row mx-2">
                <div class="col-md-2">
                    {!! Form::select('type', [2=>'Reusable Bun',3 =>'Reusable Bread',4=>'Reusable Cake'] , null , ['class' => 'form-control','placeholder'=>'Select Type']) !!}
                </div>
                <div class="col-md-2">
                    {!! Form::select('received_type', ['semi_grade'=>'Semi Burden Grade','end_grade'=>'End Burden Grade','debit_re_use'=>'Sales Return From Agents','stores_return'=>'Store Return','wastage'=>'Wastage','merge_reuse'=>'Store Return-BTS'] , null , ['class' => 'form-control','placeholder'=>'Select Received Type']) !!}
                </div>
                <div class="col-md-2">
                    <input type="text" name="date" id="date_range" class="form-control" placeholder="Select Date">
                </div>
                <div class="col-md-2">
                    {!! Form::select('status', ['pending'=>'Pending','used' =>'Used'] , null , ['class' => 'form-control']) !!}
                </div>
                <div class="col-md-2">
                    <button class="btn btn-info " type="submit">Filter</button>
                    <a class="btn btn-warning " href="/reusable/stock/list">Reset</a>
                </div>


             <input type="hidden" name="filter" value="true">
            </div>
        </div>
    {{ Form::close() }}
    {{ Form::open(array('url' => '/reusable/stock/recycle','id'=>'mergerForm','method'=>'POST'))}}
    <div class="">
        <table id="invoice_table" class="display text-center">
            <thead>
            <tr>
                <th>Created Date</th>
                <th>Type</th>
                <th>Received Type</th>
                <th>Name</th>
                <th>Reusable Type</th>
                <th>Weight</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>

            @php $total =0; @endphp
            @foreach($data as $key=> $item)
                <tr>
                    <td>{{\Carbon\Carbon::parse($item->created_at)->format('Y-m-d H:i:s')}}</td>
                    <td>{{$item->reference_type}}</td>
                    <td>{{$item->received_type}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->reusable_type_text}}</td>
                    <td>{{$item->expected_weight}}</td>
                    <td>{{$item->status}}
                        <input type="hidden" name="reusable_stock_id[]" value="{{$item->id}}">
                    </td>

                </tr>

                @php $total =$total + $item->expected_weight; @endphp
            @endforeach

            </tbody>
            <tfoot>
            <tr>

            <tr>
                <td>Total Weight</td>
                <td>(From 15 Minutes ago)</td>
                <td></td>
                <td></td>
                <td>{{$total}}</td>
                <td></td>

            </tr>
            </tfoot>
        </table>

        @if (\Illuminate\Support\Facades\Request::get('filter') == 'true' && \Illuminate\Support\Facades\Request::get('status') !='used' && \Illuminate\Support\Facades\Request::get('type') !=null)
            <div class="row">
                @if(Auth::user()->hasRole(['Stock Manager']))
                <div class="col-md-3 mt-5">
                    <input type="number" name="qty" min="0" max="{{$total}}" class="form-control" required>
                </div>
                <div class="col-md-3 mt-5">
                    <input type="text"  value="{{$total}}" name="system_qty" readonly  class="form-control">
                    <input type="hidden" name="received_type" value="{{$rt}}">
                </div>
                <div class="col-md-6 mt-5 ">
                    <button type="submit" name="submit" onclick="this.form.submit();this.disabled = true;"
                            class="btn btn-block btn-warning">Next
                    </button>
                </div>
                @endif

                <input type="hidden" name="reusable_type" value="{{\Illuminate\Support\Facades\Request::get('type')}}">
            </div>
        @endif


        {{ Form::close() }}
    </div>
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>

    <script>
        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019",
            "maxDate":moment()
        });



        $('#date_range').val('')
        $(document).ready(function () {
            table = $('#invoice_table').DataTable({
                searching: true,
                'columnDefs': [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                'select': {
                    'style': 'multi'
                },
                dom: 'Bfrtip',
                pageLength: 10000,
                responsive: true
            });
        });
    </script>

@stop
