@extends('layouts.back.master')@section('title','Sales Return')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}">


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .app-wrapper {
            font-family: Raleway, sans-serif;
            width: 100%;
            border: 1px solid #eee;
        }
    </style>
@stop


@section('current','Settle Debit Notes')
@section('current_url',route('admin.settle_notes'))




@section('content')
    <div class="app-wrapper">
        <div class="app-main">
            <div class="app-main-header">
                <div class="row">
                    <div class="col-md-6">
                        <h5 class=text-left">SALES RETURN - {{ $invoice->time}} - {{$invoice->debit_note_number}}</h5>
                        <h6 class="text-left">Collected By
                            : {{!empty($invoice->collector)?$invoice->collector->name_with_initials:'-'}}</h6>
                    </div>
                    <div class="col-md-6">
                        <h5 class="text-right">{{$invoice->agent->first_name.' '.$invoice->agent->last_name}}</h5>
                    </div>


                </div>
            </div>
            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">
                    <div class="container">
                        <div class="projects-list">

                            <div class="row">
                                <div class="col-md-2 text-nowrap">
                                    <h5 class="">End Product</h5>
                                </div>
                                <div class="col-md-2 text-right">
                                    <h5 class="">Unit Price</h5>
                                </div>
                                <div class="col-md-1 text-right">
                                    <h5 class="">QTY</h5>
                                </div>
                                <div class="col-md-1 text-right">
                                    <h5 class="">Total</h5>
                                </div>

                                <div class="col-md-2 text-right">
                                    <h5 class="">Commission</h5>
                                </div>
                                <div class="col-md-2 text-right">
                                    <h5 class="">Unit Discount</h5>
                                </div>
                                <div class="col-md-1 text-right">
                                    <h5 class="">Net Value</h5>
                                </div>

                            </div>
                            <hr class="m-1">

                            @php $tot = 0 ; $com = 0; $dis = 0; $net = 0;   @endphp
                            @foreach($items as $product)
                                @if ($product->qty != 0)
                                    <input type="hidden" name="product_id[]" value="{{$product->endProduct->id}}">

                                    <div class="row">
                                        <div class="col-md-2 text-nowrap">
                                            <h5 class="">{{$product->endProduct->name}}</h5>
                                        </div>
                                        <div class="col-md-2 text-right">
                                            <h5 class="">{{number_format((float)$product->unit_value, 2, '.', '')}}</h5>
                                        </div>
                                        <div class="col-md-1 text-right">
                                            <h5 class="">{{$product->qty}}</h5>
                                        </div>
                                        <div class="col-md-1 text-right">
                                            <h5 class="">{{number_format($product->total,2)}}</h5>
                                        </div>

                                        <div class="col-md-2 text-right">
                                            <h5 class="">{{number_format($product->commission,2)}}</h5>
                                        </div>
                                        <div class="col-md-2 text-right">
                                            <h5 class="">{{number_format($product->unit_discount,2)}}</h5>
                                        </div>
                                        <div class="col-md-1 text-right">
                                            <h5 class="">{{number_format($product->row_value,2)}}</h5>
                                        </div>
                                        <div class="col-md-1 text-right">
                                            <i class="fa fa-chevron-circle-down" data-toggle="collapse"
                                               href="#collapseExample{{$product->endProduct->id}}" role="button"
                                               aria-expanded="false"
                                               aria-controls="collapseExample{{$product->endProduct->id}}"></i>
                                        </div>
                                    </div>
                                    <div class="collapse" id="collapseExample{{$product->endProduct->id}}">
                                        <div class="card card-body">
                                            @php $products = $invoice->reUsableProducts()->get();
                                            $tot = $tot + $product->total;
                                            $com = $com + $product->commission;
                                            $dis = $dis + $product->unit_discount;
                                            $net = $net + $product->row_value;
                                            @endphp
                                            <div class="row">
                                                <div class="col-md-6"></div>
                                                <div class="col-md-3 text-nowrap text-left">Type</div>
                                                <div class="col-md-3 text-right">Qty</div>
                                            </div>
                                            @foreach($products  as $itemx)
                                                @if ($itemx->product_id == $product->end_product_id)
                                                    <div class="row">
                                                        <div class="col-md-6"></div>
                                                        <div class="col-md-3 text-nowrap text-left">{{ucwords(str_replace('_',' ',$itemx->type))}}</div>
                                                        <div class="col-md-3 text-right">{{$itemx->qty}}</div>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                    <hr class="m-1">
                               @endif

                            @endforeach
                        </div>
                        <div class="row">
                            <div class="col-md-2 text-nowrap">
                                <h5 class=""></h5>
                            </div>
                            <div class="col-md-2 text-right">
                                <h5 class=""></h5>
                            </div>
                            <div class="col-md-1 text-right">
                                <h5 class=""></h5>
                            </div>
                            <div class="col-md-1 text-right">
                                <h5 class="">{{number_format($tot,2)}}</h5>
                            </div>

                            <div class="col-md-2 text-right">
                                <h5 class="">{{number_format($com,2)}}</h5>
                            </div>
                            <div class="col-md-2 text-right">
                                <h5 class="">{{number_format($dis,2)}}</h5>
                            </div>
                            <div class="col-md-1 text-right">
                                <h5 class="">{{number_format($net,2)}}</h5>
                            </div>
                            <div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 text-right"></div>
                            <div class="col-md-9 text-right">
                                <h6 class="text-right">Return Discount Amount : {{number_format($invoice->return_discount,'2')}} </h6>

                                <h6 class="text-right">Hold Amount : {{number_format($invoice->hold_amount,'2')}} </h6>
                                <h6 class="text-right">Final Amount : {{number_format($invoice->final_amount,'2')}} </h6>

                                <h5 class="text-right">
                                    <button data-toggle="collapse" href="#collapseExample" role="button"
                                            aria-expanded="false" aria-controls="collapseExample"
                                            class="btn btn-sm align-content-end text-right btn-info">SETTLED BY
                                    </button>
                                </h5>
                                <div class="collapse" id="collapseExample">
                                    <div class="card card-body">
                                        @if (!empty($invoice->paid_invoices))
                                            <div class="row">
                                                <div class="col-md-3 text-nowrap">Settle Date</div>
                                                <div class="col-md-3 text-nowrap">Invoice Number</div>
{{--                                                <div class="col-md-3">Fully Paid</div>--}}
                                                <div class="col-md-3">Invoice Value</div>
                                                <div class="col-md-3">Settled Value</div>
                                            </div>
                                            @foreach(json_decode($invoice->paid_invoices)  as $data)
                                                <div class="row">
                                                    <div class="col-md-3 text-nowrap">{{\Carbon\Carbon::parse($invoice->updated_at)->format('Y-m-d H:i:s')}}</div>
                                                    <div class="col-md-3 text-nowrap">{{\App\LoadingHeader::find($data->loading_header_id)->invoice_number	}}</div>
{{--                                                    <div class="col-md-3">{{$data->is_full_payment}}</div>--}}
                                                    <div class="col-md-3">{{$data->invoice_value}}</div>
                                                    <div class="col-md-3">{{$data->settled_value}}</div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-12 mb-2">
                            <a class="btn btn-block btn-warning" href="{{ url()->previous() }}">Back</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

    </script>
@stop
