@extends('layouts.back.master')@section('title','Raw Inquiry List')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }
    </style>
@stop

@section('content')

        <div class="row mb-12">

            <div class="col-md-2">
                {!! Form::select('raw_material',$raw_materials , null , ['class' => 'form-control','placeholder'=>'Select Raw Material','id'=>'raw_material']) !!}
            </div>
{{--            <div class="col-md-3">--}}
{{--                <select name="type" class="form-control select2 ">--}}
{{--                    <option value="">Select Type</option>--}}
{{--                    <option value="">Issued For Make Semi Finish</option><!--buden & cooking_requests table[Description =serial $ burden name] & move_type=""-->--}}
{{--                    <option value="">Issued For Make End Product</option><!--raw_issue_data_lists table[data]-->--}}
{{--                    <option value="">Issued For Production Purpose</option><!--raw_issue_data_lists table[data]-->--}}
{{--                    <option value="">Issued For Make Customized Cake</option><!--raw_issue_data_lists table[data]-->--}}
{{--                    <option value="">Destroy & Return</option><!--raw_issue_data_lists table[data]-->--}}
{{--                    <option value="">Cleaning Purpose</option><!--raw_issue_data_lists table[data]-->--}}
{{--                    <option value="">Meals</option><!--raw_issue_data_lists table[data]-->--}}
{{--                    <option value="">Purchasing</option><!--purchasing_order_data table[purchasing_order_id & supplier name according purchasing_order_id]-->--}}

{{--                </select>--}}
{{--            </div>--}}

            <div class="col-md-2">
                 {!! Form::select('desc_id',[
                1=>'Purchased',
                2=>'Burden Request',
                3=>'Cooking Request',
                4=>'End Burden Request-Raw',
                5=>'End Burden Request-Packing',
                6=>'Meals',
                7=>'Production Purpose',
                8=>'Customized Cake',
                9=>'Destroy or Expire In Stores',
                10=>'Cleaning Purpose',
                11=>'Crates Purchasing',
                12=>'Issue Shop Item',
                13=>'Add Reusable Wastage',
              //  14=>'Creates issue with invoice',
              //  15=>'Approved Return Crates',
              //  16=>'Update Invoiced Crates',
                17=>'Measuring Variance',
                18=>'Employee Deduction',
                19=>'Return to Supplier',
                20=>'Return From End Burden Request',
               // 21=>'Admin Crates Adjustment',
                22=>'Reverse Raw Material',
                23=>'Tea Beverages & Extra Meals',
                24=>'Refill Deep Fryers',
                ], null , ['class' => 'form-control','placeholder'=>'Select Type','id'=>'desc_id']) !!}
            </div>
            <div class="col-md-2">
                <input type="text" name="date" id="date_range" class="form-control" placeholder="Select Date" required>
            </div>

            <div class="col-sm-2">
                <div class="form-group">
                    <select id="emp_id" name="emp_id" class="form-control" required>
                        <option value="">Select Employee</option>

                        @foreach($employees as $emp)

                            <option value="{{$emp->id}}">{{$emp->full_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-md-2">
                <button class="btn btn-info" onclick="process_form(this)">Filter</button>
            </div>
            <div class="col-md-2">
                <!--a onclick="process(this)" class="btn btn-danger" target="_blank">Multiple Entries</a-->

                <button class="btn btn-danger" onclick="process(this)">Multiple Entries</button>
            </div>

            <!-- loading view -->


        </div>

    <div class="table-responsive">
        <table id="rawi_table" class="display text-center ">
            <thead>
            <tr>
                <th width="5%">Opening Balance:</th>
                <th colspan="10" style="text-align: left"><div id="openning_balance"></div></th>
            </tr>
            <tr>

                <th>Date & Time</th>
                <th>Raw Material</th>
                <th>Movement Type & Description</th>
                <th>Start Balance</th>
                <th>Qty</th>
                <th>Amount</th>
                <th>Balance Qty</th>
                <th>Action</th>
            </tr>
            </thead>
            <tfoot>
            <tr>

                <th>Date & Time</th>
                <th>Raw Material</th>
                <th>Movement Type & Description</th>
                <th>Start Balance</th>
                <th>Qty</th>
                <th>Amount</th>
                <th>Balance Qty</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
    </div>

@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script>
        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019",
            "maxDate":moment()
        });


        $('#date_range').val('')
        var amount_sum;
        var qty_sum;
        var openning_balance;
        $(document).ready(function () {
            table = $('#rawi_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "ajax": {
                    url: "{{url('/raw-inquiry/list/table/data')}}",
                    type: "get",
                    error: function () {
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    },
                    dataSrc: function (data) {
                        amount_sum = data.amount_sum;
                        qty_sum = data.qty_sum;
                        openning_balance = data.openning_balance;
                        return data.data;
                    }
                },
                pageLength: 100,
                responsive: true,
                drawCallback: function (settings) {
                    var api = this.api();
                    $(api.column(4).footer()).html(
                        'qty : ' + qty_sum
                    );
                    $(api.column(5).footer()).html(
                        'amount : ' + amount_sum
                    );

                    $("#openning_balance").empty();
                    $('#openning_balance').append(openning_balance);
                }
            });
        });

        $(document).ready(function () {
            $("#emp_id").hide();

        });
        $("#desc_id").change(function () {
            let descid = $('#desc_id').val();
            let empid = $('#emp_id').val();


            if (descid != 6) {

                $("#emp_id").hide();

            }else {

                $("#emp_id").show();

                    $.ajax({
                        url: "/raw-inquiry/list/table/data" + raw_material + "/" + reference_type + "/" + date_range + "/" + emp_id,
                        type: "GET",
                        success: function (data) {
                            console.log(data);
                            appendRawList(data);
                        },
                        error: function (xhr, status) {

                        }
                    });

            }

        }).trigger("change");




        function process_form(e) {
            let raw_material = $("#raw_material").val();
            let table = $('#rawi_table').DataTable();
            let reference_type = $("#desc_id").val();
            let date_range = $("#date_range").val();
            let emp_id = $("#emp_id").val();

            table.ajax.url('/raw-inquiry/list/table/data?raw_material=' + raw_material + '&reference_type=' + reference_type + '&date_range=' + date_range + '&emp_id=' + emp_id +'&filter=' + true).load();
        }

        function process()
        {
            let raw_material = $("#raw_material").val();
            let table = $('#rawi_table').DataTable();
            let reference_type = $("#desc_id").val();
            let date_range = $("#date_range").val();
            let emp_id = $("#emp_id").val();

            table.ajax.url('/raw-inquiry/list/table/data_multiple?raw_material=' + raw_material + '&reference_type=' + reference_type + '&date_range=' + date_range + '&emp_id=' + emp_id +'&filter=' + true).load();

            //    let date_range = $("#date_range").val();

        //    window.location.href = "/raw-inquiry/list/table/data_multiple?date_range=" + date_range;
        }


        function process_form_reset() {
            $("#raw_material").val('');

            let table = $('#rawi_table').DataTable();
            table.ajax.url('raw-inquiry/list/table/data').load();
        }

    </script>

@stop
