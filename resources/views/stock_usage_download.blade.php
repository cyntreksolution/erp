<html>
<head>
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/invoice.css')}}" rel="stylesheet">
</head>
<body>
<div class="row">
    <h4 class="inv">
        STOCK USAGE
    </h4>
</div>
<div class="row">
    <table>
        <tr>
            <th>Raw material</th>
            <th>Date</th>
            <th>Out time</th>
            <th>Quantity</th>
        </tr>
        @foreach($records as $record)
            <tr>
                <td>{{$record->name}}</td>
                <td>{{$record->created_at}}</td>
                <td>{{$record->created_at}}</td>
                <td>{{$record->qty}}</td>
            </tr>
        @endforeach
    </table>
</div>
<footer>
    <div class="row" style="margin-top: 20px;">
        <h4 align="center">
            <h6 class="text-muted">powered by cyntrek<br>
                <h7 class="num">+9471 122 5489 / +9471 532 2262</h7>
            </h6>
        </h4>

    </div>
</footer>
</body>

</html>