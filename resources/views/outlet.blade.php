
@extends('layouts.back.master')@section('title','Cash Book')
@section('css')

    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/modal.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.wizard.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.validation.css')}}">
    <link rel="stylesheet" href="{{asset('assets/packages/raw_materials/raw_material.css')}}">

    <style>


    </style>
@stop
{{--@section('current','Cash Book')--}}
{{--@section('current_url',route('raw_materials.create'))--}}

@section('content')
    <div class="app-wrapper">
        <div class="app-panel" id="app-panel">
            <div class="app-search"><input type="search" class="search-field" placeholder="Search">
                <i class="search-icon fa fa-search"></i></div>
            <div class="app-panel-inner">
                <div class="scroll-container">
                    <div class="p-3">
                        @can('outlet-create')
                        <button class="btn btn-success py-3 btn-block btn-lg" data-toggle="modal"
                                data-target="#inflow">Add Outlet
                        </button>
                        @endcan
                    </div>

                    <hr class="m-0">
                    <div class="people-list d-flex justify-content-start flex-wrap p-3">
                        {{--<button class="avatar-sm avatar-circle fz-base font-weight-bold add-people-btn mx-1"--}}
                        {{--data-toggle="modal" data-target="#supplier">--}}
                        {{--<i data-toggle="tooltip"--}}
                        {{--data-placement="bottom" title="New Supplier" class="zmdi zmdi-plus"></i>--}}
                        {{--</button>--}}

                        {{--<a href="stock\status">--}}
                        {{--<button class="avatar-sm avatar-circle fz-base font-weight-bold add-people-btn mx-1">--}}
                        {{--<i data-toggle="tooltip" data-placement="bottom" title="Stock Status"--}}
                        {{--class="zmdi zmdi-view-list"></i>--}}
                        {{--</button>--}}
                        {{--</a>--}}

                    </div>
                    <hr class="m-0">
                    <div class="media-list" id="container2">
                        <a>
                            <div class="media content2">


                                <div class="media-body">
                                    <h6 class="media-heading my-1">
                                        <h6></h6>
                                    </h6>
                                </div>
                            </div>
                        </a>


                    </div>
                </div>
                <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show">
                    <i class="fa fa-chevron-right"></i> <i class="fa fa-chevron-left"></i></a></div>
        </div>
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">Outlet Details</h5>
            </div>

            <div class="app-main-content px-3 py-4" id="container">
                <div class="content ">
                    <div class="contacts-list">
                        <div class="media-list">
                            @foreach($datas as $t)

                                <div class="media">
                                    <div class="avatar avatar-sm bg-warning">+
                                    </div>
                                    <div class="media-body">
                                        <h6 class="media-heading" id="media-list-item-3">{{$t->name}}</h6>
                                        <small>{{$t->address}}</small>
                                    </div>
                                <h6 class="media-heading">{{$t->owner_tp}}</h6>
                                </div>

                            @endforeach

                        </div>


                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="inflow" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'outlet/add','enctype'=>'multipart/form-data'))}}

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="task-name-wrap">
                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                        <input class="task-name-field" type="text" name="oname"
                               placeholder="Outlet Name">
                    </div>
                    <hr>
                    <div class="task-name-wrap">
                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                        <input class="task-name-field" type="text" name="address"
                               placeholder="Address">
                    </div>
                    <hr>
                    </br>
                    <select name="otype" required
                            class="form-control select2 ">

                        <option value="">Select Outlet Type</option>
                        <option value="Agents">Own Outlet</option>
                        <option value="Vehicle">Rented Outlet</option>


                    </select>

                    <hr>
                    <div class="task-name-wrap">
                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                        <input class="task-name-field" type="text" name="keymoney"
                               placeholder="Key Money">
                    </div>

                    <hr>
                    <div class="task-name-wrap">
                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                        <input class="task-name-field" type="text" name="rent_period"
                               placeholder="Rent Period">
                    </div>

                    <hr>
                    <div class="task-name-wrap">
                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                        <input class="task-name-field" type="text" name="open_date"
                               placeholder="First Started Date">
                    </div>
                    <hr><h6>Rental Remark</h6>
                {!! Form::textarea('remark',null, array('class'=>'form-control',
                'rows' => 10, 'cols' => 40))  !!}


                </div>
                <div class="modal-footer">

                    <input type="submit" id="finish-btn" class="finish btn btn-block btn-warning "
                           value="Submit"/>

                </div>
                {!!Form::close()!!}
            </div>
        </div>
        </div-->

        @stop
        @section('js')
            <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
            <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
            <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
            <script src="{{asset('assets/examples/js/demos/form.wizard.js')}}"></script>
            <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
            <script src="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.js')}}"></script>
            <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
            <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
            <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/validate.min.js')}}"></script>
            <script src="{{asset('assets/packages/raw_materials/raw_material.js')}}"></script>

@stop