<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::post('logout', 'App\Http\Controllers\Auth\LoginController@logout')->name('logout');

Route::post('login', 'App\Http\Controllers\Auth\LoginController@login')->name('login');
Route::get('login', 'UserManager\Controllers\UserController@loginView')->name('user.login');

Route::middleware(['auth'])->group(function () {
    Route::get('dsuper', function () {
        return view('welcome');
    })->name('dashboard.superadministrator');
});

Route::middleware(['auth'])->group(function () {
    Route::get('downer', function () {
        return view('owner');
    })->name('dashboard.owner');
});

Route::middleware(['auth'])->group(function () {
    Route::get('dassisatant', function () {
        return view('assistant');
    })->name('dashboard.assistant');
});

Route::middleware(['auth'])->group(function () {
    Route::get('dpm', function () {
        return view('pm');
    })->name('dashboard.production-manager');
});

Route::middleware(['auth'])->group(function () {
    Route::get('dsm', function () {
        return view('sm');
    })->name('dashboard.stock-manager');
});

Route::middleware(['auth'])->group(function () {
    Route::get('dsr', function () {
        return view('sr');
    })->name('dashboard.sales-rep-manager');
});

Route::middleware(['auth'])->group(function () {
    Route::get('dsalesm', function () {
        return view('salesm');
    })->name('dashboard.sales-head-manager');
});
Route::middleware(['auth'])->group(function () {
    Route::get('dp', function () {
        return view('dpack');
    })->name('dashboard.packing');
});

Route::middleware(['auth'])->group(function () {
    Route::get('dhr', function () {
        return view('dhr');
    })->name('dashboard.human-resource');
});
Route::middleware(['auth'])->group(function () {
    Route::get('daccountant', function () {
        return view('daccount');
    })->name('dashboard.accountant');
});

Route::middleware(['auth'])->group(function () {
    Route::get('daccountant', function () {
        return view('daccount');
    })->name('dashboard.accountant');
});

Route::middleware(['auth'])->group(function () {
    Route::get('end', function () {
        return view('end');
    })->name('dashboard.end');
});


Route::middleware(['auth'])->group(function () {
    Route::get('/', 'UserManager\Controllers\UserController@home')->name('home');
});


Route::get('/a', function () {
    $credentials = [
        'email' => 'cyntrek',
        'password' => 'cyntrek2017',
    ];

    $user = Sentinel::registerAndActivate($credentials);
});

Route::get('/b', function () {
    $role = Sentinel::getRoleRepository()->createModel()->create([
        'name' => 'Super Administrator',
        'slug' => 'superadministrator',
    ]);
});

Route::get('/c', function () {
    $role = Sentinel::findRoleById(1);

    $role->permissions = [
        'admin' => true,
    ];

    $role->save();
});

Route::get('/d', function () {
    $user = Sentinel::findById(1);

    $role = Sentinel::findRoleByName('Super Administrator');

    $role->users()->attach($user);
});


Route::get('/p', function () {
    $credentials = [
        'email' => 'user',
        'password' => '123',
    ];

    $user = Sentinel::registerAndActivate($credentials);
});

Route::get('/q', function () {
    $role = Sentinel::getRoleRepository()->createModel()->create([
        'name' => 'Super Administrator',
        'slug' => 'superadministrator',
    ]);
});

Route::get('/r', function () {
    $role = Sentinel::findRoleById(1);

    $role->permissions = [
        'back' => true,
        'admin' => true,
    ];

    $role->save();
});

Route::get('/s', function () {
    $user = Sentinel::findById(1);

    $role = Sentinel::findRoleByName('Sales');

    $role->users()->attach($user);
});



Route::group(['middleware' => ['auth','web']], function () {
    Route::get('loading/invoice/list', 'App\Http\Controllers\LoadingInvoiceController@index')->name('loading.invoice');

    Route::resource('roles','App\Http\Controllers\RoleController')->middleware('role_or_permission:Super Admin');
    Route::get('roles/table/data', 'App\Http\Controllers\RoleController@tableData')->name('roles.table')->middleware('role_or_permission:Super Admin');
    Route::post('roles/details', 'App\Http\Controllers\RoleController@details')->name('roles.details')->middleware('role_or_permission:Super Admin');

    Route::resource('permissions','App\Http\Controllers\PermissionController')->middleware('role_or_permission:Super Admin');
    Route::get('permissions/table/data', 'App\Http\Controllers\PermissionController@tableData')->name('permissions.table')->middleware('role_or_permission:Super Admin');

    Route::resource('permission-groups','App\Http\Controllers\PermissionGroupController')->middleware('role_or_permission:Super Admin');
    Route::get('permission-groups/table/data', 'App\Http\Controllers\PermissionGroupController@tableData')->name('permission-groups.table')->middleware('role_or_permission:Super Admin');

    Route::resource('users','App\Http\Controllers\UserController')->middleware('role_or_permission:Super Admin');
    Route::get('users/table/data', 'App\Http\Controllers\UserController@tableData')->name('users.table')->middleware('role_or_permission:Super Admin');
    Route::get('user/otpstates', 'App\Http\Controllers\UserController@otpstatus');
    Route::post('user/otp', 'App\Http\Controllers\UserController@otp');

    Route::get('loading/invoice/list', 'App\Http\Controllers\LoadingInvoiceController@index')->name('loading.invoice')->middleware('role_or_permission:Super Admin');

    Route::post('merged_sales_requests/downloads', 'SalesRequestManager\Controllers\SalesRequestController@download_production_order');

    Route::get('stock_usage/datatable', 'App\Http\Controllers\StockUsageController@datatable');
    Route::get('stock_usage/get_new_rows', 'App\Http\Controllers\StockUsageController@get_new_rows');
    Route::post('stock_usage/download', 'App\Http\Controllers\StockUsageController@download');
    Route::resource('stock_usage', 'App\Http\Controllers\StockUsageController');

    Route::get('buffer_stock_summary/datatable', 'App\Http\Controllers\BufferStockSummaryController@datatable');
    Route::post('buffer_stock_summary/download', 'App\Http\Controllers\BufferStockSummaryController@download');
    Route::resource('buffer_stock_summary', 'App\Http\Controllers\BufferStockSummaryController');

    Route::resource('sales_return', 'App\Http\Controllers\SalesReturnController');
    Route::get('sales_return/send/{return_id}', 'App\Http\Controllers\SalesReturnController@sendReturn')->name('sales_return.send');
    Route::post('semi-issue/reusable/wastage', 'App\Http\Controllers\SalesReturnController@wastageSemiIssue')->name('wastage.send');

    Route::get('admin/sales_return', 'App\Http\Controllers\SalesReturnController@adminIndex')->name('admin.return');
    Route::get('admin/sales_return/{sales_return_header}', 'App\Http\Controllers\SalesReturnController@adminShow')->name('admin.return_show');
    Route::post('admin/sales/return/accept', 'App\Http\Controllers\SalesReturnController@accept')->name('admin.sales_return_accept');
    Route::post('admin/sales/return/accepted', 'App\Http\Controllers\SalesReturnController@accepted')->name('admin.sales_return_accepted');
    Route::post('admin/sales/return/print', 'App\Http\Controllers\SalesReturnController@print')->name('admin.sales_return_print');
    Route::get('admin/sales/return/print/{sales_return_header}', 'App\Http\Controllers\SalesReturnController@printGet')->name('admin.sales_return_print_get');
    Route::get('admin/sales/return/print/collect/{sales_return_header}', 'App\Http\Controllers\SalesReturnController@printToCollect')->name('admin.returnCollect');
    Route::post('admin/sales_return/delete/{return_id}', 'App\Http\Controllers\SalesReturnController@adminDestroy')->name('sales_return.admin-destroy');

    //settleNotes
    Route::get('admin/settle_notes/table', 'App\Http\Controllers\SalesReturnController@settleDebitNotes')->name('admin.settle_notes');
    Route::get('admin/settle_notes/table/data', 'App\Http\Controllers\SalesReturnController@settleDebitNotesTableData');
    Route::get('admin/settle_notes/approve/show/{sales_return}', 'App\Http\Controllers\SalesReturnController@settleDebitNoteApproveView')->name('return_note.approve');
    Route::post('admin/settle_notes/approve', 'App\Http\Controllers\SalesReturnController@settleDebitNoteApprove');
    Route::get('admin/settle_notes/payment/show/{sales_return}', 'App\Http\Controllers\SalesReturnController@settleDebitNotePaymentView');
    Route::get('admin/settle_notes/payment/returndis/{sales_return}', 'App\Http\Controllers\SalesReturnController@returnDiscount')->name('return.discount');
    Route::post('admin/settle_notes/payment/process/{sales_return}', 'App\Http\Controllers\SalesReturnController@processPayment')->name('settleNote-payment.process');

    Route::get('supplier_wise_materials/datatable', 'App\Http\Controllers\SupplierWiseMaterialController@datatable');
    Route::get('supplier_wise_materials/get_new_rows', 'App\Http\Controllers\SupplierWiseMaterialController@get_new_rows');
    Route::post('supplier_wise_materials/download', 'App\Http\Controllers\SupplierWiseMaterialController@download');
    Route::resource('supplier_wise_materials', 'App\Http\Controllers\SupplierWiseMaterialController');

    Route::get('debit-note', 'App\Http\Controllers\DebitNoteController@index')->name('debit-note.index');
    Route::get('debit-note/list', 'App\Http\Controllers\DebitNoteController@tableData')->name('debit-note.list');
    Route::post('debit-note/filter', 'App\Http\Controllers\DebitNoteController@exportFilter')->name('debit-note.export');

    Route::resource('buffer-stock/agents', 'App\Http\Controllers\AgentsBufferStockController');
    Route::get('buffer-stock/{buffer_stock}/agents/edit/request', 'App\Http\Controllers\AgentsBufferStockController@agentRequest')->name('buffer-stock.agent-request');
    Route::post('buffer-stock/{buffer_stock}/agents/edit/request', 'App\Http\Controllers\AgentsBufferStockController@updateRequest')->name('buffer-stock.agent-request');
    Route::post('buffer-stock/{buffer_stock}/agents/approve/request', 'App\Http\Controllers\AgentsBufferStockController@approvePending')->name('buffer-stock.agent-request-approve');

    Route::get('buffer-stock/{buffer_stock}/agents/edit/pending', 'App\Http\Controllers\AgentsBufferStockController@showPending')->name('buffer-stock.agent-request-update');
    Route::post('buffer-stock/{buffer_stock}/agents/edit/pending', 'App\Http\Controllers\AgentsBufferStockController@updatePending')->name('buffer-stock.agent-request-update');

    Route::get('buffer-stock/agents/table/data', 'App\Http\Controllers\AgentsBufferStockController@tableData')->name('buffer-stock.list');
    Route::get('buffer-stock/agents/pending/table/data/view', 'App\Http\Controllers\AgentsBufferStockController@tableDataView')->name('buffer-stock.list-view');
    Route::get('buffer-stock/agents/pending/table/data', 'App\Http\Controllers\AgentsBufferStockController@pendingTableData')->name('buffer-stock.pending-list');
    Route::get('buffer-stock/agents/filter', 'App\Http\Controllers\AgentsBufferStockController@exportFilter')->name('buffer-stock.export');

    Route::resource('pending/sales-request', 'App\Http\Controllers\PendingSalesRequestController');
    Route::resource('sales-request/approve-list', 'App\Http\Controllers\ApproveSalesRequestController');
    Route::get('invoice/correction/{loadingid}', 'LoadingManager\Controllers\LoadingController@inv_correction')->name('sales-wrong.show');
    Route::get('sales/return', 'LoadingManager\Controllers\LoadingController@salesVsRerurn')->name('sales-return.list');

    Route::get('sales-request/approve-list/preview/{orderId}', array('as' => 'approve-list.preview', 'uses' => 'App\Http\Controllers\ApproveSalesRequestController@previewList'));

    Route::get('pending/cratesListrequest', array('as' => 'crates-request.cratesRequestList', 'uses' => 'App\Http\Controllers\PendingSalesRequestController@cratesRequestList'));
    Route::get('pending/cratesShowrequest/{loadingid}', array('as' => 'crates-request.cratesRequestShow', 'uses' => 'App\Http\Controllers\PendingSalesRequestController@cratesRequestShow'));
    Route::resource('pending/cratesPending', 'App\Http\Controllers\PendingCratesController');
    //add new by nadee
    Route::get('pending/cratesEditrequest/{loadingid}', array('as' => 'crates-request.cratesRequestEdit', 'uses' => 'App\Http\Controllers\PendingSalesRequestController@cratesRequestEdit'));
    Route::get('pending/cratesPending', array('as' => 'crates-return.allcrates', 'uses' => 'App\Http\Controllers\PendingCratesController@index'));
    Route::patch('pending/cratesPending/{id}', array('as' => 'crates-return.updatecrates', 'uses' => 'App\Http\Controllers\PendingCratesController@update'));


    Route::get('crates/edit/{id}', array('as' => 'crates-edit.admin', 'uses' => 'App\Http\Controllers\PendingCratesController@editCrates'));
    Route::post('crates/edit/{id}', array('as' => 'crates-edit.admin', 'uses' => 'App\Http\Controllers\PendingCratesController@updateCrates'));


    Route::get('pending/cratesShowPending/{showId}', array('as' => 'cratesShowPending.cratesShowRequest', 'uses' => 'App\Http\Controllers\PendingCratesController@cratesShowRequest'));
    Route::get('pending/cratesCreateRequest', array('as' => 'crates-request.cratesRequestCreate', 'uses' => 'App\Http\Controllers\PendingSalesRequestController@cratesRequestCreate'));
    Route::post('pending/cratesStoreRequest', array('as' => 'crates-request.cratesStoreRequest', 'uses' => 'App\Http\Controllers\PendingSalesRequestController@cratesStoreRequest'));

    Route::resource('crates', 'App\Http\Controllers\CrateController');
    Route::get('crate-inquire/data/{agentId}', 'App\Http\Controllers\CrateController@crateInquireData')->name('crate-inquire.data');


    Route::post('variants/{endProduct}', 'App\Http\Controllers\VariantController@store')->name('variants.store');
    Route::get('variants/{variant}/destroy', 'App\Http\Controllers\VariantController@destroy')->name('variants.destroy');
    Route::post('variant/change/{id}/return', 'App\Http\Controllers\VariantController@changeReturn')->name('variants.return');

    //driver code generate paths
    Route::resource('driverCodeGenerate', 'App\Http\Controllers\DriverCodeGenerateController');
    Route::get('driverCodeGenerate/create', array('as' => 'driverCodeGenerate.create', 'uses' => 'App\Http\Controllers\DriverCodeGenerateController@create'));

    Route::resource('issue', 'App\Http\Controllers\RawMaterialIssueController');
    Route::resource('semi-issue', 'App\Http\Controllers\SemiIssueController');

    Route::get('issue/merged/burden/{burden}', 'App\Http\Controllers\RawMaterialIssueController@issueForBurden');
    Route::get('issue/semi/merged/burden/{burden}', 'App\Http\Controllers\SemiIssueController@issueForBurden');


    Route::get('esr', 'App\Http\Controllers\EndProductStockReport@index')->name('esr.index');
    Route::get('esr/table/data', 'App\Http\Controllers\EndProductStockReport@tableData')->name('esr.list');
    Route::post('esr/filter', 'App\Http\Controllers\EndProductStockReport@exportFilter')->name('esr.export');
    Route::get('/end-inquiry/list/{id}/{date}', 'App\Http\Controllers\EndInquiryListController@show')->name('end-inquiry.show');
    Route::get('/end-inquiry/new/reverse/{id}', 'App\Http\Controllers\EndInquiryListController@revers')->name('add-store.revers');

    Route::get('/end-inquiry/list/table/data/{id}/{datefiil}', 'App\Http\Controllers\EndInquiryListController@tableData');
    Route::get('/end-inquiry-yes/list/table/data/{id}/{datefiil}', 'App\Http\Controllers\EndInquiryListController@tableDatayes');
    Route::get('/end-inquiry-tomorrow/list/table/data/{id}/{datefiil}', 'App\Http\Controllers\EndInquiryListController@tableDatatomorrow');


    Route::get('/end-inquiry-new/list/table/data/{id}/{date}', 'App\Http\Controllers\EndInquiryListController@tableDataX');
    Route::get('/end-inquiry-new/vs/table/data/{id}/{date}', 'App\Http\Controllers\EndInquiryListController@vs');


    Route::get('bulk-burden', 'App\Http\Controllers\BulkBurdenCreateController@index')->name('bulk_burden.index');
    Route::post('bulk-burden', 'App\Http\Controllers\BulkBurdenCreateController@createNow')->name('bulk_burden.store');
    Route::get('bulk-burden/table/data', 'App\Http\Controllers\BulkBurdenCreateController@tableData')->name('bulk_burden.list');

    Route::get('bulk-burden-loading', 'App\Http\Controllers\ToLoadingController@index')->name('bulk_burden_loading.index');
    Route::get('bulk-burden-loading/table/data', 'App\Http\Controllers\ToLoadingController@tableData')->name('bulk_burden_loading.list');
    Route::post('bulk-burden-loading', 'App\Http\Controllers\ToLoadingController@createNow')->name('bulk_burden_loading.store');


    Route::get('bulk-burden-bake', 'App\Http\Controllers\BulkBurdenBakeController@index')->name('bulk_burden_bake.index');
    Route::get('burden-grade', 'App\Http\Controllers\BulkBurdenBakeController@gradeIndex')->name('burden_grade.index');
    Route::post('bulk-burden-bake', 'App\Http\Controllers\BulkBurdenBakeController@createNow')->name('bulk_burden_bake.store');
    Route::get('bulk-burden-bake/table/data', 'App\Http\Controllers\BulkBurdenBakeController@tableData')->name('bulk_burden_bake.list');
    Route::get('burden-grade/table/data', 'App\Http\Controllers\BulkBurdenBakeController@GradeTableData')->name('bulk_burden_grade.list');

    Route::get('semi-bulk-burden', 'App\Http\Controllers\BulkSemiBurdenCreateController@index')->name('semi_bulk_burden.index');
    Route::get('semi-bulk-burden/table/data', 'App\Http\Controllers\BulkSemiBurdenCreateController@tableData')->name('semi_bulk_burden.list');
    Route::post('semi-bulk-burden', 'App\Http\Controllers\BulkSemiBurdenCreateController@createNow')->name('semi_bulk_burden.store');

    Route::get('semi-bulk-burden-bake', 'App\Http\Controllers\BulkSemiBurdenBakeController@index')->name('semi_bulk_burden_bake.index');
    Route::get('semi-bulk-burden-bake/table/data', 'App\Http\Controllers\BulkSemiBurdenBakeController@tableData')->name('semi_bulk_burden_bake.list');
    Route::post('semi-bulk-burden-bake', 'App\Http\Controllers\BulkSemiBurdenBakeController@createNow')->name('semi_bulk_burden_bake.store');


    Route::get('short-bulk-burden', 'App\Http\Controllers\BulkShortBurdenCreateController@index')->name('short_bulk_burden.index');
    Route::get('short-bulk-burden/table/data', 'App\Http\Controllers\BulkShortBurdenCreateController@tableData')->name('short_bulk_burden.list');
    Route::post('short-bulk-burden', 'App\Http\Controllers\BulkShortBurdenCreateController@createNow')->name('short_bulk_burden.store');

    Route::get('short-bulk-burden-bake', 'App\Http\Controllers\BulkShortBurdenBakeController@index')->name('short_bulk_burden_bake.index');
    Route::get('short-bulk-burden-bake/table/data', 'App\Http\Controllers\BulkShortBurdenBakeController@tableData')->name('short_bulk_burden_bake.list');
    Route::post('short-bulk-burden-bake', 'App\Http\Controllers\BulkShortBurdenBakeController@createNow')->name('short_bulk_burden_bake.store');


    Route::resource('agent-payment', 'App\Http\Controllers\AgentPaymentController');
    Route::get('agent-payment2', 'App\Http\Controllers\AgentPaymentController@index2')->name('a.g');
    Route::get('agent-payment/table/data/preview/{id}', 'App\Http\Controllers\AgentPaymentController@payPreview')->name('pay-list.preview');

    Route::get('agent-payment/table/data', 'App\Http\Controllers\AgentPaymentController@tableData')->name('agent-payment.table');
    Route::get('agent-payment/table/data2', 'App\Http\Controllers\AgentPaymentController@tableData2')->name('agent-payment.table');
    Route::get('agent-payment/{id}/approve', 'App\Http\Controllers\AgentPaymentController@approvePayment')->name('agent-payment.approve');
    Route::post('agent-payment/{id}/process', 'App\Http\Controllers\AgentPaymentController@processPayment')->name('agent-payment.process');
    Route::get('agent-payment/{id}/remove', 'App\Http\Controllers\AgentPaymentController@removePayment')->name('agent-payment.remove');
    Route::get('agent-payment-print/{id}', 'App\Http\Controllers\AgentPaymentController@slipPrint')->name('payment.print');
    Route::get('agent-payment-accept/{id}', 'App\Http\Controllers\AgentPaymentController@accept')->name('payment.accept');


    //agentInquire
    Route::resource('agent-inquire', 'App\Http\Controllers\AgentInquireController');
    Route::get('agent-inquire/data/{agentId}', 'App\Http\Controllers\AgentInquireController@agentInquireData')->name('agent-inquire.data');
    Route::get('agent-inquire/print/{agentId}', 'App\Http\Controllers\AgentInquireController@agentInquirePrint')->name('agent-inquire.print');
    Route::get('agent-inquiries', 'App\Http\Controllers\AgentInquireController@index2')->name('agents.inquiries');
    Route::post('agent-inquiries/render/data', 'App\Http\Controllers\AgentInquireController@renderData')->name('agents.render');
    Route::get('agent-inquiries/download/{agent}/{date}', 'App\Http\Controllers\AgentInquireController@agentInquireDownload')->name('agent-inquiries.download');


    //cratesInquire
    Route::resource('crates-inquire', 'App\Http\Controllers\CratesInquireController');
    Route::get('crates-inquire/data/{agentId}', 'App\Http\Controllers\CratesInquireController@cratesInquireData')->name('crates-inquire.data');
    Route::get('crates-inquire/print/{agentId}', 'App\Http\Controllers\CratesInquireController@cratesInquirePrint')->name('crates-inquire.print');
    Route::get('crates-inquire/print/invoice/{agentId}', 'App\Http\Controllers\CratesInquireController@cratesInquirePrintInvoice')->name('crates-inquire.print-invoice');

    Route::get('crate-inquiries', 'App\Http\Controllers\CratesInquireController@indexnew')->name('crates.indexnew');
    Route::post('crate/inquiries/render/data', 'App\Http\Controllers\CratesInquireController@renderData')->name('crates.render');
    Route::get('crate/inquiries/download/{agent}/{date}', 'App\Http\Controllers\CratesInquireController@crateInquireDownload')->name('crate-inquiries.download');


    Route::get('/order/{loading_header}/preview', 'App\Http\Controllers\OrderListController@orderPreview')->name('order-list.preview');
    Route::get('/order/{loading_header}/preview2', 'App\Http\Controllers\OrderListController@orderPreview2')->name('order-list.preview2');
    Route::get('/order/list/table', 'App\Http\Controllers\OrderListController@index')->name('order.list');
    //Route::get('/order/list/table', 'App\Http\Controllers\OrderListController@index')->name('inquiry.list');

    Route::get('/order/list/table/data', 'App\Http\Controllers\OrderListController@tableData');
    Route::get('/order/list/{id}', 'App\Http\Controllers\OrderListController@editPendingRequest')->name('pendingOrder.edit');
    Route::get('/order/list/show/{id}', 'App\Http\Controllers\OrderListController@showPendingRequest')->name('pendingOrder.show');
    Route::post('/order/update/{id}', 'App\Http\Controllers\OrderListController@updatePendingRequest')->name('pendingOrder.update');
    Route::post('/order/replace/{id}', 'App\Http\Controllers\OrderListController@replacePendingRequest')->name('pendingOrder.replace');

    Route::resource('shop-item-issue', 'App\Http\Controllers\ShopItemIssueController');
    Route::post('shop-item-issue/{loading_data_raw}/load', 'App\Http\Controllers\ShopItemIssueController@issueRawMaterial');
    Route::get('shop-item-issue/table/data', 'App\Http\Controllers\ShopItemIssueController@tableData')->name('shop-item-issue.table');

    Route::get('loading/table/revers', 'LoadingManager\Controllers\LoadingController@revers')->name('loading.revers');

    Route::get('loading/table/data', 'LoadingManager\Controllers\LoadingController@tableData')->name('loading.table');
    Route::get('loading/table/view', 'LoadingManager\Controllers\LoadingController@showTable')->name('loading.stable');
    Route::get('sales/return/data', 'LoadingManager\Controllers\LoadingController@SalesReturnData')->name('loading.sales-return-data');



    Route::post('loading/with-discount/generate/bill', 'LoadingManager\Controllers\LoadingController@withDiscount')->name('loading.discount');
    Route::post('loading/with-discount/generate/pdf', 'LoadingManager\Controllers\LoadingController@printDiscount')->name('loading.dpdf');
    Route::get('generate/{id}/pdf', 'LoadingManager\Controllers\LoadingController@printDiscount')->name('loading.gpdf');

    Route::resource('crates-return', 'App\Http\Controllers\CratesReturnController');
    Route::get('crates-return/table/data', 'App\Http\Controllers\CratesReturnController@tableData')->name('crates-return.table');

    Route::resource('end-product-merge-list', 'App\Http\Controllers\EndProductBurdenMergeController');
    Route::get('end-product-merge-list/table/data', 'App\Http\Controllers\EndProductBurdenMergeController@tableData')->name('crates-return.table');
    Route::delete('end-product-burden/delete}', 'App\Http\Controllers\EndProductBurdenMergeController@deleteBurden')->name('end-b.delete');


    Route::get('agent/outstanding', 'App\Http\Controllers\AgentOutstandingController@index')->name('outstanding.index');
    Route::get('agent/outstanding/table/data', 'App\Http\Controllers\AgentOutstandingController@tableData')->name('outstanding.table');
    Route::get('agent/pay/{id}/force', 'App\Http\Controllers\AgentOutstandingController@forcePay')->name('outstanding.force');

    Route::post('reports/mgvsl', 'App\Http\Controllers\MergeVsLoadedController@getItems')->name('mgvsl.items');


    Route::get('reports/end-product/movements', 'App\Http\Controllers\EndProductMovementReport@index')->name('reports.end-move');
    Route::get('/reports/end-product/movements/table/data', 'App\Http\Controllers\EndProductMovementReport@tableData')->name('reports.end-move-table');
    Route::get('/reports/end-product/movements/table/sales/data', 'App\Http\Controllers\EndProductMovementReport@tableDataSales')->name('reports.end-move-table-sales');
    Route::get('/reports/end-product/movements/table/return/data', 'App\Http\Controllers\EndProductMovementReport@tableDataReturns')->name('reports.end-move-table-return');


    Route::resource('numbers', 'App\Http\Controllers\NumberController');
    Route::post('numbers/assign/user', 'App\Http\Controllers\NumberController@assign')->name('numbers.assign');
    Route::get('/delete/user/number/{id}', 'App\Http\Controllers\NumberController@deleteUserNumber')->name('numbers.delete');
    Route::get('/delete/number/{id}', 'App\Http\Controllers\NumberController@deleteNumber')->name('number.delete');
    Route::get('/edit/number/{id}', 'App\Http\Controllers\NumberController@updateNumber')->name('number.update');
    Route::post('/change/user/number/{id}', 'App\Http\Controllers\NumberController@changeStatus')->name('number.status');


    Route::get('supplier/outstanding/data', 'App\Http\Controllers\SupplierOutstandingController@index')->name('supplier-outstanding.index');
    Route::get('supplier/outstanding/table/data', 'App\Http\Controllers\SupplierOutstandingController@tableData')->name('supplier-outstanding.table');


    Route::get('cash/inquiry', 'App\Http\Controllers\CashBookInquiryController@index')->name('cash-inquiry.index');
    Route::get('end/product/history', 'App\Http\Controllers\EndProductBurdenMergeController@endProductBurdenList')->name('end-product.history');


    Route::get('view-history/{burden}', 'App\Http\Controllers\EndProductBurdenMergeController@show_end_product')->name('end-burden.show');


    Route::get('end/product/history/table', 'App\Http\Controllers\EndProductBurdenMergeController@endProductHistoryData')->name('end-product.history-data');
    Route::get('cash/inquiry/table/data', 'App\Http\Controllers\CashBookInquiryController@tableData')->name('cash-inquiry.table');
    Route::get('cash/inquiry/table/summery', 'App\Http\Controllers\CashBookInquiryController@tableSum')->name('cash-inquiry.summery');

    Route::get('raw/request', 'App\Http\Controllers\RawMaterialIssueController@rawIndex')->name('raw-request.index');
    Route::get('raw/request/store', 'App\Http\Controllers\RawMaterialIssueController@rawStore')->name('raw-request.store');
    Route::get('raw/issue/store', 'App\Http\Controllers\RawMaterialIssueController@rawIssue')->name('raw-issue.stores');
    Route::get('raw/request/print/{ref}', 'App\Http\Controllers\RawMaterialIssueController@print')->name('ppcc.show');


//============================================= ProductionController =============================================
    Route::get('production', 'App\Http\Controllers\ProductionController@index')->name('production.index');
    Route::get('production/sales/request', 'App\Http\Controllers\ProductionController@salesRequestTableData')->name('production.sales');

    Route::post('production/sr/cal/pro', 'App\Http\Controllers\ProductionController@mergeSalesRequests')->name('production.create');
    Route::post('production/sr/cal/pro/to/burden', 'App\Http\Controllers\ProductionController@salesRequestsToBurden')->name('production.burden');

    Route::get('production/merged/request', 'App\Http\Controllers\ProductionController@mergedSalesRequests')->name('production.merged');
    Route::get('production/sr/end/burden/list', 'App\Http\Controllers\ProductionController@endBurdenList')->name('production.burdens');

    Route::post('production/burden/merge', 'App\Http\Controllers\ProductionController@mergeBurdens')->name('pburden.merge');
    Route::get('production/issue/semi/{id}', 'App\Http\Controllers\ProductionController@issueSemi')->name('issue.semi');

    Route::Post('production/make/semi/{id}', 'App\Http\Controllers\ProductionController@makeSemiBurden')->name('make.semi');
    Route::Post('production/make/cooking/{id}', 'App\Http\Controllers\ProductionController@makeSemiBurdenCooking')->name('make.semi');
    Route::Post('production/create/semi/{id}', 'App\Http\Controllers\ProductionController@createSemiBurden')->name('create.semi');
    Route::Post('production/create/cooking/{id}', 'App\Http\Controllers\ProductionController@createSemiBurdenCooking')->name('create.semi');


    Route::Post('production/semi/issue/store', 'App\Http\Controllers\ProductionController@semiIssue')->name('create.semi');
    Route::get('production/sales/order/list/{id}', 'App\Http\Controllers\ProductionController@salesLoadingList')->name('pro.loading');

    Route::get('sr/merge/list', 'App\Http\Controllers\ProductionController@srMergeList')->name('sr.list');
    Route::get('ep/merge/list', 'App\Http\Controllers\ProductionController@epMergeList')->name('ep.list');


    Route::get('merged/sales/requests/{id}', 'App\Http\Controllers\ProductionController@mergedSalesRequestList')->name('merged.list');
    Route::get('merged/sales/delete/{id}', 'App\Http\Controllers\ProductionController@mergedSalesHeaderDelete')->name('merged.delete');

    Route::get('bulk/burden/make/merged/{id}', 'App\Http\Controllers\ProductionController@semiBulk')->name('semi.bulk');
    Route::get('bulk/burden/end/merged/{id}', 'App\Http\Controllers\ProductionController@endBulk')->name('end.bulk');

    Route::post('bulk/production/burden/store', 'App\Http\Controllers\ProductionController@bulkBurdenStage')->name('production_burden.store');
    Route::post('bulk/production/cooking/store', 'App\Http\Controllers\ProductionController@bulkCookingStage')->name('production_cook.store');
    Route::post('bulk/production/end/store', 'App\Http\Controllers\ProductionController@bulkEndStage')->name('production_end.store');


    Route::get('production/semi/diff/issue/{id}', 'App\Http\Controllers\ProductionController@differenceIssue')->name('production.diff');
    Route::get('production/adjust/semi/{id}', 'App\Http\Controllers\ProductionController@adjustSemi')->name('adjust.semi');
    Route::Post('production/adjust/semi/issue/store', 'App\Http\Controllers\ProductionController@adjustSemiBalance')->name('adjust.semiB');

    Route::get('production/vs/accept/{id}', 'App\Http\Controllers\ProductionController@ProductionVsAccept')->name('production.vs');
    Route::post('production/vs/accept', 'App\Http\Controllers\ProductionController@productionAdjust')->name('production.vss');
    Route::get('production/currant/balance/{id}', 'App\Http\Controllers\ProductionController@endProductCurrantBalance')->name('production.currantB');
    Route::post('production/currant/balance/update', 'App\Http\Controllers\ProductionController@endProductCurrantBalanceUpdate')->name('production.cuBal');


    Route::get('production/approve/admin/{id}', 'App\Http\Controllers\ProductionController@adminApprove')->name('production.approve');


    Route::get('agent/sales/return/', 'App\Http\Controllers\AgentSalesReturnController@index')->name('agent.return');
    Route::post('agent/sales/return/submit', 'App\Http\Controllers\AgentSalesReturnController@agentReturn')->name('agent.submit');


    Route::get('end/product/remove', 'App\Http\Controllers\EndProductRemoveController@index')->name('end.remove');
    Route::Post('end/product/removed', 'App\Http\Controllers\EndProductRemoveController@endProductCurrantBalanceUpdate')->name('end.removed');

    Route::get('reusable/stock/list', 'App\Http\Controllers\ReusableStockController@index')->name('reusable.index');
    Route::post('reusable/stock/recycle', 'App\Http\Controllers\ReusableStockController@store')->name('reusable.store');


    Route::get('raw/mat', 'RawMaterialManager\Controllers\RawMaterialController@index')->name('raw_materials.proindex');
    Route::get('manager/approval', 'RawMaterialManager\Controllers\RawMaterialController@managerApp')->name('manager.approve');


});


Route::get('approvePrice', 'PurchasingOrderManager\Controllers\PurchasingOrderController@approvePrice')->name('po.approve-price');

Route::get('salary/create/details/{employee?}/{date?}', 'App\Http\Controllers\SalaryHeaderController@index')->name('salary-comis.index');
Route::get('salCalc', 'SalaryManager\Controllers\SalaryController@index')->name('salary.index');
Route::get('salary/list/table/data', 'App\Http\Controllers\SalaryHeaderController@tableData')->name('salary.data');

Route::get('salary/make/commission/{attendance}', 'App\Http\Controllers\SalaryHeaderController@makeCommission')->name('make.commission');
Route::post('salary/destroy/commission', 'App\Http\Controllers\SalaryHeaderController@destroyCommission')->name('destroy.commission');

//Route::get('/saveDesc', 'App\Http\Controllers\CashLockerController@descshow')->name('cash_locker.mdesc');
Route::get('/desc', 'CashLockerManager\Controllers\CashLockerController@descshow')->name('cash_locker.mdesc');
Route::get('/salary', 'SalaryManager\Controllers\AllowanceController@index')->name('salwages.index');


Route::get('/loan', 'CashLockerManager\Controllers\CashLockerController@loanview')->name('cash_locker.loan');
Route::get('/utilitybill', 'App\Http\Controllers\UtilitybillController@show')->name('utility.bill');
Route::post('/utility/bill', 'App\Http\Controllers\UtilitybillController@create')->name('utility.new');
Route::get('/outlet', 'App\Http\Controllers\OutletController@show')->name('outlet.add');
Route::post('/outlet/add', 'App\Http\Controllers\OutletController@create');
Route::post('/outlet/update', 'App\Http\Controllers\OutletController@outupdate');

Route::get('/sales/comparison', 'App\Http\Controllers\StockUsageController@comparison')->name('sales-return.comparison');
Route::get('/sales/comparison/sales/return', 'App\Http\Controllers\StockUsageController@comTable');

Route::get('/shop_item', 'App\Http\Controllers\StockUsageController@shopitem')->name('shopitemlist.shopitem');
Route::get('/shopitems/list/table/data', 'App\Http\Controllers\StockUsageController@tableData');
Route::get('/crates_usage', 'App\Http\Controllers\StockUsageController@crates')->name('crate_usage.index');
//Route::get('/stock_usage', 'App\Http\Controllers\StockUsageController@list')->name('stock_usage.index');
Route::get('/stock_usage', 'App\Http\Controllers\RawInquiryListController@index')->name('stock_usage.index');
Route::get('/raw-inquiry/list/table/data', 'App\Http\Controllers\RawInquiryListController@tableData');
Route::get('/raw-inquiry/list/table/data_multiple', 'App\Http\Controllers\RawInquiryListController@multipleData');
Route::get('raw/request/revers', 'App\Http\Controllers\RawInquiryListController@reverse')->name('issued-raw.revers');



//Route::get('/usage','SemiFinishProductManager\Controllers\SemiFinishProductController@usage')->name('semi_finish_product.usage');
Route::get('/semilist', 'App\Http\Controllers\SemiInquiryListController@index')->name('semi_finish_product.list');
Route::get('/semi-inquiry/list/table/data', 'App\Http\Controllers\SemiInquiryListController@tableData');

Route::get('/supplier-inquiry', 'App\Http\Controllers\SupplierInquiryListController@index')->name('supplier.list');
Route::get('/supplier-inquiry/list/table/data', 'App\Http\Controllers\SupplierInquiryListController@tableData');

Route::get('/crates-inquiry-stores', 'App\Http\Controllers\CratesInquiryStoreController@index')->name('creates-stores.list');
Route::get('/crates-inquiry-stores/list/table/data', 'App\Http\Controllers\CratesInquiryStoreController@tableData');

Route::get('/crates-inquiry-agent', 'App\Http\Controllers\CratesInquiryAgentController@index')->name('creates-agent.list');
Route::get('/crates-inquiry-agent/list/table/data', 'App\Http\Controllers\CratesInquiryAgentController@tableData');
Route::get('/crates-inquiry-agent/print/download', 'App\Http\Controllers\CratesInquiryAgentController@cratesInquirePrint')->name('crates-inquire-ag.print');
Route::get('/crates-inquiry-agent/list/table/print', 'App\Http\Controllers\CratesInquiryAgentController@cratesInquirePrintInvoice');

Route::get('/equip', 'CashLockerManager\Controllers\CashLockerController@equipview')->name('cash_locker.equip');
Route::post('/loan', 'CashLockerManager\Controllers\CashLockerController@loan');
Route::post('/equip', 'CashLockerManager\Controllers\CashLockerController@equip');

Route::get('create', 'BurdenManager\Controllers\BurdenController@create')->name('burden.create');
Route::get('view/history', 'BurdenManager\Controllers\BurdenController@history')->name('burden.history');
Route::get('view/history/{burden}', 'BurdenManager\Controllers\BurdenController@historyView')->name('burden.historyView');
//Route::get('', 'ProductionOrderManager\Controllers\ProductionOrderController@index')->name('proOrder.index');
Route::post('', 'ProductionOrderManager\Controllers\ProductionOrderController@store')->name('proOrder.store');

Route::get('/merge/end/purchasing/items/{merge}', 'App\Http\Controllers\RawMaterialIssueController@mergeRaw')->name('merge.raw');


Route::get('debug', 'App\Http\Controllers\DebugController@index');
Route::get('loading-to-api', 'App\Http\Controllers\DebugController@loadingToAPI');
Route::post('day/commis/store', 'App\Http\Controllers\DayCommisDataController@store')->name('daycommis.store');


Route::get('/get/semi/burden/size/{semi}', 'App\Http\Controllers\ProductionController@getBurdenSizes');
Route::post('end_product/update_group/{id}', 'App\Http\Controllers\EndProductBurdenMergeController@updateEndGroup')->name('update-endgroup');
Route::post('end_product/update_baker/{id}', 'App\Http\Controllers\EndProductBurdenMergeController@updateEndBaker')->name('update-end_baker');
Route::post('update_stage1', 'App\Http\Controllers\EndProductBurdenMergeController@sstage')->name('update-first-stage');
Route::post('update_stage2', 'App\Http\Controllers\EndProductBurdenMergeController@fstage')->name('update-second-stage');

Route::get('db-backup', 'App\Http\Controllers\DataBaseBackupController@index')->name('db-backup.index');
Route::get('db-backup/create', 'App\Http\Controllers\DataBaseBackupController@create')->name('db-backup.create');
Route::get('shedule/update', 'App\Http\Controllers\DataBaseBackupController@shedule_update')->name('shedule.update');
Route::post('db-backup/delete', 'App\Http\Controllers\DataBaseBackupController@delete')->name('db-backup.delete');
Route::get('db-backup/dbBackup/request', 'App\Http\Controllers\DataBaseBackupController@dbBackupTableData')->name('db-backup.dbBackup');
Route::get('db-backup/download/file/{id}', 'App\Http\Controllers\DataBaseBackupController@downloadFile')->name('db.download');

Route::get('/burden-payments', 'SemiFinishProductManager\Controllers\SemiFinishProductController@burdenPayDetails')->name('semi_finish_product.payburden');
Route::get('/burden-payments/create', 'SemiFinishProductManager\Controllers\SemiFinishProductController@burdenPayCreate')->name('semi_finish_product.payburden-create');
Route::get('/get/customized/cake-list', 'App\Http\Controllers\RawMaterialIssueController@getBurdenList')->name('cake-list.index');

Route::post('/get/supplier/materials', 'PurchasingOrderManager\Controllers\PurchasingOrderController@getSupplierMaterial')->name('gsm.index');
Route::get('get/cash/category', 'App\Http\Controllers\CashBookInquiryController@getSubCashCategory')->name('get-cate');
Route::get('get/cash/subdesc/category', 'App\Http\Controllers\CashBookInquiryController@getSubCashCategoryType')->name('get-cate');
Route::get('get/cash/subdesc/desc1', 'App\Http\Controllers\CashBookInquiryController@getCategoryData')->name('get-desc');


Route::post('month/commis/store', 'App\Http\Controllers\DayCommisDataController@monthlyCommission')->name('month.store');


Route::get('dashboard/sales-return', 'App\Http\Controllers\ReturnDashboardController@index')->name('dashboard.sales-return');
Route::get('dashboard/sales-return/main-chart', 'App\Http\Controllers\ReturnDashboardController@mainChart')->name('dashboard.sales-return');
Route::get('dashboard/sales-return/product-pie-chart', 'App\Http\Controllers\ReturnDashboardController@productPieChart')->name('dashboard.sales-return');
Route::get('dashboard/sales-return/agent-donut-chart', 'App\Http\Controllers\ReturnDashboardController@agentDonutChart')->name('dashboard.sales-return');
Route::get('dashboard/sales-return/category-donut-chart', 'App\Http\Controllers\ReturnDashboardController@categoryDonutChart')->name('dashboard.sales-return');
Route::get('dashboard/sales-return/reusable-donut-chart', 'App\Http\Controllers\ReturnDashboardController@reusableDonutChart')->name('dashboard.sales-return');

Route::get('sales-routes', 'App\Http\Controllers\SalesRouteController@getSalesRoutes')->name('sales-routes.all');
Route::post('sales-routes/create', 'App\Http\Controllers\SalesRouteController@create')->name('sales-routes.create');
Route::delete('sales-routes/{id}', 'App\Http\Controllers\SalesRouteController@delete')->name('sales-routes.delete');
Route::post('sales-routes/{id}', 'App\Http\Controllers\SalesRouteController@update')->name('sales-routes.update');
Route::get('sales-routes/{id}/shops', 'App\Http\Controllers\ShopController@list')->name('sales-routes.shops');

Route::get('sales-shops', 'App\Http\Controllers\ShopController@list')->name('sales-shops.list');
Route::post('sales-shops/create', 'App\Http\Controllers\ShopController@create')->name('sales-shops.create');
Route::post('sales-shops/bulk/update', 'App\Http\Controllers\ShopController@bulkUpdate')->name('sales-shops.bulk');
Route::delete('sales-shops/{id}', 'App\Http\Controllers\ShopController@delete')->name('sales-shops.delete');
Route::post('sales-shops/{id}', 'App\Http\Controllers\ShopController@update')->name('sales-shops.update');
Route::get('sales-shops/{id}/shops', 'App\Http\Controllers\ShopController@list')->name('sales-shops.shops');
Route::post('sales-shop/bulk/qr/download', 'App\Http\Controllers\ShopController@bulkQRDownload')->name('sales-shops.qr');
Route::post('sales-shop/bulk/qr/download/pdf', 'App\Http\Controllers\ShopController@bulkQRDownloadPDF')->name('sales-shops.pdf');
Route::post('sales-shops-active/{id}', 'App\Http\Controllers\ShopController@active')->name('sales-shops-active.active');

Route::get('sales-agent-visited-shops', 'App\Http\Controllers\ShopController@visitedShopReport')->name('sales-shops.visited');
Route::get('sales-agent-visited-shops/filter', 'App\Http\Controllers\ShopController@visitedShopFilterReport')->name('sales-shops.filter');

Route::get('shop-orders', 'App\Http\Controllers\ShopOrderController@index')->name('shop-orders.index');
Route::get('shop-order/items', 'App\Http\Controllers\ShopOrderController@orderItems')->name('shop-order-items.index');
Route::get('shop-orders-return', 'App\Http\Controllers\ShopOrderController@getReturns')->name('shop-order-return.index');
Route::get('shop-orders-return/items', 'App\Http\Controllers\ShopOrderController@returnItems')->name('shop-order-return-items.index');

Route::get('shop-order-list', 'App\Http\Controllers\ShopOrderController@filterList')->name('shop-orders.list');
Route::get('shops-by-agent', 'App\Http\Controllers\ShopOrderController@getAgentShops')->name('shop-orders.agent');
Route::post('shops/change-shop-id', 'App\Http\Controllers\ShopOrderController@changeShopId')->name('shop-orders.agent-change');

Route::get('agent/shop/statistics', 'App\Http\Controllers\AgentShopStatisticsController@index')->name('agent.stats');
Route::get('agent/shop/statistics/by-number', 'App\Http\Controllers\AgentShopStatisticsController@getAgentVisitedShopsByCount')->name('agent.stats-number');
Route::get('agent/visited-shop-data', 'App\Http\Controllers\AgentShopStatisticsController@agentVisitedShops');
Route::get('agent/not-visited-shop-data', 'App\Http\Controllers\AgentShopStatisticsController@agentNotVisitedShops');
Route::get('agent/visited-shop-count-data', 'App\Http\Controllers\AgentShopStatisticsController@getAgentVisitedShopsByCountView');

Route::get('/agent/sync/user/{id}', 'SalesRepManager\Controllers\SalesRepController@syncUser')->name('agent.sync');


Route::get('privacy-policy-agreement', function (){
    return view('privacy');
});

Route::get('mobile-app', function (){
    return view('mobile-app');
});
