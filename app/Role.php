<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;


class Role extends \Spatie\Permission\Models\Role
{
//    use HasFactory;

    const BUSINESS_ROLES=['Businessman', 'User'];

    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query
            ->select('roles.*')
            ->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }
    protected static function booted()
    {
        $user = Auth::user();
        if ($user) {
            $role_level = Auth::user()->roles()->first()->role_level;
            if ($user->hasRole(Role::BUSINESS_ROLES)) {
                static::addGlobalScope('role_level', function (Builder $builder) use ($role_level) {
                    $builder->where('roles.role_level', '>', $role_level);
                });

            }
        }



    }

    public function scopeFilterData($query, $promotion, $channel, $outlet, $user, $date)
    {
        if (!empty($promotion)) {
            $query->where('id', '=', $promotion);
        }
        return $query;
    }

    public function scopeSearchData($query, $term)
    {
        return $query
            ->orWhere('roles.id', 'like', "%" . $term . "%")
            ->orWhere('roles.name', 'like', "%" . $term . "%");
    }
}
