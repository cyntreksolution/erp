<?php

namespace App;

use EndProductManager\Models\EndProduct;
use Illuminate\Database\Eloquent\Model;

class Variant extends Model
{
    protected $table = 'end_product_variants';
    protected $fillable = ['variant','size','return_accepted'];

    public function product(){
        return $this->belongsTo(EndProduct::class,'end_product_id');
    }
}
