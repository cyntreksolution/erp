<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasPermissions;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{

    use Notifiable;
    use HasRoles;
    use SoftDeletes;

    protected $table = 'users';
    protected $fillable = [
        'first_name', 'email', 'password','second_name','telephone1','address','status','otp_status'
    ];

    public function numbers(){
        return $this->belongsToMany(Number::class,'user_numbers','user_id');
    }


    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query
            ->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }


    public function scopeFilterData($query, $role)
    {
        if (!empty($role)) {
            $t =$query->whereHas('Roles', function ($q) use ($role) {
                return $q->where('name', '=', $role);
            });

        }

        return $query;
    }

    public function scopeSearchData($query, $term)
    {
        return $query
            ->orWhere('users.id', 'like', "%" . $term . "%")
            ->orWhere('users.first_name', 'like', " % " . $term . " % ")
            ->orWhere('users.second_name', 'like', " % " . $term . " % ")
            ->orWhere('users.telephone1', 'like', " % " . $term . " % ")
            ->orWhere('users.address', 'like', " % " . $term . " % ")
            ->orWhere('users.email', 'like', "%" . $term . "%");

    }

   public function customRoleRelation(){
        return $this->belongsToMany(Role::class, 'model_has_roles',  'model_id', 'role_id' );
   }
}
