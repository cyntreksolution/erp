<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentCrate extends Model
{
    protected $table ='agent_crates';

    protected $fillable=[
        'agent_id',
        'crate_id',
        'balance',
        'is_init',
    ];
}
