<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesMergeEndProductBurdenHeader extends Model
{
    protected $table = 'sales_merge_end_product_burden_headers';
}
