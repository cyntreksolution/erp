<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Number extends Model
{
    protected $fillable=['number','description'];
    public function users(){
        return $this->belongsToMany(\UserManager\Models\Users::class);
    }
}
