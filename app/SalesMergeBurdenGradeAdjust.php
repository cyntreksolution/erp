<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesMergeBurdenGradeAdjust extends Model
{
    protected $table = 'sales_merge_burden_grade_adjust';
}
