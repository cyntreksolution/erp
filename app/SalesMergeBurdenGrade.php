<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesMergeBurdenGrade extends Model
{
    protected $table = 'sales_merge_burden_grade';
}
