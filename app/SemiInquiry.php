<?php

namespace App;

use GradeManager\Models\Grade;
use Illuminate\Database\Eloquent\Model;
use RawMaterialManager\Models\RawMaterial;
use SalesRepManager\Models\SalesRep;
use SemiFinishProductManager\Models\SemiFinishProduct;
use Carbon\Carbon;

class SemiInquiry extends Model
{
    protected $table = 'semi_inquiry';

    public function getReferenceCodeAttribute()
    {
        $data = null;
        switch ($this->reference_type) {
            case 'App\LoadingHeader':
                $data = LoadingHeader::find($this->reference)->invoice_number;
                break;
            case 'App\SalesReturnHeader':
                $data = SalesReturnHeader::find($this->reference)->debit_note_number;
                break;
            case 'App\AgentPayment':
                $data = AgentPayment::find($this->reference)->serial;
                break;
        }
        return $data;

    }

    public function semi_finish_product()
    {
        return $this->belongsTo(SemiFinishProduct::class, 'semi_id', 'semi_finish_product_id');
    }

    public function grade()
    {
        return $this->belongsTo(Grade::class);
    }

    public function scopeIsInit($query)
    {
        return $query->whereIsInit(1);
    }

    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }

    public function scopeSearchData($query, $term)
    {
        return $query
            ->Where('id', 'like', "%" . $term . "%")
            ->orWhere('start_balance', 'like', "%" . $term . "%")
            ->orWhere('qty', 'like', "%" . $term . "%")
            ->orWhere('end_balance', 'like', "%" . $term . "%")
            ->orWhere('price', 'like', "%" . $term . "%")
            ->orWhere('amount', 'like', "%" . $term . "%")
            ->orWhere('description', 'like', "%" . $term . "%")
            ->orWhere('reference', 'like', "%" . $term . "%")
            ->orWhere('meta', 'like', "%" . $term . "%")
            ->orWhere('burden_id', 'like', "%" . $term . "%")
            ->orWhere('end_burden_id', 'like', "%" . $term . "%")
            ->orWhere('created_at', 'like', "%" . $term . "%");
    }

   // public function scopefilterData($query, $semi_product = null, $reference_type = null, $date_range = null)
    public function scopefilterData($query, $semi_product = null, $meta = null, $semi_type = null, $date_range = null)
    {

        if (!empty($semi_product)) {
            $query->whereHas('semi_finish_product', function ($q) use ($semi_product) {
                return $q->whereSemiId($semi_product);
            });
        }

        if (!empty($meta)) {
            $meta1 = '';
            switch ($meta) {
                case 1:
                    $meta1 = 'BurdenRequestGrade';

                    break;
                case 2:
                    $meta1 = 'CookingRequestGrade';

                    break;
                case 3:
                    $meta1 = 'EndBurdenRequest';

                    break;
                case 4:
                    $meta1 = 'ProductionPurpose';

                    break;
                case 5:
                    $meta1 = 'CustomizeCake';

                    break;
                case 6:
                    $meta1 = 'DestroyAndReturn';

                    break;
                case 7:
                    $meta1 = 'AdminAdjustments';

                    break;
                case 8:
                    $meta1 = 'BurdenGradeProduction';

                    break;
                case 9:
                    $meta1 = 'CookingGradeProduction';

                    break;
                case 10:
                    $meta1 = 'IssuedBurdenProduction';

                    break;
                case 11:
                    $meta1 = 'IssuedCookingProduction';

                    break;
                case 12:
                    $meta1 = 'BurdenWastageProduction';

                    break;
                case 13:
                    $meta1 = 'CookingWastageProduction';

                    break;
                case 14:
                    $meta1 = 'AllocateEnd';

                    break;


            }
         $query->where('meta', '=', $meta1);
        }


        if (!empty($semi_type)) {

            if($semi_type == 1){
                $semi_type1 = 'burden';
                }else{
                $semi_type1 = 'cooking_request';
            }

            $query->where('semi_type', '=', $semi_type1);


        }



        if (!empty($date_range)) {
            $dates = explode(' - ', $date_range);
            $start = $dates[0];
            $end = $dates[1];
            $query->where('created_at', '>=', Carbon::parse($start)->format('Y-m-d'))
                ->where('created_at', '<=', Carbon::parse($end)->addDay()->format('Y-m-d'));
        }

        return $query;
    }
}
