<?php

namespace App;

use Carbon\Carbon;
use EndProductManager\Models\EndProduct;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalesReturnData extends Model
{
    //use SoftDeletes;
    protected $fillable = [
        'sales_return_header_id',
        'end_product_id',
        'unit_value',
        'qty',
        'type',
        'old_data',
        'unit_value',
        'commission',
        'unit_discount',
        'total',
        'total_discount',
        'row_value',
        'is_system_generated',
        'all_qty',
        'return_stat',

    ];

    protected $table='sales_return_data';

    public function endProduct(){
        return $this->belongsTo(EndProduct::class);
    }

    public function header(){
        return $this->belongsTo(SalesReturnHeader::class,'sales_return_header_id');
    }

    public function scopeIsInit($query){
        return $query->whereIsInit(1);
    }
    public function scopecomTable($query, $order_column, $order_by_str, $start, $length)
    {
        return $query->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }


    public function scopefilterDataReturn($query, $end_product = null , $agent_id = null, $category = null, $date_range = null , $desc1 = null)
    {

        if (!empty($end_product)) {
            $query->where('end_product_id','=',$end_product);
        }

        if (!empty($category)) {
            $query->where('category_id','=',$category);
        }

        if (!empty($agent_id)) {

            $query->where('agent_id','=',$agent_id);
        }

        if (!empty($date_range)) {
            $dates = explode(' - ', $date_range);
            $start = $dates[0];
            $end = $dates[1];
            $query->where('sales_return_data.created_at', '>=', Carbon::parse($start)->format('Y-m-d'))
                ->where('sales_return_data.created_at', '<=', Carbon::parse($end)->addDay()->format('Y-m-d'));

        }

        if (!empty($desc1)) {
            $query = $query->whereHas('header', function ($q) use ($desc1) {
                return $q->where('shop_type', '=', $desc1);
            });
        }
    }



}
