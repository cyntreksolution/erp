<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DbBackupShedule extends Model
{
    protected $table='db_backup_shedule';
}
