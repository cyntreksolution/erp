<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatabaseBackup extends Model
{
    protected $table='database_backups';

    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query
            ->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);

    }


    public function scopeSearchData($query, $term)
    {
        return $query
            ->orWhere('id', 'like', "%" . $term . "%")
            ->orWhere('created_at', 'like', "%" . $term . "%")
            ->orWhere('size', 'like', "%" . $term . "%");
    }
}
