<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SemiBurdenIssueData extends Model
{
    protected $table ='semi_burden_issue_data';
}
