<?php

namespace App;

use EndProductBurdenManager\Models\EndProductBurden;
use GradeManager\Models\Grade;
use Illuminate\Database\Eloquent\Model;

class EndProductBurdenGrade extends Model
{
    public function burden()
    {
        return $this->belongsTo(EndProductBurden::class);
    }

    public function grade(){
        return $this->belongsTo(Grade::class);
    }



    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query->whereHas('burden', function ($query) {
            return $query->whereStatus(5);
        })
            ->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }

    public function scopeSearchData($query, $term)
    {
        return $query
            ->Where('id', 'like', "%" . $term . "%")
            ->orWhere('serial', 'like', "%" . $term . "%")
            ->orWhere('status', 'like', "%" . $term . "%")
            ->orWhere('burden_size', 'like', "%" . $term . "%")
            ->orWhere('created_at', 'like', "%" . $term . "%")
            ->orWhereHas('semiProdcuts', function ($query) use ($term) {
                $query->where('name', 'like', "%" . $term . "%");
            })
            ->orWhereHas('recipe', function ($query) use ($term) {
                $query->where('name', 'like', "%" . $term . "%");
            });
    }

    public function scopefilterData($query, $category, $end_product)
    {
        if (!empty($category)) {
            $query = $query->whereHas('endProduct.category', function ($q) use ($category) {
                return $q->whereId($category);
            });
        }

        if (!empty($end_product)) {
            $query = $query->whereEndProductId($end_product);
        }

        return $query;
    }
}
