<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AgentPayment extends Model
{
    use SoftDeletes;
    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }

    public function scopeSearchData($query, $term)
    {
        return $query
            ->Where('id', 'like', "%" . $term . "%")
            ->orWhere('agent_id', '=',  $term)
            ->orWhere('cheque_amount_value	', 'like', "%" . $term . "%")
            ->orWhere('cash_amount', 'like', "%" . $term . "%")
          //  ->orWhere('total_amount', 'like', "%" . $term . "%")
            ->orWhere('total_amount', 'like', "%" . $term . "%");
    }

    public function scopeFilterData($query, $date_range , $status , $agent )
    {
   //     dd('Agent :'.$agent .' Status :'.$status. 'Date :'.$date_range);


        if (!empty($agent)) {
            $query = $query->whereAgentId($agent);
        }
        if (!empty($date_range)) {
            $dates = explode(' - ', $date_range);
            $start = $dates[0];
            $end = $dates[1];
            $query->where('created_at','>=',Carbon::parse($start)->format('Y-m-d'))
                ->where('created_at','<=',Carbon::parse($end)->addDay()->format('Y-m-d'));
        }
        if (!empty($status)) {
            if($status == 1){
                $query->where('status','=','Pending');
            }elseif($status == 2){
                $query->where('status','=','Processing');
            }elseif($status == 3){
                $query->where('status','=','Approved');
            }

        }

        return $query;
    }

    public function agent()
    {
        return $this->belongsTo('SalesRepManager\Models\SalesRep', 'agent_id', 'id');
    }
}
