<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesMergeCookingRequest extends Model
{
    protected $table='sales_merge_cooking_requests';
}
