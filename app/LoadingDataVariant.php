<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class LoadingDataVariant extends Model
{
    protected $table = 'loading_data_variants';

    public function variant(){
        return $this->belongsTo(Variant::class);
    }

    public function header(){
        return $this->belongsTo(LoadingHeader::class,'loading_header_id');
    }
}
