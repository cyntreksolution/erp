<?php


namespace App\Traits;



use App\CrateInquiryStore;
use App\RawInquiry;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\Auth;
use RawMaterialManager\Models\RawMaterial;

trait RawMaterialLog
{
    public function currentRawMaterialBalance(RawMaterial $raw_material)
    {
        $raw_material = RawMaterial::find($raw_material->raw_material_id);
        return $raw_material->available_qty;

    }


    function recordRawMaterialOutstanding(RawMaterial $raw_material, $qty, $description = null, $reference = null, $reference_type = null,$desc_id,$type,$ref_id)
    {
        $current_balance = $this->currentRawMaterialBalance($raw_material);
        $log = new RawInquiry();
        $upp = RawMaterial::where('raw_material_id','=',$raw_material->raw_material_id)->first();
        $log->raw_material_id = $raw_material->raw_material_id;
        $log->start_balance =  $current_balance - $qty;
        $log->amount = (($raw_material->price)/($upp->units_per_packs)) * $qty;
        $log->price = (($raw_material->price)/($upp->units_per_packs));
        $log->qty = $qty;
        $log->end_balance = $current_balance;
        $log->desc_id = $desc_id;
        $log->type = $type; // 1=purchase ,2= Burden Request ,3=Cooking Request ,4=End Burden Request-Raw,5=End Burden Request-Packing,6=Meals,7=Production Purpose,8=Customized Cake,9=Destroy and Return,10=Cleaning Purpose,11=Crates Purchasing,12=Issue Shop Item,13=Add Reusable Wastage,14=Creates issue with invoice,15=Approved Return Crates,16=Update Invoiced Crates
                            //17=Measuring Variance,18=Employee Deduction,19=Return to Supplier,20=Return From Burden Request,21=Admin Crates Adjustment,22=Reverse Raw,23=Tea Beverage & Extra Meals,24=Refill Deep Fryers
        $log->Issued_ref_id = $ref_id;  //supplier id,burden_semi-product_id,burden_end-product_id,(-1)=Reusable Wastage
        $log->description = $description;
        $log->reference = $reference;
        $log->reference_type = $reference_type;
        $log->created_by = Auth::user()->id;
        $log->save();
    }


    public function initMaterialInquiry(RawMaterial $raw_material, $amount)
    {
        $log = new RawInquiry();
        $log->raw_material_id = $raw_material->raw_material_id;
        $log->start_balance = 0;
        $log->amount = $amount;
        $log->end_balance = $amount;
        $log->description = 'Initialize Value';
        $log->is_init = 1;
        $log->created_at = '2017-01-01 00:00:01';
        $log->created_by = Auth::user()->id;
        $log->save();
    }

    public function initInquiryMaterialEdit(RawMaterial $raw_material, RawInquiry $raw_materialInquiry, $amount)
    {
        $raw_materialInquiry->start_balance = 0;
        $raw_materialInquiry->amount = $amount;
        $raw_materialInquiry->end_balance = $amount;
        $raw_materialInquiry->description = 'Initialize Value Edited';
        $raw_materialInquiry->is_init = 1;
        $raw_materialInquiry->created_at = '2017-01-01 00:00:01';
        $raw_materialInquiry->created_by = Auth::user()->id;
        $raw_materialInquiry->save();
    }
}