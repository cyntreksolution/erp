<?php


namespace App\Traits;



use App\SupplierInquiry;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\Auth;
use SupplierManager\Models\Supplier;

trait SupplierLog
{
    public function currentBalance(Supplier $supplier)
    {
        return $supplier->balance;
    }

    function recordSupplierOutstanding(Supplier $supplier, $amount, $description = null, $reference = null, $reference_type = null)
    {
        $current_balance = $this->currentBalance($supplier);
        $log = new SupplierInquiry();
        $log->supplier_id = $supplier->id;
        $log->start_balance = $current_balance;
        $log->amount = $amount;
        $log->end_balance = $current_balance + $amount;
        $log->description = $description;
        $log->reference = $reference;
        $log->reference_type = $reference_type;
        $log->created_by = Auth::user()->id;
        $log->save();
    }

    public function initInquiry(Supplier $supplier, $amount)
    {
        $log = new SupplierInquiry();
        $log->supplier_id = $supplier->id;
        $log->start_balance = 0;
        $log->amount = $amount;
        $log->end_balance = $amount;
        $log->description = 'Initialize Value';
        $log->is_init = 1;
        $log->created_at = '2017-01-01 00:00:01';
        $log->created_by = Auth::user()->id;
        $log->save();
    }

    public function initInquiryEdit(Supplier $supplier, SupplierInquiry $supplierInquiry, $amount)
    {
        $supplierInquiry->start_balance = 0;
        $supplierInquiry->amount = $amount;
        $supplierInquiry->end_balance = $amount;
        $supplierInquiry->description = 'Initialize Value Edited';
        $supplierInquiry->is_init = 1;
        $supplierInquiry->created_at = '2017-01-01 00:00:01';
        $supplierInquiry->created_by = Auth::user()->id;
        $supplierInquiry->save();
    }
}