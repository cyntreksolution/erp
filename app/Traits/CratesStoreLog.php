<?php


namespace App\Traits;



use App\CrateInquiryStore;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\Auth;
use RawMaterialManager\Models\RawMaterial;
use SalesRepManager\Models\SalesRep;

trait CratesStoreLog
{
    public function currentCratesStoreLogBalance(RawMaterial $raw_material)
    {
        return $raw_material->available_qty;
    }

    function recordCratesStoreLogOutstanding(RawMaterial $raw_material,$qty, $description = null, $reference = null, $reference_type = null,$desc_id,$type,$ag_id)
    {
        $current_balance = $this->currentCratesStoreLogBalance($raw_material);
        $log = new CrateInquiryStore();
        $log->crate_id = $raw_material->raw_material_id;
        $log->agent_id = $ag_id;
        $log->start_balance = $current_balance;
        $log->qty = $qty;
        $log->end_balance = $current_balance + $qty;
        $log->description = $description;//1=Purchasing , 2=Destroy & Return,3=Creates issue with invoice, 4=Approved Return Crates,5=Update Invoiced Crates
        $log->reference = $reference;
        $log->reference_type = $reference_type;
        $log->desc_id = $desc_id;
        $log->type = $type;
        $log->created_by = Auth::user()->id;
        $log->save();
    }

    public function initCratesStoreLogInquiry(RawMaterial $raw_material, $amount)
    {
        $log = new CrateInquiryStore();
        $log->crate_id = $raw_material->raw_material_id;
        $log->start_balance = 0;
        $log->amount = $amount;
        $log->end_balance = $amount;
        $log->description = 'Initialize Value';
        $log->is_init = 1;
        $log->created_at = '2017-01-01 00:00:01';
        $log->created_by = Auth::user()->id;
        $log->save();
    }

    public function initCratesStoreLogInquiryEdit(RawMaterial $raw_material, CrateInquiryStore $raw_materialInquiry, $amount)
    {
        $raw_materialInquiry->start_balance = 0;
        $raw_materialInquiry->amount = $amount;
        $raw_materialInquiry->end_balance = $amount;
        $raw_materialInquiry->description = 'Initialize Value Edited';
        $raw_materialInquiry->is_init = 1;
        $raw_materialInquiry->created_at = '2017-01-01 00:00:01';
        $raw_materialInquiry->created_by = Auth::user()->id;
        $raw_materialInquiry->save();
    }
}