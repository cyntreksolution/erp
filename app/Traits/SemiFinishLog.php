<?php


namespace App\Traits;



use App\CrateInquiryStore;
use App\RawInquiry;
use App\SemiInquiry;
use BurdenManager\Models\Burden;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\Auth;
use RawMaterialManager\Models\RawMaterial;
use SemiFinishProductManager\Models\SemiFinishProduct;
use ShortEatsManager\Models\CookingRequest;

trait SemiFinishLog
{
    public function currentSemiFinishProductBalance(SemiFinishProduct $semi_finish_product)
    {
        $semi = SemiFinishProduct::where('semi_finish_product_id','=',$semi_finish_product->semi_finish_product_id)->first();
        return $semi->available_qty;
    }

    function recordSemiFinishProductOutstanding(SemiFinishProduct $semi_finish_product, $grade_id, $qty, $description = null, $reference = null, $reference_type = null,$burden_id=null,$meta=null, $type,$gradeRef,$end_burden_id)
    {


        $semi_type = $semi_finish_product->is_bake ==1 ?'burden':'cooking_request';
       // $current_balance = $this->currentSemiFinishProductBalance($semi_finish_product);


        $log = new SemiInquiry();
        $log->semi_id = $semi_finish_product->semi_finish_product_id;
        $log->grade_id = $grade_id;

        if ($type == 1) {
            $qty = $qty;
        } else {
            $qty = (($qty)*(-1));
        }

      //  if($semi_type == 'burden') {
          if(($semi_finish_product->is_bake) == 1) {
           $bdt = Burden::find($burden_id);

            $semi = SemiFinishProduct::find($semi_finish_product->semi_finish_product_id);
            $op_bal = $semi->available_qty;



            $end_bal = $op_bal + $qty;
            if($grade_id == 1 || $grade_id == 2) {

                $semi->available_qty = $end_bal;
                $semi->save();
            }elseif ($grade_id == 0){
                $semi->available_qty = $op_bal + $qty;
                $semi->save();
            }

        }else{

            $bdt = CookingRequest::find($burden_id);


            $semi = SemiFinishProduct::find($semi_finish_product->semi_finish_product_id);
              $op_bal = $semi->available_qty;
              $end_bal = $op_bal + $qty;

              if ($grade_id == 0){

                  $semi->available_qty = $semi->available_qty + $qty;
                  $semi->save();
              }else {
                  $semi->available_qty = $end_bal;
                  $semi->save();
              }
        }

        $log->start_balance = $op_bal;
        $log->qty = $qty;



        if($meta == 'AdminAdjustments'){
            $log->price = 0;
            $log->amount = 0;
        }else{
            $log->price = $bdt->value;
            $log->amount = (($bdt->value)/($bdt->expected_semi_product_qty)) * $qty;
        }



        if($grade_id <= 2) {
            $log->end_balance = $end_bal;
        }else{
            $log->end_balance = $op_bal;
        }
        $log->grade_ref_id = $gradeRef;
        $log->end_burden_id = $end_burden_id;
        $log->type = $type;

        $log->description = $description;
        $log->reference = $reference;
        $log->reference_type = $reference_type;
        $log->created_by = Auth::user()->id;
        $log->burden_id =$burden_id;
        $log->meta =$meta;
        $log->semi_type =$semi_type;

        $log->save();

    }

    public function initSemiFinishProductInquiry(SemiFinishProduct $semi_finish_product,$grade_id, $amount)
    {
        $log = new SemiInquiry();
        $log->semi_id = $semi_finish_product->semi_finish_product_id;
        $log->grade_id = $grade_id;
        $log->start_balance = 0;
        $log->amount = $amount;
        $log->end_balance = $amount;
        $log->description = 'Initialize Value';
        $log->is_init = 1;
        $log->created_at = '2017-01-01 00:00:01';
        $log->created_by = Auth::user()->id;
        $log->save();
    }

    public function initSemiFinishProductInquiryEdit(SemiFinishProduct $semi_finish_product, SemiInquiry $semi_finish_productInquiry, $amount)
    {
        $semi_finish_productInquiry->start_balance = 0;
        $semi_finish_productInquiry->amount = $amount;
        $semi_finish_productInquiry->end_balance = $amount;
        $semi_finish_productInquiry->description = 'Initialize Value Edited';
        $semi_finish_productInquiry->is_init = 1;
        $semi_finish_productInquiry->created_at = '2017-01-01 00:00:01';
        $semi_finish_productInquiry->created_by = Auth::user()->id;
        $semi_finish_productInquiry->save();
    }
}