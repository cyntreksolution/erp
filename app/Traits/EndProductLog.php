<?php


namespace App\Traits;



use App\CrateInquiryStore;
use App\EndProductInquiry;
use App\RawInquiry;
use App\SemiInquiry;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use EndProductManager\Models\EndProduct;
use Illuminate\Support\Facades\Auth;
use RawMaterialManager\Models\RawMaterial;
use SemiFinishProductManager\Models\SemiFinishProduct;

trait EndProductLog
{
    public $message =[
        'production_line_grade_a'=>'Grade A Stock Add From Production',
        'production_line_grade_d'=>'Grade D Adjust From Production',
        'production_line_grade_c'=>'Grade C Adjust From Production'
    ];

    public function currentEndProductBalance(EndProduct $end_product)
    {
        return EndProduct::find($end_product->id)->available_qty;
    }

    function recordEndProductOutstanding(EndProduct $end_product, $grade_id, $qty, $description = null, $reference = null, $reference_type = null,$ag_id,$desc_id,$type,$so_id)
    {
        $current_balance = $this->currentEndProductBalance($end_product);
        $log = new EndProductInquiry();
        $log->end_product_id = $end_product->id;
        $log->grade_id = $grade_id;
        $log->start_balance =  $current_balance - $qty;
        $log->qty = $qty;
        $log->amount = $end_product->distributor_price * $qty;
        $log->price = $end_product->distributor_price;
        $log->end_balance =$current_balance;
        $log->description = $description;//1=Loading ,2=Variant Loading ,3=Unloading,4=Variant Unloading,5=Burden Grade-Production,6=Grade Adjustment-Production,7=Reusable-Production,8=Destroy-Production
                                         //9=Lost-Production,10=Stores Addition-Grade A,11=Stores Addition-Grade B,12=System Genarated Return,13=Stock Rotation-Sales Return,14=Factory Fault-Sales Return
                                         //15=Reusable-Stores Return,16=Destroy-Stores Return,17=Lost-Stores Return
        $log->desc_id = $desc_id;
        $log->type = $type;
        $log->agent_id = $ag_id;
        $log->category_id = $end_product->category_id;
        $log->reference = $reference;
        $log->so_id = $so_id;
        $log->reference_type = $reference_type;
        $log->created_by = Auth::user()->id;
        $log->save();

    }

    public function initEndProductInquiry(EndProduct $end_product,$grade_id, $amount)
    {
        $log = new EndProductInquiry();
        $log->end_product_id = $end_product->id;
        $log->grade_id = $grade_id;
        $log->start_balance = 0;
        $log->amount = $amount;
        $log->end_balance = $amount;
        $log->description = 'Initialize Value';
        $log->is_init = 1;
        $log->created_at = '2017-01-01 00:00:01';
        $log->created_by = Auth::user()->id;
        $log->save();
    }

    public function initEndProductInquiryEdit(EndProduct $end_product, EndProductInquiry $end_productInquiry, $amount)
    {
        $end_productInquiry->start_balance = 0;
        $end_productInquiry->amount = $amount;
        $end_productInquiry->end_balance = $amount;
        $end_productInquiry->description = 'Initialize Value Edited';
        $end_productInquiry->is_init = 1;
        $end_productInquiry->created_at = '2017-01-01 00:00:01';
        $end_productInquiry->created_by = Auth::user()->id;
        $end_productInquiry->save();
    }
}