<?php


namespace App\Traits;


trait SendSMS
{
    public function getAccountStatus(){
        $endpoint = "http://send.ozonedesk.com/api/v2/status.php";
        $client = new \GuzzleHttp\Client();
        $user_id = env('SMS_GATEWAY_USER_ID');
        $api_key = env('SMS_GATEWAY_API_KEY');

        $response = $client->request('POST', $endpoint, ['query' => [
            'user_id' => $user_id,
            'api_key' => $api_key,
        ]]);

        $statusCode = $response->getStatusCode();
        $content = $response->getBody();

        return $content;
    }


    public function sendSMS($to,$content){
        $endpoint = env('SMS_GATEWAY_URL');
        $client = new \GuzzleHttp\Client();
        $user_id = env('SMS_GATEWAY_USER_ID');
        $api_key = env('SMS_GATEWAY_API_KEY');
        $sender_id = env('SMS_GATEWAY_SENDER_ID');
        $message =$content;

//        $response = $client->request('POST', 'https://kentacy.com/api/v1/sms', ['query' => [
//            'business_id' => 893484,
//            'business_secret' => 'BdmDdwftIciKHjYXUTLpTTuJeaTei6o3',
//            'sender_id' => $sender_id,
//            'number' => $to,
//            'message' => $message,
//        ]]);


        $response = $client->request('GET', 'https://api.kentacy.com/api/sms/create', ['query' => [
            'app_id' => "2Arj3ZqkGV0PwErnzfNApm",
            'app_secret' => '9c63cf24-60ab-4049-b204-0d08a4f453b5',
            'from' => 'BunTalk',
            'numbers' => $to,
            'message' => $message,
        ]]);


//        $response = $client->request('POST', $endpoint, ['query' => [
//            'user_id' => $user_id,
//            'api_key' => $api_key,
//            'sender_id' => $sender_id,
//            'to' => $to,
//            'message' => $message,
//        ]]);

        $statusCode = $response->getStatusCode();
        $content = $response->getBody();

        return $content;
    }
}
