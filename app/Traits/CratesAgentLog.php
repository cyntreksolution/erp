<?php


namespace App\Traits;



use App\AgentCrate;
use App\CrateInquiryAgent;
use App\RawInquiry;
use App\SupplierInquiry;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\Auth;
use RawMaterialManager\Models\RawMaterial;
use SalesRepManager\Models\SalesRep;
use SupplierManager\Models\Supplier;

trait CratesAgentLog
{
    public function currentCratesAgentLogBalance(RawMaterial $raw_material,SalesRep $salesRep)
    {
        $maxid =CrateInquiryAgent::where('crate_id','=',$raw_material->raw_material_id)
            ->where('agent_id','=',$salesRep->id)
            ->orderByDesc('id')
            ->first();

        if(!empty($maxid)) {
            $cbal = CrateInquiryAgent::where('id', '=', $maxid->id)
                ->first();
            return $cbal->end_balance;
        }else{
            return 0;
        }
    }

    function recordCratesAgentLogOutstanding(RawMaterial $raw_material,SalesRep $salesRep, $qty, $description = null, $reference = null, $reference_type = null,$desc_id,$type)
    {
        $current_balance = $this->currentCratesAgentLogBalance($raw_material,$salesRep);
        $log = new CrateInquiryAgent();
        $log->crate_id = $raw_material->raw_material_id;
        $log->agent_id = $salesRep->id;
        $log->start_balance = $current_balance;
        $log->qty = $qty;
        $log->end_balance = $current_balance + $qty;
        $log->description = $description;//1=Creates receive with invoice ,2=Approved Return Creates ,3=Update Invoiced Crates,21=Agent's Crates Adjustment(Admin)
        $log->reference = $reference;
        $log->desc_id = $desc_id;
        $log->type = $type;
        $log->reference_type = $reference_type;
        $log->created_by = Auth::user()->id;
        $log->save();

        $agentCrate = AgentCrate::updateOrCreate([
            'agent_id' => $salesRep->id,
            'crate_id' => $raw_material->raw_material_id,
            'balance' => $current_balance + $qty,
        ]);
    }

    public function initCratesAgentLogInquiry(RawMaterial $raw_material,SalesRep $salesRep, $amount)
    {
        $log = new CrateInquiryAgent();
        $log->crate_id = $raw_material->raw_material_id;
        $log->agent_id = $salesRep->id;
        $log->raw_material_id = $raw_material->id;
        $log->start_balance = 0;
        $log->amount = $amount;
        $log->end_balance = $amount;
        $log->description = 'Initialize Value';
        $log->is_init = 1;
        $log->created_at = '2017-01-01 00:00:01';
        $log->created_by = Auth::user()->id;
        $log->save();
    }

    public function initCratesAgentLogInquiryEdit(RawMaterial $raw_material, CrateInquiryAgent $raw_materialInquiry, $amount)
    {
        $raw_materialInquiry->start_balance = 0;
        $raw_materialInquiry->amount = $amount;
        $raw_materialInquiry->end_balance = $amount;
        $raw_materialInquiry->description = 'Initialize Value Edited';
        $raw_materialInquiry->is_init = 1;
        $raw_materialInquiry->created_at = '2017-01-01 00:00:01';
        $raw_materialInquiry->created_by = Auth::user()->id;
        $raw_materialInquiry->save();
    }
}