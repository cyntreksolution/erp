<?php


namespace App\Traits;


use App\AgentInquiry;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\Auth;
use SalesRepManager\Models\SalesRep;

trait AgentLog
{
    public function currentBalance(SalesRep $agent)
    {
        return $agent->balance;
    }

    function recordOutstanding(SalesRep $agent, $amount, $description = null, $reference = null, $reference_type = null, $desc = null)
    {
        $current_balance = $this->currentBalance($agent);
        $log = new AgentInquiry();
        $log->agent_id = $agent->id;
        $log->start_balance = $current_balance;
        $log->amount = $amount;
        $log->end_balance = $current_balance + $amount;
        $log->description = $description;
        $log->sub_desc = $desc;
        $log->reference = $reference;
        $log->reference_type = $reference_type;
        $log->created_by = Auth::user()->id;
        $log->save();
    }

    public function initInquiry(SalesRep $agent, $amount)
    {
        $log = new AgentInquiry();
        $log->agent_id = $agent->id;
        $log->start_balance = 0;
        $log->amount = $amount;
        $log->end_balance = $amount;
        $log->description = 'Initialize Value';
        $log->is_init = 1;
        $log->created_at = '2017-01-01 00:00:01';
        $log->created_by = Auth::user()->id;
        $log->save();
    }

    public function initInquiryEdit(SalesRep $agent, AgentInquiry $agentInquiry, $amount)
    {
        $agentInquiry->start_balance = 0;
        $agentInquiry->amount = $amount;
        $agentInquiry->end_balance = $amount;
        $agentInquiry->description = 'Initialize Value Edited';
        $agentInquiry->is_init = 1;
        $agentInquiry->created_at = '2017-01-01 00:00:01';
        $agentInquiry->created_by = Auth::user()->id;
        $agentInquiry->save();
    }
}