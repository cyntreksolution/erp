<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserNumber extends Model
{
    public function number(){
        return $this->belongsTo(Number::class);
    }


    public function user(){
        return $this->belongsTo(\UserManager\Models\Users::class);
    }
}
