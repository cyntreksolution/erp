<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesMergeMergeRequest extends Model
{
    protected $table ='sales_merge_merge_requests';
}
