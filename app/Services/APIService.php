<?php

namespace App\Services;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class APIService
{
    private $token;
    private $client;

    public function __construct()
    {
        try {
            $this->client = new \GuzzleHttp\Client();
            $this->token = "Bearer " . $this->getTokenFromSession();
        } catch (\Exception $exception) {

        }

    }


    private function getPersonalAccessToken()
    {
        $endpoint = env('API_URL') . "/oauth/token";
        $username = env('API_USERNAME');
        $userPassword = env('API_USER_PASSWORD');
        $clientId = env('API_CLIENT_ID');
        $clientSecret = env('API_CLIENT_SECRET');


        $response = $this->client->request('POST', $endpoint, [
            'headers' => [
                'Accept' => 'application/json',
            ],
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => $clientId,
                'client_secret' => $clientSecret,
                'username' => $username,
                'password' => $userPassword,
            ]]);

        $statusCode = $response->getStatusCode();
        $content = $response->getBody();
        $token = json_decode($content)->access_token;

        return $token;
    }

    private function putTokenToSession($token)
    {
        session()->put('api_token', $token);
    }

    private function getTokenFromSession()
    {
        if ($this->isSessionExists()) {
            return session()->get('api_token');
        } else {
            $this->putTokenToSession($this->getPersonalAccessToken());
            return $this->getTokenFromSession();
        }

    }

    private function isSessionExists()
    {
        return Session::has('api_token');
    }


    public function getAllRoutes($params = null)
    {
        $endpoint = env('API_URL') . "/api/shop-routes/get-routes$params";
        $response = $this->client->request('GET', $endpoint, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => $this->token
            ]]);

        $data = collect();
        $statusCode = $response->getStatusCode();
        if ($statusCode == 200) {
            $content = $response->getBody();
            $data = json_decode($content);
        }
        return $data;
    }

    public function createSalesRoute($dataset)
    {
        $endpoint = env('API_URL') . "/api/shop-routes";
        $response = $this->client->request('POST', $endpoint, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => $this->token
            ],
            'query' => $dataset
        ]);

        $data = collect();
        $statusCode = $response->getStatusCode();
        if ($statusCode == 200) {
            $content = $response->getBody();
            $data = json_decode($content);
        }
        return $data;
    }

    public function updateSalesRoute($id, $dataset)
    {
        $endpoint = env('API_URL') . "/api/shop-routes/$id";
        $response = $this->client->request('PUT', $endpoint, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => $this->token
            ],
            'query' => $dataset
        ]);

        $data = collect();
        $statusCode = $response->getStatusCode();
        if ($statusCode == 200) {
            $content = $response->getBody();
            $data = json_decode($content);
        }
        return $data;
    }

    public function deleteSalesRoute($id)
    {
        $endpoint = env('API_URL') . "/api/shop-routes/$id";
        $response = $this->client->request('DELETE', $endpoint, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => $this->token
            ]
        ]);

        return $response;
    }


    public function getAllShops($params = null)
    {
        $data = [];
        try {
            $endpoint = env('API_URL') . "/api/shop/get-shops$params";
            $response = $this->client->request('GET', $endpoint, [
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => $this->token
                ]]);

            $data = collect();
            $statusCode = $response->getStatusCode();
            if ($statusCode == 200) {
                $content = $response->getBody();
                $data = json_decode($content);
            }
        } catch (\Exception $exception) {

        }

        return $data;
    }

    public function createShop($dataset)
    {
        $endpoint = env('API_URL') . "/api/shop";
        $response = $this->client->request('POST', $endpoint, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => $this->token
            ],
            'query' => $dataset
        ]);

        $data = collect();
        $statusCode = $response->getStatusCode();
        if ($statusCode == 200) {
            $content = $response->getBody();
            $data = json_decode($content);
        }
        return $data;
    }

    public function updateShop($id, $dataset)
    {
        $endpoint = env('API_URL') . "/api/shop/$id";
        $response = $this->client->request('PUT', $endpoint, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => $this->token
            ],
            'query' => $dataset
        ]);

        $data = collect();
        $statusCode = $response->getStatusCode();
        if ($statusCode == 200) {
            $content = $response->getBody();
            $data = json_decode($content);
        }
        return $data;
    }

    public function activeShop($id)
    {
        $endpoint = env('API_URL') . "/api/agents/enable-portal/";
        $response = $this->client->request('POST', $endpoint, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => $this->token
            ],
            'query' => ['shop_id' => $id]
        ]);

        $data = collect();
        $statusCode = $response->getStatusCode();

        if ($statusCode == 200) {
            $content = $response->getBody();
            $data = json_decode($content);
        }
        return $data;
    }

    public function deleteShop($id)
    {
        $endpoint = env('API_URL') . "/api/shop/$id";
        $response = $this->client->request('DELETE', $endpoint, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => $this->token
            ]
        ]);

        return $response;
    }

    public function bulkShopInsert($data)
    {
        $file = $data->file('update_file');

        $file = $data->file('update_file');
        $destinationPath = public_path('uploads');
        $file->move($destinationPath, $file->getClientOriginalName());

//        $name = $file->getFileName();
//        $path = $file->getRealPath();
        $fileinfo = array(
            'name' => $file->getClientOriginalName(),
            'clientNumber' => "102425",
            'type' => 'Writeoff',
        );
        $endpoint = env('API_URL') . "/api/shop/bulk/insert";
        $response = $this->client->request('POST', $endpoint, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => $this->token
            ],
            'query' => ['file_name' => "/var/www/erp/public/uploads/" . $file->getClientOriginalName(), 'shop_route_id' => $data->shop_route_id]
        ]);

//        $response = $this->client->request('POST', $endpoint, [
//            'headers' => [
//                'Accept' => 'application/json',
//                'Authorization' => $this->token
//            ],
//            'query' =>['shop_route_id'=> $data->shop_route_id],
//            'multipart' => [
//                [
//                    'name'     => 'update_file',
//                    'contents' => file_get_contents($destinationPath.'/'.$file->getClientOriginalName()),
//                    'filename' => $file->getClientOriginalName()
//                ],
//                [
//                    'name'     => 'FileInfo',
//                    'contents' => json_encode($fileinfo)
//                ]
//            ],
//        ]);

        $data = collect();
        $statusCode = $response->getStatusCode();
        if ($statusCode == 200) {
            $content = $response->getBody();
            $data = json_decode($content);
        }

        return $data;
    }

    public function bulkQRDownload($data)
    {
        $endpoint = env('API_URL') . "/api/shop/bulk/qr/download";
        $response = $this->client->request('POST', $endpoint, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => $this->token
            ],
            'query' => ['id_list' => $data]
        ]);

        $data = collect();
        $statusCode = $response->getStatusCode();
        if ($statusCode == 200) {
            $content = $response->getBody();
            $data = json_decode($content);
        }
        return $data;
    }


    public function bulkQRDownloadPDF($data)
    {
        $endpoint = env('API_URL') . "/api/shop/bulk/qr/download/pdf";
        $response = $this->client->request('POST', $endpoint, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => $this->token
            ],
            'query' => ['id_list' => $data]
        ]);

        $data = collect();
        $statusCode = $response->getStatusCode();
        if ($statusCode == 200) {
            $content = $response->getBody();
            $data = json_decode($content);
        }
        return $data;
    }

    public function getAllAgents($params = null)
    {
        $endpoint = env('API_URL') . "/api/agents/get-all-agents$params";
        $response = $this->client->request('GET', $endpoint, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => $this->token
            ]]);

        $data = collect();
        $statusCode = $response->getStatusCode();
        if ($statusCode == 200) {
            $content = $response->getBody();
            $data = json_decode($content);
        }
        return $data;
    }


    public function getVisitedData($dateRange, $routeId=null, $agentId=null)
    {
        $endpoint = env('API_URL') . "/api/reports/visited-shops?date_range=$dateRange&agent_id=$agentId&route_id=$routeId";
        $response = $this->client->request('GET', $endpoint, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => $this->token
            ]]);

        $data = collect();
        $statusCode = $response->getStatusCode();
        if ($statusCode == 200) {
            $content = $response->getBody();
            $data = json_decode($content);
        }
        return $data;
    }

    public function getNotVisitedData($dateRange, $routeId=null, $agentId=null)
    {
        $endpoint = env('API_URL') . "/api/reports/not-visited-shops?date_range=$dateRange&agent_id=$agentId&route_id=$routeId";
        $response = $this->client->request('GET', $endpoint, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => $this->token
            ]]);

        $data = collect();
        $statusCode = $response->getStatusCode();
        if ($statusCode == 200) {
            $content = $response->getBody();
            $data = json_decode($content);
        }
        return $data;
    }

    public function getVisitedDataFilter($dateRange, $routeId, $agentId, $shopId)
    {
        $endpoint = env('API_URL') . "/api/reports/visited-shops/filter?date_range=$dateRange&agent_id=$agentId&route_id=$routeId&shop_id=$shopId";
        $response = $this->client->request('GET', $endpoint, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => $this->token
            ]]);

        $data = collect();
        $statusCode = $response->getStatusCode();
        if ($statusCode == 200) {
            $content = $response->getBody();
            $data = json_decode($content);
        }
        return $data;
    }

    public function sendLoadingDataToAPI($data)
    {
        $endpoint = env('API_URL') . "/api/update/agent/stocks";
        $response = $this->client->request('POST', $endpoint, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => $this->token
            ],
            'form_params' => ['order_data' => json_decode($data)]
        ]);

        $data = collect();
        $statusCode = $response->getStatusCode();
        if ($statusCode == 200) {
            $content = $response->getBody();
            $data = json_decode($content);
        }
        return $data;
    }

    public function getShopOrders($shopId = null, $agentId = null, $dateRange = null)
    {
        $endpoint = env('API_URL') . "/api/order/get-orders";
        $response = $this->client->request('GET', $endpoint, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => $this->token
            ],
            'query' => ['shop_id' => $shopId, 'agent_id' => $agentId, 'date_range' => $dateRange]
        ]);

        $data = collect();
        $statusCode = $response->getStatusCode();
        if ($statusCode == 200) {
            $content = $response->getBody();
            $data = json_decode($content);
        }
        return $data;
    }

    public function getShopOrderItems($orderId)
    {
        $endpoint = env('API_URL') . "/api/order/get-orders/items";
        $response = $this->client->request('GET', $endpoint, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => $this->token
            ],
            'query' => ['order_id' => $orderId]
        ]);

        $data = collect();
        $statusCode = $response->getStatusCode();
        if ($statusCode == 200) {
            $content = $response->getBody();
            $data = json_decode($content);
        }
        return $data;
    }

    public function getShopsByAgent($agentId)
    {
        $endpoint = env('API_URL') . "/api/shop/get-by-agent";
        $response = $this->client->request('GET', $endpoint, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => $this->token
            ],
            'query' => ['agent_id' => $agentId]
        ]);

        $data = collect();
        $statusCode = $response->getStatusCode();
        if ($statusCode == 200) {
            $content = $response->getBody();
            $data = json_decode($content);
        }
        return $data;
    }

    public function changeOrderShopId($orderId, $shopId)
    {
        $endpoint = env('API_URL') . "/api/order/change-shop-id";
        $response = $this->client->request('POST', $endpoint, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => $this->token
            ],
            'query' => [
                'shop_id' => $shopId,
                'order_id' => $orderId,
            ]
        ]);

        $data = collect();
        $statusCode = $response->getStatusCode();
        if ($statusCode == 200) {
            $content = $response->getBody();
            $data = json_decode($content);
        }
        return $data;
    }

    public function getAgentSalesStat($dateRange = null, $agentId = null)
    {
        $endpoint = env('API_URL') . "/api/dashboard/dashboard-data";
        $response = $this->client->request('GET', $endpoint, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => $this->token
            ],
            'query' => [
                'agent_id' => $agentId,
                'date_range' => $dateRange
            ]
        ]);

        $data = collect();
        $statusCode = $response->getStatusCode();
        if ($statusCode == 200) {
            $content = $response->getBody();
            $data = json_decode($content);
        }
        return $data;
    }

    public function agentVisitedShopsByCount($agentId, $number = 1, $symbol = '=',$dateRange=null)
    {
        $endpoint = env('API_URL') . "/api/dashboard/visited-shops";
        $response = $this->client->request('GET', $endpoint, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => $this->token
            ],
            'query' => [
                'agent_id' => $agentId,
                'number' => $number,
                'symbol' => $symbol,
                'date_range' => $dateRange,
            ]
        ]);

        $data = collect();
        $statusCode = $response->getStatusCode();
        if ($statusCode == 200) {
            $content = $response->getBody();
            $data = json_decode($content);
        }
        return $data;
    }


}
