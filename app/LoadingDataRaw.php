<?php

namespace App;

use EndProductManager\Models\EndProduct;
use Illuminate\Database\Eloquent\Model;
use RawMaterialManager\Models\RawMaterial;

class LoadingDataRaw extends Model
{
    protected $table='loading_data_raw';

    public function rawMaterial(){
        return $this->belongsTo(RawMaterial::class,'raw_material_id','raw_material_id')->withoutGlobalScope('type');
    }
}
