<?php

namespace App;

use EndProductManager\Models\EndProduct;
use GradeManager\Models\Grade;
use Illuminate\Database\Eloquent\Model;
use SalesRepManager\Models\SalesRep;
use SemiFinishProductManager\Models\SemiFinishProduct;
use Carbon\Carbon;

class EndProductInquiry extends Model
{
    protected $table ='end_inquiry';

    public function getReferenceCodeAttribute()
    {
        $data =null;
        switch ($this->reference_type){
            case 'App\LoadingHeader':
                $data= LoadingHeader::find($this->reference)->invoice_number;
                break;
            case 'App\SalesReturnHeader':
                $data= SalesReturnHeader::find($this->reference)->debit_note_number;
                break;
            case 'App\AgentPayment':
                $data= AgentPayment::find($this->reference)->serial;
                break;
        }
        return $data;

    }

    public function end_product(){
        return $this->belongsTo(EndProduct::class);
    }
    public function agent(){
        return $this->belongsTo(SalesRep::class,'agent_id');
    }
    public function scopeLoadData(){
        switch ($this->reference_type){
            case 'App\LoadingData':
                $data= LoadingData::find($this->so_id);
                break;
            case 'App\LoadingDataVariant':
                $data= LoadingDataVariant::find($this->so_id);
                break;
        }
        return $data;
    }

    public function grade(){
        return $this->belongsTo(Grade::class);
    }

    public function header()
    {
        return $this->belongsTo(LoadingHeader::class,'so_id');

    }

    public function scopeIsInit($query){
        return $query->whereIsInit(1);
    }
    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }

    public function scopeSearchData($query, $term)
    {
        return $query
            ->Where('id', 'like', "%" . $term . "%")
            ->orWhere('start_balance', 'like', "%" . $term . "%")
            ->orWhere('qty', 'like', "%" . $term . "%")
            ->orWhere('end_balance', 'like', "%" . $term . "%")
            ->orWhere('price', 'like', "%" . $term . "%")
            ->orWhere('amount', 'like', "%" . $term . "%")
            ->orWhere('description', 'like', "%" . $term . "%")
            ->orWhere('reference', 'like', "%" . $term . "%")
            ->orWhere('created_at', 'like', "%" . $term . "%");
    }

    public function scopefilterData($query, $end_product = null, $date_range = null, $category =null, $agent_id=null, $item_type=null, $inquiry_type =null)
    {

        if (!empty($end_product)) {
            $query->whereHas('end_product', function ($q) use ($end_product) {
                return $q->whereId($end_product);
            });
        }

        if (!empty($category)) {
            $query->where('category_id','=',$category);
        }

        if (!empty($agent_id)) {

            $query->where('agent_id','=',$agent_id);
        }
        /*
        if (!empty($item_type)) {
            $query->where('desc_id','=',$item_type);
        }else{

            if (!empty($inquiry_type)) {
                $query->whereIn('desc_id',[1,2]);
            }
        }*/

        if (!empty($date_range)) {
            $dates = explode(' - ', $date_range);
            $start = $dates[0];
            $end = $dates[1];
            $query->where('created_at', '>=', Carbon::parse($start)->format('Y-m-d'))
                ->where('created_at', '<=', Carbon::parse($end)->addDay()->format('Y-m-d'));
        }
    }

    public function scopeFilterDataReport($query, $end_product = null, $date)
    {
        if (!empty($end_product)) {
            $query->whereHas('end_product.category', function ($q) use ($end_product) {
                return $q->whereId($end_product);
            });
        }

        if (!empty($date)) {
            $query->where('created_at', 'like', Carbon::parse($date)->format('Y-m-d').'%');
        }
    }

    public function scopeTargetData($query,  $date_range, $agent=null , $desc1=null , $id1 )
    {


        // dd('date :'.$date_range.'ag :'.$agent.'de '.$desc1);
        if (!empty($desc1)) {
            $query = $query->whereHas('header', function ($q) use ($desc1) {
                return $q->where('shop_type', '=', $desc1);
            });
        }

        if (!empty($agent)) {

            $query->where('agent_id', '=', $agent);
        }
        if (empty($id1)) {
            if (!empty($date_range)) {
                $dates = explode(' - ', $date_range);
                $start = $dates[0];
                $end = $dates[1];
                $query->where('loading_date', '>=', Carbon::parse($start)->format('Y-m-d'))
                    ->where('loading_date', '<=', Carbon::parse($end)->addDay()->format('Y-m-d'));
            }
        }else {
                if (!empty($date_range)) {
                    $dates = explode(' - ', $date_range);
                    $start = $dates[0];
                    $end = $dates[1];
                    $query->where('created_at', '>=', Carbon::parse($start)->format('Y-m-d'))
                        ->where('created_at', '<=', Carbon::parse($end)->addDay()->format('Y-m-d'));

                }
            }
        }

}
