<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AgentBufferData extends Model
{
    protected $table ='agent_buffer_data';

    public function endProduct(){
        return $this->belongsTo('EndProductManager\Models\EndProduct','end_product_id');
    }
}
