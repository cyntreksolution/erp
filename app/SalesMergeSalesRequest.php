<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesMergeSalesRequest extends Model
{
    protected $table = 'sales_merge_sales_requests';
}
