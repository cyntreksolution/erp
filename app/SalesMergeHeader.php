<?php

namespace App;

use EndProductBurdenManager\Models\EndProductBurden;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use SalesOrderManager\Models\SalesOrder;
use SalesRequestManager\Models\SalesRequest;

class SalesMergeHeader extends Model
{
    use SoftDeletes;
    protected $table = 'sales_merge_header';

    public function mergeRequests(){
        return $this->belongsToMany(MergeRequest::class,'sales_merge_merge_requests');
    }

    public function salesRequests(){
        return $this->belongsToMany(SalesRequest::class,'sales_merge_sales_requests');
    }

    public function endProductBurdens(){
        return $this->belongsToMany(EndProductBurden::class,'sales_merge_end_product_burdens');
    }
}
