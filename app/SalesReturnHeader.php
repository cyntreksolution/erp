<?php

namespace App;

use BurdenManager\Models\ReusableProducts;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use SalesRepManager\Models\SalesRep;

class SalesReturnHeader extends Model
{
    use  SoftDeletes;

    protected $fillable = [
        'agent_id', 'total', 'all_total', 'debit_note_number', 'time', 'driver', 'received_by', 'received_at', 'changed', 'count'
    ];

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d H:i:s');
    }

    public function items()
    {
        return $this->hasMany(SalesReturnData::class);
    }

    public function agent()
    {
        return $this->hasOne(SalesRep::class, 'id', 'agent_id');
    }

    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }

    public function scopeSearchData($query, $term)
    {
        return $query
            ->Where('id', 'like', "%" . $term . "%")
            ->orWhere('serial', 'like', "%" . $term . "%")
            ->orWhere('status', 'like', "%" . $term . "%")
            ->orWhere('burden_size', 'like', "%" . $term . "%")
            ->orWhere('created_at', 'like', "%" . $term . "%")
            ->orWhereHas('semiProdcuts', function ($query) use ($term) {
                $query->where('name', 'like', "%" . $term . "%");
            })
            ->orWhereHas('recipe', function ($query) use ($term) {
                $query->where('name', 'like', "%" . $term . "%");
            });
    }


    public function scopeFilterData($query, $date_range=null, $agent=null, $status=null,$type=null,$time=null,$desc1=null)
    {
        if (!empty($agent)) {
            $query->where('agent_id', '=', $agent);
        }
        if (!empty($status)) {
            $query->where('status', '=', $status);
        }

        if (!empty($time)) {
            $query->where('time', '=', $time);
        }

        if (!empty($type)) {

            $type = $type==1?1:0;

            $query->where('is_system_generated', '=', $type);
        }

        if (!empty($date_range)) {
            $dates = explode(' - ', $date_range);
            $start = $dates[0];
            $end = $dates[1];
            $query->where('created_at','>=',Carbon::parse($start)->format('Y-m-d'))
                ->where('created_at','<=',Carbon::parse($end)->addDay()->format('Y-m-d'));
        }

        if (!empty($desc1)) {
            $query->where('shop_type','=',$desc1);
            }

        return $query;
    }


    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('status', function (Builder $builder) {
            $builder->where('status', '<>', 0);
        });

    }


    public function scopeSumData($query){
      return  $query->select(
          DB::raw('SUM(total) as  total_sum'),
          DB::raw('SUM(return_discount) as dis_sum'),
          DB::raw('SUM(hold_amount) as hold_sum'),
          DB::raw('SUM(final_amount) as final_sum'));

    }


    public function reUsableProducts(){
        return $this->belongsTo(ReusableProducts::class,'debit_note_number','note_id');
    }


    public function collector(){
        return $this->belongsTo(User::class,'collected_by');
    }
}
