<?php

namespace App\Jobs;

use App\LoadingHeader;
use App\Services\APIService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class SalesApiStockUpdateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $order_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($order_id)
    {
        \Log::info("Running Agent Stock Update API");
        $this->order_id = $order_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $order = LoadingHeader::where('id', $this->order_id)->with(['items','variants','salesOrder'])->first();;
        $order['agent_id'] = $order->salesOrder->created_by;

//        if ($order->is_fetched_to_api ==0) {
            $client = new APIService();
            $client->sendLoadingDataToAPI($order);
//        }else{
//            \Log::info("updated order : ".$order->id);
//        }

    }
}
