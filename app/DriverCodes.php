<?php

namespace App;

use EmployeeManager\Models\Employee;
use Illuminate\Database\Eloquent\Model;

class DriverCodes extends Model
{

    protected $fillable = ['id','employee_id','code','expire_at','created_by','updated_by','deleted_at'];
  public function employee(){
    return $this->belongsTo(Employee::class,'employee_id');
  }
}
