<?php

namespace App\Console;

use App\Console\Commands\DBBackupCommand;
use App\Console\Commands\DBBackupDelete;
use App\DbBackupShedule;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $db_shedule_now = DbBackupShedule::find(2);
        if ($db_shedule_now->status ==1) {
            $schedule->command(DBBackupCommand::class)->dailyAt($db_shedule_now->time)->appendOutputTo(storage_path('logs/shedule.log'));
        }
        $set_update_time =Carbon::parse($db_shedule_now->time)->addMinutes(10)->format('H:i');
        if ($set_update_time == (Carbon::now()->format('H:i'))) {
            $db_shedule_now->status = 2;
            $db_shedule_now->update();
        }


        $db_shedule = DbBackupShedule::find(1);
        if ($db_shedule->status ==1) {
            $schedule->command(DBBackupCommand::class)->hourly();
        }elseif ($db_shedule->status ==2){
            $schedule->command(DBBackupCommand::class)->dailyAt($db_shedule->time);
        }elseif ($db_shedule->status ==3){
            $schedule->command(DBBackupCommand::class)->weeklyOn(1, '00:00');
        }else {
            $schedule->command(DBBackupCommand::class)->monthlyOn(1, '00:00');
        }
        $schedule->command(DBBackupDelete::class)->daily();
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
