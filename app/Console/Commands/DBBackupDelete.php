<?php

namespace App\Console\Commands;

use App\DatabaseBackup;
use App\DbBackupShedule;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class DBBackupDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete DB Backup';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $id = $request->backup_id;
        $db_shedule = DbBackupShedule::first();
        $latest_backup = DatabaseBackup::latest('created_at')->first();
        if($db_shedule->delete_status == 1){
            $lates_date = Carbon::parse($latest_backup->created_at)->subDay(1);
        }elseif ($db_shedule->delete_status == 2){
            $lates_date = Carbon::parse($latest_backup->created_at)->subDay(3);
        }elseif ($db_shedule->delete_status == 3){
            $lates_date = Carbon::parse($latest_backup->created_at)->subDay(7);
        }elseif ($db_shedule->delete_status == 4){
            $lates_date = Carbon::parse($latest_backup->created_at)->subDay(30);
        }elseif ($db_shedule->delete_status == 5){
            $lates_date = Carbon::parse($latest_backup->created_at)->subDay(90);
        }
        $deleteAll_records = DatabaseBackup::where('created_at','<',$lates_date)->get();
        foreach ($deleteAll_records as $delete_data) {
            $file = public_path() . "/backup/" . $delete_data->file_path;
            File::delete($file);
            $delete_data->delete();
        }
        return 'true';
    }
}
