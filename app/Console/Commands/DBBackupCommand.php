<?php

namespace App\Console\Commands;

use App\DatabaseBackup;
use App\Traits\SendSMS;
use Carbon\Carbon;
use GuzzleHttp\Exception\BadResponseException;
use ZipArchive;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
class DBBackupCommand extends Command
{
    use SendSMS;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup Database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $zip = new \ZipArchive();
            $filename = "system_backup_" . Carbon::now()->format('YmdHis') . ".sql";
            $command = "mysqldump  --user=" . env('DB_USERNAME') . " --password=" . env('DB_PASSWORD') . " --host=" . env('DB_HOST') . " " . env('DB_DATABASE') ."   >  /home/super/buntalk/backup/$filename";
            $returnVar = NULL;
            $output = NULL;
            exec($command, $output, $returnVar);

            $name = Carbon::now()->format('YmdHis');
            $this->makeZip($name, [$filename]);

            $msg = "Database Backup Created : ".Carbon::now()->format('Y-m-d H:i:s');
            $this->sendSMS(env('SMS_NUMBER'), $msg);

        }catch (BadResponseException $exception){
            @file_put_contents("../storage/backup/" . date("Y-m-d") . "-She.log", $exception->getMessage() . PHP_EOL . $exception->getResponse()->getBody()->getContents() . PHP_EOL . PHP_EOL, FILE_APPEND);

        }
    }

    public function makeZip($zipFileName, $files)
    {
        $zip = new ZipArchive;
        $fileName = $zipFileName . '.zip';

        foreach ($files as $file) {
            $oldFile = "/home/super/buntalk/backup/$file";
            Storage::makeDirectory("backup/$zipFileName");
            $destination = storage_path("backup/$zipFileName/$file");
            File::copy($oldFile, $destination);
        }

        if ($zip->open(storage_path("backup/zip/$fileName"), ZipArchive::CREATE) === TRUE) {
            $filess = File::files(storage_path("backup/$zipFileName"));
            foreach ($filess as $key => $value) {
                $relativeNameInZipFile = basename($value);
                $zip->addFile($value, $relativeNameInZipFile);
            }
            $zip->close();
        }
        $file_size = File::size(storage_path() . "/backup/zip/" . $zipFileName. ".zip");
        $db = new DatabaseBackup();
        $zip_file = "/backup/zip/" . $zipFileName . ".zip";
        $db->file_path = $zip_file;
        $db->size = $this->bytesToHuman($file_size);
        $db->save();

        $this->deleteCreatedFolder($zipFileName);
        $this->deleteCreatedFile($files);
    }

    public function deleteCreatedFolder($name)
    {
        $directory = "backup/$name";
        Storage::deleteDirectory($directory);
    }

    public function deleteCreatedFile($files)
    {
        foreach ($files as $file) {
            $directory = public_path("backup/$file");
            File::delete($directory);
        }
    }

    public static function bytesToHuman($bytes)
    {
        $units = ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB'];

        for ($i = 0; $bytes > 1024; $i++) {
            $bytes /= 1024;
        }

        return round($bytes, 2) . ' ' . $units[$i];
    }
}
