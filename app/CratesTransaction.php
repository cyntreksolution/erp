<?php

namespace App;
use App\User;
use Illuminate\Database\Eloquent\Model;

class CratesTransaction extends Model
{
    protected $table='crates_transaction';
    public function agent(){
      return $this->belongsTo(User::class,'agent_id');
    }

    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }

    public function scopeSearchData($query, $term)
    {
        return $query
            ->Where('id', 'like', "%" . $term . "%")
            ->orWhere('serial', 'like', "%" . $term . "%")
            ->orWhere('status', 'like', "%" . $term . "%")
            ->orWhere('burden_size', 'like', "%" . $term . "%")
            ->orWhere('created_at', 'like', "%" . $term . "%")
            ->orWhereHas('semiProdcuts', function ($query) use ($term) {
                $query->where('name', 'like', "%" . $term . "%");
            })
            ->orWhereHas('recipe', function ($query) use ($term) {
                $query->where('name', 'like', "%" . $term . "%");
            });
    }

    public function scopefilterData($query, $category = null, $day = null, $time = null, $date = null, $agent = null, $status = null)
    {
        if (!empty($category)) {
            $query->whereHas('template', function ($q) use ($category) {
                return $q->whereCategoryId($category);
            });
        }

        if (!empty($day)) {
            $query->whereHas('template', function ($q) use ($day) {
                return $q->where('day', '=', $day);
            });
        }

        if (!empty($time)) {
            $query->whereHas('template', function ($q) use ($time) {
                return $q->where('time', '=', $time);
            });
        }

        if (!empty($date)) {
            $query->whereDate('order_date', $date);
        }

        if (!empty($agent)) {
            $query->whereCreatedBy($agent);
        }
        if (!empty($status)) {
            $query->whereStatus($status);
        }
    }

    public function loading(){
        return $this->belongsTo(LoadingHeader::class,'ref_id');
    }
}
