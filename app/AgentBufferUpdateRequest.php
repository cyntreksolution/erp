<?php

namespace App;

use EndProductManager\Models\Category;
use EndProductManager\Models\EndProduct;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AgentBufferUpdateRequest extends Model
{
    use SoftDeletes;
    protected $table = 'agent_buffer_update_requests';

    protected $fillable=['status','agent_buffer_header_id','dataset','created_by'];

    const day = [1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday', 7 => 'Sunday', 8 => 'Long Weekend',9=>'Extra'];
    const time = [1 => 'Morning', 2 => 'Noon', 3 => 'Evening'];



    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }

    public function scopeFilterData($query,$agent_id, $category_id, $day,$time)
    {
        if (!empty($agent_id)) {
            $query->whereAgentId($agent_id);
        }

    }

    public function salesRep()
    {
        return $this->belongsTo('SalesRepManager\Models\SalesRep', 'created_by', 'id');
    }

    public function template()
    {
        return $this->belongsTo(AgentBuffer::class, 'agent_buffer_header_id', 'id');
    }



}
