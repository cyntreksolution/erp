<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use SalesRepManager\Models\SalesRep;

class AgentInquiry extends Model
{
    public function agent(){
        return $this->belongsTo(SalesRep::class,'agent_id');
    }

    public function scopeIsInit($query){
        return $query->whereIsInit(1);
    }

    public function getReferenceCodeAttribute()
    {
        $data =null;
        switch ($this->reference_type){
            case 'App\LoadingHeader':
                $data= LoadingHeader::find($this->reference)->invoice_number;
                break;
            case 'App\SalesReturnHeader':
                $data= SalesReturnHeader::find($this->reference)->debit_note_number;
                break;
            case 'App\AgentPayment':
                $data= AgentPayment::find($this->reference)->serial;
                break;
        }
        return $data;

    }
    public function scopefilterData($query, $agent = null, $date_range = null)
    {
        if (!empty($agent)) {
            $query->whereHas('agent', function ($q) use ($agent) {
                return $q->whereAgentId($agent);
            });
        }

        if (!empty($date_range)) {
            $dates = explode(' - ', $date_range);
            $start = $dates[0];
            $end = $dates[1];
            $query->where('created_at', '>=', Carbon::parse($start)->format('Y-m-d'))
                ->where('created_at', '<=', Carbon::parse($end)->addDay()->format('Y-m-d'));
        }
    }
}
