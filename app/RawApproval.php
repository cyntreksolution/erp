<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use RawMaterialManager\Models\RawMaterial;
use SalesRepManager\Models\SalesRep;
use Carbon\Carbon;

class RawApproval extends Model
{
    protected $table = 'raw_approvals';
}
