<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use RawMaterialManager\Models\RawMaterial;
use SalesRepManager\Models\SalesRep;
use Carbon\Carbon;

class RawInquiry extends Model
{
    protected $table ='raw_inquiry';

    public function getReferenceCodeAttribute()
    {
        $data =null;
        switch ($this->reference_type){
            case 'App\LoadingHeader':
                $data= LoadingHeader::find($this->reference)->invoice_number;
                break;
            case 'App\SalesReturnHeader':
                $data= SalesReturnHeader::find($this->reference)->debit_note_number;
                break;
            case 'App\AgentPayment':
                $data= AgentPayment::find($this->reference)->serial;
                break;
        }
        return $data;

    }


    public function raw_material(){
        return $this->belongsTo(RawMaterial::class,'raw_material_id','raw_material_id');
    }

    public function scopeIsInit($query){
        return $query->whereIsInit(1);
    }
    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }

    public function scopemultipleData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }

    public function scopeSearchData($query, $term)
    {
        return $query
            ->Where('id', 'like', "%" . $term . "%")
            ->orWhere('start_balance', 'like', "%" . $term . "%")
            ->orWhere('qty', 'like', "%" . $term . "%")
            ->orWhere('end_balance', 'like', "%" . $term . "%")
            ->orWhere('price', 'like', "%" . $term . "%")
            ->orWhere('amount', 'like', "%" . $term . "%")
            ->orWhere('description', 'like', "%" . $term . "%")
            ->orWhere('reference', 'like', "%" . $term . "%")
            ->orWhere('created_at', 'like', "%" . $term . "%");
    }

    public function scopefilterData($query, $raw_material = null, $reference_type = null, $date_range = null)
    {
        if (!empty($raw_material)) {
            $query->whereHas('raw_material', function ($q) use ($raw_material) {
                return $q->whereRawMaterialId($raw_material);
            });
        }

        if (!empty($reference_type)) {
            $query->where('desc_id',$reference_type);
        }

        if (!empty($date_range)) {
            $dates = explode(' - ', $date_range);
            $start = $dates[0];
            $end = $dates[1];
            $query->where('raw_inquiry.created_at', '>=', Carbon::parse($start)->format('Y-m-d'))
                ->where('raw_inquiry.created_at', '<=', Carbon::parse($end)->addDay()->format('Y-m-d'));
//                ->orderByRaw('DATE_FORMAT(created_at,"%y-%m-%d %h:%i:%s")');
        }
    }

    public function scopemultiFilterData($query, $raw_material = null, $reference_type = null, $date_range = null)
    {
        if (!empty($raw_material)) {
            $query->whereHas('raw_material', function ($q) use ($raw_material) {
                return $q->whereRawMaterialId($raw_material);
            });
        }

        if (!empty($reference_type)) {
            $query->where('desc_id',$reference_type);
        }

        if (!empty($date_range)) {
            $dates = explode(' - ', $date_range);
            $start = $dates[0];
            $end = $dates[1];
            $query->where('raw_inquiry.created_at', '>=', Carbon::parse($start)->format('Y-m-d'))
                ->where('raw_inquiry.created_at', '<=', Carbon::parse($end)->addDay()->format('Y-m-d'));
//                ->orderByRaw('DATE_FORMAT(created_at,"%y-%m-%d %h:%i:%s")');
        }
    }

    public function scopefilterDataShopItem($query, $raw_material = null, $agent = null, $desc_id = 12,$date_range = null)
    {
        if (!empty($raw_material)) {
            $query->whereHas('raw_material', function ($q) use ($raw_material) {
                return $q->whereRawMaterialId($raw_material)->whereType(4);
            });
        }

        if (!empty($agent)) {
            $query->where('Issued_ref_id',$agent);
        }

        $query->where('desc_id',$desc_id);

        if (!empty($date_range)) {
            $dates = explode(' - ', $date_range);
            $start = $dates[0];
            $end = $dates[1];
            $query->where('raw_inquiry.created_at', '>=', Carbon::parse($start)->format('Y-m-d'))
                ->where('raw_inquiry.created_at', '<=', Carbon::parse($end)->addDay()->format('Y-m-d'));

        }
    }

}
