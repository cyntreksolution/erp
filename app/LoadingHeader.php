<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use SalesOrderManager\Models\SalesOrder;

class LoadingHeader extends Model
{
    public function items()
    {
        return $this->hasMany(LoadingData::class);
    }

    public function rawItems()
    {
        return $this->hasMany(LoadingDataRaw::class);
    }

    public function variants()
    {
        return $this->hasMany(LoadingDataVariant::class);
    }

    public function salesOrder()
    {
        return $this->belongsTo(SalesOrder::class, 'so_id');
    }

    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }

    public function scopeSearchData($query, $term)
    {
        return $query
            ->Where('id', 'like', "%" . $term . "%")
            ->orWhere('serial', 'like', "%" . $term . "%")
            ->orWhere('status', 'like', "%" . $term . "%")
            ->orWhere('is_paid', 'like', "%" . $term . "%")
            ->orWhere('burden_size', 'like', "%" . $term . "%")
            ->orWhere('created_at', 'like', "%" . $term . "%")
            ->orWhereHas('semiProdcuts', function ($query) use ($term) {
                $query->where('name', 'like', "%" . $term . "%");
            })
            ->orWhereHas('recipe', function ($query) use ($term) {
                $query->where('name', 'like', "%" . $term . "%");
            });
    }

//    public function scopeFilterData($query, $semi, $status, $date_range)
//    {
//        if (!empty($semi)) {
//            $query->whereSemiFinishProductId($semi);
//        }
//        if (!empty($status)) {
//            $query->whereStatus($status);
//        }
//
//        if (!empty($date_range)) {
//            $dates = explode(' - ', $date_range);
//            $start = $dates[0];
//            $end = $dates[1];
//            $query->whereBetween('created_at', [$start, $end]);
//        }
//    }
    public function scopeFilterData($query, $date_range, $agent , $category=null, $day=null,$type=null,$time=null,$desc1=null)
    {
       // dd($date_range."-".$agent."-".$category."-".$type);
        if (!empty($agent)) {
            $query = $query->whereHas('salesOrder.salesRep', function ($q) use ($agent) {
                return $q->where('id', '=', $agent);
            });
        }

        if (!empty($category)) {
            $query = $query->whereHas('salesOrder.template.category', function ($q) use ($category) {
                return$q->where('id', '=', $category);
            });
        }
        if (!empty($day)) {
            $query = $query->whereHas('salesOrder.template.category', function ($q) use ($day) {
                return$q->where('day', '=', AgentBuffer::day[$day]);
            });
        }

        if (!empty($time)) {
            $query = $query->whereHas('salesOrder.template.category', function ($q) use ($time) {
                return$q->where('time', '=', AgentBuffer::time[$time]);
            });
        }

        if (!empty($date_range)) {
            $dates = explode(' - ', $date_range);
            $start = $dates[0];
            $end = $dates[1];
            $query->where('loading_date','>=',Carbon::parse($start)->format('Y-m-d'))
                ->where('loading_date','<=',Carbon::parse($end)->addDay()->format('Y-m-d'));
        }
        if (!empty($type)) {
            if($type == 1){
                $query->where('is_paid','=',0);
            }elseif($type == 2){
                $query->where('is_paid','=',1);
            }

        }

        if (!empty($desc1)) {

                $query->where('shop_type','=',$desc1);


        }
    }

    public function scopeFilterDataSales($query, $date_range, $agent , $category=null, $type=null,$desc1=null)
    {
       // dd('date_range :'.$date_range.' agent:'.$agent.'category :'.$category.'type :'.$type.'desc1 :'.$desc1);
        // dd($date_range."-".$agent."-".$category."-".$type);
        if (!empty($agent)) {
            $query = $query->whereHas('salesOrder.salesRep', function ($q) use ($agent) {
                return $q->where('id', '=', $agent);
            });
        }

        if (!empty($category)) {
            $query = $query->whereHas('salesOrder.template.category', function ($q) use ($category) {
                return$q->where('id', '=', $category);
            });
        }
        if (!empty($day)) {
            $query = $query->whereHas('salesOrder.template.category', function ($q) use ($day) {
                return$q->where('day', '=', AgentBuffer::day[$day]);
            });
        }

        if (!empty($time)) {
            $query = $query->whereHas('salesOrder.template.category', function ($q) use ($time) {
                return$q->where('time', '=', AgentBuffer::time[$time]);
            });
        }

        if (!empty($date_range)) {
            $dates = explode(' - ', $date_range);
            $start = $dates[0];
            $end = $dates[1];
            $query->where('loading_date','>=',Carbon::parse($start)->format('Y-m-d'))
                ->where('loading_date','<=',Carbon::parse($end)->addDay()->format('Y-m-d'));
        }
        if (!empty($type)) {
            if($type == 1){
                $query->where('is_paid','=',0);
            }elseif($type == 2){
                $query->where('is_paid','=',1);
            }

        }

        if (!empty($desc1)) {

            $query->where('shop_type','=',$desc1);


        }
    }

    public function scopeTargetData($query, $agent ,$desc1=null)
    {
        // dd(' agent:'.$agent.'desc1 :'.$desc1);
        // dd($date_range."-".$agent."-".$category."-".$type);
        if (!empty($agent)) {
            $query = $query->whereHas('salesOrder.salesRep', function ($q) use ($agent) {
                return $q->where('id', '=', $agent);
            });
        }

        if (!empty($desc1)) {

            $query->where('shop_type','=',$desc1);


        }
    }




    public function isShopItemsLoaded()
    {
        $shopItemsStatus = $this->rawItems->pluck('status');
        if (!empty($shopItemsStatus)) {
            if ($shopItemsStatus->contains(0)) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
}
