<?php

namespace App;

use GradeManager\Models\Grade;
use Illuminate\Database\Eloquent\Model;
use SemiFinishProductManager\Models\SemiFinishProduct;
use SupplierManager\Models\Supplier;

class SupplierInquiry extends Model
{
    protected $table ='supplier_inquiries';

    public function getReferenceCodeAttribute()
    {
        $data =null;
        switch ($this->reference_type){
            case 'App\LoadingHeader':
                $data= LoadingHeader::find($this->reference)->invoice_number;
                break;
            case 'App\SalesReturnHeader':
                $data= SalesReturnHeader::find($this->reference)->debit_note_number;
                break;
            case 'App\AgentPayment':
                $data= AgentPayment::find($this->reference)->serial;
                break;
        }
        return $data;

    }

    public function supplier(){
        return $this->belongsTo(Supplier::class);
    }

    public function scopeIsInit($query){
        return $query->whereIsInit(1);
    }
}
