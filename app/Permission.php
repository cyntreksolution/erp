<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Permission extends \Spatie\Permission\Models\Permission
{
    protected $table='permissions';
    protected $fillable = [
        'name', 'guard_name'
    ];

    public function permissionGroup(){
        return $this->belongsTo(PermissionGroup::class,'permission_group_id');
    }
    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query
            ->select('permission_groups.name','permissions.*')
            ->join('permission_groups', 'permissions.permission_group_id', '=', 'permission_groups.id')
            ->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }


    public function scopeFilterData($query, $promotion, $channel, $outlet, $user, $date)
    {
        if (!empty($promotion)) {
            $query->where('id', '=', $promotion);
        }
        return $query;
    }

    public function scopeSearchData($query, $term)
    {
        return $query
            ->orWhere('permissions.id', 'like', "%" . $term . "%")
            ->orWhere('permissions.name', 'like', "%" . $term . "%");
    }

}
