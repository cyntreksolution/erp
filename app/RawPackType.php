<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RawPackType extends Model
{
    protected $table="raw_material_pack";
}
