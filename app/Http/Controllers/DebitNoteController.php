<?php

namespace App\Http\Controllers;

use App\LoadingHeader;
use App\SalesReturnHeader;
use App\SmsSourcePoint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use SalesRepManager\Models\SalesRep;

class DebitNoteController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:debit_note-index', ['only' => ['index','tableData']]);
        $this->middleware('permission:DebitNoteHistory-list', ['only' => ['list']]);

    }
    public function index()
    {
        $agents = SalesRep::all()->pluck('name_with_initials','id');
        return view('debit-note', compact('agents'));
    }

    public function tableData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'debit_note_number', 'total', 'agent_id'];
        $order_column = $columns[$order_by[0]['column']];

        $records = SalesReturnHeader::tableData($order_column, $order_by_str, $start, $length);

        $recordsCount =0;
        $total_amount = 0;

        $stat_data = SalesReturnHeader::query();
        $stst = null;

        if ($request->filter == true) {
            $agent = $request->agent;
            $date_range = $request->date_range;
            $status = $request->status;
            $type = $request->type;
            $time = $request->time;
            $desc1 = $request->desc1;

            $recordsCount = SalesReturnHeader::filterData($date_range, $agent, $status, $type, $time,$desc1)->count();

            $records = SalesReturnHeader::filterData($date_range, $agent, $status, $type, $time,$desc1)->tableData($order_column, $order_by_str, $start, $length)->get();

            $stst = $stat_data->filterData($date_range, $agent, $status, $type, $time ,$desc1)
                ->select(DB::raw('SUM(total) as total_sum'),DB::raw('SUM(all_total) as alltotal_sum'),DB::raw('SUM(return_discount) as retdis_sum'),DB::raw('SUM(hold_amount) as hold_sum'),DB::raw('SUM(final_amount) as final_sum'))
                ->first();
            $total_amount = $stst->total_sum;
            $all_total = $stst->alltotal_sum;
            $ret_dis = $stst->retdis_sum;
            $hold_amount = $stst->hold_sum;
            $set_amount = $stst->final_sum;

//            $invoices = $records->filterData($date_range, $agent, $status, $type, $time)->get();

        }/* elseif (is_null($search) || empty($search)) {
            $records = $records->get();
            $recordsCount = SalesReturnHeader::all()->count();
            $stst = $stat_data
                ->select(DB::raw('SUM(total) as  total_sum'))
                ->first();
        } else {
            $stst = $stat_data->searchData($search)
                ->select(DB::raw('SUM(total) as  total_sum'))
                ->first();
            $records = $records->searchData($search)->get();
            $recordsCount = $records->count();
        }*/




        $data = [];
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;


        foreach ($records as $key => $record) {
            $statusBtn = null;
            $printBtn = null;
            switch ($record->status) {
                case 1:
                    $statusBtn = '<button class="btn btn-outline-danger btn-sm mr-1">pending</button>';
                    break;

                case 2:
                    $statusBtn = '<button class="btn btn-outline-warning btn-sm mr-1">received</button>';
                    break;

                case 3:
                    $statusBtn = '<button class="btn btn-outline-success btn-sm mr-1">billed</button>';
                    break;
            }

            $viewBtn = '<a href="'.route('sales_return.show',$record->id).'" class="btn btn-outline-info btn-sm mr-1"><i class="fa fa-eye"></i> </a>';




            // $printBtn = '{!! Form::open(["route" => "admin.sales_return_print", "method" => "post","target"=>"_blank"]) !!}<input type="hidden" name="sales_return_header" value="'.$record->id.'"><button type="submit" name="submit" value="print_and_approve"
            //                class="btn btn-outline-info btn-sm mr-1"><i class="icon-print"></i> </button>{!! Form::close() !!}';

            $printBtn = '<a href="'.route('admin.sales_return_print_get',$record->id).'" class="btn btn-outline-info btn-sm mr-1"><i class="fa fa-print"></i> </a>';
            $receiveBtn = ($record->status == 1) ? '<a href="'.route('sales_return.edit',$record->id).'" class="btn btn-outline-success btn-sm mr-1"><i class="fa fa-truck"></i> </a>' : null;

            $btn = null;

            if (Auth::user()->hasRole(['Sales Agent'])) {
                $btn = $viewBtn;
            }else{
                $btn = $viewBtn . $receiveBtn . $printBtn;
            }

            $data[$i] = array(
                $record->debit_note_number,
                $record->created_at,
                $record->time,
                $record->agent->first_name . ' ' . $record->agent->last_name,
                number_format($record->all_total, '2'),
                number_format($record->total, '2') ,
                number_format($record->return_discount, '2') ,
                number_format($record->hold_amount, '2') ,
                number_format($record->final_amount, '2') ,
                $btn.$statusBtn
            );
            $i++;
        }


        if ($recordsCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($recordsCount),
            "recordsFiltered" => intval($recordsCount),
            "total_sum" => number_format($all_total, 2),
            "sent_sum" => number_format($total_amount, 2),
            "dis_sum" => number_format($ret_dis, 2),
            "hold_sum" => number_format($hold_amount, 2),
            "final_sum" => number_format($set_amount, 2),
            "data" => $data
        ];

        return json_encode($json_data);
    }

    public function exportFilter(Request $request)
    {
        $campaign = $request->campaign;
        $records = Record::CampaignUtilizationReportsData($campaign);
        return Excel::download(new CampaignUtilizationReportExport($records), $this->reportFileName('campaign_utilization', '.xlsx', '', 'report'));
    }
}
