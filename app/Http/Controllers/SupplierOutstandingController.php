<?php

namespace App\Http\Controllers;

use App\LoadingHeader;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PurchasingOrderManager\Models\PurchasingOrder;
use SupplierManager\Models\Supplier;

class SupplierOutstandingController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:supplier_outstanding-index', ['only' => ['index','tableData']]);
        $this->middleware('permission:SupplierOutstanding-list', ['only' => ['list']]);
    }
    public function index()
    {
        $suppliers = Supplier::pluck('supplier_name', 'id');
        return view('account.supplier-outstand_list', compact('suppliers'));
    }

    public function tableData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'created_at'];
        $order_column = $columns[$order_by[0]['column']];

        $order_column = 'created_at';
        $order_by_str = 'DESC';

        $stat_data = PurchasingOrder::query();
        $invoiceCount = PurchasingOrder::count();
        $invoices = PurchasingOrder::tableData($order_column, $order_by_str, $start, $length);

        $filterCount = 0;

        if ($request->filter == true) {
            $supplier = $request->supplier;
            $date_range = $request->date_range;
            $status = $request->status;
            $payment = $request->payments;



            $stst = $stat_data
                ->select(DB::raw('SUM(total) as  total_sum'))
                ->filterData($date_range, $supplier, $status, $payment)
                ->first();

            $filterCount = PurchasingOrder::filterData($date_range, $supplier, $status, $payment)->count();
            $invoices = $invoices->filterData($date_range, $supplier, $status, $payment)->get();




        } elseif (is_null($search) || empty($search)) {
            $invoices = $invoices->whereStatus(2)->get();
            $filterCount = $invoices->count();
            $stst = $stat_data
                ->select(DB::raw('SUM(total) as  total_sum'))
                ->whereStatus(2)
                ->first();


        } else {
            $invoices = $invoices->searchData($search)->get();
            $invoiceCount = $invoices->count();
            $stst = $stat_data
                ->select(DB::raw('SUM(total) as  total_sum'))
                ->whereStatus(2)
                ->first();
        }

        $total_sum = $stst->total_sum;
        $net_sum = $stst->net_sum;
        $data = [];
        $i = 0;
        $user = Auth::user();
        $can_view = ($user->can('purchasing_order-show')) ? 1 : 0;


        foreach ($invoices as $key => $invoice) {
            $payment  = $request->status;
            $status= $request->payments;
            $time_column =$invoice->created_at;

            $last = null;

            if (!empty($date_range)) {
                $dates = explode(' - ', $date_range);
                $end = Carbon::parse($dates[1]);
                $last = Carbon::parse($end);

                $time_column =$invoice->updated_at;

                if (!empty($status)) {
                    switch ($status) {
                        case 1:
                            $time_column = $invoice->created_at;
                            break;
                        case 3:
                            $time_column =$invoice->received_date;
                            break;
                    }
                }

                if (!empty($payment)) {
                    if ($payment == 1) {
                        $time_column = $invoice->paid_at;
                    }
                }
            }else{

                $last =  Carbon::today();

            }
            $first = Carbon::parse($invoice->created_at);
            $age = $last->diffInDays($first);
            if ($can_view) {
                $btn_show = "<a target='_blank' href='" . route('po.show', $invoice->id) . "'><i class='fa fa-file'></i> </a>";
            }else{
                $btn_show=[];
            }
            $brn_staus = $invoice->is_paid ? "<button class='btn btn-sm btn-success'>paid</button>" : "<button class='btn btn-sm btn-danger'>unpaid</button>";
            $btn_status = !empty($invoice->received_date) ? "<button class='btn btn-sm btn-primary'>received</button>" : "<button class='btn btn-sm btn-warning'>Pending</button>";
            $data[$i] = array(
                $invoice->id,
                Carbon::parse($time_column)->format('Y-m-d H:m:s'),
                $invoice->supplier->supplier_name,
                $invoice->sup_invoice_no.' ('.$invoice->invoice_no.')',
                number_format($invoice->total, 2),
                $age,
                $brn_staus,
                $btn_status,
                $btn_show,
            );
            $i++;
        }

        if ($invoiceCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($invoiceCount),
            "recordsFiltered" => intval($filterCount),
            "data" => $data,
            "total_sum" => number_format($total_sum, 2),
        ];

        return json_encode($json_data);
    }
}
