<?php

namespace App\Http\Controllers;

use App\LoadingData;
use App\LoadingDataVariant;
use App\LoadingHeader;
use App\User;
use App\AgentBuffer;
use Carbon\Carbon;
use BurdenManager\Models\Burden;
use EndProductManager\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use SalesOrderManager\Models\SalesOrder;
use SalesRepManager\Models\AgentCategory;
use SalesRepManager\Models\SalesRep;
use SalesOrderManager\Models\Content;
use RawMaterialManager\Models\RawMaterial;
use SalesOrderManager\Models\SalesVariant;
use SalesOrderManager\Models\RawContent;
use Illuminate\Support\Facades\DB;

class OrderListController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:order_list-index', ['only' => ['index', 'tableData']]);
        $this->middleware('permission:OrderList-list', ['only' => ['list']]);
        $this->middleware('permission:Order_list-Accept', ['only' => ['accept']]);
        $this->middleware('permission:Order_list-Approving', ['only' => ['approving']]);
        $this->middleware('permission:order_list-orderPreview', ['only' => ['orderPreview']]);
    }

    public function index()
    {
        $categories = Category::all()->pluck('name', 'id');
        $agents = SalesRep::all()->pluck('email', 'id');
        return view('order_list', compact('agents', 'categories'));
    }

    public function tableData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $recordsCount = 0;

        $date = $request->date_range;
        $columns = ['order_date', 'template_id', 'status', 'created_by'];
        $order_column = $columns[$order_by[0]['column']];

        $order_column = 'created_at';
        $order_by_str = 'DESC';
        $records = SalesOrder::tableData($order_column, $order_by_str, $start, $length);
        if (Auth::user()->hasRole(['Sales Agent'])) {
            $records = $records->where('created_by', '=', Auth::user()->id);
        }

        $filter_count = 0;
        if ($request->filter == true) {
            $date_range = $request->date_range;
            $category = $request->category;
            $day = $request->day;
            $time = $request->time;
            $status = $request->status;
            $agent = $request->agent;
            $records = $records->filterData($category, $day, $time, null, $agent, $status, $date_range)->get();
            $filter_count = $records->count();
            $salesOrdersCount = SalesOrder::all()->count();
        } elseif (is_null($search) || empty($search)) {
            if (Auth::user()->hasRole(['Sales Agent'])) {
                $records = $records->where('status', '<', 5)->get();
                $salesOrdersCount = SalesOrder::where('status', '<', 5)->count();
            } else {
                $records = $records->where('created_at', '=', Carbon::today())->get();
                $salesOrdersCount = SalesOrder::where('created_at', '=', Carbon::today())->count();
            }
        } else {
            $records = $records->searchData($search)->get();
            $salesOrdersCount = SalesOrder::all()->count()->count();
        }


        $recordsCount = $salesOrdersCount;


        $data = [];
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;

        $btn_status = null;
        $btn_status1 = null;
        $order_display_id = 0;
        $user = Auth::user();
        $OrderListApproving = ($user->can('Order_list-Approving')) ? 1 : 0;
        $OrderListAccept= ($user->can('Order_list-Accept')) ? 1 : 0;
        foreach ($records as $key => $record) {
            $btn_edit = null;
            $btn_agent_approve = null;
            $btn_admin_approve = null;
            $btn_download = null;
            $btn_preview = null;
            $btn_pending_preview = null;
            $btn_processing_preview = null;
            $btn_order_preview = null;
            $btn_preview_approving = null;
            $btn_order_preview_approving = null;
            $btn_preview_approved = null;
            $btn_preview_act_approved = null;
            $btn_crates_edit = null;
            $btn_discount_edit = null;

            $btn_wrong = null;

            if ($record->status == 1) {
                $order_display_id = $record->id;
                $btn_status = '<button class="btn btn-sm btn-warning">Pending</button>';
                $btn_pending_preview = "<a href='/order/list/show/" . $record->id . "' class='ml-2 btn btn-sm btn-primary'>Preview</a>";
                if (Auth::user()->hasRole(['Super Admin', 'Sales Agent'])) {
                    $btn_status = $btn_status;
                    $btn_edit = '<a href="/order/list/' . $record->id . '" class="btn btn-sm btn-primary" style="margin-left:10px;">Edit</a>';
                }
            } elseif ($record->status == 2) {
                $order_display_id = $record->id;
                $btn_status = "<button class='btn btn-sm btn-info'>Processing</button>";
                $btn_processing_preview = "<a href='/order/list/show/" . $record->id . "' class='ml-2 btn btn-sm btn-primary'>Preview</a>";
            } elseif ($record->status == 3) {
                $order_inv_id = $record->loadingHeader->id;
                $order_display_id = $record->loadingHeader->invoice_number;

                $itemsum1 = LoadingData::selectRaw('COALESCE(sum(total), 0) as itemtotal')->whereLoadingHeaderId($order_inv_id)->first();
                $itemsum2 = LoadingData::selectRaw('COALESCE(sum(total_discount), 0) as itemdis')->whereLoadingHeaderId($order_inv_id)->first();

                $itemvari1 = LoadingDataVariant::selectRaw('COALESCE(sum(total), 0) as varitotal')->whereLoadingHeaderId($order_inv_id)->first();
                $itemvari2 = LoadingDataVariant::selectRaw('COALESCE(sum(total_discount), 0) as varidis')->whereLoadingHeaderId($order_inv_id)->first();

                $itemtot = LoadingHeader::whereId($order_inv_id)->first();

                $tot1 = $itemtot->total;
                $tot2 = $itemtot->total_discount;

                $tot = (($tot1) - ($tot2));

                $load = (($itemsum1->itemtotal) - ($itemsum2->itemdis)) + (($itemvari1->varitotal) - ($itemvari2->varidis));
                $dif = $tot - $load;
                $diff = number_format(($dif), 0);

                $difval = number_format(($dif), 2);

                if ($diff == 0) {


                    $btn_status = "<button class='btn btn-sm btn-secondary'>Accepting</button>";
                    if (Auth::user()->hasRole(['Super Admin', 'Owner'])) {
                        $agentCategory = AgentCategory::where('sales_ref_id', '=', $record->created_by)->where('category_id', '=', $record->template->category->id)->first();
                        if (!empty($agentCategory) && $agentCategory->count() > 0) {
                            if ($agentCategory->discount_mode == 1) {
                                $btn_discount_edit = "<a href='" . route('loading.discountx', $record->loadingHeader->id) . "' class='btn btn-sm btn-danger m-1' style='margin-left:10px;'>Edit Discount</a>";
                            }
                        }
                        if ($OrderListAccept) {
                            $btn_agent_approve = "<a href='" . route('sales-request.show', $record->id) . "' class='btn btn-sm btn-success' style='margin-left:10px;'>Accept</a>";
                        }else{
                            $btn_agent_approve='';
                        }
                    }
                    if (Sentinel::check()->roles[0]->slug == 'owner' || Sentinel::check()->roles[0]->slug == 'stores-end-product') {
                        if($itemtot->status == 1 ){

                            $btn_status1 = "<button class='btn btn-sm btn-danger'>Something Went Wrong .....!</button>";
                            $agentCategory = AgentCategory::where('sales_ref_id', '=', $record->created_by)->where('category_id', '=', $record->template->category->id)->first();
                            if (!empty($agentCategory) && $agentCategory->count() > 0) {
                                if ($agentCategory->discount_mode == 1) {
                                    $btn_discount_edit = "<a href='" . route('loading.discountx', $record->loadingHeader->id) . "' class='btn btn-sm btn-primary m-1' style='margin-left:10px;'>Edit Discount</a>";
                                }
                            }
                        }else {
                            $btn_crates_edit = "<a href='" . route('crates-edit.admin', [$record->loadingHeader->id]) . "' class='ml-2 btn btn-sm btn-warning text-dark'>Edit Crates</a>";
                            $btn_preview = "<a href='" . route('order-list.preview', [$record->loadingHeader->id]) . "' class='ml-2 btn btn-sm btn-primary'>Preview</a>";
                            $btn_download = "<a href='" . route('loading.gpdf', $record->loadingHeader->id) . "' class='btn btn-sm btn-outline-danger' style='margin-left:10px;'>Print</a>";
                        }
                    }
                    if (Sentinel::check()->roles[0]->slug == 'sales-ref') {
                        if($itemtot->status == 1 ){

                            $btn_status1 = "<button class='btn btn-sm btn-danger'>Something Went Wrong .....!</button>";
                        }else {

                            $btn_preview = "<a href='" . route('order-list.preview', [$record->loadingHeader->id]) . "' class='ml-2 btn btn-sm btn-primary'>Preview</a>";
                            $btn_agent_approve = "<a href='" . route('sales-request.show', $record->id) . "' class='btn btn-sm btn-success' style='margin-left:10px;'>Accept</a>";
                        }
                    }
                    $btn_order_preview = "<a href='/order/list/show/" . $record->id . "' class='ml-2 btn btn-sm btn-outline-success'>Order<br> Preview</a>";

                } else {
                    $btn_status = "<button class='btn btn-sm btn-danger'>Something Went Wrong .....! [ Value of $difval ]</button>";
                    $btn_preview_approving = "<a href='" . route('order-list.preview', [$record->loadingHeader->id]) . "' class='ml-2 btn btn-sm btn-primary'>Preview</a>";
                    if (Auth::user()->hasRole(['Super Admin', 'Owner'])) {
                        $btn_wrong = "<a href='" . route('sales-wrong.show', [$record->loadingHeader->id]) . "' class='btn btn-sm btn-danger' style='margin-left:15px;'>Correction</a>";
                    }
                }
            } elseif ($record->status == 4) {
                $order_display_id = $record->loadingHeader->invoice_number;
                if ($OrderListApproving) {
                    $btn_status = "<button class='btn btn-sm btn-primary'>Approving</button>";
                }else{
                    $btn_status="";
                }
                if (Auth::user()->hasRole(['Sales Agent'])) {
                    $btn_admin_approve = "<a href='" . route('approve-list.edit', $record->id) . "' class='btn btn-sm btn-primary' style='margin-left:10px;'>Edit</a>";
                    $btn_order_preview_approving = "<a href='/order/list/show/" . $record->id . "' class='ml-2 btn btn-sm btn-outline-success'>Order<br> Preview</a>";
                    $btn_preview_approving = "<a href='" . route('order-list.preview', [$record->loadingHeader->id]) . "' class='ml-2 btn btn-sm btn-primary'>Preview</a>";
                }
                if (Auth::user()->hasRole(['Stores End Product'])) {
                    $btn_admin_approve = "<a href='" . route('approve-list.show', $record->id) . "' class='btn btn-sm btn-success' style='margin-left:10px;'>Approve</a>";
                }
                if (Auth::user()->hasRole(['Sales Manager', 'Owner'])) {
                    $btn_order_preview_approving = "<a href='/order/list/show/" . $record->id . "' class='ml-2 btn btn-sm btn-outline-success'>Order<br> Preview</a>";
                    $btn_preview_approving = "<a href='" . route('order-list.preview', [$record->loadingHeader->id]) . "' class='ml-2 btn btn-sm btn-primary'>Preview</a>";
                }
            } elseif ($record->status == 5) {
                $order_display_id = $record->loadingHeader->invoice_number;
                $btn_status = "<button class='btn btn-sm btn-success'>Approved</button>";
                if (Auth::user()->hasRole(['Super Admin', 'Owner'])) {
                    $btn_preview_approved = "<a href='" . route('order-list.preview', [$record->loadingHeader->id]) . "' class='ml-2 btn btn-sm btn-primary'>Preview</a>";
                    $btn_preview_act_approved = "<a href='" . route('approve-list.preview', $record->id) . "' class='ml-2 btn btn-sm btn-primary'>Act.View</a>";
                    $btn_download = "<a href='" . route('loading.gpdf', $record->loadingHeader->id) . "' class='btn btn-sm btn-outline-danger' style='margin-left:10px;'>Print</a>";
                    $btn_wrong = "<a href='" . route('sales-wrong.show', [$record->loadingHeader->id]) . "' class='btn btn-sm btn-danger' style='margin-left:15px;'>Correction</a>";
                    $agentCategory = AgentCategory::where('sales_ref_id', '=', $record->created_by)->where('category_id', '=', $record->template->category->id)->first();
                    if (!empty($agentCategory) && $agentCategory->count() > 0) {
                        if ($agentCategory->discount_mode == 1) {
                            $btn_discount_edit = "<a href='" . route('loading.discountx', $record->loadingHeader->id) . "' class='btn btn-sm btn-primary m-1' style='margin-left:10px;'>Edit Discount</a>";
                        }
                    }
                }
                if (Auth::user()->hasRole(['Stores End Product'])) {
                    $btn_preview_approved = "<a href='" . route('order-list.preview', [$record->loadingHeader->id]) . "' class='ml-2 btn btn-sm btn-primary'>Preview</a>";
                    $btn_preview_act_approved = "<a href='" . route('approve-list.preview', $record->id) . "' class='ml-2 btn btn-sm btn-primary'>Act.View</a>";
                }
                if (Auth::user()->hasRole(['Sales Agent'])) {
                    $btn_preview_act_approved = "<a href='" . route('approve-list.preview', $record->id) . "' class='ml-2 btn btn-sm btn-primary'>Act.View</a>";
                }
            }
            if (Auth::user()->hasRole(['Sales Agent'])) {
                $updateAt = $record->updated_at->toDateString();
                $todayAt = Carbon::now();
                if ($record->status == 5 && $updateAt !== $todayAt->toDateString()) {
                } else {
                    $data[$i] = array(
                        $order_display_id,
                        Carbon::parse($record->order_date)->format('Y-m-d H:m:s'),
                        $record->template->category->name,
                        $record->template->day . " - " . $record->template->time,
                        $btn_pending_preview . $btn_edit . $btn_processing_preview . $btn_agent_approve . $btn_download . $btn_preview . $btn_order_preview . $btn_admin_approve . $btn_preview_approving . $btn_order_preview_approving . $btn_preview_approved . $btn_preview_act_approved,
                        $btn_status . $btn_status1,
                        User::find($record->created_by)->email
                    );
                    $i++;
                }
            } else {
                $data[$i] = array(
                    $order_display_id,
                    Carbon::parse($record->order_date)->format('Y-m-d H:m:s'),
                    $record->template->category->name,
                    $record->template->day . " - " . $record->template->time,
                    $btn_status,
                    User::find($record->created_by)->email,
                    $btn_crates_edit . $btn_pending_preview . $btn_discount_edit . $btn_edit . $btn_processing_preview . $btn_agent_approve . $btn_download . $btn_preview . $btn_order_preview . $btn_admin_approve . $btn_preview_approving . $btn_order_preview_approving . $btn_preview_approved . $btn_preview_act_approved .$btn_wrong . $btn_status1,
                );
                $i++;
            }

            // $data[$i] = array(
            //       $order_display_id,
            //       $record->order_date,
            //       $record->template->category->name,
            //       $record->template->day." - ".$record->template->time,
            //       $btn_status,
            //       User::find($record->created_by)->email,
            //       $btn_pending_preview.$btn_edit .$btn_processing_preview. $btn_agent_approve . $btn_download . $btn_preview.$btn_order_preview.$btn_admin_approve.$btn_preview_approving.$btn_order_preview_approving.$btn_preview_approved.$btn_preview_act_approved,
            //   );

        }


        if ($recordsCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($recordsCount),
            "recordsFiltered" => intval($filter_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }

    public function orderPreview(Request $request, LoadingHeader $loadingHeader)
    {
        // $order = SalesOrder::whereId($order)->first();
        return view('order_list_inv', with(['loading' => $loadingHeader]));
    }

    public function orderPreview2(Request $request, LoadingHeader $loadingHeader)
    {
        return view('order_list_inv2', with(['loading' => $loadingHeader]));
    }

    public function editPendingRequest($id)
    {
        // var_dump($id);
        $items = Content::where('sales_order_header_id', '=', $id)->get();
        // foreach($items as $item){
        //     dd($item);
        // }
        $salesHeader = SalesOrder::where('id', '=', $id)->first();
        $bf = AgentBuffer::where('id', '=', $salesHeader['template_id'])->first();
        $all_raw_materials = RawMaterial::withoutGlobalScope('type')->whereType(4)->get();
        $raw_materials = RawContent::where('sales_order_header_id', $id)->get();
        // foreach($raw_materials as $raw){
        //     dd($raw->qty);
        // }
        return view('edit_pending_request', compact('id', 'items', 'raw_materials', 'salesHeader', 'bf', 'all_raw_materials'));
    }

    public function updatePendingRequest(Request $request, $id)
    {
        $sales_ref = Auth::user()->id;

        $endProductsId = $request->end_product_id;
        $endProductsName = $request->end_product_name;
        $qtys = $request->end_product_qty;
        $qtys_x = $request->extra_qty;
        $available_qty = $request->available_qty;


        $templateId = $request->template_id;
        $orderDate = $request->order_date;


        //raw material outlet items
        $raw_materials = $request->raw_materials;
        $r_qty = $request->r_qty;


        $description = $request->description;

        $variants = $request->variant;
        $variant_qty = $request->variant_qty;
        $variant_extra_qty = $request->variant_extra_qty;
        $variant_available_qty = $request->variant_available_qty;

        $so = SalesOrder::where('id', '=', $id)->first();
        $items = Content::where('sales_order_header_id', '=', $id)->get();
        // dd($so);

        return view('preview_edit_pending_request', compact('id', 'items', 'endProductsId', 'endProductsName', 'qtys', 'qtys_x', 'available_qty', 'orderDate', 'templateId', 'raw_materials', 'r_qty', 'description', 'so', 'variants', 'variant_qty', 'variant_extra_qty', 'variant_available_qty'));

    }

    public function replacePendingRequest(Request $request, $id)
    {
        $records = SalesOrder::where('id', '=', $id)->first();
        if ($records->status != 1) {
            return redirect(route('pendingOrder.edit', $id))->with([
                'errorMessage' => 'Yes'
            ]);
        } else {

            $endProductsId = $request->end_product_id;
            $endProductsName = $request->end_product_name;
            $qtys = $request->end_product_qty;
            $qtys_x = $request->extra_qty;
            $available_qty = $request->available_qty;
            $description = $request->description;


            $qtys_x_variant = $request->extra_qty_varient;
            $available_qty_variant = $request->available_qty_varient;

            var_dump($request->extra_qty_varient);
            $items = Content::where('sales_order_header_id', '=', $id)->get();
            $indexProduct = 0;
            foreach ($endProductsId as $key => $value) {
                $updateItemContent = Content::where('end_product_end_product_id', '=', $value)->where('sales_order_header_id', '=', $id)->first();
                $updateItemContent->extra_qty = $qtys_x[$key];
                $updateItemContent->available_qty = $available_qty[$key];
                $updateItemContent->save();
            }


            $templateId = $request->template_id;
            $orderDate = $request->order_date;


            //raw material outlet items
            $raw_materials = $request->raw_materialsUpdate;
            $r_qty = $request->r_qtyUpdate;
            $key = 0;
            if (!empty($raw_materials)) {
                foreach ($raw_materials as $raw_material) {
                    $sod = new RawContent();
                    $sod->sales_order_header_id = $id;
                    $sod->raw_material_id = $raw_material;
                    $sod->qty = $r_qty[$key];
                    $sod->save();
                    $key++;
                }

            }

            if ($description != Null) {
                $order = SalesOrder::where('id', '=', $id)->first();
                $order->description = $description;
                $order->save();
            }
            if (!empty($request->variant_id)) {
                foreach ($request->variant_id as $key => $variant) {
                    $variantUpdate = SalesVariant::where('sales_order_header_id', '=', $id)
                        ->where('variant_id', '=', $variant)->first();
                    $variantUpdate->extra_qty = $qtys_x_variant[$key];
                    $variantUpdate->available_qty = $available_qty_variant[$key];
                    $variantUpdate->save();
                    // var_dump($qtys_x_variant[$key]);
                }
            }
            return redirect(route('pendingOrder.edit', $id))->with([
                'errorMessage' => 'No'
            ]);

        }


    }

    function showPendingRequest($id)
    {
        $items = Content::where('sales_order_header_id', '=', $id)->get();
        $salesHeader = SalesOrder::where('id', '=', $id)->first();
        $bf = AgentBuffer::where('id', '=', $salesHeader['template_id'])->first();
        $all_raw_materials = RawMaterial::withoutGlobalScope('type')->whereType(4)->get();
        $raw_materials = RawContent::where('sales_order_header_id', $id)->get();

        return view('show_pending_request', compact('id', 'items', 'raw_materials', 'salesHeader', 'bf', 'all_raw_materials'));
    }
}
