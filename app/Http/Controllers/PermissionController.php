<?php

namespace App\Http\Controllers;

use App\Permission;
use App\PermissionGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:permission-index', ['only' => ['index','tableData']]);
        $this->middleware('permission:permission-list', ['only' => ['list']]);
        $this->middleware('permission:permission-create', ['only' => ['create','store']]);
        $this->middleware('permission:permission-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:permission-delete', ['only' => ['destroy']]);
    }
    public function index()
    {
        $permissionGroups = PermissionGroup::pluck('name','id')->all();
        return view('permission.index',compact('permissionGroups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'permission_group' => 'required',
        ]);
        $permission = new Permission();
        $permission->name = $request->name;
        $permission->permission_group_id = $request->permission_group;
        $permission->guard_name = "web";
        $permission->save();

        $msg = 'Permission Created Successfully';
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Permission $permission)
    {
        $this->validate($request, [
            'name' => 'required',
            'permission_group' => 'required',
        ]);
        $permission->name = $request->name;
        $permission->permission_group_id = $request->permission_group;
        $permission->guard_name = "web";
        $permission->save();

        $msg = 'Permission Updated Successfully';
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $permission = Permission::find($request->id);
        $permission->delete();
        return 'true';
    }

    public function tableData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $recordsCount = 0;

        $date = $request->date_range;
        $columns = ['id','name','permission_group_id'];
        $order_column = $columns[$order_by[0]['column']];

        // $records = EndProductBurden::tableData($order_column, $order_by_str, $start, $length);
        // $records = $records->where('status', '!=', '5')->orderBy('status');


        $records = Permission::query();


        if (is_null($search) || empty($search)) {
            $recordsCount = $records->count();
            $records = $records->tableData($order_column, $order_by_str, $start, $length)->get();
        } else {
            $recordsCount = $records->count();
            $records = $records->searchData($search)->tableData($order_column, $order_by_str, $start, $length)->get();
        }

//        $recordsCount = $salesOrdersCount;

        $data[][] = array();
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;

        $btn_status = null;

        $condition = (!empty($request->category)) ? 1 : 0;

        $btn_approve = null;

        $user = Auth::user();
        $can_edit = ($user->can('permission-edit')) ? 1 : 0;
        $can_delete = ($user->can('permission-delete')) ? 1 : 0;
        foreach ($records as $key => $record) {
            if ($can_edit) {
                $edit_btn = "<button onclick=\"edit(this)\" data-id='{$record->id}' data-name='{$record->name}' data-permission_group='{$record->permission_group_id}' class='addItem btn btn-sm btn-warning ml-1' style='margin-right: 5px'><i class='fa fa-edit'></i></button>";
            }else{
                $edit_btn=[];
            }
            if ($can_delete) {
                $delete_btn = "<button class='btn btn-xs btn-icon btn-light-danger mr-2' onclick=\"deletepermission('$record->id')\"> <i class='fa fa-trash'></i></button>";
            }else{
                $delete_btn=[];
            }


            $data[$i] = array(
                $record->id,
                $record->permissionGroup->name,
                $record->name,
                $edit_btn . $delete_btn
            );
            $i++;
        }


        if ($recordsCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($recordsCount),
            "recordsFiltered" => intval($recordsCount),
            "data" => $data
        ];

        return json_encode($json_data);
    }

}
