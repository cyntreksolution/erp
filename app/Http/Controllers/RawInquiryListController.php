<?php
/**
 * Created by PhpStorm.
 * User: yaish
 * Date: 10/15/2020
 * Time: 10:23 PM
 */

namespace App\Http\Controllers;

use App\CrateInquiryStore;
use App\Crates;
use App\LoadingDataRaw;
use App\RawIssueDataList;
use App\ReusableUsage;
use App\Traits\CratesStoreLog;
use App\Traits\RawMaterialLog;
use BurdenManager\Models\Burden;
use Carbon\Carbon;
use App\RawInquiry;
use EmployeeManager\Models\Employee;
use EndProductBurdenManager\Models\EndProductBurden;
use EndProductBurdenManager\Models\EndProductBurdenHeader;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PurchasingOrderManager\Models\PurchasingOrder;
use RawMaterialManager\Models\RawMaterial;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use RawMaterialManager\Models\RawRequestTable;
use ShortEatsManager\Models\CookingRequest;
use StockManager\Classes\StockTransaction;

class RawInquiryListController extends Controller
{
    use RawMaterialLog;

    public function index()
    {
        $employees = Employee::get();
        $raw_materials = RawMaterial::all()->pluck('name', 'raw_material_id');

        return view('raw_inquiry_list', compact('raw_materials','employees'));
    }

    public function tableData(Request $request)
    {
   //     dd($request);

        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'created_at'];
        $order_column = $columns[$order_by[0]['column']];


        $stat_data =$rawmaterials= RawInquiry::query();
        $rawmaterialsCount = 0;
        $allSum = null;

        if ($request->filter == true) {
            $raw_material = $request->raw_material;
            $date_range = $request->date_range;
            $desc_id = $request->reference_type;
            $emp_id = $request->emp_id;


            if($emp_id == '') {
                $allSum = $stat_data->filterData($raw_material, $desc_id, $date_range)
                    ->select(DB::raw('SUM(raw_inquiry.amount) as  amount_sum'), DB::raw('SUM(raw_inquiry.qty) as qty_sum'))
                    ->first();
                $rawmaterialsCount = $rawmaterials->count();
                $rawmaterials = RawInquiry::filterData($raw_material, $desc_id, $date_range)->tableData($order_column, $order_by_str, $start, $length)->orderBy('id', 'asc')->get();

            }else{
                $allSum = $stat_data->filterData($raw_material, $desc_id, $date_range)
                    ->select(DB::raw('SUM(raw_inquiry.amount) as  amount_sum'), DB::raw('SUM(raw_inquiry.qty) as qty_sum'))
                    ->where('Issued_ref_id','=',$emp_id)
                    ->first();
                $rawmaterialsCount = $rawmaterials->count();
                $rawmaterials = RawInquiry::filterData($raw_material, $desc_id, $date_range)->where('Issued_ref_id','=',$emp_id)->tableData($order_column, $order_by_str, $start, $length)->orderBy('id', 'asc')->get();

            }

        }

        if (!empty($allSum['amount_sum'])) {
            $sumAmount = $allSum['amount_sum'];
        } else {
            $sumAmount = null;
        }
        if (!empty($allSum['qty_sum'])) {
            $sumQty = $allSum['qty_sum'];
        } else {
            $sumQty = null;
        }
        $data[][] = array();
        $i = 0;

        if ((!empty($date_range)) && (!empty($raw_material)) && (empty($desc_id))) {
            $openningBalance = $rawmaterials->sortBy('created_at')->first();
            $openningBalanceStart = !empty($openningBalance)?$openningBalance['start_balance']:0;
        } else {
            $openningBalanceStart = null;
        }

        $chk = "";
        foreach ($rawmaterials as $key => $rawmaterial) {

          //  $chkdouble = RawInquiry::where('raw_material_id','=',$rawmaterial->raw_material_id)->where('desc_id','=',$rawmaterial->desc_id)->where('reference','=',$rawmaterial->reference)->get();
            $chkdouble = RawInquiry::where('raw_material_id','=',$rawmaterial->raw_material_id)->where('desc_id','=',$rawmaterial->desc_id)->where('reference','=',$rawmaterial->reference)->where('Issued_ref_id','=',$rawmaterial->Issued_ref_id)->get();

            $approving_filter = !empty($approving_orders) ? 1 : 0;
            $description = '';

            $emp = null;
            if($rawmaterial->desc_id == 6){
                $emp1 = Employee::whereId($rawmaterial->Issued_ref_id)->first();
                $emp = ' [ '. $emp1->full_name .' ]';
            }

            if ((!empty($raw_material)) && (!empty($date_range)) && (empty($desc_id))) {
                $startBalance = number_format($rawmaterial->start_balance, 2);
                $endBalance = number_format($rawmaterial->end_balance, 2);
            } else {
                $startBalance = null;
                $endBalance = null;
            }
            if($rawmaterial->is_approved == 1){
                $chk = '<a class="btn btn-danger btn-sm mr-1" href="#">Approved Here</a>';
            }else{
                $chk = "";
            }

            $refBtn = "";

            if($chkdouble->count() > 1){
                $isin = RawInquiry::where('reference','=',$rawmaterial->id)->where('desc_id','=',22)->first();


                if(!empty($isin->reference)){
                    $refBtn = '<a class="btn btn-warning btn-sm mr-1" href="#">' . $rawmaterial->reference . '</a>'."<a title='Reverse' href='#' class='btn btn-info btn-sm mr-1' >Reversed</a>";

                }else{
                    if (Sentinel::check()->roles[0]->slug == 'owner') {
                        $refBtn = '<a class="btn btn-warning btn-sm mr-1" href="#">' . $rawmaterial->reference . '</a>'."<a title='Reverse' href='" . route('issued-raw.revers', [ 'id' => $rawmaterial->id]) . "' class='ml-3 btn btn-danger mr-2' ><i class='fa fa-angle-double-left'></i> </a>";

                    }else{
                        $refBtn = '<a class="btn btn-warning btn-sm mr-1" href="#">' . $rawmaterial->reference . '</a>'."<a title='Reverse' href='#' class='ml-3 btn btn-info mr-2' ><i class='fa fa-angle-double-left'></i> </a>";

                    }

                }

            }else{

                $refBtn = '<a class="btn btn-warning btn-sm mr-1" href="#">' . $rawmaterial->reference . '</a>';

            }


            $data[$i] = array(
                //$rawmaterial->id,
                Carbon::parse($rawmaterial->created_at)->format('Y-m-d H:i:s'),
                optional($rawmaterial->raw_material)->name,
                $rawmaterial->description.$emp,
               // number_format($rawmaterial->start_balance, 3),
                $startBalance,
                number_format($rawmaterial->qty, 3),
              //  number_format($rawmaterial->price, 2),
                number_format(($rawmaterial->amount), 2),
                $endBalance,
               // '',$referenceLinkBtn,
                $chk.$refBtn
            );
            $i++;
        }

        if ($rawmaterialsCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($rawmaterialsCount),
            "recordsFiltered" => intval($rawmaterialsCount),
            "data" => $data,
            "qty_sum" => number_format($sumQty, 2),
            "amount_sum" => number_format($sumAmount, 2),
            "openning_balance" => number_format($openningBalanceStart, 2),
        ];

        return json_encode($json_data);
    }

    public function multipleData(Request $request)
    {
        $date_range = $request->date_range;
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $raw_material = $request->raw_material;
        $date_range = $request->date_range;
        $desc_id = $request->reference_type;
        $emp_id = $request->emp_id;


        $columns = ['id', 'created_at'];
        $rawmaterialsCount = 0;

        $data = [];
        $i = 0;


        $raws = RawInquiry::multifilterData($raw_material, $desc_id, $date_range)->orderBy('id', 'asc')->get();



        $chk = "";


        foreach ($raws as $key => $rawmaterial) {

            $rawmaterialsCount = $raws->count();

            $chkdouble = RawInquiry::where('raw_material_id','=',$rawmaterial->raw_material_id)->where('desc_id','=',$rawmaterial->desc_id)->where('reference','=',$rawmaterial->reference)->where('Issued_ref_id','=',$rawmaterial->Issued_ref_id)->get()->count();

            $emp = null;

            $refBtn = "";


            if($chkdouble > 1){

                $isin = RawInquiry::where('reference','=',$rawmaterial->id)->where('desc_id','=',22)->first();

                $val = "Reverse-".$rawmaterial->reference."[".$rawmaterial->id."]";
               if(!empty($isin->reference)){
                    $refBtn =  "<a title='Reverse' href='#' class='btn btn-info btn-sm mr-1' >$val</a>";

               }else{
                    if (Sentinel::check()->roles[0]->slug == 'owner') {
                        $refBtn = "<a title='Reverse' href='" . route('issued-raw.revers', [ 'id' => $rawmaterial->id]) . "' class='ml-3 btn btn-danger mr-2' >$val</a>";

                    }else{
                        $refBtn = "<a title='Reverse' href='#' class='ml-3 btn btn-info mr-2' >$val</a>";

                    }

               }




                $data[$i] = array(
                    Carbon::parse($rawmaterial->created_at)->format('Y-m-d H:i:s'),
                    optional($rawmaterial->raw_material)->name,
                    $rawmaterial->description.$emp,
                    '',
                    number_format($rawmaterial->qty, 3),
                    number_format(($rawmaterial->amount), 2),
                    '',
                    $chk.$refBtn
                );
                $i++;
            }

        }



        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($rawmaterialsCount),
            "recordsFiltered" => intval($rawmaterialsCount),
            "data" => $data,
          //  "qty_sum" => number_format($sumQty, 2),
         //   "amount_sum" => number_format($sumAmount, 2),
         //   "openning_balance" => number_format($openningBalanceStart, 2),
        ];

        return json_encode($json_data);
    }




    public function reverse(Request $request)
    {
        $rawitem = RawInquiry::find($request->id);

        StockTransaction::rawMaterialStockTransaction(1, $rawitem->price, $rawitem->raw_material_id, (($rawitem->qty) * (-1)), $rawitem->id, 1);
        StockTransaction::updateRawMaterialAvailableQty(1, $rawitem->raw_material_id, (($rawitem->qty) * (-1)));
        $raw_material = RawMaterial::find($rawitem->raw_material_id);
        $this->recordRawMaterialOutstanding($raw_material, (($rawitem->qty) * (-1)), 'Reverse Raw', $request->id , 'App\\RawIssueDataList\\Reverse', 22, 1, $request->id);

        return redirect()->back()->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => 'Raw Material Successfully Reversed.'
        ]);

        /*
                $type = $request->type;

                $raw_mat_id = null;
                if ($type == 1) {
                    $desc = 'Production Purpose';
                    $desc_id = 7;
                    $raw_mat_id = $request->raw_id;
                    $request_qty = $request->qty;
                    $description = $request->ref;

                } elseif ($type == 2) {
                    $desc = 'Customized Cake';
                    $desc_id = 8;
                    $raw_mat_id = $request->raw_id;
                    $request_qty = $request->qty;
                    $description = $request->ref;
                }
                $rc = RawIssueDataList::where('raw_id','=',$raw_mat_id)->where('description','=',$description)->first();

                $record = new RawIssueDataList();
                $record->raw_id = $raw_mat_id;
                $record->type = $type;
                $record->qty = $request->qty;
                $record->description = $description;
                $record->created_by = Sentinel::getUser()->id;


                if(empty($rc->employee_id)) {

                    $record->save();


                    StockTransaction::rawMaterialStockTransaction(2, 0, $raw_mat_id, $request_qty, 0, 1);
                    StockTransaction::updateRawMaterialAvailableQty(2, $raw_mat_id, $request_qty);
                    $raw_material = RawMaterial::find($raw_mat_id);
                    $this->recordRawMaterialOutstanding($raw_material, (($request_qty) * (-1)), $desc, $record->id, 'App\\RawIssueDataList', $desc_id, 2, $description);


                    $raw = RawRequestTable::where('ref', '=', $description)->get();

                    foreach ($raw as $raws) {

                        $raws->status = 1;
                        $raws->measurer_id = $request->emp;
                        $raws->save();
                    }



                    return redirect()->back()->with([
                        'success' => true,
                        'success.title' => 'Congratulations !',
                        'success.message' => 'Raw Material Released !'
                    ]);
                }else{
                    return redirect()->back()->with([
                        'danger' => true,
                        'danger.title' => 'Oh.. My God !',
                        'danger.message' => 'Already Exsist Entry'
                    ]);
                }
        */

    }



}