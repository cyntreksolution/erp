<?php

namespace App\Http\Controllers;

use App\AgentInquiry;
use App\AgentPayment;
use App\EndProductBurdenGrade;
use App\Jobs\CheckQueue;
use App\Jobs\SalesApiStockUpdateJob;
use App\LoadingHeader;
use App\Mail\MailCheck;
use App\ReusableStock;
use App\SemiBurdenIssueData;
use App\SemiInquiry;
use App\SemiIssueDataList;
use App\Services\APIService;
use App\Traits\SendSMS;
use Barryvdh\DomPDF\Facade as PDF;
use BurdenManager\Models\Burden;
use BurdenManager\Models\ReusableProducts;
use EndProductBurdenManager\Models\EndProductBurden;
use EndProductBurdenManager\Models\EndProductBurdenHeader;
use EndProductManager\Models\EndProduct;
use Ichtrojan\Otp\Otp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Psy\Test\Exception\RuntimeExceptionTest;
use EndProductRecipeManager\Models\EndProductRecipe;
use PurchasingOrderManager\Models\PurchasingOrder;
use RecipeManager\Models\RecipeSemiProduct;
use RecipeManager\Models\RecipeContent;
use RawMaterialManager\Models\RawMaterial;
use SalesRepManager\Models\SalesRep;
use SemiFinishProductManager\Models\SemiFinishProduct;

class DebugController extends Controller
{
    use SendSMS;

    private $client;

    public function index(Request $request)
    {
//        $order_id =19268;
//        $client = new APIService();
//        $client->sendLoadingDataToAPI($order);
//        CheckQueue::dispatch()->delay(now()->addMinute());
//        Mail::to("ccdilan@gmail.com")->queue(new MailCheck());
//        dd(1);

//        SalesApiStockUpdateJob::dispatch($order_id);

//        $this->sendToAPI($data);

        return $this->syncAgents();
    }

    //dont remove
    public function loadingToAPI(Request $request)
    {

        $invoice_number = $request->invoice_number;
        $order = LoadingHeader::where('invoice_number','=', $invoice_number)->with(['items','variants','salesOrder'])->first();

        if (empty($order)) {
            return "invalid invoice number : #$invoice_number";
        }
        if ($order->is_fetched_to_api) {
            return "already sync";
        }else{
            $order['agent_id'] = $order->salesOrder->created_by;
            $client = new APIService();
            $client->sendLoadingDataToAPI($order);

            $orderx = LoadingHeader::where('invoice_number','=', $invoice_number)->with(['items','variants','salesOrder'])->first();
            $orderx->is_fetched_to_api=1;
            $orderx->save();
            return "order sync invoice no:  #$invoice_number";
        }


    }

    public function syncAgents(){
        dd(1);
    }
}
