<?php

namespace App\Http\Controllers;

use App\LoadingDataRaw;
use App\LoadingHeader;
use App\Traits\RawMaterialLog;
use Carbon\Carbon;
use EndProductManager\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use LoadingManager\Models\Loading;
use RawMaterialManager\Models\RawMaterial;
use SalesOrderManager\Models\RawContent;
use SalesOrderManager\Models\SalesOrder;
use SalesRepManager\Models\SalesRep;
use StockManager\Classes\StockTransaction;

class ShopItemIssueController extends Controller
{
    use RawMaterialLog;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:shop_item_issue-index', ['only' => ['index','tableData']]);
        $this->middleware('permission:IssueShopItems-list', ['only' => ['list']]);
        $this->middleware('permission:shop_item_issue-show', ['only' => ['index','show']]);


    }
    public function index()
    {
        $categories = Category::all()->pluck('name', 'id');
        $agents = SalesRep::all()->pluck('name_with_initials', 'id');
        return view('shop-items.table', compact('categories', 'agents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $loading= LoadingHeader::find($id);
        return  view('shop-items.item-table',compact('loading'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function tableData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'serial', 'semi_finish_product_id', 'status', 'burden_size', 'recipe_id', 'created_at'];
        $order_column = $columns[$order_by[0]['column']];

        $invoices = LoadingHeader::tableData($order_column, $order_by_str, $start, $length)->whereStatus(1);
        if ($request->filter == true) {
            $time = $request->time;
            $day = $request->day;
            $agent = $request->agent;
            $category = $request->category;
            $date_range = $request->date_range;
            $invoices = $invoices->filterData($date_range, $agent, $category, $day, $time)->get();
            $invoiceCount = $invoices->count();
        } elseif (is_null($search) || empty($search)) {
            $invoices = $invoices->get();
            $invoiceCount = LoadingHeader::whereStatus(1)->get()->count();
        } else {
            $invoices = $invoices->searchData($search)->get();
            $invoiceCount = $invoices->count();
        }

        $data=[];
        $i = 0;
        $user = Auth::user();
        $can_view = ($user->can('shop_item_issue-show')) ? 1 : 0;


        foreach ($invoices as $key => $invoice) {
            $inv = LoadingHeader::find($invoice->id);
            if (!$inv->isShopItemsLoaded()){
                $rep = $invoice->salesOrder->salesRep;
                $statusBtn = null;
                if ($can_view) {
                    $btn3 = "<a title='Issue Shop Items'  href='" . route('shop-item-issue.show', $invoice->id) . "' class='btn btn-outline-primary ml-3'><i class='fa fa-plane'></i> </a>";
                }
                $data[$i] = array(
                    $invoice->id,
                    Carbon::parse($invoice->created_at)->format('Y-m-d H:m:s'),
                    $invoice->salesOrder->template->category->name,
                    $invoice->salesOrder->template->day,
                    $invoice->salesOrder->template->time,
                    $invoice->salesOrder->salesRep->territory,
                    $rep->name_with_initials,
                    $btn3
                );
                $i++;
            }
        }

        if ($invoiceCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($invoiceCount),
            "recordsFiltered" => intval($invoiceCount),
            "data" => $data
        ];

        return json_encode($json_data);
    }

    public function issueRawMaterial(Request $request,LoadingDataRaw $LoadingDataRaw){

        $status = $LoadingDataRaw->status;
        $qty = $request->qty;
        $item=RawMaterial::find($LoadingDataRaw->raw_material_id);

        $ag=LoadingHeader::where('id','=',$LoadingDataRaw->loading_header_id)->first();
        $ag_id=RawContent::where('sales_order_header_id','=',$ag->so_id)->first();

        if ($status==0){
            if ($item->available_qty < $qty || $qty<0){
                return '0';
            }
            $newStatus = 1;
            StockTransaction::rawMaterialStockTransaction(2, 0, $LoadingDataRaw->raw_material_id, $qty, 0, 1);
            StockTransaction::updateRawMaterialAvailableQty(2, $LoadingDataRaw->raw_material_id, $qty);
            $item=RawMaterial::find($LoadingDataRaw->raw_material_id);
            $this->recordRawMaterialOutstanding($item, (($qty)*(-1)), 'Shop Item Issue Loading', $LoadingDataRaw->id, 'App\\LoadingDataRaw',12,2,$ag_id->agent_id);

        }elseif ($status==1){
            $newStatus = 0;
            $qty= $LoadingDataRaw->admin_approved_qty;
            StockTransaction::rawMaterialStockTransaction(1, 0, $LoadingDataRaw->raw_material_id, $qty, 0, 1);
            StockTransaction::updateRawMaterialAvailableQty(1, $LoadingDataRaw->raw_material_id, $qty);
            $item=RawMaterial::find($LoadingDataRaw->raw_material_id);
            $this->recordRawMaterialOutstanding($item, $qty, 'Shop Item Issue Unloading', $LoadingDataRaw->id, 'App\\LoadingDataRaw',12,1,$ag_id->agent_id);

        }
        $LoadingDataRaw->qty =$qty;
        $LoadingDataRaw->agent_approved_qty =$qty;
        $LoadingDataRaw->admin_approved_qty =$qty;
        $LoadingDataRaw->status =$newStatus;
        $LoadingDataRaw->save();


        return $LoadingDataRaw;
    }
}
