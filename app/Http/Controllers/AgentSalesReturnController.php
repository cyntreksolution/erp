<?php

namespace App\Http\Controllers;


use App\AgentBuffer;
use App\LoadingData;
use App\LoadingDataVariant;
use App\SalesReturnData;
use App\SalesReturnHeader;
use App\Variant;
use Carbon\Carbon;
use EndProductManager\Models\EndProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use SalesOrderManager\Models\SalesOrder;
use SalesRepManager\Models\AgentCategory;
use SalesRepManager\Models\SalesRep;

class AgentSalesReturnController extends Controller
{
    public function index()
    {
        $agent = Sentinel::getUser()->id;
        $categories =AgentCategory::where('sales_ref_id',$agent)->where('status',1)->pluck('category_id');

        $end_products = EndProduct::whereIn('category_id', $categories)->where('status',1)->orderBy('category_id')->get();
        return view('sales_return_ne', compact('end_products'));
    }

    public function agentReturn(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'end_product_ids' => 'required',
            'unit_values' => 'required',
            'qty' => 'required',
            'send_qty' => 'required',
            'time' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->with([
                    'error' => true,
                    'error.title' => 'Error !',
                    'error.message' => 'Please Add Items Before Create Return Request'
                ]);
        }

        $agent_id = Auth::user()->id;
        $end_product_ids = $request->end_product_ids;
        $unit_values = $request->unit_values;;
        $type = $request->type;;
        $qty = $request->send_qty;
        $all_qty = $request->qty;
        $time = $request->time;
        $total_qty = 0;
        $total_value = $request->total_value;
        $all_total_value = $request->all_total_value;

        $sx = SalesReturnHeader::whereAgentId($agent_id)->whereStatus(1)->where('time', '=', AgentBuffer::time[$time])->first();

        if (!empty($sx)) {
            return redirect(route('sales_return.index'))->with([
                'warning' => true,
                'warning.title' => 'Something Went Wrong !',
                'warning.message' => 'You have un approved sales return.Please contact system administrator'
            ]);
        }

        if (!empty($agent_id)) {
            $td = Carbon::today()->format('Y-m-d');
            $isin = SalesReturnHeader::whereAgentId($agent_id)->where('time', '=', AgentBuffer::time[$time])->where('created_at','like',$td.'%')->where('is_system_generated','!=',1)->first();

            if(empty($isin)) {
                DB::transaction(function () use ($agent_id, $total_value, $all_total_value, $end_product_ids, $type, $total_qty, $unit_values, $qty, $time, $all_qty) {
                    $sales_return = SalesReturnHeader::create([
                        'agent_id' => $agent_id,
                        'total' => $total_value,
                        'all_total' => $all_total_value,
                        'time' => $time,
                        'debit_note_number' => 'SR-' . Carbon::now()->format('ymdHis')
                    ]);
                    foreach ($end_product_ids as $key => $end_product_id) {
                        $item = SalesReturnData::create([
                            'sales_return_header_id' => $sales_return->id,
                            'end_product_id' => $end_product_id,
                            'type' => $type[$key],
                            'unit_value' => $unit_values[$key] != null ? $unit_values[$key] : 0,
                            'qty' => $qty[$key],
                            'all_qty' => $all_qty[$key],
                            'total' => (($unit_values[$key]) * ($qty[$key]))
                        ]);

                        $endP = EndProduct::find($end_product_id);
                        $total_qty += $qty[$key];
                        $percentage = $this->calPercentage($end_product_id);
                        $item->return_stat = $percentage;
                        $item->category_id = $endP->category_id;
                        $item->agent_id = $agent_id;
                        $item->save();



                    }
                    $sales_return->save();

                    return redirect(route('sales_return.index'))->with([
                        'success' => true,
                        'success.title' => 'Congratulations !',
                        'success.message' => 'Sales return created!'
                    ]);
                });
                return redirect(route('sales_return.index'))->with([
                    'error' => true,
                    'error.title' => 'Error !',
                    'error.message' => 'Something Went wrong'
                ]);
            }
        }

    }

    function calPercentage($end_product_id)//in SalesReturnController
    {
        $variant_size = 0;
        $endProduct = EndProduct::where('id', '=', $end_product_id)->first();
        $varainat = Variant::where('end_product_id', '=', $end_product_id)->where('return_accepted', '=', 1)->first();


        $last_seven_days_total_ordered_qty = LoadingData::where('end_product_id', '=', $end_product_id)->whereHas('header.salesOrder', function ($q) {
            return $q->where('created_by', '=', Auth::user()->id)->where('deleted_at', '=', null);
        })->where('created_at', '>', Carbon::now()->subDays(7)->format('Y-m-d H:i:s'))->sum('qty');

        $last_seven_days_total_ordered_vari_qty = LoadingDataVariant::where('end_product_id', '=', $end_product_id)->whereHas('header.salesOrder', function ($q) {
            return $q->where('created_by', '=', Auth::user()->id)->where('deleted_at', '=', null);
        })->where('created_at', '>', Carbon::now()->subDays(7)->format('Y-m-d H:i:s'))->sum('weight');


        $last_seven_days_total_returned_qty = SalesReturnData::where('end_product_id', '=', $end_product_id)->whereHas('header', function ($q) {
            return $q->where('agent_id', '=', Auth::user()->id)->where('deleted_at', '=', null);
        })->where('created_at', '>', Carbon::now()->subDays(7)->format('Y-m-d H:i:s'))->sum('all_qty');

        $percentage = 0;


        if (!empty($varainat) && $varainat->count() > 0) {
            $variant_size = $varainat->size;
            if ($last_seven_days_total_ordered_vari_qty != 0) {
                $percentage = ($last_seven_days_total_returned_qty / ($last_seven_days_total_ordered_vari_qty * $variant_size)) * 100;
            }
        } else {
            if ($last_seven_days_total_ordered_qty != 0) {
                $percentage = ($last_seven_days_total_returned_qty / $last_seven_days_total_ordered_qty) * 100;
            }
        }

        return $percentage;
    }
}
