<?php
namespace App\Http\Controllers;

use App\Outlet;
use Carbon\Carbon;
use CashLockerManager\Models\CashBookTransaction;
use CashLockerManager\Models\CashLocker;
use CashLockerManager\Models\main_desc_cashlocker;

use EmployeeManager\Models\Employee;
use Illuminate\Http\Request;
use SalesRepManager\Models\SalesRep;

class OutletController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:outlet-index', ['only' => ['show']]);
        $this->middleware('permission:AddOutlet-list', ['only' => ['list']]);
        $this->middleware('permission:outlet-create', ['only' => ['create']]);
    }
    public function show()
    {

        $datas = Outlet::all();
        $agents = SalesRep::all();

        return view('outlet', compact('agents', 'datas'));
    }



    public function create(Request $req)
    {
        $main = new Outlet();

        $main->name = $req->oname;
        $main->address = $req->address;
        $main->manage_by = $req->agent_id;
        $main->owner_name = $req->owner;
        $main->owner_tp = $req->ownertp;
        $main->type = $req->otype;
        $main->key_money = $req->keymoney;
        $main->rented_period = $req->rent_period;
        $main->first_rent_date = $req->open_date;
        $main->remark = $req->remark;
        $main->save();

        $datas = Outlet::all();
        $agents = SalesRep::all();

        return view('outlet', compact('agents', 'datas'));
    }

    public function outupdate(Request $req)
    {
        $id = $req->id;
        $name = $req->oname;
        $address = $req->address;
        $manage_by = $req->agent_id;
        $owner_name = $req->owner;
        $owner_tp = $req->ownertp;
        $type = $req->otype;
        $key_money = $req->keymoney;
        $rented_period = $req->rent_period;
        $first_rent_date = $req->open_date;
        $remark = $req->remark;

        $main = Outlet::find($id);
        $main->name = $name;
        $main->address = $address;
        $main->manage_by = $manage_by;
        $main->owner_name = $owner_name;
        $main->owner_tp = $owner_tp;

        $main->type = $type;
        $main->key_money = $key_money;
        $main->rented_period = $rented_period;
        $main->first_rent_date = $first_rent_date;
        $main->remark = $remark;
        $main->save();

        $datas = Outlet::all();
        $agents = SalesRep::all();

        return view('outlet', compact('agents', 'datas'));
    }

}