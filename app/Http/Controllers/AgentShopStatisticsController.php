<?php

namespace App\Http\Controllers;

use App\Services\APIService;
use Carbon\Carbon;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;

class AgentShopStatisticsController extends Controller
{
    protected $client;

    public function __construct()
    {
        $this->client = new APIService();
    }

    public function index(Request $request)
    {
        $yesterday = new Carbon('first day of this month');
        $today = Carbon::today();
        $dateRange = !empty($request->date_range) ? $request->date_range : "$yesterday - $today";


        if (Sentinel::check()->roles[0]->slug == 'sales-ref') {
            $agentIds = Sentinel::getUser()->id;
        } else {
            $agentIds = !empty($request->agent_id) ? $request->agent_id : null;
        }

        $data = $this->client->getAgentSalesStat($dateRange, $agentIds);
        $summery = $data->data;

        $shops = collect($this->client->getAllShops())->pluck('name', 'id');
        $agents = collect($this->client->getAllAgents())->pluck('name_with_initials', 'id');
        return view('api.agent-stats.list', compact('shops', 'agents', 'dateRange', 'agentIds', 'summery'));
    }

    public function getAgentVisitedShopsByCount(Request $request)
    {
        $agentId = $request->agent_id;
        $number = $request->number;
        $symbol = $number >= 9 ? ">" : "=";
        $dateRange = $request->date_range;
        $data = $this->client->agentVisitedShopsByCount($agentId, $number, $symbol, $dateRange);
        return !empty($data) ? $data : [];
    }

    public function getAgentVisitedShopsByCountView(Request $request)
    {
        $agentId = $request->agent_id;
        $number = $request->number;
        $symbol = $number >= 9 ? ">" : "=";
        $dateRange = $request->date_range;
        $shops = $this->client->agentVisitedShopsByCount($agentId, $number, $symbol, $dateRange);
        return view('api.agent-visit.counted-shops', compact('shops'));
    }

    public function agentVisitedShops(Request $request)
    {
        $dateRange = $request->date_range;
        $agentId = $request->agent_id;
        $data = $this->client->getVisitedData($dateRange, null, $agentId);
        $shops =$data->data->data;
        return view('api.agent-visit.visited-shops', compact('shops'));
    }

    public function agentNotVisitedShops(Request $request)
    {
        $dateRange = $request->date_range;
        $agentId = $request->agent_id;
        $data = $this->client->getNotVisitedData($dateRange, null, $agentId);
        $shops =$data->data->data;
        return view('api.agent-visit.not-visited-shops', compact('shops'));
    }
}
