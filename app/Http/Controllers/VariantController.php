<?php

namespace App\Http\Controllers;

use App\Variant;
use EndProductManager\Models\Category;
use EndProductManager\Models\EndProduct;
use Illuminate\Http\Request;

class VariantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, EndProduct $endProduct)
    {
        $endProduct->variants()->save(new Variant(['variant' => $request->variant, 'size' => $request->size]));
        return redirect()->back()->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => 'End Product Variant Created Successfully'
        ]);;
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Variant $variant
     * @return \Illuminate\Http\Response
     */
    public function show(Variant $variant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Variant $variant
     * @return \Illuminate\Http\Response
     */
    public function edit(Variant $variant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Variant $variant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Variant $variant)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Variant $variant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Variant $variant)
    {
        if ($variant->status == 1) {
            $variant->status = 0;
            $variant->save();
        } elseif ($variant->status == 0) {
            $variant->status = 1;
            $variant->save();
        }
        return redirect()->back();
    }


    public function changeReturn(Request $request,$id){
        $variant = Variant::find($id);
        $siblings= Variant::whereEndProductId($variant->end_product_id)->get();

        foreach ($siblings as $sibling){
            $sibling->return_accepted=0;
            $sibling->save();
        }

        $variant->return_accepted =1;
        $variant->save();
        return 'true';
    }
}
