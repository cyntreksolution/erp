<?php

namespace App\Http\Controllers;

use App\AgentBuffer;
use App\AgentBufferData;
use App\AgentBufferUpdateRequest;
use App\SalesReturnHeader;
use BurdenManager\Models\Burden;
use EndProductManager\Models\Category;
use EndProductManager\Models\EndProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use SalesRepManager\Models\SalesRep;

class AgentsBufferStockController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:agents_buffer_stock-index', ['only' => ['index','tableData']]);
        $this->middleware('permission:agents_buffer_stock-create', ['only' => ['create']]);
        $this->middleware('permission:UpdateOrderTemplates-list', ['only' => ['ulist']]);
        $this->middleware('permission:CreateOrderTemplates-list', ['only' => ['clist']]);
        $this->middleware('permission:PendingTemplateUpdateRequest-list', ['only' => ['plist']]);
        $this->middleware('permission:agents_buffer_stock-pendingData', ['only' => ['tableDataView','pendingTableData']]);
    }
    public function index()
    {
        $agents = SalesRep::all()->pluck('email', 'id');
        $categories = Category::all()->pluck('name', 'id');
        return view('agents.list', compact('agents', 'categories'));
    }

    public function create()
    {
        $agents = SalesRep::all()->pluck('email', 'id');
        $categories = Category::all()->pluck('name', 'id');
        $products = EndProduct::all();
        return view('agents.buffer', compact('products', 'agents', 'categories'));
    }

    public function store(Request $request)
    {
        $agent = $request->agent;
        $category = $request->category;
        $day = $request->day;
        $time = $request->time;


        $endProduct = $request->end_product;
        $qty = $request->qty;


        $variants = $request->variants;
        $variants_qty = $request->variants_qty;

        $serial = 'AB-' . date('ymdHms');

        $data = [];
        if (!empty($endProduct)){
            foreach ($endProduct as $key => $item) {
                $product = ['end_product_id' => $item, 'qty' => $qty[$key], 'isVariant' => false];
                array_push($data, $product);
            }
        }


        $variants_data = [];

        if (!empty($variants)){
            foreach ($variants as $key => $item) {
                $product = ['variant_id' => $item, 'qty' => $variants_qty[$key], 'isVariant' => true];
                array_push($data, $product);
            }
        }


        $merged = array_merge($data, $variants_data);

//        DB::transaction(function () use ($agent, $category, $data, $time, $serial, $day, $request) {
        $agentBuffer = AgentBuffer::firstOrCreate(
            ['agent_id' => $agent, 'category_id' => $category, 'day' => $day, 'time' => $time],
            ['status' => 1, 'serial' => $serial, 'dataset' => json_encode($merged), 'created_by' => Auth::user()->id]);

        if ($agentBuffer->wasRecentlyCreated) {
            return redirect('/buffer-stock/agents')->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'New Agent Buffer Order Created Successfully'
            ]);
        } else {
            return redirect('/buffer-stock/agents')->with([
                'error' => true,
                'error.title' => 'Sorry !',
                'error.message' => 'Duplicated Template'
            ]);
        }

//            $agentBuffer = new AgentBuffer();
//            $agentBuffer->agent_id = $agent;
//            $agentBuffer->category_id = $category;
//            $agentBuffer->day = $day;
//            $agentBuffer->time = $time;
//            $agentBuffer->status = 1;
//            $agentBuffer->serial = $serial;
//            $agentBuffer->dataset = json_encode($data);
//            $agentBuffer->created_by = Sentinel::getUser()->id;
//            $agentBuffer->save();
//        });


        return redirect('/buffer-stock/agents')->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => 'New Agent Buffer Order Created Successfully'
        ]);
    }

    public function tableData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'agent_id', 'category_id', 'day', 'time'];
        $order_column = $columns[$order_by[0]['column']];


        $records = AgentBuffer::tableData($order_column, $order_by_str, $start, $length)->whereStatus(1);

        if (Auth::user()->hasRole(['Sales Agent'])) {
            $records = $records->whereAgentId(Auth::user()->id);
        }

        if ($request->filter == true) {
            $agent_id = Auth::user()->hasRole(['Sales Agent']) ? Auth::user()->id : $request->agent;
            $category_id = $request->category;
            $day = $request->day;
            $time = $request->time;
            $records = $records->filterData($agent_id, $category_id, $day, $time)->get();
            $recordCount = $records->count();
        } elseif (is_null($search) || empty($search)) {
            $records = $records->get();
            $recordCount = (Auth::user()->hasRole(['Sales Agent'])) ? AgentBuffer::whereAgentId(Auth::user()->id)->count() : AgentBuffer::all()->count();
        } else {
            $records = $records->searchData($search)->get();
            $recordCount = $records->count();
        }

        $data[][] = array();
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;


        foreach ($records as $key => $record) {
            $viewBtn = '<a href="' . route('agents.show', $record->id) . '" class="btn btn-outline-dark btn-sm mr-1"><i class="fa fa-eye"></i> </a>';
            $receiveBtn = '<a href="' . route('agents.show', $record->id) . '" class="btn btn-outline-dark btn-sm mr-1"><i class="fa fa-pencil"></i> </a>';
            $deleteBtn = '<a href="' . route('agents.destroy', $record->id) . '" class="btn btn-outline-danger btn-sm mr-1"><i class="fa fa-trash"></i> </a>';
            if (Auth::user()->hasRole(['Sales Agent','Owner','Super Admin'])) {
                $viewBtn = '<a href="' . route('buffer-stock.agent-request', $record->id) . '" class="btn btn-outline-primary btn-sm mr-1"><i class="fa fa-eye"></i> </a>';
                $receiveBtn = '<a href="' . route('buffer-stock.agent-request', $record->id) . '" class="btn btn-outline-danger btn-sm mr-1"><i class="fa fa-pencil"></i> </a>';
            }


            $data[$i] = array(
                $record->serial,
                $record->salesRep->type . ' ' . $record->salesRep->name_with_initials . ' [' . $record->salesRep->territory . ']',
                $record->category->name,
                $record->day,
                $record->time,
                $viewBtn . $receiveBtn
            );
            $i++;
        }

        if ($recordCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($recordCount),
            "recordsFiltered" => intval($recordCount),
            "data" => $data
        ];

        return json_encode($json_data);
    }

    public function exportFilter(Request $request)
    {

        $campaign = $request->campaign;
        $records = Record::CampaignUtilizationReportsData($campaign);
        return Excel::download(new CampaignUtilizationReportExport($records), $this->reportFileName('campaign_utilization', '.xlsx', '', 'report'));
    }

    public function show(Request $request, $id)
    {
        $ab = AgentBuffer::find($id);

        $current_end_products = collect(json_decode($ab->dataset))->pluck('end_product_id')->filter()->all();
        $current_variants = collect(json_decode($ab->dataset))->pluck('variant_id')->filter()->all();
        $endProducts = EndProduct::whereCategoryId($ab->category_id)->pluck('id');

        $variant_list = EndProduct::whereCategoryId($ab->category_id)->with('variants')->get();
        $variant_list = $variant_list->pluck('variants');
        $all_variants = collect();

        foreach ($variant_list as $value) {
            if (!empty($value->id) || sizeof($value) > 0) {
                $all_variants = $all_variants->push($value->pluck('id'));
            }
        }
//        $all_variants = $all_variants->collapse();
//        $all_variants = $all_variants->all();
        $newItems = $endProducts->diff($current_end_products);
        $newVariants = $all_variants->collapse()->diff($current_variants);

        return view('agents.edit', compact('ab', 'newItems', 'newVariants'));
    }

    public function agentRequest(Request $request, $id)
    {
        $exist = AgentBufferUpdateRequest::where('agent_buffer_header_id', '=', $id)->whereStatus(1)->first();
        if (!empty($exist)) {
            return redirect('/buffer-stock/agents')->with([
                'error' => true,
                'error.title' => 'Sorry !',
                'error.message' => 'You have pending un approved request. Please contact system administrator'
            ]);
        }

        $ab = AgentBuffer::find($id);


        $current_end_products = collect(json_decode($ab->dataset))->pluck('end_product_id');
        $endProducts = EndProduct::whereCategoryId($ab->category_id)->pluck('id');

        $newItems = $endProducts->diff($current_end_products);
        return view('agents.edit-request', compact('ab', 'newItems'));
    }

    public function update(Request $request, $id)
    {

        $agentBuffer = AgentBuffer::find($id);


        $endProduct = $request->end_product;
        $qty = $request->qty;

        $variants = $request->variants;
        $variants_qty = $request->variants_qty;


        $data = [];
        foreach ($endProduct as $key => $item) {
            $product = ['end_product_id' => $item, 'qty' => $qty[$key]];
            array_push($data, $product);
        }

        $variants_data = [];
        if (!empty($variants)){
            foreach ($variants as $key => $item) {
                $product = ['variant_id' => $item, 'qty' => $variants_qty[$key], 'isVariant' => true];
                array_push($data, $product);
            }

        }

        $merged = array_merge($data, $variants_data);

        $agentBuffer->dataset = json_encode($merged);
        $agentBuffer->created_by = Auth::user()->id;
        $agentBuffer->save();


        return redirect('/buffer-stock/agents')->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => ' Agent Buffer Order Updated Successfully'
        ]);
    }

    public function updateRequest(Request $request, $id)
    {
        $exist = AgentBufferUpdateRequest::where('agent_buffer_header_id', '=', $id)->whereStatus(1)->first();
        if (!empty($exist)) {
            return redirect('/buffer-stock/agents')->with([
                'error' => true,
                'error.title' => 'Sorry !',
                'error.message' => 'You have pending un approved request. Please contact system administrator'
            ]);
        }

        $agentBufferUpadte = new AgentBufferUpdateRequest();
        $endProduct = $request->end_product;
        $qty = $request->qty;
        $current_end_qty = $request->current_end_qty;

        $variants = $request->variants;
        $variants_qty = $request->variants_qty;
        $current_variant_qty = $request->current_variant_qty;
        $data = [];

        foreach ($endProduct as $key => $item) {
            $product = ['end_product_id' => $item, 'qty' => $qty[$key], 'current_qty' => $current_end_qty[$key]];
            array_push($data, $product);
        }

        $variants_data = [];
        if (!empty($variants)) {
            foreach ($variants as $key => $item) {
                $product = ['variant_id' => $item, 'qty' => $variants_qty[$key], 'current_qty' => $current_variant_qty[$key], 'isVariant' => true];
                array_push($data, $product);
            }
        }


        $merged = array_merge($data, $variants_data);

        $agentBufferUpadte->agent_buffer_header_id = $id;
        $agentBufferUpadte->dataset = json_encode($merged);
        $agentBufferUpadte->created_by = Auth::user()->id;
        $agentBufferUpadte->status = 1;
        $agentBufferUpadte->save();


        return redirect('/buffer-stock/agents')->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => ' Agent Buffer Order Update Requested Successfully'
        ]);
    }

    public function tableDataView()
    {
        $agents = SalesRep::all();
        $categories = Category::all()->pluck('name', 'id');
        return view('agents.pending-request-list', compact('agents', 'categories'));
    }

    public function pendingTableData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'agent_id', 'category_id', 'day', 'time'];
        $order_column = $columns[$order_by[0]['column']];


        $records = AgentBufferUpdateRequest::tableData($order_column, $order_by_str, $start, $length)->whereStatus(1);

        if (Auth::user()->hasRole(['Sales Agent'])) {
            $records = $records->whereCreatedBy(Auth::user()->id);
        }

        if ($request->filter == true) {
            $agent_id = Auth::user()->hasRole(['Sales Agent']) ? Auth::user()->id : $request->agent;
            $category_id = $request->category;
            $day = $request->day;
            $time = $request->time;
            $records = $records->filterData($agent_id, $category_id, $day, $time)->get();
            $recordCount = $records->count();
        } elseif (is_null($search) || empty($search)) {
            $records = $records->get();
            $recordCount = (Auth::user()->hasRole(['Sales Agent'])) ? AgentBufferUpdateRequest::whereCreatedBy(Auth::user()->id)->count() : AgentBufferUpdateRequest::all()->count();
        } else {
            $records = $records->searchData($search)->get();
            $recordCount = $records->count();
        }

        $data = array();
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;


        foreach ($records as $key => $record) {
            $viewBtn = '<a href="' . route('buffer-stock.agent-request-update', $record->id) . '" class="btn btn-outline-dark btn-sm mr-1"><i class="fa fa-eye"></i> </a>';
            $receiveBtn = '<a href="' . route('buffer-stock.agent-request-update', $record->id) . '" class="btn btn-outline-dark btn-sm mr-1"><i class="fa fa-pencil"></i> </a>';
            $deleteBtn = '<a href="' . route('buffer-stock.agent-request-update', $record->id) . '" class="btn btn-outline-danger btn-sm mr-1"><i class="fa fa-trash"></i> </a>';
            if (Auth::user()->hasRole(['Sales Agent'])) {
                $viewBtn = '<a href="' . route('buffer-stock.agent-request-update', $record->id) . '" class="btn btn-outline-primary btn-sm mr-1"><i class="fa fa-eye"></i> </a>';
                $receiveBtn = '<a href="' . route('buffer-stock.agent-request-update', $record->id) . '" class="btn btn-outline-warning btn-sm mr-1"><i class="fa fa-pencil"></i> </a>';
            }


            $data[$i] = array(
                $record->template->serial,
                $record->salesRep->type . ' ' . $record->salesRep->name_with_initials . ' [' . $record->salesRep->territory . ']',
                $record->template->category->name,
                $record->template->day,
                $record->template->time,
                $viewBtn . $receiveBtn . $deleteBtn
            );
            $i++;
        }

        if ($recordCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($recordCount),
            "recordsFiltered" => intval($recordCount),
            "data" => $data
        ];

        return json_encode($json_data);
    }

    public function showPending(Request $request, $id)
    {
        $ab = AgentBufferUpdateRequest::find($id);

        $current_end_products = collect(json_decode($ab->dataset))->pluck('end_product_id');
        $endProducts = EndProduct::whereCategoryId($ab->category_id)->pluck('id');
        $newItems = $endProducts->diff($current_end_products);

        return view('agents.edit-pending', compact('ab', 'newItems'));
    }

    public function updatePending(Request $request, $id)
    {
        $agentBuffer = AgentBufferUpdateRequest::find($id);


        $endProduct = $request->end_product;
        $qty = $request->qty;
        $current_end_qty = $request->current_end_qty;

        $variants = $request->variants;
        $variants_qty = $request->variants_qty;
        $current_variant_qty = $request->current_variant_qty;

        $data = [];
        foreach ($endProduct as $key => $item) {
            $product = ['end_product_id' => $item, 'qty' => $qty[$key], 'current_qty' => $current_end_qty[$key]];
            array_push($data, $product);
        }

        $variants_data = [];
        if (!empty($variants)){
            foreach ($variants as $key => $item) {
                $product = ['variant_id' => $item, 'qty' => $variants_qty[$key], 'current_qty' => $current_variant_qty[$key], 'isVariant' => true];
                array_push($data, $product);
            }
        }

        $merged = array_merge($data, $variants_data);


        $agentBuffer->dataset = json_encode($merged);
        $agentBuffer->created_by = Auth::user()->id;
        $agentBuffer->save();


        return redirect(route('buffer-stock.list-view'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => ' Agent Buffer Order Request Updated Successfully'
        ]);
    }

    public function approvePending(Request $request, $id)
    {
        $agentBufferRequest = AgentBufferUpdateRequest::find($id);
        $agentBufferRequest->status = 0;
        $agentBufferRequest->save();


        $agentBuffer = AgentBuffer::find($agentBufferRequest->agent_buffer_header_id);
        $agentBuffer->dataset = json_encode($agentBufferRequest->dataset);
        $agentBuffer->status = 0;
        $agentBuffer->save();

        $serial = 'AB-' . date('ymdHms');

        $newAgentBuffer = new AgentBuffer();
        $newAgentBuffer->agent_id = $agentBuffer->agent_id;
        $newAgentBuffer->category_id = $agentBuffer->category_id;
        $newAgentBuffer->day = $agentBuffer->day;
        $newAgentBuffer->time = $agentBuffer->time;
        $newAgentBuffer->status = 1;
        $newAgentBuffer->serial = $serial;
        $newAgentBuffer->dataset = $agentBufferRequest->dataset;
        $newAgentBuffer->created_by = Auth::user()->id;
        $newAgentBuffer->save();


        return redirect(route('buffer-stock.list-view'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => 'New Agent Buffer Order Created Successfully'
        ]);
    }

}
