<?php

namespace App\Http\Controllers;

use App\PermissionGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class PermissionGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:permission_group-index', ['only' => ['index','tableData']]);
        $this->middleware('permission:permission_group-list', ['only' => ['list']]);
        $this->middleware('permission:permission_group-create', ['only' => ['create','store']]);
        $this->middleware('permission:permission_group-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:permission_group-delete', ['only' => ['destroy']]);
    }
    public function index()
    {
        $permissionGroup = PermissionGroup::all()->pluck('name', 'id');
        return view('permission-group.index', compact('permissionGroup'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);
        $input = $request->all();

        $group = PermissionGroup::create($input);

        $msg = 'Permission Group Created Successfully';
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $input = $request->all();

        $user = PermissionGroup::find($id);
        $user->update($input);

        $msg = 'Permission Group Updated Successfully';
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->id;
        $perGroup = PermissionGroup::find($id);
        $perGroup->delete();
        return 'true';
    }

    public function tableData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $recordsCount = 0;

        $date = $request->date_range;
        $columns = ['id','name'];
        $order_column = $columns[$order_by[0]['column']];

        // $records = EndProductBurden::tableData($order_column, $order_by_str, $start, $length);
        // $records = $records->where('status', '!=', '5')->orderBy('status');


        $records = PermissionGroup::query();


        if (is_null($search) || empty($search)) {
            $salesOrdersCount = $records->count();
            $records = $records->tableData($order_column, $order_by_str, $start, $length)->get();
        } else {
            $salesOrdersCount = $records->count();
            $records = $records->searchData($search)->tableData($order_column, $order_by_str, $start, $length)->get();
        }

        $recordsCount = $salesOrdersCount;

        $data[][] = array();
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;

        $btn_status = null;

        $condition = (!empty($request->category)) ? 1 : 0;

        $btn_approve = null;

        $user = Auth::user();
        $can_edit = ($user->can('permission_group-edit')) ? 1 : 0;
        $can_delete = ($user->can('permission_group-delete')) ? 1 : 0;
        foreach ($records as $key => $record) {
            if ($can_edit) {
                $edit_btn = "<button onclick=\"edit(this)\" data-id='{$record->id}' data-name='{$record->name}' class='addItem btn btn-sm btn-warning ml-1' style='margin-right: 5px;'><i class='fa fa-edit'></i></button>";

            }else{
                $edit_btn=[];
            }
            if ($can_delete) {
                $delete_btn = "<button class='btn btn-xs btn-icon btn-light-danger mr-2' onclick=\"deletepermissionGroup('$record->id')\"> <i class='fa fa-trash'></i></button>";

            }else{
                $delete_btn=[];
            }


            $data[$i] = array(
                $record->id,
                $record->name,
                $edit_btn . $delete_btn
            );
            $i++;
        }


        if ($recordsCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($recordsCount),
            "recordsFiltered" => intval($recordsCount),
            "data" => $data
        ];

        return json_encode($json_data);
    }
}
