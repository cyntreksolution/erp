<?php

namespace App\Http\Controllers;

use EndProductManager\Models\EndProduct;
use Illuminate\Http\Request;
use SemiFinishProductManager\Models\SemiFinishProduct;

class CommonMethodsController extends Controller
{
    //

    public static function get_end_products($id)
    {
        $end_products = EndProduct::select("end_product.name","end_product.id","po_end_product.total","po_end_product.semi_product_id","po_end_product.semi_qty")
            ->join("po_end_product","po_end_product.end_product_id","=","end_product.id")
            ->where("po_end_product.production_order_id","=",$id)
            ->get();
        return $end_products;
    }

    public static function get_semi_products($po_id,$id)
    {
        $semi_products = SemiFinishProduct::select("semi_finish_product.name","semi_finish_product.semi_finish_product_id","po_semi.semi_qty","po_semi.semi_total")
            ->join("po_semi","po_semi.semi_product_id","=","semi_finish_product.semi_finish_product_id")
            ->where("po_semi.production_order_id","=",$po_id)
            ->where("po_semi.semi_product_id","=",$id)
            ->get();
        return $semi_products;
    }
}
