<?php

namespace App\Http\Controllers;


use App\PermissionGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;
use DB;


class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:role-list', ['only' => ['index','tableData']]);
        $this->middleware('permission:role-create', ['only' => ['create','store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $roles = Role::orderBy('id','DESC')->paginate(5);
        $permissionGroups = PermissionGroup::with('permissions')->get();
        $rolePermissions = [ ];
//        dd($roles);
        return view('roles.index',compact('roles','permissionGroups','rolePermissions'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissionGroups = PermissionGroup::with('permissions')->get();
        $permission = Permission::get();
        return view('roles.create',compact('permission','permissionGroups'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|unique:roles,name',
            'permission' => 'required',
        ]);


        $role = Role::create(['name' => $request->input('name')]);
        $role->givePermissionTo($request->input('permission'));
//        $role->syncPermissions($request->input('permission'));


        return redirect()->route('roles.index')
            ->with('success','Role created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $role = Role::find($id);
        $rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
            ->where("role_has_permissions.role_id",$id)
            ->get();


        return view('roles.show',compact('role','rolePermissions'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function details(Request $request){
        $permissionGroups = PermissionGroup::with('permissions')->get();
        $role = Role::find($request->id);
        $permission = Permission::get();
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$request->id)
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all();


        return view('roles.edit',compact('permission','rolePermissions','permissionGroups','role'));
    }


    public function edit($id)
    {
       //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'permission' => 'required',
        ]);


        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->save();


//        $role->givePermissionTo($request->input('permission'));
        $role->syncPermissions($request->input('permission'));

        return redirect()->route('roles.index')
            ->with('success','Role updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        DB::table("roles")->where('id',$request->id)->delete();
        return 'true';
    }

    public function tableData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $recordsCount = 0;

        $date = $request->date_range;
        $columns = ['id','name'];
        $order_column = $columns[$order_by[0]['column']];

        // $records = EndProductBurden::tableData($order_column, $order_by_str, $start, $length);
        // $records = $records->where('status', '!=', '5')->orderBy('status');


        $records = Role::query();


        if (is_null($search) || empty($search)) {
            $recordsCount = $records->count();
            $records = $records->tableData($order_column, $order_by_str, $start, $length)->get();
        } else {
            $recordsCount = $records->count();
            $records = $records->searchData($search)->tableData($order_column, $order_by_str, $start, $length)->get();
        }

//        $recordsCount = $salesOrdersCount;

        $data[][] = array();
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;

        $btn_status = null;

        $condition = (!empty($request->category)) ? 1 : 0;

        $btn_approve = null;

        $user = Auth::user();
        $can_edit = ($user->can('role-edit')) ? 1 : 0;
        $can_delete = ($user->can('role-delete')) ? 1 : 0;
        foreach ($records as $key => $record) {
            if ($can_edit) {
                $edit_btn = "<button onclick=\"edit(this)\" data-id='{$record->id}' data-name='{$record->name}' data-permission='{$record->permission}' class='addItem btn btn-sm btn-warning ml-1' style='margin-right: 5px;'><i class='fa fa-edit'></i></button>";

            }else{
                $edit_btn=[];
            }
            if ($can_delete) {
                $delete_btn = "<button class='btn btn-xs btn-icon btn-light-danger mr-2' onclick=\"deleterole('$record->id')\"> <i class='fa fa-trash'></i></button>";
            }else{
                $delete_btn=[];
            }


            $data[$i] = array(
                $record->id,
                $record->name,
                $edit_btn . $delete_btn
            );
            $i++;
        }


        if ($recordsCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($recordsCount),
            "recordsFiltered" => intval($recordsCount),
            "data" => $data
        ];

        return json_encode($json_data);
    }

}