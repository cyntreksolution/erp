<?php

namespace App\Http\Controllers;

use App\Services\APIService;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    protected $client;

    public function __construct()
    {
        $this->client = new APIService();
    }


    public function list(Request $request)
    {
        $routes = collect($this->client->getAllRoutes())->pluck('name', 'id');
        $shopRouteId = $request->id;
        $param = !empty($shopRouteId) ? "?shop_route_id=$shopRouteId" : null;
        $shops = $this->client->getAllShops($param);
        return view('api.shops.list', compact('shops', 'routes'));
    }

    public function create(Request $request)
    {
        $dataset = $request->except('_token');
        $route = $this->client->createShop($dataset);
        return $route->status == 'success' ? 'true' : 'false';
    }

    public function update(Request $request)
    {
        $dataset = $request->except('_token', 'id');
        $route = $this->client->updateShop($request->id, $dataset);
        return $route->status == 'success' ? 'true' : 'false';
    }

    public function active(Request $request)
    {

        $route = $this->client->activeShop($request->id);
        return $route->status == 'success' ? 'true' : 'false';
    }

    public function delete(Request $request)
    {
        $this->client->deleteShop($request->id);
        return 'true';
    }

    public function bulkUpdate(Request $request)
    {
        $dataset = $request->except('_token');
        $x = $this->client->bulkShopInsert($request);
        return "true";
    }

    public function bulkQRDownload(Request $request)
    {
        $dataset = $request->list;
        $x = $this->client->bulkQRDownload($dataset);
        return $x->data->file_name;
    }

    public function bulkQRDownloadPDF(Request $request)
    {
        $dataset = $request->list;
        $x = $this->client->bulkQRDownloadPDF($dataset);
        return $x->data->file_name;
    }

    public function visitedShopReport(Request $request)
    {
        $dateRange = !empty($request->date_range) ? $request->date_range : Carbon::now()->subMonth()->format('Y-m-d') . " - " . Carbon::now()->format('Y-m-d');
        $routeId = $request->route_id;
        $agentId = $request->agent_id;

        $routes = collect($this->client->getAllRoutes())->pluck('name', 'id');
        $agents = collect($this->client->getAllAgents())->pluck('name_with_initials', 'id');

        $visited_data = [];
        $analytics = [];
        if (!empty($routeId) || !empty($agentId)) {
            $response = $this->client->getVisitedData($dateRange, $routeId, $agentId);
            $visited_data = $response->data;
            $analytics = $response->data->analytics;

        }
        return view('api.shops.visited-list', compact('visited_data', 'agents', 'routes', 'dateRange', 'agentId', 'routeId', 'analytics'));
    }

    public function visitedShopFilterReport(Request $request)
    {
        $dateRange = !empty($request->date_range) ? $request->date_range : Carbon::now()->subMonth()->format('Y-m-d') . " - " . Carbon::now()->format('Y-m-d');
        $routeId = $request->route_id;
        $agentId = $request->agent_id;
        $shopId = $request->shop_id;


        $routes = collect($this->client->getAllRoutes('?order_by=name'))->pluck('name', 'id');
        $agents = collect($this->client->getAllAgents('?order_by=name_with_initials'))->pluck('name_with_initials', 'id');
        $shops = collect($this->client->getAllShops('?order_by=name'))->pluck('name', 'id');

        $visited_data = [];
        $analytics = [];

        $response = $this->client->getVisitedDataFilter($dateRange, $routeId, $agentId, $shopId);
        $visited_data = $response->data;
        $analytics = $response->data->analytics;


        return view('api.shops.visited-filter', compact('visited_data', 'shops', 'agents', 'routes', 'dateRange', 'agentId', 'routeId', 'analytics', 'shopId'));
    }


}
