<?php

namespace App\Http\Controllers;

use App\EndProductBurdenGrade;
use App\EndProductInquiry;
use App\LoadingData;
use App\LoadingDataVariant;
use App\SalesReturnData;
use App\User;
use BurdenManager\Models\ReusableProducts;
use Carbon\Carbon;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use EndProductBurdenManager\Models\EndProductBurden;
use EndProductManager\Models\Category;
use EndProductManager\Models\EndProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SalesOrderManager\Models\SalesOrder;
use SalesRepManager\Models\AgentCategory;
use SalesRepManager\Models\SalesRep;

class EndProductMovementReport extends Controller
{
    function __construct()
    {
        $this->middleware('permission:end_product_movement-index', ['only' => ['index','tableData']]);
        $this->middleware('permission:EndProductMovement-list', ['only' => ['list']]);
        $this->middleware('permission:EndProductMovementReport-list', ['only' => ['rlist']]);
//        $this->middleware('permission:agent_outstanding-view', ['only' => ['forcePay']]);

    }
    public function index(Request $request)
    {
        $end_products = EndProduct::all()->pluck('name', 'id');
        $categories = Category::pluck('name', 'id');
        $agents = SalesRep::get();
        return view('reports.end-move', compact('end_products', 'categories', 'agents'));
    }

    public function tableData(Request $request)
    {

        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $date = $request->date_range;
        $columns = ['order_date', 'template_id', 'status', 'created_by'];
        $order_column = $columns[$order_by[0]['column']];


        $end = null;
        $filter_count = 0;
        $recordsCount = 0;
        $data = [];
        $i = 0;
        $stst = null;

        $ts = 0;
        $tsa = 0;

        if ($request->filter == true) {
            $burdens = EndProductBurdenGrade::query();
            $stst = EndProductBurdenGrade::query();
            $end_product = $request->end_product;
            $end = EndProduct::find($end_product);
            $date_range = $request->date_range;
            $grade = $request->grade;
            $category = $request->category;

            if (!empty($end_product)) {
                $burdens = $burdens->whereHas('burden', function ($q) use ($end_product, $grade) {
                    return $q->where('end_product_id', '=', $end_product);
                });


            }


            if (!empty($grade)) {
                switch ($grade) {
                    case 9:
                        $burdens = $burdens->whereHas('burden', function ($q) {
                            return $q->where('status', '=', 6);
                        })->where('grade_id', '<', 3);
                        break;


                    default:
                        $burdens = $burdens->whereGradeId($grade);
                }
            }


            if (!empty($category)) {
                $burdens = $burdens->whereHas('burden.endProduct', function ($q) use ($category) {
                    return $q->where('category_id', '=', $category);
                });
            }

            if (!empty($date_range)) {
                $dates = explode(' - ', $date_range);
                $start_date = $dates[0];
                $end_date = $dates[1];
                $burdens = $burdens->where('created_at', '>=', Carbon::parse($start_date)->format('Y-m-d'))
                    ->where('created_at', '<=', Carbon::parse($end_date)->addDay()->format('Y-m-d'));
            }

            $recordsCount = EndProductBurdenGrade::query()->count();
            $filter_count = $burdens->count();

            $bdns = $burdens->orderBy('id', $order_by_str);
            $rcds = $bdns->get();


            $burdens = $burdens->orderBy('id', $order_by_str)
                ->offset($start)
                ->limit($length);



           // $stst = $burdens;



            $records = $burdens->get();

        //    $stst = $stst->select(DB::raw('SUM(burden_qty) as total'), DB::raw('SUM(total_value) as total_value_sum'));

            foreach ($rcds as $key => $rcd) {
                $ts = $ts + $rcd->burden_qty;
                $exqty = EndProductBurden::where('id','=',$rcd->burden_id)->first();
                $unit = 0;
                if(empty($exqty->expected_end_product_qty)){
                    $unit = 1;
                }else{
                    $unit = $exqty->expected_end_product_qty;
                }


                $tsa = $tsa + ((($rcd->total_value)/($unit)) * $rcd->burden_qty);
            }


            foreach ($records as $key => $record) {
               // $ts = $ts + $rcd->burden_qty;
               // $tsa = $tsa + $rcd->total_value;
                $exqty1  = EndProductBurden::where('id','=',$record->burden_id)->first();
                $unit1 =0;
                if(empty($exqty1->expected_end_product_qty)){
                    $unit1 = 1;
                }else{
                    $unit1 = $exqty1->expected_end_product_qty;
                }
                $grade_text = "Grade  " . optional($record->grade)->name;
                $data[$i] = array(
                    Carbon::parse($record->created_at)->format('Y-m-d H:i:s'),
                    optional(optional($record->burden)->endProduct)->name,
                    optional($record->burden)->serial,
                    $record->burden_qty,
                    null,
                    number_format(((($record->total_value)/($unit1))*($record->burden_qty)), 2),
                    $grade_text,
                    null
                );
                $i++;
            }
        }

        $data[$i] = array(
            '',
            '',
            'Total',
            $ts,
            null,
            $tsa,
            '',
            null
        );

      //  $sum_data = $stst->first();

      //  $ts = !empty($sum_data->total) ? $sum_data->total : 0;
      //  $tsa = !empty($sum_data->total_value_sum) ? $sum_data->total_value_sum : 0;


        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($recordsCount),
            "recordsFiltered" => intval($filter_count),
            "data" => $data,
            "total_qty" => number_format($ts, 3),
            "total_amount" => number_format($tsa, 2),
        ];


        return json_encode($json_data);
    }


    /*public function tableDataSalesX(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $date = $request->date_range;
        $columns = ['order_date', 'template_id', 'status', 'created_by'];
        $order_column = $columns[$order_by[0]['column']];


        $end = null;
        $filter_count = 0;
        $recordsCount = 0;
        $data = [];
        $stst = LoadingData::query();
        if ($request->filter == true) {
            $end_product = $request->end_product;
            $end = EndProduct::find($end_product);
            $date_range = $request->date_range;
            $agent = $request->agent;
            $category = $request->category;


            if ($request->filter == true) {
                if (!empty($end)) {
                    if ($end->variants->count() == 0) {
                        $query = LoadingData::query();
                        $query = $query->where('end_product_id', '=', $end->id)->where('qty', '>', 0);


                        if (!empty($date_range)) {
                            $query->whereHas('header', function ($q) use ($date_range) {
                                $dates = explode(' - ', $date_range);
                                $start_date = $dates[0];
                                $end_date = $dates[1];
                                $q->where('loading_date', '>=', Carbon::parse($start_date)->format('Y-m-d'))
                                    ->where('loading_date', '<=', Carbon::parse($end_date)->addDay()->format('Y-m-d'));
                            });
                        }
////
//
                        if (!empty($agent) && $agent != 'null') {
                            $query->whereHas('header.salesOrder.salesRep', function ($q) use ($agent) {
                                $q->where('id', '=', $agent);
                            });
                        }

//                        if (!empty($category)) {
//
//                        }
                        $filter_count = $query->count();
                        $stst = $query;
                        $products = $query->whereHas('header', function ($q) {
                            $q->orderBy('loading_date', 'DESC');
                        })
                            ->offset($start)
                            ->limit($length);

                        $records = $products->get();
                        $i = 0;
                        $stst = $stst->select(DB::raw('SUM(agent_approved_qty) as agent_approved_total'), DB::raw('SUM(qty) as qty_total'), DB::raw('SUM(total) as row_value_sum'));


                        foreach ($records as $key => $record) {
                            $btn_preview = "<a href='" . route('order-list.preview', [$record->header->id]) . "' class='ml-2 btn btn-sm btn-primary'>Preview</a>";

                            $remark = $record->header->invoice_number;
                            $total = !empty($record->total) ? $record->total : 0;
                            $sales_header = !empty($record->header->salesOrder) ? $record->header->salesOrder : null;
                            $data[$i] = array(
                                Carbon::parse($record->header->loading_date)->format('Y-m-d H:i:s'),
                                $record->endProduct->name,
                                !empty($sales_header->salesRep) ? $sales_header->salesRep->name_with_initials : 0,
                                $record->qty,
                                $record->agent_approved_qty,
                                number_format($total, 2),
                                $remark,
                                $btn_preview
                            );
                            $i++;
                        }
                    } else {
                        $variant_id = $end->variants->pluck('id');
                        $query = LoadingDataVariant::query();
                        $query = $query->whereIn('variant_id', $variant_id)->where('weight', '>', 0);
                        $filter_count = $query->count();

                        $products = $query->whereHas('header', function ($q) {
                            $q->orderBy('loading_date', 'DESC');
                        });

                        if (!empty($date_range)) {
                            $products->whereHas('header', function ($q) use ($date_range) {
                                $dates = explode(' - ', $date_range);
                                $start_date = $dates[0];
                                $end_date = $dates[1];
                                $q->where('loading_date', '>=', Carbon::parse($start_date)->format('Y-m-d'))
                                    ->where('loading_date', '<=', Carbon::parse($end_date)->addDay()->format('Y-m-d'));
                            });
                        }
//
                        if (!empty($agent) && $agent != 'null') {
                            $products->whereHas('header.salesOrder.salesRep', function ($q) use ($agent) {
                                $q->whereId($agent);
                            });
                        }
                        $filter_count = $products->count();
                        $stst = $products;
                        $records = $products->get();
                        $i = 0;
                        $stst = $stst->select(DB::raw('SUM(agent_approved_qty) as agent_approved_total'), DB::raw('SUM(weight) as qty_total'), DB::raw('SUM(total) as row_value_sum'));

                        foreach ($records as $key => $record) {
                            $remark = $record->header->invoice_number;
                            $btn_preview = "<a href='" . route('order-list.preview', [$record->header->id]) . "' class='ml-2 btn btn-sm btn-primary'>Preview</a>";

//                            $agent_approved = "<button class='btn btn-outline-dark' onclick='showData()'><i class='fa fa-info-circle'></i> </button>"
                            $total = !empty($record->total) ? $record->total : 0;
                            $sales_header = !empty($record->header->salesOrder) ? $record->header->salesOrder : null;
                            $data[$i] = array(
                                Carbon::parse($record->header->loading_date)->format('Y-m-d H:i:s'),
                                $record->variant->product->name . ' - ' . $record->variant->variant,
                                !empty($sales_header->salesRep) ? $sales_header->salesRep->name_with_initials : 0,
                                $record->weight,
                                $record->agent_approved_qty,
                                number_format($total, 2),
                                $remark,
                                $btn_preview
                            );
                            $i++;
                        }
                    }
                }
            }


        }

        $sum_data = $stst->first();

        $ts = !empty($sum_data->qty_total) ? $sum_data->qty_total : 0;
        $tsac = !empty($sum_data->row_value_sum) ? $sum_data->row_value_sum : 0;

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($recordsCount),
            "recordsFiltered" => intval($filter_count),
            "data" => $data,
            "total_qty" => number_format($ts, 3),
            "total_amount" => number_format($tsac, 2),
        ];

        return json_encode($json_data);
    }*/

    public function tableDataSales(Request $request)
    {

        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];


        $columns = ['created_at', 'template_id', 'status', 'created_by'];
        $order_column = $columns[$order_by[0]['column']];

        $i = 0;
        $end = null;
        $filter_count = 0;
        $recordsCount = 0;
        $data = [];


        if ($request->filter == true) {

            $end_product = $request->end_product;
            $date_range = $request->date_range;
            $agent_id = $request->agent;
            $category = $request->category;
            $item_type = $request->item_type;
          //  $stst = null;
          //  $unstst = null;

            if($item_type == 1) {
             //   $stst = EndProductInquiry::where('desc_id','=',1);
              //  $unstst = EndProductInquiry::where('desc_id','=',3);
                $query = EndProductInquiry::where('qty', '<>', 0)->where('desc_id','=',1);


            }elseif ($item_type == 2){
             //   $stst = EndProductInquiry::where('desc_id','=',2);
             //   $unstst = EndProductInquiry::where('desc_id','=',4);
                $query = EndProductInquiry::where('qty', '<>', 0)->where('desc_id','=',2);

            }else{
             //   $stst = EndProductInquiry::whereIn('desc_id', [1, 2]);
             //   $unstst = EndProductInquiry::whereIn('desc_id', [3, 4]);
                $query = EndProductInquiry::where('qty', '<>', 0)->whereIn('desc_id', [1, 2]);


            }


            $recordsCount = $query->count();

            $query = $query->filterData($end_product, $date_range, $category, $agent_id,'sales');
        //    $stst = $stst->filterData($end_product, $date_range, $category, $agent_id,'sales');
        //    $unstst = $unstst->filterData($end_product, $date_range, $category, $agent_id,'sales');


            $filter_count = $query->count();
       //     dd($filter_count);
            $rcds = $query->groupby('reference')->distinct()->get();
            $records = $query->tableData($order_column, $order_by_str, $start, $length)->groupby('reference')->distinct()->get();




            $totqty = 0;
            $totvalue = 0;

            foreach ($rcds as $key => $rcd) {

                $sumitem = EndProductInquiry::where('reference','=',$rcd->reference)->orderByDesc('id')->first();

              //  dd($item->count());
                if($sumitem->desc_id == 1) {
                    $sumunitem = EndProductInquiry::where('reference','=',$rcd->reference)->whereId($sumitem->id)->where('desc_id', '=', 3)->orderByDesc('id')->first();
                    if(empty($sumunitem->id) || $sumunitem->id < $sumitem->id) {

                        $totqty = $totqty + $sumitem->qty;
                        $totvalue = $totvalue + $sumitem->amount;
                    }
                }elseif ($sumitem->desc_id == 2) {
                    $sumunveri = EndProductInquiry::where('reference','=',$rcd->reference)->whereId($sumitem->id)->where('desc_id', '=', 4)->orderByDesc('id')->first();
                    if(empty($sumunveri->id) || $sumunveri->id < $sumitem->id) {

                        $totqty = $totqty + $sumitem->qty;
                        $totvalue = $totvalue + $sumitem->amount;
                    }
                }

            }


            foreach ($records as $key => $record) {

                $btn_preview = "<a href='" . route('order-list.preview', [$record->so_id]) . "' class='ml-2 btn btn-sm btn-primary'>Preview</a>";


                $item = EndProductInquiry::where('reference','=',$record->reference)->orderByDesc('id')->first();
                if($item->desc_id == 1) {
                    $unitem = EndProductInquiry::where('reference','=',$record->reference)->whereId($item->id)->where('desc_id', '=', 3)->orderByDesc('id')->first();
                    if(empty($unitem->id) || $unitem->id < $item->id){

                     //     $totqty = $totqty + $item->qty;
                      //    $totvalue = $totvalue + $item->amount;

                        $data[$i] = array(
                            Carbon::parse($item->created_at)->format('Y-m-d H:i:s'),
                            $item->end_product->name,
                            optional($item->agent)->name_with_initials,
                            $item->qty * -1,
                            100,
                            number_format($item->amount * -1,2),
                            $item->so_id,//remark
                            $btn_preview,//btn
                            '',//btn

                        );
                        $i++;
                    }
                }elseif ($item->desc_id == 2) {
                    $unveri = EndProductInquiry::where('reference','=',$record->reference)->whereId($item->id)->where('desc_id', '=', 4)->orderByDesc('id')->first();
                    if(empty($unveri->id) || $unveri->id < $item->id){

                    //    $totqty = $totqty + $item->qty;
                    //    $totvalue = $totvalue + $item->amount;

                        $data[$i] = array(
                            Carbon::parse($item->created_at)->format('Y-m-d H:i:s'),
                            $item->end_product->name,
                            optional($item->agent)->name_with_initials,
                            $item->qty * -1,
                            200,
                            number_format($item->amount * -1,2),
                            $item->so_id,//remark
                            $btn_preview,//btn
                            '',//btn

                        );
                        $i++;
                    }
                }

            }

            $ts = $totqty * -1;
            $tsac = $totvalue * -1;

          /*  $data[$i] = array(
                '',//Carbon::parse($item->created_at)->format('Y-m-d H:i:s'),
                '',//$item->end_product->name,
                'Total',//optional($item->agent)->name_with_initials,
                number_format($ts,3),
                0,
                number_format($tsac,2),
                '',//remark
                '',//btn
                '',//btn
            );*/

        }



      /*  $stst = $stst->select( DB::raw('SUM(qty) as qty_total'), DB::raw('SUM(amount) as row_value_sum'));
        $sum_data = $stst->first();

        $ts1 = !empty($sum_data->qty_total) ? $sum_data->qty_total : 0;
        $tsac1 = !empty($sum_data->row_value_sum) ? $sum_data->row_value_sum : 0;


        $unstst = $unstst->select( DB::raw('SUM(qty) as unqty_total'), DB::raw('SUM(amount) as unrow_value_sum'));
        $unsum_data = $unstst->first();

        $ts2 = !empty($unsum_data->unqty_total) ? $unsum_data->unqty_total : 0;
        $tsac2 = !empty($unsum_data->unrow_value_sum) ? $unsum_data->unrow_value_sum : 0;

        $ts = $ts1 - $ts2;
        $tsac = $tsac1 - $tsac2;*/



        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($recordsCount),
            "recordsFiltered" => intval($filter_count),
            "data" => $data,
            "total_qty" => number_format($ts, 3),
            "total_amount" => number_format($tsac, 2)
        ];

       // array_push($json_data, [$i, '', '', number_format($ts, 2), '', number_format($tsac, 2), '', '', '']);

        // return Response::json(array('data' => $jsonList));
        return json_encode($json_data);
    }

    public function tableDataReturns(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $date = $request->date_range;
        $columns = ['order_date', 'template_id', 'status', 'created_by'];
        $order_column = $columns[$order_by[0]['column']];


        $end = null;
        $filter_count = 0;
        $recordsCount = 0;
        $data = [];
        if ($request->filter == true) {
            $query = ReusableProducts::query();
            $end_product = $request->end_product;
            $date_range = $request->date_range;
            $agent = $request->agent;
            $category = $request->category;
            $type = $request->type;
            $reuse_type = $request->reuse_type;


            if (!empty($end_product)) {
                $query = $query->where('product_id', '=', $end_product);
            }

            if (!empty($agent) && $agent != 'null') {
                $query = $query->whereHas('debitNoteHeader', function ($q) use ($agent) {
                    return $q->where('agent_id', '=', $agent);
                });
            }

            if (!empty($category)) {
                $query = $query->whereHas('endProduct', function ($q) use ($category) {
                    return $q->where('reusable_products.category_id', '=', $category);
                });
            }

            if (!empty($reuse_type)) {
                $query = $query->where('type', '=', $reuse_type);
            }

            if (!empty($date_range)) {
                if (!empty($type)) {
                    $dates = explode(' - ', $date_range);
                    $start_date = $dates[0];
                    $end_date = $dates[1];
                    switch ($type) {
                        case 4:
                        case 5:
                            $query = $query->where('reusable_products.created_at', '>=', Carbon::parse($start_date)->format('Y-m-d'))
                                ->where('reusable_products.created_at', '<=', Carbon::parse($end_date)->addDay()->format('Y-m-d'));
                            break;

                        default:
                            $query = $query->whereHas('debitNoteHeader', function ($q) use ($date_range) {
                                $dates = explode(' - ', $date_range);
                                $start_date = $dates[0];
                                $end_date = $dates[1];
                                $q->where('reusable_products.created_at', '>=', Carbon::parse($start_date)->format('Y-m-d'))
                                    ->where('reusable_products.created_at', '<=', Carbon::parse($end_date)->addDay()->format('Y-m-d'));
                            });

                    }

                } else {
                    $query = $query->whereHas('debitNoteHeader', function ($q) use ($date_range) {
                        $dates = explode(' - ', $date_range);
                        $start_date = $dates[0];
                        $end_date = $dates[1];
                        $q->where('reusable_products.created_at', '>=', Carbon::parse($start_date)->format('Y-m-d'))
                            ->where('reusable_products.created_at', '<=', Carbon::parse($end_date)->addDay()->format('Y-m-d'));
                    });

                }
            }


            if (!empty($type)) {
                switch ($type) {
                    case 2:
                        $query = $query->whereHas('debitNoteHeader', function ($q) {
                            $q->where('is_system_generated', '=', 1);
                        });
                        break;
                    case 3:
                        $query = $query->whereHas('debitNoteHeader', function ($q) {
                            $q->where('is_system_generated', '=', 0);
                        });
                        break;
                    case 4:
                        $query = $query->where('collect_type', '=', 4);
                        break;
                    case 5:
                        $query = $query->where('collect_type', '=', 5);
                        break;

                }
            }

//*****************************************************

            $recordsCount = ReusableProducts::query()->count();
            $filter_count = $query->count();

            $qry = $query->orderBy('reusable_products.id', $order_by_str);

            $query = $query->orderBy('reusable_products.id', $order_by_str)
                ->offset($start)
                ->limit($length);


//            $query = $query->where('collect_type', '=', 3);

     //       $stst = $query;

            $stst = $qry;

            $records = $query->get();
            $i = 0;

            $stst = $stst->select(DB::raw('SUM(qty * end_product.selling_price) as price_total'), DB::raw('SUM(qty) as qty_total'))
                ->join('end_product', 'end_product.id', '=', 'reusable_products.product_id')
                ->first();

            $qt_total = !empty($stst->qty_total) ? $stst->qty_total : 0;
            $am_total = !empty($stst->price_total) ? $stst->price_total : 0;


            foreach ($records as $key => $record) {
                $name = '-';
                $viewBtn = null;
                $desc = null;
                if (empty($type)) {
                    $viewBtn = '<a href="' . route('sales_return.show', $record->debitNoteHeader->id) . '" class="btn btn-outline-info btn-sm mr-2"><i class="fa fa-eye"></i> </a>';
                    $ref = $dec = null;
                } else {
                    if ($type == 2 || $type == 1 || $type == 3) {
                        $dec = optional($record->debitNoteHeader)->debit_note_number;
                        $ref = optional($record->debitNoteHeader)->agent->name_with_initials;
                        $name = optional($record->debitNoteHeader)->agent->name_with_initials;
                    } else {
                        $ref = $dec = $name = null;
                    }

                }

                $data[$i] = array(
                    Carbon::parse($record->created_at)->format('Y-m-d H:i:s'),
                    $record->endProduct->name,
                    $dec,
                    $record->qty,
                    $name,
                    number_format($record->qty * $record->endProduct->selling_price, 2),
                    $ref,
                    $viewBtn,
                );
                $i++;
            }

        }
        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($recordsCount),
            "recordsFiltered" => intval($filter_count),
            "data" => $data,
            "total_qty" => number_format($qt_total, 3),
            "total_amount" => number_format($am_total, 2),
        ];

        return json_encode($json_data);
    }
}
