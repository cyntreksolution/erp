<?php

namespace App\Http\Controllers;

use App\ReusableStock;
use App\Traits\EndProductLog;
use BurdenManager\Models\ReusableProducts;
use Carbon\Carbon;
use EmployeeManager\Models\Employee;
use EndProductBurdenManager\Models\EndProductBurdenHeader;
use EndProductManager\Models\Category;
use EndProductManager\Models\EndProduct;
use Illuminate\Http\Request;
use StockManager\Classes\StockTransaction;

class EndProductRemoveController extends Controller
{
    use EndProductLog;

    function __construct()
    {
        $this->middleware('permission:order_list-index', ['only' => ['index','tableData']]);
        $this->middleware('permission:RemoveStoresReturn-list', ['only' => ['list']]);
        $this->middleware('permission:order_list-BalanceUpdate', ['only' => ['endProductCurrantBalanceUpdate']]);


    }
    public function index(Request $request)
    {
        $flag =0;
        if (!empty($request->category)) {
            $end_products = EndProduct::whereCategoryId($request->category)->get();
            $flag=1;
        }else{
            $end_products =[];
        }

        $categories = Category::pluck('name','id');
        $employees = Employee::pluck('full_name', 'id');
        return view('end-product.end-remove-list', compact('end_products', 'employees','flag','categories'));
    }

    public function endProductCurrantBalanceUpdate(Request $request)
    {
        $end_products = $request->end_product_id;
        $available_qty = $request->available_qty;
        $destroy_qty = $request->destroy_qty;
        $re_use_qty = $request->re_use_qty;
        $lost_qty = $request->lost_qty;
        $employee = $request->employee;


        if (empty($employee)) {
            return redirect()->back()->withInput()->with([
                'error' => true,
                'error.title' => 'Sorry !',
                'error.message' => 'Employee Required'
            ]);
        }

        if (!empty($end_products) && sizeof($end_products) > 0) {
            foreach ($end_products as $key => $end_product_id) {

                $is_varients = EndProduct::find($end_product_id)->variants;


                if (!empty($is_varients) && $is_varients->count() > 0) {
                    $size = $is_varients->min('size');
                }


                $end_pro = EndProduct::find($end_product_id);

                if ($re_use_qty[$key] > 0) {
                    $reusableReturn = new ReusableProducts();
                    $reusableReturn->reusable_type = $end_pro->reusable_type;
                    $reusableReturn->product_id = $end_product_id;
                    $reusableReturn->note_id = -1;
                    $reusableReturn->qty = $re_use_qty[$key];
                    $reusableReturn->expected_weight = $re_use_qty[$key] * $end_pro->weight;
                    $reusableReturn->accepted_weight = $re_use_qty[$key] * $end_pro->weight;
                    //collect type (1->semi,2->endproduct,3->salesReturn,4->MergeRemoval,5->StoresReturn)
                    $reusableReturn->collect_type = 5;
                    $reusableReturn->status = 1;
                    $reusableReturn->type = 're_use';
                    $reusableReturn->category_id = $end_pro->category_id;
                    $reusableReturn->agent_id = 0;
                    $reusableReturn->type_id = 4;//1=merge-reuse,2=merge-destroy,3=merge-lost,4=stores-reuse,5=stores-destroy,6=stores-lost,7=System Genarated,8=Return-Reuse,9=Return-Destroy,10=Return-SR,11-Return-Factory,12=Return-Lost

                    $reusableReturn->created_by =$employee;
                    $reusableReturn->save();

                    $end_product = EndProduct::find($end_product_id);
                    $trans = new ReusableStock();
                    $trans->causer_id = $end_product_id;
                    $trans->causer_type = 'APP\\EndProduct';
                    $trans->qty = $re_use_qty[$key];
                    $trans->expected_weight = $re_use_qty[$key] * $end_pro->weight;
                    $trans->sales_return_header_id = 0;
                    $trans->reusable_product_id = $end_product_id;
                    $trans->reusable_type = $end_product->reusable_type;
                    $trans->received_type = 'stores_return';
                    $trans->save();

                    StockTransaction::endProductStockTransaction(2, $end_product_id, 0, 0, 1, $re_use_qty[$key], 0, 1);
                    StockTransaction::updateEndProductAvailableQty(2, $end_product_id,$re_use_qty[$key]);
                    $end_product = EndProduct::find($end_product_id);
                    $this->recordEndProductOutstanding($end_product,1, $re_use_qty[$key], 'Reusable-Stores Return', $trans->id, 'App\\ReusableStock',0,15,2,$reusableReturn->id);


                }

                if ($destroy_qty[$key] > 0) {
                    $end_product = EndProduct::find($end_product_id);
                    $reusableReturn = new ReusableProducts();
                    $reusableReturn->reusable_type = $end_pro->reusable_type;
                    $reusableReturn->product_id = $end_product_id;
                    $reusableReturn->note_id = -1;
                    $reusableReturn->qty = $destroy_qty[$key];
                    $reusableReturn->expected_weight = $destroy_qty[$key];
                    $reusableReturn->accepted_weight = 0;
                    //collect type (1->semi,2->endproduct,3->salesReturn,4->MergeRemoval,5->StoresReturn)
                    $reusableReturn->collect_type = 5;
                    $reusableReturn->status = 1;
                    $reusableReturn->type = 'destroy';
                    $reusableReturn->category_id = $end_product->category_id;
                    $reusableReturn->agent_id = 0;
                    $reusableReturn->type_id = 5;//1=merge-reuse,2=merge-destroy,3=merge-lost,4=stores-reuse,5=stores-destroy,6=stores-lost,7=System Genarated,8=Return-Reuse,9=Return-Destroy,10=Return-SR,11-Return-Factory,12=Return-Lost

                    $reusableReturn->created_by =$employee;
                    $reusableReturn->save();


                    StockTransaction::endProductStockTransaction(2, $end_product_id, 0, 0, 1, $destroy_qty[$key], 0, 1);
                    StockTransaction::updateEndProductAvailableQty(2, $end_product_id,$destroy_qty[$key]);

                    $this->recordEndProductOutstanding($end_product,1, $destroy_qty[$key], 'Destroy-Stores Return', $reusableReturn->id, 'BurdenManager\\Models\\ReusableProducts',0,16,2,$reusableReturn->id);

                }


                if ($lost_qty[$key] != 0) {
                    $end_product = EndProduct::find($end_product_id);
                    $reusableReturn = new ReusableProducts();
                    $reusableReturn->reusable_type = $end_pro->reusable_type;
                    $reusableReturn->product_id = $end_product_id;
                    $reusableReturn->note_id = -1;
                    $reusableReturn->qty = $lost_qty[$key];
                    $reusableReturn->expected_weight = $lost_qty[$key];
                    $reusableReturn->accepted_weight = 0;
                    //collect type (1->semi,2->endproduct,3->salesReturn,4->MergeRemoval,5->StoresReturn)
                    $reusableReturn->collect_type = 5;
                    $reusableReturn->status = 1;
                    $reusableReturn->type = 'lost';
                    $reusableReturn->category_id = $end_product->category_id;
                    $reusableReturn->agent_id = 0;
                    $reusableReturn->type_id = 6;//1=merge-reuse,2=merge-destroy,3=merge-lost,4=stores-reuse,5=stores-destroy,6=stores-lost,7=System Genarated,8=Return-Reuse,9=Return-Destroy,10=Return-SR,11-Return-Factory,12=Return-Lost

                    $reusableReturn->created_by =$employee;
                    $reusableReturn->save();


                    StockTransaction::endProductStockTransaction(2, $end_product_id, 0, 0, 1, $lost_qty[$key], 0, 1);
                    StockTransaction::updateEndProductAvailableQty(2, $end_product_id,$lost_qty[$key]);

                    $this->recordEndProductOutstanding($end_product,1, $lost_qty[$key], 'Lost-Stores Return', $reusableReturn->id, 'BurdenManager\\Models\\ReusableProducts',0,17,2,$reusableReturn->id);

                }

            }
        }

        return redirect()->route('end.remove')->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => 'Process Finished Successfully'
        ]);
    }
}
