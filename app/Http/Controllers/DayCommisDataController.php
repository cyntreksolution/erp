<?php

namespace App\Http\Controllers;

use App\CratesTransaction;
use App\User;
use App\DayCommisData;
use Carbon\Carbon;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use EmployeeManager\Models\Employee;
use Illuminate\Http\Request;
use SalesOrderManager\Models\SalesOrder;

class DayCommisDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $existDayCommis = DayCommisData::where(['emp_id' => $request->emp_id, 'comis_date' => $request->comis_date, 'comis_type' => 1])->first();
        $dayCommis = new DayCommisData();
        $dayCommis->emp_id = $request->emp_id;
        $dayCommis->comis_date = $request->comis_date;
        $dayCommis->comis_value = round($request->day_comis, 2);
        $dayCommis->comis_type = 1;
        if (empty($existDayCommis)) {
            $dayCommis->save();
        }
        //   return redirect(route('salary.index'));
        $employees = Employee::pluck('full_name', 'id');
        return redirect(route('salary-comis.index', ['employee' => $request->emp_id, 'date' => $request->comis_date]));
        return view('salary-attendance', compact('employees'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function monthlyCommission(Request $request)
    {
        $employee = $request->employee;
        $date_range = $request->date_range;
        $comission = $request->comission;

        $dates = explode(' - ', $date_range);
        $start = $dates[0];
        $end = $dates[1];

        $month = Carbon::parse($end)->format('m');

    }


}

