<?php

namespace App\Http\Controllers;

use App\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use EmployeeManager\Models\Group;
use EmployeeManager\Models\GroupEmployee;
use EndProductBurdenManager\Models\EndProductBurden;
use EndProductManager\Models\Category;
use EndProductManager\Models\EndProduct;
use Illuminate\Http\Request;
use SalesOrderManager\Models\SalesOrder;
use SalesRepManager\Models\SalesRep;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class EndProductBurdenMergeController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:end_product_burden_manage-index', ['only' => ['endProductHistoryData']]);
        $this->middleware('permission:EndProductBurdenHistory-list', ['only' => ['list']]);
        $this->middleware('permission:EndProductBurdenHistory-tabledata', ['only' => ['index','tabledata','endProductBurdenList']]);

    }
    public function index()
    {
        $categories = Category::all()->pluck('name', 'id');
        $endProducts = EndProduct::all()->pluck('name', 'id');
        return view('EndProductBurdenManager::burden.merge-list', compact('endProducts', 'categories'));
    }

    public function endProductBurdenList(Request $request)
    {

        $endProduct = EndProduct::get()->pluck('name', 'id');
        $category = Category::get()->pluck('name', 'id');
        return view('history-list', compact('endProduct', 'category'));
    }

    public function show_end_product(EndProductBurden $burden)
    {
        $burden = EndProductBurden::where('id', '=', $burden->id)
            ->with('endProdcuts', 'recipe')
            ->first();


       // $date = Carbon::now();

       // $employee_group=GroupEmployee::where('group_id', $burden->employee_group)->first();
      //  $baker=GroupEmployee::where('group_id', $burden->baker)->first();


        $employee_group=Group::where('id', $burden->employee_group)->first();
        $baker=Group::where('id', $burden->baker)->first();

       // $teams = GroupEmployee::whereDate('created_at', $date)->get();
        if(!empty($employee_group)){
            $date_string = Carbon::parse($employee_group->created_at)->format('Y-m-d') . '%';
        }else{
            $date_string = Carbon::parse($burden->created_at)->format('Y-m-d') . '%';
        }

        if(!empty($baker)){
            $date_string1 = Carbon::parse($baker->created_at)->format('Y-m-d') . '%';
        }else{
            $date_string1 = Carbon::parse($burden->created_at)->format('Y-m-d') . '%';
        }


        $employee_groups = Group::where('created_at', 'like',$date_string)->get();
        $baker = Group::where('created_at', 'like',$date_string1)->get();


        return view('show_end_products', compact('employee_groups','baker'))->with([
            'burden' => $burden,
            'burdenClass' => new EndProductBurdenMergeController
        ]);
    }

    public function getBakers($baker){
        return Group::where('id', '=', $baker)->get();
    }

    public function getMakers($maker){
        return Group::where('id', '=', $maker)->get();
    }

    public function updateEndGroup(Request $request,$id){
        $burden = EndProductBurden::where('id', '=', $id)
            ->with('endProdcuts', 'recipe')
            ->first();
        $burden->employee_group = $request->employee_group;
        $burden->save();
        return redirect()->back()->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => "Employee Group Updated",

        ]);

    }

    public function updateEndBaker(Request $request,$id){
        $burden = EndProductBurden::where('id', '=', $id)
            ->with('endProdcuts', 'recipe')
            ->first();
        $burden->baker = $request->baker;
        $burden->save();
        return redirect()->back()->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => "Employee Group Updated",
        ]);

    }




    public function endProductHistoryData(Request $request)
    {
        ini_set('memory_limit', '64M');

        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];


        $columns = ['serial', 'end_product_id', 'burden_qty', 'created_at'];
        $order_column = $columns[$order_by[0]['column']];

        $stat_data = EndProductBurden::query();

        $burdens = EndProductBurden::tableData($order_column, $order_by_str, $start, $length);
        $burdenCount = $stat_data->count();

        $filterCount = 0;
        $stat_a = 0;
        $stat_b = 0;
        $stat_c = 0;
        $stat_d = 0;

        if ($request->filter == true) {
            $category = $request->category;
            $end_product = $request->end_product;
            $status = $request->status;
            $date_range = $request->date_range;
            $date_type = $request->date_type;


            $burdens = EndProductBurden::tableData($order_column, $order_by_str, $start, $length)->filterData($category, $end_product, $status, $date_range)->get();
            $filterCount = EndProductBurden::filterData($category, $end_product, $status, $date_range,$date_type)->count();




            $dataset = $stat_data->filterData($category, $end_product, $status, $date_range,$date_type)->get();
            foreach ($dataset as $stat) {
                if ($stat->status >= 5) {
                    $grades = $stat->grade;
                    foreach ($grades as $key => $grade) {
                        switch ($grade['grade_id']) {
                            case  1:
                                $stat_a = $stat_a + $grade['burden_qty'];
                                break;
                            case 2:
                                $stat_b = $stat_b + $grade['burden_qty'];
                                break;
                            case  3:
                                $stat_c = $stat_c + $grade['burden_qty'];
                                break;
                            case  4:
                                $stat_d = $stat_d + $grade['burden_qty'];
                                break;
                        }
                    }
                }
            }
            $stat_data = $stat_data->filterData($category, $end_product,$status, $date_range)->select(DB::raw('SUM(burden_size) as burden_size'))->first();



        } elseif (is_null($search) || empty($search)) {
            $date_string = Carbon::today()->startOfDay()->format('Y-m-d H:i:s');
            $dataset = $stat_data->where('created_at', '>', $date_string)->get();
            foreach ($dataset as $stat) {
                if ($stat->status >= 5) {
                    $grades = $stat->grade;
                    foreach ($grades as $key => $grade) {
                        switch ($grade['grade_id']) {
                            case  1:
                                $stat_a = $stat_a + $grade['burden_qty'];
                                break;
                            case 2:
                                $stat_b = $stat_b + $grade['burden_qty'];
                                break;
                            case  3:
                                $stat_c = $stat_c + $grade['burden_qty'];
                                break;
                            case  4:
                                $stat_d = $stat_d + $grade['burden_qty'];
                                break;
                        }
                    }
                }
            }
            $stat_data = $stat_data->where('created_at', '>', $date_string)->select(DB::raw('SUM(burden_size) as burden_size'))->first();
            $burdens = $burdens->where('created_at', '>', $date_string)->get();
            $burdenCount = EndProductBurden::where('created_at', '>', $date_string)->count();
        } else {
            $burdens = $burdens->searchData($search)->get();
            $burdenCount = $burdens->count();
        }

        $data = [];
        $i = 0;

        $stat_burden_size = 0;


        foreach ($burdens as $key => $burden) {
            $teameb = null;
            if($date_type == 2) {
                $groups = Group::find($burden->employee_group);
                $teameb = $serial = "<a target='_blank' href='#'>$groups->name</a>";
            }elseif ($date_type == 3){
                $groups = Group::find($burden->baker);
                $teameb = $serial = "<a target='_blank' href='#'>$groups->name</a>";
            }

            $grades = null;
            $u = "/view-history/$burden->id";
            $serial = "<a target='_blank' href='$u'>$burden->serial</a>";
             $btnDelete =null;
            if (Sentinel::check()->roles[0]->slug == 'owner') {
                $btnDelete = ($burden->status == 1) ? '<button class="btn btn-outline-danger btn-sm mr-1" onclick="deleteBurden(' . $burden->id . ')"> <i class="fa fa-trash"></i> </button>' : null;
            }

            switch ($burden->status) {
                case 1:
                    $statusBtn = '<button class="btn btn-outline-warning btn-sm mr-1">pending</button>';
                    break;

                case 2:
                    $statusBtn = '<button class="btn btn-outline-info btn-sm mr-1">received</button>';
                    break;

                case 3:
                    $statusBtn = '<button class="btn btn-outline-primary btn-sm mr-1">creating</button>';
                    break;
                case 4:
                    $statusBtn = '<button class="btn btn-outline-danger btn-sm mr-1">burning</button>';
                    break;
                case 5:
                    $statusBtn = '<button class="btn btn-outline-success btn-sm mr-1">grade</button>';
                    break;
                case 6:
                    $statusBtn = '<button class="btn btn-outline-dark btn-sm mr-1">finished</button>';
                    break;
            }

            $gradeA = 0;
            $gradeB = 0;
            $gradeC = 0;
            $gradeD = 0;

            if ($burden->status >= 5) {
                $grades = $burden->grade;
                foreach ($grades as $key => $grade) {
                    switch ($grade['grade_id']) {
                        case  1:
                            $gradeA = $grade['burden_qty'];
                            break;
                        case 2:
                            $gradeB = $grade['burden_qty'];
                            break;
                        case  3:
                            $gradeC = $grade['burden_qty'];
                            break;
                        case  4:
                            $gradeD = $grade['burden_qty'];
                            break;
                    }
                }
            }


            $stat_burden_size = $stat_data->burden_size;

            $ct = null;

            if($burden->endProdcuts->customize == 1){
                $ct = " -XX";
            }

            $data[$i] = array(
                $serial,
                $burden->endProdcuts->name.$ct,
                $burden->burden_size,
                Carbon::parse($burden->created_at)->format('Y-m-d H:m:s'),
                $gradeA,
                $gradeB,
                $gradeC,
                $gradeC * $burden->endProdcuts->weight,
                $gradeD,
                $btnDelete.$teameb.$statusBtn
            );
            $i++;
        }


        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($burdenCount),
            "recordsFiltered" => intval($filterCount),
            "data" => $data,
            "stat_burden_size" => number_format($stat_burden_size, 3),
            "stat_a" => number_format($stat_a, 3),
            "stat_b" => number_format($stat_b, 3),
            "stat_c" => number_format($stat_c, 3),
            "stat_d" => number_format($stat_d, 3),
        ];

        return json_encode($json_data);
    }

    public function tableData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $recordsCount = 0;

        $date = $request->date_range;
        $columns = ['id'];
        $order_column = $columns[$order_by[0]['column']];

        // $records = EndProductBurden::tableData($order_column, $order_by_str, $start, $length);
        // $records = $records->where('status', '!=', '5')->orderBy('status');


        $records = EndProductBurden::tableData($order_column, $order_by_str, $start, $length);
        $records = $records->where('status', '=', 1)->orderBy('status');

        if ($request->filter == true) {
            $category = $request->category;
            $end_product = $request->end_product;

            $records = $records->filterData($category, $end_product)->get();
            $salesOrdersCount = $records->count();
        } elseif (is_null($search) || empty($search)) {
            $records = $records->get();
            $salesOrdersCount = $records->count();
        } else {
            $records = $records->searchData($search)->get();
            $salesOrdersCount = $records->count();
        }

        $recordsCount = $salesOrdersCount;

        $data[][] = array();
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;

        $btn_status = null;

        $condition = (!empty($request->category)) ? 1 : 0;

        $btn_approve = null;

        foreach ($records as $key => $record) {
            if ($record->endProduct->category->loading_type == 'to_burden') {
                $btn_approve = "<button value='$record->id' onclick='showModal($record->id)' class='addItem btn btn-sm btn-warning ml-1'><i class='fa fa-hand-grab-o'></i></button>";
            }

            $data[$i] = array(
                ($condition) ? "<input type='checkbox' name='burden_id[]' value='$record->id'>" : null,
                $record->serial,
                $record->endProduct->name,
                $record->expected_end_product_qty,
                $record->recipe->name,
                $btn_approve,
            );
            $i++;
        }


        if ($recordsCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($recordsCount),
            "recordsFiltered" => intval($recordsCount),
            "data" => $data
        ];

        return json_encode($json_data);
    }

    public function deleteBurden(Request $request){
        $burden = EndProductBurden::find($request->burden_id);
        $burden->delete();
        return 'true';
    }

    public function destroy(Burden $burden)
    {
        $burden->delete();
        if ($burden->trashed()) {
            return true;
        } else {
            return false;
        }
    }

    public function fstage(Request $request)
    {

        $id = $request->burden_id;
        $data = EndProductBurden::find($id);
        $data->response = 0;
        $data->save();
        return redirect()->back();
    }

    public function sstage(Request $request)
    {
        
        $id = $request->burden_id;
        $data = EndProductBurden::find($id);
        $data->response = 1;
        $data->save();
        return redirect()->back();
    }


}
