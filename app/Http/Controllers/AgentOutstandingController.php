<?php

namespace App\Http\Controllers;

use App\AgentPayment;
use App\LoadingHeader;
use App\SalesReturnHeader;
use App\Traits\AgentLog;
use Carbon\Carbon;
use EndProductManager\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use SalesRepManager\Models\SalesRep;
use StockManager\Classes\StockTransaction;

class AgentOutstandingController extends Controller
{
    use AgentLog;
    function __construct()
    {
        $this->middleware('permission:agent_outstanding-index', ['only' => ['index','tableData']]);
        $this->middleware('permission:AgentOutstanding-list', ['only' => ['list']]);
//        $this->middleware('permission:agent_outstanding-view', ['only' => ['forcePay']]);

    }
    public function index()
    {
        $agents = SalesRep::all()->pluck('name_with_initials', 'id');
        $categories = Category::all()->pluck('name', 'id');
        return view('account.outstand_list', compact('agents', 'categories'));
    }

    public function tableData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'loading_date', 'total', 'agent_id'];
        $order_column = $columns[$order_by[0]['column']];

        $order_column = 'loading_date';
        $order_by_str = 'DESC';

        $stat_data = LoadingHeader::query();
        $invoices = LoadingHeader::tableData($order_column, $order_by_str, $start, $length);

        if ($request->filter == true) {
            $agent = $request->agent;
            $category = $request->category;
            $date_range = $request->date_range;
            $desc1 = $request->desc1;

            $stst = $stat_data
                ->select(DB::raw('SUM(net_amount) as net_sum'), DB::raw('SUM(paid_amount) as paid_amount'))
                ->filterData($date_range, $agent, $category, null, null,null,$desc1)
                ->whereIsPaid(0)
                ->first();
            $invoices = $invoices->filterData($date_range, $agent, $category, null, null,null,$desc1)->whereIsPaid(0)->get();
            $invoiceCount = $invoices->count();
        } elseif (is_null($search) || empty($search)) {
            $invoices = $invoices->get();
            $stst = $stat_data
                ->select(DB::raw('SUM(net_amount) as net_sum'), DB::raw('SUM(paid_amount) as paid_amount'))
                ->whereIsPaid(0)
                ->first();
            $invoiceCount = LoadingHeader::whereIsPaid(0)->count();
        } else {
            $invoices = $invoices->searchData($search)->get();
            $invoiceCount = $invoices->count();
        }

        $paid_sum = $stst->paid_amount;
        $net_sum = $stst->net_sum;
        $net_rem = $stst->net_sum - $stst->paid_amount;
        $data = [];
        $i = 0;
        $user = Auth::user();
        $can_view = ($user->can('order_list-orderPreview')) ? 1 : 0;

        foreach ($invoices as $key => $invoice) {

            $last = null;

            if (!empty($date_range)) {
                $dates = explode(' - ', $date_range);
                $end = Carbon::parse($dates[1]);
                $last = Carbon::parse($end);
            }else{
                $last =  Carbon::today();
            }

            $first = Carbon::parse($invoice->loading_date);

            $age = $last->diffInDays($first);

            if ($invoice->net_amount - $invoice->paid_amount !=0) {
                $btn_force_pay = null;
                if ($can_view) {
                    $btn_preview = "<a target='_blank' href='" . route('order-list.preview', [$invoice->id]) . "' class='ml-2 btn btn-sm btn-primary'>Preview</a>";
                }else{
                    $btn_preview=[];
                }
                $rep = !empty($invoice->salesOrder) ? $invoice->salesOrder->salesRep : null;
                $statusBtn = null;

                if ($role = Auth::user()->hasRole(['Owner','Super Admin'])) {
                    if ($invoice->net_amount < 0 && $invoice->is_paid == 0) {
                        $btn_force_pay = "<a href='" . route('outstanding.force', [$invoice->id]) . "' class='ml-2 btn btn-sm btn-danger'><i class='fa fa-dollar'></i> </a>";
                    }
                }


                $data[$i] = array(
                    $invoice->id,
                    Carbon::parse($invoice->loading_date)->format('Y-m-d H:m:s'),
                    !empty($rep) ? $rep->name_with_initials : 0,
                    !empty($invoice->salesOrder) ? $invoice->salesOrder->template->category->name : null,
                    $invoice->invoice_number,
                    number_format($invoice->net_amount, 2),
                    number_format($invoice->paid_amount, 2),
                    number_format($invoice->net_amount - $invoice->paid_amount, 2),
                    $age,
                    $btn_preview . $btn_force_pay
                );
                $i++;
            }
        }

        if ($invoiceCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($invoiceCount),
            "recordsFiltered" => intval($invoiceCount),
            "data" => $data,
            "paid_sum" => number_format($paid_sum, 2),
            "net_sum" => number_format($net_sum, 2),
            "net_rem" => number_format($net_rem, 2),
        ];

        return json_encode($json_data);
    }

    public function forcePay(Request $request)
    {
        $loadingHeader = LoadingHeader::find($request->id);
        $amount = $loadingHeader->net_amount;
        $loadingHeader->is_paid = 1;
        $loadingHeader->paid_amount = $amount;
        $loadingHeader->save();

        $data = ['loading_header_id' => $loadingHeader->id, 'invoice_value' => $amount, 'is_full_payment' => 1, 'settled_value' => $amount];

        $payment = new AgentPayment();
        $payment->agent_id = $loadingHeader->salesOrder->salesRep->id;
        $payment->serial = 'PAY-FORCE-' . Carbon::now()->format('ymdHis');
        $payment->cheque_amount_value = 0;
        $payment->cash_amount = $amount;
        $payment->total_amount = $amount;
        $payment->notes_data = json_encode([]);
        $payment->status = 'approved';
        $payment->invoice_data = json_encode($data);
        $payment->save();

        $ref = SalesRep::find($payment->agent_id);
        $this->recordOutstanding($ref, -($payment->total_amount), "Agent's FORCE Payment", $payment->id, 'App\\AgentPayment');
        StockTransaction::updateAgentBalance(2, $payment->agent_id, $payment->total_amount);

        return redirect()->route('outstanding.index');
    }



}
