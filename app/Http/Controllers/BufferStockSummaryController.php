<?php

namespace App\Http\Controllers;

use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RawMaterialManager\Models\RawMaterial;

class BufferStockSummaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:buffer_stock_summary-index', ['only' => ['index','datatable']]);
        $this->middleware('permission:BufferStockSummary-list', ['only' => ['list']]);

    }
    public function index()
    {
        //
        return view('buffer_stock_summary');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Return JSON array for datatable.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function datatable(Request $request)
    {
        $order_by = $_REQUEST['order']; // This array contains order information of clicked column
        $search = $_REQUEST['search']['value']; // This array contains search value information datatable
        $start = $_REQUEST['start']; // start limit of data
        $length = $_REQUEST['length']; // end limit of data
        $order_by_str = $order_by[0]['dir'];
        if ($order_by[0]['column']==0){
            $order_column = "name";
        }
        if ($order_by[0]['column']==1){
            $order_column = "buffer_stock";
        }
        if ($order_by[0]['column']==2){
            $order_column = "available_qty";
        }
        if ($order_by[0]['column']==3){
            $order_column = "status";
        }
        if ($order_by[0]['column']==4){
            $order_column = "name";
        }
        $where_query = "1";
        if ($search!='' || $search!=NULL) {
            $where_query = "(name like '%$search%' or available_qty like '%$search%' or buffer_stock like '$search' or status like '$search')";
        }
        $records = RawMaterial::select('name','available_qty','buffer_stock','status')
            ->whereRaw($where_query)
            ->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length)
            ->get();
        $records_count = RawMaterial::select(DB::raw('count(*) as all_count'))
            ->whereRaw($where_query)
            ->first();

        $data[][] = array();
        $i = 0;
        foreach ($records as $record)
        {

            $label_text = "Enabled";
            $label_class = "success";
            if ($record->enabled == 0) {
                $label_text = "Disabled";
                $label_class = "danger";
            }
            $label = "<label class='label label-$label_class'>$label_text</label>";

            $data[$i] = array(
                $record->name,
                $record->buffer_stock,
                $record->available_qty,
                $label,
                0
            );
            $i++;
        }

        $totalRecords = $records_count->all_count;
        if ($totalRecords==0){
            $data = array();
        }
        $json_data = array(
            "draw"            => intval($_REQUEST['draw']),
            "recordsTotal"    => intval($totalRecords),
            "recordsFiltered" => intval($totalRecords),
            "data"            => $data   // total data array
        );
        $json_data = json_encode($json_data);
        return $json_data;
    }

    public function download(Request $request)
    {
        $records = RawMaterial::select('name','available_qty','buffer_stock','status')->get();
        $pdf = PDF::loadView('buffer_stock_summary_download', compact('records'))->setPaper('a4', 'portrait');
        return $pdf->download('BUFFER_STOCK_SUMMARY.pdf');

    }
}
