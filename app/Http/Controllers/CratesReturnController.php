<?php

namespace App\Http\Controllers;

use App\CratesTransaction;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use SalesOrderManager\Models\SalesOrder;

class CratesReturnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('crates-return.table');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function tableData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $recordsCount = 0;

        $date = $request->date_range;
        $columns = ['id'];
        $order_column = $columns[$order_by[0]['column']];

        $records = CratesTransaction::tableData($order_column, $order_by_str, $start, $length);

        if (Auth::user()->hasRole(['Sales Agent'])) {
            $records = $records->where('created_by', '=', Auth::user()->id);
        }

        $records = $records->whereType('return')->whereCratesApprove(null);

        $records = $records->get();
        $salesOrdersCount = $records->count();
        $recordsCount = $salesOrdersCount;


        $data[][] = array();
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;

        $btn_status = null;

        $btnReturnCrates = null;
        $btnSendCrates = null;
        $btnViewCrates = null;
        $btnApproveCrates = null;

        foreach ($records as $key => $record) {
            $btnViewCrates = "<a class='btn btn-sm btn-success' href='" . route('crates-request.cratesRequestShow', [$record->id]) . "'>Approve</a>";
            $data[$i] = array(
                $record->id,
                Carbon::parse($record->created_at)->format('Y-m-d H:i:s'),
                $record->agent->name_with_initials,
                $record->loading->salesOrder->template->category->name,
                $record->loading->salesOrder->template->day . ' - ' . $record->loading->salesOrder->template->time,
                $record->loading->invoice_number,
                $btnViewCrates ,
            );
            $i++;
        }


        if ($recordsCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($recordsCount),
            "recordsFiltered" => intval($recordsCount),
            "data" => $data
        ];

        return json_encode($json_data);
    }

}
