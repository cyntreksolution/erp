<?php

namespace App\Http\Controllers;

use App\CrateInquiryAgent;
use App\CrateInquiryStore;
use App\LoadingHeader;
use Carbon\Carbon;
use CashLockerManager\Models\CashBookTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PurchasingOrderManager\Models\PurchasingOrder;
use RawMaterialManager\Models\RawMaterial;
use SalesRepManager\Models\SalesRep;

class CratesInquiryStoreController extends Controller
{
    public function index()
    {
        $agents = SalesRep::all()->pluck('name_with_initials', 'id');
        $raw_materials = RawMaterial::where('type', '=', 5)->get()->pluck('name', 'raw_material_id');
        return view('crate_usage_list', compact('raw_materials', 'agents'));
    }

    public function tableData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'created_at'];
        $order_column = $columns[$order_by[0]['column']];

        $query = CrateInquiryStore::select('users.name_with_initials', 'crates_inquiry_stores.*')->leftJoin('users', 'users.id', 'crates_inquiry_stores.agent_id');

        $count = 0;
        $records = collect();
        $balance_records = collect();

        if ($request->filter == true) {
            $agent_id = $request->agent;
            $crate_id = null;// for future use
            $date_range = $request->date_range;
            $reference_type = $request->reference_type;

            $dates = explode(' - ', $date_range);
            $start = Carbon::parse($dates[0])->format('Y-m-d 00:00:00');
            $end = Carbon::parse($dates[1])->format('Y-m-d 23:59:59');
            //  $start = Carbon::parse($dates[0])->format('Y-m-d');
            //  $end = Carbon::parse($dates[1])->format('Y-m-d');
            $whereQuery = "where t1.created_at >='$start' and t1.created_at <= '$end'";

            if (!empty($agent_id)) {
                $whereQuery .= " and t1.agent_id = $agent_id";
            }
            if (!empty($reference_type)) {
                $whereQuery .= " and t1.desc_id = $reference_type";
            }


            $query = "with t1 as (SELECT crates_inquiry_stores.reference,crates_inquiry_stores.created_at,crates_inquiry_stores.agent_id, crates_inquiry_stores.desc_id,crates_inquiry_stores.description,
CASE WHEN crates_inquiry_stores.crate_id = '193' THEN sum(crates_inquiry_stores.qty) END 'bcl',
CASE WHEN crates_inquiry_stores.crate_id = '194' THEN sum(crates_inquiry_stores.qty) END 'bcs',
CASE WHEN crates_inquiry_stores.crate_id = '195' THEN sum(crates_inquiry_stores.qty) END 'crl',
CASE WHEN crates_inquiry_stores.crate_id = '196' THEN sum(crates_inquiry_stores.qty) END 'crs',
CASE WHEN crates_inquiry_stores.crate_id = '197' THEN sum(crates_inquiry_stores.qty) END 'lid'
FROM crates_inquiry_stores
GROUP BY DATE_FORMAT(crates_inquiry_stores.created_at,'%Y-%m-%d %H:%i'),crates_inquiry_stores.agent_id, crates_inquiry_stores.crate_id, crates_inquiry_stores.desc_id 
ORDER BY 'crates_inquiry_stores.created_at' DESC)
SELECT loading_headers.invoice_number,t1.created_at,users.name_with_initials,t1.description,sum(t1.bcl) as bcl,sum(t1.bcs) as bcs,sum(t1.crl) as crl,sum(t1.crs) as crs,sum(t1.lid) as lid 
FROM  t1  LEFT JOIN users ON t1.agent_id = users.id 
LEFT JOIN loading_headers on t1.reference = loading_headers.id
$whereQuery GROUP BY DATE_FORMAT(t1.created_at,'%Y-%m-%d %H:%i'),t1.agent_id";

            $records = collect(DB::select(DB::raw($query)));

            // dd($records);

            $start_b = Carbon::parse($dates[0])->format('Y-m-d');
            $end_b = Carbon::parse($dates[1])->format('Y-m-d');

            /*  $balance_query = "select (SELECT start_balance from crates_inquiry_stores where created_at like '$start_b%' and crate_id = 193 order by created_at asc LIMIT 1 ) as 'bcl_st',
  (SELECT start_balance from crates_inquiry_stores where created_at like '$start_b%' and crate_id = 194 order by created_at asc LIMIT 1 ) as 'bcs_st',
  (SELECT start_balance from crates_inquiry_stores where created_at like '$start_b%' and crate_id = 195 order by created_at asc LIMIT 1 ) as 'crl_st',
  (SELECT start_balance from crates_inquiry_stores where created_at like '$start_b%' and crate_id = 196 order by created_at asc LIMIT 1 ) as 'crs_st',
  (SELECT start_balance from crates_inquiry_stores where created_at like '$start_b%' and crate_id = 197 order by created_at asc LIMIT 1 ) as 'lid_st',
  (SELECT end_balance from crates_inquiry_stores where created_at like '$end_b%' and crate_id = 193 order by created_at desc LIMIT 1 ) as 'bcl_en',
  (SELECT end_balance from crates_inquiry_stores where created_at like '$end_b%' and crate_id = 194 order by created_at desc LIMIT 1 ) as 'bcs_en',
  (SELECT end_balance from crates_inquiry_stores where created_at like '$end_b%' and crate_id = 195 order by created_at desc LIMIT 1 ) as 'crl_en',
  (SELECT end_balance from crates_inquiry_stores where created_at like '$end_b%' and crate_id = 196 order by created_at desc LIMIT 1 ) as 'crs_en',
  (SELECT end_balance from crates_inquiry_stores where created_at like '$end_b%' and crate_id = 197 order by created_at desc LIMIT 1 ) as 'lid_en'";*/

            $balance_query = "select (SELECT end_balance from crates_inquiry_stores where created_at < '$start_b' and crate_id = 193 order by created_at desc LIMIT 1 ) as 'bcl_st',
(SELECT end_balance from crates_inquiry_stores where created_at <  '$start_b' and crate_id = 194 order by created_at desc LIMIT 1 ) as 'bcs_st',
(SELECT end_balance from crates_inquiry_stores where created_at < '$start_b' and crate_id = 195 order by created_at desc LIMIT 1 ) as 'crl_st',
(SELECT end_balance from crates_inquiry_stores where created_at < '$start_b' and crate_id = 196 order by created_at desc LIMIT 1 ) as 'crs_st',
(SELECT end_balance from crates_inquiry_stores where created_at < '$start_b' and crate_id = 197 order by created_at desc LIMIT 1 ) as 'lid_st',
(SELECT start_balance from crates_inquiry_stores where created_at like '$start_b%' and crate_id = 193 order by created_at asc LIMIT 1 ) as 'bcl_st1',
(SELECT start_balance from crates_inquiry_stores where created_at like  '$start_b%' and crate_id = 194 order by created_at asc LIMIT 1 ) as 'bcs_st1',
(SELECT start_balance from crates_inquiry_stores where created_at like '$start_b%' and crate_id = 195 order by created_at asc LIMIT 1 ) as 'crl_st1',
(SELECT start_balance from crates_inquiry_stores where created_at like '$start_b%' and crate_id = 196 order by created_at asc LIMIT 1 ) as 'crs_st1',
(SELECT start_balance from crates_inquiry_stores where created_at like '$start_b%' and crate_id = 197 order by created_at asc LIMIT 1 ) as 'lid_st1',
(SELECT end_balance from crates_inquiry_stores where created_at like '$end_b%' and crate_id = 193 order by created_at desc LIMIT 1 ) as 'bcl_en',
(SELECT end_balance from crates_inquiry_stores where created_at like  '$end_b%' and crate_id = 194 order by created_at desc LIMIT 1 ) as 'bcs_en',
(SELECT end_balance from crates_inquiry_stores where created_at like '$end_b%' and crate_id = 195 order by created_at desc LIMIT 1 ) as 'crl_en',
(SELECT end_balance from crates_inquiry_stores where created_at like '$end_b%' and crate_id = 196 order by created_at desc LIMIT 1 ) as 'crs_en',
(SELECT end_balance from crates_inquiry_stores where created_at like '$end_b%' and crate_id = 197 order by created_at desc LIMIT 1 ) as 'lid_en'";

            $balance_records = collect(DB::select(DB::raw($balance_query)))->first();

         //   dd($balance_records);

        } elseif (is_null($search) || empty($search)) {

        } else {

        }


        $data = [];
        $i = 0;
        $count = $records->count();

        $bcl = 0; $bcs = 0; $crl = 0; $crs = 0; $lid = 0;

        if (empty($balance_records->bcl_st1)) {

            $data[0] = array(
                'Balance In Hand',
                '',
                '',
                $balance_records->bcl_st,
                $balance_records->bcs_st,
                $balance_records->crl_st,
                $balance_records->crs_st,
                $balance_records->lid_st,

            );
        }else {
            if (empty($agent_id) && empty($reference_type)) {

                if (!empty($balance_records->bcl_st1)) {

                    $data[0] = array(
                        'Start Balance',
                        '',
                        '',
                        $balance_records->bcl_st1,
                        $balance_records->bcs_st1,
                        $balance_records->crl_st1,
                        $balance_records->crs_st1,
                        $balance_records->lid_st1,

                    );


                } elseif (!empty($balance_records->bcl_st)) {

                    $data[0] = array(
                        'Start Balance',
                        '',
                        '',
                        $balance_records->bcl_st,
                        $balance_records->bcs_st,
                        $balance_records->crl_st,
                        $balance_records->crs_st,
                        $balance_records->lid_st,

                    );

                }
                $i++;

            }

            foreach ($records as $key => $record) {

                $bcl = $bcl + $record->bcl;
                $bcs = $bcs + $record->bcs;
                $crl = $crl + $record->crl;
                $crs = $crs + $record->crs;
                $lid = $lid + $record->lid;

                $data[$i] = array(
                    Carbon::parse($record->created_at)->format('Y-M-d H:i:s'),
                    $record->description,
                    $record->name_with_initials . ' - ' . $record->invoice_number,
                    $record->bcl,
                    $record->bcs,
                    $record->crl,
                    $record->crs,
                    $record->lid,

                );
                $i++;

            }


            if ($count == 0) {
                $data = [];
            } else {
                if (empty($agent_id) && empty($reference_type) && !empty($balance_records)) {
                    $data[$i] = array(
                        'End Balance',
                        '',
                        '',
                        $balance_records->bcl_en,
                        $balance_records->bcs_en,
                        $balance_records->crl_en,
                        $balance_records->crs_en,
                        $balance_records->lid_en,

                    );
                } else {

                    $data[$i] = array(
                        'Total',
                        '',
                        '',
                        $bcl,
                        $bcs,
                        $crl,
                        $crs,
                        $lid,
                    );
                }
            }

        }


        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($count),
            "recordsFiltered" => intval($count),
            "data" => $data
        ];
        return json_encode($json_data);
    }
}
