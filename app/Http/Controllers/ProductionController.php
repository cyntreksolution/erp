<?php

namespace App\Http\Controllers;

use App\EndProductBurdenGrade;
use App\EndProductInquiry;
use App\Exports\BurdenExport;
use App\LoadingData;
use App\LoadingDataRaw;
use App\LoadingDataVariant;
use App\LoadingHeader;
use App\MergeRequest;
use App\ReusableStock;
use App\SalesMergeBurdenGrade;
use App\SalesMergeBurdenGradeAdjust;
use App\SalesMergeCookingRequest;
use App\SalesMergeCookingRequestAdjust;
use App\SalesMergeEndProductBurdenHeader;
use App\SalesMergeHeader;
use App\SalesMergeEndProductBurden;
use App\SalesMergeSalesRequest;
use App\SalesMergeSemiDif;
use App\SalesReturnHeader;
use App\SemiBurdenIssueData;
use App\SemiInquiry;
use App\SemiIssueDataList;
use App\Traits\EndProductLog;
use App\Traits\SemiFinishLog;
use App\Variant;
use BurdenManager\Models\Burden;
use BurdenManager\Models\BurdenGrade;
use BurdenManager\Models\ReusableProducts;
use Carbon\Carbon;
use EmployeeManager\Models\Employee;
use EmployeeManager\Models\Group;
use EndProductBurdenManager\Models\BurdenPacking;
use EndProductBurdenManager\Models\BurdenRaw;
use EndProductBurdenManager\Models\BurdenSemi;
use EndProductBurdenManager\Models\EndProductBurden;
use EndProductBurdenManager\Models\EndProductBurdenHeader;
use EndProductBurdenManager\Models\TempBurdenPacking;
use EndProductBurdenManager\Models\TempBurdenRaw;
use EndProductBurdenManager\Models\TempBurdenSemi;
use EndProductManager\Models\Category;
use EndProductManager\Models\EndProduct;
use EndProductRecipeManager\Models\EndProductRecipe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use ProductionOrderManager\Models\ProductionOrder;
use ProductionOrderManager\Models\ProductionOrderEndProduct;
use ProductionOrderManager\Models\ProductionOrderSemiProduct;
use ProductionOrderManager\Models\ProductionRawMaterial;
use RecipeManager\Classes\RecipeContent;
use SalesHeadManager\Models\SalesHead;
use SalesOrderManager\Models\SalesOrder;
use SalesRepManager\Models\SalesRep;
use SalesRequestManager\Models\SalesRequest;
use SemiFinishProductManager\Models\RecipeSize;
use SemiFinishProductManager\Models\SemiFinishProduct;
use ShortEatsManager\Models\CookingRequest;
use StockManager\Classes\StockTransaction;
use function GuzzleHttp\Promise\all;

class ProductionController extends Controller
{
    use EndProductLog;
    use SemiFinishLog;

    function __construct()
    {
        $this->middleware('permission:production_manager-index', ['only' => ['index','salesRequestTableData']]);
        $this->middleware('permission:production_manager-show', ['only' => ['show']]);
        $this->middleware('Production-list', ['only' => ['list']]);
        $this->middleware('SalesRequests-list', ['only' => ['sales-list']]);
        $this->middleware('EndProductMerge-list', ['only' => ['merge-list']]);
        $this->middleware('permission:production_manager-mergedSalesRequests', ['only' => ['mergedSalesRequests']]);
        $this->middleware('permission:production_manager-mergedSalesRequestList', ['only' => ['mergedSalesRequestList']]);
        $this->middleware('permission:production_manager-endBurdenList', ['only' => ['endBurdenList']]);
        $this->middleware('permission:production_manager-editQty', ['only' => ['editQty']]);
        $this->middleware('permission:production_manager-epMergeList', ['only' => ['epMergeList']]);
        $this->middleware('permission:production_manager-srMergeList', ['only' => ['srMergeList']]);
        $this->middleware('permission:production_manager-issueSemi', ['only' => ['issueSemi']]);
        $this->middleware('permission:production_manager-semiIssue', ['only' => ['semiIssue']]);
        $this->middleware('permission:production_manager-differenceIssue', ['only' => ['differenceIssue']]);
        $this->middleware('permission:production_manager-salesLoadingList', ['only' => ['salesLoadingList']]);
        $this->middleware('permission:production_manager-mergedSalesHeaderDelete', ['only' => ['mergedSalesHeaderDelete']]);
    }
    public function index()
    {
        $categories = Category::where('loading_type', '=', 'to_burden')->get()->pluck('name', 'id');
        $agents = SalesRep::all()->pluck('email', 'id');
        return view('production.list', compact('categories', 'agents'));
    }

    public function salesRequestTableData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'created_at', 'order_date'];
        $order_column = $columns[$order_by[0]['column']];

        $salesOrders = SalesOrder::tableData($order_column, $order_by_str, $start, $length)->orderType()->whereConfirmed(1)->whereStatus(1);
        $salesOrdersCount = SalesOrder::whereStatus(1)->orderType()->whereConfirmed(1)->count();

        if ($request->filter == true) {
            $category = $request->category;
            $day = $request->day;
            $time = $request->time;
            $date = $request->date;
            $agent = $request->agent;
            $salesOrders = $salesOrders->filterData($category, $day, $time, $date, $agent)->get();
            $salesOrdersCount = $salesOrders->count();
        } elseif (is_null($search) || empty($search)) {
            $salesOrders = $salesOrders->get();
            $salesOrdersCount = SalesOrder::whereStatus(1)->orderType()->whereConfirmed(1)->count();
        } else {
            $salesOrders = $salesOrders->searchData($search)->get();
            $salesOrdersCount = $salesOrders->count();
        }

        $data[][] = array();
        $i = 0;

        $user = Auth::user();
        $can_show = ($user->can('production_manager-show')) ? 1 : 0;


        foreach ($salesOrders as $key => $salesOrder) {

            $approving_orders = SalesOrder::whereStatus(4)
                ->whereCreatedBy($salesOrder->created_by)
                ->whereHas('template', function ($q) use ($salesOrder) {
                    return $q->whereCategoryId($salesOrder->template->category_id);
                })->first();

            $app_btn_filter = null;
            if($salesOrder->is_approved != 1) {
                $app_btn_filter = "<button class='btn btn-sm btn-warning mr-2'>Approve Manager </button>";
            }

            $approving_filter = !empty($approving_orders) ? 1 : 0;

            $btn_filter = ($approving_filter) ? "<button class='btn btn-sm btn-danger mr-1'> Due Approval </button>" : null;

            $rep = $salesOrder->salesRep->type . ' ' . $salesOrder->salesRep->name_with_initials;
            if ($can_show) {
                $btnShow = '<a href="' . route('sr.show', $salesOrder->id) . '"  target="_blank" class="btn btn-sm btn-outline-dark mr-1"><i class="fa fa-eye"></i> </a>' ;
            }else{
                $btnShow= [];
            }
            $btnMerge = '<button class="btn btn-sm btn-outline-dark mr-1"><i class="fa fa-hand-lizard-o"></i> </button>';
            $data[$i] = array(
                ($request->filter == true && !empty($request->category) && !$approving_filter && $salesOrder->is_approved == 1) ? "<input type='checkbox' name='order[]' value='$salesOrder->id'/>" : null,
                $salesOrder->id,
                Carbon::parse($salesOrder->created_at)->format('d-m-Y H:m'),
                Carbon::parse($salesOrder->order_date)->format('d-m-Y'),
                $salesOrder->template->category->name,
                $salesOrder->template->day,
                $salesOrder->template->time,
                $rep,
                $salesOrder->salesRep->territory,
                $btn_filter.$app_btn_filter,
                $btnShow,
            );
            $i++;
        }

        if ($salesOrdersCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($salesOrdersCount),
            "recordsFiltered" => intval($salesOrdersCount),
            "data" => $data
        ];

        return json_encode($json_data);
    }

    public function mergeSalesRequests(Request $request)
    {
        $orders = $request->order;
        $sales_orders = $orders;
        if (empty($orders)) {
            return redirect(route('sr.index'))->with([
                'error' => true,
                'error.title' => 'Sorry !',
                'error.message' => 'No Sales Request Selected.'
            ]);
        }

        $order_s = SalesOrder::whereIn('id', $orders)->with('salesOrderContent')->get();

        $category = null;
        foreach ($order_s as $order_) {
            if ($order_->status != 1) {
                return "Your request " . $order_->id . " has been Merged";
            }
            $category = $order_->template->category;
        }


        $condition = $request->submit == 'summary';

        if ($condition) {
            $table_data = [];
            $table_data_variant = [];

            foreach ($order_s as $order) {
                $order_item = [];
                $order_item['agent_name'] = $order->salesRep->name_with_initials;
                $order_item['name'] = [];
                $order_item['end_product_id'] = [];
                $order_item['qty'] = [];

                $order_item_variant = [];
                $order_item_variant['agent_name'] = $order->salesRep->name_with_initials;
                $order_item_variant['name'] = [];
                $order_item_variant['variant_id'] = [];
                $order_item_variant['end_product_id'] = [];
                $order_item_variant['qty'] = [];

                foreach ($order->salesOrderContent as $item) {
                    if ($order->id == $item->sales_order_header_id) {
                        array_push($order_item['name'], EndProduct::whereId($item->end_product_end_product_id)->first()->name);
                        array_push($order_item['end_product_id'], $item->end_product_end_product_id);
                        $qty = ($item->extra_qty + $item->qty) - $item->available_qty;
                        array_push($order_item['qty'], $qty);
                    }
                }

                foreach ($order->salesOrderVariants as $item) {
                    if ($order->id == $item->sales_order_header_id) {
                        $variant = Variant::whereId($item->variant_id)->first();
                        array_push($order_item_variant['name'], $variant->product->name . ' ' . $variant->variant);
                        $qty = ($item->extra_qty + $item->qty) - $item->available_qty;
                        array_push($order_item_variant['variant_id'], $item->variant_id);
                        array_push($order_item_variant['qty'], $qty);
                        array_push($order_item_variant['end_product_id'], $variant->end_product_id);
                    }
                }

                array_push($table_data, $order_item);
                array_push($table_data_variant, $order_item_variant);
            }


            $name_list = [];
            $name_list['end_product'] = [];
            $name_list ['variant'] = [];
            $table_headers = [];
            array_push($table_headers, 'End Products');

            foreach ($table_data as $key => $agent) {
                array_push($table_headers, $agent['agent_name']);
                foreach ($agent['name'] as $key1 => $name) {
                    if (!in_array($name, $name_list['end_product'])) {
                        array_push($name_list['end_product'], $name);
                    }
                }
            }

            foreach ($table_data_variant as $key => $agent) {
                foreach ($agent['name'] as $key1 => $name) {
                    if (!in_array($name, $name_list['variant'])) {
                        array_push($name_list['variant'], $name);
                    }
                }
            }


            array_push($table_headers, 'Total');

            $table_rows = [];
            $table_rows['end_product'] = [];
            $table_rows['variant'] = [];

            foreach ($name_list['end_product'] as $key => $item) {
                $row = [];
                $name = $item;
                array_push($row, $name);
                foreach ($table_data as $table_datum) {
                    $qty = (isset($table_datum['qty'][$key])) ? $table_datum['qty'][$key] : 0;
                    array_push($row, $qty);
                }
                array_push($table_rows['end_product'], $row);
            }

            foreach ($name_list['variant'] as $key => $item) {
                $row = [];
                $name = $item;
                array_push($row, $name);
                foreach ($table_data_variant as $table_datum) {
                    $qty = (isset($table_datum['qty'][$key])) ? $table_datum['qty'][$key] : 0;
                    array_push($row, $qty);
                }
                array_push($table_rows['variant'], $row);
            }

            $mr = null;
            $serial = 'MR' . Carbon::now()->format('YmdHis') . rand(1000, 9999);
            foreach ($table_rows['end_product'] as $items) {
                $count = 0;
                $endProduct = EndProduct::where('name', '=', $items[0])->first();

                foreach ($items as $item) {
                    $qty = (is_numeric($item)) ? $item : 0;
                    $count = $count + $qty;
                }

                if ($count > 0) {
                    $mr = new MergeRequest();
                    $mr->end_product_id = $endProduct->id;
                    $mr->qty = $count;
                    $mr->serial = $serial;
                    $mr->sales_request_data = json_encode($sales_orders);
                    $mr->save();
                }

            }

            return view('production.sr-merge-print', compact('category', 'table_rows', 'table_headers', 'orders', 'mr'));

        }

        $products = DB::table('sales_order_header')
            ->select('end_product.name as end_product_name', 'end_product.image as end_product_image', 'end_product.image_path as end_product_path',
                'end_product_end_product_id', 'end_product.available_qty As end_product_available_qty',
                'end_product.buffer_stock As end_product_buffer_stock', 'semi_finish_product_semi_finish_product_id As semi_finish_product',
                'semi_finish_product.name as semi_name', 'semi_finish_product.image as semi_image', 'semi_finish_product.image_path as semi_path',
                'end_product_semi_finish_product.qty AS semi_qty', 'semi_finish_product.available_qty As semi_finish_availbel_qty',
                'semi_finish_product.buffer_stock as semi_finish_buffer_stock', 'sales_order_data.available_qty as agent_qty',
                DB::raw('SUM(sales_order_data.qty) as total'))
            ->leftJoin('sales_order_data', 'sales_order_header.id', '=', 'sales_order_header_id')
            ->leftJoin('end_product', 'sales_order_data.end_product_end_product_id', '=', 'end_product.id')
            ->leftJoin('end_product_semi_finish_product', 'end_product.id', '=', 'end_product_semi_finish_product.end_product_id')
            ->leftJoin('semi_finish_product', 'end_product_semi_finish_product.semi_finish_product_semi_finish_product_id', '=', 'semi_finish_product.semi_finish_product_id')
            ->where('sales_order_header.status', '=', 1)
            ->groupBy('end_product_end_product_id')
            ->havingRaw('COUNT(sales_order_data.id) > 0')
            ->get();


        $semiproducts = DB::table('sales_order_header')
            ->select('semi_finish_product_semi_finish_product_id As semi_finish_product',
                'semi_finish_product.name as semi_name', 'semi_finish_product.image as semi_image', 'semi_finish_product.image_path as semi_path',
                'end_product_semi_finish_product.qty AS semi_qty', 'semi_finish_product.available_qty As semi_finish_availbel_qty',
                'semi_finish_product.buffer_stock as semi_finish_buffer_stock',
                DB::raw('(SUM(sales_order_data.qty)-SUM(sales_order_data.available_qty))* end_product_semi_finish_product.qty as total'))
            ->leftJoin('sales_order_data', 'sales_order_header.id', '=', 'sales_order_header_id')
            ->leftJoin('end_product', 'sales_order_data.end_product_end_product_id', '=', 'end_product.id')
            ->leftJoin('end_product_semi_finish_product', 'end_product.id', '=', 'end_product_semi_finish_product.end_product_id')
            ->leftJoin('semi_finish_product', 'end_product_semi_finish_product.semi_finish_product_semi_finish_product_id', '=', 'semi_finish_product.semi_finish_product_id')
            ->where('sales_order_header.status', '=', 1)
            ->groupBy('end_product_semi_finish_product.semi_finish_product_semi_finish_product_id')
            ->havingRaw('COUNT(sales_order_data.id) > 0')
            ->get();


        $rawMaterials = DB::table('sales_order_header')
            ->select(DB::raw('SUM(sales_order_raw_data.qty) as total'), 'raw_material.raw_material_id')
            ->leftJoin('sales_order_raw_data', 'sales_order_header.id', '=', 'sales_order_header_id')
            ->leftJoin('raw_material', 'sales_order_raw_data.raw_material_id', '=', 'raw_material.raw_material_id')
            ->where('sales_order_header.status', '=', 1)
            ->groupBy('raw_material.raw_material_id')
            ->havingRaw('COUNT(sales_order_raw_data.id) > 0')
            ->get();


        DB::transaction(function () use ($products, $semiproducts, $rawMaterials, $orders) {
            $po = new ProductionOrder();
            $po->created_by = Auth::user()->id;
            $po->save();

            foreach ($products as $product) {
                $poe = new ProductionOrderEndProduct();
                $poe->production_order_id = $po->id;
                $poe->end_product_id = $product->end_product_end_product_id;
                $poe->semi_product_id = $product->semi_finish_product;
                $poe->semi_qty = $product->semi_qty;
                $poe->total = $product->total - $product->agent_qty;
                $poe->save();
            }

            foreach ($semiproducts as $semiproduct) {
                $pos = new ProductionOrderSemiProduct();
                $pos->production_order_id = $po->id;
                $pos->semi_product_id = $semiproduct->semi_finish_product;
                $pos->semi_qty = $semiproduct->semi_qty;
                $pos->semi_total = $semiproduct->total;
                $pos->save();
            }


            foreach ($rawMaterials as $raw) {
                $pos = new ProductionRawMaterial();
                $pos->production_order_id = $po->id;
                $pos->raw_material_id = $raw->raw_material_id;
                $pos->qty = $raw->total;
                $pos->save();
            }

            foreach ($orders as $order) {
                $orde = SalesOrder::find($order);
                $orde->status = 2;
                $orde->save();

                $loadingHeader = new LoadingHeader();
                $loadingHeader->so_id = $orde->id;
                $loadingHeader->invoice_number = 'SO-' . Carbon::now()->format('ymd') . $orde->id;;
                $loadingHeader->save();

                $total = 0;
                foreach ($orde->salesOrderContent as $item) {
                    $loadingData = new LoadingData();
                    $loadingData->loading_header_id = $loadingHeader->id;
                    $loadingData->end_product_id = $item->end_product_end_product_id;
                    $loadingData->qty = $item->qty;
                    $loadingData->extra_qty = $item->extra_qty;
                    $loadingData->agent_available_qty = $item->available_qty;
                    $loadingData->agent_approved_qty = 0;
                    $loadingData->admin_approved_qty = 0;
                    $loadingData->unit_value = EndProduct::whereId($item->end_product_end_product_id)->first()->distributor_price;
                    $loadingData->status = 0;
                    $loadingData->is_extra_order = $item->is_extra_order;
                    $loadingData->save();

                    $total = $total + ($loadingData->unit_value * $loadingData->qty);
                }

                $loadingHeader->total = $total;
                $loadingHeader->save();


                if (!empty($orde->salesOrderRawContent)) {
                    foreach ($orde->salesOrderRawContent as $item) {
                        $loadingData = new LoadingDataRaw();
                        $loadingData->loading_header_id = $loadingHeader->id;
                        $loadingData->raw_material_id = $item->raw_material_id;
                        $loadingData->qty = $item->qty;
                        $loadingData->agent_approved_qty = 0;
                        $loadingData->admin_approved_qty = 0;
                        $loadingData->status = 0;
                        $loadingData->save();
                    }
                }


            }

        });

//        return 'true';
        return view('production.create');

    }

    public function salesRequestsToBurden(Request $request)
    {
        $orders = explode(',', $request->order);
        $order_s = SalesOrder::whereIn('id', $orders)->with('salesOrderContent')->get();

        foreach ($order_s as $order_) {
            if ($order_->status != 1) {
                return "Your request " . $order_->id . " has been Merged";
            }
        }
        $serial = $request->merge_serial;

        $sales_merge_header = new SalesMergeHeader();
        $sales_merge_header->serial = 'SMR-' . Carbon::now()->format('ymdHis');
        $sales_merge_header->status = 'merged';
        $sales_merge_header->created_by = Auth::user()->id;
        $sales_merge_header->merge_request_serial = $serial;
        $sales_merge_header->save();

        $sales_merge_header->salesRequests()->attach($order_s);


        $mr = MergeRequest::whereSerial($serial)->get();

        foreach ($mr as $product) {
            $sales_merge_header->mergeRequests()->attach($product);
            $endProduct = EndProduct::find($product->end_product_id);
            $active_recipe = EndProductRecipe::whereEndProductId($product->end_product_id)->whereStatus(1)->first();

            if (empty($active_recipe)) {
                return 'No Active Recipe Found for ' . $endProduct->name;
            }

//            $burdenCount = EndProductBurden::whereDate('created_at', '=', Carbon::today()->toDateString())->get()->count();
//            $last_digit = $burdenCount + 1;
//            $serial = date('ymd') . '-' . $last_digit;
//
//            $burden = new EndProductBurden();
//            $burden->end_product_id = $product->end_product_id;
//            $burden->serial = $serial;
//            $burden->burden_size = $product->qty;
//            $burden->expected_end_product_qty = $product->qty;
//            $burden->recipe_id = $active_recipe->id;
//            $burden->merge_request_id = $product->id;
//            $burden->created_by = \Cartalyst\Sentinel\Laravel\Facades\Sentinel::getUser()->id;
//            $burden->save();
//
//            $sales_merge_header->endProductBurdens()->attach($burden);

            $mr = MergeRequest::find($product->id);
            $mr->status = 2;
            $mr->save();
        }


        $products = DB::table('sales_order_header')
            ->select('end_product.name as end_product_name', 'end_product.image as end_product_image', 'end_product.image_path as end_product_path',
                'end_product_end_product_id', 'end_product.available_qty As end_product_available_qty',
                'end_product.buffer_stock As end_product_buffer_stock', 'semi_finish_product_semi_finish_product_id As semi_finish_product',
                'semi_finish_product.name as semi_name', 'semi_finish_product.image as semi_image', 'semi_finish_product.image_path as semi_path',
                'end_product_semi_finish_product.qty AS semi_qty', 'semi_finish_product.available_qty As semi_finish_availbel_qty',
                'semi_finish_product.buffer_stock as semi_finish_buffer_stock', 'sales_order_data.available_qty as agent_qty',
                DB::raw('SUM(sales_order_data.qty) as total'))
            ->leftJoin('sales_order_data', 'sales_order_header.id', '=', 'sales_order_header_id')
            ->leftJoin('end_product', 'sales_order_data.end_product_end_product_id', '=', 'end_product.id')
            ->leftJoin('end_product_semi_finish_product', 'end_product.id', '=', 'end_product_semi_finish_product.end_product_id')
            ->leftJoin('semi_finish_product', 'end_product_semi_finish_product.semi_finish_product_semi_finish_product_id', '=', 'semi_finish_product.semi_finish_product_id')
            ->where('sales_order_header.status', '=', 1)
            ->groupBy('end_product_end_product_id')
            ->havingRaw('COUNT(sales_order_data.id) > 0')
            ->get();


        $semiproducts = DB::table('sales_order_header')
            ->select('semi_finish_product_semi_finish_product_id As semi_finish_product',
                'semi_finish_product.name as semi_name', 'semi_finish_product.image as semi_image', 'semi_finish_product.image_path as semi_path',
                'end_product_semi_finish_product.qty AS semi_qty', 'semi_finish_product.available_qty As semi_finish_availbel_qty',
                'semi_finish_product.buffer_stock as semi_finish_buffer_stock',
                DB::raw('(SUM(sales_order_data.qty)-SUM(sales_order_data.available_qty))* end_product_semi_finish_product.qty as total'))
            ->leftJoin('sales_order_data', 'sales_order_header.id', '=', 'sales_order_header_id')
            ->leftJoin('end_product', 'sales_order_data.end_product_end_product_id', '=', 'end_product.id')
            ->leftJoin('end_product_semi_finish_product', 'end_product.id', '=', 'end_product_semi_finish_product.end_product_id')
            ->leftJoin('semi_finish_product', 'end_product_semi_finish_product.semi_finish_product_semi_finish_product_id', '=', 'semi_finish_product.semi_finish_product_id')
            ->where('sales_order_header.status', '=', 1)
            ->groupBy('end_product_semi_finish_product.semi_finish_product_semi_finish_product_id')
            ->havingRaw('COUNT(sales_order_data.id) > 0')
            ->get();


        $rawMaterials = DB::table('sales_order_header')
            ->select(DB::raw('SUM(sales_order_raw_data.qty) as total'), 'raw_material.raw_material_id')
            ->leftJoin('sales_order_raw_data', 'sales_order_header.id', '=', 'sales_order_header_id')
            ->leftJoin('raw_material', 'sales_order_raw_data.raw_material_id', '=', 'raw_material.raw_material_id')
            ->where('sales_order_header.status', '=', 1)
            ->groupBy('raw_material.raw_material_id')
            ->havingRaw('COUNT(sales_order_raw_data.id) > 0')
            ->get();


        DB::transaction(function () use ($products, $semiproducts, $rawMaterials, $orders) {
            $po = new ProductionOrder();
            $po->created_by = Auth::user()->id;
            $po->save();

            foreach ($products as $product) {
                $poe = new ProductionOrderEndProduct();
                $poe->production_order_id = $po->id;
                $poe->end_product_id = $product->end_product_end_product_id;
                $poe->semi_product_id = $product->semi_finish_product;
                $poe->semi_qty = $product->semi_qty;
                $poe->total = $product->total - $product->agent_qty;
                $poe->save();
            }

            foreach ($semiproducts as $semiproduct) {
                $pos = new ProductionOrderSemiProduct();
                $pos->production_order_id = $po->id;
                $pos->semi_product_id = $semiproduct->semi_finish_product;
                $pos->semi_qty = $semiproduct->semi_qty;
                $pos->semi_total = $semiproduct->total;
                $pos->save();
            }


            foreach ($rawMaterials as $raw) {
                $pos = new ProductionRawMaterial();
                $pos->production_order_id = $po->id;
                $pos->raw_material_id = $raw->raw_material_id;
                $pos->qty = $raw->total;
                $pos->save();
            }

            foreach ($orders as $order) {
                $orde = SalesOrder::find($order);
                $orde->status = 2;
                $orde->save();

                $loadingHeader = new LoadingHeader();
                $loadingHeader->so_id = $orde->id;
                $loadingHeader->invoice_number = 'SO-' . Carbon::now()->format('ymd') . $orde->id;;
                $loadingHeader->save();

                $total = 0;
                foreach ($orde->salesOrderContent as $item) {
                    $loadingData = new LoadingData();
                    $loadingData->loading_header_id = $loadingHeader->id;
                    $loadingData->end_product_id = $item->end_product_end_product_id;
                    $loadingData->qty = $item->qty;
                    $loadingData->requested_qty = $item->qty;
                    $loadingData->extra_qty = $item->extra_qty;
                    $loadingData->agent_available_qty = $item->available_qty;
                    $loadingData->agent_approved_qty = 0;
                    $loadingData->admin_approved_qty = 0;
                    $loadingData->unit_value = EndProduct::whereId($item->end_product_end_product_id)->first()->distributor_price;
                    $loadingData->status = 0;
                    $loadingData->is_extra_order = $item->is_extra_order;
                    $loadingData->save();

                    $total = $total + ($loadingData->unit_value * $loadingData->qty);
                }

                $loadingHeader->total = $total;
                $loadingHeader->save();


                if (!empty($orde->salesOrderRawContent)) {
                    foreach ($orde->salesOrderRawContent as $item) {
                        $loadingData = new LoadingDataRaw();
                        $loadingData->loading_header_id = $loadingHeader->id;
                        $loadingData->raw_material_id = $item->raw_material_id;
                        $loadingData->qty = $item->qty;
                        $loadingData->agent_approved_qty = 0;
                        $loadingData->admin_approved_qty = 0;
                        $loadingData->status = 0;
                        $loadingData->save();
                    }
                }


            }

        });

        return redirect(route('production.merged'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => 'new burdens created successfully'
        ]);

    }

    public function mergedSalesRequests(Request $request)
    {
        $merged = SalesMergeHeader::whereStatus('merged')->get();
        return view('production.merged-list', compact('merged'));
    }

    public function endBurdenList(Request $request)
    {
        $merge_ids = array_keys($request->merge_ids);
        $mr_serials = SalesMergeHeader::whereIn('id', $merge_ids)->pluck('merge_request_serial')->toArray();


        $mr = MergeRequest::select(DB::raw('sum(qty) as qty'), 'end_product_id', 'id', 'burden_created')->whereIn('serial', $mr_serials)->groupBy('end_product_id')->get();

        $mr_ungrouped = MergeRequest::whereIn('serial', $mr_serials)->get();
//        $mr_ungrouped = MergeRequest::whereIn('serial', $mr_serials)->where('burden_created', '=', 0)->get();
//ttt


        foreach ($mr as $product) {
            if ($product->burden_created == 0) {
                $mr = MergeRequest::find($product->id);
                $mr->burden_created = 1;
                $mr->save();
//                $sales_merge_header->mergeRequests()->attach($product);
                $endProduct = EndProduct::find($product->end_product_id);
                $active_recipe = EndProductRecipe::whereEndProductId($product->end_product_id)->whereStatus(1)->first();

                if (empty($active_recipe)) {
                    return 'No Active Recipe Found for ' . $endProduct->name;
                }


                $burdenCount = EndProductBurden::whereDate('created_at', '=', Carbon::today()->toDateString())->get()->count();
                $last_digit = $burdenCount + 1;
                $serial = date('ymd') . '-' . $last_digit;

                $burden = new EndProductBurden();
                $burden->end_product_id = $product->end_product_id;
                $burden->serial = $serial;
                $burden->burden_size = $product->qty;
                $burden->expected_end_product_qty = $product->qty;
                // $burden->actual_end_product_qty = $product->qty;
                $burden->recipe_id = $active_recipe->id;
                $burden->merge_request_id = $product->id;
                $burden->created_by = Auth::user()->id;
                $burden->save();


                $dat = new SalesMergeEndProductBurden();
                $dat->sales_merge_header_id = $merge_ids[0];
                $dat->end_product_burden_id = $burden->id;
                $dat->save();


            }
        }

        foreach ($mr_ungrouped as $product) {
            $product->burden_created = 1;
            $product->save();
        }
        //===================================================================================================

        $merge_burdens = SalesMergeEndProductBurden::with('endProductBurden')->whereIn('sales_merge_header_id', $merge_ids)->get();

        return view('production.end-merge-list', compact('merge_burdens', 'merge_ids'));
    }

    public function mergeBurdens(Request $request, $burden_id = null)
    {
        /*$dataRawProductPoints = [];
        $dataSemiProductPoints = [];
        $dataPackingProductPoints = [];
        $raw_materials_total = 0;
        $semi_finish_products_total = 0;
        $packing_total = 0;*/

        $sales_merge_header = $request->sales_merge_header_id;

        $burdens = (empty($burden_id)) ? $request->burden_id : $burden_id;
        $endProductIdAll = [];
        $showRawTypes = [];
        $showSemiTypes = [];
        $showPackingTypes = [];
        $serial = 'EM-' . date('ymd') . '-' . random_int(10000, 99999);
        $serialCodeRegister = 0;

        foreach ($burdens as $burden) {
            $bd = EndProductBurden::find($burden);
            if ($bd->status == 2) {
                return "Burden Already Merged";
            }
        }


        if (isset($request->saveButton) || !empty($burden_id)) {
            $serialCodeRegister = new EndProductBurdenHeader();
            $serialCodeRegister->serial = $serial;
            $serialCodeRegister->merge_status = 'created';
            $serialCodeRegister->created_by = Auth::user()->id;
            $serialCodeRegister->burdens = json_encode($burdens);
            if (!empty($burden_id)) {
                $serialCodeRegister->issue_semi = 1;
            }


            $saleM = SalesMergeHeader::whereIn('id', json_decode($sales_merge_header))->get();
            $mr_serials = $saleM->pluck('merge_request_serial');
            foreach ($saleM as $m) {
                $m->status = 'end_product_merge';
                $m->save();
            }


            $serialCodeRegister->save();
            $serialNo = $serialCodeRegister->id;


            foreach (json_decode($sales_merge_header) as $merge) {
                $dat = new SalesMergeEndProductBurdenHeader();
                $dat->sales_merge_header_id = $merge;
                $dat->end_product_burden_header_id = $serialCodeRegister->id;
                $dat->save();
            }


        }


        foreach ($burdens as $burden) {
            $bd = EndProductBurden::find($burden);
            if ($bd->status == 2) {
                return "Burden Already Merged";
            }


            $unitsCount = $bd->burden_size;
            $endProductId = $bd->end_product_id;
            $endProductIdAll[] = $endProductId;
            $raws = $bd->recipe->rawContent()->get();
            $semis = $bd->recipe->semiContent()->get();
            $packing = $bd->recipe->packingContent()->get();
            foreach ($raws as $item) {
                $showRawTypes[] = $item->rawMaterial->name;
            }
            foreach ($semis as $item) {
                $showSemiTypes[] = $item->semiFinishProduct->name;
            }
            foreach ($packing as $item) {
                $showPackingTypes[] = $item->packingItem->name;
            }

            $category = 0;
            if (isset($request->saveButton) || !empty($burden_id)) {
//                DB::transaction(function () use ($raws, $semis, $packing, $unitsCount, $serialNo, $endProductId, $bd) {
                $bd = EndProductBurden::find($bd->id);
                $bd->status = 2;
                $bd->save();

                $category = $bd->endProduct->category_id;
                foreach ($raws as $item) {
                    BurdenRaw::create([
                        'end_product_id' => $endProductId,
                        'end_product_burden_id' => $bd->id,
                        'merge_id' => $serialNo,
                        'raw_material_id' => $item->raw_material_id,
                        'raw_material_name' => $item->rawMaterial->name,
                        'qty' => ($item->units * $unitsCount
                        )]);

                }
                foreach ($semis as $item) {
                    $semi = BurdenSemi::create([
                        'end_product_id' => $endProductId,
                        'end_product_burden_id' => $bd->id,
                        'merge_id' => $serialNo,
                        'semi_finish_product_id' => $item->semi_finish_product_id,
                        'semi_finish_product_name' => $item->semiFinishProduct->name,
                        'qty' => ($item->units * $unitsCount)
                    ]);


                }
                foreach ($packing as $item) {
                    BurdenPacking::create([
                        'end_product_id' => $endProductId,
                        'merge_id' => $serialNo,
                        'raw_material_id' => $item->packing_id,
                        'raw_material_name' => $item->packingItem->name,
                        'qty' => ($item->units * $unitsCount
                        )]);
                }
//                });
            }

            if (isset($request->saveButton) || !empty($burden_id)) {
                $serialCodeRegister = EndProductBurdenHeader::find($serialNo);
                $serialCodeRegister->category = $category;
                $serialCodeRegister->save();
            }
            if (isset($request->showButton)) {
                DB::transaction(function () use ($raws, $semis, $packing, $unitsCount, $serial, $endProductId) {
                    foreach ($raws as $item) {
                        TempBurdenRaw::create([
                            'end_product_id' => $endProductId,
                            'merge_id' => $serial,
                            'raw_material_id' => $item->raw_material_id,
                            'raw_material_name' => $item->rawMaterial->name,
                            'qty' => ($item->units * $unitsCount
                            )]);

                    }
                    foreach ($semis as $item) {
                        TempBurdenSemi::create([
                            'end_product_id' => $endProductId,
                            'merge_id' => $serial,
                            'semi_finish_product_id' => $item->semi_finish_product_id,
                            'semi_finish_product_name' => $item->semiFinishProduct->name,
                            'qty' => ($item->units * $unitsCount
                            )]);
                    }
                    foreach ($packing as $item) {
                        TempBurdenPacking::create([
                            'end_product_id' => $endProductId,
                            'merge_id' => $serial,
                            'packing_id' => $item->packing_id,
                            'packing_name' => $item->packingItem->name,
                            'qty' => ($item->units * $unitsCount
                            )]);
                    }
                });
            }

        }
        $showRawTypeUnique[] = array_unique($showRawTypes);
        $showSemiTypeUnique[] = array_unique($showSemiTypes);
        $showPackingTypeUnique[] = array_unique($showPackingTypes);
        // $serial = 'EM-200204-11921';
        $endProductIdAll = array_unique($endProductIdAll);
        foreach ($endProductIdAll as $endProductId) {
            $endProduct = EndProduct::whereId($endProductId)->first();
            $endProductName[] = $endProduct->name;
        }
        if (isset($request->showButton)) {
            $raw_materials_total = TempBurdenRaw::whereMergeId($serial)
                ->selectRaw(' COALESCE(sum(qty), 0) as qty, raw_material_id,raw_material_name')
                ->groupBy('raw_material_id')
                ->get();
            $count = 0;
            $dataRawProductPoints = array();

            foreach ($endProductIdAll as $endProductId) {
                $dataPoints = array();
                foreach ($showRawTypeUnique[0] as $showRawType) {
                    $raw_material = TempBurdenRaw::selectRaw(' COALESCE(sum(qty), 0) as qty, raw_material_id,raw_material_name')
                        ->whereEndProductId($endProductId)
                        ->whereRawMaterialName($showRawType)
                        ->whereMergeId($serial)
                        ->first();
                    if ($raw_material == Null) {
                        $dataPoints[] = array("type" => $showRawType, "qty" => 0);
                    } else {
                        $dataPoints[] = array("type" => $showRawType, "qty" => $raw_material->qty);
                    }
                }

                $dataRawProductPoints[] = array("end_product_name" => $endProductName[$count], "data" => $dataPoints);
                $count++;
            }

            $semi_finish_products_total = TempBurdenSemi::whereMergeId($serial)
                ->selectRaw(' COALESCE(sum(qty), 0) as qty, semi_finish_product_id,semi_finish_product_name')
                ->groupBy('semi_finish_product_id')
                ->get();
            $count = 0;
            $dataSemiProductPoints = array();

            foreach ($endProductIdAll as $endProductId) {
                $dataPoints = array();
                foreach ($showSemiTypeUnique[0] as $showSemiType) {
                    $semi_finish_product = TempBurdenSemi::selectRaw(' COALESCE(sum(qty), 0) as qty, semi_finish_product_id,semi_finish_product_name')
                        ->whereEndProductId($endProductId)
                        ->whereSemiFinishProductName($showSemiType)
                        ->whereMergeId($serial)
                        ->first();
                    if ($semi_finish_product == Null) {
                        $dataPoints[] = array("type" => $showSemiType, "qty" => 0);
                    } else {
                        $dataPoints[] = array("type" => $showSemiType, "qty" => $semi_finish_product->qty);
                    }
                }

                $dataSemiProductPoints[] = array("end_product_name" => $endProductName[$count], "data" => $dataPoints);
                $count++;
            }


            $packing_total = TempBurdenPacking::whereMergeId($serial)
                ->selectRaw(' COALESCE(sum(qty), 0) as qty, packing_id,packing_name')
                ->groupBy('packing_id')
                ->get();

            $count = 0;
            $dataPackingProductPoints = array();
            foreach ($endProductIdAll as $endProductId) {
                $dataPoints = array();
                foreach ($showPackingTypeUnique[0] as $showPackingType) {
                    $packing_product = TempBurdenPacking::selectRaw(' COALESCE(sum(qty), 0) as qty, packing_id,packing_name')
                        ->whereEndProductId($endProductId)
                        ->wherePackingName($showPackingType)
                        ->whereMergeId($serial)
                        ->first();
                    if ($packing_product == Null) {
                        $dataPoints[] = array("type" => $showPackingType, "qty" => 0);
                    } else {
                        $dataPoints[] = array("type" => $showPackingType, "qty" => $packing_product->qty);
                    }
                }

                $dataPackingProductPoints[] = array("end_product_name" => $endProductName[$count], "data" => $dataPoints);
                $count++;
            }
        }

        if (isset($request->saveButton) || !empty($burden_id)) {
            $raw_materials_total = BurdenRaw::whereMergeId($serialNo)
                ->selectRaw(' COALESCE(sum(qty), 0) as qty, raw_material_id,raw_material_name')
                ->groupBy('raw_material_id')
                ->get();
            $count = 0;
            $dataRawProductPoints = array();
            foreach ($endProductIdAll as $endProductId) {
                $dataPoints = array();
                foreach ($showRawTypeUnique[0] as $showRawType) {
                    $raw_material = BurdenRaw::selectRaw(' COALESCE(sum(qty), 0) as qty, raw_material_id,raw_material_name')
                        ->whereEndProductId($endProductId)
                        ->whereRawMaterialName($showRawType)
                        ->whereMergeId($serialNo)
                        ->first();
                    if ($raw_material == Null) {
                        $dataPoints[] = array("type" => $showRawType, "qty" => 0);
                    } else {
                        $dataPoints[] = array("type" => $showRawType, "qty" => $raw_material->qty);
                    }
                }

                $dataRawProductPoints[] = array("end_product_name" => $endProductName[$count], "data" => $dataPoints);
                $count++;
            }

            $semi_finish_products_total = BurdenSemi::whereMergeId($serialNo)
                ->selectRaw(' COALESCE(sum(qty), 0) as qty, semi_finish_product_id,semi_finish_product_name')
                ->groupBy('semi_finish_product_id')
                ->get();

            $count = 0;
            $dataSemiProductPoints = array();

            foreach ($endProductIdAll as $endProductId) {
                $dataPoints = array();
                foreach ($showSemiTypeUnique[0] as $showSemiType) {
                    $semi_finish_product = BurdenSemi::selectRaw(' COALESCE(sum(qty), 0) as qty, semi_finish_product_id,semi_finish_product_name')
                        ->whereEndProductId($endProductId)
                        ->whereSemiFinishProductName($showSemiType)
                        ->whereMergeId($serialNo)
                        ->first();
                    if ($semi_finish_product == Null) {
                        $dataPoints[] = array("type" => $showSemiType, "qty" => 0);
                    } else {
                        $dataPoints[] = array("type" => $showSemiType, "qty" => $semi_finish_product->qty);
                    }
                }

                $dataSemiProductPoints[] = array("end_product_name" => $endProductName[$count], "data" => $dataPoints);
                $count++;
            }

            $packing_total = BurdenPacking::whereMergeId($serialNo)
                ->selectRaw(' COALESCE(sum(qty), 0) as qty, raw_material_id,raw_material_name')
                ->groupBy('raw_material_id')
                ->get();

            $count = 0;
            $dataPackingProductPoints = array();
            foreach ($endProductIdAll as $endProductId) {
                $dataPoints = array();
                foreach ($showPackingTypeUnique[0] as $showPackingType) {
                    $packing_product = BurdenPacking::selectRaw(' COALESCE( COALESCE(sum(qty), 0), 0) as qty, raw_material_id,raw_material_name')
                        ->whereEndProductId($endProductId)
                        ->whereRawMaterialName($showPackingType)
                        ->whereMergeId($serialNo)
                        ->first();
                    if ($packing_product == Null) {
                        $dataPoints[] = array("type" => $showPackingType, "qty" => 0);
                    } else {
                        $dataPoints[] = array("type" => $showPackingType, "qty" => $packing_product->qty);
                    }
                }

                $dataPackingProductPoints[] = array("end_product_name" => $endProductName[$count], "data" => $dataPoints);
                $count++;
            }
            return redirect()->route('issue.semi', ['id' => $serialNo]);
        }


        return view('EndProductBurdenManager::burden.merge-print', compact('dataRawProductPoints', 'dataSemiProductPoints', 'dataPackingProductPoints', 'endProductIdAll', 'showRawTypeUnique', 'showSemiTypeUnique', 'showPackingTypeUnique', 'raw_materials_total', 'semi_finish_products_total', 'packing_total'));

    }

    public function issueSemi(Request $request)
    {
//        $sales_head = SalesMergeEndProductBurdenHeader::where('end_product_burden_header_id','=',$request->id)->first();
//        $sh = $sales_head->sales_merge_header_id;


        $endProductBurdenHeaderId = $request->id;
        $burden = EndProductBurdenHeader::find($endProductBurdenHeaderId);
        $items = BurdenSemi::selectRaw(' COALESCE(sum(qty), 0) as qty, semi_finish_product_id,semi_finish_product_name,end_product_id')
            ->whereMergeId($burden->id)
            ->groupBy('semi_finish_product_id')
            ->get();

        $data = [];
        $data['items'] = [];
        $data['raw_issue'] = $burden->issue_raw;
        $data['serial'] = $burden->serial;
        $data['burden_id'] = $burden->id;

        foreach ($items as $raw) {
            $loading_type = $raw->endProduct->category->loading_type;
            $mat = SemiFinishProduct::whereSemiFinishProductId($raw->semi_finish_product_id)->first();
            $available_qty = $mat->available_qty;

            $is_bake = $mat->is_bake;

            $requested_qty = null;
            $burden_size = null;
            if ($raw->qty > $available_qty) {
                $requested_qty = $raw->qty - $available_qty;
                $expected_semi_qty = RecipeContent::getExpectedQty($mat->semi_finish_product_id);
                $burden_size = $requested_qty / $expected_semi_qty;

            }

            $rawmate = ['id' => $mat->semi_finish_product_id, 'semi_name' => $mat->name, 'qty' => $raw->qty, 'available_qty' => $available_qty, 'loading_type' => $loading_type, 'requested_qty' => $requested_qty, 'burden_size' => $burden_size, 'is_bake' => $is_bake];
            array_push($data['items'], $rawmate);
        }


        return view('production.semi-issue', compact('data'));
    }

    public function makeSemiBurden(Request $request)
    {


        $semi_finish_product = $request->id;

        $active_recipe = RecipeContent::getActiveRecipe($semi_finish_product)->recipe_id;
        $burden_size = $request->qty;
        $burden_count = 1;
        $expected_semi_qty = RecipeContent::getExpectedQty($semi_finish_product) * $burden_size;


        for ($i = 0; $i < $burden_count; $i++) {
            $in = DB::select("show table status like 'burden'");

            $burdenCount = Burden::whereDate('created_at', '=', Carbon::today()->toDateString())->get()->count();
            $last_digit = $burdenCount + 1;
            $serial = date('ymd') . '-' . $last_digit;
            $burden = new Burden();
            $burden->semi_finish_product_id = $semi_finish_product;
            $burden->serial = $serial;
            $burden->burden_size = $burden_size;
            $ft = SemiFinishProduct::where('semi_finish_product_id', '=', $semi_finish_product)->first();
            $nft = $ft->is_fractional;
            if ($nft == 1) {

                $burden->expected_semi_product_qty = round($expected_semi_qty, 3);
            } else {
                $burden->expected_semi_product_qty = round($expected_semi_qty);
            }
            // $burden->expected_semi_product_qty = $expected_semi_qty;
            $burden->recipe_id = $active_recipe;
            $burden->created_by = Auth::user()->id;
            $burden->save();


        }

        return redirect()->back();
    }

    public function createSemiBurden(Request $request)
    {

        $burden = Burden::where('semi_finish_product_id', '=', $request->id)->whereStatus(2)->first();
        $qty = $burden->expected_semi_product_qty;
        $burden->status = 6;
        $burden->save();

        $bg = new BurdenGrade();
        $bg->burden_id = $burden->id;
        $bg->grade_id = 1;
        $bg->burden_qty = $qty;
        $bg->used_qty = $qty;
        $bg->save();

        StockTransaction::semiFinishStockTransaction(1, $request->id, 1, $qty, 1, 1);
        //  StockTransaction::updateSemiFinishAvailableQty(1, $request->id, $qty);
        $semi_product = SemiFinishProduct::find($request->id);
        $this->recordSemiFinishProductOutstanding($semi_product, 1, $qty, 'Burden Grade-Production', $bg->id, 'BurdenManager\\Models\\BurdenGrade', $burden->id, 'BurdenGradeProduction', 1, $bg->id, $request->mid);

        return 'true';

    }

    public function makeSemiBurdenCooking(Request $request)
    {


        $semi_finish_product = $request->id;
        $active_recipe = RecipeContent::getActiveRecipe($semi_finish_product)->recipe_id;
        $burden_size = $request->qty;
        $burden_count = 1;
        $expected_semi_qty = RecipeContent::getExpectedQty($semi_finish_product) * $burden_size;


        $in = DB::select("show table status like 'cooking_requests'");

        $burdenCount = CookingRequest::whereDate('created_at', '=', Carbon::today()->toDateString())->get()->count();
        $last_digit = $burdenCount + 1;
        $serial = 'BR-' . date('ymd') . '-' . $last_digit;
        $burden = new CookingRequest();
        $burden->semi_finish_product_id = $semi_finish_product;
        $burden->serial = $serial;
        $burden->burden_size = $burden_size;
        $burden->used_qty = $expected_semi_qty;
        $burden->expected_semi_product_qty = $expected_semi_qty;
        $burden->recipe_id = $active_recipe;
        $burden->created_by = Auth::user()->id;
        $burden->save();

        return redirect()->back();


    }

    public function createSemiBurdenCooking(Request $request)
    {

        $burden = CookingRequest::where('semi_finish_product_id', '=', $request->id)->whereStatus(2)->first();
        $burden->baker = null;
        $burden->status = 6;
        $burden->actual_semi_product_qty = $burden->expected_semi_product_qty;
        $burden->save();

        StockTransaction::semiFinishStockTransaction(1, $burden->semi_finish_product_id, '', $burden->actual_semi_product_qty, 1, 1);
        //   StockTransaction::updateSemiFinishAvailableQty(1, $burden->semi_finish_product_id, $burden->actual_semi_product_qty);
        $semi_product = SemiFinishProduct::find($burden->semi_finish_product_id);
        $this->recordSemiFinishProductOutstanding($semi_product, 1, $burden->actual_semi_product_qty, 'Cooking Request Grade-Production', $burden->id, 'ShortEatsManager\\Models\\CookingRequest', $burden->id, 'CookingGradeProduction', 1, $burden->id, $request->mid);
        //   return redirect()->back();
        return 'true';

    }

    public function semiIssue(Request $request)
    {

        $merge_serial = $request->merge_serial;
        $items = $request->semi;
        $qty = $request->qty;
        $expectedQty = $request->expected;
        $json_data = [];


        $sales_merge_header = SalesMergeEndProductBurdenHeader::where('end_product_burden_header_id', '=', $merge_serial)->first();


        foreach ($items as $key => $item) {
            $dif = new SalesMergeSemiDif();
            $dif->sales_merge_header_id = $sales_merge_header->sales_merge_header_id;
            $dif->end_product_burden_header_id = $merge_serial;
            $dif->semi_finish_product_id = $items[$key];
            $dif->expected_qty = $expectedQty[$key];
            $dif->issued_qty = $qty[$key];
            $dif->difference = $qty[$key] - $expectedQty[$key];
            $dif->save();


            $semi = SemiFinishProduct::where('semi_finish_product_id', '=', $items[$key])->first();
            $burden_ids = $semi->GradeABurdenList($semi->semi_finish_product_id)->pluck('id');
            $semi_id = $semi->semi_finish_product_id;
            $qty_total = $qty[$key];

            $sm = BurdenSemi::whereMergeId($merge_serial)->whereSemiFinishProductId($semi->semi_finish_product_id)->get();

            if ($semi->is_bake == 1) {
                $counter = 0;
                foreach ($burden_ids as $key_x => $burden_i) {
                    $sm = BurdenSemi::whereMergeId($merge_serial)->whereSemiFinishProductId($items[$key])->get();

                    if ($qty_total > 0) {

                        $semiBurden = BurdenGrade::where('id', '=', $burden_i)->first();

                        if (!empty($semiBurden)) {
                            if ($semiBurden->used_qty > 0) {
                                $deductable = ($qty_total < $semiBurden->used_qty) ? $qty_total : $semiBurden->used_qty;
                                $semiBurden->used_qty = $semiBurden->used_qty - $deductable;
                                $semiBurden->save();
                                $counter = $counter + 1;

                                StockTransaction::semiFinishStockTransaction(2, $semi_id, '', $deductable, 0, 1);
                                //  StockTransaction::updateSemiFinishAvailableQty(2, $semi_id, $deductable);
                                $semi_product = SemiFinishProduct::find($semi_id);
                                $this->recordSemiFinishProductOutstanding($semi_product, 1, $deductable, 'Issued For Burden Request-Production', $burden_i, 'BurdenManager\\Models\\BurdenGrade', $semiBurden->burden_id, 'IssuedBurdenProduction', 2, $semiBurden->id, $merge_serial);


                                $bdata = new SalesMergeBurdenGrade();
                                $bdata->sales_merge_header_id = $sales_merge_header->sales_merge_header_id;
                                $bdata->burden_grade_id = $semiBurden->id;
                                $bdata->burden_qty = $semiBurden->burden_qty;
                                $bdata->used_qty = $semiBurden->used_qty;
                                $bdata->save();


                                $filling_qty = $deductable;

                                foreach ($sm as $s) {
                                    $qty_x = $s->qty;
                                    if ($qty_x != $s->send_qty) {
                                        $fillable = $qty_x - $s->send_qty;
                                        $fil_qty = ($filling_qty < $fillable) ? $filling_qty : $fillable;
                                        $s->send_qty = $s->send_qty + $fil_qty;
                                        $filling_qty = $filling_qty - $fil_qty;
                                        $s->save();

                                        $issue = new SemiBurdenIssueData();
                                        $issue->burden_id = $semiBurden->id;
                                        $issue->type = 0;
                                        $issue->grade_id = 1;
                                        $issue->used_qty = $fil_qty;
                                        $issue->burden_qty = 0;
                                        $issue->data_list_id = 0;
                                        $issue->reference_type = "App/Burden";
                                        $issue->reference = $semiBurden->burden_id;
                                        $issue->sales_merge_header_id = $sales_merge_header->sales_merge_header_id;
                                        $issue->end_product_burden_header_id = $merge_serial;
                                        $issue->end_product_id = $s->end_product_id;
                                        $issue->merege_burden_semi_id = $s->id;
                                        $issue->description = "Burden Request - From Production";
                                        $issue->save();
                                    }
                                }


                                array_push($json_data, json_encode($semiBurden));
                                $qty_total = $qty_total - $deductable;
                            }
                        }
                    }

                }
            } else {
                $counter = 0;

                foreach ($burden_ids as $key_y => $burden) {
                    if ($qty_total > 0) {
                        $semiBurden = CookingRequest::where('id', '=', $burden)->first();
                        if (!empty($semiBurden)) {
                            if ($semiBurden->used_qty > 0) {
                                $deductable = ($qty_total < $semiBurden->used_qty) ? $qty_total : $semiBurden->used_qty;
                                $semiBurden->used_qty = $semiBurden->used_qty - $deductable;
                                $semiBurden->save();
                                $counter = $counter + 1;


                                $bdata = new SalesMergeCookingRequest();
                                $bdata->sales_merge_header_id = $sales_merge_header->sales_merge_header_id;
                                $bdata->cooking_request_id = $semiBurden->id;
                                $bdata->burden_qty = $semiBurden->expected_semi_product_qty;
                                $bdata->used_qty = $semiBurden->used_qty;
                                $bdata->save();


                                StockTransaction::semiFinishStockTransaction(2, $semi_id, '', $deductable, 0, 1);
                                //  StockTransaction::updateSemiFinishAvailableQty(2, $semi_id, $deductable);
                                $semi_product = SemiFinishProduct::find($semi_id);
                                $this->recordSemiFinishProductOutstanding($semi_product, 1, $deductable, 'Issued For Cooking Request-Production', $bdata->id, 'App\\SalesMergeCookingRequest', $semiBurden->id, 'IssuedCookingProduction', 2, $semiBurden->id, $merge_serial);

                                $filling_qty = $deductable;

                                foreach ($sm as $s) {
                                    $qty_x = $s->qty;
                                    if ($qty_x != $s->send_qty) {
                                        $fillable = $qty_x - $s->send_qty;
                                        $fil_qty = ($filling_qty < $fillable) ? $filling_qty : $fillable;
                                        $s->send_qty = $s->send_qty + $fil_qty;
                                        $filling_qty = $filling_qty - $fil_qty;
                                        $s->save();

                                        $issue = new SemiBurdenIssueData();
                                        $issue->burden_id = $semiBurden->id;
                                        $issue->type = 0;
                                        $issue->grade_id = 1;
                                        $issue->used_qty = $fil_qty;
                                        $issue->burden_qty = 0;
                                        $issue->data_list_id = 0;
                                        $issue->reference_type = "App/CookingRequest";
                                        $issue->reference = $semiBurden->id;
                                        $issue->sales_merge_header_id = $sales_merge_header->sales_merge_header_id;
                                        $issue->end_product_burden_header_id = $merge_serial;
                                        $issue->end_product_id = $s->end_product_id;
                                        $issue->merege_burden_semi_id = $s->id;
                                        $issue->description = "Burden Request - From Production";
                                        $issue->save();
                                    }


                                }


                                array_push($json_data, json_encode($semiBurden));
                                $qty_total = $qty_total - $deductable;
                            }
                        }
                    }
                }
            }

            array_push($json_data, json_encode($sm));
        }


        $merge = EndProductBurdenHeader::find($merge_serial);
        $merge->status = 2;
        $merge->issue_semi = 1;
        $merge->save();
        array_push($json_data, json_encode($merge));
        $burdens = json_decode($merge->burdens);


        foreach ($burdens as $burden) {
            $burden = EndProductBurden::find($burden);
            $burden->issue_semi = 1;
            $burden->save();
        }


        $record = new SemiIssueDataList();
        $record->type = 1;
        $record->data = json_encode($json_data);
        $record->created_by = Auth::user()->id;
        $record->save();


        $this->bypassMakeAndBakeProcess($sales_merge_header->sales_merge_header_id);
        return redirect()->route('pro.loading', ['id' => $merge->id]);
    }

    function bypassMakeAndBakeProcess($sales_merge_header_id)
    {
        $end_product_burden_ids = SalesMergeEndProductBurden::where('sales_merge_header_id', '=', $sales_merge_header_id)->pluck('end_product_burden_id');
        $end_product_burdens = EndProductBurden::whereIn('id', $end_product_burden_ids)->get();

        foreach ($end_product_burdens as $burden) {
            $burden->employee_group = null;
            $burden->created_start_time = Carbon::now();
            $burden->baker = null;
            $burden->bake_start_time = Carbon::now();
            $burden->status = 4;
            $burden->save();

            $category = $burden->endProduct->category->loading_type;
            $condition = ($category == 'to_burden') ? 1 : 0;

            if ($condition) {
                $burden_qty = $burden->expected_end_product_qty;
                $bg = new EndProductBurdenGrade();
                $bg->burden_id = $burden->id;
                $bg->grade_id = 1;
                $bg->burden_qty = $burden_qty;
                $bg->created_by = Auth::user()->id;
                $bg->save();

                // $this->recordOutstanding($burden->endProduct, 1, $burden_qty, $this->message['production_line_grade_a'], $bg->id, "APP\\EndProductBurdenGrade");
                StockTransaction::endProductStockTransaction(1, $burden->end_product_id, 0, 0, 1, $burden_qty, $burden->id, 1);
                StockTransaction::updateEndProductAvailableQty(1, $burden->end_product_id, $burden_qty);
                $end_product = EndProduct::find($burden->end_product_id);
                $this->recordEndProductOutstanding($end_product, 1, $burden_qty, 'Burden Grade-Production', $bg->id, 'App\\EndProductBurdenGrade', 0, 5, 1, $burden->id);


                $burden->status = 5;
                $burden->save();
            }

        }
    }

    public function salesLoadingList(Request $request)
    {
        $sales_merge_header_id = $request->id;
        $sales_merge_header_ids = SalesMergeEndProductBurdenHeader::where('end_product_burden_header_id', '=', $sales_merge_header_id)->pluck('sales_merge_header_id');

        $sales_order_ids = SalesMergeSalesRequest::whereIn('sales_merge_header_id', $sales_merge_header_ids)->pluck('sales_request_id');

        $sales_orders = LoadingHeader::with('salesOrder')->whereIn('so_id', $sales_order_ids)->get();

        $cooking_request = SalesMergeCookingRequest::whereIn('sales_merge_header_id', $sales_merge_header_ids)->pluck('cooking_request_id');
        $cook = CookingRequest::whereIn('id', $cooking_request)->get();
        $cooking_request_stage_1 = $cook->pluck('stage_1')->toArray();
        $cooking_request_stage_2 = $cook->pluck('stage_2')->toArray();


        $end_burden_ids = SalesMergeBurdenGrade::whereIn('sales_merge_header_id', $sales_merge_header_ids)->pluck('burden_grade_id');
        $end_b_ids = BurdenGrade::whereIn('id', $end_burden_ids)->pluck('burden_id');
        $br = Burden::whereIn('id', $end_b_ids)->get();
        $br_stage_1 = $br->pluck('stage_1')->toArray();
        $br_stage_2 = $br->pluck('stage_2')->toArray();


        $end_burden_ids = SalesMergeEndProductBurden::whereIn('sales_merge_header_id', $sales_merge_header_ids)->pluck('end_product_burden_id');
        $end_burdens = EndProductBurden::whereIn('id', $end_burden_ids)->get();
        $end_burdens_stage_1 = $end_burdens->pluck('stage_1')->toArray();
        $end_burdens_stage_2 = $end_burdens->pluck('stage_2')->toArray();


        $issue_condition = in_array(0, $cooking_request_stage_1) || in_array(0, $cooking_request_stage_2) || in_array(0, $br_stage_1) || in_array(0, $br_stage_2) || in_array(0, $end_burdens_stage_1) || in_array(0, $end_burdens_stage_2);


        return view('production.p_order_list', compact('sales_orders', 'sales_merge_header_id', 'issue_condition'));
    }

    public function srMergeList(Request $request)
    {
        $merged = SalesMergeHeader::whereStatus('merged')->get();
        return view('production.ep-list', compact('merged'));
    }

    public function epMergeList(Request $request)
    {
        $status = $request->status;
        $date_range = $request->date;


        $query = EndProductBurdenHeader::query();

        if (!empty($status)) {
            $query = $query->where('merge_status', '=', $status);
        }

        if (!empty($date_range)) {
            $dates = explode(' - ', $date_range);
            $start = $dates[0];
            $end = $dates[1];
            $query = $query->where('created_at', '>=', Carbon::parse($start)->format('Y-m-d'))
                ->where('created_at', '<=', Carbon::parse($end)->addDay()->format('Y-m-d'));
        }


        if (empty($status)) {
            $merged = $query->whereMergeStatus('created')->orderBy('created_at', 'DESC')->get();
        } else {
            $merged = $query->orderBy('created_at', 'DESC')->get();
        }

        return view('production.enp-list', compact('merged'));
    }

    public function mergedSalesRequestList(Request $request)
    {
        $sr_list = SalesMergeSalesRequest::where('sales_merge_header_id', '=', $request->id)->pluck('sales_request_id');
        $so_list = SalesOrder::whereIn('id', $sr_list)->get();
        return view('production.req-list', compact('so_list'));
    }

    public function semiBulk(Request $request)
    {
        $sales_merge_header_id = $request->id;
        $sales_merges = SalesMergeEndProductBurdenHeader::where('end_product_burden_header_id', '=', $sales_merge_header_id)->pluck('sales_merge_header_id');


        $end_burden_ids = SalesMergeBurdenGrade::whereIn('sales_merge_header_id', $sales_merges)->pluck('burden_grade_id');
        $end_b_ids = BurdenGrade::whereIn('id', $end_burden_ids)->pluck('burden_id');


        $cooking_request_ids = SalesMergeCookingRequest::whereIn('sales_merge_header_id', $sales_merges)->pluck('cooking_request_id');
        $cooking_request = SalesMergeCookingRequest::whereIn('sales_merge_header_id', $sales_merges)->first();


        //2021-07-18
        //$date_string = Carbon::parse($cooking_request->created_at)->format('Y-m-d') . '%';
        //$teams = Group::where('created_at', 'like', $date_string)->get();

        $date_string = Carbon::now()->subDays(3)->format('Y-m-d') . ' 00:00:00';
        $teams = Group::where('created_at', '>', $date_string)->get();


        $employees = Group::where('created_at', '>', Carbon::now()->subDays(3)->format('Y-m-d') . ' 00:00:00')->get();
        //2021-07-18
//        $employees = Group::where('created_at', 'like', Carbon::today()->format('Y-m-d') . '%')->get();

        $burdens = Burden::whereIn('id', $end_b_ids)->get();
        $cooking_requests = CookingRequest::whereIn('id', $cooking_request_ids)->get();

        //$employees = Employee::whereHas('attendance', function ($q) use ($date_string) {
        //    $q->where('date', 'like', Carbon::today()->format('Y-m-d') . '%');
        //})->pluck('full_name', 'id');

        return view('production.bulk_burden__semi_create', compact('burdens', 'cooking_requests', 'teams', 'employees'));
    }

    public function bulkBurdenStage(Request $request)
    {

        $burdens = $request->id;
        $team = $request->team;
        $employee = $request->employee;

        $type = $request->type;
        if ($type == 'make') {
            if (empty($burdens) || empty($team)) {
                return redirect()->back()->with([
                    'error' => true,
                    'error.title' => 'Sorry',
                    'error.message' => 'Required Field Missing',
                ]);
            }

            $burdens = Burden::whereIn('id', $burdens)->get();

            foreach ($burdens as $burden) {
                if ($burden->stage_1 == 0) {
                    //$burden->employee_group = $request->employee_group;
                    $burden->employee_group = $team;
                    $burden->created_start_time = Carbon::now();
                    $burden->stage_1 = 1;
                    $burden->save();
                }
            }

            return redirect()->back()->with([
                'success' => true,
                'success.title' => 'Burdens Make !',
                'success.message' => 'Burdens Make Successfully!',
            ]);


        }
        if ($type == 'bake') {
            if (empty($burdens) || empty($employee)) {
                return redirect()->back()->with([
                    'error' => true,
                    'error.title' => 'Sorry',
                    'error.message' => 'Required Field Missing',
                ]);
            }

            $burdens = Burden::whereIn('id', $burdens)->get();

            foreach ($burdens as $burden) {
                if ($burden->stage_2 == 0) {
                    //                  $burden->employee_group = $request->employee_group;
                    $burden->baker = $employee;
                    $burden->bake_start_time = Carbon::now();
                    $burden->stage_2 = 1;
                    $burden->save();
                }
            }

            return redirect()->back()->with([
                'success' => true,
                'success.title' => 'Burdens Baked !',
                'success.message' => 'Burdens Baked Successfully!',
            ]);
        }


    }

    public function bulkCookingStage(Request $request)
    {
        $burdens = $request->id;
        $team = $request->team;
        $employee = $request->employee;

        $type = $request->type;

        if ($type == 'make') {
            if (empty($burdens) || empty($team)) {
                return redirect()->back()->with([
                    'error' => true,
                    'error.title' => 'Sorry',
                    'error.message' => 'Required Field Missing',
                ]);
            }

            $burdens = CookingRequest::whereIn('id', $burdens)->get();

            foreach ($burdens as $burden) {
                if ($burden->stage_1 == 0) {
                    //                $burden->employee_group = $request->employee_group;
                    $burden->employee_group = $team;
                    $burden->created_start_time = Carbon::now();
                    $burden->stage_1 = 1;
                    $burden->save();
                }
            }

            return redirect()->back()->with([
                'success' => true,
                'success.title' => 'Cooking Request Make !',
                'success.message' => 'Cooking Request Make Successfully!',
            ]);
        }


        if ($type == 'bake') {
            if (empty($burdens) || empty($employee)) {
                return redirect()->back()->with([
                    'error' => true,
                    'error.title' => 'Sorry',
                    'error.message' => 'Required Field Missing',
                ]);
            }

            $burdens = CookingRequest::whereIn('id', $burdens)->get();
            foreach ($burdens as $burden) {
                if ($burden->stage_2 == 0) {
                    //      $burden->employee_group = $request->employee_group;
                    $burden->baker = $employee;
                    $burden->bake_start_time = Carbon::now();
                    $burden->stage_2 = 1;
                    $burden->save();
                }
            }

            return redirect()->back()->with([
                'success' => true,
                'success.title' => 'Cooking Request Baked !',
                'success.message' => 'Cooking Request Baked Successfully!',
            ]);

        }


        return redirect()->back();
    }

    public function endBulk(Request $request)
    {
        $end_product_burden_heder_id = $request->id;
        $sales_merges = SalesMergeEndProductBurdenHeader::where('end_product_burden_header_id', '=', $end_product_burden_heder_id)->pluck('sales_merge_header_id');
        $end_burden_ids = SalesMergeEndProductBurden::whereIn('sales_merge_header_id', $sales_merges)->pluck('end_product_burden_id');
        $end_burden_date = SalesMergeEndProductBurden::whereIn('sales_merge_header_id', $sales_merges)->first();

        $date_string = Carbon::now()->subDays(3)->format('Y-m-d') . ' 00:00:00';
        $teams = Group::where('created_at', '>', $date_string)->get();
        //2021-07-18
//        $teams = Group::where('created_at', 'like', $date_string)->get();
//        $date_string = Carbon::parse($end_burden_date->created_at)->format('Y-m-d') . '%';

        $end_burdens = EndProductBurden::whereIn('id', $end_burden_ids)->get();


        $employees = Group::where('created_at', '>', Carbon::now()->subDays(3)->format('Y-m-d') . ' 00:00:00')->get();
        //2021-07-18
//        $employees = Group::where('created_at', 'like', Carbon::today()->format('Y-m-d') . '%')->get();



        // $employees = Employee::whereHas('attendance', function ($q) {
        //     $q->where('date', 'like', Carbon::today()->format('Y-m-d') . '%');
        // })->pluck('full_name', 'id');
        return view('production.bulk_end', compact('end_burdens', 'teams', 'employees'));
    }

    public function bulkEndStage(Request $request)
    {
        $burdens = $request->id;
        $team = $request->team;
        $employee = $request->employee;
        $type = $request->type;

        $burdens = EndProductBurden::whereIn('id', $burdens)->get();


        if ($type == 'make') {
            if (empty($burdens) || empty($team)) {
                return redirect()->back()->with([
                    'error' => true,
                    'error.title' => 'Sorry',
                    'error.message' => 'Required Field Missing',
                ]);
            }

            foreach ($burdens as $burden) {
                if ($burden->stage_1 == 0) {
//                    $burden->employee_group = $request->employee_group;
                    $burden->employee_group = $team;
                    $burden->created_start_time = Carbon::now();
                    $burden->stage_1 = 1;
                    $burden->save();
                }
            }

            return redirect()->back()->with([
                'success' => true,
                'success.title' => 'End Product Make !',
                'success.message' => 'End Product Make Successfully!',
            ]);
        }


        if ($type == 'bake') {
            if (empty($burdens) || empty($employee)) {
                return redirect()->back()->with([
                    'error' => true,
                    'error.title' => 'Sorry',
                    'error.message' => 'Required Field Missing',
                ]);

            }
            foreach ($burdens as $burden) {
                if ($burden->stage_2 == 0) {
                    //       $burden->employee_group = $request->employee_group;
                    $burden->baker = $employee;
                    $burden->bake_start_time = Carbon::now();
                    $burden->stage_2 = 1;
                    $burden->save();
                }
            }

            return redirect()->back()->with([
                'success' => true,
                'success.title' => 'End Product Baked !',
                'success.message' => 'End Product Baked Successfully!',
            ]);
        }

    }

    public function differenceIssue(Request $request)
    {
        $header = $request->id;
        $deb = 'BurdenWastageProduction';
        $dec = 'CookingWastageProduction';
        //$data = SalesMergeSemiDif::where('end_product_burden_header_id', '=', $header)->get();
        $data_b = SemiInquiry::where('end_burden_id', '=', $header)
            ->where('meta', '=', $deb)
            ->get();
        $data_c = SemiInquiry::where('end_burden_id', '=', $header)
            ->where('meta', '=', $dec)
            ->get();

        return view('production.dif-list', compact('data_b', 'data_c'));
    }

    public function adjustSemi(Request $request)
    {
        $burden = EndProductBurdenHeader::find($request->id);
        $items = BurdenSemi::selectRaw(' COALESCE(sum(qty), 0) as qty, semi_finish_product_id,semi_finish_product_name,end_product_id')
            ->whereMergeId($burden->id)
            ->groupBy('semi_finish_product_id')
            ->get();


        $data = [];
        $data['items'] = [];
        $data['raw_issue'] = $burden->issue_raw;
        $data['serial'] = $burden->serial;
        $data['burden_id'] = $burden->id;

        foreach ($items as $raw) {
            $bgrade = 'BurdenGradeProduction';
            $cgrade = 'CookingGradeProduction';
            $bd = SemiInquiry::where('end_burden_id', '=', $burden->id)
                ->where('semi_id', '=', $raw->semi_finish_product_id)
                ->where('meta', '=', $bgrade)
                ->first();

            $bc = SemiInquiry::where('end_burden_id', '=', $burden->id)
                ->where('semi_id', '=', $raw->semi_finish_product_id)
                ->where('meta', '=', $cgrade)
                ->first();


            if (!empty($bd) || !empty($bc)) {
                $loading_type = $raw->endProduct->category->loading_type;
                $mat = SemiFinishProduct::whereSemiFinishProductId($raw->semi_finish_product_id)->first();
                $available_qty = $mat->available_qty;

                $is_bake = $mat->is_bake;

                $requested_qty = null;
                $burden_size = null;
                if ($raw->qty > $available_qty) {
                    $requested_qty = $raw->qty - $available_qty;
                    $expected_semi_qty = RecipeContent::getExpectedQty($mat->semi_finish_product_id);
                    $burden_size = $requested_qty / $expected_semi_qty;

                }

                $rawmate = ['id' => $mat->semi_finish_product_id, 'semi_name' => $mat->name, 'qty' => $raw->qty, 'available_qty' => $available_qty, 'loading_type' => $loading_type, 'requested_qty' => $requested_qty, 'burden_size' => $burden_size, 'is_bake' => $is_bake];
                array_push($data['items'], $rawmate);
            }
        }


        return view('production.adjust-issue', compact('data'));
    }

    public function adjustSemiBalance(Request $request)
    {
        $merge_serial = $request->merge_serial;
        $items = $request->semi;
        $qty = $request->qty;
        $expectedQty = $request->expected;
        $json_data = [];


        $sales_merge_header = SalesMergeEndProductBurdenHeader::where('end_product_burden_header_id', '=', $merge_serial)->first();


        foreach ($items as $key => $item) {

            $semi = SemiFinishProduct::where('semi_finish_product_id', '=', $item)->first();

            $burden_ids = $semi->GradeABurdenList($semi->semi_finish_product_id)->pluck('id');

            $semi_id = $semi->semi_finish_product_id;
            $qty_total = $qty[$key];


            if ($semi->is_bake == 1) {

                $counter = 0;
                foreach ($burden_ids as $key => $burden_i) {

                    if ($qty_total > 0) {
                        $semiBurden = BurdenGrade::where('id', '=', $burden_i)->first();

                        if (!empty($semiBurden)) {
                            if ($semiBurden->used_qty > 0) {
                                $deductable = ($qty_total < $semiBurden->used_qty) ? $qty_total : $semiBurden->used_qty;
                                $semiBurden->used_qty = $semiBurden->used_qty - $deductable;
                                $semiBurden->save();
                                $counter = $counter + 1;

                                StockTransaction::semiFinishStockTransaction(2, $semi_id, '', $deductable, 0, 1);
                                //    StockTransaction::updateSemiFinishAvailableQty(2, $semi_id, $deductable);
                                $semi_product = SemiFinishProduct::find($semi_id);


                                $bdata = new SalesMergeBurdenGradeAdjust();
                                $bdata->sales_merge_header_id = $sales_merge_header->sales_merge_header_id;
                                $bdata->burden_grade_id = $semiBurden->id;
                                $bdata->burden_qty = $semiBurden->burden_qty;
                                $bdata->used_qty = $semiBurden->used_qty;
                                $bdata->save();


                                $issue = new SemiBurdenIssueData();
                                $issue->burden_id = $semiBurden->id;
                                $issue->type = 0;
                                $issue->grade_id = 1;
                                $issue->used_qty = $deductable;
                                $issue->burden_qty = 0;
                                $issue->data_list_id = 0;
                                $issue->reference_type = "App/Burden";
                                $issue->reference = $semiBurden->burden_id;
                                $issue->description = "Adjust Semi Balance";
                                $issue->save();


                                $semi_product = SemiFinishProduct::find($semi_id);
                                $this->recordSemiFinishProductOutstanding($semi_product, 1, $deductable, 'Burden Wastage-Production', $issue->id, 'App\SemiBurdenIssueData', $semiBurden->burden_id, "BurdenWastageProduction", 2, $semiBurden->id, $merge_serial);

                                array_push($json_data, json_encode($semiBurden));
                                $qty_total = $qty_total - $deductable;
                            }
                        }
                    }

                }
            } else {
                $counter = 0;

                foreach ($burden_ids as $key => $burden) {
                    if ($qty_total > 0) {
                        $semiBurden = CookingRequest::where('id', '=', $burden)->first();
                        if (!empty($semiBurden)) {
                            if ($semiBurden->used_qty > 0) {
                                $deductable = ($qty_total < $semiBurden->used_qty) ? $qty_total : $semiBurden->used_qty;
                                $semiBurden->used_qty = $semiBurden->used_qty - $deductable;
                                $semiBurden->save();
                                $counter = $counter + 1;


                                $bdata = new SalesMergeCookingRequestAdjust();
                                $bdata->sales_merge_header_id = $sales_merge_header->sales_merge_header_id;
                                $bdata->cooking_request_id = $semiBurden->id;
                                $bdata->burden_qty = $semiBurden->expected_semi_product_qty;
                                $bdata->used_qty = $semiBurden->used_qty;
                                $bdata->save();

                                $issue = new SemiBurdenIssueData();
                                $issue->burden_id = $semiBurden->id;
                                $issue->type = 0;
                                $issue->grade_id = 1;
                                $issue->used_qty = $deductable;
                                $issue->burden_qty = 0;
                                $issue->data_list_id = 0;
                                $issue->reference_type = "App/CookingRequest";
                                $issue->reference = $semiBurden->id;
                                $issue->description = "Adjust Semi Balance";
                                $issue->save();

                                StockTransaction::semiFinishStockTransaction(2, $semi_id, '', $deductable, 0, 1);
                                //  StockTransaction::updateSemiFinishAvailableQty(2, $semi_id, $deductable);
                                $semi_product = SemiFinishProduct::find($semi_id);
                                $this->recordSemiFinishProductOutstanding($semi_product, 1, $deductable, 'Cooking Request Wastage-Production', $issue->id, 'App\SemiBurdenIssueData', $semiBurden->id, "CookingWastageProduction", 2, $semiBurden->id, $merge_serial);
                                array_push($json_data, json_encode($semiBurden));
                                $qty_total = $qty_total - $deductable;
                            }
                        }
                    }
                }
            }

        }

        $record = new SemiIssueDataList();
        $record->type = 1;
        $record->data = json_encode($json_data);
        $record->created_by = Auth::user()->id;
        $record->save();

        return redirect()->back();
    }

    public function ProductionVsAccept(Request $request)
    {
        $returnNoteCount = SalesReturnHeader::where('status', '<', 3)->where('is_system_generated', '=', 1)->count();

        $end_product_burden_header = $request->id;
        $merge = SalesMergeEndProductBurdenHeader::where('end_product_burden_header_id', '=', $end_product_burden_header)->pluck('sales_merge_header_id');
        $sales = SalesMergeSalesRequest::whereIn('sales_merge_header_id', $merge)->pluck('sales_request_id');
        $loadingHeader = LoadingHeader::whereIn('so_id', $sales)->pluck('id');

        $loadingData = LoadingData::select(DB::raw("SUM(admin_approved_qty) as qty"), DB::raw("SUM(qty) as requested_qty"), 'end_product_id')->whereIn('loading_header_id', $loadingHeader)->groupBy('end_product_id')->get();


        $end_product_burdens = SalesMergeEndProductBurden::whereIn('sales_merge_header_id', $merge)->pluck('end_product_burden_id');
        $end_pro = EndProductBurden::select(DB::raw("SUM(burden_size) as qty"), 'end_product_id')->whereIn('id', $end_product_burdens)->groupBy('end_product_id')->get();


        $test = EndProductBurdenGrade::with('burden')->whereHas('burden', function ($q) use ($end_product_burdens) {
            $q->whereIn('id', $end_product_burdens);
        })->whereGradeId(1)->get();


        $grade_2 = EndProductBurdenGrade::with('burden')->whereHas('burden', function ($q) use ($end_product_burdens) {
            $q->whereIn('id', $end_product_burdens);
        })->where('grade_id', '=', 2)->get();


        $grade_3 = EndProductBurdenGrade::with('burden')->whereHas('burden', function ($q) use ($end_product_burdens) {
            $q->whereIn('id', $end_product_burdens);
        })->where('grade_id', '=', 3)->get();


        $grade_4 = EndProductBurdenGrade::with('burden')->whereHas('burden', function ($q) use ($end_product_burdens) {
            $q->whereIn('id', $end_product_burdens);
        })->where('grade_id', '=', 4)->get();


        $bgx = collect();
        foreach ($test as $key => $bg) {
            $g2 = $g3 = $g4 = 0;

            if (!empty($grade_2)) {
                foreach ($grade_2 as $g2_qty) {
                    if ($bg->burden_id == $g2_qty->burden_id) {
                        $g2 = $g2_qty->burden_qty;
                    }
                }
            }

            if (!empty($grade_3)) {
                foreach ($grade_3 as $g3_qty) {
                    if ($bg->burden_id == $g3_qty->burden_id) {
                        $g3 = $g3_qty->burden_qty;
                    }
                }
            }

            if (!empty($grade_4)) {
                foreach ($grade_4 as $g4_qty) {
                    if ($bg->burden_id == $g4_qty->burden_id) {
                        $g4 = $g4_qty->burden_qty;
                    }
                }
            }

            $bgx->push(['burden_id' => $bg->burden_id, 'grade_id' => $bg->id, 'end_product_id' => $bg->burden->end_product_id, 'qty' => $bg->burden_qty, 'grade_2_qty' => $g2, 'grade_3_qty' => $g3, 'grade_4_qty' => $g4]);
        }


        //temp
        $end_pro = $bgx;

        return view('production.vs-list', compact('loadingData', 'end_pro', 'end_product_burden_header', 'returnNoteCount'));
    }

    public function productionAdjust(Request $request)
    {

        $burden_ids = $request->burden_id;
        $grade_ids = $request->grade_id;
        $end_products = $request->end_product;
        $differences = $request->difference;
        $end_product_burden_header = $request->end_product_burden_header;
        $grade_c_array = $request->grade_c;


        foreach ($end_products as $key => $end_productx) {
            $end_product = EndProduct::find($end_productx)->first();
            $grade_a_dif = $differences[$key];
            $grade_d_dif = $differences[$key];

            $isin = EndProductInquiry::where('so_id','=',$burden_ids[$key])->where('desc_id','=',6)->first();
            if(empty($isin)) {
                $grade_a_burden = EndProductBurdenGrade::find($grade_ids[$key]);
                $grade_a_burden->burden_qty = $grade_a_burden->burden_qty - $grade_a_dif;
                $grade_a_burden->save();


                StockTransaction::endProductStockTransaction(2, $end_productx, $end_product->distributor_price, $end_product->distributor_price, $end_product->distributor_price, $grade_a_dif, 0, 1);
                StockTransaction::updateEndProductAvailableQty(2, $end_productx, $grade_a_dif);
                $end_product = EndProduct::find($end_productx);
                $this->recordEndProductOutstanding($end_product, 1, $grade_a_dif, 'Grade Adjustment-Production', $grade_ids[$key], 'App\\EndProductBurdenGrade', 0, 6, 2, $burden_ids[$key]);

                if (!empty($grade_c_array)) {

                    if (array_key_exists($end_productx, $grade_c_array)) {
                        $grade_c = $grade_c_array[$end_productx];

                        $grade_d_dif = $differences[$key] - $grade_c;
                        $grade_c_weight = $grade_c * $end_product->weight;


                        $grade_c_burden = new EndProductBurdenGrade();
                        $grade_c_burden->burden_id = $grade_a_burden->burden_id;
                        $grade_c_burden->burden_qty = $grade_c;
                        $grade_c_burden->grade_id = 3;
                        //  $grade_c_burden->selling_price = $grade_a_burden->endProduct->selling_price;
                        //  $grade_c_burden->total_value = $grade_a_burden->endProduct->selling_price * $grade_c;
                        $grade_c_burden->save();

//                    $this->recordOutstanding($end_product, 4, -$grade_c, $this->message['production_line_grade_c'], $grade_c_burden->id, "APP\\EndProductBurdenGrade");


                        $trans = new ReusableStock();
                        $trans->causer_id = $grade_c_burden->id;
                        $trans->causer_type = 'APP\\EndProductBurdenGrade';
                        $trans->qty = $grade_c;
                        $trans->expected_weight = $grade_c_weight;
                        $trans->sales_return_header_id = 0;
                        $trans->reusable_product_id = $end_product;
                        $trans->received_type = 'end_grade';
                        $trans->reusable_type = $end_product->reusable_type;
                        $trans->save();


                    }
                }


                $grade_d_burden = new EndProductBurdenGrade();
                $grade_d_burden->burden_id = $grade_a_burden->burden_id;
                $grade_d_burden->burden_qty = $grade_d_dif;
                $grade_d_burden->grade_id = 4;

                //   $grade_d_burden->selling_price = $end_product->selling_price;
                //   $grade_d_burden->total_value = $end_product->selling_price * $grade_d_dif;

                $grade_d_burden->save();

            }
//            $this->recordOutstanding($end_product, 4, -$grade_d_dif, $this->message['production_line_grade_d'], $grade_d_burden->id, "APP\\EndProductBurdenGrade");


        }


        return redirect()->route('production.currantB', ['id' => $end_product_burden_header]);
    }

    public function endProductCurrantBalance(Request $request)
    {
        $end_product_burden_header = $request->id;
        $merge = SalesMergeEndProductBurdenHeader::where('end_product_burden_header_id', '=', $end_product_burden_header)->pluck('sales_merge_header_id');
        $end_product_burdens = SalesMergeEndProductBurden::whereIn('sales_merge_header_id', $merge)->pluck('end_product_burden_id');
        $end_pro = EndProductBurden::whereIn('id', $end_product_burdens)->pluck('end_product_id');

        $end_products = EndProduct::whereIn('id', $end_pro)->get();
        $employees = Employee::pluck('full_name', 'id');
        return view('production.end-list', compact('end_products', 'end_product_burden_header', 'employees'));
    }

    public function endProductCurrantBalanceUpdate(Request $request)
    {

        $end_product_burden_header = $request->end_product_burden_header;
        $end_products = $request->end_product_id;
        $available_qty = $request->available_qty;
        $destroy_qty = $request->destroy_qty;
        $re_use_qty = $request->re_use_qty;
        $lost_qty = $request->lost_qty;
        $employee = $request->employee;


        $merged = EndProductBurdenHeader::find($end_product_burden_header);

        $merged->merge_status = 'finished';
        $merged->finished_by = $employee;
        $merged->finished_at = Carbon::now()->format('Y-m-d H:i:s');
        $merged->save();


        if (!empty($end_products) && sizeof($end_products) > 0) {
            foreach ($end_products as $key => $end_product_id) {

                $is_varients = EndProduct::find($end_product_id)->variants;


                if (!empty($is_varients) && $is_varients->count() > 0) {
                    $size = $is_varients->min('size');
                }


                $end_pro = EndProduct::find($end_product_id);
                if ($re_use_qty[$key] > 0) {
                    $reusableReturn = new ReusableProducts();
                    $reusableReturn->reusable_type = $end_pro->reusable_type;
                    $reusableReturn->product_id = $end_product_id;
                    $reusableReturn->note_id = 0;
                    $reusableReturn->qty = $re_use_qty[$key];
                    $reusableReturn->expected_weight = $re_use_qty[$key] * $end_pro->weight;
                    $reusableReturn->accepted_weight = $re_use_qty[$key] * $end_pro->weight;
                    //collect type (1->semi,2->endproduct,3->salesReturn,4->MergeRemoval,5->StoresReturn)
                    $reusableReturn->collect_type = 4;
                    $reusableReturn->status = 1;
                    $reusableReturn->category_id = $end_pro->category_id;
                    $reusableReturn->agent_id = 0;
                    $reusableReturn->type_id = 1;//1=merge-reuse,2=merge-destroy,3=merge-lost,4=stores-reuse,5=stores-destroy,6=stores-lost,7=System Genarated,8=Return-Reuse,9=Return-Destroy,10=Return-SR,11-Return-Factory,12=Return-Lost

                    $reusableReturn->type = 're_use';
                    $reusableReturn->save();

                    $end_product = EndProduct::find($end_product_id);

                    $trans = new ReusableStock();
                    $trans->causer_id = $end_product_id;
                    $trans->causer_type = 'APP\\EndProduct';
                    $trans->qty = $re_use_qty[$key];
                    $trans->expected_weight = $re_use_qty[$key] * $end_pro->weight;
                    $trans->sales_return_header_id = 0;
                    $trans->reusable_product_id = $end_product_id;
                    $trans->reusable_type = $end_product->reusable_type;
                    $trans->received_type = 'merge_reuse';
                    $trans->save();
//                    $end_product = EndProduct::find($end_product_id);
                    StockTransaction::endProductStockTransaction(2, $end_product_id, 0, 0, 1, $re_use_qty[$key], 0, 1);
                    StockTransaction::updateEndProductAvailableQty(2, $end_product_id, $re_use_qty[$key]);

                    $this->recordEndProductOutstanding($end_product, 1, $re_use_qty[$key], 'Reusable-Production', $trans->id, 'App\\ReusableStock', 0, 7, 2, $end_product_burden_header);


                }

                if ($destroy_qty[$key] != 0) {
                    $end_product = EndProduct::find($end_product_id);
                    $reusableReturn = new ReusableProducts();
                    $reusableReturn->reusable_type = $end_pro->reusable_type;
                    $reusableReturn->product_id = $end_product_id;
                    $reusableReturn->note_id = 0;
                    $reusableReturn->qty = $destroy_qty[$key];
                    $reusableReturn->expected_weight = $destroy_qty[$key];
                    $reusableReturn->accepted_weight = 0;
                    //collect type (1->semi,2->endproduct,3->salesReturn,4->MergeRemoval,5->StoresReturn)
                    $reusableReturn->collect_type = 4;
                    $reusableReturn->status = 1;
                    $reusableReturn->category_id = $end_product->category_id;
                    $reusableReturn->agent_id = 0;
                    $reusableReturn->type_id = 2;//1=merge-reuse,2=merge-destroy,3=merge-lost,4=stores-reuse,5=stores-destroy,6=stores-lost,7=System Genarated,8=Return-Reuse,9=Return-Destroy,10=Return-SR,11-Return-Factory,12=Return-Lost

                    $reusableReturn->type = 'destroy';
                    $reusableReturn->save();

                    StockTransaction::endProductStockTransaction(2, $end_product_id, 0, 0, 1, $destroy_qty[$key], 0, 1);
                    StockTransaction::updateEndProductAvailableQty(2, $end_product_id, $destroy_qty[$key]);

                    $this->recordEndProductOutstanding($end_product, 1, $re_use_qty[$key], 'Destroy-Production', $reusableReturn->id, 'BurdenManager\\Models\\ReusableProducts', 0, 8, 2, $end_product_burden_header);

                }


                if ($lost_qty[$key] != 0) {
                    $end_product = EndProduct::find($end_product_id);
                    $reusableReturn = new ReusableProducts();
                    $reusableReturn->reusable_type = $end_pro->reusable_type;
                    $reusableReturn->product_id = $end_product_id;
                    $reusableReturn->note_id = 0;
                    $reusableReturn->qty = $lost_qty[$key];
                    $reusableReturn->expected_weight = $lost_qty[$key];
                    $reusableReturn->accepted_weight = 0;
                    //collect type (1->semi,2->endproduct,3->salesReturn,4->MergeRemoval,5->StoresReturn)
                    $reusableReturn->collect_type = 4;
                    $reusableReturn->status = 1;
                    $reusableReturn->type = 'lost';
                    $reusableReturn->category_id = $end_product->category_id;
                    $reusableReturn->agent_id = 0;
                    $reusableReturn->type_id = 3;//1=merge-reuse,2=merge-destroy,3=merge-lost,4=stores-reuse,5=stores-destroy,6=stores-lost,7=System Genarated,8=Return-Reuse,9=Return-Destroy,10=Return-SR,11-Return-Factory,12=Return-Lost

                    $reusableReturn->save();

                    StockTransaction::endProductStockTransaction(2, $end_product_id, 0, 0, 1, $lost_qty[$key], 0, 1);
                    StockTransaction::updateEndProductAvailableQty(2, $end_product_id, $lost_qty[$key]);

                    $this->recordEndProductOutstanding($end_product, 1, $re_use_qty[$key], 'Lost-Production', $reusableReturn->id, 'BurdenManager\\Models\\ReusableProducts', 0, 9, 2, $end_product_burden_header);

                }

            }
        }

        return redirect()->route('ep.list')->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => 'Process Finished Successfully'
        ]);
    }

    public function adminApprove(Request $request)
    {
        $end_product_burden_header = $request->id;
        $merged = EndProductBurdenHeader::find($end_product_burden_header);
        $merged->merge_status = 'approved';
        $merged->save();
        return redirect()->route('production.vs', ['id' => $end_product_burden_header]);

    }

    public function mergedSalesHeaderDelete(Request $request)
    {
        $sh = SalesMergeHeader::find($request->id);
        $sh->delete();
        return redirect()->back();
    }

    public function getBurdenSizes(Request $request)
    {

        $semi_prices = RecipeSize::whereSemiId($request->semi)->whereIsInactive(0)->pluck('burden_size', 'burden_size')->toArray();
        return $semi_prices;

    }
}
