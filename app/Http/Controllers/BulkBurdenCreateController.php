<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use EmployeeManager\Models\Group;
use EmployeeManager\Models\GroupEmployee;
use EndProductBurdenManager\Models\EndProductBurden;
use Illuminate\Http\Request;

class BulkBurdenCreateController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:BulkBurdenMake-list', ['only' => ['list']]);
        $this->middleware('permission:bulk_burden_create-index', ['only' => ['index','tableData']]);
        $this->middleware('permission:bulk_burden_create-createNow', ['only' => ['createNow']]);

    }
    public function index()
    {
        $teams = Group::whereDate('created_at', Carbon::today())->get();
        return view('bulk_burden_create', compact('teams'));
    }

    public function tableData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $recordsCount = 0;

        $date = $request->date_range;
        $columns = ['end_product_id', 'end_product_id', 'qty'];
        $order_column = $columns[$order_by[0]['column']];

        $records = EndProductBurden::tableData($order_column, $order_by_str, $start, $length);

        $records = $records->whereStatus(2)->get();
//
//            $records = $records->get();
        $recordsCount = (!empty($records)) ? sizeof($records) : 0;

        $data[][] = array();
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;


        foreach ($records as $key => $record) {
            $data[$i] = array(
                ($record->issue_raw && $record->issue_semi) ? "<input type='checkbox' name='id[]' value='$record->id'/>" : null,
                $record->endProduct->name,
                $record->serial,
                $record->burden_size,
                $record->expected_end_product_qty,
                ($record->issue_raw) ? "<i class='fa fa-check text-center'/>" : "<i class='text-center fa fa-close'/>",
                ($record->issue_semi) ? "<i class='fa fa-check'/>" : "<i class='fa fa-close'/>",
            );
            $i++;
        }


        if ($recordsCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($recordsCount),
            "recordsFiltered" => intval($recordsCount),
            "data" => $data
        ];

        return json_encode($json_data);
    }

    public function createNow(Request $request)
    {
        $burdens = $request->id;
        $team = $request->team;

        $burdens = EndProductBurden::whereIn('id', $burdens)->get();

        foreach ($burdens as $burden) {
           // $burden->employee_group = $request->employee_group;
            $burden->employee_group = $team;
            $burden->created_start_time = Carbon::now();
            $burden->status = 3;
            $burden->save();
        }



        return redirect(route('bulk_burden.index'));
    }

}
