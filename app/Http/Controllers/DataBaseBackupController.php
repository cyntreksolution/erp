<?php

namespace App\Http\Controllers;

use App\Console\Commands\DBBackupCommand;
use App\DatabaseBackup;
use App\DbBackupShedule;
use App\Traits\SendSMS;
use Carbon\Carbon;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use ZipArchive;
use Chumper\Zipper\Zipper;
use EndProductManager\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use hisorange\BrowserDetect\Parser as Browser;
class DataBaseBackupController extends Controller
{

    use SendSMS;
    function __construct()
    {
        $this->middleware('permission:database_backup-index', ['only' => ['index','dbBackupTableData']]);
        $this->middleware('permission:DatabaseBackup-list', ['only' => ['list']]);
    }
    public function index()
    {

        $shedule = DbBackupShedule::first();
        return view('db-backup.list',compact('shedule'));
    }

    public function create()
    {
        $id = 2;
        $db = DbBackupShedule::find($id);
        $time = Carbon::now()->addMinutes(2)->format('H:i');
        $db->status =  1;
        $db->time =  $time;
        $db->update();
        return redirect(route('db-backup.index'))->with('msgbackup','true');
    }

    public function delete(Request $request)
    {
        $id = $request->backup_id;
        $db = DatabaseBackup::find($id);
        $file= public_path() . "/" . $db->file_path;
        File::delete($file);
        $db->delete();
        return 'true';
    }

    public function shedule_update(Request $request){
        $db = DbBackupShedule::find(1);
        $time = Carbon::parse($request->time)->format('H:i');
        $db->status =  $request->sheduleID;
        $db->delete_status =  $request->delete_schedule;
        $db->time =  $time;
        $db->save();
        return $db;
    }

    public function dbBackupTableData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'created_at', 'size','download_count'];
        $order_column = $columns[$order_by[0]['column']];

        $dataset = DatabaseBackup::tableData($order_column, $order_by_str, $start, $length);

        if (is_null($search) || empty($search)) {
            $dataset = $dataset->get();
            $channels_count = DatabaseBackup::all()->count();
        } else {
            $dataset = $dataset->searchData($search)->get();
            $channels_count = $dataset->count();
        }

        $data[][] = array();
        $i = 0;


        foreach ($dataset as $key => $item) {
            $edit_btn = null;
            $delete_btn = null;



            $delete_btn = "<button class='btn btn-xs btn-icon btn-light-danger mr-2' onclick=\"deleteDBbackup('$item->id')\"> <i class='fa fa-trash'></i></button>";


            $url = route('db.download',$item->id);
            $file  = "backup/$item->file_path";
            $btnDownload = "<a href='$url'  target='_blank' class='btn btn-xs mr-2 btn-icon btn btn-primary'><i class='fa fa-download'></i> </a>";


            $data[$i] = array(
                $item->id,
                Carbon::parse($item->created_at)->format('Y-m-d H:i:s'),
                $item->size,
                $item->download_count,
                $btnDownload.$delete_btn
            );
            $i++;
        }


        if ($channels_count == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($channels_count),
            "recordsFiltered" => intval($channels_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }

    public function downloadFile(Request  $request,$id){
        $dbfile = DatabaseBackup::find($id);

        $count =$dbfile->download_count;
        $dbfile->download_count = $count +1;
        $dbfile->save();

        $file= storage_path() . "/" . $dbfile->file_path;

        $headers = array(
            'Content-Type: application/sql',
        );

        $user = Auth::user()->first_name;
        $ip = $request->getClientIp();
        $os =Browser::platformName();
        $br =Browser::browserName();

        $msg = "Database Backup downloaded by user $user from $br on $os : $ip";
        $this->sendSMS(env('SMS_NUMBER'), $msg);

        return Response::download($file);

    }

    public static function bytesToHuman($bytes)
    {
        $units = ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB'];

        for ($i = 0; $bytes > 1024; $i++) {
            $bytes /= 1024;
        }

        return round($bytes, 2) . ' ' . $units[$i];
    }
}
