<?php

namespace App\Http\Controllers;

use App\SalaryHeader;
use App\User;
use App\DayCommisData;
use BurdenManager\Models\Burden;
use BurdenManager\Models\BurdenGroupEmployee;
use Carbon\Carbon;
use EmployeeManager\Models\Employee;
use EmployeeManager\Models\Group;
use EmployeeManager\Models\GroupEmployee;
use EndProductBurdenManager\Models\EndProductBurden;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use SalaryManager\Models\Attendance;
use SalesOrderManager\Models\SalesOrder;
use SalesRepManager\Models\AgentCategory;
use ShortEatsManager\Models\CookingRequest;
use ShortEatsManager\Models\CookingRequestGroupEmployee;
use BurdenManager\Models\BurdenPayment;

class SalaryHeaderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:salary_header-index', ['only' => ['index','tableData']]);
        $this->middleware('permission:Commission-list', ['only' => ['list']]);
        $this->middleware('permission:salary_header-deleteCommission', ['only' => ['destroyCommission']]);
        $this->middleware('permission:salary_header-makeCommission', ['only' => ['makeCommission']]);

    }
    public function index(Request $request)
    {
        $date_range = $request->date_range;
        $employee = $request->employee;
        $employees = Employee::OrderBy('full_name')->pluck('full_name', 'id');
        return view('salary-attendance', compact('employees','employee','date_range'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\SalaryHeader $salaryHeader
     * @return \Illuminate\Http\Response
     */
    public function show(SalaryHeader $salaryHeader)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\SalaryHeader $salaryHeader
     * @return \Illuminate\Http\Response
     */
    public function edit(SalaryHeader $salaryHeader)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\SalaryHeader $salaryHeader
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SalaryHeader $salaryHeader)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\SalaryHeader $salaryHeader
     * @return \Illuminate\Http\Response
     */
    public function destroy(SalaryHeader $salaryHeader)
    {
        //
    }

    public function tableData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $recordsCount = 0;

        $date = $request->date_range;
        $columns = ['id', 'template_id', 'status', 'created_by'];
        $order_column = $columns[$order_by[0]['column']];

        $records = Attendance::tableData($order_column, $order_by_str, $start, $length);


        $filter_count = 0;
        if ($request->filter == true) {
            $date_range = $request->date_range;
            $employee = $request->employee;
            $records = $records->filterData($employee, $date_range)->get();
            $filter_count = $records->count();
            $salesOrdersCount = Attendance::all()->count();

        } elseif (is_null($search) || empty($search)) {
//            $records = $records->get();
//            $salesOrdersCount = Attendance::count();
            $records =[];
            $salesOrdersCount =0;
        } else {
            $records = $records->searchData($search)->get();
            $salesOrdersCount = Attendance::all()->count();
        }


        $recordsCount = $salesOrdersCount;


        $data = [];
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;

        $btn_status = null;
        $order_display_id = 0;
        $totMonCom = 0;
        $totwh = 0;
        $totcount = 0;
        $totcomcount = 0;

        $btn_totcommission = null;
        foreach ($records as $key => $record) {
            $totcount++;
            $btn_commission =$existDayCommis=null;
            $commis_employee_id=$records[$key]->employee_id;
            $commis_date=$records[$key]->date;
            $totDayCom=0;
            $existDayCommis = DayCommisData::where('emp_id','=',$commis_employee_id)->where('comis_date','=',$commis_date)->where('comis_type','=',1)->first();
            $commission_url = route('make.commission', $record->id);
           // ->selectRaw('*, sum(qty) as usages')

            if(empty($existDayCommis) && (!empty($employee))){
                $Comis = 0;
                $btn_commission = "<a target='_blank' href='$commission_url' class='btn btn-success'>Add Commission</a>";
            }else {
                $totcomcount++;
                $totDayCom = DayCommisData::where('emp_id', '=', $commis_employee_id)
                    ->where('comis_date', '=', $commis_date)
                    ->selectRaw('*, sum(comis_value) as comis')
                    ->first();
                $Comis = $totDayCom->comis;
                $workh = $record->work_hours;
                $totwh = $totwh + $workh;
                $totMonCom = $totMonCom + $Comis;


                if (!empty($employee) ){
                  if((Sentinel::check()->roles[0]->slug=='accountant') || (Sentinel::check()->roles[0]->slug=='owner')) {
                    $btn_commission = "<a href='#' class='btn btn-danger delete-commis' onclick='deleteCommis($existDayCommis->id)' data-id='$existDayCommis->id'>Delete Commission</a>". " "."<a target='_blank' href='$commission_url' class='btn btn-warning'>View Commission</a>";
                }else{
                      $btn_commission = "<a target='_blank' href='$commission_url' class='btn btn-warning'>View Commission</a>";

                  }
                }
            }
            $data[$i] = array(
                $record->date,
                $record->employee->full_name,
                $record->work_hours,
                number_format($Comis,2),
                $btn_commission
            );
            $i++;
        }
        if(!empty($employee)) {
            if (($totcount == $totcomcount) && (Auth::user()->hasRole(['Accountant','Owner']))) {
                $btn_totcommission = "<a href='#' class='btn btn-warning month-commis'  onclick='monthCommis($totMonCom)' '>Add Monthly Commission</a>";
            }
        }
        if($totMonCom != 0) {
            $emp_id =$record->employee->id;
            $data[$i] = array(
                'Monthly',
                'Commission',
                $totwh,
                number_format($totMonCom,2),
                $btn_totcommission
            );
        }
        //  $btn_mon_commission = ("<a href='$mon_commission_url' class='btn btn-success'>Monthly Commission</a>"));



        if ($recordsCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($recordsCount),
            "recordsFiltered" => intval($filter_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }

    public function getMembers($group_id){
        return GroupEmployee::where('group_id', '=', $group_id)->count();
    }

    public function getPay($product_id,$stage_id,$pro_type){
        return BurdenPayment::where(['pro_id'=> $product_id,'stage_id'=>$stage_id,'pro_type'=>$pro_type,'is_inactive'=>0])->first();
    }

    public function destroyCommission(Request $request){
        $dayCommis = DayCommisData::find($request->commis_id);

        if(!empty($dayCommis)){
            DayCommisData::destroy($request->commis_id);
        }
        $json_data = [
            "status" => true
        ];

        return json_encode($json_data);

    }

    public function makeCommission(Request $request, Attendance $attendance)
    {

        $work_date = $attendance->date;
        $employee_id = $attendance->employee_id;
        $employee=  Employee::find($employee_id);


        $group_ids = Group::where('created_at','like',$work_date.'%')
        ->whereHas('employees',function ($q) use ($employee_id){
            $q->where('employee_id','=',$employee_id);
        })
        ->pluck('id');

        $end_product_baker = EndProductBurden::whereIn('baker',$group_ids)->with('grade')->get();

           $semi_finish_baker = Burden::whereIn('baker',$group_ids)->with('grade')->get();


            $cooking_request_baker = CookingRequest::whereIn('baker',$group_ids)->get();

           $end_product_maker = EndProductBurden::whereIn('employee_group',$group_ids)->with('grade')->get();

           $semi_finish_maker = Burden::whereIn('employee_group',$group_ids)->with('grade')->get();


            $cooking_request_maker = CookingRequest::whereIn('employee_group',$group_ids)->get();




        //   $end_product_burdens = EndProductBurden::whereIn('employee_group',$group_ids)->with('grade')->get();

     //   $semi_finish_burdens_ids = BurdenGroupEmployee::whereIn('group_id',$group_ids)->pluck('burden_id');

     //   $semi_finish_burdens =Burden::whereIn('id',$semi_finish_burdens_ids)->with('grade')->get();

     //   $semi_finish_burdens =Burden::whereIn('employee_group',$group_ids)->with('grade')->get();


    //    $cooking_requests_ids = CookingRequestGroupEmployee::whereIn('group_id',$group_ids)->pluck('cooking_request_id');
     //   $cooking_requests = CookingRequest::whereIn('id',$cooking_requests_ids)->get();

     //   $cooking_requests = CookingRequest::whereIn('employee_group',$group_ids)->get();

     //   $end_product_baker = EndProductBurden::whereIn('baker',$group_ids)->with('grade')->where('bake_start_time','like',$work_date.'%')->get();

     //   $semi_finish_baker = Burden::whereIn('baker',$group_ids)->where('bake_start_time','like',$work_date.'%')->with('grade')->get();


    //    $cooking_request_baker = CookingRequest::whereIn('baker',$group_ids)->where('bake_start_time','like',$work_date.'%')->get();

     //   $end_product_maker = EndProductBurden::whereIn('employee_group',$group_ids)->with('grade')->where('created_start_time','like',$work_date.'%')->get();

     //   $semi_finish_maker = Burden::whereIn('employee_group',$group_ids)->where('created_start_time','like',$work_date.'%')->with('grade')->get();


    //    $cooking_request_maker = CookingRequest::whereIn('employee_group',$group_ids)->where('created_start_time','like',$work_date.'%')->get();


   //     return view('summary',compact('end_product_burdens','semi_finish_burdens','cooking_requests'
        return view('summary',compact('end_product_baker','semi_finish_baker','cooking_request_baker','end_product_maker','semi_finish_maker','cooking_request_maker','employee','work_date'))->with('commission', new SalaryHeaderController);

    }
}
