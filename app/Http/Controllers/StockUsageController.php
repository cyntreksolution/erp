<?php

namespace App\Http\Controllers;

use App\LoadingData;
use App\LoadingHeader;
use App\RawInquiry;
use App\SalesReturnData;
use App\SalesReturnHeader;
use App\Variant;
use Carbon\Carbon;
use EndProductManager\Models\Category;
use EndProductManager\Models\EndProduct;
use LoadingManager\Models\Loading;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RawMaterialManager\Models\RawMaterial;
use SalesOrderManager\Models\SalesOrder;
use SalesRepManager\Models\SalesRep;

class StockUsageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:stock_usage-index', ['only' => ['index']]);
        $this->middleware('permission:stock_usage-datatable', ['only' => ['datatable']]);
        $this->middleware('permission:stock_usage-tableData', ['only' => ['shopitem','tableData']]);
        $this->middleware('permission:ShopItemMovement-list', ['only' => ['list']]);
        $this->middleware('permission:stock_usage-comparison', ['only' => ['comparison']]);
        $this->middleware('permission:SalesReturnComparison-list', ['only' => ['list']]);
    }

    public function index()
    {
        //
        $raw_materials = RawMaterial::all();
        return view('stock_usage',compact('raw_materials'));
    }

    public function crates()
    {
        $agents = SalesRep::all()->pluck('full_name', 'id');

        $raw_materials = RawMaterial::where('type' , '=' , 5)->get()->pluck('name', 'raw_material_id');
        return view('crate_usage_list',compact('raw_materials','agents'));
    }

    public function shopitem()
    {
        $agents = SalesRep::orderBy('name_with_initials')->get()->pluck('name_with_initials', 'id');

        $raw_materials = RawMaterial::where('type' , '=' , 4)->get()->pluck('name', 'raw_material_id');
        return view('shopitemslist',compact('raw_materials','agents'));


    }

    public function comparison()
    {
        $agents = SalesRep::orderBy('name_with_initials')->get()->pluck('name_with_initials', 'id');

        $end_products = EndProduct::all()->pluck('name', 'id');
        $categories = Category::pluck('name', 'id');

        return view('sales_return_comparison', compact('end_products', 'categories', 'agents'));
    }

    public function list()
    {
        //
        $raw_materials = RawMaterial::all()->pluck('name', 'raw_material_id');;
        return view('raw_inquiry_list',compact('raw_materials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Return JSON array for datatable.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function datatable(Request $request)
    {
        $start_date = date("Y-m-d",strtotime("-1 days"));
        $end_date = date("Y-m-d");
        $order_by = $_REQUEST['order']; // This array contains order information of clicked column
        $search = $_REQUEST['search']['value']; // This array contains search value information datatable
        $start = $_REQUEST['start']; // start limit of data
        $length = $_REQUEST['length']; // end limit of data
        $order_by_str = $order_by[0]['dir'];
        if ($order_by[0]['column']==0){
            $order_column = "raw_material.name";
        }
        if ($order_by[0]['column']==1){
            $order_column = "stock_raw_material.created_at";
        }
        if ($order_by[0]['column']==2){
            $order_column = "stock_raw_material.created_at";
        }
        if ($order_by[0]['column']==3){
            $order_column = "stock_raw_material.qty";
        }
        $where_query = "date(stock_raw_material.created_at) between '$start_date' and '$end_date' ";
        if ($search!='' || $search!=NULL) {
            $where_query .= "and (raw_material.name like '%$search%' or stock_raw_material.created_at like '%$search%' or stock_raw_material.qty like '$search')";
        }
        $records = DB::table("stock_raw_material")
            ->join('raw_material','raw_material.raw_material_id','=','stock_raw_material.raw_material_id')
            ->select('raw_material.name','stock_raw_material.created_at','stock_raw_material.qty')
            ->whereRaw($where_query)
            ->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length)
            ->get();
        $records_count = DB::table("stock_raw_material")
            ->join('raw_material','raw_material.raw_material_id','=','stock_raw_material.raw_material_id')
            ->select(DB::raw('count(*) as all_count'))
            ->whereRaw($where_query)
            ->first();

        $data[][] = array();
        $i = 0;
        foreach ($records as $record)
        {
            $data[$i] = array(
                $record->name,
                $record->created_at,
                $record->created_at,
                $record->qty
            );
            $i++;
        }

        $totalRecords = $records_count->all_count;
        if ($totalRecords==0){
            $data = array();
        }
        $json_data = array(
            "draw"            => intval($_REQUEST['draw']),
            "recordsTotal"    => intval($totalRecords),
            "recordsFiltered" => intval($totalRecords),
            "data"            => $data   // total data array
        );
        $json_data = json_encode($json_data);
        return $json_data;
    }


    public function tableData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'created_at'];
        $order_column = $columns[$order_by[0]['column']];

        $stat_data =$rawmaterials= RawInquiry::query();
        $rawmaterialsCount = 0;
        $allSum = null;

        if ($request->filter == true) {
            $raw_material = $request->raw_material;
            $date_range = $request->date_range;
            $agent = $request->agent;
            $desc_id = 12;


            $allSum = $stat_data->filterDataShopItem($raw_material, $agent, $desc_id, $date_range)
                ->select(DB::raw('SUM(raw_inquiry.amount) as  amount_sum'), DB::raw('SUM(raw_inquiry.qty) as qty_sum'))
                ->first();

            $rawmaterialsCount = $rawmaterials->count();
            $rawmaterials = RawInquiry::filterDataShopItem($raw_material, $agent, $desc_id, $date_range)->tableData($order_column, $order_by_str, $start, $length)->orderBy('id', 'asc')->get();

        }

        if (!empty($allSum['amount_sum'])) {
            $sumAmount = $allSum['amount_sum'];
        } else {
            $sumAmount = null;
        }
        if (!empty($allSum['qty_sum'])) {
            $sumQty = $allSum['qty_sum'];
        } else {
            $sumQty = null;
        }
        $data[][] = array();
        $i = 0;



        foreach ($rawmaterials as $key => $rawmaterial) {


          /*  if(!empty($agent)) {
                $ag = SalesRep::whereId($agent)->first();
            }else{
                $ag = SalesRep::whereId($rawmaterial->Issued_ref_id)->first();
            }*/
            $agname='';

            $ag = SalesRep::whereId($rawmaterial->Issued_ref_id)->first();
            $refBtn = '<a class="btn btn-warning btn-sm mr-1" href="#">' . $rawmaterial->reference . '</a>';
            if($rawmaterial->Issued_ref_id != 0 || !empty($rawmaterial->Issued_ref_id)) {
                $agname = $ag->name_with_initials;
            }
            $data[$i] = array(
                Carbon::parse($rawmaterial->created_at)->format('Y-M-d H:i:s'),
                $agname,
                optional($rawmaterial->raw_material)->name,
                (($rawmaterial->qty)*(-1)),
                number_format(((($rawmaterial->amount)*(-1))), 2),
                $refBtn
            );
            $i++;
        }

        $rawMats = RawInquiry::filterDataShopItem($raw_material, $agent, $desc_id, $date_range)->orderBy('id', 'asc')->get();
        $totqty = 0;
        $totval = 0;
        foreach ($rawMats as $key => $rawMat) {
            $totqty = $totqty + (($rawMat->qty) * (-1));
            $totval = $totval + (($rawMat->amount) * (-1));
        }

        $data[$i] = array(
            'Total',
            '',
            '',
            $totqty,
            number_format(($totval), 2),
            null
        );

      /*  if ($rawmaterialsCount == 0) {
            $data = [];
        }*/

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($rawmaterialsCount),
            "recordsFiltered" => intval($rawmaterialsCount),
            "data" => $data,
            "qty_sum" => number_format($sumQty, 2),
            "amount_sum" => number_format($sumAmount, 2),
            ];

        return json_encode($json_data);
    }

    public function comTable(Request $request)
    {


        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'created_at'];
        $order_column = $columns[$order_by[0]['column']];



        if ($request->filter == true) {


            $category = $request->category;
            $end_product = $request->end_product;
            $date_range = $request->date_range;
            $agent_id = $request->agent_id;
            $type = $request->type;
            $data[][] = array();
            $i = 0;

            if ($type == 6) {
                $stat_data = $retproducts = SalesReturnData::query();
                $retproductsCount = 0;
                $retSum = null;


                $retSum = $stat_data->filterDataReturn($end_product, $agent_id, $category, $date_range)
                    ->select(DB::raw('SUM(sales_return_data.all_qty) as allret_sum'), DB::raw('SUM(sales_return_data.qty) as ret_sum'))
                    ->where('is_system_generated', '=', 0)
                    ->first();

                $retproductsCount = $retproducts->count();
                $retproducts = SalesReturnData::filterDataReturn($end_product, $agent_id, $category, $date_range)->comTable($order_column, $order_by_str, $start, $length)->orderBy('id', 'asc')->get();


                //  $endProCount = $endPro->count();
                //  $rawmaterials = RawInquiry::filterDataShopItem($raw_material, $agent, $desc_id, $date_range)->tableData($order_column, $order_by_str, $start, $length)->orderBy('id', 'asc')->get();


                if (!empty($retSum['allret_sum'])) {
                    $sumAllQty = $retSum['allret_sum'];
                } else {
                    $sumAllQty = 0;
                }
                if (!empty($retSum['ret_sum'])) {
                    $sumQty = $retSum['ret_sum'];
                } else {
                    $sumQty = 0;
                }



                $totqty = 0;
                $totqtyVal = 0;
                $tot_allqty = 0;
                $tot_allqtyVal = 0;
                $tot_val = 0;



                foreach ($retproducts as $key => $retproduct) {
                    $sz = 1;
                    $endP = EndProduct::find($retproduct->end_product_id);
                    $vari = Variant::where('end_product_id', '=', $endP->id)->where('return_accepted', '=', 1)->first();
                    if (!empty($vari)) {
                        $sz = $vari->size;
                    }
                    //$totqty = $totqty + ($retPro->qty);
                    //$totqtyVal = $totqtyVal + (($retPro->qty) * ($endP->distributor_price) * $sz);
                    //$tot_allqty = $tot_allqty + ($retPro->all_qty);
                    //$tot_allqtyVal = $tot_allqtyVal + (($retPro->all_qty) * ($endP->distributor_price) * $sz);


                    $agname = '';

                    $ag = SalesRep::whereId($retproduct->agent_id)->first();
                    $debid = SalesReturnHeader::find($retproduct->sales_return_header_id);

                    $refBtn = '<a class="btn btn-warning btn-sm mr-1" href="#">' . $debid->debit_note_number . '</a>';
                    if ($retproduct->agent_id != 0 || !empty($retproduct->agent_id)) {
                        $agname = $ag->name_with_initials;
                    }


                    $tot_val1 = ((($retproduct->all_qty) - ($retproduct->qty)) * ($endP->distributor_price) * $sz);
                    $tot_val = $tot_val + $tot_val1;

                    if ($retproduct->all_qty != 0) {
                        $data[$i] = array(
                            Carbon::parse($retproduct->created_at)->format('Y-M-d H:i:s'),
                            $endP->name,
                            $agname,
                            number_format($retproduct->all_qty, 3),
                            number_format((($retproduct->all_qty) * ($endP->distributor_price) * $sz), 3),
                            number_format($retproduct->qty, 3),
                            number_format((($retproduct->qty) * ($endP->distributor_price) * $sz), 3),
                            number_format((($retproduct->all_qty) - ($retproduct->qty)), 3),
                            number_format(((($retproduct->all_qty) - ($retproduct->qty)) * ($endP->distributor_price) * $sz), 2),
                            number_format(((($retproduct->all_qty) - ($retproduct->qty)) / ($retproduct->all_qty)) * 100, 3),
                            $refBtn
                        );
                        $i++;
                    }
                }
                $retPro = SalesReturnData::filterDataReturn($end_product, $agent_id, $category, $date_range)->orderBy('id', 'asc')->get();
                foreach ($retPro as $key => $retprosum) {
                    $sz = 1;
                    $endP = EndProduct::find($retprosum->end_product_id);
                    $vari = Variant::where('end_product_id', '=', $endP->id)->where('return_accepted', '=', 1)->first();
                    if (!empty($vari)) {
                        $sz = $vari->size;
                    }
                    $totqty = $totqty + ($retprosum->qty);
                    $totqtyVal = $totqtyVal + (($retprosum->qty) * ($endP->distributor_price) * $sz);
                    $tot_allqty = $tot_allqty + ($retprosum->all_qty);
                    $tot_allqtyVal = $tot_allqtyVal + (($retprosum->all_qty) * ($endP->distributor_price) * $sz);
                }


                    if ($sumAllQty != 0) {
                    $xx = number_format(((($sumAllQty) - ($sumQty)) / ($sumAllQty)) * 100, 3);
                } else {
                    $xx = 0;
                }



                $data[$i] = array(
                    'Total',
                    '',
                    '',
                    number_format($sumAllQty, 3),
                    number_format($tot_allqtyVal, 3),
                    number_format($sumQty, 3),
                    number_format($totqtyVal, 3),
                    number_format((($sumAllQty) - ($sumQty)), 3),
                    number_format((($tot_allqtyVal) - ($totqtyVal)), 2),
                    $xx,
                    null
                );


                $json_data = [
                    "draw" => intval($_REQUEST['draw']),
                    "recordsTotal" => intval($retproductsCount),
                    "recordsFiltered" => intval($retproductsCount),
                    "data" => $data,
                    //  "qty_sum" => number_format($sumQty, 2),
                    // "amount_sum" => number_format($sumAmount, 2),
                ];

                return json_encode($json_data);




            }elseif ($type == 1 || $type == 2 || $type == 3 ){
                $stat_data = $nonveri = LoadingData::query();
                $nonveriCount = 0;
                $nonveriSum = null;

                $nonveriSoldSum = $stat_data->filterDataNonVariant($end_product, $agent_id, $category, $date_range)
                    ->select(DB::raw('SUM(loading_data.qty) as soldqty_sum'))
                    ->where('status', '=', 1)
                    ->first();

                $nonveriSum = $stat_data->filterDataNonVariant($end_product, $agent_id, $category, $date_range)
                        ->select(DB::raw('SUM(loading_data.requested_qty) as reqqty_sum'),DB::raw('SUM(loading_data.extra_qty) as extqty_sum'),DB::raw('SUM(loading_data.agent_available_qty) as avbqty_sum'),DB::raw('SUM(loading_data.admin_approved_qty) as aprqty_sum'))
                        ->first();

                $nonveriCount = $nonveri->count();
              //  $nonveri = LoadingData::filterDataNonVariant($end_product, $agent_id, $category, $date_range)->comTable($order_column, $order_by_str, $start, $length)->orderBy('id', 'asc')->get();
                $nonveri = LoadingData::filterDataNonVariant($end_product, $agent_id, $category, $date_range)->orderBy('id', 'asc')->get();



                if (!empty($nonveriSoldSum['soldqty_sum'])) {
                    $sumSoldQty = $nonveriSoldSum['soldqty_sum'];
                } else {
                    $sumSoldQty = 0;
                }
                if (!empty($nonveriSum['reqqty_sum'])) {
                    $sumReqQty = $nonveriSum['reqqty_sum'];
                } else {
                    $sumReqQty = 0;
                }

                if (!empty($nonveriSum['extqty_sum'])) {
                    $sumExtQty = $nonveriSum['extqty_sum'];
                } else {
                    $sumExtQty = 0;
                }

                if (!empty($nonveriSum['avbqty_sum'])) {
                    $sumAvbQty = $nonveriSum['avbqty_sum'];
                } else {
                    $sumAvbQty = 0;
                }

                if (!empty($nonveriSum['aprqty_sum'])) {
                    $sumAprQty = $nonveriSum['aprqty_sum'];
                } else {
                    $sumAprQty = 0;
                }



                $totqty_t1 = 0;
                $totqty_t2 = 0;
                $totval_t1 = 0;
                $totval_t2 = 0;




                foreach ($nonveri as $key => $nonveriPro) {
                  //  $sz = 1;
                    $endP = EndProduct::find($nonveriPro->end_product_id);


                   /* $vari = Variant::where('end_product_id', '=', $endP->id)->where('return_accepted', '=', 1)->first();
                    if (!empty($vari)) {
                        $sz = $vari->size;
                    }*/


                    //$totqty = $totqty + ($retPro->qty);
                    //$totqtyVal = $totqtyVal + (($retPro->qty) * ($endP->distributor_price) * $sz);
                    //$tot_allqty = $tot_allqty + ($retPro->all_qty);
                    //$tot_allqtyVal = $tot_allqtyVal + (($retPro->all_qty) * ($endP->distributor_price) * $sz);


                    $agname = '';

                    $ag1 = LoadingHeader::find($nonveriPro->loading_header_id);
                    $ag2 = SalesOrder::find($ag1->so_id);

                    $ag = SalesRep::whereId($ag2->created_by)->first();

                    $reqQty = $nonveriPro->requested_qty;
                    $extQty = $nonveriPro->extra_qty;
                    $avbQty = $nonveriPro->agent_available_qty;
                    $ordQty = ($reqQty + $extQty) - $avbQty ;

                    $soldQty = ($nonveriPro->qty)->status(1);
                    $aprQty = $nonveriPro->admin_approved_qty;

                     if (empty($agent_id)) {
                         $refBtn = '<a class="btn btn-warning btn-sm mr-1" href="#">' . $ag1->invoice_number . '</a>';

                        if (($ordQty != 0 ) || ($soldQty != 0 )) {
                            $data[$i] = array(
                                Carbon::parse($nonveriPro->created_at)->format('Y-M-d H:i:s'),
                                $endP->name,
                                $agname,
                                number_format($ordQty, 3),
                                number_format((($ordQty) * ($nonveriPro->unit_price)), 3),
                                number_format($soldQty, 3),
                                number_format((($soldQty) * ($nonveriPro->unit_price)), 3),
                                number_format((($ordQty) - ($soldQty)), 3),
                                number_format(((($ordQty) - ($soldQty)) * ($nonveriPro->unit_price)), 2),
                                number_format(((($ordQty) - ($soldQty)) / ($ordQty)) * 100, 3),
                                $refBtn
                            );
                            $i++;
                        }

                    }else{
                        $agname = $ag->name_with_initials;
                    }

                }
           /*     $retPro = SalesReturnData::filterDataReturn($end_product, $agent_id, $category, $date_range)->orderBy('id', 'asc')->get();
                foreach ($retPro as $key => $retprosum) {
                    $sz = 1;
                    $endP = EndProduct::find($retprosum->end_product_id);
                    $vari = Variant::where('end_product_id', '=', $endP->id)->where('return_accepted', '=', 1)->first();
                    if (!empty($vari)) {
                        $sz = $vari->size;
                    }
                    $totqty = $totqty + ($retprosum->qty);
                    $totqtyVal = $totqtyVal + (($retprosum->qty) * ($endP->distributor_price) * $sz);
                    $tot_allqty = $tot_allqty + ($retprosum->all_qty);
                    $tot_allqtyVal = $tot_allqtyVal + (($retprosum->all_qty) * ($endP->distributor_price) * $sz);
                }


                if ($sumAllQty != 0) {
                    $xx = number_format(((($sumAllQty) - ($sumQty)) / ($sumAllQty)) * 100, 3);
                } else {
                    $xx = 0;
                }



                $data[$i] = array(
                    'Total',
                    '',
                    '',
                    number_format($sumAllQty, 3),
                    number_format($tot_allqtyVal, 3),
                    number_format($sumQty, 3),
                    number_format($totqtyVal, 3),
                    number_format((($sumAllQty) - ($sumQty)), 3),
                    number_format((($tot_allqtyVal) - ($totqtyVal)), 2),
                    $xx,
                    null
                );

        */
                $json_data = [
                    "draw" => intval($_REQUEST['draw']),
                    "recordsTotal" => intval($nonveriCount),
                    "recordsFiltered" => intval($nonveriCount),
                    "data" => $data,
                    //  "qty_sum" => number_format($sumQty, 2),
                    // "amount_sum" => number_format($sumAmount, 2),
                ];

                return json_encode($json_data);



            }
        }
    }



    /**
     * Return array of new row to datatable.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public static function get_new_rows(Request $request)
    {
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $raw_material_id = $request->raw_material_id;
        $search = NULL;
        $order_by = $_REQUEST['order']; // This array contains order information of clicked column
        $start = $_REQUEST['start']; // start limit of data
        $length = $_REQUEST['length']; // end limit of data
        $order_by_str = $order_by[0]['dir'];
        if ($order_by[0]['column']==0){
            $order_column = "raw_material.name";
        }
        if ($order_by[0]['column']==1){
            $order_column = "stock_raw_material.created_at";
        }
        if ($order_by[0]['column']==2){
            $order_column = "stock_raw_material.created_at";
        }
        if ($order_by[0]['column']==3){
            $order_column = "stock_raw_material.qty";
        }
        if (!empty($_REQUEST['search']['value'])) {
            $search = $_REQUEST['search']['value']; // This array contains search value information datatable
        }
        $where_query = "date(stock_raw_material.created_at) between '$start_date' and '$end_date' ";
        if ($raw_material_id != 'all')
        {
            $where_query .= "and raw_material.raw_material_id=$request->raw_material_id ";
        }
        if ($search!='' || $search!=NULL) {
            $where_query .= "and (raw_material.name like '%$search%' or stock_raw_material.created_at like '%$search%' or stock_raw_material.qty like '$search')";
        }
        $records = DB::table("stock_raw_material")
            ->join('raw_material','raw_material.raw_material_id','=','stock_raw_material.raw_material_id')
            ->select('raw_material.name','stock_raw_material.created_at','stock_raw_material.qty')
            ->whereRaw($where_query)
            ->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length)
            ->get();
        $records_count = DB::table("stock_raw_material")
            ->join('raw_material','raw_material.raw_material_id','=','stock_raw_material.raw_material_id')
            ->select(DB::raw('count(*) as all_count'))
            ->whereRaw($where_query)
            ->first();

        $data[][] = array();
        $i = 0;
        foreach ($records as $record)
        {
            $data[$i] = array(
                $record->name,
                $record->created_at,
                $record->created_at,
                $record->qty
            );
            $i++;
        }

        $totalRecords = $records_count->all_count;
        if ($totalRecords==0){
            $data = array();
        }
        $json_data = array(
            "draw"            => intval($_REQUEST['draw']),
            "recordsTotal"    => intval($totalRecords),
            "recordsFiltered" => intval($totalRecords),
            "data"            => $data   // total data array
        );
        $json_data = json_encode($json_data);
        return $json_data;
    }

    public function download(Request $request)
    {
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $raw_material_id = $request->raw_material_id;
        $where_query = "date(stock_raw_material.created_at) between '$start_date' and '$end_date' ";
        if ($raw_material_id != 'all')
        {
            $where_query .= "and raw_material.raw_material_id=$request->raw_material_id ";
        }
        $records = DB::table("stock_raw_material")
            ->join('raw_material','raw_material.raw_material_id','=','stock_raw_material.raw_material_id')
            ->select('raw_material.name','stock_raw_material.created_at','stock_raw_material.qty')
            ->whereRaw($where_query)
            ->get();

        $pdf = PDF::loadView('stock_usage_download', compact('records'))->setPaper('a4', 'portrait');
        return $pdf->download('STOCK_USAGE.pdf');

    }
}
