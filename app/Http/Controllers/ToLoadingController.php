<?php

namespace App\Http\Controllers;

use App\EndProductBurdenGrade;
use App\Traits\EndProductLog;
use Carbon\Carbon;
use EmployeeManager\Models\Employee;
use EndProductBurdenManager\Models\EndProductBurden;
use EndProductManager\Models\Category;
use EndProductManager\Models\EndProduct;
use Illuminate\Http\Request;
use StockManager\Classes\StockTransaction;

class ToLoadingController extends Controller
{
    use EndProductLog;
    function __construct()
    {
        $this->middleware('permission:end_product_add_store-index', ['only' => ['index','tableData']]);
        $this->middleware('permission:ProductAddStores-list', ['only' => ['list']]);
        $this->middleware('permission:BulkBurdenLoading-list', ['only' => ['blist']]);
        $this->middleware('permission:end_product_add_store-createNow', ['only' => ['createNow']]);

    }
    public function index()
    {
        $categories = Category::pluck('name', 'id');
        $end_products = EndProduct::whereHas('category', function ($q) {
            return $q->where('loading_type', '=', 'to_loading');
        })->pluck('name', 'id');
        return view('bulk_burden_loading', compact('categories', 'end_products'));
    }

    public function tableData(Request $request)
    {

        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;


        $recordsCount = 0;

        $date = $request->date_range;
        $columns = ['id'];
        $order_column = $columns[0];

        $records = EndProductBurden::tableData($order_column, 'ASC', $start, $length);

        $records = $records->whereHas('endProduct.category', function ($q) {
            return $q->where('loading_type', '=', 'to_loading');
        })
            ->whereStatus(5);


        if ($request->filter == true) {
            $category = $request->category;
            $end_product = $request->end_product;
            $records = $records->filterData($category, $end_product);
        }


        $records = $records->get();
        $recordsCount = (!empty($records)) ? sizeof($records) : 0;

        $data[][] = array();
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;


        foreach ($records as $key => $record) {

            $grades = EndProductBurdenGrade::whereBurdenId($record->id)->get();
            $gradeA = 0;
            $gradeB = 0;
            foreach ($grades as $g) {
                if ($g->grade_id == 1) {
                    $gradeA = $g->burden_qty;
                }
                if ($g->grade_id == 2) {
                    $gradeB = $g->burden_qty;
                }
            }

            $total= $gradeA+$gradeB;
            $data[$i] = array(
                ($request->end_product)?"<input type='checkbox' onclick='showTotal(this)' data-qty='$total' name='id[]' value='$record->id'/>":null,
                $record->endProduct->name,
                $record->serial,
                $record->expected_end_product_qty,
                $gradeA,
                $gradeB,
            );
            $i++;
        }


        if ($recordsCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($recordsCount),
            "recordsFiltered" => intval($recordsCount),
            "data" => $data
        ];

        return json_encode($json_data);
    }


    public function createNow(Request $request)
    {
        $burdens = $request->id;

        $burdens = EndProductBurden::whereIn('id', $burdens)->get();

        foreach ($burdens as $burden) {
            $burden->status = 6;
            $burden->save();
            $grades = EndProductBurdenGrade::whereBurdenId($burden->id)->get();
            $end_product = EndProduct::find($burden->end_product_id);
            foreach ($grades as $g) {
                if ($g->grade_id == 1) {
                    StockTransaction::endProductStockTransaction(1, $burden->end_product_id, 0, 0, 1, $g->burden_qty, 0, 1);
                    StockTransaction::updateEndProductAvailableQty(1, $burden->end_product_id,$g->burden_qty);
                    $end_product = EndProduct::find($burden->end_product_id);
                    $this->recordEndProductOutstanding($end_product,$g->grade_id, $g->burden_qty, 'Stores Addition-Grade A', $g->id, 'App\\EndProductBurdenGrade',0,10,1,$burden->id);

                }
                if ($g->grade_id == 2) {
                    StockTransaction::endProductStockTransaction(1, $burden->end_product_id, 0, 0, 1, $g->burden_qty, 0, 1);
                    StockTransaction::updateEndProductAvailableQty(1, $burden->end_product_id,$g->burden_qty);
                    $end_product = EndProduct::find($burden->end_product_id);
                    $this->recordEndProductOutstanding($end_product,$g->grade_id, $g->burden_qty, 'Stores Addition-Grade B', $g->id, 'App\\EndProductBurdenGrade',0,11,1,$burden->id);

                }
            }

        }

        return redirect(route('bulk_burden_loading.index'));
    }
}
