<?php

namespace App\Http\Controllers;

use App\EndProductInquiry;
use App\LoadingHeader;
use App\SalesReturnHeader;
use Carbon\Carbon;
use EndProductManager\Models\Category;
use EndProductManager\Models\EndProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use phpDocumentor\Reflection\Types\Null_;
use StockManager\Models\StockEndProduct;

class EndProductStockReport extends Controller
{
    function __construct()
    {
        $this->middleware('permission:end_product_stock-index', ['only' => ['index','tableData']]);
        $this->middleware('permission:EndProductUsage-list', ['only' => ['list']]);
        $this->middleware('permission:EndProductUsageReport-list', ['only' => ['rlist']]);
        $this->middleware('permission:EndProductStockReport-list', ['only' => ['rlist']]);

    }
    public function index()
    {
        $category = Category::all()->pluck('name', 'id');
        return view('esr', compact('category'));
    }

    public function tableData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];
        $columns = ['id', 'end_product_id', 'qty'];
        $order_column = $columns[$order_by[0]['column']];

        $records = EndProduct::query();

        $recordsCount = 0;

        $stst = null;

        $stat_data = EndProductInquiry::query();

        if ($request->filter == true) {
            $category = $request->category;
            $date = $request->date_range;


            $records = $records->whereCategoryId($category);
            $recordsCount = $records->count();
            $records = $records->tableData($order_column, $order_by_str, $start, $length)->get();
        } elseif (is_null($search) || empty($search)) {

        } else {

        }



        $data[][] = array();
        $i = 0;
        $total_price = 0;
        $total_qty = 0;
        $total_weight = 0;

        $total_qty = 0;
        $total_price = 0;
        $total_weight = 0;
        foreach ($records as $key => $record) {

            $btnShow = '<a href="' . url('end-inquiry/list' . '/' . $record->id . '/' . $date) . '"  target="_blank"  class="btn btn-sm btn-outline-dark mr-1"><i class="fa fa-eye"></i> </a>';
            $current_balance = $record->available_qty;

            $date = $request->date_range;
            $time_data_sum = EndProductInquiry::whereEndProductId($record->id)->where('created_at', 'like',$date.'%')->latest();
            $total_sum = EndProductInquiry::select(DB::raw('sum(qty) as qty_sum'),)->whereEndProductId($record->id)->where('created_at', 'like',$date.'%')->first();

            $total =!empty($time_data_sum->first())?$time_data_sum->first()->end_balance:0;

            $value = $record->distributor_price * $total;
            $tw =$record->weight * $total;

            $data[$i] = array(
                $record->id,
                $record->name,
                number_format($total, 3),
                number_format($tw, 3),
                number_format($value, 2),
                $btnShow,
                $current_balance,
                $time_data_sum
            );
            $total_qty = $total_qty + $total;
            $total_price = $total_price+$tw;
            $total_weight = $total_weight+$value;
            $i++;
        }


        if ($recordsCount == 0) {
            $data = [];
        }


        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($recordsCount),
            "recordsFiltered" => intval($recordsCount),
            "data" => $data,
            "qty" => number_format($total_qty, 3),
            "weight" => number_format($total_weight, 3),
            "price" => number_format($total_price, 2)
        ];

        return json_encode($json_data);
    }

    public function exportFilter(Request $request)
    {

        $campaign = $request->campaign;
        $records = Record::CampaignUtilizationReportsData($campaign);
        return Excel::download(new CampaignUtilizationReportExport($records), $this->reportFileName('campaign_utilization', '.xlsx', '', 'report'));
    }
}
