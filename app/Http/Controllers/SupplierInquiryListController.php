<?php
/**
 * Created by PhpStorm.
 * User: yaish
 * Date: 12/22/2020
 * Time: 5:26 PM
 */

namespace App\Http\Controllers;

use App\SupplierInquiry;
use BurdenManager\Models\Burden;
use Illuminate\Http\Request;
use PurchasingOrderManager\Models\PurchasingOrder;
use SupplierManager\Models\Supplier;

class SupplierInquiryListController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:supplier_inquiry_list-index', ['only' => ['index','tableData']]);
        $this->middleware('permission:SupplierInquiry-list', ['only' => ['list']]);

    }
    public function index()
    {
        $suppliers = Supplier::all()->pluck('supplier_name', 'id');
        return view('supplier_inquiry_list', compact('suppliers'));
    }

    public function tableData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'created_at'];
        $order_column = $columns[$order_by[0]['column']];

        $suppliers = SupplierInquiry::tableData($order_column, $order_by_str, $start, $length);

        // $salesOrdersCount = SalesOrder::whereStatus(1)->orderType()->whereConfirmed(1)->count();
        $suppliersCount =0;
        if ($request->filter == true) {
            $supplier_id = $request->supplier;
            $date_range = $request->date_range;
            $reference_type = $request->reference_type;
            $suppliers = $suppliers->filterData($supplier_id,$reference_type, $date_range)->get();
            $suppliersCount = $suppliers->count();
        } elseif (is_null($search) || empty($search)) {
//            $rawmaterials = $rawmaterials->get();
//            $rawmaterialsCount = RawInquiry::count();
        } else {
            $suppliers = $suppliers->searchData($search)->get();
            $suppliersCount = $suppliers->count();
        }

        $data[][] = array();
        $i = 0;


        foreach ($suppliers as $key => $supplier) {
            $referenceBtn ='';
            $referenceLinkBtn ='';
            switch ($supplier->reference_type) {
                case 'App\PurchasingOrder':
                    $referenceBtn ='Purchasing Order';
                    $id = PurchasingOrder::where('id',$supplier->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">'.$id['invoice_no'].'</a>';
                    break;

                case 'PurchasingOrderManager\Models\PurchasingOrder':
                    $referenceBtn = 'Purchasing Order';
                    $id = PurchasingOrder::where('id',$supplier->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">'.$id['invoice_no'].'</a>';
                    break;

                case 'App\Burden':
                    $referenceBtn = 'Burden';
                    $id = Burden::where('id',$supplier->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">'.$id['serial'].'</a>';
                    break;

            }
            $approving_orders = SupplierInquiry::get()->first();

            $approving_filter = !empty($approving_orders) ? 1 : 0;

            $data[$i] = array(
                $supplier->id,
                $supplier->supplier->supplier_name,
                number_format($supplier->start_balance,3),
                number_format($supplier->qty,3),
                number_format($supplier->end_balance,3),
                number_format($supplier->price,2),
                number_format($supplier->amount,2),
                $supplier->description,
                $referenceBtn,
                $referenceLinkBtn,
            );
            $i++;
        }

        if ($suppliersCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($suppliersCount),
            "recordsFiltered" => intval($suppliersCount),
            "data" => $data
        ];

        return json_encode($json_data);
    }
}