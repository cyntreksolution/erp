<?php

namespace App\Http\Controllers;

use App\LoadingData;
use EndProductBurdenManager\Models\EndProductBurden;
use EndProductBurdenManager\Models\EndProductBurdenHeader;
use EndProductManager\Models\EndProduct;
use Illuminate\Http\Request;

class MergeVsLoadedController extends Controller
{
    public function getItems(Request $request)
    {
        $merge_id = $request->merge_id;
        $loading_id = $request->loading_id;

        $merge_headers = EndProductBurdenHeader::whereIn('id', $merge_id)->pluck('burdens');

        $burden_end_product = collect();

        foreach ($merge_headers as $header) {
            $burdens = EndProductBurden::whereIn('id', json_decode($header))->get();
            $burden_end_product->push($burdens);
        }

        $loading_items = LoadingData::whereIn('loading_header_id', $loading_id)->get();

        $end_products = EndProduct::whereHas('category', function ($q) {
            return $q->whereLoadingType('to_burden');
        })->get();

        $end_list = collect();
        $burden_list = collect();
        $loading_list = collect();
        $row =collect();

        foreach ($end_products as $key => $end_product) {
            $end = ['id'=>$end_product->id,'name'=>$end_product->name,'qty'=>$end_product->available_qty];
            $end_list->push($end);

            $load_qty =0;
            $data_l = collect();

            foreach ($loading_items as $item){
                if ($end_product->id === $item->end_product_id) {
                    $load_qty = $load_qty + $item->admin_approved_qty;
                    $data_l->push(['invoice_id'=>$item->header->invoice_number,'qty'=>$item->admin_approved_qty]);
                }
            }

            $load = ['id'=>$end_product->id,'name'=>$end_product->name,'qty'=>$load_qty,'l_data'=>$data_l];
            $loading_list->push($load);

            $b_qty =0;
            $data_m = collect();
            foreach ($burden_end_product->flatten() as $item) {
                if ($end_product->id === $item->end_product_id) {
                    $b_qty = $b_qty + $item->burden_size;
                    $data_m->push(['burden_serial'=>$item->serial,'qty'=>$item->burden_size]);
                }
            }

            $bur = ['id'=>$end_product->id,'name'=>$end_product->name,'qty'=>$b_qty,'m_data'=>$data_m];
            $burden_list->push($bur);

            $row->push([
                'id'=>$end_product->id,
                'name'=>$end_product->name,
                'burden_data'=>$data_m,
                'loading_data'=>$data_l,
                'burden_total'=>$b_qty,
                'loading_total'=>$load_qty,
                ]);
        }


        return  view('reports.merged-sr',compact('row'));
        return $request->all();
    }
}
