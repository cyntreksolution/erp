<?php

namespace App\Http\Controllers;

use App\SalesMergeCookingRequest;
use App\SemiBurdenIssueData;
use App\SemiInquiry;
use App\SemiIssueDataList;
use BurdenManager\Models\Burden;
use BurdenManager\Models\BurdenGrade;
use Carbon\Carbon;
use EndProductBurdenManager\Models\BurdenSemi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use RawMaterialManager\Models\RawMaterial;
use SemiFinishProductManager\Models\SemiFinishProduct;
use ShortEatsManager\Models\CookingRequest;
use function Composer\Autoload\includeFile;

class SemiInquiryListController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:semi_inquiry_list-index', ['only' => ['index','tableData']]);
        $this->middleware('permission:SemiFinishUsage-list', ['only' => ['list']]);

    }
    public function index()
    {
        $semi_products = SemiFinishProduct::all()->pluck('name', 'semi_finish_product_id');

        return view('semi_inquiry_list', compact('semi_products'));
    }

    public function tableData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];
        $columns = ['id', 'created_at'];
        $order_column = $columns[$order_by[0]['column']];
        $semiproducts = SemiInquiry::query();
        $stat_data = SemiInquiry::query();
        $semiproductsCount = 0;

      //  $records = EndProduct::query();
        $sumQty = null;
        $sumAmount = null;
        $openningBalanceStart = null;
//********************************************************************
        if ($request->filter == true) {


             $referenceBtn = null;

            $semi_product = $request->semi_product;
            $date_range = $request->date_range;
            $meta = $request->meta;
            $semi_type = $request->semi_type;

            $allSum = $stat_data->filterData($semi_product, $meta,$semi_type, $date_range)
                ->select(DB::raw('SUM(amount) as  amount_sum'), DB::raw('SUM(qty) as qty_sum'))
                ->first();

            $semiproductsCount = SemiInquiry::filterData($semi_product, $meta,$semi_type, $date_range)->count();
            $semiproducts = SemiInquiry::filterData($semi_product, $meta,$semi_type, $date_range)
                ->tableData($order_column, $order_by_str, $start, $length)
                ->get();


             if (!empty($allSum['amount_sum'])) {
                 $sumAmount = $allSum['amount_sum'];
             } else {
                 $sumAmount = null;
             }
             if (!empty($allSum['qty_sum'])) {
                 $sumQty = $allSum['qty_sum'];
             } else {
                 $sumQty = null;
             }
             $data[][] = array();
             $i = 0;



             if ((!empty($date_range)) && (!empty($semi_product)) && (empty($meta)) && (empty($semi_type))) {
                 $openningBalance = $semiproducts->first();
                 if (!empty($openningBalance)) {
                     $openningBalanceStart = $openningBalance['start_balance'];
                 } else {
                     $openningBalanceStart = null;
                 }
             } else {
                 $openningBalanceStart = null;
             }

             foreach ($semiproducts as $key => $semiproduct) {

                 $referenceBtn = $semiproduct->meta;
                 if ((!empty($date_range)) && (!empty($semi_product)) && (empty($meta)) && (empty($semi_type))) {
                     $startBal = number_format($semiproduct->start_balance,3);
                     $endBalance = number_format($semiproduct->end_balance, 3);
                 } else {
                     $startBal = null;
                     $endBalance = null;
                 }

                 $burden_price = 0;
                 $burden_qty = 0;
                 $unit_price = 0;



                 if (Sentinel::check()->roles[0]->slug=='owner') {
                     $semiamo = $semiproduct->amount;
                 }else{
                     $semiamo = null;
                 }

                 $serial = null;
                 $chk = '';

                 if($semiproduct->meta != 'AdminAdjustments'){
                     if ($semiproduct->semi_type == 'cooking_request') {
                         $bid = CookingRequest::find($semiproduct->burden_id);
                     }else{
                         $bid = Burden::find($semiproduct->burden_id);
                     }

                     $serial = $bid->serial;

                     if($semiproduct->is_checked == 1){

                         $chk = 'Checked Here - ['. $semiproduct->check_ref . ']';
                     }


                 }



                 $data[$i] = array(
                     Carbon::parse($semiproduct->created_at)->format('Y-m-d H:i:s'),
                     $semiproduct->semi_finish_product->name,
                     $referenceBtn.'-'.$this->semiDataDescription($semiproduct->description),
                     $startBal,
                     $semiproduct->qty,
                     $semiamo,
                     $endBalance,
                     $serial.$chk
                 );
                 $i++;
             }



        } elseif (is_null($search) || empty($search)) {

        } else {

         }


        if ($semiproductsCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($semiproductsCount),
            "recordsFiltered" => intval($length),
            "data" => $data,
            "openning_balance" => number_format($openningBalanceStart, 3),
            "qty_sum" => number_format($sumQty, 3),
            "amount_sum" => number_format($sumAmount, 2)

        ];

        return json_encode($json_data);
    }

    function semiDataDescription($desc)
    {
        $description = '';
        switch ($desc) {

            case 'Burden Grade 1':
                $description = 'Grade A';
                break;
            case 'Burden Grade 2':
                $description = 'Grade B';
                break;
            case 'Burden Grade 3':
                $description = 'Grade C';
                break;
            case 'Burden Grade 4':
                $description = 'Grade D';
                break;
            default:
                $description = $desc;
        }

        return $description;
    }
}
