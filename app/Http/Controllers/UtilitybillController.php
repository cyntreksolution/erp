<?php
namespace App\Http\Controllers;


use App\UtilityBill;
use Carbon\Carbon;
use CashLockerManager\Models\CashBookTransaction;
use CashLockerManager\Models\CashLocker;
use CashLockerManager\Models\main_desc_cashlocker;

use EmployeeManager\Models\Employee;
use Illuminate\Http\Request;

class UtilitybillController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:utility_bill-show', ['only' => ['show']]);
        $this->middleware('permission:AddUtilityBill-list', ['only' => ['list']]);
        $this->middleware('permission:utility_bill-create', ['only' => ['create']]);

    }
    public function show()
    {

        $data = UtilityBill::all();
        return view('utilitybill')->with('datas',$data);

    }

    public function create(Request $req)
    {
        $main = new UtilityBill();

        $main->bill_no = $req->billNo;
        $main->description = $req->udesc;

        $main->save();

        $data = UtilityBill::all();
        return view('utilitybill')->with('datas',$data);

    }

    public function billupdate(Request $req)
    {

        $id = $req->id;
        $bn = $req->billNo;
        $desc = $req->udesc;

        $main = UtilityBill::find($id);
        $main->bill_no = $bn;
        $main->description = $desc;

        $main->save();

        $data = UtilityBill::all();
        return view('utilitybill')->with('datas',$data);

    }

}
