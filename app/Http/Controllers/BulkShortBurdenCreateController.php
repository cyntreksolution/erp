<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use EmployeeManager\Models\Group;
use Illuminate\Http\Request;
use ShortEatsManager\Models\CookingRequest;
use ShortEatsManager\Models\CookingRequestGroupEmployee;

class BulkShortBurdenCreateController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:bulk_short_burden-index', ['only' => ['index','tableData']]);
        $this->middleware('permission:FillingBulkBurdenMake-list', ['only' => ['list']]);
        $this->middleware('permission:bulk_short_burden-create', ['only' => ['createNow']]);

    }
    public function index()
    {
        $teams = Group::whereDate('created_at', Carbon::today())->get();
        return view('bulk_burden__cooking_create',compact('teams'));
    }

    public function tableData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $recordsCount = 0;

        $date = $request->date_range;
        $columns = ['semi_finish_product_id', 'semi_finish_product_id', 'qty'];
        $order_column = $columns[$order_by[0]['column']];

        $records = CookingRequest::tableData($order_column, $order_by_str, $start, $length);

        $records = $records->whereStatus(2)->get();
//
//            $records = $records->get();
        $recordsCount = (!empty($records)) ? sizeof($records) : 0;

        $data[][] = array();
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;


        foreach ($records as $key => $record) {
            $data[$i] = array(
                "<input type='checkbox' name='id[]' value='$record->id'/>",
                $record->semiProdcuts->name,
                $record->serial,
                $record->burden_size,
                $record->expected_semi_product_qty,
            );
            $i++;
        }


        if ($recordsCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($recordsCount),
            "recordsFiltered" => intval($recordsCount),
            "data" => $data
        ];

        return json_encode($json_data);
    }

    public function createNow(Request $request){
        $burdens = $request->id;
        $team = $request->team;

        $burdens = CookingRequest::whereIn('id',$burdens)->get();

        foreach ($burdens as $burden){
            //$burden->employee_group = $request->employee_group;
            $burden->employee_group = $team;
            $burden->created_start_time = Carbon::now();
            $burden->stage_1 = 1;
            $burden->status = 3;
            $burden->save();

            $t = new CookingRequestGroupEmployee();
            $t->cooking_request_id = $burden->id;
            $t->group_id = $team;
            $t->save();
        }

        return redirect(route('semi_bulk_burden.index'));
    }
}
