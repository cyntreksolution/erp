<?php

namespace App\Http\Controllers;

use App\AgentBuffer;
use App\CrateInquiryAgent;
use App\CrateInquiryStore;
use App\EndProductInquiry;
use App\LoadingHeader;
use App\User;
use Carbon\Carbon;
use EndProductManager\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use RawMaterialManager\Models\RawMaterial;
use SalesOrderManager\Models\SalesOrder;
use SalesRepManager\Models\SalesRep;

class CratesInquiryAgentController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:crates_inquiry_agent-index', ['only' => ['index','tableData']]);
        $this->middleware('permission:CratesInquiryAgent-list', ['only' => ['list']]);
        $this->middleware('permission:CratesInquiryAgentReport-list', ['only' => ['rlist']]);

    }
    public function index(){
        $agents = SalesRep::all()->pluck('name_with_initials', 'id');
        $raw_materials = RawMaterial::where('type' , '=' , 5)->get()->pluck('name', 'raw_material_id');
        return view('crate_usage_list_agent',compact('raw_materials','agents'));
    }



    public function tableData(Request $request)
    {

        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'created_at'];
        $order_column = $columns[$order_by[0]['column']];

        $query = CrateInquiryAgent::select('users.name_with_initials', 'crates_inquiry_agents.*')->leftJoin('users', 'users.id', 'crates_inquiry_agents.agent_id');

        $count = 0;
        $records = collect();
        $balance_records = collect();

        $data = [];
        $i = 0;
        $reference_type = null;

        if ($request->filter == true) {
            $agent_id = $request->agent;
            $crate_id = null;// for future use
            $date_range = $request->date_range;
            $reference_type = $request->reference_type;



            $dates = explode(' - ', $date_range);
            $start = Carbon::parse($dates[0])->format('Y-m-d 00:00:00');
            $end = Carbon::parse($dates[1])->format('Y-m-d 23:59:59');
            //  $start = Carbon::parse($dates[0])->format('Y-m-d');
            //  $end = Carbon::parse($dates[1])->format('Y-m-d');
            $whereQuery = "where t1.created_at >='$start' and t1.created_at <= '$end'";

            if (!empty($agent_id)) {
                $whereQuery .= " and t1.agent_id = $agent_id";
            }
            if (!empty($reference_type)) {
                $whereQuery .= " and t1.desc_id = $reference_type";
            }


            $query = "with t1 as (SELECT crates_inquiry_agents.reference,crates_inquiry_agents.created_at,crates_inquiry_agents.agent_id, crates_inquiry_agents.desc_id,crates_inquiry_agents.description,
CASE WHEN crates_inquiry_agents.crate_id = '193' THEN sum(crates_inquiry_agents.qty) END 'bcl',
CASE WHEN crates_inquiry_agents.crate_id = '194' THEN sum(crates_inquiry_agents.qty) END 'bcs',
CASE WHEN crates_inquiry_agents.crate_id = '195' THEN sum(crates_inquiry_agents.qty) END 'crl',
CASE WHEN crates_inquiry_agents.crate_id = '196' THEN sum(crates_inquiry_agents.qty) END 'crs',
CASE WHEN crates_inquiry_agents.crate_id = '197' THEN sum(crates_inquiry_agents.qty) END 'lid'
FROM crates_inquiry_agents
GROUP BY DATE_FORMAT(crates_inquiry_agents.created_at,'%Y-%m-%d %H:%i'),crates_inquiry_agents.agent_id, crates_inquiry_agents.crate_id, crates_inquiry_agents.desc_id 
ORDER BY 'crates_inquiry_agents.created_at' DESC)
SELECT loading_headers.invoice_number,t1.created_at,users.name_with_initials,t1.description,sum(t1.bcl) as bcl,sum(t1.bcs) as bcs,sum(t1.crl) as crl,sum(t1.crs) as crs,sum(t1.lid) as lid 
FROM  t1  LEFT JOIN users ON t1.agent_id = users.id 
LEFT JOIN loading_headers on t1.reference = loading_headers.id
$whereQuery GROUP BY DATE_FORMAT(t1.created_at,'%Y-%m-%d %H:%i'),t1.agent_id";

            $records = collect(DB::select(DB::raw($query)));

            // dd($records);

            $start_b = Carbon::parse($dates[0])->format('Y-m-d');
            $end_b = Carbon::parse($dates[1])->format('Y-m-d');


            $balance_query = "select (SELECT end_balance from crates_inquiry_agents where created_at < '$start_b' and crate_id = 193 order by created_at desc LIMIT 1 ) as 'bcl_st',
(SELECT end_balance from crates_inquiry_agents where created_at <  '$start_b' and crate_id = 194 order by created_at desc LIMIT 1 ) as 'bcs_st',
(SELECT end_balance from crates_inquiry_agents where created_at < '$start_b' and crate_id = 195 order by created_at desc LIMIT 1 ) as 'crl_st',
(SELECT end_balance from crates_inquiry_agents where created_at < '$start_b' and crate_id = 196 order by created_at desc LIMIT 1 ) as 'crs_st',
(SELECT end_balance from crates_inquiry_agents where created_at < '$start_b' and crate_id = 197 order by created_at desc LIMIT 1 ) as 'lid_st',
(SELECT start_balance from crates_inquiry_agents where created_at = '$start_b' and crate_id = 193 order by created_at asc LIMIT 1 ) as 'bcl_st1',
(SELECT start_balance from crates_inquiry_agents where created_at =  '$start_b' and crate_id = 194 order by created_at asc LIMIT 1 ) as 'bcs_st1',
(SELECT start_balance from crates_inquiry_agents where created_at = '$start_b' and crate_id = 195 order by created_at asc LIMIT 1 ) as 'crl_st1',
(SELECT start_balance from crates_inquiry_agents where created_at = '$start_b' and crate_id = 196 order by created_at asc LIMIT 1 ) as 'crs_st1',
(SELECT start_balance from crates_inquiry_agents where created_at = '$start_b' and crate_id = 197 order by created_at asc LIMIT 1 ) as 'lid_st1',
(SELECT end_balance from crates_inquiry_agents where created_at <= '$end_b%' and crate_id = 193 order by created_at desc LIMIT 1 ) as 'bcl_en',
(SELECT end_balance from crates_inquiry_agents where created_at <= '$end_b%' and crate_id = 194 order by created_at desc LIMIT 1 ) as 'bcs_en',
(SELECT end_balance from crates_inquiry_agents where created_at <= '$end_b%' and crate_id = 195 order by created_at desc LIMIT 1 ) as 'crl_en',
(SELECT end_balance from crates_inquiry_agents where created_at <= '$end_b%' and crate_id = 196 order by created_at desc LIMIT 1 ) as 'crs_en',
(SELECT end_balance from crates_inquiry_agents where created_at <= '$end_b%' and crate_id = 197 order by created_at desc LIMIT 1 ) as 'lid_en'";

            $balance_records = collect(DB::select(DB::raw($balance_query)))->first();


            if ($reference_type==100 && !empty($start)) {

                $start_b1 = Carbon::parse($start)->format('Y-m-d');

                if(!empty($agent_id)){
                    $agent_query = "select (SELECT end_balance from crates_inquiry_agents where created_at < '$start_b1' and crate_id = 193 and agent_id = '$agent_id' order by created_at desc LIMIT 1 ) as 'bcl_ag',
(SELECT end_balance from crates_inquiry_agents where created_at < '$start_b1' and crate_id = 194 and agent_id = '$agent_id' order by created_at desc LIMIT 1 ) as 'bcs_ag',
(SELECT end_balance from crates_inquiry_agents where created_at < '$start_b1' and crate_id = 195 and agent_id = '$agent_id' order by created_at desc LIMIT 1 ) as 'crl_ag',
(SELECT end_balance from crates_inquiry_agents where created_at < '$start_b1' and crate_id = 196 and agent_id = '$agent_id' order by created_at desc LIMIT 1 ) as 'crs_ag',
(SELECT end_balance from crates_inquiry_agents where created_at < '$start_b1' and crate_id = 197 and agent_id = '$agent_id' order by created_at desc LIMIT 1 ) as 'lid_ag',
(SELECT start_balance from crates_inquiry_agents where created_at = '$start_b1' and crate_id = 193 and agent_id = '$agent_id' order by created_at asc LIMIT 1 ) as 'bcl_ag1',
(SELECT start_balance from crates_inquiry_agents where created_at = '$start_b1' and crate_id = 194 and agent_id = '$agent_id' order by created_at asc LIMIT 1 ) as 'bcs_ag1',
(SELECT start_balance from crates_inquiry_agents where created_at = '$start_b1' and crate_id = 195 and agent_id = '$agent_id' order by created_at asc LIMIT 1 ) as 'crl_ag1',
(SELECT start_balance from crates_inquiry_agents where created_at = '$start_b1' and crate_id = 196 and agent_id = '$agent_id' order by created_at asc LIMIT 1 ) as 'crs_ag1',
(SELECT start_balance from crates_inquiry_agents where created_at = '$start_b1' and crate_id = 197 and agent_id = '$agent_id' order by created_at asc LIMIT 1 ) as 'lid_ag1'";

                    $agent_records = collect(DB::select(DB::raw($agent_query)))->first();



                    if(!empty($agent_records)) {


                        if(!empty($agent_records->bcl_ag)) {
                            $ag_st_bcl = ($agent_records->bcl_ag1) ? $agent_records->bcl_ag1 : $agent_records->bcl_ag;

                        }else{
                            $ag_st_bcl = 0;
                        }
                        if(!empty($agent_records->bcs_ag)) {
                            $ag_st_bcs = ($agent_records->bcs_ag1)?$agent_records->bcs_ag1 : $agent_records->bcs_ag ;
                        }else{
                            $ag_st_bcs = 0;
                        }
                        if(!empty($agent_records->crl_ag)) {
                            $ag_st_crl = ($agent_records->crl_ag1)?$agent_records->crl_ag1 : $agent_records->crl_ag ;
                        }else{
                            $ag_st_crl = 0;
                        }
                        if(!empty($agent_records->crs_ag)) {
                            $ag_st_crs = ($agent_records->crs_ag1)?$agent_records->crs_ag1 : $agent_records->crs_ag ;
                        }else{
                            $ag_st_crs = 0;
                        }
                        if(!empty($agent_records->lid_ag)) {
                            $ag_st_lid = ($agent_records->lid_ag1)?$agent_records->lid_ag1 : $agent_records->lid_ag ;
                        }else{
                            $ag_st_lid = 0;
                        }



                        $ag_name = SalesRep::whereId($agent_id)->first();

                        $data[0] = array(
                            Carbon::parse($start_b1)->format('Y-M-d'),
                            $ag_name->name_with_initials,
                            ($ag_st_bcl),
                            ($ag_st_bcs),
                            ($ag_st_crl),
                            ($ag_st_crs),
                            ($ag_st_lid),
                            'Opening Balance',

                        );


                    }

                }else{
                    $agents = SalesRep::OrderBy('name_with_initials')->get()->pluck('name_with_initials', 'id');

                    $tag_st_bcl=0;
                    $tag_st_bcs=0;
                    $tag_st_crl=0;
                    $tag_st_crs=0;
                    $tag_st_lid=0;

                    foreach ($agents as $key=> $agent) {

                        $agent_query = "select (SELECT end_balance from crates_inquiry_agents where created_at < '$start_b1' and crate_id = 193 and agent_id = '$key' order by created_at desc LIMIT 1 ) as 'bcl_ag',
(SELECT end_balance from crates_inquiry_agents where created_at < '$start_b1' and crate_id = 194 and agent_id = '$key' order by created_at desc LIMIT 1 ) as 'bcs_ag',
(SELECT end_balance from crates_inquiry_agents where created_at < '$start_b1' and crate_id = 195 and agent_id = '$key' order by created_at desc LIMIT 1 ) as 'crl_ag',
(SELECT end_balance from crates_inquiry_agents where created_at < '$start_b1' and crate_id = 196 and agent_id = '$key' order by created_at desc LIMIT 1 ) as 'crs_ag',
(SELECT end_balance from crates_inquiry_agents where created_at < '$start_b1' and crate_id = 197 and agent_id = '$key' order by created_at desc LIMIT 1 ) as 'lid_ag',
(SELECT start_balance from crates_inquiry_agents where created_at = '$start_b1' and crate_id = 193 and agent_id = '$key' order by created_at asc LIMIT 1 ) as 'bcl_ag1',
(SELECT start_balance from crates_inquiry_agents where created_at = '$start_b1' and crate_id = 194 and agent_id = '$key' order by created_at asc LIMIT 1 ) as 'bcs_ag1',
(SELECT start_balance from crates_inquiry_agents where created_at = '$start_b1' and crate_id = 195 and agent_id = '$key' order by created_at asc LIMIT 1 ) as 'crl_ag1',
(SELECT start_balance from crates_inquiry_agents where created_at = '$start_b1' and crate_id = 196 and agent_id = '$key' order by created_at asc LIMIT 1 ) as 'crs_ag1',
(SELECT start_balance from crates_inquiry_agents where created_at = '$start_b1' and crate_id = 197 and agent_id = '$key' order by created_at asc LIMIT 1 ) as 'lid_ag1'";

                        $agent_records = collect(DB::select(DB::raw($agent_query)))->first();


                        if (!empty($agent_records)) {


                            if (!empty($agent_records->bcl_ag)) {
                                $ag_st_bcl = ($agent_records->bcl_ag1) ? $agent_records->bcl_ag1 : $agent_records->bcl_ag;
                            } else {
                                $ag_st_bcl = 0;
                            }
                            if (!empty($agent_records->bcs_ag)) {
                                $ag_st_bcs = ($agent_records->bcs_ag1) ? $agent_records->bcs_ag1 : $agent_records->bcs_ag;
                            } else {
                                $ag_st_bcs = 0;
                            }
                            if (!empty($agent_records->crl_ag)) {
                                $ag_st_crl = ($agent_records->crl_ag1) ? $agent_records->crl_ag1 : $agent_records->crl_ag;
                            } else {
                                $ag_st_crl = 0;
                            }
                            if (!empty($agent_records->crs_ag)) {
                                $ag_st_crs = ($agent_records->crs_ag1) ? $agent_records->crs_ag1 : $agent_records->crs_ag;
                            } else {
                                $ag_st_crs = 0;
                            }
                            if (!empty($agent_records->lid_ag)) {
                                $ag_st_lid = ($agent_records->lid_ag1) ? $agent_records->lid_ag1 : $agent_records->lid_ag;
                            } else {
                                $ag_st_lid = 0;
                            }

                            $tag_st_bcl=$tag_st_bcl+$ag_st_bcl;
                            $tag_st_bcs=$tag_st_bcs+$ag_st_bcs;
                            $tag_st_crl=$tag_st_crl+$ag_st_crl;
                            $tag_st_crs=$tag_st_crs+$ag_st_crs;
                            $tag_st_lid=$tag_st_lid+$ag_st_lid;

                            $data[$i] = array(
                                Carbon::parse($start_b1)->format('Y-M-d'),
                                $agent,
                                ($ag_st_bcl),
                                ($ag_st_bcs),
                                ($ag_st_crl),
                                ($ag_st_crs),
                                ($ag_st_lid),
                                'Opening Balance',
                            );
                        }
                        $i++;
                    }
                    $data[$i] = array(
                        'Total',
                        '',
                        $tag_st_bcl,
                        $tag_st_bcs,
                        $tag_st_crl,
                        $tag_st_crs,
                        $tag_st_lid,
                        'Opening Balance',
                    );

                }




            }

            if ($reference_type==200 && !empty($end)) {

                $end_b1 = Carbon::parse($end)->format('Y-m-d');

                if(!empty($agent_id)){
                    $agent_query = "select (SELECT end_balance from crates_inquiry_agents where created_at <= '$end_b1' and crate_id = 193 and agent_id = '$agent_id' order by created_at desc LIMIT 1 ) as 'bcl_ag',
(SELECT end_balance from crates_inquiry_agents where created_at <= '$end_b1' and crate_id = 194 and agent_id = '$agent_id' order by created_at desc LIMIT 1 ) as 'bcs_ag',
(SELECT end_balance from crates_inquiry_agents where created_at <= '$end_b1' and crate_id = 195 and agent_id = '$agent_id' order by created_at desc LIMIT 1 ) as 'crl_ag',
(SELECT end_balance from crates_inquiry_agents where created_at <= '$end_b1' and crate_id = 196 and agent_id = '$agent_id' order by created_at desc LIMIT 1 ) as 'crs_ag',
(SELECT end_balance from crates_inquiry_agents where created_at <= '$end_b1' and crate_id = 197 and agent_id = '$agent_id' order by created_at desc LIMIT 1 ) as 'lid_ag'";

                    $agent_records = collect(DB::select(DB::raw($agent_query)))->first();



                    if(!empty($agent_records)) {


                        if(!empty($agent_records->bcl_ag)) {
                            $ag_st_bcl = $agent_records->bcl_ag;

                        }else{
                            $ag_st_bcl = 0;
                        }
                        if(!empty($agent_records->bcs_ag)) {
                            $ag_st_bcs = $agent_records->bcs_ag ;
                        }else{
                            $ag_st_bcs = 0;
                        }
                        if(!empty($agent_records->crl_ag)) {
                            $ag_st_crl = $agent_records->crl_ag ;
                        }else{
                            $ag_st_crl = 0;
                        }
                        if(!empty($agent_records->crs_ag)) {
                            $ag_st_crs = $agent_records->crs_ag ;
                        }else{
                            $ag_st_crs = 0;
                        }
                        if(!empty($agent_records->lid_ag)) {
                            $ag_st_lid = $agent_records->lid_ag ;
                        }else{
                            $ag_st_lid = 0;
                        }



                        $ag_name = SalesRep::whereId($agent_id)->first();

                        $data[0] = array(
                            Carbon::parse($end_b1)->format('Y-M-d'),
                            $ag_name->name_with_initials,
                            ($ag_st_bcl),
                            ($ag_st_bcs),
                            ($ag_st_crl),
                            ($ag_st_crs),
                            ($ag_st_lid),
                            'Balance Crates In Hand',

                        );


                    }

                }else{
                    $agents = SalesRep::OrderBy('name_with_initials')->get()->pluck('name_with_initials', 'id');

                    $tag_st_bcl=0;
                    $tag_st_bcs=0;
                    $tag_st_crl=0;
                    $tag_st_crs=0;
                    $tag_st_lid=0;

                    foreach ($agents as $key=> $agent) {

                        $agent_query = "select (SELECT end_balance from crates_inquiry_agents where created_at <= '$end_b1' and crate_id = 193 and agent_id = '$key' order by created_at desc LIMIT 1 ) as 'bcl_ag',
(SELECT end_balance from crates_inquiry_agents where created_at <= '$end_b1' and crate_id = 194 and agent_id = '$key' order by created_at desc LIMIT 1 ) as 'bcs_ag',
(SELECT end_balance from crates_inquiry_agents where created_at <= '$end_b1' and crate_id = 195 and agent_id = '$key' order by created_at desc LIMIT 1 ) as 'crl_ag',
(SELECT end_balance from crates_inquiry_agents where created_at <= '$end_b1' and crate_id = 196 and agent_id = '$key' order by created_at desc LIMIT 1 ) as 'crs_ag',
(SELECT end_balance from crates_inquiry_agents where created_at <= '$end_b1' and crate_id = 197 and agent_id = '$key' order by created_at desc LIMIT 1 ) as 'lid_ag'";

                        $agent_records = collect(DB::select(DB::raw($agent_query)))->first();


                        if (!empty($agent_records)) {


                            if (!empty($agent_records->bcl_ag)) {
                                $ag_st_bcl = $agent_records->bcl_ag;
                            } else {
                                $ag_st_bcl = 0;
                            }
                            if (!empty($agent_records->bcs_ag)) {
                                $ag_st_bcs = $agent_records->bcs_ag;
                            } else {
                                $ag_st_bcs = 0;
                            }
                            if (!empty($agent_records->crl_ag)) {
                                $ag_st_crl = $agent_records->crl_ag;
                            } else {
                                $ag_st_crl = 0;
                            }
                            if (!empty($agent_records->crs_ag)) {
                                $ag_st_crs = $agent_records->crs_ag;
                            } else {
                                $ag_st_crs = 0;
                            }
                            if (!empty($agent_records->lid_ag)) {
                                $ag_st_lid = $agent_records->lid_ag;
                            } else {
                                $ag_st_lid = 0;
                            }

                            $tag_st_bcl=$tag_st_bcl+$ag_st_bcl;
                            $tag_st_crl=$tag_st_crl+$ag_st_crl;
                            $tag_st_crs=$tag_st_crs+$ag_st_crs;
                            $tag_st_lid=$tag_st_lid+$ag_st_lid;

                            $data[$i] = array(
                                Carbon::parse($end_b1)->format('Y-M-d'),
                                $agent,
                                ($ag_st_bcl),
                                ($ag_st_bcs),
                                ($ag_st_crl),
                                ($ag_st_crs),
                                ($ag_st_lid),
                                'Balance Crates In Hand',
                            );
                        }
                        $i++;
                    }
                    $data[$i] = array(
                        'Total',
                        '',
                        $tag_st_bcl,
                        $tag_st_bcs,
                        $tag_st_crl,
                        $tag_st_crs,
                        $tag_st_lid,
                        'Balance Crates In Hand',

                    );

                }
            }
      }



        $count = $records->count();

        $bcl = 0; $bcs = 0; $crl = 0; $crs = 0; $lid = 0;



        if (empty($agent_id) && empty($reference_type)) {
            /*if (!empty($balance_records->bcl_st1)) {

                $data[0] = array(
                    'Start Balance',
                    '',
                    '',
                    $balance_records->bcl_st1,
                    $balance_records->bcs_st1,
                    $balance_records->crl_st1,
                    $balance_records->crs_st1,
                    $balance_records->lid_st1,

                );



            }else if(!empty($balance_records->bcl_st)){

                $data[0] = array(
                    'Start Balance',
                    '',
                    '',
                    $balance_records->bcl_st,
                    $balance_records->bcs_st,
                    $balance_records->crl_st,
                    $balance_records->crs_st,
                    $balance_records->lid_st,

                );

            }
            $i = 1;*/

        }

        if (!empty($agent_id) && empty($reference_type) && !empty($start)){

            $start_b1 = Carbon::parse($start)->format('Y-m-d');



            $agent_query = "select (SELECT end_balance from crates_inquiry_agents where created_at < '$start_b1' and crate_id = 193 and agent_id = '$agent_id' order by created_at desc LIMIT 1 ) as 'bcl_ag',
(SELECT end_balance from crates_inquiry_agents where created_at < '$start_b1' and crate_id = 194 and agent_id = '$agent_id' order by created_at desc LIMIT 1 ) as 'bcs_ag',
(SELECT end_balance from crates_inquiry_agents where created_at < '$start_b1' and crate_id = 195 and agent_id = '$agent_id' order by created_at desc LIMIT 1 ) as 'crl_ag',
(SELECT end_balance from crates_inquiry_agents where created_at < '$start_b1' and crate_id = 196 and agent_id = '$agent_id' order by created_at desc LIMIT 1 ) as 'crs_ag',
(SELECT end_balance from crates_inquiry_agents where created_at < '$start_b1' and crate_id = 197 and agent_id = '$agent_id' order by created_at desc LIMIT 1 ) as 'lid_ag',
(SELECT start_balance from crates_inquiry_agents where created_at = '$start_b1' and crate_id = 193 and agent_id = '$agent_id' order by created_at asc LIMIT 1 ) as 'bcl_ag1',
(SELECT start_balance from crates_inquiry_agents where created_at = '$start_b1' and crate_id = 194 and agent_id = '$agent_id' order by created_at asc LIMIT 1 ) as 'bcs_ag1',
(SELECT start_balance from crates_inquiry_agents where created_at = '$start_b1' and crate_id = 195 and agent_id = '$agent_id' order by created_at asc LIMIT 1 ) as 'crl_ag1',
(SELECT start_balance from crates_inquiry_agents where created_at = '$start_b1' and crate_id = 196 and agent_id = '$agent_id' order by created_at asc LIMIT 1 ) as 'crs_ag1',
(SELECT start_balance from crates_inquiry_agents where created_at = '$start_b1' and crate_id = 197 and agent_id = '$agent_id' order by created_at asc LIMIT 1 ) as 'lid_ag1'";

            $agent_records = collect(DB::select(DB::raw($agent_query)))->first();



            if(!empty($agent_records)) {


                if(!empty($agent_records->bcl_ag)) {
                    $ag_st_bcl = ($agent_records->bcl_ag1) ? $agent_records->bcl_ag1 : $agent_records->bcl_ag;
                }else{
                    $ag_st_bcl = 0;
                }
                if(!empty($agent_records->bcs_ag)) {
                    $ag_st_bcs = ($agent_records->bcs_ag1)?$agent_records->bcs_ag1 : $agent_records->bcs_ag ;
                }else{
                    $ag_st_bcs = 0;
                }
                if(!empty($agent_records->crl_ag)) {
                    $ag_st_crl = ($agent_records->crl_ag1)?$agent_records->crl_ag1 : $agent_records->crl_ag ;
                }else{
                    $ag_st_crl = 0;
                }
                if(!empty($agent_records->crs_ag)) {
                    $ag_st_crs = ($agent_records->crs_ag1)?$agent_records->crs_ag1 : $agent_records->crs_ag ;
                }else{
                    $ag_st_crs = 0;
                }
                if(!empty($agent_records->lid_ag)) {
                    $ag_st_lid = ($agent_records->lid_ag1)?$agent_records->lid_ag1 : $agent_records->lid_ag ;
                }else{
                    $ag_st_lid = 0;
                }




                $data[0] = array(
                    'Start Balance',
                    'Balance Crates In Hand',
                    ($ag_st_bcl),
                    ($ag_st_bcs),
                    ($ag_st_crl),
                    ($ag_st_crs),
                    ($ag_st_lid),
                    '',
                );
            }else{

                $data[0] = array(
                    'Start Balance',
                    '',
                    0,
                    0,
                    0,
                    0,
                    0,
                    '',
                );

            }
            $i = 1;
        }

        foreach ($records as $key => $record) {

            $bcl = $bcl + $record->bcl;
            $bcs = $bcs + $record->bcs;
            $crl = $crl + $record->crl;
            $crs = $crs + $record->crs;
            $lid = $lid + $record->lid;

            $data[$i] = array(
                Carbon::parse($record->created_at)->format('Y-M-d H:i:s'),
                $record->name_with_initials.' - '.$record->invoice_number,
                $record->bcl,
                $record->bcs,
                $record->crl,
                $record->crs,
                $record->lid,
                $record->description,

            );
            $i++;

        }


        if ($count == 0 ) {
            if($reference_type == 1 || $reference_type == 2 || $reference_type == 3 || $reference_type == 21) {
                $data = [];

            }
        }
        else {
            if (empty($agent_id) && empty($reference_type) && !empty($balance_records)) {
              /*  $data[$i] = array(
                    'End Balance',
                    '',
                    '',
                    $balance_records->bcl_en,
                    $balance_records->bcs_en,
                    $balance_records->crl_en,
                    $balance_records->crs_en,
                    $balance_records->lid_en,

                );*/
            }else{
                if(!empty($agent_id) && empty($reference_type) && !empty($start)) {
                    $data[$i] = array(
                        'Total',
                        '',
                        $bcl + $agent_records->bcl_ag,
                        $bcs + $agent_records->bcs_ag,
                        $crl + $agent_records->crl_ag,
                        $crs + $agent_records->crs_ag,
                        $lid + $agent_records->lid_ag,
                        '',
                    );
                }else{
                    $data[$i] = array(
                        'Total',
                        '',
                        $bcl,
                        $bcs,
                        $crl,
                        $crs,
                        $lid,
                        '',
                    );

                }

            }
        }



        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($count),
            "recordsFiltered" => intval($count),
            "data" => $data
        ];

        return json_encode($json_data);

    }
    /*public function cratesInquirePrint(Request $request)
    {
        $agents = SalesRep::whereId($request->agent)->first();
        $date = $request->date;
        $ref = $request->ref;
        $filter = true;
        $raw_materials = RawMaterial::where('type', '=', 5)->get()->pluck('name', 'raw_material_id');
        return view('crate_usage_list_agent_invoice', compact('raw_materials', 'agents','date','ref','filter'));
    }*/


  //  public function cratesInquirePrintInvoice(Request $request)

    public function cratesInquirePrint(Request $request)
    {
        /*  dd($request);
          $order_by = $request->order;
          $search = $request->search['value'];
          $start = $request->start;
          $length = $request->length;
          $order_by_str = $order_by[0]['dir'];

          $columns = ['id', 'created_at'];
          $order_column = $columns[$order_by[0]['column']];*/

        $query = CrateInquiryAgent::select('users.name_with_initials', 'crates_inquiry_agents.*')->leftJoin('users', 'users.id', 'crates_inquiry_agents.agent_id');

        $count = 0;
        $records = collect();
        $balance_records = collect();

        $data = [];
        $i = 0;
        $reference_type = null;

        if ($request->filter == true) {
            $agent_id = $request->agent;
            $crate_id = null;// for future use
            $date_range = $request->date;
            $reference_type = $request->ref;

            $agents = SalesRep::whereId($agent_id)->first();

            $dates = explode(' - ', $date_range);
            $start = Carbon::parse($dates[0])->format('Y-m-d 00:00:00');
            $end = Carbon::parse($dates[1])->format('Y-m-d 23:59:59');
            //  $start = Carbon::parse($dates[0])->format('Y-m-d');
            //  $end = Carbon::parse($dates[1])->format('Y-m-d');
            $whereQuery = "where t1.created_at >='$start' and t1.created_at <= '$end'";

            if (!empty($agent_id)) {
                $whereQuery .= " and t1.agent_id = $agent_id";
            }
            if (!empty($reference_type)) {
                $whereQuery .= " and t1.desc_id = $reference_type";
            }


            $query = "with t1 as (SELECT crates_inquiry_agents.reference,crates_inquiry_agents.created_at,crates_inquiry_agents.agent_id, crates_inquiry_agents.desc_id,crates_inquiry_agents.description,
CASE WHEN crates_inquiry_agents.crate_id = '193' THEN sum(crates_inquiry_agents.qty) END 'bcl',
CASE WHEN crates_inquiry_agents.crate_id = '194' THEN sum(crates_inquiry_agents.qty) END 'bcs',
CASE WHEN crates_inquiry_agents.crate_id = '195' THEN sum(crates_inquiry_agents.qty) END 'crl',
CASE WHEN crates_inquiry_agents.crate_id = '196' THEN sum(crates_inquiry_agents.qty) END 'crs',
CASE WHEN crates_inquiry_agents.crate_id = '197' THEN sum(crates_inquiry_agents.qty) END 'lid'
FROM crates_inquiry_agents
GROUP BY DATE_FORMAT(crates_inquiry_agents.created_at,'%Y-%m-%d %H:%i'),crates_inquiry_agents.agent_id, crates_inquiry_agents.crate_id, crates_inquiry_agents.desc_id 
ORDER BY 'crates_inquiry_agents.created_at' DESC)
SELECT loading_headers.invoice_number,t1.created_at,t1.desc_id,users.name_with_initials,t1.description,sum(t1.bcl) as bcl,sum(t1.bcs) as bcs,sum(t1.crl) as crl,sum(t1.crs) as crs,sum(t1.lid) as lid 
FROM  t1  LEFT JOIN users ON t1.agent_id = users.id 
LEFT JOIN loading_headers on t1.reference = loading_headers.id
$whereQuery GROUP BY DATE_FORMAT(t1.created_at,'%Y-%m-%d %H:%i'),t1.agent_id";

            $records = collect(DB::select(DB::raw($query)));

            // dd($records);

            $start_b = Carbon::parse($dates[0])->format('Y-m-d');
            $end_b = Carbon::parse($dates[1])->format('Y-m-d');


            $balance_query = "select (SELECT end_balance from crates_inquiry_agents where created_at < '$start_b' and crate_id = 193 order by created_at desc LIMIT 1 ) as 'bcl_st',
(SELECT end_balance from crates_inquiry_agents where created_at <  '$start_b' and crate_id = 194 order by created_at desc LIMIT 1 ) as 'bcs_st',
(SELECT end_balance from crates_inquiry_agents where created_at < '$start_b' and crate_id = 195 order by created_at desc LIMIT 1 ) as 'crl_st',
(SELECT end_balance from crates_inquiry_agents where created_at < '$start_b' and crate_id = 196 order by created_at desc LIMIT 1 ) as 'crs_st',
(SELECT end_balance from crates_inquiry_agents where created_at < '$start_b' and crate_id = 197 order by created_at desc LIMIT 1 ) as 'lid_st',
(SELECT start_balance from crates_inquiry_agents where created_at = '$start_b' and crate_id = 193 order by created_at asc LIMIT 1 ) as 'bcl_st1',
(SELECT start_balance from crates_inquiry_agents where created_at =  '$start_b' and crate_id = 194 order by created_at asc LIMIT 1 ) as 'bcs_st1',
(SELECT start_balance from crates_inquiry_agents where created_at = '$start_b' and crate_id = 195 order by created_at asc LIMIT 1 ) as 'crl_st1',
(SELECT start_balance from crates_inquiry_agents where created_at = '$start_b' and crate_id = 196 order by created_at asc LIMIT 1 ) as 'crs_st1',
(SELECT start_balance from crates_inquiry_agents where created_at = '$start_b' and crate_id = 197 order by created_at asc LIMIT 1 ) as 'lid_st1',
(SELECT end_balance from crates_inquiry_agents where created_at <= '$end_b%' and crate_id = 193 order by created_at desc LIMIT 1 ) as 'bcl_en',
(SELECT end_balance from crates_inquiry_agents where created_at <= '$end_b%' and crate_id = 194 order by created_at desc LIMIT 1 ) as 'bcs_en',
(SELECT end_balance from crates_inquiry_agents where created_at <= '$end_b%' and crate_id = 195 order by created_at desc LIMIT 1 ) as 'crl_en',
(SELECT end_balance from crates_inquiry_agents where created_at <= '$end_b%' and crate_id = 196 order by created_at desc LIMIT 1 ) as 'crs_en',
(SELECT end_balance from crates_inquiry_agents where created_at <= '$end_b%' and crate_id = 197 order by created_at desc LIMIT 1 ) as 'lid_en'";

            $balance_records = collect(DB::select(DB::raw($balance_query)))->first();


            if (!empty($agent_id) && empty($reference_type) && !empty($start)) {

                $start_b1 = Carbon::parse($start)->format('Y-m-d');


                $agent_query = "select (SELECT end_balance from crates_inquiry_agents where created_at < '$start_b1' and crate_id = 193 and agent_id = '$agent_id' order by created_at desc LIMIT 1 ) as 'bcl_ag',
(SELECT end_balance from crates_inquiry_agents where created_at < '$start_b1' and crate_id = 194 and agent_id = '$agent_id' order by created_at desc LIMIT 1 ) as 'bcs_ag',
(SELECT end_balance from crates_inquiry_agents where created_at < '$start_b1' and crate_id = 195 and agent_id = '$agent_id' order by created_at desc LIMIT 1 ) as 'crl_ag',
(SELECT end_balance from crates_inquiry_agents where created_at < '$start_b1' and crate_id = 196 and agent_id = '$agent_id' order by created_at desc LIMIT 1 ) as 'crs_ag',
(SELECT end_balance from crates_inquiry_agents where created_at < '$start_b1' and crate_id = 197 and agent_id = '$agent_id' order by created_at desc LIMIT 1 ) as 'lid_ag',
(SELECT start_balance from crates_inquiry_agents where created_at = '$start_b1' and crate_id = 193 and agent_id = '$agent_id' order by created_at asc LIMIT 1 ) as 'bcl_ag1',
(SELECT start_balance from crates_inquiry_agents where created_at = '$start_b1' and crate_id = 194 and agent_id = '$agent_id' order by created_at asc LIMIT 1 ) as 'bcs_ag1',
(SELECT start_balance from crates_inquiry_agents where created_at = '$start_b1' and crate_id = 195 and agent_id = '$agent_id' order by created_at asc LIMIT 1 ) as 'crl_ag1',
(SELECT start_balance from crates_inquiry_agents where created_at = '$start_b1' and crate_id = 196 and agent_id = '$agent_id' order by created_at asc LIMIT 1 ) as 'crs_ag1',
(SELECT start_balance from crates_inquiry_agents where created_at = '$start_b1' and crate_id = 197 and agent_id = '$agent_id' order by created_at asc LIMIT 1 ) as 'lid_ag1'";

                $agent_records = collect(DB::select(DB::raw($agent_query)))->first();


                if (!empty($agent_records)) {


                    if (!empty($agent_records->bcl_ag)) {
                        $ag_st_bcl = ($agent_records->bcl_ag1) ? $agent_records->bcl_ag1 : $agent_records->bcl_ag;
                    } else {
                        $ag_st_bcl = 0;
                    }
                    if (!empty($agent_records->bcs_ag)) {
                        $ag_st_bcs = ($agent_records->bcs_ag1) ? $agent_records->bcs_ag1 : $agent_records->bcs_ag;
                    } else {
                        $ag_st_bcs = 0;
                    }
                    if (!empty($agent_records->crl_ag)) {
                        $ag_st_crl = ($agent_records->crl_ag1) ? $agent_records->crl_ag1 : $agent_records->crl_ag;
                    } else {
                        $ag_st_crl = 0;
                    }
                    if (!empty($agent_records->crs_ag)) {
                        $ag_st_crs = ($agent_records->crs_ag1) ? $agent_records->crs_ag1 : $agent_records->crs_ag;
                    } else {
                        $ag_st_crs = 0;
                    }
                    if (!empty($agent_records->lid_ag)) {
                        $ag_st_lid = ($agent_records->lid_ag1) ? $agent_records->lid_ag1 : $agent_records->lid_ag;
                    } else {
                        $ag_st_lid = 0;
                    }


                    $data[0] = array(
                        'Start',
                        ' Balance',
                        ' In Hand',
                        ($ag_st_bcl),
                        ($ag_st_bcs),
                        ($ag_st_crl),
                        ($ag_st_crs),
                        ($ag_st_lid),

                    );
                } else {

                    $data[0] = array(
                        'Start',
                        ' Balance',
                        ' In Hand',
                        0,
                        0,
                        0,
                        0,
                        0,
                    );

                }
                $i = 1;
            }

            $bcl = 0;
            $bcs = 0;
            $crl = 0;
            $crs = 0;
            $lid = 0;

            foreach ($records as $key => $record) {

                $ds = '';
                $bcl = $bcl + $record->bcl;
                $bcs = $bcs + $record->bcs;
                $crl = $crl + $record->crl;
                $crs = $crs + $record->crs;
                $lid = $lid + $record->lid;


                if ($record->desc_id == 1) {
                    $ds = 'SEND';
                } elseif ($record->desc_id == 2) {
                    $ds = 'RECV';
                } elseif ($record->desc_id == 3) {
                    $ds = 'UPD';
                } elseif ($record->desc_id == 21) {
                    $ds = 'ADJ';
                }

                $soid = LoadingHeader::where('invoice_number', '=', $record->invoice_number)->first();
                if(!empty($soid)) {
                    $temid = SalesOrder::where('id', '=', $soid->so_id)->first();
                    $catid = AgentBuffer::where('id', '=', $temid->template_id)->first();

                    $catname = Category::find($catid->category_id);
                }
                $data[$i] = array(
                  //  Carbon::parse($record->created_at)->format('Y-M-d H:i:s') . ' - ' . $ds . ' - ' . $record->invoice_number . ' | ' . $catname->name,
                    Carbon::parse($record->created_at)->format('Y-M-d H:i:s'),
                    $ds. ' | ' . $catname->name,
                    $record->invoice_number,
                    $record->bcl,
                    $record->bcs,
                    $record->crl,
                    $record->crs,
                    $record->lid,

                );
                $i++;

            }
                  if(!empty($agent_id) && empty($reference_type) && !empty($start)) {
                        $data[$i] = array(
                        'Balance',
                        ' In',
                        ' Hand',
                        $bcl + $agent_records->bcl_ag,
                        $bcs + $agent_records->bcs_ag,
                        $crl + $agent_records->crl_ag,
                        $crs + $agent_records->crs_ag,
                        $lid + $agent_records->lid_ag,
                    );
                }
        }





        $json_data = [
            "draw" => intval(1000),
            "recordsTotal" => intval($count),
            "recordsFiltered" => intval($count),
            "data" => $data,

        ];


        return view('crate_usage_list_agent_invoice', compact('data','agents'));

    }


}
