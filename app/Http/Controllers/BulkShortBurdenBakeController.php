<?php

namespace App\Http\Controllers;

use App\Traits\SemiFinishLog;
use BurdenManager\Models\Burden;
use Carbon\Carbon;
use EmployeeManager\Models\Employee;
use EmployeeManager\Models\Group;
use Illuminate\Http\Request;
use SemiFinishProductManager\Models\SemiFinishProduct;
use ShortEatsManager\Models\CookingRequest;
use StockManager\Classes\StockTransaction;

class BulkShortBurdenBakeController extends Controller
{
    use SemiFinishLog;

    function __construct()
    {
        $this->middleware('permission:bulk_short_burden_bake-index', ['only' => ['index','tableData']]);
        $this->middleware('permission:FillingBulkBurdenBurn-list', ['only' => ['list']]);
        $this->middleware('permission:bulk_short_burden_bake-create', ['only' => ['createNow']]);

    }
    public function index()
    {
        /*$employees =Employee::whereHas('attendance', function ($q) {
            $q->where('date','like',Carbon::today()->format('Y-m-d').'%');
        })->pluck('full_name', 'id');*/
        $teams = Group::whereDate('created_at', Carbon::today())->get();
        return view('bulk_burden__cooking_bake',compact('teams'));
    }

    public function tableData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $recordsCount = 0;

        $date = $request->date_range;
        $columns = ['semi_finish_product_id', 'semi_finish_product_id', 'qty'];
        $order_column = $columns[$order_by[0]['column']];

        $records = CookingRequest::tableData($order_column, $order_by_str, $start, $length);

        $records = $records->whereStatus(3)->get();
//
//            $records = $records->get();
        $recordsCount = (!empty($records)) ? sizeof($records) : 0;

        $data[][] = array();
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;


        foreach ($records as $key => $record) {
            $data[$i] = array(
                "<input type='checkbox' name='id[]' value='$record->id'/>",
                $record->semiProdcuts->name,
                $record->serial,
                $record->burden_size,
                $record->expected_semi_product_qty,
            );
            $i++;
        }


        if ($recordsCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($recordsCount),
            "recordsFiltered" => intval($recordsCount),
            "data" => $data
        ];

        return json_encode($json_data);
    }


    public function createNow(Request $request)
    {
        $burdens = $request->id;
        $employee = $request->employee;

        $burdens = CookingRequest::whereIn('id', $burdens)->get();

        foreach ($burdens as $burden) {
            $burden->actual_semi_product_qty = $burden->expected_semi_product_qty;
            $burden->used_qty = $burden->expected_semi_product_qty;
           // $burden->employee_group = $request->employee_group;
            $burden->baker = $employee;
            $burden->bake_start_time = Carbon::now();
            $burden->stage_2 = 1;
            // $burden->status = 5;
            $burden->status = 6;
            $burden->save();
           // StockTransaction::semiFinishStockTransaction(1, $burden->semi_finish_product_id, 1, $burden->expected_semi_product_qty, 1, 1);
            //StockTransaction::updateSemiFinishAvailableQty(1, $burden->semi_finish_product_id, $burden->expected_semi_product_qty);
            $semi_product = SemiFinishProduct::find($burden->semi_finish_product_id);
            $this->recordSemiFinishProductOutstanding($semi_product,1,$burden->expected_semi_product_qty, 'Grade Cooking Request', $burden->id, 'ShortEatsManager\\Models\\CookingRequest',$burden->id,'CookingRequestGrade', 1,$burden->id,'');

        }

        return redirect(route('short_bulk_burden_bake.index'));
    }
}
