<?php

namespace App\Http\Controllers;

use App\Crate;
use App\CrateInquiryAgent;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use SalesRepManager\Models\SalesRep;

class CrateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  view('crates.index');
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Crate  $crate
     * @return \Illuminate\Http\Response
     */
    public function show(Crate $crate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Crate  $crate
     * @return \Illuminate\Http\Response
     */
    public function edit(Crate $crate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Crate  $crate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Crate $crate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Crate  $crate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Crate $crate)
    {
        //
    }
}
