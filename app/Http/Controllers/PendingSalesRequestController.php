<?php

namespace App\Http\Controllers;

use App\Jobs\SalesApiStockUpdateJob;
use App\LoadingData;
use App\LoadingDataRaw;
use App\LoadingDataVariant;
use App\LoadingHeader;
use App\CratesTransaction;
use App\Services\APIService;
use App\Traits\SendSMS;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use LoadingManager\Models\Loading;
use SalesOrderManager\Models\SalesOrder;
use UserManager\Models\Users;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use StockManager\Classes\StockTransaction;

class PendingSalesRequestController extends Controller
{
    use SendSMS;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $loadings = LoadingHeader::whereHas('salesOrder', function ($q) use ($user) {
            return $q->whereCreatedBy($user->id);
        })->whereStatus(2)->orderBy('updated_at', 'desc')->latest()->get()->take(20);
        return view('agents.pending-list', compact('loadings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function cratesRequestList()
    {
        $user = Auth::user();
        $loadings = CratesTransaction::whereType('return')
            ->where('agent_id', '=', $user->id)
            ->where('crates_approve', '=', Null)->get();
        return view('agents.returnCrates-list', compact('loadings'));
    }

    public function cratesRequestShow(CratesTransaction $loadingid)
    {
//        $crates = json_decode($loadingid->crates);
        return view('agents.returnCrates-show', compact('loadingid'));
    }

    public function cratesRequestEdit(CratesTransaction $loadingid)
    {
//        $crates = json_decode($loadingid->crates);
        return view('agents.returnCrates-edit', compact('loadingid'));
    }

    public function cratesRequestCreate()
    {
        return view('agents.returnCrates-create');
    }

    public function cratesStoreRequest(Request $request)
    {

        $available_crates_data = [];
        $user = Auth::user();
        $creatReturn = new CratesTransaction();
        $creatReturn->type = 2;
        if (!empty($request->return_crate) && sizeof($request->return_crate) > 0) {
            for ($i = 0; $i < sizeof($request->return_crate); $i++) {
                $received_crates_d = ['crate_id' => $request->return_crate[$i], 'qty' => $request->return_crate_qty[$i]];
                array_push($available_crates_data, $received_crates_d);
            }
        }
        $creatReturn->crates = json_encode($available_crates_data);
        $creatReturn->agent_id = $user->id;
        $creatReturn->created_by = $user->id;
        $creatReturn->save();
        return redirect(route('crates-request.cratesRequestList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Sentinel::getUser();

        DB::transaction(function () use ($request) {
            $items = $request->item_id;
            $item_qty = $request->product_qty;

            $variants = $request->variant_item_id;
            $variant_qty = $request->variant_qty;

            $shop_item_id = $request->shop_items;
            $shop_item_qty = $request->shop_items_qty;

            if (!empty($items)) {
                foreach ($items as $key => $item) {
                    $data = LoadingData::find($item);
                    $data->agent_approved_qty = $item_qty[$key];
                    $data->admin_approved_qty = $item_qty[$key];

                    if ($item_qty[$key] != $data->qty) {
                        $data->is_changed = 1;
                        $loading = LoadingHeader::find($data->loading_header_id);
                        $loading->is_changed = 1;
                        $loading->save();
                    }

                    $data->save();
                }
            }

            if (!empty($variants)) {
                foreach ($variants as $key => $item) {
                    $data = LoadingDataVariant::find($item);
                    $data->agent_approved_qty = $variant_qty[$key];
                    $data->admin_approved_qty = $variant_qty[$key];

                    if ($variant_qty[$key] != $data->weight) {
                        $data->is_changed = 1;
                        $loading = LoadingHeader::find($data->loading_header_id);
                        $loading->is_changed = 1;
                        $loading->save();
                    }

                    $data->save();
                }
            }
//            if (!empty($shop_item_id)) {
//                foreach ($shop_item_id as $key => $item) {
//                    $data = LoadingDataRaw::find($item);
//                    $data->agent_approved_qty = $shop_item_qty[$key];
//                    $data->admin_approved_qty = $shop_item_qty[$key];
//
//                    if ($shop_item_qty[$key] != $data->qty) {
//                        $data->is_changed = 1;
//                        $loading = LoadingHeader::find($data->loading_header_id);
//                        $loading->is_changed = 1;
//                        $loading->save();
//                    }
//
//                    $data->save();
//                }
//            }

        });

        $header = LoadingHeader::find($request->id);
        $header->status = 3;
        $crates_data = [];


        //get crates at user table

//        if (!empty($request->new_crate) && sizeof($request->new_crate) > 0) {
//            for ($i = 0; $i < sizeof($request->new_crate); $i++) {
//                $received_crates_d = ['crate_id' => $request->new_crate[$i], 'qty' => $request->crate_qty_new[$i]];
//                array_push($crates_data, $received_crates_d);
//            }
//        }

        $available_crates_data = [];
        $tp = 'return';
        $isin = CratesTransaction::where('ref_id','=',$request->id)->where('type','=',$tp)->first();
        //dd($request->id);
        if(empty($isin)) {
            $creatReturn = new CratesTransaction();
            $creatReturn->type = 2;
            if (!empty($request->return_crate) && sizeof($request->return_crate) > 0) {
                for ($i = 0; $i < sizeof($request->return_crate); $i++) {
                    $return_crates_d = ['crate_id' => $request->return_crate[$i], 'qty' => $request->return_crate_qty[$i]];
                    array_push($available_crates_data, $return_crates_d);
                }

                $creatReturn->crates = json_encode($available_crates_data);
                $creatReturn->agent_id = $user->id;
                $creatReturn->created_by = $user->id;
                $creatReturn->ref_id = $header->id;
                $creatReturn->save();
            }

            $header->crate_receive = json_encode($crates_data);

            $header->save();

            $so = SalesOrder::find($header->so_id);
            $so->status = 4;//agent accepted
            $so->save();


        }
//        $creatSentApprove = CratesTransaction::whereRefId($request->id)->first();
//        $creatSentApprove->crates_approve = json_encode($crates_data);
//        $creatSentApprove->created_by = $user->id;
//        $creatSentApprove->save();
        //add crates to user





        //loading data in to sales-api database via queue
        //$this->sendSMS('0711225489', 'Order Accepted');

        $order = LoadingHeader::where('id', $header->id)->with(['items','variants','salesOrder'])->first();;
        $order['agent_id'] = $order->salesOrder->created_by;
        $client = new APIService();
        $client->sendLoadingDataToAPI($order);
        $header->save();



        return redirect(route('order.list'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $order = LoadingHeader::find($id);
        $order = LoadingHeader::where('so_id','=',$id)->first();
        $crates = json_decode($order->crate_sent, true);
        return view('agents.pending-show', compact('order', 'crates'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
