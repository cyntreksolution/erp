<?php

namespace App\Http\Controllers;

use App\Services\APIService;
use Carbon\Carbon;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;

class ShopOrderController extends Controller
{
    protected $client;

    public function __construct()
    {
        $this->client = new APIService();
    }


    public function index(Request $request)
    {
        $shopId = $request->shop_id;
        $orders = $this->client->getShopOrders($shopId);
        return view('api.orders.list', compact('orders'));
    }

    public function orderItems(Request $request)
    {
        $orderId = $request->order_id;
        $items = $this->client->getShopOrderItems($orderId);
        $items = $items->data;
        return view('api.orders.items', compact('items'));
    }

    public function filterList(Request $request)
    {
        $yesterday=Carbon::yesterday();
        $today=Carbon::today();
        $dateRange = !empty($request->date_range) ? $request->date_range : "$yesterday - $today";
        if (Sentinel::check()->roles[0]->slug == 'sales-ref') {
            $agentId = Sentinel::getUser()->id;
        }else{
            $agentId = !empty($request->agent_id) ? $request->agent_id : null;
        }
        $shopId = !empty($request->shopId) ? $request->shopId : null;

        $orders = $this->client->getShopOrders($shopId, $agentId, $dateRange);
        $orders = $orders->data;

        $shops = collect($this->client->getAllShops())->pluck('name', 'id');
        $agents = collect($this->client->getAllAgents())->pluck('name_with_initials', 'id');


        return view('api.orders.all', compact('orders', 'shops', 'agents', 'dateRange', 'agentId', 'shopId'));
    }

    public function getAgentShops(Request $request)
    {
        $agentId = $request->agent_id;
        $shops = $this->client->getShopsByAgent($agentId);
        return json_encode($shops);
    }

    public function changeShopId(Request $request)
    {
        $shopId = $request->shop_id;
        $orderId = $request->order_id;
        $shops = $this->client->changeOrderShopId($orderId,$shopId);
        return redirect()->back();
    }

    public function getReturns()
    {

    }

    public function returnItems()
    {

    }
}
