<?php

namespace App\Http\Controllers;


use App\Number;
use App\Traits\SendSMS;
use App\UserNumber;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use DB;


class UserController extends Controller
{
    use SendSMS;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    function __construct()
    {
        $this->middleware('permission:user-list|user-create|user-edit|user-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:user-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:user-list', ['only' => ['list']]);
        $this->middleware('permission:user-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
        $this->middleware('permission:user-view', ['only' => ['view']]);
    }

    public function index(Request $request)
    {
        $data = User::orderBy('id', 'ASC')->paginate(10);
        $roles = Role::pluck('name', 'name')->all();
        return view('users.index', compact('data', 'roles'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name', 'name')->all();
        return view('users.create', compact('roles'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'second_name' => 'required',
            'email' => 'required|email|unique:users,email',
            'roles' => 'required'
        ]);

        $password = $this->randomPassword();

        $hash_password = Hash::make($password);

        $user = User::create([
                'first_name' => $request->input('first_name'),
                'second_name' => $request->input('second_name'),
                'email' => $request->input('email'),
                'telephone1' => $request->input('telephone1'),
                'address' => $request->input('address'),
                'password' => $hash_password,
                'otp_status' => $request->input('otp_status')==1 ? 1:0,
            ]);
        $user->assignRole($request->input('roles'));
        $otpStatus = $request->input('otp_status')==1 ? 1:0;

        if ($otpStatus ==1) {
          $number=  Number::create([
                'number' => $request->input('telephone1'),
                'description' => $request->input('address'),
            ]);
            $user->numbers()->attach($number->id);
        }
        $msg = "Your Username -". $user->email ." Password - ".$password;
        $this->sendSMS($user->telephone1, $msg);


        return redirect()->route('users.index')
            ->with('success', 'User created successfully');
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('users.show', compact('user'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::pluck('name', 'name')->all();
        $userRole = $user->roles->pluck('name', 'name')->all();


        return view('users.edit', compact('user', 'roles', 'userRole'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
            'roles' => 'required'
        ]);


        $user = User::find($id);

        $user->update([
            'first_name' => $request->input('first_name'),
            'second_name' => $request->input('second_name'),
            'email' => $request->input('email'),
            'telephone1' => $request->input('telephone1'),
            'address' => $request->input('address'),
            'otp_status' => $request->input('otp_status')==1 ? 1:0,
            ]);
        DB::table('model_has_roles')->where('model_id', $id)->delete();


//        $user->assignRole($request->input('roles'));

        $user->syncRoles([$request->input('roles')]);

        $otpStatus = $request->input('otp_status')==1 ? 1:0;

        if ($otpStatus ==1) {
            $number= Number::updateOrCreate(
                [
                    'number' => $request->input('telephone1'),
                ],
                ['description' => $request->input('address'),
                ]);
            $user->numbers()->sync($number->id);
        }else{
            if (!empty($request->input('telephone1'))) {
                $number = Number::where('number',$request->input('telephone1'))->first();
                $userNO = UserNumber::where('user_id',$id);
                $userNO->delete();
            }else{
                $user_number = User::find($id)->pluck('telephone1');
                $number = Number::where('number',$user_number)->first();
                $userNO = UserNumber::where('user_id',$id);
                $userNO->delete();
            }
            if (!empty($number)) {
                $number->delete();

            }

        }

        return redirect()->route('users.index')
            ->with('success', 'User updated successfully');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        User::find($request->id)->delete();
        return 'true';
    }

    public function otpstatus(Request $request){
        $user = User::find($request->id);
        $user->status =$request->status;
        $user->save();
        return response()->json(['success'=>'Status change successfully.']);
    }

    function randomPassword()
    {
        $alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890#*@';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function tableData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];
        $recordsCount = 0;
        $columns = ['id', 'first_name', 'second_name', 'email', 'telephone1', 'address'];
        $order_column = $columns[$order_by[0]['column']];

        $records = User::query();
        if ($request->filter == true) {
            $roles = $request->roles;

            $recordsCount = $records->count();
            $records = $records->filterData($roles)->tableData($order_column, $order_by_str, $start, $length)->get();
        }
        elseif (is_null($search) || empty($search)) {
            $recordsCount = $records->count();
            $records = $records->tableData($order_column, $order_by_str, $start, $length)->get();
        } else {
            $recordsCount = $records->count();
            $records = $records->searchData($search)->tableData($order_column, $order_by_str, $start, $length)->get();
        }


        $data[][] = array();
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;

        $btn_status = null;
        $btn_approve = null;
        $user = Auth::user();
        $can_edit = ($user->can('users edit')) ? 1 : 0;
        $can_delete = ($user->can('users delete')) ? 1 : 0;
        foreach ($records as $key => $record) {
            $rol = [];
            if (!empty($record->getRoleNames())) {
                foreach ($record->getRoleNames() as $v) {
                    array_push($rol, $v);
                }
            }
            $rol = $rol[0] ?? 0;
            if (Auth::id() != $record->id) {
                if ($can_edit) {
                    $edit_btn = "<button onclick=\"edit(this)\" data-id='{$record->id}' data-first_name='{$record->first_name}' data-second_name='{$record->second_name}' data-email='{$record->email}' data-telephone1='{$record->telephone1}' data-address='{$record->address}' data-otp_status='{$record->otp_status}' data-role='{$rol}' class='addItem btn btn-sm btn-warning ml-1' style='margin-right: 5px;'><i class='fa fa-edit'></i></button>";
                }
                if ($can_delete) {
                    $delete_btn = "<button class='btn btn-xs btn-icon btn-light-danger mr-2' onclick=\"deleteuser('$record->id')\"> <i class='fa fa-trash'></i></button>";
                }
            }

            if ($record->status == 1) {
                $status_btn = '<div  title="Change OTP Status"data-toggle="tooltip">
                                 <label>
                                    <input type="checkbox" name="otp" data-plugin="switchery" data-size="medium" data-toggle="toggle" data-id="' . $record->id . '" onclick="chengeotpStatus(' . $key . ',' . $record->id . ')" id="myonoffswitch_' . $key . '" checked>
                                    
                                  </label>
                                 </div> 
                                ';
            } else {
                $status_btn = '<div  title="Change OTP Status"data-toggle="tooltip">
                                 <label>
                                    <input type="checkbox" name="otp" data-toggle="toggle" data-id="' . $record->id . '" onclick="chengeotpStatus(' . $key . ',' . $record->id . ')" id="myonoffswitch_' . $key . '">
                                    
                                  </label>
                                 </div>';
            }


            $data[$i] = array(
                $record->id,
                $record->name_with_initials,
                $record->email,
                $record->telephone1,
                $record->address,
                $rol,
                $record->otp_status ==1? '<button class="tn btn-sm btn-success">Active</button>':'<button class="tn btn-sm btn-default">InActive</button>',
                $status_btn,
                $edit_btn
//                $delete_btn
            );
            $i++;
        }


        if ($recordsCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($recordsCount),
            "recordsFiltered" => intval($recordsCount),
            "data" => $data
        ];

        return json_encode($json_data);
    }

}
