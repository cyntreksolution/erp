<?php

namespace App\Http\Controllers;

use App\AgentInquiry;
use App\AgentPayment;
use App\CrateInquiryAgent;
use App\LoadingHeader;
use App\SalesReturnHeader;
use Carbon\Carbon;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use EndProductBurdenManager\Models\EndProductBurden;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\View;
use PassBookManager\Models\Cheque;
use SalesRepManager\Models\SalesRep;
use StockManager\Classes\StockTransaction;
use function GuzzleHttp\Promise\all;
use SalesOrderManager\Models\SalesOrder;
use LoadingManager\Models\LoadingStatusChanges;
use App\CratesTransaction;
use RawMaterialManager\Models\RawMaterial;

class CratesInquireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    function __construct()
    {
        $this->middleware('permission:crates_inquiry-index', ['only' => ['index','tableData']]);
        $this->middleware('permission:crates_inquiry-indexnew', ['only' => ['indexnew']]);
        $this->middleware('permission:CratesInquiryOld-list', ['only' => ['list']]);
        $this->middleware('permission:CratesInquiry-list', ['only' => ['list']]);

    }
    public function index()
    {
        $agents = SalesRep::all();
        return view('agents.crates-inquire-index', compact('agents'));
    }

    public function indexnew()
    {

        $agents = SalesRep::orderBy('email')->get();
        //return view('crate-inquiries', compact('agents'));
        return view('agents.crate-inquiries', compact('agents'));
    }

    public function renderData(Request $request)
    {
        $agent = SalesRep::find($request->agent);
        $selectedDate = $request->date;

        $crate_init_value = CrateInquiryAgent::whereAgentId($agent->id)->whereIsInit(1)->first();
        $crate_init_value = (!empty($crate_init_value) && $crate_init_value->count() > 0) ? $crate_init_value->qty : 0;
//        $agent_records = AgentInquiry::whereAgentId($agent->id)->where('created_at','like',$selectedDate.'%')->orderBy('created_at')->get();

        $crate_records = CrateInquiryAgent::filterData($agent->id, $selectedDate)->get()->groupBy(function ($val) {
            return Carbon::parse($val->created_at)->format('Y-m-d');
        });


        return View::make('agents.crate-inquire-data', compact('agent', 'selectedDate', 'crate_init_value', 'crate_records'));
    }

    public function crateInquireDownload(Request $request, $agent, $date)
    {
//        // dd($request);
//        $agent = SalesRep::find($request->agent);
//        $selectedDate = $request->date;
//
//        $crate_init_value = CrateInquiryAgent::whereAgentId($agent->id)->whereIsInit(1)->first();
//        $crate_init_value = (!empty($crate_init_value) && $crate_init_value->count() > 0) ? $crate_init_value->qty : 0;
////        $agent_records = AgentInquiry::whereAgentId($agent->id)->where('created_at','like',$selectedDate.'%')->orderBy('created_at')->get();
//        $crate_records = CrateInquiryAgent::filterData($agent->id, $selectedDate)->get()->groupBy(function ($val) {
//            return Carbon::parse($val->created_at)->format('Y-m-d');
//        });
//
//        return view('agents.crate-inquire-download', compact('agent', 'selectedDate', 'crate_init_value', 'crate_records'));


        $agent = SalesRep::find($request->agent);
        $selectedDate = $request->date;

        $crate_init_value = CrateInquiryAgent::whereAgentId($agent->id)->whereIsInit(1)->first();
        $crate_init_value = (!empty($crate_init_value) && $crate_init_value->count() > 0) ? $crate_init_value->qty : 0;
//        $agent_records = AgentInquiry::whereAgentId($agent->id)->where('created_at','like',$selectedDate.'%')->orderBy('created_at')->get();

        $crate_records = CrateInquiryAgent::filterData($agent->id, $selectedDate)->get()->groupBy(function ($val) {
            return Carbon::parse($val->created_at)->format('Y-m-d');
        });


        return view('agents.crate-inquire-data-download', compact('agent', 'selectedDate', 'crate_init_value', 'crate_records'));
        return view('agents.crate-inquire-data', compact('agent', 'selectedDate', 'crate_init_value', 'crate_records'));

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function getCratesInquireData($agentId)
    {
        $totalsendCrates193 = 0;
        $totalsendCrates194 = 0;
        $totalsendCrates195 = 0;
        $totalsendCrates196 = 0;
        $totalsendCrates197 = 0;

        $totalreturnCrates193 = 0;
        $totalreturnCrates194 = 0;
        $totalreturnCrates195 = 0;
        $totalreturnCrates196 = 0;
        $totalreturnCrates197 = 0;

        $totaldiffCrates193 = 0;
        $totaldiffCrates194 = 0;
        $totaldiffCrates195 = 0;
        $totaldiffCrates196 = 0;
        $totaldiffCrates197 = 0;

        $rawMatNameArray = [];
        $rawMatIdArray = [];
        $totalHeadersArray = [];
        $totalDetailsHeadersArray = [];
        $countRawMat = 0;
        $exists = 0;
        // $rawMat = RawMaterial::where('type','=',5)->get();
        // foreach ($rawMat as $key => $value) {
        //   $rawMatIdArray[] = $value->raw_material_id;
        //   $rawMatNameArray[] = $value->name;
        // }
        $sendCrates = CratesTransaction::where('agent_id', '=', $agentId)->get();
        foreach ($sendCrates as $key => $sendCrate) {
            $timestamp = strtotime($sendCrate->created_at);
            $date = date('Y-m-d', $timestamp);
            if ($date < Carbon::today()->toDateString()) {
                $exists = 1;
                if ($sendCrate->type == 'send') {
                    foreach (json_decode($sendCrate->crates) as $key => $invoice) {
                        if ($invoice->crate_id == '193') {
                            $totalsendCrates193 = $totalsendCrates193 + (int)$invoice->qty;
                        }
                        if ($invoice->crate_id == '194') {
                            $totalsendCrates194 = $totalsendCrates194 + (int)$invoice->qty;
                        }
                        if ($invoice->crate_id == '195') {
                            $totalsendCrates195 = $totalsendCrates195 + (int)$invoice->qty;
                        }
                        if ($invoice->crate_id == '196') {
                            $totalsendCrates196 = $totalsendCrates196 + (int)$invoice->qty;
                        }
                        if ($invoice->crate_id == '197') {
                            $totalsendCrates197 = $totalsendCrates197 + (int)$invoice->qty;
                        }

                    }
                }
                if ($sendCrate->type == 'return') {
                    if ($sendCrate->crates_approve != null) {
                        foreach (json_decode($sendCrate->crates_approve) as $key => $invoice) {
                            if ($invoice->crate_id == '193') {
                                $totalreturnCrates193 = $totalreturnCrates193 + (int)$invoice->qty;
                            }
                            if ($invoice->crate_id == '194') {
                                $totalreturnCrates194 = $totalreturnCrates194 + (int)$invoice->qty;
                            }
                            if ($invoice->crate_id == '195') {
                                $totalreturnCrates195 = $totalreturnCrates195 + (int)$invoice->qty;
                            }
                            if ($invoice->crate_id == '196') {
                                $totalreturnCrates196 = $totalreturnCrates196 + (int)$invoice->qty;
                            }
                            if ($invoice->crate_id == '197') {
                                $totalreturnCrates197 = $totalreturnCrates197 + (int)$invoice->qty;
                            }

                        }
                    }
                }

            }
            $loadingHeaderData = LoadingHeader::find($sendCrate->ref_id);
            array_push($totalDetailsHeadersArray, ['agent_name' => $loadingHeaderData->salesOrder->salesRep->name_with_initials, 'agent_territory' => $loadingHeaderData->salesOrder->salesRep->territory, 'loading_header_id' => '', 'loading_header_note' => '', 'category' => '']);
        }

        $totaldiffCrates193 = $totalsendCrates193 - $totalreturnCrates193;
        $totaldiffCrates194 = $totalsendCrates194 - $totalreturnCrates194;
        $totaldiffCrates195 = $totalsendCrates195 - $totalreturnCrates195;
        $totaldiffCrates196 = $totalsendCrates196 - $totalreturnCrates196;
        $totaldiffCrates197 = $totalsendCrates197 - $totalreturnCrates197;
        $totalcratesArray[0] = $totaldiffCrates193;
        $totalcratesArray[1] = $totaldiffCrates194;
        $totalcratesArray[2] = $totaldiffCrates195;
        $totalcratesArray[3] = $totaldiffCrates196;
        $totalcratesArray[4] = $totaldiffCrates197;
        // var_dump($totaldiffCrates193." | ",$totaldiffCrates194." | ",$totaldiffCrates195." | ".$totaldiffCrates196." | ".$totaldiffCrates197);
        //----------------------------------------------------------------------------------------------------------------------------------------

        $totalsendHeadersArray = [];
        $totalreturnHeadersArray = [];
        $sendViewCrates = CratesTransaction::where('agent_id', '=', $agentId)->get();
        foreach ($sendViewCrates as $key => $value) {
            $timestamp2 = strtotime($value->created_at);
            $date2 = date('Y-m-d', $timestamp2);
            if ($date2 == Carbon::today()->toDateString()) {
                $exists = 2;
                $totalsendCrates193 = 0;
                $totalsendCrates194 = 0;
                $totalsendCrates195 = 0;
                $totalsendCrates196 = 0;
                $totalsendCrates197 = 0;

                $totalreturnCrates193 = 0;
                $totalreturnCrates194 = 0;
                $totalreturnCrates195 = 0;
                $totalreturnCrates196 = 0;
                $totalreturnCrates197 = 0;

                $totaldiffCrates193 = 0;
                $totaldiffCrates194 = 0;
                $totaldiffCrates195 = 0;
                $totaldiffCrates196 = 0;
                $totaldiffCrates197 = 0;
                // $data1 = CratesTransaction::where('ref_id','=',$sendViewCrate->ref_id)->get();
                // foreach ($data1 as $key => $value) {
                if ($value->type == 'send') {
                    foreach (json_decode($value->crates) as $key => $invoice) {
                        if ($invoice->crate_id == '193') {
                            $totalsendCrates193 = $totalsendCrates193 + (int)$invoice->qty;
                        }
                        if ($invoice->crate_id == '194') {
                            $totalsendCrates194 = $totalsendCrates194 + (int)$invoice->qty;
                        }
                        if ($invoice->crate_id == '195') {
                            $totalsendCrates195 = $totalsendCrates195 + (int)$invoice->qty;
                        }
                        if ($invoice->crate_id == '196') {
                            $totalsendCrates196 = $totalsendCrates196 + (int)$invoice->qty;
                        }
                        if ($invoice->crate_id == '197') {
                            $totalsendCrates197 = $totalsendCrates197 + (int)$invoice->qty;
                        }

                    }
                    $loadingHeaderData = LoadingHeader::find($value->ref_id);
                    array_push($totalsendHeadersArray, ['loading_header_id' => $value->ref_id, 'loading_header_note' => $loadingHeaderData->invoice_number, 'category' => $loadingHeaderData->salesOrder->template->category->name, 'totalsendCrates193' => $totalsendCrates193, 'totalsendCrates194' => $totalsendCrates194, 'totalsendCrates195' => $totalsendCrates195, 'totalsendCrates196' => $totalsendCrates196, 'totalsendCrates197' => $totalsendCrates197]);
                }
                if ($value->type == 'return') {
                    if ($value->crates_approve != null) {
                        foreach (json_decode($value->crates_approve) as $key => $invoice) {
                            if ($invoice->crate_id == '193') {
                                $totalreturnCrates193 = $totalreturnCrates193 + (int)$invoice->qty;
                            }
                            if ($invoice->crate_id == '194') {
                                $totalreturnCrates194 = $totalreturnCrates194 + (int)$invoice->qty;
                            }
                            if ($invoice->crate_id == '195') {
                                $totalreturnCrates195 = $totalreturnCrates195 + (int)$invoice->qty;
                            }
                            if ($invoice->crate_id == '196') {
                                $totalreturnCrates196 = $totalreturnCrates196 + (int)$invoice->qty;
                            }
                            if ($invoice->crate_id == '197') {
                                $totalreturnCrates197 = $totalreturnCrates197 + (int)$invoice->qty;
                            }

                        }
                        $loadingHeaderData = LoadingHeader::find($value->ref_id);
                        array_push($totalreturnHeadersArray, ['loading_header_id' => $value->ref_id, 'loading_header_note' => $loadingHeaderData->invoice_number, 'category' => $loadingHeaderData->salesOrder->template->category->name, 'totalreturnCrates193' => $totalreturnCrates193, 'totalreturnCrates194' => $totalreturnCrates194, 'totalreturnCrates195' => $totalreturnCrates195, 'totalreturnCrates196' => $totalreturnCrates196, 'totalreturnCrates197' => $totalreturnCrates197]);
                    } else {
                        $loadingHeaderData = LoadingHeader::find($value->ref_id);
                        array_push($totalreturnHeadersArray, ['loading_header_id' => $value->ref_id, 'loading_header_note' => $loadingHeaderData->invoice_number, 'category' => $loadingHeaderData->salesOrder->template->category->name, 'totalreturnCrates193' => 0, 'totalreturnCrates194' => 0, 'totalreturnCrates195' => 0, 'totalreturnCrates196' => 0, 'totalreturnCrates197' => 0]);
                    }
                    // var_dump($value->id);
                }
                // }
                // $totaldiffCrates193 = $totalsendCrates193 - $totalreturnCrates193;
                // $totaldiffCrates194 = $totalsendCrates194 - $totalreturnCrates194;
                // $totaldiffCrates195 = $totalsendCrates195 - $totalreturnCrates195;
                // $totaldiffCrates196 = $totalsendCrates196 - $totalreturnCrates196;
                // $totaldiffCrates197 = $totalsendCrates197 - $totalreturnCrates197;
                $loadingHeaderData = LoadingHeader::find($value->ref_id);
                array_push($totalHeadersArray, ['agent_name' => $loadingHeaderData->salesOrder->salesRep->name_with_initials, 'agent_territory' => $loadingHeaderData->salesOrder->salesRep->territory, 'loading_header_id' => $value->ref_id, 'loading_header_note' => $loadingHeaderData->invoice_number, 'category' => $loadingHeaderData->salesOrder->template->category->name]);

            }
        }
        if ($exists == 0) {
            $returnData[0] = $exists;
            $returnData[1] = [];
            $returnData[2] = [];
            $returnData[3] = [];
            $returnData[4] = [];
        } else {
            if ($exists == 1) {
                $totalHeadersArray = $totalDetailsHeadersArray;
                $returnData[0] = $exists;
                $returnData[1] = $totalHeadersArray;
                $returnData[2] = $totalcratesArray;
                $returnData[3] = $totalreturnHeadersArray;
                $returnData[4] = $totalsendHeadersArray;
                return $returnData;
            } else {

                $returnData[0] = $exists;
                $returnData[1] = $totalHeadersArray;
                $returnData[2] = $totalcratesArray;
                $returnData[3] = $totalreturnHeadersArray;
                $returnData[4] = $totalsendHeadersArray;
                return $returnData;
            }

        }
    }


    public function cratesInquireData($agentId)
    {
        $allArray = $this->getCratesInquireData($agentId);
        $exists = $allArray[0];
        if ($exists == 0) {
            // return redirect(route('crates-inquire.index'))->with([
            //     'error' => true,
            //     'error.title' => 'No Crates',
            //     'error.message' => 'Agent has not crates'
            // ]);
        } else {
            if ($exists == 1) {
                //   $totalHeadersArray = $allArray[1];
                //   $totalcratesArray = $allArray[2];
                //   $totalreturnHeadersArray = $allArray[3];
                //   $totalsendHeadersArray = $allArray[4];
                // return view('agents.crates-inquire-print-invoice', compact('agentId','totalcratesArray','totalHeadersArray','totalreturnHeadersArray','totalsendHeadersArray'));
                return $allArray;
            } else {
                // $totalHeadersArray = $allArray[1];
                // $totalcratesArray = $allArray[2];
                // $totalreturnHeadersArray = $allArray[3];
                // $totalsendHeadersArray = $allArray[4];
                // // var_dump($totalsendHeadersArray);
                // // $invoices = $this->cratesInquireData($agentId);
                // return view('agents.crates-inquire-print-invoice', compact('agentId','totalcratesArray','totalHeadersArray','totalreturnHeadersArray','totalsendHeadersArray'));
                return $allArray;
            }

        }
    }

    public function cratesInquirePrint($agentId)
    {
        $allArray = [];
        $exists = 0;

        $allArray = $this->getCratesInquireData($agentId);
        $exists = $allArray[0];
        if ($exists == 0) {
            return redirect(route('crates-inquire.index'))->with([
                'error' => true,
                'error.title' => 'No Crates',
                'error.message' => 'Agent hasnot crates'
            ]);
        } else {
            if ($exists == 1) {
                $totalHeadersArray = $allArray[1];
                $totalcratesArray = $allArray[2];
                $totalreturnHeadersArray = $allArray[3];
                $totalsendHeadersArray = $allArray[4];
                return view('agents.crates-inquire-print', compact('agentId', 'totalcratesArray', 'totalHeadersArray', 'totalreturnHeadersArray', 'totalsendHeadersArray'));

            } else {
                $totalHeadersArray = $allArray[1];
                $totalcratesArray = $allArray[2];
                $totalreturnHeadersArray = $allArray[3];
                $totalsendHeadersArray = $allArray[4];
                // var_dump($totalsendHeadersArray);
                // $invoices = $this->cratesInquireData($agentId);
                return view('agents.crates-inquire-print', compact('agentId', 'totalcratesArray', 'totalHeadersArray', 'totalreturnHeadersArray', 'totalsendHeadersArray'));
            }

        }

    }

    public function cratesInquirePrintInvoice($agentId)
    {
        $allArray = [];
        $exists = 0;

        $allArray = $this->getCratesInquireData($agentId);
        $exists = $allArray[0];
        if ($exists == 0) {
            return redirect(route('crates-inquire.index'))->with([
                'error' => true,
                'error.title' => 'No Crates',
                'error.message' => 'Agent has not crates'
            ]);
        } else {
            if ($exists == 1) {
                $totalHeadersArray = $allArray[1];
                $totalcratesArray = $allArray[2];
                $totalreturnHeadersArray = $allArray[3];
                $totalsendHeadersArray = $allArray[4];
                return view('agents.crates-inquire-print-invoice', compact('agentId', 'totalcratesArray', 'totalHeadersArray', 'totalreturnHeadersArray', 'totalsendHeadersArray'));

            } else {
                $totalHeadersArray = $allArray[1];
                $totalcratesArray = $allArray[2];
                $totalreturnHeadersArray = $allArray[3];
                $totalsendHeadersArray = $allArray[4];
                // var_dump($totalsendHeadersArray);
                // $invoices = $this->cratesInquireData($agentId);
                return view('agents.crates-inquire-print-invoice', compact('agentId', 'totalcratesArray', 'totalHeadersArray', 'totalreturnHeadersArray', 'totalsendHeadersArray'));
            }

        }

    }
}
