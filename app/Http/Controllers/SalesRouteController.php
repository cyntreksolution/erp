<?php

namespace App\Http\Controllers;

use App\Services\APIService;
use http\Env\Response;
use Illuminate\Http\Request;
use function GuzzleHttp\Promise\all;

class SalesRouteController extends Controller
{
    protected $client;

    public function __construct()
    {
        $this->client = new APIService();
    }

    public function getSalesRoutes()
    {
        $agents = collect($this->client->getAllAgents())->pluck('name_with_initials','id');
        $routes = $this->client->getAllRoutes();
//        dd($routes);
        return view('api.routes.list', compact('routes','agents'));
    }

    public function create(Request $request)
    {
        $dataset = $request->except('_token');
        $route = $this->client->createSalesRoute($dataset);
        return $route->status == 'success' ? 'true' : 'false';
    }

    public function delete(Request $request){
        $this->client->deleteSalesRoute($request->id);
        return 'true';
    }

    public function update(Request $request){
        $dataset = $request->except('_token','id');
        $route = $this->client->updateSalesRoute($request->id,$dataset);
        return $route->status == 'success' ? 'true' : 'false';
    }
}
