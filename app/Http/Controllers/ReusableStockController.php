<?php

namespace App\Http\Controllers;

use App\ReusableStock;
use App\ReusableUsage;
use App\ReusableUsageItems;
use App\Traits\RawMaterialLog;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RawMaterialManager\Models\RawMaterial;
use StockManager\Classes\StockTransaction;

class ReusableStockController extends Controller
{
    use RawMaterialLog;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:reusable_stock-index', ['only' => ['index','tableData']]);
        $this->middleware('permission:ReusableWastage-list', ['only' => ['list']]);


    }
    public function index(Request $request)
    {
        $data = ReusableStock::query();
        $rt = $request->received_type;
        if (!empty($request->filter)) {
            if (!empty($request->type)) {
                $data = $data->where('reusable_type','=',$request->type);
            }

            if (!empty($request->received_type)){
                $data = $data->where('received_type','=',$request->received_type);
            }

            if (!empty($request->date)) {
                $dates = explode(' - ', $request->date);
                $start = $dates[0];
                $end = $dates[1];
                $data->where('created_at', '>=', Carbon::parse($start)->format('Y-m-d'))
                    ->where('created_at', '<=', Carbon::parse($end)->addDay()->format('Y-m-d'));
            }else{
//                $data = $data->where('created_at', '<', Carbon::now()->subMinutes(15));
            }

            if (!empty($request->status)) {
                $data = $data->where('status','=',$request->status);
            }
        } else {
            $data = $data->where('created_at', '<', Carbon::now()->subMinutes(15))->where('status','=','pending');
        }
        $data = $data->get();
        return view('reusable-list', compact('data','rt'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $reusable_type = $request->reusable_type;
        $qty = $request->qty;
        $system_qty = $request->system_qty;
        $reusable_stock_id = $request->reusable_stock_id;




        $purchasing_item = null;
        switch ($reusable_type){
            case 2:
                $purchasing_item = RawMaterial::find(90);
                break;
            case 3:
                $purchasing_item = RawMaterial::find(92);
                break;
            case 4:
                $purchasing_item = RawMaterial::find(285);
                break;

        }



        $reusableUsage = new ReusableUsage();
        $reusableUsage->qty = $qty;
        $reusableUsage->system_qty = $system_qty;
        $reusableUsage->created_by =  Auth::user()->id;
        $reusableUsage->save();

        $reusable_items = ReusableStock::whereIn('id',$reusable_stock_id)->get();
        foreach ($reusable_items as $reusable_item){
            $reusable_item->status ='used';
            $reusable_item->save();

            $reusableUsageItem = new ReusableUsageItems();
            $reusableUsageItem->reusable_usage_id = $reusableUsage->id;
            $reusableUsageItem->reusable_stock_id =$reusable_item->id;
            $reusableUsageItem->save();
        }

        StockTransaction::rawMaterialStockTransaction(1, 0, $purchasing_item->raw_material_id, $qty, 0, 1);
        StockTransaction::updateRawMaterialAvailableQty(1, $purchasing_item->raw_material_id, $qty);
        $this->recordRawMaterialOutstanding($purchasing_item,$qty,'Reusable Wastage',$reusableUsage->id,'App\\ReusableUsage',13,1,-1);


        return redirect(route('reusable.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
