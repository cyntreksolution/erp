<?php

namespace App\Http\Controllers;

use App\LoadingHeader;
use App\Outlet;
use App\UtilityBill;
use Carbon\Carbon;
use CashLockerManager\Classes\CashTransaction;
use CashLockerManager\Models\CashBookTransaction;
use CashLockerManager\Models\loanlease;
use CashLockerManager\Models\main_desc_cashlocker;
use CashLockerManager\Models\VehicleEquipment;
use EmployeeManager\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SalesRepManager\Models\SalesRep;
use SupplierManager\Models\Supplier;
use VehicleManager\Models\Vehicle;

class CashBookInquiryController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:cash_book_inquiry-index', ['only' => ['index','tableData']]);
        $this->middleware('permission:CashBookInquiry-list', ['only' => ['list']]);

    }
    public function index(){
        $data = main_desc_cashlocker::distinct('sub_desc')->get()->pluck('sub_desc','id');
        $emp = Employee::OrderBy('full_name')->get()->pluck('full_name','id');
        $main_descs = CashBookTransaction::groupBy('main_desc')->pluck('main_desc','main_desc');
        $categories = main_desc_cashlocker::groupBy('category')->pluck('category','category');
        $data2 = [];


        return view('account.cash_list')->with([
            'main_descs'=>$main_descs,
            'datas' => $data,
            'emp' => $emp,
            'data2' => $data2,
            'categories'=>$categories
        ]);

    }




    public function tableData(Request  $request){
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'loading_date', 'total', 'agent_id'];
        $order_column = $columns[$order_by[0]['column']];

        $order_column = 'created_at';
        $order_by_str = 'DESC';

        $stat_data = CashBookTransaction::query();
        $stat_data2 = CashBookTransaction::query();

        $invoiceCount = CashBookTransaction::where('created_at','>=',Carbon::today()->format('Y-m-d'))->count();
        $invoices = CashBookTransaction::query();

        $filter_count =$invoiceCount;

        if ($request->filter == true) {

            $date_range = $request->date_range;
            $main_desc = $request->maindesc;
            $sub_desc = $request->sub_desc;
            $paytype = $request->paytype;
            $category = $request->category;
            $desc1 = $request->desc1;



            $stst = $stat_data
                ->select(DB::raw('SUM(amount) as  income_sum'))
                ->filterData($date_range,$main_desc,$sub_desc,$paytype,$category,$desc1)
                ->whereType(1)
                ->first();

            $stst2 = $stat_data2
                ->select(DB::raw('SUM(amount) as outcome_sum '))
                ->filterData($date_range,$main_desc,$sub_desc,$paytype,$category,$desc1)
                ->whereType(2)
                ->first();

            $invoices = $invoices->filterData($date_range,$main_desc,$sub_desc,$paytype,$category,$desc1);

            $invoiceCount= $filter_count = $invoices->count();


        } elseif (is_null($search) || empty($search)) {
            $invoices = $invoices->where('created_at','>=',Carbon::today()->format('Y-m-d'));
            $stst = $stat_data
                ->select(DB::raw('SUM(amount) as  income_sum '))
                ->whereType(1)
                ->where('created_at','=',Carbon::today())
                ->first();

            $stst2 = $stat_data2
                ->select(DB::raw('SUM(amount) as  outcome_sum '))
                ->where('created_at','=',Carbon::today())
                ->whereType(2)
                ->first();


        } else {
            $invoices = $invoices->searchData($search);
            $invoiceCount= $filter_count = $invoices->count();
        }


        $invoices = $invoices->tableData($order_column, $order_by_str, $start, $length)->get();

        $income_sum = $stst->income_sum;
        $outcome_sum = $stst2->outcome_sum;
        $balance = $income_sum + $outcome_sum;
        $data = [];
        $i = 0;


        foreach ($invoices as $key => $invoice) {

            $data[$i] = array(
                Carbon::parse($invoice->created_at)->format('Y-m-d H:m:s'),
                $invoice->description,
                ($invoice->type==1)?$invoice->amount:'-',
                ($invoice->type==2)?$invoice->amount:'-',
                $invoice->balance,
            );
            $i++;
        }

        if ($invoiceCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($invoiceCount),
            "recordsFiltered" => intval($filter_count),
            "data" => $data,
            "income_sum" => number_format($income_sum, 2),
            "outcome_sum" => number_format($outcome_sum, 2),
            "balance" => number_format($balance, 2),
        ];

        return json_encode($json_data);
    }

    public function tableSum(Request  $request){

        $date_range = $request->date_range;
        $data = [];
        $i = 0;


        $dates = explode(' - ', $date_range);
            $start = $dates[0];
            $end = $dates[1];

        $op = CashBookTransaction::where('created_at', '>=', Carbon::parse($start)->format('Y-m-d'))->orderBy('id', 'ASC')->first();
        $opBal = (($op->balance)-($op->amount));
        $endBal = CashBookTransaction::where('created_at', '<=', Carbon::parse($end)->addDay()->format('Y-m-d'))->orderBy('id', 'DESC')->first();

        $maindesc = CashBookTransaction::groupby('main_desc')->groupby('main_desc_id')->distinct()->orderBy('main_desc', 'ASC')->orderBy('main_desc_id', 'ASC')->get();
        foreach ($maindesc as $key => $mdesc){
                if(!empty($mdesc->main_desc)){

                            $sumTot = CashBookTransaction::select(DB::raw('SUM(amount) as subTot '))
                                    ->where('created_at', '>=', Carbon::parse($start)->format('Y-m-d'))
                                    ->where('created_at', '<=', Carbon::parse($end)->addDay()->format('Y-m-d'))
                                    ->where('main_desc','=',$mdesc->main_desc)
                                    ->where('main_desc_id','=',$mdesc->main_desc_id)
                                    ->first();

                            if(empty($sumTot->subTot)){
                                $sumTot = 0 ;
                            }else{

                            $subname = main_desc_cashlocker::find($mdesc->main_desc_id);

                                $data[$i] = array(
                                    $mdesc->main_desc,
                                    $subname->sub_desc,
                                    ($sumTot->subTot >= 0)?$sumTot->subTot:'-',
                                    ($sumTot->subTot < 0)?$sumTot->subTot:'-',
                                );

                                $i++;
                            }

                }
        }

        $json_data = [
            //"draw" => intval(1000),
            //"recordsTotal" => intval($count),
           // "recordsFiltered" => intval($count),
            "data" => $data,


        ];


       // return view('account.cash_sum', compact('data','date_range'));
        return view('account.cash_sum')->with([
            'datas' => $data,
            'date_range'=>$date_range,
            "opBal" => $opBal,
            "endBal" => $endBal->balance,
        ]);

    }


    public function getSubCashCategory(Request $request){
        $main_cat =$request->category_name;
        $cash_flow =$request->cash_flow;

        $sub =DB::table('main_desc_cashlockers');

        if (!empty($main_cat) ) {
            $sub->where('main_desc','=',$main_cat);
        }
        if (!empty($cash_flow)) {
            $sub= $sub->where('type','=',$cash_flow);
        }

        return $sub->get();
    }

    public function getSubCashCategoryType(Request $request){
        $main_cat =$request->subdesc;
        $sub = DB::table('main_desc_cashlockers')->where('id',$request->subdesc);


        return $sub->get();
    }

    public function getCategoryData(Request $request)
    {


        $desc1 = $request->cat;

        $data2 = null;
        switch ($desc1) {
            case 'Agent';
                $data2 = SalesRep::pluck('name_with_initials','id');
                break;
            case 'Agents';
                $data2 = SalesRep::pluck('name_with_initials','id');
                break;
            case 'Vehicle';
                $data2 = Vehicle::pluck('name','id');
                break;
            case 'Employees';
                $data2 =Employee::pluck('full_name','id');
                break;
            case 'Equipments';
                $data2 =VehicleEquipment::pluck('eq_desc','id');
                break;
            case 'LoanLease';
                $data2 =loanlease::pluck('loan_desc','id');
                break;
            case 'Supliers';
                $data2 =Supplier::pluck('supplier_name','id');
                break;
            case 'UtilityBills';
                $data2 =UtilityBill::pluck('description','id');
                break;
            case 'Outlet';
                $data2 =Outlet::pluck('name','id');
                break;
        }
        return !empty($data2)?$data2:'1';
    }

}
