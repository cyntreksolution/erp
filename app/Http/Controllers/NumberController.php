<?php

namespace App\Http\Controllers;

use App\Number;
use App\User;
use App\UserNumber;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NumberController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:number-index', ['only' => ['index']]);
        $this->middleware('permission:number-list', ['only' => ['list']]);
        $this->middleware('permission:number-store', ['only' => ['store']]);
        $this->middleware('permission:number-delete', ['only' => ['deleteUserNumber']]);

    }
    public function index()
    {
        $numbers = Number::all();
        $number_list = Number::pluck('number', 'id');
        $users = User::pluck('email', 'id');
        $list = UserNumber::all();
        return view('numbers.create', compact('numbers', 'number_list', 'users', 'list'));
    }

    public function store(Request $request)
    {
        $num = new Number();
        $num->number = $request->number;
        $num->description = $request->description;
        $num->save();
        return redirect()->route('numbers.index')->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => 'You have entered a number',
        ]);
    }

    public function updateNumber(Request $request)
    {

    }

    public function changeStatus(Request $request){
       $number_data = $request->id;
       $type = $request->type;
       $data =UserNumber::find($number_data);
       $data->status = $type;
       $data->save();
       return 1;

    }

    public function deleteNumber(Request $request)
    {
        $number_id = $request->id;
        $numbers = UserNumber::whereNumberId($number_id)->get();
        if (!empty($numbers) && $numbers->count() > 0) {
            return redirect()->back()->with([
                'error' => true,
                'error.title' => 'Sorry',
                'error.message' => 'You have assigned this number to users',
            ]);
        }

        $number = Number::find($request->id);
        $number->delete();
        return redirect()->route('numbers.index')->with([
            'success' => true,
            'success.title' => 'Oh !',
            'success.message' => 'You have deleted a number',
        ]);

    }


    public function assign(Request $request)
    {
        $user = User::find($request->user);
        $number = Number::find($request->number);

        $exsist = UserNumber::whereUserId($user->id)->whereNumberId($number->id)->get();
        if (!empty($exsist) && $exsist->count() > 0) {
            return redirect()->route('numbers.index')->with([
                'error' => true,
                'error.title' => 'Sorry',
                'error.message' => 'Record Already Exist !',
            ]);
        }
        $con = new UserNumber();
        $con->user_id = $user->id;
        $con->number_id = $number->id;
        $con->created_by = Auth::user()->id;
        $con->save();
        return redirect()->route('numbers.index');
    }

    public function deleteUserNumber(Request $request)
    {
        $list = UserNumber::find($request->id);
        if ($role = Sentinel::check()->roles[0]->slug == 'owner') {
            $list->delete();
        } else {
            return redirect()->back()->with([
                'error' => true,
                'error.title' => 'Sorry',
                'error.message' => 'You Have Not Permissions To Delete Records',
            ]);
        }
        return redirect()->route('numbers.index');
    }
}
