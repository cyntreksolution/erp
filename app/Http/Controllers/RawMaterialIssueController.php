<?php

namespace App\Http\Controllers;

use App\RawInquiry;
use App\RawIssueDataList;
use App\SemiIssueDataList;
use App\Traits\RawMaterialLog;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use EmployeeManager\Models\Employee;
use EndProductBurdenManager\Models\BurdenPacking;
use EndProductBurdenManager\Models\BurdenRaw;
use EndProductBurdenManager\Models\EndProductBurden;
use EndProductBurdenManager\Models\EndProductBurdenHeader;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use RawMaterialManager\Models\RawMaterial;
use RawMaterialManager\Models\RawRequestTable;
use SalesRepManager\Models\SalesRep;
use StockManager\Classes\StockTransaction;
use App\Traits\CratesStoreLog;
use SupplierManager\Models\Supplier;

class RawMaterialIssueController extends Controller
{
    use RawMaterialLog, CratesStoreLog;

    function __construct()
    {
        $this->middleware('permission:raw_materials-index', ['only' => ['index','issueForBurden']]);
        $this->middleware('permission:raw_materials_product-index', ['only' => ['rawIndex']]);
        $this->middleware('permission:IssueRawMaterialsEndProduct-list', ['only' => ['list']]);
        $this->middleware('permission:RequestRawMaterials-list', ['only' => ['rlist']]);
        $this->middleware('permission:raw_materials-mergeRaw', ['only' => ['mergeRaw']]);
        $this->middleware('permission:raw_materials-store', ['only' => ['store']]);

    }
    public function index()
    {
        $raw_materials1 = RawMaterial::where('type', '!=', 5)->get()->pluck('name', 'raw_material_id');
        $raw_materials = RawMaterial::get()->pluck('name', 'raw_material_id');
        $raw_materials2 = RawMaterial::where('type', '=', 9)->get()->pluck('name', 'raw_material_id');
        $merges = EndProductBurdenHeader::whereIssueRaw(0)->get()->pluck('serial', 'id');
        $transaction = RawInquiry::whereHas('raw_material', function ($q) {
            $q->where('type', '<>', 5);
        })->where('created_at', 'like', Carbon::today()->format('Y-m-d') . '%')->OrderByDesc('id')->limit(25)->get();

        $supplier = Supplier::orderBy('supplier_name')->get()->pluck('supplier_name', 'id');

        $custo = EndProductBurden::where('created_at', '>', Carbon::today()->subDays(3)->format('Y-m-d') . '%')->orderBy('created_at', 'desc')->whereHas('endProduct', function ($query) {
            $query->where('customize', '=', 1);
        })->get();
        $return_b = EndProductBurden::where('created_at', 'like', Carbon::today()->subDays(10)->format('Y-m-d') . '%')->orderBy('created_at','desc')->pluck('serial','id');


        $employees = Employee::get()->pluck('full_name', 'id');
        return view('raw-material-issue.index', compact('return_b','merges', 'raw_materials', 'employees', 'raw_materials2', 'raw_materials1', 'transaction', 'supplier', 'custo'));
    }

    public function rawIndex()
    {
        $raw_materials = RawMaterial::where('type', '!=', 5)->get()->pluck('name', 'raw_material_id');

      //  $transaction = RawRequestTable::where('created_at', 'like', Carbon::today()->format('Y-m-d') . '%')->OrderByDesc('id')->get();
        $transaction = RawRequestTable::where('status', '<=', 0)->OrderByDesc('id')->get();


        $items = RawRequestTable::select('ref')->distinct()->where('status', '<=', 0)->get();



        $custo = EndProductBurden::where('created_at', '>', Carbon::today()->subDays(3)->format('Y-m-d') . '%')->orderBy('created_at', 'desc')->whereHas('endProduct', function ($query) {
            $query->where('customize', '=', 1);
        })->get();
        $return_b = EndProductBurden::where('created_at', 'like', Carbon::today()->subDays(10)->format('Y-m-d') . '%')->orderBy('created_at','desc')->pluck('serial','id');

        $employees = Employee::get()->pluck('full_name', 'id');
        return view('raw-material-issue.request_index', compact('return_b', 'raw_materials', 'transaction',  'custo','items','employees'));
    }

    public function issueForBurden(Request $request, EndProductBurdenHeader $burden)
    {
        $items = BurdenRaw::selectRaw('COALESCE(sum(qty), 0) as qty,raw_material_id')->whereMergeId($burden->id)->groupBy('raw_material_id')->get();
        $packing = BurdenPacking::selectRaw('COALESCE(sum(qty), 0) as qty,raw_material_id')->whereMergeId($burden->id)->groupBy('raw_material_id')->get();

//        $merged = (!empty($packing) && $packing->count() > 0)? $packing->merge($items) : $items;
        $merged = collect();
        $merged = $merged->merge($packing);
        $merged = $merged->merge($items);

//        $merged = (!empty($packing) && $packing->count() > 0)? 1 :2;
        $data = [];

        foreach ($merged->all() as $raw) {
            $mat = RawMaterial::whereRawMaterialId($raw->raw_material_id)
                ->withoutGlobalScopes(['type'])
                ->first();
            $available_qty = ($raw->measurement == 'unit') ? ($mat->available_qty) . ' units' : $mat->available_qty;
            $rawmate = ['id' => $mat->raw_material_id, 'raw_material_name' => $mat->name, 'qty' => $raw->qty, 'available_qty' => $available_qty];
            array_push($data, $rawmate);
        }
        return $data;
    }

    public function store(Request $request)
    {
        $type = $request->type;

        if ($type == '0') {
            $merge_serial = $request->merge_serial;

            $merge = EndProductBurdenHeader::find($merge_serial);
            $ebid = null;
            foreach (json_decode($merge->burdens) as $key => $end_bid){
                $ebid = $end_bid;

            }


            $items = BurdenRaw::whereMergeId($merge_serial)->get();
            $packing = BurdenPacking::whereMergeId($merge_serial)->get();

            $raw_count = BurdenRaw::where('merge_id', '=', $merge_serial)
                ->where('qty', '!=', 0)
                ->count();
            $pack_count = BurdenPacking::where('merge_id', '=', $merge_serial)
                ->where('qty', '!=', 0)
                ->count();


            $merge->raw_count = $raw_count;
            $merge->pack_count = $pack_count;
            $merge->emp_measurer_id = $request->emp;
            $merge->status = 2;
            $merge->issue_raw = 1;
            $merge->save();

            $arr = [];

            $raw_cost = 0;

            foreach ($items as $item) {



                $raw = RawMaterial::find($item->raw_material_id);
                $raw_material_price = $raw->price / $raw->units_per_packs;
                $raw_cost = $raw_cost + ($raw_material_price * $item->qty);

                $chkdouble = RawInquiry::where('raw_material_id','=',$item->raw_material_id)->where('desc_id','=',4)->where('reference','=',$merge_serial)->where('Issued_ref_id','=',$item->end_product_burden_id)->first();

                if(empty($chkdouble)) {
                    StockTransaction::rawMaterialStockTransaction(2, 0, $item->raw_material_id, $item->qty, $merge_serial, 1);
                    StockTransaction::updateRawMaterialAvailableQty(2, $item->raw_material_id, $item->qty);
                    $raw_material = RawMaterial::find($item->raw_material_id);
                    if(empty($item->end_product_burden_id)){
                        $this->recordRawMaterialOutstanding($raw_material, (($item->qty) * (-1)), 'End Burden Request-Raw', $merge_serial, 'EndProductBurdenManager\\Models\\EndProductBurdenHeader', 4, 2, $ebid);

                    }else{
                        $this->recordRawMaterialOutstanding($raw_material, (($item->qty) * (-1)), 'End Burden Request-Raw', $merge_serial, 'EndProductBurdenManager\\Models\\EndProductBurdenHeader', 4, 2, $item->end_product_burden_id);

                    }
                     array_push($arr, json_encode($item));
                }
            }

            foreach ($packing as $item) {
                $raw = RawMaterial::find($item->raw_material_id);
                $raw_material_price = $raw->price / $raw->units_per_packs;
                $raw_cost = $raw_cost + ($raw_material_price * $item->qty);

                $chkdouble = RawInquiry::where('raw_material_id','=',$item->raw_material_id)->where('desc_id','=',5)->where('reference','=',$merge_serial)->first();

                if(empty($chkdouble)) {

                    StockTransaction::rawMaterialStockTransaction(2, 0, $item->raw_material_id, $item->qty, $merge_serial, 1);
                    StockTransaction::updateRawMaterialAvailableQty(2, $item->raw_material_id, $item->qty);
                    $raw_material = RawMaterial::find($item->raw_material_id);
                    $this->recordRawMaterialOutstanding($raw_material, (($item->qty) * (-1)), 'End Burden Request-Packing', $merge_serial, 'EndProductBurdenManager\\Models\\EndProductBurdenHeader', 5, 2, $ebid);
                    array_push($arr, json_encode($item));
                }
            }

            $burdens = json_decode($merge->burdens);
            foreach ($burdens as $burden) {
                $burden = EndProductBurden::find($burden);
                $burden->issue_raw = 1;
                $burden->save();
            }

            $merge->status = 2;
            $burden->value = $raw_cost;
            $merge->save();


            $record = new RawIssueDataList();
            $record->raw_id = $request->raw_material;
            $record->type = $type;
            $record->qty = $request->qty;
            $record->data = json_encode($arr);
            $record->description = $request->description;
            $record->created_by = Auth::user()->id;
            $record->save();

            return redirect()->back()->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Raw Material Send'
            ]);
        } elseif ($type == '3') {

            $record = new RawIssueDataList();
            $record->raw_id = $request->raw_material_2;
            $record->type = $type;
            $record->qty = $request->qty_2;
            $record->employee_id = !empty($request->employee)?$request->employee:$request->emp;
            $record->created_by = Auth::user()->id;
            $record->save();


            StockTransaction::rawMaterialStockTransaction(2, 0, $request->raw_material_2, $request->qty_2, 0, 1);
            StockTransaction::updateRawMaterialAvailableQty(2, $request->raw_material_2,$request->qty_2);
            $raw_material = RawMaterial::find($request->raw_material_2);
            $this->recordRawMaterialOutstanding($raw_material, (($request->qty_2) * (-1)), 'Meals', $record->id, 'App\\RawIssueDataList', 6, 2, !empty($request->employee)?$request->employee:$request->emp);

            return redirect()->back()->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Raw Material Released !'
            ]);
        } else {
            $raw_mat_id = null;
            if ($type == 1) {
                $desc = 'Destroy or Expire in Store';
                $desc_id = 9;
                $raw_mat_id = $request->raw_material;
                $request_qty = $request->qty;
                $description = $request->description;

            } elseif ($type == 2) {
                $desc = 'Cleaning Purpose';
                $desc_id = 10;
                $raw_mat_id = $request->raw_material;
                $request_qty = $request->qty;
                $description = $request->description;
            } elseif ($type == 4) {
                $desc = 'Measuring Variance';
                $desc_id = 17;
                $raw_mat_id = $request->raw_material;
                $request_qty = $request->qty;
                $description = $request->description;
            } elseif ($type == 5) {
                $desc = 'Employee Deduction';
                $desc_id = 18;
                $raw_mat_id = $request->raw_material_1;
                $request_qty = $request->qty_1;
                $description = $request->description_1;
            } elseif ($type == 6) {
                $desc = 'Return to Supplier';
                $desc_id = 19;
                $raw_mat_id = $request->raw_material_3;
                $request_qty = $request->qty_3;
                $description = $request->description_3;
            } elseif ($type == 7) {
                $desc = 'Return From Burden Request';
                $desc_id = 20;
                $raw_mat_id = $request->raw_material_4;
                $request_qty = $request->qty_4;
                $description = $request->description_4;
            } elseif ($type == 8) {
                $desc = 'Tea Beverage & Extra Meals';
                $desc_id = 23;
                $raw_mat_id = $request->raw_material;
                $request_qty = $request->qty;
                $description = $request->description;
            }


            $record = new RawIssueDataList();
            $record->raw_id = $raw_mat_id;
            $record->type = $type;
            $record->qty = !empty($request_qty)?$request_qty:array_sum($request->customized_qty);
            $record->description = !empty($description)?$description:'Issued for customized cake';
            $record->created_by = Auth::user()->id;
            $record->save();


            $rawtype = RawMaterial::where('raw_material_id', '=', $raw_mat_id)->first();

            if ($rawtype->type == 5) {
                if ($type == 1) {
                    $this->recordCratesStoreLogOutstanding($rawtype, (($request_qty) * (-1)), 'Crates Destroyed', 0, 'RawMaterialIssueController\\Models\\IssueRaw', 2, 2, 0);
                    StockTransaction::rawMaterialStockTransaction(2, 0, $raw_mat_id, $request_qty, 0, 1);
                    StockTransaction::updateRawMaterialAvailableQty(2, $raw_mat_id, $request_qty);
                    $raw_material = RawMaterial::find($raw_mat_id);
                    $this->recordRawMaterialOutstanding($raw_material, (($request_qty) * (-1)), $desc, $record->id, 'App\\RawIssueDataList', $desc_id, 2, $record->id);
                }
            } else {
                /*if ($type == 2) {
                    $raw_qty = $request->customized_qty;
                    $burden_q = $request->customized_burden_id;
                    foreach ($raw_qty as $key => $q) {
                        if ($q > 0) {
                            $burden_id = $burden_q[$key];
                            StockTransaction::rawMaterialStockTransaction(2, 0, $raw_mat_id, $q, 0, 1);
                            StockTransaction::updateRawMaterialAvailableQty(2, $raw_mat_id, $q);
                            $raw_material = RawMaterial::find($raw_mat_id);
                            $this->recordRawMaterialOutstanding($raw_material, (($q) * (-1)), $desc, $burden_id, 'EndProductBurdenManager\\Models\\EndProductBurden', $desc_id, 2, $record->id);
                        }
                    }
                } else {*/
                
                    StockTransaction::rawMaterialStockTransaction(2, 0, $raw_mat_id, $request_qty, 0, 1);
                    StockTransaction::updateRawMaterialAvailableQty(2, $raw_mat_id, $request_qty);
                    $raw_material = RawMaterial::find($raw_mat_id);
                    $this->recordRawMaterialOutstanding($raw_material, (($request_qty) * (-1)), $desc, $record->id, 'App\\RawIssueDataList', $desc_id, 2, $record->id);

               // }

            }


            return redirect()->back()->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Raw Material Released !'
            ]);
        }

    }

    public function rawIssue(Request $request)
    {

        $type = $request->type;

            $raw_mat_id = null;
            if ($type == 1) {
                $desc = 'Production Purpose';
                $desc_id = 7;
                $raw_mat_id = $request->raw_id;
                $request_qty = $request->qty;
                $description = $request->ref;

            } elseif ($type == 2) {
                $desc = 'Customized Cake';
                $desc_id = 8;
                $raw_mat_id = $request->raw_id;
                $request_qty = $request->qty;
                $description = $request->ref;
            } elseif ($type == 3) {
                $desc = 'Refill Deep Fryers';
                $desc_id = 24;
                $raw_mat_id = $request->raw_id;
                $request_qty = $request->qty;
                $description = $request->ref;
            }
        $rc = RawIssueDataList::where('raw_id','=',$raw_mat_id)->where('description','=',$description)->first();

        $record = new RawIssueDataList();
            $record->raw_id = $raw_mat_id;
            $record->type = $type;
            $record->qty = $request->qty;
            $record->description = $description;
            $record->created_by = Sentinel::getUser()->id;


            if(empty($rc->employee_id)) {

                $record->save();


                StockTransaction::rawMaterialStockTransaction(2, 0, $raw_mat_id, $request_qty, 0, 1);
                StockTransaction::updateRawMaterialAvailableQty(2, $raw_mat_id, $request_qty);
                $raw_material = RawMaterial::find($raw_mat_id);
                $this->recordRawMaterialOutstanding($raw_material, (($request_qty) * (-1)), $desc, $record->id, 'App\\RawIssueDataList', $desc_id, 2, $description);


                $raw = RawRequestTable::where('ref', '=', $description)->get();

                foreach ($raw as $raws) {

                    $raws->status = 1;
                    $raws->measurer_id = $request->emp;
                    $raws->save();
                }



            return redirect()->back()->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Raw Material Released !'
            ]);
            }else{
                return redirect()->back()->with([
                    'danger' => true,
                    'danger.title' => 'Oh.. My God !',
                    'danger.message' => 'Already Exsist Entry'
                ]);
            }


    }


    public function print(Request $request,$ref)
    {

        $qty = 0;
        $rawRef = RawRequestTable::find($ref);
        $raw = RawRequestTable::where('ref', '=', $rawRef->ref)->get();

        foreach ($raw as $raws) {
            $qty = $qty + $raws->qty;
            $raws->status = 0;

            $raws->save();

        }


        return view('ppcc_print', compact('rawRef','qty'));
    }

    public function rawStore(Request $request)
    {

        $type = $request->type;

        if ($type == '0') {
            $record = new RawRequestTable();
            $record->ref = 'PP-' . Carbon::now()->format('ymdHis');
            $record->raw_id = $request->raw_material;
            $record->type = 1;//Production Purpose;
            $record->qty = $request->qty;
            $record->status = -1;
            $record->description = $request->description;
            $record->request_by = Auth::user()->id;
            $record->save();

            }elseif($type == '1') {

            $raw_qty = $request->customized_qty;
            $burden_q = $request->customized_burden_id;
            $refid = 'CC-' . Carbon::now()->format('ymdHis');
            foreach ($raw_qty as $key => $q) {
                if ($q > 0) {
                    $record = new RawRequestTable();
                    $record->ref = $refid;
                    $record->raw_id = $request->raw_material_c;
                    $record->type = 2;//Customized Cake;
                    $record->qty = $q;
                    $record->status = -1;
                    $record->description = $burden_q[$key];
                    $record->request_by = Auth::user()->id;
                    $record->save();
                }
            }
        }elseif($type == '2'){

            $record = new RawRequestTable();
            $record->ref = 'RD-' . Carbon::now()->format('ymdHis');
            $record->raw_id = $request->raw_material;
            $record->type = 3;//Refill Deep Fryers;
            $record->qty = $request->qty;
            $record->status = -1;
            $record->description = $request->description;
            $record->request_by = Sentinel::getUser()->id;
            $record->save();
        }


            return redirect()->back()->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Request Successfully !'
            ]);


    }



    public function mergeRaw(Request $request)
    {
        $merge = EndProductBurdenHeader::find($request->merge);
        $eb = json_decode($merge->burdens);
        $items = BurdenRaw::select('raw_material_id', DB::raw('SUM(qty) as qty'))->whereIn('end_product_burden_id', $eb)->groupBy('raw_material_id')->get();


      //  $pdf = PDF::loadView('raw_print', compact('items', 'merge'));
      //  $pdf->setPaper([0, 0, 220, 9999], 'portrait');
      //  return $pdf->stream();

        return view('raw_print', compact('items', 'merge'));
    }

    public function getBurdenList(Request $request)
    {

    }


}
