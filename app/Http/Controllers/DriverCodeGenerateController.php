<?php

namespace App\Http\Controllers;

use App\DriverCodes;
use EmployeeManager\Models\Employee;
use Illuminate\Http\Request;

class DriverCodeGenerateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:driver_code_generate-index', ['only' => ['index']]);
        $this->middleware('permission:DriverCodeGenerate-list', ['only' => ['list']]);

    }
    public function index()
    {
      $today = \Carbon\Carbon::today()->Format('y-m-d, h:m');
      $codeLists = DriverCodes::whereExpireAt($today)->get();
      return view('drivercode.drivercode_list',compact('codeLists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $employees = Employee::all();
      return view('drivercode.drivercode_create',compact('employees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $employeeDetails = Employee::whereId($request->Employee)->first();
      if($employeeDetails == false){
        return redirect(route('driverCodeGenerate.create'))->with([
            'error' => true,
            'error.title' => 'Error !',
            'error.message' => 'Employee Not Exists'
        ]);
      }else{
        $today = \Carbon\Carbon::today()->Format('y-m-d, h:m');
        $randomletter = substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 6);
        $codeRegister = DriverCodes::firstOrCreate([
                'employee_id' => $request->Employee,
                'expire_at' => $today
            ], [
                'code' => $randomletter
            ]);
            if($codeRegister->wasRecentlyCreated){
              return redirect(route('driverCodeGenerate.index'))->with([
                  'success' => true,
                  'success.title' => 'Congratulations !',
                  'success.message' => 'Driver Code Done!'
              ]);
            } else {
              return redirect(route('driverCodeGenerate.create'))->with([
                  'error' => true,
                  'error.title' => 'Error !',
                  'error.message' => 'Already exist'
              ]);
            }
      }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        echo "true";
    }
}
