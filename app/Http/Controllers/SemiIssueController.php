<?php

namespace App\Http\Controllers;

use App\SalesMergeBurdenGrade;
use App\SemiBurdenIssueData;
use App\SemiCustomizedCake;
use App\SemiInquiry;
use App\SemiIssueDataList;
use App\Traits\SemiFinishLog;
use BurdenManager\Models\BurdenGrade;
use Carbon\Carbon;
use EndProductBurdenManager\Models\BurdenPacking;
use EndProductBurdenManager\Models\BurdenRaw;
use EndProductBurdenManager\Models\BurdenSemi;
use EndProductBurdenManager\Models\EndProductBurden;
use EndProductBurdenManager\Models\EndProductBurdenHeader;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use RawMaterialManager\Models\RawMaterial;
use SemiFinishProductManager\Models\SemiFinishProduct;
use ShortEatsManager\Models\CookingRequest;
use StockManager\Classes\StockTransaction;

class SemiIssueController extends Controller
{
    use SemiFinishLog;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:semi_issue-index', ['only' => ['index']]);
        $this->middleware('permission:SemiFinishProducts-list', ['only' => ['list']]);
        $this->middleware('permission:semi_issue-create', ['only' => ['create','store']]);
        $this->middleware('permission:semi_issue-reUsableWestage', ['only' => ['create']]);
    }
    public function index()
    {
        $merges = EndProductBurdenHeader::whereIssueSemi(0)->get()->pluck('serial', 'id');

        $semi_finish = SemiFinishProduct::get()->pluck('name', 'semi_finish_product_id');
        $semi_finish_reuse = SemiFinishProduct::where('reusable_type', '<>', 1)->pluck('name', 'semi_finish_product_id');
        $transaction = SemiInquiry::where('created_at', 'like', Carbon::today()->format('Y-m-d') . '%')->OrderByDesc('id')->limit(25)->get();

        $custo = EndProductBurden::where('created_at', '>', Carbon::today()->subDays(3)->format('Y-m-d') . '%')->orderBy('created_at', 'desc')->whereHas('endProduct', function ($query) {
            $query->where('customize', '=', 1);
        })->get();
        $return_b = EndProductBurden::where('created_at', 'like', Carbon::today()->subDays(10)->format('Y-m-d') . '%')->orderBy('created_at', 'desc')->pluck('serial', 'id');

        $custo1 = EndProductBurden::where('created_at', '>', Carbon::today()->subDays(3)->format('Y-m-d') . '%')->orderBy('created_at', 'desc')->whereHas('endProduct', function ($query) {
            $query->where('customize', '=', 1);
        })->get()->pluck('serial', 'id');

        // $semi_name = SemiFinishProduct::where('semi_finish_product_id','=',$transaction->semi_id)->first();

        return view('semi-issue.index', compact('merges', 'semi_finish', 'semi_finish_reuse', 'transaction', 'custo', 'custo1', 'return_b'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type = $request->type;
        if ($type == '0') {
            $merge_serial = $request->merge_serial;
            $items = $request->semi;
            $qty = $request->qty;
            $json_data = [];

//            try {
//                DB::transaction(function () use ($items, $merge_serial, $qty,&$json_data) {
            foreach ($items as $key => $item) {
                $sm = BurdenSemi::whereMergeId($merge_serial)->whereSemiFinishProductId($items[$key])->get();


                $semi = SemiFinishProduct::where('semi_finish_product_id', '=', $items[$key])->first();
                $burden_ids = $semi->GradeABurdenList($semi->semi_finish_product_id)->pluck('id');
                $semi_id = $semi->semi_finish_product_id;

                $qty_total = $qty[$key];


                if ($semi->is_bake) {
                    $counter = 0;
                    foreach ($burden_ids as $key => $burden_i) {
                        if ($qty_total > 0) {
                            $semiBurden = BurdenGrade::where('id', '=', $burden_i)->first();
                            if (!empty($semiBurden)) {
                                if ($semiBurden->used_qty > 0) {
                                    $deductable = ($qty_total < $semiBurden->used_qty) ? $qty_total : $semiBurden->used_qty;
                                    $semiBurden->used_qty = $semiBurden->used_qty - $deductable;
                                    $semiBurden->save();
                                    $counter = $counter + 1;

                                    StockTransaction::semiFinishStockTransaction(2, $semi_id, '', $deductable, 0, 1);
                                    // StockTransaction::updateSemiFinishAvailableQty(2, $semi_id, $deductable);
                                    // $semi_product = SemiFinishProduct::find($semi_id);
                                    //$this->recordSemiFinishProductOutstanding($semi_product,1,-$deductable, 'Burden Grade', $burden_i, 'BurdenManager\\Models\\BurdenGrade',$semiBurden->burden_id);
                                    $semi_product = SemiFinishProduct::find($semi_id);
                                    $this->recordSemiFinishProductOutstanding($semi_product, 1, $deductable, 'End Product Burden Request', $semiBurden->id, 'BurdenManager\\Models\\BurdenGrade', $semiBurden->burden_id, 'EndBurdenRequest', 2, $semiBurden->id, $merge_serial);


                                    array_push($json_data, json_encode($semiBurden));
                                    $qty_total = $qty_total - $deductable;

                                    $filling_qty = $deductable;

                                    foreach ($sm as $s) {
                                        $qty_x = $s->qty;
                                        if ($qty_x != $s->send_qty) {
                                            $fillable = $qty_x - $s->send_qty;
                                            $fil_qty = ($filling_qty < $fillable) ? $filling_qty : $fillable;
                                            $s->send_qty = $s->send_qty + $fil_qty;
                                            $filling_qty = $filling_qty - $fil_qty;
                                            $s->save();

                                            $issue = new SemiBurdenIssueData();
                                            $issue->burden_id = $semiBurden->id;
                                            $issue->type = 0;
                                            $issue->grade_id = 1;
                                            $issue->used_qty = $fil_qty;
                                            $issue->burden_qty = 0;
                                            $issue->data_list_id = 0;
                                            $issue->reference_type = "App/Burden";
                                            $issue->reference = $semiBurden->burden_id;
                                            $issue->end_product_id = $s->end_product_id;
                                            $issue->merege_burden_semi_id = $s->id;
                                            $issue->description = "Burden Request - From Semi Issue";
                                            $issue->save();
                                        }
                                    }
                                }


                            }


                        }

                    }
                } else {
                    $counter = 0;
                    foreach ($burden_ids as $key => $burden) {
                        if ($qty_total > 0) {
                            $semiBurden = CookingRequest::where('serial', '=', $burden)->first();
                            if (!empty($semiBurden)) {
                                if ($semiBurden->used_qty > 0) {
                                    $deductable = ($qty_total < $semiBurden->used_qty) ? $qty_total : $semiBurden->used_qty;
                                    $semiBurden->used_qty = $semiBurden->used_qty - $deductable;
                                    $semiBurden->save();
                                    $counter = $counter + 1;

                                    StockTransaction::semiFinishStockTransaction(2, $semi_id, '', $deductable, 0, 1);
                                    //  StockTransaction::updateSemiFinishAvailableQty(2, $semi_id, $deductable);
                                    // $semi_product = SemiFinishProduct::find($semi_id);
                                    // $this->recordSemiFinishProductOutstanding($semi_product,1,-$deductable, 'Cooking Request', $burden, 'ShortEatsManager\\Models\\CookingRequest',$semiBurden->id);
                                    $semi_product = SemiFinishProduct::find($semi_id);
                                    $this->recordSemiFinishProductOutstanding($semi_product, 1, $deductable, 'End Product Burden Request', $semiBurden->id, 'ShortEatsManager\\Models\\CookingRequest', $semiBurden->burden_id, 'EndBurdenRequest', 2, $semiBurden->id, $merge_serial);

                                    array_push($json_data, json_encode($semiBurden));
                                    $qty_total = $qty_total - $deductable;


                                    foreach ($sm as $s) {
                                        $qty_x = $s->qty;
                                        if ($qty_x != $s->send_qty) {
                                            $fillable = $qty_x - $s->send_qty;
                                            $fil_qty = ($filling_qty < $fillable) ? $filling_qty : $fillable;
                                            $s->send_qty = $s->send_qty + $fil_qty;
                                            $filling_qty = $filling_qty - $fil_qty;
                                            $s->save();

                                            $issue = new SemiBurdenIssueData();
                                            $issue->burden_id = $semiBurden->id;
                                            $issue->type = 0;
                                            $issue->grade_id = 1;
                                            $issue->used_qty = $fil_qty;
                                            $issue->burden_qty = 0;
                                            $issue->data_list_id = 0;
                                            $issue->reference_type = "App/CookingRequest";
                                            $issue->reference = $semiBurden->id;
                                            $issue->end_product_id = $s->end_product_id;
                                            $issue->merege_burden_semi_id = $s->id;
                                            $issue->description = "Burden Request - From Semi Issue";
                                            $issue->save();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                array_push($json_data, json_encode($sm));
            }

            $merge = EndProductBurdenHeader::find($merge_serial);
            $merge->status = 2;
            $merge->issue_semi = 1;
            $merge->save();
            array_push($json_data, json_encode($merge));

            $burdens = json_decode($merge->burdens);

            foreach ($burdens as $burden) {
                $burden = EndProductBurden::find($burden);
                $burden->issue_semi = 1;
                $burden->save();
            }

//                });
//            } catch (\Exception $exception) {
//                return redirect()->back()->with([
//                    'error' => true,
//                    'error.title' => 'Sorry !',
//                    'error.message' => 'Contact Admin'
//                ]);
//            }

            $record = new SemiIssueDataList();
            $record->type = $type;
            $record->data = json_encode($json_data);
            $record->created_by = Auth::user()->id;
            $record->save();


            return redirect()->back()->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Semi Finish Send'
            ]);
        } elseif ($type == '2') {

            //Customized Cake 2021-07-30
            $type_names = ['End Product Burden Request', 'Production Purpose', 'Customize Cake', 'Destroy and Return', 'Admin Adjustment'];
            $type_nm = ['EndBurdenRequest', 'ProductionPurpose', 'CustomizeCake', 'DestroyAndReturn', 'AdminAdjustment'];

            $semi_id = $request->semi_finish_c;
            $qty = $request->qty;

            $description = $request->description;

            $json_data = [];

            $bd_id = 0;

            $semi = SemiFinishProduct::where('semi_finish_product_id', '=', $semi_id)->first();
            try {
                DB::transaction(function () use ($request, $semi, $semi_id, $qty, $json_data, $type, $type_names, &$type_nm) {


                    if ($semi->is_bake == 1) {
                        $counter = 0;
                        $burden_ids = $request->burden_id;
                        $burden_qty = $request->burden_qty;
                        foreach ($burden_ids as $key => $burden) {
                            $bd_id = $key;

                            $semiBurden = BurdenGrade::where('id', '=', $key)->first();
                            $semiBurden->used_qty = $semiBurden->used_qty - $burden_qty[$key];
                            $semiBurden->save();
                            $counter = $counter + 1;
                            StockTransaction::semiFinishStockTransaction(2, $semi_id, '', $burden_qty[$key], 0, 1);
                            //  StockTransaction::updateSemiFinishAvailableQty(2, $semi_id, $burden_qty[$key]);
                            $semi_product = SemiFinishProduct::find($semi_id);
                            //  $semi_product = SemiFinishProduct::find($semi_id);
                            //  $this->recordSemiFinishProductOutstanding($semi_product, 1, $deductable, 'End Product Burden Request', $burden, 'ShortEatsManager\\Models\\CookingRequest',$semiBurden->id,'EndBurdenRequest',2,$semiBurden->id,$merge_serial);


                            $this->recordSemiFinishProductOutstanding($semi_product, 1, $burden_qty[$key], $type_names[$type], $semiBurden->id, 'BurdenManager\\Models\\BurdenGrade', $semiBurden->burden_id, $type_nm[$type], 2, $semiBurden->id, '');
                            array_push($json_data, json_encode($semiBurden));


                            $issue = new SemiBurdenIssueData();
                            $issue->burden_id = $semiBurden->id;
                            $issue->type = $type;
                            $issue->end_product_burden_header_id = $request->end_bid;
                            $issue->end_product_id = $semi_id;
                            $issue->grade_id = $semiBurden->grade_id;
                            $issue->used_qty = $burden_qty[$key];
                            $issue->burden_qty = $semiBurden->burden_qty;
                            $issue->data_list_id = 0;
                            $issue->reference_type = "App/Burden";
                            $issue->reference = $semiBurden->burden_id;
                            $issue->description = $type_names[$type];
                            $issue->save();
                        }



                    } elseif ($semi->is_bake == 2) {
                        $counter = 0;
                        $burden_ids = $request->cooking_request_id;
                        $burden_qty = $request->cooking_request_qty;
                        foreach ($burden_ids as $key => $burden) {



                            $semiBurden = CookingRequest::where('id', '=', $key)->first();
                            $semiBurden->used_qty = $semiBurden->used_qty - $burden_qty[$key];
                            $semiBurden->save();
                            $counter = $counter + 1;
                            StockTransaction::semiFinishStockTransaction(2, $semi_id, '', $burden_qty[$key], 0, 1);
                            // StockTransaction::updateSemiFinishAvailableQty(2, $semi_id, $burden_qty[$key]);
                            $semi_product = SemiFinishProduct::find($semi_id);
                            // $this->recordSemiFinishProductOutstanding($semi_product,1,-$burden_qty[$key], 'Burden Grade', $key, 'ShortEatsManager\\Models\\CookingRequest',$semiBurden->id);

                            $this->recordSemiFinishProductOutstanding($semi_product, 1, $burden_qty[$key], $type_names[$type], $key, 'ShortEatsManager\\Models\\CookingRequest', $key, $type_nm[$type], 2, $key, '');

                            array_push($json_data, json_encode($semiBurden));

                            $issue = new SemiBurdenIssueData();
                            $issue->burden_id = $semiBurden->id;
                            $issue->type = $type;
                            $issue->grade_id = 1;
                            $issue->end_product_burden_header_id = $request->end_bid;
                            $issue->end_product_id = $semi_id;
                            $issue->used_qty = $burden_qty[$key];
                            $issue->burden_qty = $semiBurden->used_qty;
                            $issue->data_list_id = 0;
                            $issue->reference_type = "App/CookingRequest";
                            $issue->reference = $semiBurden->id;
                            $issue->description = $type_names[$type];
                            $issue->save();
                        }
                    }



                });
            } catch (\Exception $exception) {
                dd($exception);
                return redirect()->back()->with([
                    'error' => true,
                    'error.title' => 'Sorry !',
                    'error.message' => 'Contact Admin'
                ]);
            }



            $record = new SemiIssueDataList();
            $record->semi_id = $semi_id;
            $record->type = $type;
            $record->qty = $qty;
            $record->description = $description;
            $record->data = json_encode($json_data);
            $record->created_by = Sentinel::getUser()->id;
            $record->save();

            $sm_qty = $request->customized_qty;
            $sm_ids = $request->customized_burden_id;

        /*    foreach ($sm_ids as $key => $sm_id) {

                $cust = new SemiCustomizedCake();
                $cust->custo_burden_id = $sm_ids[$key];
                $cust->semi_id = $semi_id;
                $cust->qty = $sm_qty[$key];
                $cust->value = 0;
                $cust->is_bake = $semi->is_bake;
                $cust->save();


            }*/

            return redirect()->back()->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Semi Finish Stock Released !'
            ]);

        } else {

            $type_names = ['End Product Burden Request', 'Production Purpose', 'Customize Cake', 'Destroy and Return', 'Admin Adjustment'];
            $type_nm = ['EndBurdenRequest', 'ProductionPurpose', 'CustomizeCake', 'DestroyAndReturn', 'AdminAdjustment'];

            $semi_id = $request->semi_finish;
            $qty = $request->qty;

            $description = $request->description;

            $json_data = [];


            $semi = SemiFinishProduct::where('semi_finish_product_id', '=', $semi_id)->first();
            try {
                DB::transaction(function () use ($request, $semi, $semi_id, $qty, $json_data, $type, $type_names, &$type_nm) {


                    if ($semi->is_bake == 1) {
                        $counter = 0;
                        $burden_ids = $request->burden_id;
                        $burden_qty = $request->burden_qty;
                        foreach ($burden_ids as $key => $burden) {
                            $semiBurden = BurdenGrade::where('id', '=', $key)->first();
                            $semiBurden->used_qty = $semiBurden->used_qty - $burden_qty[$key];
                            $semiBurden->save();
                            $counter = $counter + 1;
                            StockTransaction::semiFinishStockTransaction(2, $semi_id, '', $burden_qty[$key], 0, 1);
                            //  StockTransaction::updateSemiFinishAvailableQty(2, $semi_id, $burden_qty[$key]);
                            $semi_product = SemiFinishProduct::find($semi_id);
                            //  $semi_product = SemiFinishProduct::find($semi_id);
                            //  $this->recordSemiFinishProductOutstanding($semi_product, 1, $deductable, 'End Product Burden Request', $burden, 'ShortEatsManager\\Models\\CookingRequest',$semiBurden->id,'EndBurdenRequest',2,$semiBurden->id,$merge_serial);


                            $this->recordSemiFinishProductOutstanding($semi_product, 1, $burden_qty[$key], $type_names[$type], $semiBurden->id, 'BurdenManager\\Models\\BurdenGrade', $semiBurden->burden_id, $type_nm[$type], 2, $semiBurden->id, '');
                            array_push($json_data, json_encode($semiBurden));


                            $issue = new SemiBurdenIssueData();
                            $issue->burden_id = $semiBurden->id;
                            $issue->type = $type;
                            $issue->grade_id = $semiBurden->grade_id;
                            $issue->used_qty = $burden_qty[$key];
                            $issue->burden_qty = $semiBurden->burden_qty;
                            $issue->data_list_id = 0;
                            $issue->reference_type = "App/Burden";
                            $issue->reference = $semiBurden->burden_id;
                            $issue->description = $type_names[$type];
                            $issue->save();
                        }

                    } elseif ($semi->is_bake == 2) {
                        $counter = 0;
                        $burden_ids = $request->cooking_request_id;
                        $burden_qty = $request->cooking_request_qty;
                        foreach ($burden_ids as $key => $burden) {
                            $semiBurden = CookingRequest::where('id', '=', $key)->first();
                            $semiBurden->used_qty = $semiBurden->used_qty - $burden_qty[$key];
                            $semiBurden->save();
                            $counter = $counter + 1;
                            StockTransaction::semiFinishStockTransaction(2, $semi_id, '', $burden_qty[$key], 0, 1);
                            // StockTransaction::updateSemiFinishAvailableQty(2, $semi_id, $burden_qty[$key]);
                            $semi_product = SemiFinishProduct::find($semi_id);
                            // $this->recordSemiFinishProductOutstanding($semi_product,1,-$burden_qty[$key], 'Burden Grade', $key, 'ShortEatsManager\\Models\\CookingRequest',$semiBurden->id);

                            $this->recordSemiFinishProductOutstanding($semi_product, 1, $burden_qty[$key], $type_names[$type], $key, 'ShortEatsManager\\Models\\CookingRequest', $key, $type_nm[$type], 2, $key, '');

                            array_push($json_data, json_encode($semiBurden));

                            $issue = new SemiBurdenIssueData();
                            $issue->burden_id = $semiBurden->id;
                            $issue->type = $type;
                            $issue->grade_id = 1;
                            $issue->used_qty = $burden_qty[$key];
                            $issue->burden_qty = $semiBurden->used_qty;
                            $issue->data_list_id = 0;
                            $issue->reference_type = "App/CookingRequest";
                            $issue->reference = $semiBurden->id;
                            $issue->description = $type_names[$type];
                            $issue->save();
                        }
                    }

                });
            } catch (\Exception $exception) {
                dd($exception);
                return redirect()->back()->with([
                    'error' => true,
                    'error.title' => 'Sorry !',
                    'error.message' => 'Contact Admin'
                ]);
            }

            $record = new SemiIssueDataList();
            $record->semi_id = $semi_id;
            $record->type = $type;
            $record->qty = $qty;
            $record->description = $description;
            $record->data = json_encode($json_data);
            $record->created_by = Auth::user()->id;
            $record->save();


            return redirect()->back()->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Semi Finish Stock Released !'
            ]);


        }

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function issueForBurden(Request $request, EndProductBurdenHeader $burden)
    {
        $items = BurdenSemi::selectRaw(' COALESCE(sum(qty), 0) as qty, semi_finish_product_id,semi_finish_product_name,end_product_id')
            ->whereMergeId($burden->id)
            ->groupBy('semi_finish_product_id')
            ->get();

        $data = [];

        foreach ($items as $raw) {
            $loading_type = $raw->endProduct->category->loading_type;
            $mat = SemiFinishProduct::whereSemiFinishProductId($raw->semi_finish_product_id)->first();
            $available_qty = $mat->available_qty;
            $rawmate = ['id' => $mat->semi_finish_product_id, 'semi_name' => $mat->name, 'qty' => $raw->qty, 'available_qty' => $available_qty, 'loading_type' => $loading_type];
            array_push($data, $rawmate);
        }
        return $data;
    }
}
