<?php

namespace App\Http\Controllers;

use App\SalesReturnHeader;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PurchasingOrderManager\Models\Content;
use PurchasingOrderManager\Models\PurchasingOrder;
use RawMaterialManager\Models\RawMaterial;
use SupplierManager\Models\Supplier;

class SupplierWiseMaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:supplier_wise_Material-index', ['only' => ['index','datatable']]);
        $this->middleware('permission:SupplierWiseMaterial-list', ['only' => ['list']]);

    }
    public function index()
    {
        //
        $suppliers = Supplier::all()->pluck('supplier_name','id');
        $items = RawMaterial::all()->pluck('name','raw_material_id');
        return view('supplier_wise_materials',compact('suppliers','items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Return JSON array for datatable.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function datatable(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'purchasing_order_id', 'raw_material_id'];
        $order_column = $columns[$order_by[0]['column']];

        $records = Content::tableData($order_column, $order_by_str, $start, $length);

        $recordsCount =0;
        $total_amount = 0;
        $total_qty = 0;

        $stat_data = Content::query();
        $stst = null;

        if ($request->filter == true) {
            $supplier = $request->supplier;
            $date_range = $request->date_range;
            $item = $request->item;

         //   dd('Sup :'.$supplier .'date :'.$date_range.'item :'.$item);

            $recordsCount = Content::DataTable($date_range, $supplier, $item)->count();

            $records = Content::DataTable($date_range, $supplier, $item)->tableData($order_column, $order_by_str, $start, $length)->get();

       /*     $stst = $stat_data->DataTable($date_range, $supplier, $item)
                ->select(DB::raw('SUM(received_price) as total_sum'))
                ->first();
            $total_amount = $stst->total_sum;*/


        }/* elseif (is_null($search) || empty($search)) {
            $records = $records->get();
            $recordsCount = SalesReturnHeader::all()->count();
            $stst = $stat_data
                ->select(DB::raw('SUM(total) as  total_sum'))
                ->first();
        } else {
            $stst = $stat_data->searchData($search)
                ->select(DB::raw('SUM(total) as  total_sum'))
                ->first();
            $records = $records->searchData($search)->get();
            $recordsCount = $records->count();
        }*/




        $data = [];
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;


        foreach ($records as $key => $record) {
            $statusBtn = null;
            $printBtn = null;

            $itemName = RawMaterial::find($record->raw_material_id);
            $phead = PurchasingOrder::find($record->purchasing_order_id);
            $sup = Supplier::find($phead->supplier_id);


            if($phead->status == 1) {

                $statusBtn = '<button class="btn btn-outline-danger btn-sm mr-1">Ordered</button>';
            }elseif($phead->status == 2) {

                $statusBtn = '<button class="btn btn-outline-warning btn-sm mr-1">Received</button>';
            }elseif($phead->status == 3){
                $statusBtn = '<button class="btn btn-outline-success btn-sm mr-1">Paid</button>';
            }



         //   $viewBtn = '<a href="'.route('sales_return.show',$record->id).'" class="btn btn-outline-info btn-sm mr-1"><i class="fa fa-eye"></i> </a>';



            $inv =  $phead->invoice_no.'['.$phead->sup_invoice_no.']';
            $total_amount = $total_amount + (($record->received_price) * ($record->recieved_qty));
            $total_qty = $total_qty + $record->recieved_qty;
            $data[$i] = array(
                $record->created_at->format('Y-m-d'),
                $inv,
                $itemName->name,
                $sup->supplier_name,
                number_format($record->ordered_price, '2'),
                number_format($record->received_price, '2') ,
                number_format($record->requested_qty, '2') ,
                number_format($record->recieved_qty, '2') ,
                number_format((($record->received_price) * ($record->recieved_qty)), '2'),
                $statusBtn
            );
            $i++;
        }

        $data[$i] = array(
            null,
            null,
            'Total',
            null,
            null,
            null ,
            null ,
            number_format($total_qty, '3') ,
            number_format($total_amount, '2'),
            null
        );
        if ($recordsCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($recordsCount),
            "recordsFiltered" => intval($recordsCount),
            "total_sum" => number_format($total_amount, 2),
            "data" => $data
        ];

        return json_encode($json_data);



        /* $start_date = date("Y-m-d",strtotime("-1 days"));
         $end_date = date("Y-m-d");
         $supplier_id = 'all';
         $order_by = $_REQUEST['order']; // This array contains order information of clicked column
         $search = $_REQUEST['search']['value']; // This array contains search value information datatable
         $start = $_REQUEST['start']; // start limit of data
         $length = $_REQUEST['length']; // end limit of data
         $order_by_str = $order_by[0]['dir'];
         if ($order_by[0]['column']==0){
             $order_column = "raw_material.name";
         }
         if ($order_by[0]['column']==1){
             $order_column = "raw_material.available_qty";
         }
         if ($order_by[0]['column']==2){
             $order_column = "raw_material.buffer_stock";
         }
         if ($order_by[0]['column']==3){
             $order_column = "supplier.supplier_name";
         }
         if ($order_by[0]['column']==4){
             $order_column = "purchasing_order_data.ordered_price";
         }
         if ($order_by[0]['column']==5){
             $order_column = "purchasing_order_data.recieved_qty";
         }
         $where_query = "date(purchasing_order_header.received_date) between '$start_date' and '$end_date' ";
         if (!empty($supplier_id) && $supplier_id!='all'){
             $where_query .= "and supplier.id=$supplier_id ";
         }
         if ($search!='' || $search!=NULL) {
             $where_query .= "and (raw_material.name like '%$search%' or raw_material.available_qty like '%$search%' or raw_material.buffer_stock like '$search' or supplier.supplier_name like '$search' or purchasing_order_data.ordered_price like '$search' or purchasing_order_data.recieved_qty like '%$search%')";
         }
         $records = DB::table("purchasing_order_header")
             ->join('purchasing_order_data','purchasing_order_data.purchasing_order_id','=','purchasing_order_header.id')
             ->join('supplier','supplier.id','=','purchasing_order_header.supplier_id')
             ->join('raw_material','raw_material.raw_material_id','=','purchasing_order_data.raw_material_id')
             ->select('raw_material.name AS raw_material_name','raw_material.available_qty','raw_material.buffer_stock','supplier.supplier_name','purchasing_order_data.ordered_price','purchasing_order_data.recieved_qty')
             ->whereRaw($where_query)
             ->orderBy($order_column, $order_by_str)
             ->offset($start)
             ->limit($length)
             ->get();
         $records_count = DB::table("purchasing_order_header")
             ->join('purchasing_order_data','purchasing_order_data.purchasing_order_id','=','purchasing_order_header.id')
             ->join('supplier','supplier.id','=','purchasing_order_header.supplier_id')
             ->join('raw_material','raw_material.raw_material_id','=','purchasing_order_data.raw_material_id')
             ->select(DB::raw('count(*) as all_count'))
             ->whereRaw($where_query)
             ->first();

         $data[][] = array();
         $i = 0;
         foreach ($records as $record)
         {
             $data[$i] = array(
                 $record->raw_material_name,
                 $record->available_qty,
                 $record->buffer_stock,
                 $record->supplier_name,
                 $record->ordered_price,
                 $record->recieved_qty
             );
             $i++;
         }

         $totalRecords = $records_count->all_count;
         if ($totalRecords==0){
             $data = array();
         }
         $json_data = array(
             "draw"            => intval($_REQUEST['draw']),
             "recordsTotal"    => intval($totalRecords),
             "recordsFiltered" => intval($totalRecords),
             "data"            => $data   // total data array
         );
         $json_data = json_encode($json_data);
         return $json_data;

        */
    }

    /**
     * Return array of new row to datatable.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public static function get_new_rows(Request $request)
    {
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $supplier_id = $request->supplier_id;
        $search = NULL;
        $order_by = $_REQUEST['order']; // This array contains order information of clicked column
        $start = $_REQUEST['start']; // start limit of data
        $length = $_REQUEST['length']; // end limit of data
        $order_by_str = $order_by[0]['dir'];
        if ($order_by[0]['column']==0){
            $order_column = "raw_material.name";
        }
        if ($order_by[0]['column']==1){
            $order_column = "raw_material.available_qty";
        }
        if ($order_by[0]['column']==2){
            $order_column = "raw_material.buffer_stock";
        }
        if ($order_by[0]['column']==3){
            $order_column = "supplier.supplier_name";
        }
        if ($order_by[0]['column']==4){
            $order_column = "purchasing_order_data.ordered_price";
        }
        if ($order_by[0]['column']==5){
            $order_column = "purchasing_order_data.recieved_qty";
        }
        if (!empty($_REQUEST['search']['value'])) {
            $search = $_REQUEST['search']['value']; // This array contains search value information datatable
        }
        $where_query = "date(purchasing_order_header.received_date) between '$start_date' and '$end_date' ";
        if (!empty($supplier_id) && $supplier_id!='all'){
            $where_query .= "and supplier.id=$supplier_id ";
        }
        if ($search!='' || $search!=NULL) {
            $where_query .= "and (raw_material.name like '%$search%' or raw_material.available_qty like '%$search%' or raw_material.buffer_stock like '$search' or supplier.supplier_name like '$search' or purchasing_order_data.ordered_price like '$search' or purchasing_order_data.recieved_qty like '%$search%')";
        }
        $records = DB::table("purchasing_order_header")
            ->join('purchasing_order_data','purchasing_order_data.purchasing_order_id','=','purchasing_order_header.id')
            ->join('supplier','supplier.id','=','purchasing_order_header.supplier_id')
            ->join('raw_material','raw_material.raw_material_id','=','purchasing_order_data.raw_material_id')
            ->select('raw_material.name AS raw_material_name','raw_material.available_qty','raw_material.buffer_stock','supplier.supplier_name','purchasing_order_data.ordered_price','purchasing_order_data.recieved_qty')
            ->whereRaw($where_query)
            ->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length)
            ->get();
        $records_count = DB::table("purchasing_order_header")
            ->join('purchasing_order_data','purchasing_order_data.purchasing_order_id','=','purchasing_order_header.id')
            ->join('supplier','supplier.id','=','purchasing_order_header.supplier_id')
            ->join('raw_material','raw_material.raw_material_id','=','purchasing_order_data.raw_material_id')
            ->select(DB::raw('count(*) as all_count'))
            ->whereRaw($where_query)
            ->first();

        $data[][] = array();
        $i = 0;
        foreach ($records as $record)
        {
            $data[$i] = array(
                $record->raw_material_name,
                $record->available_qty,
                $record->buffer_stock,
                $record->supplier_name,
                $record->ordered_price,
                $record->recieved_qty
            );
            $i++;
        }

        $totalRecords = $records_count->all_count;
        if ($totalRecords==0){
            $data = array();
        }
        $json_data = array(
            "draw"            => intval($_REQUEST['draw']),
            "recordsTotal"    => intval($totalRecords),
            "recordsFiltered" => intval($totalRecords),
            "data"            => $data   // total data array
        );
        $json_data = json_encode($json_data);
        return $json_data;
    }

    public function download(Request $request)
    {
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $supplier_id = $request->supplier_id;
        $where_query = "date(purchasing_order_header.received_date) between '$start_date' and '$end_date' ";
        if (!empty($supplier_id) && $supplier_id!='all'){
            $where_query .= "and supplier.id=$supplier_id ";
        }
        $records = DB::table("purchasing_order_header")
            ->join('purchasing_order_data','purchasing_order_data.purchasing_order_id','=','purchasing_order_header.id')
            ->join('supplier','supplier.id','=','purchasing_order_header.supplier_id')
            ->join('raw_material','raw_material.raw_material_id','=','purchasing_order_data.raw_material_id')
            ->select('raw_material.name AS raw_material_name','raw_material.available_qty','raw_material.buffer_stock','supplier.supplier_name','purchasing_order_data.ordered_price','purchasing_order_data.recieved_qty')
            ->whereRaw($where_query)
            ->get();

        $pdf = PDF::loadView('supplier_wise_material_download', compact('records'))->setPaper('a4', 'portrait');
        return $pdf->download('SUPPLIER_WISE_RAW_MATERIALS.pdf');

    }
}
