<?php

namespace App\Http\Controllers;

use App\AgentInquiry;
use App\AgentPayment;
use App\LoadingHeader;
use App\PaymentDetail;
use App\Traits\AgentLog;
use App\User;
use Carbon\Carbon;
use CashLockerManager\Classes\CashTransaction;
use CashLockerManager\Models\CashBookTransaction;
use EndProductBurdenManager\Models\BurdenRaw;
use EndProductBurdenManager\Models\EndProductBurden;
use EndProductBurdenManager\Models\EndProductBurdenHeader;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use PassBookManager\Models\Cheque;
use SalesRepManager\Models\SalesRep;
use StockManager\Classes\StockTransaction;
use function GuzzleHttp\Promise\all;

class AgentPaymentController extends Controller
{
    use AgentLog;

    /**
     * Display a listing of the resource.
     *
     *
     * @return Response
     */
    function __construct()
    {
        $this->middleware('permission:agent_payment-index', ['only' => ['index','tableData']]);
        $this->middleware('permission:AgentPayment-list', ['only' => ['list']]);
        $this->middleware('permission:DayPaySlip-list', ['only' => ['list']]);
        $this->middleware('permission:agent_payment-index2', ['only' => ['index2','tableData2']]);

    }
    public function index()
    {
        return view('agents.payments');
    }

    public function index2()
    {
        $agents = SalesRep::all()->pluck('name_with_initials', 'id');
        return view('agents.payments2', compact('agents'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        // $agents = SalesRep::all()->pluck('email', 'id');
        $agents = SalesRep::all();
        return view('agents.make_payment', compact('agents'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        $notes_data = $request->except(['agent_id', 'cheque_amount_value', 'cash_amount', 'total_amount', '_token']);
        $payment = new AgentPayment();
        $payment->agent_id = (Auth::user()->hasRole(['Sales Agent','Owner','Super Admin'])) ? $request->agent_id : Auth::user()->id;
        $payment->serial = 'PAY-' . Carbon::now()->format('ymdHis');
        $payment->cheque_amount_value = $request->cheque_amount_value;
        $payment->cash_amount = $request->cash_amount;
        $payment->total_amount = $request->total_amount;
        $payment->notes_data = json_encode($notes_data);

        $agent = User::find($payment->agent_id);
        $description = "Payment received " . $agent->name_with_initials . ' - ' . Carbon::now();
        CashTransaction::updateCashLocker(1, $request->total_amount);
        CashTransaction::updateCashTransaction(1, $request->total_amount, $payment->id, 1, $description, -1, $payment->agent_id,'Agent Payments');

        $chkpay = AgentPayment::where('serial', '=', $payment->serial)->first();

        if (empty($chkpay)) {

            $payment->save();

            //      $agent = Users::find($payment->agent_id);
            //      $description = "Payment received " . $agent->name_with_initials . ' - ' . Carbon::now();
            //      CashTransaction::updateCashLocker(1, $request->total_amount);
            //     CashTransaction::updateCashTransaction(1, $request->total_amount, $payment->id, 1, $description, -1, -1, 'AgentPayments','Agent');
        }
        return redirect(route('agent-payment.index'));
    }


    public function accept(Request $request)
    {
        $chkpay = AgentPayment::where('id', '=', $request->id)->where('status', '=', 'pending')->first();
        if (!empty($chkpay)) {

            $chkpay->status = 'processing';
            $chkpay->save();

            $agent = Users::find($chkpay->agent_id);
            $description = "Payment received " . $agent->name_with_initials . ' - ' . Carbon::now();
            CashTransaction::updateCashLocker(1, $chkpay->total_amount);
            CashTransaction::updateCashTransaction(1, $chkpay->total_amount, $request->id, 1, $description, -1, $chkpay->agent_id, 'AgentPayments', 'Agent');
        }
        return redirect(route('agent-payment.index'));
    }

    public function slipPrint(Request $request)
    {
        $chkpay = AgentPayment::where('id', '=', $request->id)->first();
        $agent = SalesRep::find($chkpay->agent_id);
        $creBal = AgentInquiry::where('agent_id', '=', $chkpay->agent_id)->orderByDesc('id')->first();
        $agent_init_value = AgentInquiry::whereAgentId($chkpay->agent_id)->whereIsInit(1)->first();

        //   $merge = EndProductBurdenHeader::find($request->merge);
        //   $eb = json_decode($merge->burdens);
        //   $items = BurdenRaw::select('raw_material_id', DB::raw('SUM(qty) as qty'))->whereIn('end_product_burden_id', $eb)->groupBy('raw_material_id')->get();

        return view('agents.pay_print', compact('chkpay', 'agent', 'creBal', 'agent_init_value'));
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function insertCheque(Request $request)
    {
        $cheque = new Cheque();
        $cheque->type = 0;
        $cheque->bank_account_id = 1;
        $cheque->cheque_date = $request->date;
        $cheque->cheque_no = $request->cheque_no;
        $cheque->amount = $request->amount;
        $cheque->issue_date = Carbon::today();
        $cheque->supplier = Auth::user()->id;
        $cheque->description = '';
        $cheque->bank = $request->bank;
        $cheque->branch = $request->branch;
        $cheque->status = 0;
        $cheque->save();

        return $this->tempoCheques();
    }

    public function tempoCheques()
    {
        $che = Cheque::whereSupplier(Auth::user()->id)->whereStatus(0)->get();
        return $che;
    }

    public function tableData(Request $request)
    {

        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $recordsCount = 0;

        $date = $request->date_range;
        $columns = ['id', 'agent_id', 'cheque_amount_value', 'cash_amount', 'total_amount'];
        $order_column = $columns[$order_by[0]['column']];


        $records = AgentPayment::tableData($order_column, $order_by_str, $start, $length);


        if (Sentinel::check()->roles[0]->slug == 'sales-ref') {
            $records = $records->whereAgentId(Sentinel::check()->getUserId());
        }

        $records = $records->where('status', '<>', 'approved')->whereNull('deleted_at')->get();
        // dd($records);
        $recordsCount = AgentPayment::where('status', '<>', 'approved')->whereNull('deleted_at')->count();

        $data = [];
        $i = 0;
        $edit_btn = null;
        // $delete_btn = null;


        foreach ($records as $key => $record) {
            if ($record->deleted_at == Null) {
                $btn_approve = null;
                $delete_btn = null;
                $actBTN = null;
                $pntBTN = null;

                $creBal = AgentInquiry::where('agent_id', '=', $record->agent_id)->orderByDesc('id')->first();
                $agent_init_value = AgentInquiry::whereAgentId($record->agent_id)->whereIsInit(1)->first();
                if (empty($agent_init_value)) {
                    $opVal = 0;
                } else {
                    $opVal = $agent_init_value->amount;
                }

                if ($record->status == 'pending') {
                    $delete_btn = (Sentinel::check()->roles[0]->slug == 'owner') ? "<a href='/agent-payment/$record->id/remove' class='btn btn-danger'><i class='fa fa-trash' aria-hidden='true'></i></a>" : null;
                    if ((Sentinel::check()->roles[0]->slug == 'owner' || Sentinel::check()->roles[0]->slug == 'accountant') && !empty($record->agent)) {
                        $actBTN = "<a href='" . route('payment.accept', [$record->id]) . "' class='btn btn-sm btn-info' id='actBTN'>Accept</a>";
                    }
                    $pntBTN = "<a href='" . route('payment.print', [$record->id]) . "' class='btn btn-warning' id='printBTN' target='_blank'><i class='fa fa-print'></i> </a>";

                }

                if ($record->status == 'processing') {
                    $btn_approve = ((Sentinel::check()->roles[0]->slug == 'accountant') || Sentinel::check()->roles[0]->slug == 'owner') ? "<a href='/agent-payment/$record->id/approve' class='btn btn-sm btn-success'>Pay</a>" : null;
                    if (Sentinel::check()->roles[0]->slug == 'owner' || Sentinel::check()->roles[0]->slug == 'accountant') {
                        $pntBTN = "<a href='" . route('payment.print', [$record->id]) . "' class='btn btn-warning' id='printBTN' target='_blank'><i class='fa fa-print'></i> </a>";
                    }
                }

                $status_class = $record->status == 'processing' ? 'btn-outline-info' : 'btn-outline-success';
                $status_word = ucfirst($record->status);
                $status = "<button class='btn btn-sm $status_class'>$status_word</button>";
                $data[$i] = array(
                    Carbon::parse($record->created_at)->format('Y-m-d H:i:s'),
                    $record->serial,
                    !empty($record->agent) ? $record->agent->name_with_initials . ' - ' . $record->agent->territory : "N/A",
                    //  number_format($record->agent->balance, 2),
                    !empty($creBal) ? number_format(($creBal->end_balance + $opVal), 2) : 0,
                    number_format($record->cheque_amount_value, 2),
                    number_format($record->cash_amount, 2),
                    number_format($record->total_amount, 2),
                    $status,
                    $btn_approve . " " . $delete_btn . " " . $pntBTN . " " . $actBTN
                );
                $i++;
            }
        }


        if ($recordsCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($recordsCount),
            "recordsFiltered" => intval($recordsCount),
            "data" => $data
        ];

        return json_encode($json_data);
    }

    public function tableData2(Request $request)
    {
        $order_by = $request->order;
        //   $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        //   $order_by_str = $order_by[0]['dir'];

        $recordsCount = 0;


        //   $columns = ['id', 'agent_id', 'cheque_amount_value', 'cash_amount', 'total_amount'];
        //  $order_column = $columns[$order_by[0]['column']];


        if (Sentinel::check()->roles[0]->slug == 'sales-ref') {
            $records = AgentPayment::whereAgentId(Sentinel::check()->getUserId())->where('status', '!=', 'approved')->whereNull('deleted_at')->get();
            $recordsCount = AgentPayment::whereAgentId(Sentinel::check()->getUserId())->where('status', '!=', 'approved')->whereNull('deleted_at')->count();

        } else {
            $records = AgentPayment::where('status', '!=', 'approved')->whereNull('deleted_at')->get();
            $recordsCount = AgentPayment::where('status', '!=', 'approved')->whereNull('deleted_at')->count();

        }

        if ($request->filter == true) {

            $date_range = $request->date_range;
            $status = $request->status;
            $agent = $request->agent;
            $type = $request->type;


            $records = AgentPayment::FilterData($date_range, $status, $agent)->whereNull('deleted_at')->get();
            $recordsCount = AgentPayment::FilterData($date_range, $status, $agent)->whereNull('deleted_at')->count();

        } else {


            /*  if (Sentinel::check()->roles[0]->slug == 'sales-ref') {
                  $records = AgentPayment::whereAgentId(Sentinel::check()->getUserId())->where('status','!=','approved')->whereNull('deleted_at')->get();
                  $recordsCount = AgentPayment::whereAgentId(Sentinel::check()->getUserId())->where('status','!=','approved')->whereNull('deleted_at')->count();

              }else{
                  $records = AgentPayment::where('status','!=','approved')->whereNull('deleted_at')->get();
                  $recordsCount = AgentPayment::where('status','!=','approved')->whereNull('deleted_at')->count();

              }*/
            //     $records = $records->where('created_at', 'like', Carbon::today()->format('Y-m-d') . '%')->get();
            //     $recordsCount = AgentPayment::where('created_at', 'like', Carbon::today()->format('Y-m-d') . '%')->count();

            //   $records = $records->where('status','!=','approved')->get();


        }

        $data = [];
        $i = 0;
        $edit_btn = null;
        // $delete_btn = null;
        $paid_chq = 0;
        $paid_cash = 0;
        $net_rem = 0;


        foreach ($records as $key => $record) {
            if ($record->deleted_at == Null) {
                $btn_approve = null;
                $delete_btn = null;
                $pntBTN = null;
                $btn_preview = null;

                if (Sentinel::check()->roles[0]->slug == 'owner') {
                    $pntBTN = "<a href='" . route('payment.print', [$record->id]) . "' class='btn btn-warning' id='printBTN' target='_blank'><i class='fa fa-print'></i> </a>";
                }

                if ($record->status == 'approved') {

                    $btn_preview = "<a href='" . route('pay-list.preview', [$record->id]) . "'  class='btn btn-outline-info btn-sm mr-1' target='_blank' > <i class='fa fa-eye'></i> </a>";
                }

                $paid_chq = $paid_chq + $record->cheque_amount_value;
                $paid_cash = $paid_cash + $record->cash_amount;
                $net_rem = $net_rem + $record->total_amount;

                $status_class = $record->status == 'processing' ? 'btn-outline-info' : 'btn-outline-success';
                $status_word = ucfirst($record->status);
                $status = "<button class='btn btn-sm $status_class'>$status_word</button>";
                $data[$i] = array(
                    Carbon::parse($record->created_at)->format('Y-m-d H:i:s'),
                    $record->serial,
                    $record->agent->name_with_initials . ' - ' . $record->agent->territory,
                    number_format($record->agent->balance, 2),
                    number_format($record->cheque_amount_value, 2),
                    number_format($record->cash_amount, 2),
                    number_format($record->total_amount, 2),
                    $status . $pntBTN . $btn_preview
                );
                $i++;
            }
        }

        if ($recordsCount == 0) {
            $data = [];
        }

        $json_data = [
            //    "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($recordsCount),
            "recordsFiltered" => intval($recordsCount),
            "data" => $data,
            "paid_chq" => number_format($paid_chq, 2),
            "paid_cash" => number_format($paid_cash, 2),
            "net_rem" => number_format($net_rem, 2)
        ];

        return json_encode($json_data);
    }


    public function payPreview(Request $request, $id)
    {
        $payment = AgentPayment::find($id);
        $agent = Users::find($payment->agent_id);
        return view('agents.pay-preview', compact('payment', 'agent'));

    }

    public function approvePayment(Request $request, $id)
    {
        $payment = AgentPayment::find($id);
        $invoices = LoadingHeader::whereHas('salesOrder', function ($q) use ($payment) {
            return $q->whereCreatedBy($payment->agent_id);
        })->where('status', '>', 1)->where('net_amount', '>=', 0)->whereIsPaid(0)->get();
        $payment_type = 'PAY';
        return view('agents.payment_list', compact('payment', 'invoices', 'payment_type'));
    }

    public function processPayment(Request $request, $id)
    {

        $paid_amount = 0;
        $payment = null;
        if ($request->payment_type == 'PAY') {
            $payment = AgentPayment::find($id);
            $paid_amount = $payment->total_amount;
            if ($payment->status == 'approved') {
                return 'Already Approved Payment';
            }
        }


        $invoices = $request->invoices;
        $invoice_list = [];


        $invoice_order = explode(',', $request->invoice_ids);


        foreach ($invoice_order as $order) {
            foreach ($invoices as $key => $invoice) {
                if ($order == $key) {
                    $loading = LoadingHeader::find($key);
                    $paid_amount = $paid_amount - $invoice;

                    $payd = new PaymentDetail();
                    $payd->pay_id = $payment->id;
                    $payd->agent_id = $payment->agent_id;
                    $payd->load_header_id = $key;
                    $payd->desc1 = 'BILL';
                    $payd->due_balance = $invoice;

                    if ($paid_amount >= 0) {
                        $loading->is_paid = 1;
                        $loading->paid_amount = $invoice;

                        $payd->paid_amount = $invoice;

                        array_push($invoice_list, ['loading_header_id' => $key, 'invoice_value' => $invoice, 'is_full_payment' => 1, 'settled_value' => $invoice]);
                    } else {
                        $loading->paid_amount = $loading->paid_amount + $invoice + $paid_amount;

                        $payd->paid_amount = $invoice + $paid_amount;;

                        //   dd('LoadPaid :'.$loading->paid_amount. 'Inv :'. $invoice . 'Paid :'. $paid_amount);

                        array_push($invoice_list, ['loading_header_id' => $key, 'invoice_value' => $invoice, 'is_full_payment' => 0, 'settled_value' => $invoice + $paid_amount]);
                    }
                    $payd->save();
                    $loading->save();
                }
            }

        }


        if ($request->payment_type == 'PAY') {
            if ($payment->status != 'approved') {
                $payment->status = 'approved';
                $payment->invoice_data = json_encode($invoice_list);
                $payment->save();

                $ref = SalesRep::find($payment->agent_id);
                $this->recordOutstanding($ref, -($payment->total_amount), "Agent's Payment", $payment->id, 'App\\AgentPayment', $payment->serial);
                StockTransaction::updateAgentBalance(2, $payment->agent_id, $payment->total_amount);
            } else {
                return redirect()->back(error)->with([
                    'error' => true,
                    'error.title' => 'Sorry !',
                    'error.message' => "Already Settled Please Contact System Administrator",
                ]);
            }
        }


        return redirect(route('agent-payment.index'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => "Payment Settled",
        ]);
    }

    public function removePayment($id)
    {
        $payment = AgentPayment::find($id);

        if ($payment->status != 'approved') {
            $payment->deleted_at = Carbon::now();
            $payment->delete();

            $transaction = CashBookTransaction::whereType(1)->whereRefNo($payment->id)->first();
            if (!empty($transaction) && $transaction->count() > 0) {
                // $transaction->delete();

                $agent = User::find($payment->agent_id);
                $description = "Delete " . $agent->name_with_initials . ' - ' . $payment->serial;
                CashTransaction::updateCashLocker(2, $transaction->amount);
                CashTransaction::updateCashTransaction(2, $transaction->amount, $payment->id, 0, $description, -3, $payment->agent_id, 'AgentPaymentsDeleteAdmin', 'Agent');

            }

            return redirect(route('agent-payment.index'))->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => "Payment Removed",
            ]);
        } else {
            return redirect(route('agent-payment.index'))->with([
                'error' => true,
                'error.title' => 'Payment is approved !',
                'error.message' => "Payment couldn't Removed",
            ]);
        }
    }
}
