<?php

namespace App\Http\Controllers;

use App\CrateInquiryAgent;
use App\Crates;
use App\LoadingDataVariant;
use App\LoadingHeader;
use App\User;
use App\CratesTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RawMaterialManager\Models\RawMaterial;
use SalesRepManager\Models\SalesRep;
use StockManager\Classes\StockTransaction;
use App\Traits\AgentLog;
use App\Traits\CratesAgentLog;
use App\Traits\CratesStoreLog;
use App\Traits\EndProductLog;
use App\Traits\RawMaterialLog;
use StockManager\Models\StockRawMaterial;

class PendingCratesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use AgentLog, EndProductLog, RawMaterialLog, CratesAgentLog, CratesStoreLog;
    function __construct()
    {
        $this->middleware('permission:pending_crates-index', ['only' => ['index']]);
        $this->middleware('permission:PendingCrates-list', ['only' => ['list']]);
        $this->middleware('permission:PendingCratesm-list', ['only' => ['mlist']]);
        $this->middleware('permission:pending_crates-store', ['only' => ['store']]);
        $this->middleware('permission:pending_crates-notApprove', ['only' => ['cratesShowRequest']]);


    }
    public function index()
    {
        $loadings = CratesTransaction::whereType('return')->where('crates_approve', '=', Null)->get();
        return view('crates.index', compact('loadings'));
    }

    public function cratesShowRequest(CratesTransaction $showId)
    {
        $transactionID = $showId->id;
        $CrateOrderId = $showId->ref_id;
        $AgentID = $showId->agent_id;
        $crates = json_decode($showId->crates);
        return view('crates.createView', compact('crates', 'transactionID', 'AgentID','CrateOrderId'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $user = Auth::user();
        // $creatReturn = CratesTransaction::whereId($request->crate_transaction_id)->first();
        // $creatReturn->crates_approve = $creatReturn->crates;
        // $creatReturn->approved_by = $user->id;
        // $creatReturn->save();
       // dd($request);

        $crates_data = [];
        if (!empty($request->return_crate) && sizeof($request->return_crate) > 0) {
            $creatReturn = CratesTransaction::whereId($request->crateTransactionId)->first();
            foreach ($request->return_crate as $key => $return_crate) {


                $isin = CrateInquiryAgent::where('reference','=',$request->crateTransactionOrderId)->where('desc_id','=',2)->where('crate_id','=',$return_crate)->first();
                if(empty($isin)) {
                    $crates_d = ['crate_id' => $return_crate, 'qty' => $request->return_crate_qty[$key]];
                    array_push($crates_data, $crates_d);

                    $cra_r = RawMaterial::find($return_crate);
                    $agent = SalesRep::find($request->crateTransactionAgentId);

                    $this->recordCratesAgentLogOutstanding($cra_r, $agent, (($request->return_crate_qty[$key]) * (-1)), 'Aprroved Return Creates', $request->crateTransactionOrderId, 'App\\PendingCratesController', 2, 2);
                    $this->recordCratesStoreLogOutstanding($cra_r, ($request->return_crate_qty[$key]), 'Aprroved Return Creates', $request->crateTransactionOrderId, 'App\\PendingCratesController', 4, 1, $agent->id);
                    $this->recordRawMaterialOutstanding($cra_r, ($request->return_crate_qty[$key]), 'Aprroved Return Creates', $request->crateTransactionId, 'App\\PendingCratesController', 15, 1, $agent->id);

                    StockTransaction::updateRawMaterialAvailableQty(1, $return_crate, $request->return_crate_qty[$key]);
                    StockTransaction::rawMaterialStockTransaction(1, 0, $return_crate, $request->return_crate_qty[$key], $request->crateTransactionId, 1);
                    StockTransaction::cratesTransaction(2, $return_crate, $request->return_crate_qty[$key], $request->crateTransactionId, $agent->id);
                }

            }
            $creatReturn->crates_approve = json_encode($crates_data);
            $creatReturn->save();
        }
        return redirect(route('crates-return.allcrates'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => 'Crates approved',
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $crates_data = [];
        if (!empty($request->update_return_crate) && sizeof($request->update_return_crate) > 0) {
            $creatReturn = CratesTransaction::whereId($id)->first();
            foreach ($request->update_return_crate as $key => $update_return_crate) {



                $crates_d = ['crate_id' => $update_return_crate, 'qty' => $request->update_return_crate_qty[$key]];
                array_push($crates_data, $crates_d);
            }
            $creatReturn->crates = json_encode($crates_data);
            $creatReturn->save();
        }
        return redirect(route('crates-request.cratesRequestList'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => 'Crates updated complete',
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function editCrates(Request $request)
    {
        $header = LoadingHeader::find($request->id);
        return view('asminReturnCrates-edit', compact('header'));
    }

    public function updateCrates(Request $request)
    {

        $loadingHeader = LoadingHeader::whereId($request->loading_header_id)->first();
        $crates_data = [];
        if (!empty($request->update_return_crate) && sizeof($request->update_return_crate) > 0) {
            foreach ($request->update_return_crate as $key => $update_return_crate) {

                $cra_r = RawMaterial::find($update_return_crate);
                $agent = SalesRep::find($loadingHeader->salesOrder->salesRep->id);

                $preqty = Crates::where('type','=',1)
                                ->where('raw_material_id','=',$update_return_crate)
                                ->where('ref_no','=',$loadingHeader->id)
                                ->orderByDesc('id')
                                ->first();


               /* $craw = Crates::where('raw_material_id','=',$update_return_crate)
                    ->where('ref_no','=',$loadingHeader->id)
                    ->where('type','=',1)
                    ->get();
                  dd($craw);*/

                $updif = (($request->update_return_crate_qty[$key])-($preqty->qty));

                $this->recordCratesAgentLogOutstanding($cra_r, $agent, $updif, 'Update Invoiced Crates',$loadingHeader->id , 'App\\PendingCratesController\\update',3,1);
                $this->recordCratesStoreLogOutstanding($cra_r, (($updif)*(-1)), 'Update Invoiced Crates', $loadingHeader->id, 'App\\PendingCratesController\\update',5,2, $agent->id);
                $this->recordRawMaterialOutstanding($cra_r, (($updif)*(-1)), 'Update Invoiced Crates', $loadingHeader->id, 'App\\PendingCratesController\\update',16,2,$agent->id);

                StockTransaction::updateRawMaterialAvailableQty(2, $update_return_crate, $updif);
                StockTransaction::rawMaterialStockTransaction(2, 0, $update_return_crate, $request->update_return_crate_qty[$key], $loadingHeader->id, 1);
                StockTransaction::cratesTransaction(1, $update_return_crate, $request->update_return_crate_qty[$key], $loadingHeader->id, $loadingHeader->salesOrder->salesRep->id);



                $crates_d = ['crate_id' => $update_return_crate, 'qty' => $request->update_return_crate_qty[$key]];
                array_push($crates_data, $crates_d);
            }
            $loadingHeader->crate_sent = json_encode($crates_data);
            $loadingHeader->save();
        }

        $transaction = CratesTransaction::where('ref_id', '=', $loadingHeader->id)->where('type', '=' , 'send')->first();
        $transaction->crates = json_encode($crates_data);
        $transaction->save();

        return redirect(route('order.list'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => 'Crates updated complete',
        ]);

    }
}
