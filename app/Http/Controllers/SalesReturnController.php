<?php

namespace App\Http\Controllers;

use App\AgentBuffer;
use App\AgentInquiry;
use App\LoadingData;
use App\LoadingDataVariant;
use App\PaymentDetail;
use App\ReusableStock;
use App\SalesReturnData;
use App\SalesReturnHeader;
use App\Traits\AgentLog;
use App\Traits\EndProductLog;
use App\Users;
use App\Variant;
use Carbon\Carbon;
use EmployeeManager\Models\Employee;
use EndProductManager\Models\EndProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use RawMaterialManager\Models\RawMaterial;
use SalesRepManager\Models\SalesRep;
use SemiFinishProductManager\Models\SemiFinishProduct;
use StockManager\Classes\StockTransaction;
use App\LoadingHeader;
use SalesRepManager\Models\AgentCategory;
use BurdenManager\Models\ReusableProducts;

class SalesReturnController extends Controller
{
    use AgentLog;
    use EndProductLog;
    function __construct()
    {
        $this->middleware('permission:sales_return-index', ['only' => ['index']]);
        $this->middleware('permission:SettleDebitNote-list', ['only' => ['list']]);
        $this->middleware('permission:SettleDebitNoteReport-list', ['only' => ['rlist']]);
        $this->middleware('permission:sales_return-list', ['only' => ['slist']]);
        $this->middleware('permission:sales_return-noteList', ['only' => ['settleDebitNotes']]);
        $this->middleware('permission:PendingReturns-list', ['only' => ['list']]);
        $this->middleware('permission:sales_return-adminIndex', ['only' => ['adminIndex']]);
        $this->middleware('permission:sales_return-destroy', ['only' => ['destroy']]);
        $this->middleware('permission:sales_return-sendReturn', ['only' => ['sendReturn']]);
        $this->middleware('permission:sales_return-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:sales_return-print', ['only' => ['print']]);
        $this->middleware('permission:sales_return-printToCollect', ['only' => ['printToCollect']]);
        $this->middleware('permission:sales_return-adminShow', ['only' => ['adminShow']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agent_id = Auth::user()->id;
        $returns = SalesReturnHeader::whereAgentId($agent_id)->orderBy('created_at', 'desc')->get();
        return view('sales_return', compact('returns'));
    }

    public function adminIndex()
    {
        $returns = SalesReturnHeader::orderBy('created_at', 'desc')->get();
        return view('sales_return_admin', compact('returns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        // $products = EndProduct::orderBy('name')->get();
// $productsAgent = AgentBuffer::where('agent_id','=',Auth::user()->id)->get();
// foreach ($productsAgent as $key => $value) {
//   $data = json_decode($value->dataset);
//   // foreach ($data as $key => $value) {
// var_dump($value[0]);
        // }
//   foreach ($value->dataset as $key => $value) {
// var_dump($value);
//   }
// }
        $productsArray = [];
        $agent_id = Auth::user()->id;
        $agentCategories = AgentCategory::where('sales_ref_id', '=', $agent_id)->where('status', '=', 1)->get();

        foreach ($agentCategories as $key => $agentCategory) {
            if ($agentCategory->return_mode == 2) {
                $products = EndProduct::where('category_id', '=', $agentCategory->category_id)->orderBy('name')->get();
                foreach ($products as $key => $product) {
                    $productsArray[] = $product;
                }
            }
            if ($agentCategory->return_mode == 1) {
                $products = EndProduct::where('category_id', '=', $agentCategory->category_id)->where('reusable_type', '!=', 1)->orderBy('name')->get();
                foreach ($products as $key => $product) {
                    $productsArray[] = $product;
                }
            }
        }
        $agents = SalesRep::whereHas('roles', function ($query) {
            $query->where('slug', 'like', 'sales-ref');
        })->get();
        return view('create_sales_return', compact(['agents', 'productsArray']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'end_product_ids' => 'required',
            'unit_values' => 'required',
            'qty' => 'required',
            'time' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->with([
                    'error' => true,
                    'error.title' => 'Error !',
                    'error.message' => 'Please Add Items Before Create Return Request'
                ]);
        }

        $agent_id = Sentinel::getUser()->id;
        $shop_type = Sentinel::getUser()->desc1;
        $end_product_ids = $request->end_product_ids;
        $unit_values = $request->unit_values;;
        $type = $request->type;;
        $qty = $request->qty;
        $time = $request->time;
        $total_qty = 0;
        $total_value = $request->total_value;

        $sx = SalesReturnHeader::whereAgentId($agent_id)->whereStatus(1)->where('time', '=', AgentBuffer::time[$time])->first();

        if (!empty($sx)) {
            return redirect(route('sales_return.index'))->with([
                'warning' => true,
                'warning.title' => 'Something Went Wrong !',
                'warning.message' => 'You have un approved sales return.Please contact system administrator'
            ]);
        }

        if (!empty($agent_id)) {
            DB::transaction(function () use ($agent_id, $total_value, $end_product_ids, $type, $total_qty, $unit_values, $qty, $time,$shop_type) {
                $sales_return = SalesReturnHeader::create([
                    'agent_id' => $agent_id,
                    'total' => $total_value,
                    'shop_type' => $shop_type,
                    'time' => $time,
                    'debit_note_number' => 'SR-' . Carbon::now()->format('ymdHis')
                ]);
                foreach ($end_product_ids as $key => $end_product_id) {
                    SalesReturnData::create([
                        'sales_return_header_id' => $sales_return->id,
                        'end_product_id' => $end_product_id,
                        'type' => $type[$key],
                        'unit_value' => $unit_values[$key] != null ? $unit_values[$key] : 0,
                        'qty' => $qty[$key] != null ? $qty[$key] : 0,
                    ]);
                    $total_qty += $qty[$key];

//                    switch ($type[$key]){
//                        case 1:
//                            StockTransaction::updateRawMaterialAvailableQty(1,90,$qty[$key],);
//                            StockTransaction::rawMaterialStockTransaction(1,$unit_values[$key],90,$qty[$key],$sales_return->debit_note_number,1);
//                            break;
//
//                        case 2:
//                            StockTransaction::updateRawMaterialAvailableQty(1,92,$qty[$key],);
//                            StockTransaction::rawMaterialStockTransaction(1,$unit_values[$key],92,$qty[$key],$sales_return->debit_note_number,1);
//
//                            break;
//
//                        case 4:
//                            StockTransaction::updateEndProductAvailableQty(1,$end_product_id,$qty[$key]);
//                            StockTransaction::endProductStockTransaction(1,$end_product_id,$unit_values[$key],$unit_values[$key],$unit_values[$key],$qty[$key],$sales_return->debit_note_number,1);
//                            break;
//                    }

                }
                $sales_return->save();

                return redirect(route('sales_return.index'))->with([
                    'success' => true,
                    'success.title' => 'Congratulations !',
                    'success.message' => 'Sales return created!'
                ]);
            });
            return redirect(route('sales_return.index'))->with([
                'error' => true,
                'error.title' => 'Error !',
                'error.message' => 'Something Went wrong'
            ]);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $products = EndProduct::all();
        $invoice = SalesReturnHeader::whereId($id)->first();
        $items = SalesReturnData::where('sales_return_header_id', '=', $id)->get();
        return view('show_sales_return', compact(['items', 'products', 'invoice']));
    }

    public function adminShow(Request $request, SalesReturnHeader $salesReturnHeader)
    {
        $invoice = $salesReturnHeader;
        $items = SalesReturnData::where('sales_return_header_id', '=', $salesReturnHeader->id)->get();
        return view('show_sales_return_admin', compact(['items', 'invoice']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $invoice = SalesReturnHeader::whereId($id)->first();

      //  $data = SalesReturnData::where('sales_return_header_id','=',$id)->get();
        $rep = SalesRep::where('id', '=', $invoice->agent_id)->first();
        $categories = $rep->categories->pluck('id');

        $end_products = EndProduct::whereIn('category_id', $categories)->orderBy('category_id')->get();

       // $drivers = Employee::whereDesignationId(2)->get()->pluck('first_name', 'id');
        return view('edit_sales_return', compact('invoice', 'end_products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {

        /* $header = SalesReturnHeader::whereId($id)->first();
        if ($header->status == 1) {
            $header->status = 0;
            $header->save();

            $agent_id = Auth::user()->id;
            $end_product_ids = $request->end_product_ids;
            $unit_values = $request->unit_values;
            $qty = $request->qty;
            $time = $header->time;
            $total_qty = 0;
            $total_value = $request->total_value;

            if (!empty($agent_id)) {
                DB::transaction(function () use ($request, $header, $agent_id, $total_value, $end_product_ids, $total_qty, $unit_values, $qty, $time) {
                    $sales_return = SalesReturnHeader::create([
                        'agent_id' => $agent_id,
                        'total' => $request->grand_total,
                        'time' => $time,
                        'debit_note_number' => $header->debit_note_number . '-' . $header->count,
                        'count' => $header->count + 1
                    ]);
                    foreach ($end_product_ids as $key => $end_product_id) {
                        SalesReturnData::create([
                            'sales_return_header_id' => $sales_return->id,
                            'end_product_id' => $end_product_id,
                            'unit_value' => $unit_values[$key] != null ? $unit_values[$key] : 0,
                            'qty' => $qty[$key] != null ? $qty[$key] : 0,
                        ]);
                        $total_qty += $qty[$key];

//                    switch ($type[$key]){
//                        case 1:
//                            StockTransaction::updateRawMaterialAvailableQty(1,90,$qty[$key],);
//                            StockTransaction::rawMaterialStockTransaction(1,$unit_values[$key],90,$qty[$key],$sales_return->debit_note_number,1);
//                            break;
//
//                        case 2:
//                            StockTransaction::updateRawMaterialAvailableQty(1,92,$qty[$key],);
//                            StockTransaction::rawMaterialStockTransaction(1,$unit_values[$key],92,$qty[$key],$sales_return->debit_note_number,1);
//
//                            break;
//
//                        case 4:
//                            StockTransaction::updateEndProductAvailableQty(1,$end_product_id,$qty[$key]);
//                            StockTransaction::endProductStockTransaction(1,$end_product_id,$unit_values[$key],$unit_values[$key],$unit_values[$key],$qty[$key],$sales_return->debit_note_number,1);
//                            break;
//                    }

                    }
                    $sales_return->save();

                    return redirect(route('sales_return.index'))->with([
                        'success' => true,
                        'success.title' => 'Congratulations !',
                        'success.message' => 'Sales return accepted!'
                    ]);
                });
                return redirect(route('sales_return.index'))->with([
                    'error' => true,
                    'error.title' => 'Error !',
                    'error.message' => 'Something Went wrong'
                ]);
            }
        } else {
            return redirect(route('sales_return.index'))->with([
                'error' => true,
                'error.title' => 'Error !',
                'error.message' => 'Something Went wrong'
            ]);
        }*/
//************************************************************************


        $header = SalesReturnHeader::whereId($id)->first();
        if ($header->status == 1) {
            //$header->status = 0;
            //$header->save();


        $agent_id = Sentinel::getUser()->id;
        $shop_type = Sentinel::getUser()->desc1;
        $end_product_ids = $request->end_product_ids;
        $unit_values = $request->unit_values;
        $type = $request->type;
        $qty = $request->send_qty;
        $all_qty = $request->qty;
        $time = $request->time;
        $total_qty = 0;
        $total_value = $request->total_value;
        $all_total_value = $request->all_total_value;


            DB::transaction(function () use ($request,$id,$type, $header, $agent_id, $total_value,$all_total_value, $end_product_ids, $total_qty, $unit_values, $qty,$all_qty, $time,$shop_type) {

                $sales_return = SalesReturnHeader::create([
                    'agent_id' => $agent_id,
                    'total' => $total_value,
                    'all_total' => $all_total_value,
                    'shop_type' => $shop_type,
                    'time' => $time,
                    'debit_note_number' => $header->debit_note_number . '-' . $header->count,
                    'count' => $header->count + 1
                     ]);



                foreach ($end_product_ids as $key => $end_product_id) {
                    $retData = SalesReturnData::where('sales_return_header_id','=',$id)->where('end_product_id','=',$end_product_id)->first();
                    $retData->delete();

                    $item = SalesReturnData::create([
                        'sales_return_header_id' => $sales_return->id,
                        'end_product_id' => $end_product_id,
                        'unit_value' => $unit_values[$key] != null ? $unit_values[$key] : 0,
                        'type' => $type[$key],
                        'qty' => $qty[$key] != null ? $qty[$key] : 0,
                        'all_qty' => $all_qty[$key] != null ? $all_qty[$key] : 0,
                        'total' => (($unit_values[$key])*($qty[$key]))

                    ]);



                    $header->delete();

                    $endP = EndProduct::find($end_product_id);
                    $total_qty += $qty[$key];
                    $percentage = $this->calPercentage($end_product_id);
                    $item->return_stat = $percentage;
                    $item->category_id = $endP->category_id;
                    $item->agent_id = $agent_id;
                    $item->save();

                }
                $sales_return->save();

                return redirect(route('sales_return.index'))->with([
                    'success' => true,
                    'success.title' => 'Congratulations !',
                    'success.message' => 'Sales return accepted!'
                ]);
            });
            return redirect(route('sales_return.index'))->with([
                'error' => true,
                'error.title' => 'Error !',
                'error.message' => 'Something Went wrong'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function returnDiscount($rid)
    {
        $tretDis = SalesReturnHeader::find($rid);
        if($tretDis->is_system_generated == 0) {

            $retID = SalesReturnData::where('sales_return_header_id', '=', $rid)->get();
            //  $rowval = 0;
            $totdis = 0;
            foreach ($retID as $retIDs) {

                $chk = AgentCategory::where('sales_ref_id', '=', $retIDs->agent_id)->where('category_id', '=', $retIDs->category_id)->first();
                if ($chk->discount_mode == 1) {
                    $ep = EndProduct::find($retIDs->end_product_id);
                    $setRet = SalesReturnData::where('end_product_id', '=', $retIDs->end_product_id)->where('sales_return_header_id', '=', $rid)->first();
                    $setRet->commission = $setRet->total * ($ep->commission / 100);
                    $setRet->unit_discount = $setRet->qty * ($ep->unit_discount);
                    $totdis1 = (($setRet->total * ($ep->commission / 100)) + ($setRet->qty * ($ep->unit_discount)));
                    $setRet->total_discount = $totdis1;
                    $totdis = $totdis1 + $totdis;
                    $rowval1 = ($setRet->total) - (($setRet->total * ($ep->commission / 100)) + ($setRet->qty * ($ep->unit_discount)));
                    $setRet->row_value = $rowval1;
                    // $rowval = $rowval + $rowval1;
                    $setRet->save();
                }
            }


            $tretDis->return_discount = $totdis;
            $tretDis->save();
        }

        return redirect(route('return_note.approve',$rid));

    }




    public function destroy($id)
    {
        $return = SalesReturnHeader::find($id);
        if ($return->status == 1) {


            $retData = SalesReturnData::where('sales_return_header_id','=',$id)->get();
            foreach ($retData as $key => $end_product) {

                $retData1 = SalesReturnData::where('id','=',$end_product->id)->first();

                $retData1->delete();
            }
            $return->delete();

            return redirect(route('sales_return.index'))->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Sales return deleted!'
            ]);
        }
    }

    public function adminDestroy(Request $request)
    {

        $return = SalesReturnHeader::find($request->sales_return_header);
        if ($return->status == 2) {


            $retData = SalesReturnData::where('sales_return_header_id','=',$request->sales_return_header)->get();
            foreach ($retData as $key => $end_product) {

                $retData1 = SalesReturnData::where('id','=',$end_product->id)->first();

                $retData1->delete();
            }
            $return->delete();

            return redirect(route('admin.return'))->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Sales return deleted!'
            ]);
        }
    }


    public function accept(Request $request)
    {

        $invoice = SalesReturnHeader::whereId($request->sales_return_header)->first();

        $items = SalesReturnData::where('sales_return_header_id', '=', $invoice->id)->get();
        $employees = Employee::pluck('full_name', 'id');


        if ($invoice->is_system_generated) {
            $invoice->status = 3;
            $invoice->save();


            foreach ($items as $item) {
                $is_varients =Variant::where('end_product_id','=',$item->end_product_id)->where('return_accepted','=',1)->first();
//                $is_varients = EndProduct::find($item->end_product_id)->variants;

                $stock_rotation_qty_adjusted = $item->qty;

                if (!empty($is_varients) && $is_varients->count() > 0) {
                    $size = $is_varients->size;
                    $stock_rotation_qty_adjusted = ($item->qty)/$size;
                }


                StockTransaction::updateEndProductAvailableQty(1, $item->end_product_id, $item->qty);
                StockTransaction::endProductStockTransaction(1, $item->end_product_id, 0, 0, 0, $item->qty, $invoice->debit_note_number, 1);
                $end_product = EndProduct::find($item->end_product_id);
                $this->recordEndProductOutstanding($end_product,1, $item->qty, 'System Genarated Return', $invoice->id, 'App\\SalesReturnHeader',$invoice->agent_id,12,1,$invoice->id);


                $reusableReturn = new ReusableProducts();
                $reusableReturn->reusable_type = $item->endProduct->reusable_type;
                $reusableReturn->product_id = $item->end_product_id;
                $reusableReturn->note_id = $item->header->debit_note_number;
                $reusableReturn->qty = $stock_rotation_qty_adjusted;
                $reusableReturn->expected_weight = $item->qty;
                $reusableReturn->accepted_weight = $item->qty;
                //collect type (1->semi,2->endproduct,3->salesReturn,4->MergeRemoval,5->StoresReturn)
                $reusableReturn->collect_type = 3;
                $reusableReturn->status = 1;
                $reusableReturn->category_id = $end_product->category_id;
                $reusableReturn->agent_id = $invoice->agent_id;
                $reusableReturn->type_id = 7;//1=merge-reuse,2=merge-destroy,3=merge-lost,4=stores-reuse,5=stores-destroy,6=stores-lost,7=System Genarated,8=Return-Reuse,9=Return-Destroy,10=Return-SR,11-Return-Factory,12=Return-Lost

                $reusableReturn->type = 'stock_rotation';
                $reusableReturn->save();
            }

            return redirect()->route('admin.return');
        }

        return view('edit_sales_return_approve', compact('items', 'invoice', 'employees'));

        if ($request->sumbit == 'approve') {
            $srh = SalesReturnHeader::whereId($request->sales_return_header)->first();
            $srh->status = 2;
            $srh->save();
            return redirect(route('admin.return'));
        } else {
            $invoice = SalesReturnHeader::whereId($request->sales_return_header)->first();
            $invoice->status = 2;
            $invoice->save();
            $items = SalesReturnData::where('sales_return_header_id', '=', $invoice->id)->get();
            return redirect(route('admin.return'));
            return view('show_sales_return_pdf', compact(['items', 'invoice']));
        }
    }

    public function accepted(Request $request)
    {

        $invoice = SalesReturnHeader::whereId($request->sales_return_header)->first();
        $invoice->status = 3;
        $invoice->accepted_data = json_encode($request->all());
        $invoice->received_by = Auth::user()->id;
        $invoice->received_at = Carbon::now();
        $invoice->collected_by = $request->collectBy;
        $invoice->save();

        $reusable_weight = $request->reusable_weight;
        $reusable_qty = $request->reusable_qty;

        $destroy_qty = $request->destroy_qty;
        $stock_rotation_qty = $request->stock_rotation_qty;
        $factory_fault_qty = $request->factory_fault_qty;
        $lost_qty = $request->lost_qty;


        foreach ($request->end_product_ids as $key => $end_product_id) {

//            $is_varients = EndProduct::find($end_product_id)->variants;
            $is_varients =Variant::where('end_product_id','=',$end_product_id)->where('return_accepted','=',1)->first();

            $factory_fault_qty_adjusted = $factory_fault_qty[$key];
            $stock_rotation_qty_adjusted = $stock_rotation_qty[$key];

            if (!empty($is_varients)) {
                $size = $is_varients->size;
                $factory_fault_qty_adjusted = $factory_fault_qty[$key]*$size;
                $stock_rotation_qty_adjusted = $stock_rotation_qty[$key]*$size;
            }

            if ($reusable_qty[$key] != 0) {
                $endP = EndProduct::whereId($end_product_id)->first();
                $reusableReturn = new ReusableProducts();
                $reusableReturn->reusable_type = $request->reusable_type_end_product[$key];
                $reusableReturn->product_id = $end_product_id;
                $reusableReturn->note_id = $invoice->debit_note_number;
                $reusableReturn->qty = $reusable_qty[$key];
                $reusableReturn->expected_weight = $reusable_weight[$key];
                $reusableReturn->accepted_weight = 0;
                //collect type (1->semi,2->endproduct,3->salesReturn,4->MergeRemoval,5->StoresReturn)
                $reusableReturn->collect_type = 3;
                $reusableReturn->status = 1;
                $reusableReturn->category_id = $endP->category_id;
                $reusableReturn->agent_id = $invoice->agent_id;
                $reusableReturn->type_id = 8;//1=merge-reuse,2=merge-destroy,3=merge-lost,4=stores-reuse,5=stores-destroy,6=stores-lost,7=System Genarated,8=Return-Reuse,9=Return-Destroy,10=Return-SR,11-Return-Factory,12=Return-Lost

                $reusableReturn->type = 're_use';
                $reusableReturn->save();


                $trans = new ReusableStock();
                $trans->causer_id = $end_product_id;
                $trans->causer_type = 'APP\\EndProduct';
                $trans->qty = $reusable_qty[$key];
                $trans->expected_weight = $reusable_weight[$key];
                $trans->sales_return_header_id = $invoice->id;
                $trans->reusable_product_id = $reusableReturn->id;
                $trans->reusable_type = $endP->reusable_type;
                $trans->received_type = 'debit_re_use';
                $trans->save();


            }

            if ($destroy_qty[$key] != 0) {
                $endP = EndProduct::whereId($end_product_id)->first();
                $reusableReturn = new ReusableProducts();
                $reusableReturn->reusable_type = $request->reusable_type_end_product[$key];
                $reusableReturn->product_id = $end_product_id;
                $reusableReturn->note_id = $invoice->debit_note_number;
                $reusableReturn->qty = $destroy_qty[$key];
                $reusableReturn->expected_weight = $destroy_qty[$key];
                $reusableReturn->accepted_weight = 0;
                //collect type (1->semi,2->endproduct,3->salesReturn,4->MergeRemoval,5->StoresReturn)
                $reusableReturn->collect_type = 3;
                $reusableReturn->status = 1;
                $reusableReturn->category_id = $endP->category_id;
                $reusableReturn->agent_id = $invoice->agent_id;
                $reusableReturn->type_id = 9;//1=merge-reuse,2=merge-destroy,3=merge-lost,4=stores-reuse,5=stores-destroy,6=stores-lost,7=System Genarated,8=Return-Reuse,9=Return-Destroy,10=Return-SR,11-Return-Factory,12=Return-Lost

                $reusableReturn->type = 'destroy';
                $reusableReturn->save();
            }

            if ($stock_rotation_qty[$key] != 0) {
                $end_product = EndProduct::find($end_product_id);
                $reusableReturn = new ReusableProducts();
                $reusableReturn->reusable_type = $request->reusable_type_end_product[$key];
                $reusableReturn->product_id = $end_product_id;
                $reusableReturn->note_id = $invoice->debit_note_number;
                $reusableReturn->qty = $stock_rotation_qty_adjusted;
                $reusableReturn->expected_weight = $stock_rotation_qty_adjusted;
                $reusableReturn->accepted_weight = 0;
                //collect type (1->semi,2->endproduct,3->salesReturn,4->MergeRemoval,5->StoresReturn)
                $reusableReturn->collect_type = 3;
                $reusableReturn->status = 1;
                $reusableReturn->category_id = $end_product->category_id;
                $reusableReturn->agent_id = $invoice->agent_id;
                $reusableReturn->type_id = 10;//1=merge-reuse,2=merge-destroy,3=merge-lost,4=stores-reuse,5=stores-destroy,6=stores-lost,7=System Genarated,8=Return-Reuse,9=Return-Destroy,10=Return-SR,11-Return-Factory,12=Return-Lost

                $reusableReturn->type = 'stock_rotation';
                $reusableReturn->save();

                StockTransaction::updateEndProductAvailableQty(1, $end_product_id, $stock_rotation_qty_adjusted);
                StockTransaction::endProductStockTransaction(1, $end_product_id, 0, 0, 0, $stock_rotation_qty_adjusted, $invoice->debit_note_number, 1);

                $this->recordEndProductOutstanding($end_product,1, $stock_rotation_qty_adjusted, 'Stock Rotation-Sales Return', $invoice->id, 'App\\SalesReturnHeader',$invoice->agent_id,13,1,$invoice->id);

            }

            if ($factory_fault_qty[$key] != 0) {
                $end_product = EndProduct::find($end_product_id);
                $reusableReturn = new ReusableProducts();
                $reusableReturn->reusable_type = $request->reusable_type_end_product[$key];
                $reusableReturn->product_id = $end_product_id;
                $reusableReturn->note_id = $invoice->debit_note_number;
                $reusableReturn->qty = $factory_fault_qty_adjusted;
                $reusableReturn->expected_weight = $factory_fault_qty_adjusted;
                $reusableReturn->accepted_weight = 0;
                //collect type (1->semi,2->endproduct,3->salesReturn,4->MergeRemoval,5->StoresReturn)
                $reusableReturn->collect_type = 3;
                $reusableReturn->status = 1;
                $reusableReturn->category_id = $end_product->category_id;
                $reusableReturn->agent_id = $invoice->agent_id;
                $reusableReturn->type_id = 11;//1=merge-reuse,2=merge-destroy,3=merge-lost,4=stores-reuse,5=stores-destroy,6=stores-lost,7=System Genarated,8=Return-Reuse,9=Return-Destroy,10=Return-SR,11-Return-Factory,12=Return-Lost

                $reusableReturn->type = 'factory_fault';
                $reusableReturn->save();

                StockTransaction::updateEndProductAvailableQty(1, $end_product_id, $factory_fault_qty_adjusted);
                StockTransaction::endProductStockTransaction(1, $end_product_id, 0, 0, 0, $factory_fault_qty_adjusted, $invoice->debit_note_number, 1);

                $this->recordEndProductOutstanding($end_product,1, $factory_fault_qty_adjusted, 'Factory Fault-Sales Return', $invoice->id, 'App\\SalesReturnHeader',$invoice->agent_id,14,1,$invoice->id);

            }

            if ($lost_qty[$key] != 0) {
                $end_product = EndProduct::find($end_product_id);

                $reusableReturn = new ReusableProducts();
                $reusableReturn->reusable_type = $request->reusable_type_end_product[$key];
                $reusableReturn->product_id = $end_product_id;
                $reusableReturn->note_id = $invoice->debit_note_number;
                $reusableReturn->qty = $lost_qty[$key];
                $reusableReturn->expected_weight = $lost_qty[$key];
                $reusableReturn->accepted_weight = 0;
                //collect type (1->semi,2->endproduct,3->salesReturn,4->MergeRemoval,5->StoresReturn)
                $reusableReturn->collect_type = 3;
                $reusableReturn->status = 1;
                $reusableReturn->category_id = $end_product->category_id;
                $reusableReturn->agent_id = $invoice->agent_id;
                $reusableReturn->type_id = 12;//1=merge-reuse,2=merge-destroy,3=merge-lost,4=stores-reuse,5=stores-destroy,6=stores-lost,7=System Genarated,8=Return-Reuse,9=Return-Destroy,10=Return-SR,11-Return-Factory,12=Return-Lost

                $reusableReturn->type = 'lost';
                $reusableReturn->save();
            }

        }


        return redirect(route('admin.return'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => 'Sales return Accepted!'
        ]);

    }

    public function print(Request $request)
    {
        $invoice = SalesReturnHeader::whereId($request->sales_return_header)->first();
        $items = SalesReturnData::where('sales_return_header_id', '=', $invoice->id)->get();
        return view('show_sales_return_print', compact('items', 'invoice'));
    }

    public function printGet($sales_return_header)
    {
        $invoice = SalesReturnHeader::whereId($sales_return_header)->first();
        $items = SalesReturnData::where('sales_return_header_id', '=', $invoice->id)->get();
        return view('show_sales_return_print', compact('items', 'invoice'));
    }

    public function printToCollect($sales_return_header)
    {
        $invoice = SalesReturnHeader::whereId($sales_return_header)->first();
        $items = SalesReturnData::where('sales_return_header_id', '=', $invoice->id)->get();
        return view('show_sales_return_print_collect', compact('items', 'invoice'));
    }

    public function sendReturn($return_id)
    {
        $invoice = SalesReturnHeader::whereId($return_id)->first();
        $invoice->status = 2;
        $invoice->save();
        return redirect(route('sales_return.index'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => 'Sales return Send Success!'
        ]);
    }

    public function settleDebitNotes()
    {
        $agents = SalesRep::all()->pluck('email', 'id');
        return view('settle_debit_note', compact('agents'));
    }

    public function settleDebitNotesTableData(Request $request)
    {

        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $recordsCount = 0;
        $date = $request->date_range;
        $columns = ['debit_note_number', 'status', 'created_by'];
        $order_column = $columns[$order_by[0]['column']];

        $order_by = 'created_at';
        $order_by_str = 'DESC';
        $records = SalesReturnHeader::tableData($order_column, $order_by_str, $start, $length);
   //     $recordsCount = SalesReturnHeader::filterData($request->date_range, $request->agent, $request->status)->where('is_system_generated', '=', $request->type)->count();
        $recordsCount = SalesReturnHeader::filterData($request->date_range, $request->agent, $request->status, $request->type)->count();

        $totalCount = SalesReturnHeader::count();

        $stat_data = SalesReturnHeader::query();

        $total_sum = 0;
        $dis_sum = 0;
        $hold_sum = 0;
        $final_sum = 0;

        if ($request->filter == true) {


            $status = $request->status;
            $agent = $request->agent;
            $type = $request->type;
      //
            $date_range = $request->date_range;
            $recordsCount = $records->filterData($date_range, $agent, $status,$type)->count();

            $stat_data = $stat_data->filterData($date_range, $agent, $status,$type)->sumData()->first();

            $records = $records->filterData($date_range, $agent, $status , $type)->get();

            $total_sum = $stat_data->total_sum;
            $dis_sum = $stat_data->dis_sum;
            $hold_sum = $stat_data->hold_sum;
            $final_sum = $stat_data->final_sum;
        } elseif (is_null($search) || empty($search)) {
            /*$dateX = Carbon::now()->format('Y-m-d') . '%';

            $stat_data = $stat_data->whereRaw(" status =5 and `created_at` like '$dateX' or `status` = 4 ")->sumData()->first();
            $records = $records->whereRaw(" status =5 and `created_at` like '$dateX' or `status` = 4 ");

            $records = $records->get();
            $recordsCount = $records->count();*/
        } else {
            //$recordsCount = $records->count();
        }


        $data = [];
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;

        $order_display_id = 0;




        foreach ($records as $key => $record) {

            $btn_status = null;
            $btn_action = null;
            $btn_edit = null;
            $btn_agent_approve = null;
            $btn_admin_approve = null;
            $viewBtn = null;
            $printBtn = null;
            if((Sentinel::check()->roles[0]->slug=='accountant') || (Sentinel::check()->roles[0]->slug=='owner')){
                if ($record->status == 3) {
                    $btn_status = "<button class='btn btn-sm btn-warning'> Approving </button>";
                    $btn_action = "<a href='approve/show/" . $record->id . "' class='btn btn-sm btn-outline-success'>Approve</a>";
                } elseif ($record->status == 4) {
                    $btn_status = "<button class='btn btn-sm btn-info'> Clearing </button>";
                    $btn_action = "<a href='payment/show/" . $record->id . "' class='btn btn-sm btn-outline-primary mr-2'> Pay </a>";
                    $viewBtn = '<a href="' . route('sales_return.show', $record->id) . '" class="btn btn-outline-info btn-sm"><i class="fa fa-eye"></i> </a>';
                } elseif ($record->status == 5) {
                    $btn_status = "<button class='btn btn-sm btn-danger'> Settled </button>";
                    $viewBtn = '<a href="' . route('sales_return.show', $record->id) . '" class="btn btn-outline-info btn-sm mr-2"><i class="fa fa-eye"></i> </a>';
                    $printBtn = '<a href="' . route('admin.sales_return_print_get', $record->id) . '" class="btn btn-outline-danger btn-sm mr-2"><i class="fa fa-print"></i> </a>';
                    }
                }elseif(Sentinel::check()->roles[0]->slug=='stores-end-product'){
                if ($record->status == 3 && $record->is_system_generated == 1) {
                    $btn_status = "<button class='btn btn-sm btn-warning'> Approving </button>";
                    $btn_action = "<a href='approve/show/" . $record->id . "' class='btn btn-sm btn-outline-success'>Approve</a>";
                } elseif ($record->status == 4 ) {
                    $btn_status = "<button class='btn btn-sm btn-info'> Clearing </button>";
                    $btn_action = "<a href='payment/show/" . $record->id . "' class='btn btn-sm btn-outline-primary mr-2'> Pay </a>";
                    $viewBtn = '<a href="' . route('sales_return.show', $record->id) . '" class="btn btn-outline-info btn-sm"><i class="fa fa-eye"></i> </a>';
                } elseif ($record->status == 5 && $record->is_system_generated == 1) {
                    $btn_status = "<button class='btn btn-sm btn-danger'> Settled </button>";
                    $viewBtn = '<a href="' . route('sales_return.show', $record->id) . '" class="btn btn-outline-info btn-sm mr-2"><i class="fa fa-eye"></i> </a>';
                    $printBtn = '<a href="' . route('admin.sales_return_print_get', $record->id) . '" class="btn btn-outline-danger btn-sm mr-2"><i class="fa fa-print"></i> </a>';
                }
            }else{
                if ($record->status == 3) {
                    $btn_status = "<button class='btn btn-sm btn-warning'> Approving </button>";
                    $viewBtn = '<a href="' . route('sales_return.show', $record->id) . '" class="btn btn-outline-info btn-sm mr-2"><i class="fa fa-eye"></i> </a>';



                } elseif
                ($record->status == 4) {
                    $btn_status = "<button class='btn btn-sm btn-info'> Clearing </button>";
                    $viewBtn = '<a href="' . route('sales_return.show', $record->id) . '" class="btn btn-outline-info btn-sm mr-2"><i class="fa fa-eye"></i> </a>';

                } elseif ($record->status == 5) {
                    $btn_status = "<button class='btn btn-sm btn-danger'> Settled </button>";
                    $viewBtn = '<a href="' . route('sales_return.show', $record->id) . '" class="btn btn-outline-info btn-sm mr-2"><i class="fa fa-eye"></i> </a>';
                    $printBtn = '<a href="' . route('admin.sales_return_print_get', $record->id) . '" class="btn btn-outline-danger btn-sm mr-2"><i class="fa fa-print"></i> </a>';

                }

            }

            $returnDis = 0;
            $holdAmount = 0;
            $finalAmount = 0;


            if ($record->return_discount != 0 && $record->return_discount != Null) {
                $returnDis = $record->return_discount;
            }
            if ($record->hold_amount != 0 && $record->hold_amount != Null) {
                $holdAmount = $record->hold_amount;
            }
            if ($record->final_amount != 0 && $record->final_amount != Null) {
                $finalAmount = $record->final_amount;
            }

            $data[$i] = array(
                $record->id,
                $record->created_at,
                $record->debit_note_number,
                $record->agent->name_with_initials . "<br>" . $record->agent->territory,
                number_format($record->total, 2),
                number_format($returnDis, 2),
                number_format($holdAmount, 2),
                number_format($finalAmount, 2),
                $btn_status,
                $btn_action . $printBtn . $viewBtn
            );
            $i++;
        }


//        if ($recordsCount == 0) {
//            $data = [];
//        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($recordsCount),
            "recordsFiltered" => intval($recordsCount),
            "data" => $data,
            "total_sum" => number_format($total_sum, 2),
            "dis_sum" => number_format($dis_sum, 2),
            "hold_sum" => number_format($hold_sum, 2),
            "final_sum" => number_format($final_sum, 2),
        ];

        return json_encode($json_data);
    }

    public
    function settleDebitNoteApproveView($salesReturnId)
    {

        $return = SalesReturnHeader::whereId($salesReturnId)->first();
        $ur = Users::find($return->agent_id);

        return view('settle_debit_note_approve', compact('return','ur'));
    }

    public
    function settleDebitNoteApprove(Request $request)
    {
        $return = SalesReturnHeader::whereId($request->return_id)->first();
        $returnDis = $request->returnDis;
        $holdAmount = $request->holdAmount;

        $return->return_discount = $returnDis;
        $return->hold_amount = $holdAmount;
        $return->final_amount = ($return->total) - ($returnDis + $holdAmount);
        $return->status = 4;
        $return->save();

        return redirect(route('admin.settle_notes'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => 'Settle Note Approved Success!'
        ]);

    }

    public
    function settleDebitNotePaymentView($id)
    {
        $payment = SalesReturnHeader::find($id);
        $invoices = LoadingHeader::whereHas('salesOrder', function ($q) use ($payment) {
            return $q->whereCreatedBy($payment->agent_id);
        })->where('status', '>', 1)->whereIsPaid(0)->get();
        $payment_type = 'PAY';
        return view('settle-note.payment_list', compact('payment', 'invoices', 'payment_type'));
    }

    public function processPayment(Request $request, $id)
    {

        $paid_amount = 0;
        $payment = null;
        $returns = SalesReturnHeader::find($request->return_id);
        $paid_amount = $returns->final_amount;
        $invoices = $request->invoices;
        $invoice_list = [];


        $invoice_order = explode(',', $request->invoice_ids);

        if($returns->status < 5) {


            foreach ($invoice_order as $order) {
                foreach ($invoices as $key => $invoice) {
                    if ($order == $key) {
                        $loading = LoadingHeader::find($key);
                        $paid_amount = $paid_amount - $invoice;

                        $payd = new PaymentDetail();
                        $payd->pay_id = $returns->id;
                        $payd->agent_id = $returns->agent_id;
                        $payd->load_header_id = $key;
                        $payd->desc1 = 'DEBIT';
                        $payd->due_balance = $invoice;

                        if ($paid_amount >= 0) {
                            $loading->is_paid = 1;
                            $loading->paid_amount = $invoice;

                            $payd->paid_amount = $invoice;
                            array_push($invoice_list, ['loading_header_id' => $key, 'invoice_value' => $invoice, 'is_full_payment' => 1, 'settled_value' => $invoice]);
                        } else {
                            $loading->paid_amount = ($loading->paid_amount) + $invoice + $paid_amount;

                            $payd->paid_amount = $invoice + $paid_amount;
                            array_push($invoice_list, ['loading_header_id' => $key, 'invoice_value' => $invoice, 'is_full_payment' => 0, 'settled_value' => $invoice + $paid_amount]);
                        }
                        $payd->save();
                        $loading->save();

                    }
                }
            }


            if ($request->payment_type == 'PAY') {

                $returns->paid_invoices = json_encode($invoice_list);
                $tp ="App\SalesReturnHeader";
                $chk = AgentInquiry::where('reference','=',$returns->id)->where('reference_type','=',$tp)->first();

                if(empty($chk)) {
                    $ref = SalesRep::find($returns->agent_id);
                    $this->recordOutstanding($ref, -($returns->final_amount), "Agent's Sales Return", $returns->id, 'App\\SalesReturnHeader', $returns->debit_note_number);
                    StockTransaction::updateAgentBalance(2, $returns->agent_id, $returns->final_amount);

                    $returns->status = 5;
                    $returns->save();
                }
            }
        }

        return redirect(route('admin.settle_notes'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => "Payment Settle Note Completed",
        ]);
    }

    public function wastageSemiIssue(Request $request){
        $semi_finish_product  = $request->semi_finish;
        $qty  = $request->qty;
        $description  = $request->description;

        $semi= SemiFinishProduct::find($semi_finish_product);


        $trans = new ReusableStock();
        $trans->causer_id = $semi_finish_product;
        $trans->causer_type = 'SemiFinishProductManager\\Models\\SemiFinishProduct';
        $trans->qty =$qty;
        $trans->expected_weight = $qty * $semi->weight;
        $trans->sales_return_header_id = null;
        $trans->reusable_product_id = $semi_finish_product;
        $trans->reusable_type = $semi->reusable_type;
        $trans->received_type = 'wastage';
        $trans->save();

        return redirect()->back();
    }

    function calPercentage($end_product_id)// in AgentSalesReturnController
    {
        $variant_size = 0;
        $endProduct = EndProduct::where('id', '=', $end_product_id)->first();
        $varainat = Variant::where('end_product_id', '=', $end_product_id)->where('return_accepted', '=', 1)->first();


        $last_seven_days_total_ordered_qty = LoadingData::where('end_product_id', '=', $end_product_id)->whereHas('header.salesOrder', function ($q) {
            return $q->where('created_by', '=', Auth::user()->id)->where('deleted_at', '=', null);
        })->where('created_at', '>', Carbon::now()->subDays(7)->format('Y-m-d H:i:s'))->sum('qty');

        $last_seven_days_total_ordered_vari_qty = LoadingDataVariant::where('end_product_id', '=', $end_product_id)->whereHas('header.salesOrder', function ($q) {
            return $q->where('created_by', '=', Auth::user()->id)->where('deleted_at', '=', null);
        })->where('created_at', '>', Carbon::now()->subDays(7)->format('Y-m-d H:i:s'))->sum('weight');


        $last_seven_days_total_returned_qty = SalesReturnData::where('end_product_id', '=', $end_product_id)->whereHas('header', function ($q) {
            return $q->where('agent_id', '=', Auth::user()->id)->where('status', '!=', 0)->where('is_system_generated', '=', 0)->where('deleted_at', '=', null);
        })->where('created_at', '>', Carbon::now()->subDays(7)->format('Y-m-d H:i:s'))->sum('all_qty');

        $percentage = 0;


        if (!empty($varainat) && $varainat->count() > 0) {
            $variant_size = $varainat->size;
            if ($last_seven_days_total_ordered_vari_qty != 0) {
                $percentage = ($last_seven_days_total_returned_qty / ($last_seven_days_total_ordered_vari_qty * $variant_size)) * 100;
            }
        } else {
            if ($last_seven_days_total_ordered_qty != 0) {
                $percentage = ($last_seven_days_total_returned_qty / $last_seven_days_total_ordered_qty) * 100;
            }
        }

        return $percentage;
    }

}
