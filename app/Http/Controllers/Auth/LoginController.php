<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Traits\SendSMS;
use App\User;
use Ichtrojan\Otp\Otp;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use UserManager\Models\Users;

class LoginController extends Controller
{
    use SendSMS;

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     *
     */

    protected $redirectTo = '/dsuper';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);
        $credentials = $request->only('email', 'password');

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if (Auth::validate($credentials)) {
            $user = User::whereEmail($request->email)->first();
            session()->put('usr', $request->email);
            session()->put('pwd', $request->password);
            if (!empty($user) && !empty($user->numbers) && $user->numbers->count() > 0) {
                $otp = new Otp();
                $otp_code = $otp->generate($user->email, 5, 3);
                foreach ($user->numbers as $number) {
                    $msg = "Dear $user->first_name , Please use $otp_code->token as the Buntalk OTP for current login";
                    $this->sendSMS($number->number, $msg);
                }
                $email = $request->email;
                $pw = $request->password;
                $credentials = [
                    'email' => $email,
                    'password' => $pw,
                ];

                return redirect(route('users.otp'));
//                return redirect('/otp', compact('email', 'pw'));
            }

        }


        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }
}
