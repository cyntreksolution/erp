<?php

namespace App\Http\Controllers;

use BurdenManager\Models\ReusableProducts;
use Carbon\Carbon;
use EndProductManager\Models\Category;
use EndProductManager\Models\EndProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Object_;
use SalesRepManager\Models\SalesRep;

class ReturnDashboardController extends Controller
{
    public function index()
    {
        $agents = SalesRep::pluck('name_with_initials', 'id');
        $categories = Category::pluck('name', 'id');
        $endProducts = EndProduct::pluck('name', 'id');
        return view('dashboard.sales-return.sales-return', compact('agents', 'categories', 'endProducts'));
    }

    public function formatQuery($period)
    {
        $format = '%Y-%m-%d';
        switch ($period) {
            case 'daily':
                $format = '%Y-%m-%d';
                break;
            case 'weekly':
                $format = '%u';
                break;
            case 'monthly':
                $format = '%Y-%m';
                break;
            case 'annually':
                $format = '%Y';
                break;

        }

        if ($period != 'quarterly') {
            $format_query = "DATE_FORMAT(reusable_products.created_at, '$format') as new_date";
        } else {
            $format_query = "QUARTER(reusable_products.created_at) as new_date";
        }
        return $format_query;
    }

    public function mainChart(Request $request)
    {
        $date_range = $request->date_range;
        $agents = !empty($request->agents) ? $request->agents : [];
        $categories = !empty($request->categories) ? $request->categories : [];
        $end_product = !empty($request->end_product) ? $request->end_product : [];
        $reuse_type = !empty($request->reuse_type) ? $request->reuse_type : [];
        $days = !empty($request->days) ? $request->days : [];
        $period = $request->period;
        $type = $request->type;


        $query = ReusableProducts::query();

        $dates = explode(' - ', $date_range);
        $start = $dates[0];
        $end = $dates[1];

        $format_query = $this->formatQuery($period);

        if ($type == 'qty-wise') {
            $qty_query = "sum(qty) as qty,WEEKDAY(reusable_products.created_at)";
        } elseif ($type == 'value-wise') {
            $qty_query = "sum(qty * end_product.distributor_price) as qty";
        }

        $query = $query->select(
            DB::raw("$qty_query"),
            DB::raw("$format_query")
        )
            ->leftJoin('end_product', 'end_product.id', '=', 'reusable_products.product_id')
            ->where('qty', '<>', 0)
            ->where('type_id', '>', 6)
            ->groupBy('new_date')
            ->orderBy('new_date', 'asc');



        $agents = array_filter($agents);
        if (!empty($agents)) {
            $query = $query->whereIn('agent_id', $agents);
        }

        $categories = array_filter($categories);
        if (!empty($categories)) {
            $query = $query->whereIn('reusable_products.category_id', $categories);
        }

        $end_product = array_filter($end_product);
        if (!empty($end_product)) {
            $query = $query->whereIn('product_id', $end_product);
        }

        $reuse_type = array_filter($reuse_type);
        if (!empty($reuse_type)) {
            $query = $query->whereIn('type', $reuse_type);
        }


//        $days = array_filter($days);
        if (!empty($days)) {
            $dayCount =sizeof($days)-1;
            $query_day ="";
            foreach ($days as $key => $day){
                if ($key!= $dayCount) {
                    $query_day .=" WEEKDAY(reusable_products.created_at) = $day OR";
                }else{
                    $query_day .=" WEEKDAY(reusable_products.created_at) = $day ";
                }

            }
            $query->whereRaw("($query_day)");
        }


        $query
            ->where('reusable_products.created_at', '>=', Carbon::parse($start)->format('Y-m-d 00:00:00'))
            ->where('reusable_products.created_at', '<=', Carbon::parse($end)->format('Y-m-d 23:59:59'));
//         DB::enableQueryLog();
        $records = $query->get();

//        dd(DB::getQueryLog());
        return json_encode($records->toArray());
    }

    public function productPieChart(Request $request)
    {
        $date_range = $request->date_range;
        $agents = !empty($request->agents) ? $request->agents : [];
        $categories = !empty($request->categories) ? $request->categories : [];
        $end_product = !empty($request->end_product) ? $request->end_product : [];
        $reuse_type = !empty($request->reuse_type) ? $request->reuse_type : [];
        $days = !empty($request->days) ? $request->days : [];
        $type = $request->type;


        $query = ReusableProducts::query();

        $dates = explode(' - ', $date_range);
        $start = $dates[0];
        $end = $dates[1];

        if ($type == 'qty-wise') {
            $qty_query = "sum(qty) as qty";
        } elseif ($type == 'value-wise') {
            $qty_query = "sum(qty * end_product.distributor_price) as qty";
        }


        $query = $query->select(
            'end_product.name',
            DB::raw("$qty_query")
        )
            ->leftJoin('end_product', 'end_product.id', 'reusable_products.product_id')
            ->where('reusable_products.created_at', '>=', Carbon::parse($start)->format('Y-m-d 00:00:00'))
            ->where('reusable_products.created_at', '<=', Carbon::parse($end)->format('Y-m-d 23:59:59'))
            ->where('qty', '>', 0)
            ->where('type_id', '>', 6)
            ->groupBy('product_id')
            ->orderBy('product_id', 'asc');

        $agents = array_filter($agents);
        if (!empty($agents)) {
            $query = $query->whereIn('agent_id', $agents);
        }

        $categories = array_filter($categories);
        if (!empty($categories)) {
            $query = $query->whereIn('reusable_products.category_id', $categories);
        }

        $end_product = array_filter($end_product);
        if (!empty($end_product)) {
            $query = $query->whereIn('product_id', $end_product);
        }

        $reuse_type = array_filter($reuse_type);
        if (!empty($reuse_type)) {
            $query = $query->whereIn('type', $reuse_type);
        }

        if (!empty($days)) {
            $dayCount =sizeof($days)-1;
            $query_day ="";
            foreach ($days as $key => $day){
                if ($key!= $dayCount) {
                    $query_day .=" WEEKDAY(reusable_products.created_at) = $day OR";
                }else{
                    $query_day .=" WEEKDAY(reusable_products.created_at) = $day ";
                }

            }
            $query->whereRaw("($query_day)");
        }

        $records = $query->get()->toArray();

        return json_encode($records);
    }

    public function agentDonutChart(Request $request)
    {
        $date_range = $request->date_range;
        $agents = !empty($request->agents) ? $request->agents : [];
        $categories = !empty($request->categories) ? $request->categories : [];
        $end_product = !empty($request->end_product) ? $request->end_product : [];
        $reuse_type = !empty($request->reuse_type) ? $request->reuse_type : [];
        $days = !empty($request->days) ? $request->days : [];
        $type = $request->type;

        if ($type == 'qty-wise') {
            $qty_query = "sum(qty) as qty";
        } elseif ($type == 'value-wise') {
            $qty_query = "sum(qty * end_product.distributor_price) as qty";
        }
        $query = ReusableProducts::query();

        $dates = explode(' - ', $date_range);
        $start = $dates[0];
        $end = $dates[1];

        $query = $query->select(
            'users.name_with_initials',
            DB::raw($qty_query)
        )
            ->leftJoin('end_product', 'end_product.id', '=', 'reusable_products.product_id')
            ->leftJoin('users', 'users.id', 'reusable_products.agent_id')
            ->where('reusable_products.created_at', '>=', Carbon::parse($start)->format('Y-m-d 00:00:00'))
            ->where('reusable_products.created_at', '<=', Carbon::parse($end)->format('Y-m-d 23:59:59'))
            ->where('qty', '>', 0)
            ->where('type_id', '>', 6)
            ->groupBy('agent_id')
            ->orderBy('agent_id', 'asc');

        $agents = array_filter($agents);
        if (!empty($agents)) {
            $query = $query->whereIn('agent_id', $agents);
        }

        $categories = array_filter($categories);
        if (!empty($categories)) {
            $query = $query->whereIn('reusable_products.category_id', $categories);
        }

        $end_product = array_filter($end_product);
        if (!empty($end_product)) {
            $query = $query->whereIn('product_id', $end_product);
        }

        $reuse_type = array_filter($reuse_type);
        if (!empty($reuse_type)) {
            $query = $query->whereIn('reusable_products.type', $reuse_type);
        }

        if (!empty($days)) {
            $dayCount =sizeof($days)-1;
            $query_day ="";
            foreach ($days as $key => $day){
                if ($key!= $dayCount) {
                    $query_day .=" WEEKDAY(reusable_products.created_at) = $day OR";
                }else{
                    $query_day .=" WEEKDAY(reusable_products.created_at) = $day ";
                }

            }
            $query->whereRaw("($query_day)");
        }
        $records = $query->get()->toArray();

        return json_encode($records);
    }

    public function categoryDonutChart(Request $request)
    {
        $date_range = $request->date_range;
        $agents = !empty($request->agents) ? $request->agents : [];
        $categories = !empty($request->categories) ? $request->categories : [];
        $end_product = !empty($request->end_product) ? $request->end_product : [];
        $reuse_type = !empty($request->reuse_type) ? $request->reuse_type : [];
        $days = !empty($request->days) ? $request->days : [];
        $type = $request->type;

        $query = ReusableProducts::query();

        $dates = explode(' - ', $date_range);
        $start = $dates[0];
        $end = $dates[1];

        if ($type == 'qty-wise') {
            $qty_query = "sum(qty) as qty";
        } elseif ($type == 'value-wise') {
            $qty_query = "sum(qty * end_product.distributor_price) as qty";
        }

        $query = $query->select(
            'categories.name',
            DB::raw("$qty_query")
        )
            ->leftJoin('end_product', 'end_product.id', '=', 'reusable_products.product_id')
            ->leftJoin('categories', 'categories.id', 'reusable_products.category_id')
            ->where('reusable_products.created_at', '>=', Carbon::parse($start)->format('Y-m-d 00:00:00'))
            ->where('reusable_products.created_at', '<=', Carbon::parse($end)->format('Y-m-d 23:59:59'))
            ->where('qty', '>', 0)
            ->where('type_id', '>', 6)
            ->groupBy('reusable_products.category_id')
            ->orderBy('agent_id', 'asc');

        $agents = array_filter($agents);
        if (!empty($agents)) {
            $query = $query->whereIn('agent_id', $agents);
        }

        $categories = array_filter($categories);
        if (!empty($categories)) {
            $query = $query->whereIn('reusable_products.category_id', $categories);
        }

        $end_product = array_filter($end_product);
        if (!empty($end_product)) {
            $query = $query->whereIn('product_id', $end_product);
        }

        $reuse_type = array_filter($reuse_type);
        if (!empty($reuse_type)) {
            $query = $query->whereIn('type', $reuse_type);
        }

        if (!empty($days)) {
            $dayCount =sizeof($days)-1;
            $query_day ="";
            foreach ($days as $key => $day){
                if ($key!= $dayCount) {
                    $query_day .=" WEEKDAY(reusable_products.created_at) = $day OR";
                }else{
                    $query_day .=" WEEKDAY(reusable_products.created_at) = $day ";
                }

            }
            $query->whereRaw("($query_day)");
        }
        $records = $query->get()->toArray();

        return json_encode($records);
    }

    public function reusableDonutChart(Request $request)
    {
        $date_range = $request->date_range;
        $agents = !empty($request->agents) ? $request->agents : [];
        $categories = !empty($request->categories) ? $request->categories : [];
        $end_product = !empty($request->end_product) ? $request->end_product : [];
        $reuse_type = !empty($request->reuse_type) ? $request->reuse_type : [];
        $days = !empty($request->days) ? $request->days : [];
        $type = $request->type;

        $query = ReusableProducts::query();

        $dates = explode(' - ', $date_range);
        $start = $dates[0];
        $end = $dates[1];

        if ($type == 'qty-wise') {
            $qty_query = "sum(qty) as qty";
        } elseif ($type == 'value-wise') {
            $qty_query = "sum(qty * end_product.distributor_price) as qty";
        }

        $query = $query->select(
            'type as name',
            DB::raw($qty_query)
        )
            ->leftJoin('end_product', 'end_product.id', '=', 'reusable_products.product_id')
            ->where('reusable_products.created_at', '>=', Carbon::parse($start)->format('Y-m-d 00:00:00'))
            ->where('reusable_products.created_at', '<=', Carbon::parse($end)->format('Y-m-d 23:59:59'))
            ->where('qty', '>', 0)
            ->where('type_id', '>', 6)
            ->groupBy('type')
            ->orderBy('type', 'asc');

        $agents = array_filter($agents);
        if (!empty($agents)) {
            $query = $query->whereIn('agent_id', $agents);
        }

        $categories = array_filter($categories);
        if (!empty($categories)) {
            $query = $query->whereIn('reusable_products.category_id', $categories);
        }

        $end_product = array_filter($end_product);
        if (!empty($end_product)) {
            $query = $query->whereIn('product_id', $end_product);
        }

        $reuse_type = array_filter($reuse_type);
        if (!empty($reuse_type)) {
            $query = $query->whereIn('type', $reuse_type);
        }

        if (!empty($days)) {
            $dayCount =sizeof($days)-1;
            $query_day ="";
            foreach ($days as $key => $day){
                if ($key!= $dayCount) {
                    $query_day .=" WEEKDAY(reusable_products.created_at) = $day OR";
                }else{
                    $query_day .=" WEEKDAY(reusable_products.created_at) = $day ";
                }

            }
            $query->whereRaw("($query_day)");
        }

        $records = $query->get()->toArray();

        return json_encode($records);
    }


}
