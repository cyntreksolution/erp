<?php

namespace App\Http\Controllers;

use App\EndProductBurdenGrade;
use App\Traits\EndProductLog;
use Carbon\Carbon;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use EmployeeManager\Models\Employee;
use EmployeeManager\Models\Group;
use EmployeeManager\Models\GroupEmployee;
use EndProductBurdenManager\Models\EndProductBurden;
use EndProductManager\Models\EndProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use StockManager\Classes\StockTransaction;

class BulkBurdenBakeController extends Controller
{
    use EndProductLog;
    function __construct()
    {
        $this->middleware('permission:bulk_burden_bake-index', ['only' => ['index','tableData']]);
        $this->middleware('permission:BulkBurdenBurn-list', ['only' => ['list']]);
        $this->middleware('permission:ProductBurdenGrade-list', ['only' => ['list']]);
        $this->middleware('permission:bulk_burden_bake-createNow', ['only' => ['createNow']]);
        $this->middleware('permission:bulk_burden_bake-gradeIndex', ['only' => ['gradeIndex','GradeTableData']]);

    }
    public function index()
    {
        /*$employees =Employee::whereHas('attendance', function ($q) {
            $q->where('date','like',Carbon::today()->format('Y-m-d').'%');
        })->pluck('full_name', 'id');*/
        $teams = Group::whereDate('created_at', Carbon::today())->get();
        return view('bulk_burden_bake', compact('teams'));
    }

    public function tableData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $recordsCount = 0;

        $date = $request->date_range;
        $columns = ['end_product_id', 'end_product_id', 'qty'];
        $order_column = $columns[$order_by[0]['column']];

        $records = EndProductBurden::tableData($order_column, $order_by_str, $start, $length);

        $records = $records->whereStatus(3)->get();
//
//            $records = $records->get();
        $recordsCount = (!empty($records)) ? sizeof($records) : 0;

        $data[][] = array();
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;


        foreach ($records as $key => $record) {
            $data[$i] = array(
                "<input type='checkbox' name='id[]' value='$record->id'/>",
                $record->endProduct->name,
                $record->serial,
                $record->burden_size,
                $record->expected_end_product_qty,
            );
            $i++;
        }


        if ($recordsCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($recordsCount),
            "recordsFiltered" => intval($recordsCount),
            "data" => $data
        ];

        return json_encode($json_data);
    }

    public function exportFilter(Request $request)
    {
        $campaign = $request->campaign;
        $records = Record::CampaignUtilizationReportsData($campaign);
        return Excel::download(new CampaignUtilizationReportExport($records), $this->reportFileName('campaign_utilization', '.xlsx', '', 'report'));
    }

    public function createNow(Request $request)
    {
        $burdens = $request->id;
        $employee = $request->employee;


        $burdens = EndProductBurden::whereIn('id', $burdens)->get();

        foreach ($burdens as $burden) {
            //$burden->employee_group = $request->employee_group;
            $burden->baker = $employee;
            $burden->bake_start_time = Carbon::now();
            $burden->status = 4;
            $burden->save();

            $category = $burden->endProduct->category->loading_type;
            $condition = ($category == 'to_burden') ? 1 : 0;

            if ($condition) {
                $burden_qty = $burden->expected_end_product_qty;
                $bg = new EndProductBurdenGrade();
                $bg->burden_id = $burden->id;
                $bg->grade_id = 1;
                $bg->selling_price = $burden->endProduct->selling_price;
                $bg->total_value = $burden->endProduct->selling_price * $burden_qty;
                $bg->burden_qty = $burden_qty;
                $bg->created_by = Auth::user()->id;
                $bg->save();
                StockTransaction::endProductStockTransaction(1, $burden->end_product_id, 0, 0, 1, $burden_qty, $burden->id, 1);
                StockTransaction::updateEndProductAvailableQty(1, $burden->end_product_id, $burden_qty);
                $end_product = EndProduct::find($burden->end_product_id);
                $this->recordEndProductOutstanding($end_product,1, $burden_qty, 'End Product Burden Grade', $bg->id, 'EndProductManager\\Models\\EndProduct');

                $burden->status = 5;
                $burden->save();
            }

        }


        return redirect(route('bulk_burden_bake.index'));
    }


    public function gradeIndex()
    {
        return view('burden_grade');
    }

    public function GradeTableData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $recordsCount = 0;

        $date = $request->date_range;
        $columns = ['end_product_id', 'end_product_id', 'qty'];
        $order_column = $columns[$order_by[0]['column']];

        $records = EndProductBurden::tableData($order_column, $order_by_str, $start, $length);

        $records = $records->whereStatus(4)->get();
//
//            $records = $records->get();
        $recordsCount = (!empty($records)) ? sizeof($records) : 0;

        $data[][] = array();
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;

        $btnAction = null;

        foreach ($records as $key => $record) {
            $btnAction = "<a href='" . route('eburden.grade', $record->id) . "' > <i class='fa fa-cloud-upload'></i> </a>";
            $data[$i] = array(
                $record->id,
                $record->endProduct->name,
                $record->serial,
                $record->burden_size,
                $record->expected_end_product_qty,
                $btnAction
            );
            $i++;
        }


        if ($recordsCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($recordsCount),
            "recordsFiltered" => intval($recordsCount),
            "data" => $data
        ];

        return json_encode($json_data);
    }

}
