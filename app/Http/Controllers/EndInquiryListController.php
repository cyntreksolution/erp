<?php

namespace App\Http\Controllers;

use App\EndProductBurdenGrade;
use App\EndProductInquiry;
use App\LoadingData;
use App\LoadingDataVariant;
use App\LoadingHeader;
use App\ReusableStock;
use App\SalesReturnHeader;
use BurdenManager\Models\ReusableProducts;
use Carbon\Carbon;
use App\RawInquiry;
use EndProductBurdenManager\Models\EndProductBurden;
use EndProductBurdenManager\Models\EndProductBurdenHeader;
use EndProductManager\Models\EndProduct;
use Illuminate\Http\Request;
use RawMaterialManager\Models\RawMaterial;
use function Symfony\Component\Console\Tests\Command\createClosure;

class EndInquiryListController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:end_inquiry_list-index', ['only' => ['show']]);
    }
    public function show($id, $date)
    {
        $datefiil = $date;
//        $end_products = EndProductInquiry::where('end_product_id',$id)->get();
        return view('end_inquiry_list', compact('id', 'datefiil'));
    }

    public function tableData(Request $request, $id, $datefiil)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'created_at'];
        $order_column = $columns[$order_by[0]['column']];

        $yesterday = Carbon::parse($datefiil)->subDay(1)->format('Y-m-d');
        $tommorow = Carbon::parse($datefiil)->addDays(1)->format('Y-m-d');
        $endproducts = EndProductInquiry::where('end_product_id', $id)->whereBetween('created_at', [$datefiil . " 00:00:00", $datefiil . " 23:59:59"])->tableData($order_column, $order_by_str, $start, $length);
//        $yesterdays = EndProductInquiry::where('end_product_id', $id)->whereBetween('created_at', [$yesterday . " 00:00:00", $yesterday . " 23:59:59"])->tableData($order_column, $order_by_str, $start, $length);

        //        $endproducts = EndProductInquiry::where('end_product_id',$id)->whereBetween('created_at',[$tommorow." 00:00:00",$tommorow." 23:59:59"])->tableData($order_column, $order_by_str, $start, $length);
        // $salesOrdersCount = SalesOrder::whereStatus(1)->orderType()->whereConfirmed(1)->count();

        $endproductsCount = 0;
        if (is_null($search) || empty($search)) {
            $endproducts = $endproducts->get();
            $endproductsCount = EndProductInquiry::where('end_product_id', $id)->whereBetween('created_at', [$yesterday, $tommorow])->count();
        } else {
            $endproducts = $endproducts->searchData($search)->get();
            $endproductsCount = $endproducts->count();
        }

        $data[][] = array();
        $i = 0;


        $openningBalance = EndProductInquiry::where('end_product_id', $id)->whereBetween('created_at', [$datefiil . " 00:00:00", $datefiil . " 23:59:59"])->tableData($order_column, $order_by_str, $start, $length)->orderBy('id')->first();

        if (!empty($openningBalance)) {
            $openningBalance = number_format($openningBalance['start_balance'], 2);
        } else {
            $openningBalance = 0;
        }

        foreach ($endproducts as $key => $endproduct) {

            $approving_orders = EndProductInquiry::get()->first();
            $referenceBtn = '';
            $referenceLinkBtn = '';
            $description = 'x';
            switch ($endproduct->reference_type) {

                case 'App\EndProductBurdenGrade':
                    $referenceBtn = 'End Product Burden Grade';
                    $grade = EndProductBurdenGrade::find($endproduct->reference);
                    $id = EndProductBurden::find($grade->burden_id);
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id['serial'] . '</a>';
                    $description = $id->endProduct->name . " Burden";

                    break;

                case 'App\SalesReturnHeader':
                    $referenceBtn = 'Sales Return Header';
                    $id = SalesReturnHeader::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id['debit_note_number'] . '</a>';
                    $description = $endproduct->description;
                    break;

                case 'App\LoadingData':
                    $referenceBtn = 'Loading Data';
                    $id = LoadingData::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id->header->invoice_number . '</a>';
                    $description = $id->header->salesOrder->salesRep->name_with_initials;
                    break;

                case 'App\LoadingDataVariant':
                    $referenceBtn = 'Loading Data Variant';
                    $id = LoadingDataVariant::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id->header->invoice_number . '</a>';
                    $description = $id->header->salesOrder->salesRep->name_with_initials;
                    break;

                case 'App\LoadingHeader':
                    $referenceBtn = 'Loading Header';
                    $id = LoadingHeader::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id->invoice_number . '</a>';
                    $description = $id->salesOrder->salesRep->name_with_initials;
                    break;
                case 'BurdenManager\Models\ReusableProducts':
                    $referenceBtn = 'Reusable Products';
                    $id = ReusableProducts::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id->id . '</a>';

                    //  $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id['debitNoteHeader']['debit_note_number'] . '</a>';
                  //  $description = $id->salesOrder['debitNoteHeader']['name_with_initials'];
                    break;

                case 'EndProductManager\Models\EndProduct':
                    $referenceBtn = 'End Product';
                    $id = EndProduct::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id . '</a>';
                    $description = $id;
                    break;
                case 'App\ReusableStock':
                    $referenceBtn = 'Reusable Stock';
                    $id = ReusableStock::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id->reusableProduct['debitNoteHeader']['debit_note_number'] . '</a>';
                    $description = $id;
                    break;

            }
            $approving_filter = !empty($approving_orders) ? 1 : 0;

            $data[$i] = array(
                $endproduct->id,
                Carbon::parse($endproduct->created_at)->format('Y-m-d H:i:s'),
                $referenceLinkBtn,
                $description,
                number_format($endproduct->qty, 2),
                $endproduct->end_balance,

                null,
            );
            $i++;
        }

        if ($endproductsCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($endproductsCount),
            "recordsFiltered" => intval($endproductsCount),
            "data" => $data,
            "openning_balance" => $openningBalance,
        ];

        return json_encode($json_data);
    }

    public function tableDatayes(Request $request, $id, $datefiil)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'created_at'];
        $order_column = $columns[$order_by[0]['column']];
        $yesterday = Carbon::parse($datefiil)->subDay(1)->format('Y-m-d');
        $tommorow = Carbon::parse($datefiil)->addDays(1)->format('Y-m-d');
//        $today = EndProductInquiry::where('end_product_id',$id)->whereBetween('created_at',[$datefiil." 00:00:00",$datefiil." 23:59:59"])->tableData($order_column, $order_by_str, $start, $length);
        $yesterdays = EndProductInquiry::where('end_product_id', $id)->whereBetween('created_at', [$yesterday . " 00:00:00", $yesterday . " 23:59:59"])->tableData($order_column, $order_by_str, $start, $length);
//        $endproducts = EndProductInquiry::where('end_product_id',$id)->whereBetween('created_at',[$tommorow." 00:00:00",$tommorow." 23:59:59"])->tableData($order_column, $order_by_str, $start, $length);
        // $salesOrdersCount = SalesOrder::whereStatus(1)->orderType()->whereConfirmed(1)->count();

        $endproductsCount = 0;
        if (is_null($search) || empty($search)) {
            $endproducts = $yesterdays->get();
            $endproductsCount = EndProductInquiry::where('end_product_id', $id)->whereBetween('created_at', [$yesterday, $tommorow])->count();
        } else {
            $endproducts = $yesterdays->searchData($search)->get();
            $endproductsCount = $yesterdays->count();
        }

        $data[][] = array();
        $i = 0;


        foreach ($endproducts as $key => $endproduct) {

            $approving_orders = EndProductInquiry::get()->first();
            $referenceBtn = '';
            $referenceLinkBtn = '';
            $description = '';
            switch ($endproduct->reference_type) {
                case 'APP\EndProductBurdenGrade':
                    $referenceBtn = 'End Product Burden Grade';
                    $id = EndProductBurdenHeader::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id['serial'] . '</a>';
                    $description = $id->agent->name_with_initials;

                    break;

                case 'App\SalesReturnHeader':
                    $referenceBtn = 'Sales Return Header';
                    $id = SalesReturnHeader::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id['debit_note_number'] . '</a>';
                    $description = $id->agent->name_with_initials;
                    break;

                case 'App\LoadingData':
                    $referenceBtn = 'Loading Data';
                    $id = LoadingData::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id->header->invoice_number . '</a>';
                    $description = $id->header->salesOrder->salesRep->name_with_initials;
                    break;

                case 'App\LoadingHeader':
                    $referenceBtn = 'Loading Header';
                    $id = LoadingHeader::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id->invoice_number . '</a>';
                    $description = $id->salesOrder->salesRep->name_with_initials;
                    break;
                case 'BurdenManager\Models\ReusableProducts':
                    $referenceBtn = 'Reusable Products';
                    $id = ReusableProducts::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id->id . '</a>';

                    // $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id['debitNoteHeader']['debit_note_number'] . '</a>';
                   // $description = $id->salesOrder['debitNoteHeader']['name_with_initials'];
                    break;

                case 'App\LoadingDataVariant':
                    $referenceBtn = 'Loading Data Variant';
                    $id = LoadingDataVariant::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id->header->invoice_number . '</a>';
                    $description = $id->header->salesOrder->salesRep->name_with_initials;
                    break;
                case 'EndProductManager\Models\EndProduct':
                    $referenceBtn = 'End Product';
                    $id = EndProduct::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id . '</a>';
                    $description = $id;
                    break;
                case 'App\ReusableStock':
                    $referenceBtn = 'Reusable Stock';
                    $id = ReusableStock::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id->reusableProduct['debitNoteHeader']['debit_note_number'] . '</a>';
                    $description = $id;
                    break;
            }
            $approving_filter = !empty($approving_orders) ? 1 : 0;

            $data[$i] = array(
                $endproduct->id,
                Carbon::parse($endproduct->created_at)->format('Y-m-d H:i:s'),
                $referenceLinkBtn,
                $description,
                number_format($endproduct->qty, 2),
                $endproduct->end_balance,

//                $endproduct->grade->name,
//                number_format($endproduct->start_balance,3),
//                number_format($endproduct->end_balance,3),
//                number_format($endproduct->price,2),
//                number_format($endproduct->amount,2),
//                $referenceBtn,

                null,
            );
            $i++;
        }

        if ($endproductsCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($endproductsCount),
            "recordsFiltered" => intval($endproductsCount),
            "data" => $data,

        ];

        return json_encode($json_data);
    }

    public function tableDatatomorrow(Request $request, $id, $datefiil)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'created_at'];
        $order_column = $columns[$order_by[0]['column']];
        $yesterday = Carbon::parse($datefiil)->subDay(1)->format('Y-m-d');
        $tommorow = Carbon::parse($datefiil)->addDays(1)->format('Y-m-d');
        $endproducts = EndProductInquiry::where('end_product_id', $id)->whereBetween('created_at', [$tommorow . " 00:00:00", $tommorow . " 23:59:59"])->tableData($order_column, $order_by_str, $start, $length)->orderBy('created_at', 'ASC');
        // $salesOrdersCount = SalesOrder::whereStatus(1)->orderType()->whereConfirmed(1)->count();

        $endproductsCount = 0;
        if (is_null($search) || empty($search)) {
            $endproducts = $endproducts->get();
            $endproductsCount = EndProductInquiry::where('end_product_id', $id)->whereBetween('created_at', [$yesterday, $tommorow])->count();
        } else {
            $endproducts = $endproducts->searchData($search)->get();
            $endproductsCount = $endproducts->count();
        }

        $data[][] = array();
        $i = 0;


        foreach ($endproducts as $key => $endproduct) {

            $approving_orders = EndProductInquiry::get()->first();
            $referenceBtn = '';
            $referenceLinkBtn = '';
            $description = '';
            switch ($endproduct->reference_type) {
                case 'APP\EndProductBurdenGrade':
                    $referenceBtn = 'End Product Burden Grade';
                    $id = EndProductBurdenHeader::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id['serial'] . '</a>';
                    $description = $id->agent->name_with_initials;
                    break;

                case 'App\SalesReturnHeader':
                    $referenceBtn = 'Sales Return Header';
                    $id = SalesReturnHeader::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id['debit_note_number'] . '</a>';
                    $description = $id->agent->name_with_initials;
                    break;

                case 'App\LoadingData':
                    $referenceBtn = 'Loading Data';
                    $id = LoadingData::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id->header->invoice_number . '</a>';
                    $description = $id->header->salesOrder->salesRep->name_with_initials;
                    break;

                case 'App\LoadingHeader':
                    $referenceBtn = 'Loading Header';
                    $id = LoadingHeader::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id->invoice_number . '</a>';
                    $description = $id->salesOrder->salesRep->name_with_initials;
                    break;
                case 'BurdenManager\Models\ReusableProducts':
                    $referenceBtn = 'Reusable Products';
                    $id = ReusableProducts::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id->id . '</a>';

                    // $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id['debitNoteHeader']['debit_note_number'] . '</a>';
                   // $description = $id->salesOrder['debitNoteHeader']['name_with_initials'];
                    break;
                case 'App\LoadingDataVariant':
                    $referenceBtn = 'Loading Data Variant';
                    $id = LoadingDataVariant::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id->header->invoice_number . '</a>';
                    $description = $id->header->salesOrder->salesRep->name_with_initials;
                    break;
                case 'EndProductManager\Models\EndProduct':
                    $referenceBtn = 'End Product';
                    $id = EndProduct::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id . '</a>';
                    $description = $id;
                    break;

                case 'App\ReusableStock':
                    $referenceBtn = 'Reusable Stock';
                    $id = ReusableStock::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id->reusableProduct['debitNoteHeader']['debit_note_number'] . '</a>';
                    $description = $id;
                    break;

            }
            $approving_filter = !empty($approving_orders) ? 1 : 0;

            $data[$i] = array(
                $endproduct->id,
                Carbon::parse($endproduct->created_at)->format('Y-m-d H:i:s'),
                $referenceLinkBtn,
                $description,
                number_format($endproduct->qty, 2),
                $endproduct->end_balance,

//                $endproduct->grade->name,
//                number_format($endproduct->start_balance,3),
//                number_format($endproduct->end_balance,3),
//                number_format($endproduct->price,2),
//                number_format($endproduct->amount,2),
//                $referenceBtn,

                null,
            );
            $i++;
        }

        if ($endproductsCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($endproductsCount),
            "recordsFiltered" => intval($endproductsCount),
            "data" => $data,

        ];

        return json_encode($json_data);
    }


    public function tableDataX(Request $request, $end_product, $date)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'created_at'];
        $order_column = $columns[$order_by[0]['column']];


        $query = EndProductInquiry::where('end_product_id', $end_product)->where('created_at', 'like', $date . '%')->where('qty', '<>', 0);
        $openningBalance = !empty($query->first())?$query->first()->start_balance:0;

        if (is_null($search) || empty($search)) {
            $query_count = $query->count();
            $query = $query->tableData($order_column, $order_by_str, $start, $length);
        } else {
            $query = $query->searchData($search);
            $query_count = $query->count();
            $query = $query->tableData($order_column, $order_by_str, $start, $length);
        }

        $data[][] = array();
        $i = 0;

        $endproducts = $query->get();
        $referenceLinkBtn = null;
        $description = null;
        $referenceBtn = null;


        foreach ($endproducts as $key => $endproduct) {


            switch ($endproduct->reference_type) {
                case 'App\EndProductBurdenGrade':
                    $rvs_btn = null;
                    $referenceBtn = '<button class="btn btn-success btn-sm">Add To Store</button>';


                    $cnt = EndProductInquiry::where('reference','=', $endproduct->reference)->where('desc_id',[10,11])->count();


                     if($cnt > 1) {

                         if ($role = \Cartalyst\Sentinel\Laravel\Facades\Sentinel::check()->roles[0]->slug == 'owner') {
                             $rvs_btn = "<a title='Reverse' href='" . route('add-store.revers', $endproduct->id) . "' class='ml-3 btn btn-danger mr-2' ><i class='fa fa-angle-double-left'></i> </a>";
                         }else{
                             $rvs_btn = "<a title='Reverse' href='' class='ml-3 btn btn-danger mr-2' ><i class='fa fa-angle-double-left'></i> </a>";

                         }
                     }
                    $gr = EndProductBurdenGrade::find($endproduct->reference);
                    $id = EndProductBurden::where('id', $gr->burden_id)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id['serial'] . '</a>';
                    $description = $endproduct->description;
                    break;


                case 'App\SalesReturnHeader':
                    $rvs_btn = null;
                    $id = SalesReturnHeader::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id['debit_note_number'] . '</a>';
                    $description = $id->agent->name_with_initials;
                    if ($id->is_system_generated) {
                        $referenceBtn = '<button class="btn btn-danger btn-sm">System generated Return</button>';
                    } else {
                        $description = $endproduct->description;
                        $referenceBtn = "<button class='btn btn-warning btn-sm'>$description</button>";
                    }
                    break;

                case 'App\LoadingData':
                    $rvs_btn = null;
                    $referenceBtn = '<button class="btn btn-info btn-sm">Sold</button>';
                    $id = LoadingData::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id->header->invoice_number . '</a>';
                    $description = $id->header->salesOrder->salesRep->name_with_initials;
                    break;

                case 'App\LoadingHeader':
                    $rvs_btn = null;
                    $referenceBtn = 'Loading Header';
                    $id = LoadingHeader::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id->invoice_number . '</a>';
                    $description = $id->salesOrder->salesRep->name_with_initials;
                    break;
                case 'BurdenManager\Models\ReusableProducts':
                    $rvs_btn = null;
                    $description = $endproduct->description;
                    $referenceBtn = "<button class='btn btn-warning btn-sm'>$description</button>";
                    $id = ReusableProducts::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id->id . '</a>';

                    // $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id['debitNoteHeader']['debit_note_number'] . '</a>';
                   // $description = $id->salesOrder['debitNoteHeader']['name_with_initials'];
                    break;

                case 'App\LoadingDataVariant':
                    $rvs_btn = null;
                    $referenceBtn = '<button class="btn btn-info btn-sm">Sold</button>';
                    $id = LoadingDataVariant::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id->header->invoice_number . '</a>';
                    $description = $id->header->salesOrder->salesRep->name_with_initials;
                    break;
                case 'EndProductManager\Models\EndProduct':
                    $rvs_btn = null;
                    $referenceBtn = 'End Product';
                    $id = EndProduct::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id . '</a>';
                    $description = $id;
                    break;
                case 'App\ReusableStock':
                    $rvs_btn = null;
                    $id = ReusableStock::where('id', $endproduct->reference)->first();
                    $referenceLinkBtn = '<a href="" class="btn btn-outline-warning btn-sm mr-1">' . $id->reusableProduct['debitNoteHeader']['debit_note_number'] . '</a>';
                    $description = $endproduct->description;
                    $referenceBtn = "<button class='btn btn-warning btn-sm'>$description</button>";
                    break;
                default:
                    dd($endproduct->reference_type);
            }


            $data[$i] = array(
                $endproduct->id,
                Carbon::parse($endproduct->created_at)->format('Y-m-d H:i:s'),
                $referenceLinkBtn.$rvs_btn,
                $description,
                $referenceBtn,
                number_format($endproduct->qty, 2),
                $endproduct->end_balance,
                null,
            );
            $i++;
        }

        if ($query_count == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($query_count),
            "recordsFiltered" => intval($query_count),
            "data" => $data,
            "openning_balance" => number_format($openningBalance,3),
        ];

        return json_encode($json_data);

    }
}
