<?php

namespace App\Http\Controllers;

use App\AgentInquiry;
use App\AgentPayment;
use App\LoadingHeader;
use App\SalesReturnHeader;
use Carbon\Carbon;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use EmployeeManager\Models\Employee;
use EndProductBurdenManager\Models\EndProductBurden;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\View;
use PassBookManager\Models\Cheque;
use SalesRepManager\Models\SalesRep;
use StockManager\Classes\StockTransaction;
use function GuzzleHttp\Promise\all;
use SalesOrderManager\Models\SalesOrder;
use LoadingManager\Models\LoadingStatusChanges;

class AgentInquireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    function __construct()
    {
        $this->middleware('permission:agent_inquire-index', ['only' => ['index']]);
        $this->middleware('permission:AgentInquire-list', ['only' => ['list']]);
        $this->middleware('permission:AgentInquireReport-list', ['only' => ['rlist']]);
        $this->middleware('permission:agents_inquire-index2', ['only' => ['index2']]);
//        $this->middleware('permission:agent_outstanding-view', ['only' => ['forcePay']]);

    }
    public function index()
    {
        $agents = SalesRep::all();
        return view('agents.agent-inquire-index', compact('agents'));
    }


    public function index2()
    {
        $agents = SalesRep::orderBy('email')->get();
        return view('agents.agent-inquire-index2', compact('agents'));
    }


    public function renderData(Request  $request){
        $agent = SalesRep::find($request->agent);
        $selectedDate = $request->date;

        $agent_init_value = AgentInquiry::whereAgentId($agent->id)->whereIsInit(1)->first();
        $agent_init_value = (!empty($agent_init_value) && $agent_init_value->count()>0)?$agent_init_value->amount:0;
//        $agent_records = AgentInquiry::whereAgentId($agent->id)->where('created_at','like',$selectedDate.'%')->orderBy('created_at')->get();
        $agent_records = AgentInquiry::filterData($agent->id, $selectedDate)->get()->groupBy(function ($val) {
            return Carbon::parse($val->created_at)->format('Y-m-d');
        });
        return View::make('agents.agent-inquire-data',compact('agent','selectedDate','agent_init_value','agent_records'));
    }

    public function agentInquireDownload(Request $request,$agent,$date){
//        dd($agent);
        $agent = SalesRep::find($request->agent);
        $selectedDate = $request->date;

        $agent_init_value = AgentInquiry::whereAgentId($agent->id)->whereIsInit(1)->first();
        $agent_init_value = (!empty($agent_init_value) && $agent_init_value->count()>0)?$agent_init_value->amount:0;
//        $agent_records = AgentInquiry::whereAgentId($agent->id)->whereBetween('created_at',array($selectedDate))->get();
        $agent_records = AgentInquiry::filterData($agent->id, $selectedDate)->get()->groupBy(function ($val) {
            return Carbon::parse($val->created_at)->format('Y-m-d');
        });

        return view('agents.agent-inquire-download', compact('agent','selectedDate','agent_init_value','agent_records'));

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function agentInquireData($agentId)
    {
        $agents = SalesRep::find($agentId);
        $totalLoadingHeaderAmount = 0;
        $salesOrders = SalesOrder::where('created_by', '=', $agentId)->get();

        foreach ($salesOrders as $key => $salesOrder) {
            $loadHeader = $salesOrder->loadingHeader;
            if (!empty($loadHeader)) {
                $timestamp = strtotime($loadHeader->loading_date);
                $date = date('Y-m-d', $timestamp);
                if ($date < Carbon::today()->toDateString()) {
                    if ($loadHeader->net_amount == Null) {
                        $totalLoadingHeaderAmount = $totalLoadingHeaderAmount + $loadHeader->total;
                    } else {
                        $totalLoadingHeaderAmount = $totalLoadingHeaderAmount + $loadHeader->net_amount;
                    }
                }
            }
        }


        $totalSalesReturnAmount = 0;
        $salesReturns = SalesReturnHeader::where('status', '=', 5)->where('agent_id', '=', $agentId)->get();
        foreach ($salesReturns as $key => $salesReturn) {
            if ($salesReturn->updated_at->toDateString() < Carbon::today()->toDateString()) {
                if ($salesReturn->final_amount == Null) {
                    $totalSalesReturnAmount = $totalSalesReturnAmount + 0;
                } else {
                    $totalSalesReturnAmount = $totalSalesReturnAmount + $salesReturn->final_amount;
                }
            }
        }

        $totalAgentPaymentAmount = 0;
        $agentPayments = AgentPayment::where('status', '=', 'approved')->where('agent_id', '=', $agentId)->get();
        foreach ($agentPayments as $key => $agentPayment) {
            if ($agentPayment->updated_at->toDateString() < Carbon::today()->toDateString()) {
                if ($agentPayment->total_amount == Null) {
                    $totalAgentPaymentAmount = $totalAgentPaymentAmount + 0;
                } else {
                    $totalAgentPaymentAmount = $totalAgentPaymentAmount + $agentPayment->total_amount;
                }
            }
        }
        $inquireAllList = [];
//--------------------------------------------------------------------------------------------------------------------------
        $loadingHeaderDataArray = [];
        $salesOrderDatas = SalesOrder::where('created_by', '=', $agentId)->get();
        foreach ($salesOrderDatas as $key => $salesOrderData) {
            $loadHeaderData = $salesOrderData->loadingHeader;
            if (!empty($loadHeaderData)) {
                $timestamp = strtotime($loadHeaderData->loading_date);
                $date = date('Y-m-d', $timestamp);
                if ($date == Carbon::today()->toDateString()) {
                    if ($loadHeaderData->net_amount == Null) {
                        array_push($loadingHeaderDataArray, ['loading_id' => $loadHeaderData->invoice_number, 'category' => $salesOrderData->template->category->name, 'net_amount' => $loadHeaderData->total]);
                    } else {
                        array_push($loadingHeaderDataArray, ['loading_id' => $loadHeaderData->invoice_number, 'category' => $salesOrderData->template->category->name, 'net_amount' => $loadHeaderData->net_amount]);
                    }
                }
            }
        }
        // $loadHeaderDatas = LoadingStatusChanges::where('agent_id','=',$agentId)->where('status','=',2)->get();
        // foreach ($loadHeaderDatas as $key => $loadHeaderData) {
        //   $timestamp = strtotime($loadHeaderData->status_change_date);
        //   $date = date('Y-m-d', $timestamp);
        //       if($date == Carbon::today()->toDateString()){
        //         $loadingHeader = LoadingHeader::find($loadHeaderData->loading_header_id);
        //           array_push($loadingHeaderDataArray,['loading_id'=>$loadingHeader->invoice_number,'category'=>$loadingHeader->salesOrder->template->category->name,'net_amount'=>$loadHeaderData->total_amount]);
        //       }
        // }

        $salesReturnDataArray = [];
        $salesReturnDatas = SalesReturnHeader::where('status', '=', 5)->where('agent_id', '=', $agentId)->get();
        foreach ($salesReturnDatas as $key => $salesReturnData) {
            if ($salesReturnData->updated_at->toDateString() == Carbon::today()->toDateString()) {
                if ($salesReturnData->final_amount == Null) {
                    array_push($salesReturnDataArray, ['sales_return_id' => $salesReturnData->debit_note_number, 'final_amount' => 0]);
                } else {
                    array_push($salesReturnDataArray, ['sales_return_id' => $salesReturnData->debit_note_number, 'final_amount' => $salesReturnData->final_amount]);
                }
            }
        }

        $agentPaymentDataArray = [];
        $agentPaymentDatas = AgentPayment::where('status', '=', 'approved')->where('agent_id', '=', $agentId)->get();
        foreach ($agentPaymentDatas as $key => $agentPaymentData) {
            if ($agentPaymentData->updated_at->toDateString() == Carbon::today()->toDateString()) {
                if ($agentPaymentData->total_amount == Null) {
                    array_push($agentPaymentDataArray, ['agent_payment_id' => $agentPaymentData->serial, 'total_amount' => 0]);
                } else {
                    array_push($agentPaymentDataArray, ['agent_payment_id' => $agentPaymentData->serial, 'total_amount' => $agentPaymentData->total_amount]);
                }
            }
        }
        $balance = $totalLoadingHeaderAmount - ($totalSalesReturnAmount + $totalAgentPaymentAmount);
        array_push($inquireAllList, ['agent_id' => $agentId, 'agent_name' => $agents->name_with_initials, 'agent_territory' => $agents->territory, 'due_balance' => $balance, 'loading_header' => $loadingHeaderDataArray, 'sales_return' => $salesReturnDataArray, 'agent_payment' => $agentPaymentDataArray]);
        return $inquireAllList;
    }

    public function agentInquirePrint($agentId)
    {
        $invoices = $this->agentInquireData($agentId);

        return view('agents.agent-inquire-print', compact('agentId', 'invoices'));
    }
}
