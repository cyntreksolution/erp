<?php

namespace App\Http\Controllers;

use App\LoadingData;
use App\LoadingDataRaw;
use App\LoadingDataVariant;
use App\LoadingHeader;
use App\SalesReturnData;
use App\SalesReturnHeader;
use Carbon\Carbon;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use SalesOrderManager\Models\SalesOrder;
use App\CratesTransaction;
use UserManager\Models\Users;

class ApproveSalesRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:approve_sales-index', ['only' => ['index']]);
        $this->middleware('permission:ApproveAgentsReceivedOrder-list', ['only' => ['list']]);
        $this->middleware('permission:approve_sales-store', ['only' => ['store']]);
        $this->middleware('permission:approve_sales-show', ['only' => ['show']]);


    }
    public function index()
    {
        $user = Auth::user();
        $loadings = LoadingHeader::whereStatus(3)->get();
        return view('agents.request-list', compact('loadings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::transaction(function () use ($request) {
            $header = LoadingHeader::find($request->id);

            $header->status = 4;
            $header->save();

            $so = SalesOrder::find($header->so_id);
            $so->status = 5;
            $so->save();


            if ($header->is_changed) {
                $items = LoadingData::whereLoadingHeaderId($header->id)->whereIsChanged(1)->get();
                $variants = LoadingDataVariant::whereLoadingHeaderId($header->id)->whereIsChanged(1)->get();

                // $sales_return = SalesReturnHeader::create([
                //     'agent_id' => $header->salesOrder->created_by,
                //     'status' => 2,
                //     'total' => 0,
                //     'time' => Carbon::now(),
                //     'debit_note_number' => 'SR-' . $header->invoice_number . '-' . Carbon::now()->format('ymdHis'),
                //     'is_system_generated' => 1
                // ]);
                $sales_return = new SalesReturnHeader();
                $usr = Users::find($header->salesOrder->created_by);
                $sales_return->agent_id = $header->salesOrder->created_by;
                $sales_return->status = 2;
                $sales_return->total = 0;
                $sales_return->shop_type = $usr->desc1;
                $sales_return->time = $header->salesOrder->template->time;
                //$sales_return->time = Carbon::now();
                $sales_return->debit_note_number = 'SR-' . $header->invoice_number . '-' . Carbon::now()->format('ymdHis');
                $sales_return->is_system_generated = 1;
                $sales_return->save();


                $total = 0;
                if (!empty($items)) {
                    foreach ($items as $item) {
                        $difference = $item->qty - $item->agent_approved_qty;
                        if (!empty($item->commission)) {
                            $unit_commission = $item->commission / $item->qty;
                            $commission = $difference * $unit_commission;
                        } else {
                            $commission = 0;
                        }

                        if (!empty($item->unit_discount)) {
                            $unit_discount_per_one = $item->unit_discount / $item->qty;
                            $unit_discount = $difference * $unit_discount_per_one;
                        } else {
                            $unit_discount = 0;
                        }


                        // SalesReturnData::create([
                        //     'sales_return_header_id' => $sales_return->id,
                        //     'end_product_id' => $item->end_product_id,
                        //     'unit_value' => $item->unit_value,
                        //     'qty' => $difference,
                        //     'commission' => $commission,
                        //     'unit_discount' => $unit_discount,
                        //     'total' => $difference * $item->unit_value,
                        //     'total_discount' => $commission + $unit_discount,
                        //     'row_value' => $difference * $item->unit_value - ($commission + $unit_discount),
                        //     'is_system_generated' => 1,
                        // ]);

                        $sales_return_data = new SalesReturnData();
                        $sales_return_data->sales_return_header_id = $sales_return->id;
                        $sales_return_data->end_product_id = $item->end_product_id;
                        $sales_return_data->unit_value = $item->unit_value;
                        $sales_return_data->qty = $difference;
                        $sales_return_data->commission = $commission;
                        $sales_return_data->unit_discount = $unit_discount;
                        $sales_return_data->total = $difference * $item->unit_value;
                        $sales_return_data->total_discount = $commission + $unit_discount;
                        $sales_return_data->row_value = $difference * $item->unit_value - ($commission + $unit_discount);
                        $sales_return_data->is_system_generated = 1;
                        $sales_return_data->save();

                        $total += $difference * $item->unit_value - ($commission + $unit_discount);
                    }
                }

                if (!empty($variants)) {
                    foreach ($variants as $item) {

                        $difference = $item->weight - $item->agent_approved_qty;
                        if ($difference != 0) {
                            // if ($item->weight > 0){
                            if (!empty($item->commission)) {
                                $unit_commission = $item->commission / $item->weight;
                                $commission = $difference * $unit_commission;
                            } else {
                                $commission = 0;
                            }

                            if (!empty($item->unit_discount)) {
                                $unit_discount_per_one = $item->unit_discount / $item->weight;
                                $unit_discount = $difference * $unit_discount_per_one;
                            } else {
                                $unit_discount = 0;
                            }
                            // }else{
                            //     $commission = 0;
                            //     $unit_discount = 0;
                            // }


                            //
                            // SalesReturnData::create([
                            //     'sales_return_header_id' => $sales_return->id,
                            //     'end_product_id' => $item->variant->product->id,
                            //     'unit_value' => $item->unit_value,
                            //     'qty' => $difference,
                            //     'commission' => $commission,
                            //     'unit_discount' => $unit_discount,
                            //     'total' => $difference * $item->unit_value,
                            //     'total_discount' => $commission + $unit_discount,
                            //     'row_value' => $difference * $item->unit_value - ($commission + $unit_discount),
                            //     'is_system_generated' => 1,
                            // ]);


                            $sales_return_data = new SalesReturnData();
                            $sales_return_data->sales_return_header_id = $sales_return->id;
                            $sales_return_data->end_product_id = $item->variant->product->id;
                            $sales_return_data->unit_value = $item->variant->product->distributor_price;
                            $sales_return_data->qty = $difference;
                            $sales_return_data->commission = $commission;
                            $sales_return_data->unit_discount = $unit_discount;
                            // $sales_return_data->total = $difference * $item->unit_value;
                            $sales_return_data->total = $difference * $item->variant->product->distributor_price;
                            $sales_return_data->total_discount = $commission + $unit_discount;
                            $sales_return_data->row_value = $difference * $item->variant->product->distributor_price - ($commission + $unit_discount);
                            $sales_return_data->is_system_generated = 1;
                            $sales_return_data->save();

                            $total += $difference * $item->variant->product->distributor_price - ($commission + $unit_discount);
                        }

                    }
                }

                $sales_return->total = $total;
                $sales_return->save();
            }


        });
        return redirect(route('order.list'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $order = LoadingHeader::find($id);
        $order = LoadingHeader::where('so_id', '=', $id)->first();
        return view('agents.approved-show', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $order = LoadingHeader::find($id);
        $order = LoadingHeader::where('so_id', '=', $id)->first();
        // $returnCratesJson = CratesTransaction::where('ref_id','=', $id)->where('type','=','return')->first();
        $returnCratesJson = CratesTransaction::where('ref_id', '=', $order->id)->where('type', '=', 'return')->first();
        $returncrates = json_decode($returnCratesJson->crates, true);
        $crates = json_decode($order->crate_sent, true);
        return view('agents.approved-edit', compact('order', 'crates', 'returncrates', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        dd($request->all());
        $order = LoadingHeader::where('id', '=', $request->id)->first();
        $order->is_changed = 1;
        $order->save();

        $getStatus = SalesOrder::find($order->so_id);
        if ($getStatus->status == 5) {
            return redirect(route('order.list'))->with([
                'error' => true,
                'error.title' => 'Error !',
                'error.message' => 'Already Approved'
            ]);
        } else {
            $indexData = 0;
            if (!empty($request->item_id)) {
                foreach ($request->item_id as $items) {
                    $data = LoadingData::whereId($items)->first();
                    if ($data->agent_approved_qty != $request->product_qty[$indexData]) {
                        $data->agent_approved_qty = $request->product_qty[$indexData];
                        $data->admin_approved_qty = $request->product_qty[$indexData];
                        $data->is_changed = 1;
                        $data->save();
                    }
                    $indexData++;
                }
            }
            $indexVariant = 0;
            if (!empty($request->variant_item_id)) {
                foreach ($request->variant_item_id as $variant_item) {
                    $data = LoadingDataVariant::whereId($variant_item)->first();
                    if ($data->agent_approved_qty != $request->product_weight_variant[$indexVariant]) {
                        $data->agent_approved_qty = $request->product_weight_variant[$indexVariant];
                        $data->admin_approved_qty = $request->product_weight_variant[$indexVariant];
                        $data->is_changed = 1;
                        $data->save();
                    }
                    $indexVariant++;
                }
            }


            return redirect(route('order.list'))->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Successfully Updated'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function previewList($orderId)
    {
        // $order = LoadingHeader::find($id);
        $order = LoadingHeader::where('so_id', '=', $orderId)->first();
        return view('agents.approved-preview', compact('order'));
    }
}
