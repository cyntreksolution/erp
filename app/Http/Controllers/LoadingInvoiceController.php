<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoadingInvoiceController extends Controller
{
    public function index(){
        return view('LoadingManager::invoice.index');
    }
}
