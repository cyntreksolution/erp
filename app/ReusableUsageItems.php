<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReusableUsageItems extends Model
{
    protected $table ='reusable_usage_items';
}
