<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use SemiFinishProductManager\Models\SemiFinishProduct;

class SalesMergeSemiDif extends Model
{
    protected $table='sales_merge_semi_diference';


    public function semiFinish(){
        return $this->belongsTo(SemiFinishProduct::class,'semi_finish_product_id','semi_finish_product_id');
    }
}
