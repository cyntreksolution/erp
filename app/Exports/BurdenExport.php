<?php

namespace App\Exports;

use App\Burden;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class BurdenExport implements FromCollection,WithHeadings,WithMapping
{
    protected $records;
    protected $status;

    public function __construct($records)
    {
        $this->records = $records;
        $this->status = ['pending','received','creating','burning','grade','finished'];
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return $this->records;
    }
    public function headings(): array
    {
        return [
            'Serial NO',
            'Semi Finish Product Name',
            'Status',
            'Burden Size',
            'Recipe Name',
            'Date',
        ];
    }

    public function map($row): array{
        return [
            $row->serial,
            $row->semiProdcuts->name,
            $this->status[($row->status)-1],
            $row->burden_size . ' KG',
            $row->recipe->name,
            Carbon::parse($row->created_at)->format('Y-m-d H:m:s'),
        ];
    }
}
