<?php

namespace App\Exports;

use App\Variant;
use EndProductManager\Models\EndProduct;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use SalesOrderManager\Models\SalesOrder;

class SalesRequestExport implements FromView
{
    protected $orders;

    /**
     * SalesRequestExport constructor.
     * @param $orders
     */
    public function __construct($orders)
    {
        $this->orders = $orders;
    }


    public function view(): View
    {
        $orders = $this->orders;
        $order_s = SalesOrder::whereIn('id', explode(',',$orders))->with('salesOrderContent')->get();

        foreach ($order_s as $order_) {
            if ($order_->status != 1) {
                return "Your request " . $order_->id . " has been Merged";
            }
        }




            $table_data = [];
            $table_data_variant = [];

            foreach ($order_s as $order) {
                $order_item = [];
                $order_item['agent_name'] = $order->salesRep->name_with_initials;
                $order_item['name'] = [];
                $order_item['end_product_id'] = [];
                $order_item['qty'] = [];

                $order_item_variant = [];
                $order_item_variant['agent_name'] = $order->salesRep->name_with_initials;
                $order_item_variant['name'] = [];
                $order_item_variant['variant_id'] = [];
                $order_item_variant['qty'] = [];

                foreach ($order->salesOrderContent as $item) {
                    if ($order->id == $item->sales_order_header_id) {
                        array_push($order_item['name'], EndProduct::whereId($item->end_product_end_product_id)->first()->name);
                        array_push($order_item['end_product_id'], $item->end_product_end_product_id);
                        $qty = ($item->extra_qty + $item->qty) - $item->available_qty;
                        array_push($order_item['qty'], $qty);
                    }
                }

                foreach ($order->salesOrderVariants as $item) {
                    if ($order->id == $item->sales_order_header_id) {
                        $variant = Variant::whereId($item->variant_id)->first();
                        array_push($order_item_variant['name'], $variant->product->name . ' ' . $variant->variant);
                        $qty = ($item->extra_qty + $item->qty) - $item->available_qty;
                        array_push($order_item_variant['variant_id'], $item->variant_id);
                        array_push($order_item_variant['qty'], $qty);
                    }
                }

                array_push($table_data, $order_item);
                array_push($table_data_variant, $order_item_variant);
            }




            $name_list = [];
            $name_list['end_product'] = [];
            $name_list ['variant'] = [];
            $table_headers = [];
            array_push($table_headers, 'End Products');

            foreach ($table_data as $key => $agent) {
                array_push($table_headers, $agent['agent_name']);
                foreach ($agent['name'] as $key1 => $name) {
                    if (!in_array($name, $name_list['end_product'])) {
                        array_push($name_list['end_product'], $name);
                    }
                }
            }

            foreach ($table_data_variant as $key => $agent) {
                foreach ($agent['name'] as $key1 => $name) {
                    if (!in_array($name, $name_list['variant'])) {
                        array_push($name_list['variant'], $name);
                    }
                }
            }


            array_push($table_headers, 'Total');

            $table_rows = [];
            $table_rows['end_product'] = [];
            $table_rows['variant'] = [];

            foreach ($name_list['end_product'] as $key => $item) {
                $row = [];
                $name = $item;
                array_push($row, $name);
                foreach ($table_data as $table_datum) {
                    $qty = (isset($table_datum['qty'][$key])) ? $table_datum['qty'][$key] : 0;
                    array_push($row, $qty);
                }
                array_push($table_rows['end_product'], $row);
            }

            foreach ($name_list['variant'] as $key => $item) {
                $row = [];
                $name = $item;
                array_push($row, $name);
                foreach ($table_data_variant as $table_datum) {
                    $qty = (isset($table_datum['qty'][$key])) ? $table_datum['qty'][$key] : 0;
                    array_push($row, $qty);
                }
                array_push($table_rows['variant'], $row);
            }


        return view('SalesRequestManager::sales_request.sr-merge-print-table',compact('table_rows', 'table_headers'));
    }
}
