<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesAgentAPI extends Model
{
    protected $connection = 'sales';

    protected $table='users';
}
