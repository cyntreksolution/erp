<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecipeSemiFinish extends Model
{
   protected $table ='recipe_semi_finish_product';
}
