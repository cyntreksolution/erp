<?php

namespace App;

use EndProductManager\Models\EndProduct;
use Illuminate\Database\Eloquent\Model;
use SemiFinishProductManager\Models\SemiFinishProduct;

class ReusableStock extends Model
{
    protected $table = 'reusable_stock';
    protected $appends = ['reference_type'];

    public function getReferenceTypeAttribute()
    {
        $data = null;
        switch ($this->causer_type) {
            case 'APP\EndProduct':
            case 'APP\EndProductBurdenGrade':
                $data = 'End Product';
                break;
            case 'SemiFinishProductManager\Models\SemiFinishProduct':
                $data = 'Semi Finish Product';
                break;
        }
        return $data;
    }

    public function getReusableTypeOldAttribute()
    {
        $data = null;
        switch ($this->causer_type) {
            case 'APP\EndProduct':
                $data = EndProduct::find($this->causer_id);
                break;
            case 'APP\EndProductBurdenGrade':
                $gr = EndProductBurdenGrade::find($this->causer_id);
                $data = EndProduct::find($gr->burden->end_product_id);
                break;
            case 'SemiFinishProductManager\Models\SemiFinishProduct':
                $data = SemiFinishProduct::find($this->causer_id);
                break;
        }

        switch ($this->reusable_type) {
            case 1;
                $x = 'None';
                break;
            case 2;
                $x = 'Reusable Bun';
                break;
            case 3;
                $x = 'Reusable Bread';
                break;
            case 4;
                $x = 'Reusable Cake';
                break;
        }
        return $data->reusable_type;
    }

    public function getReusableTypeTextAttribute()
    {
        $x =null;
        switch ($this->reusable_type) {
            case 1;
                $x = 'None';
                break;
            case 2;
                $x = 'Reusable Bun';
                break;
            case 3;
                $x = 'Reusable Bread';
                break;
            case 4;
                $x = 'Reusable Cake';
                break;
        }
        return $x;
    }


    public function getNameAttribute()
    {
        $data = null;
        switch ($this->causer_type) {
            case 'APP\EndProduct':
                $data = EndProduct::find($this->causer_id);
                break;
            case 'APP\EndProductBurdenGrade':
                $gr = EndProductBurdenGrade::find($this->causer_id);
                $data = EndProduct::find($gr->burden->end_product_id);
                break;
            case 'SemiFinishProductManager\Models\SemiFinishProduct':
                $data = SemiFinishProduct::find($this->causer_id);
                break;
        }
        return $data->name;
    }

    public function product(){
        switch ($this->causer_type) {
            case 'APP\EndProduct':
                return $this->belongsTo(EndProduct::class,'causer_id','id');
                break;
            case 'APP\EndProductBurdenGrade':
                return $this->belongsTo(EndProduct::class,'causer_id','id');
                break;
            case 'SemiFinishProductManager\Models\SemiFinishProduct':
                return $this->belongsTo(SemiFinishProduct::class,'causer_id','id');
                break;
        }
    }
}
