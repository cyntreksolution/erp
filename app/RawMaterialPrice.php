<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RawMaterialPrice extends Model
{
    protected $table ='raw_material_price';
}
