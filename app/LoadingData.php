<?php

namespace App;

use Carbon\Carbon;
use EndProductManager\Models\EndProduct;
use Illuminate\Database\Eloquent\Model;

class LoadingData extends Model
{
    protected $table='loading_data';

    public function endProduct(){
        return $this->belongsTo(EndProduct::class)->orderBy('name');
    }

    public function header(){
        return $this->belongsTo(LoadingHeader::class,'loading_header_id');
    }

    public function scopefilterDataNonVariant($query, $end_product = null , $date_range = null)
    {

        if (!empty($end_product)) {
            $query->where('end_product_id','=',$end_product);
        }

       /* if (!empty($category)) {
            $query->where('category_id','=',$category);
        }

        if (!empty($agent_id)) {

            $query->where('agent_id','=',$agent_id);
        }*/

        if (!empty($date_range)) {
            $dates = explode(' - ', $date_range);
            $start = $dates[0];
            $end = $dates[1];
            $query->where('sales_return_data.created_at', '>=', Carbon::parse($start)->format('Y-m-d'))
                ->where('sales_return_data.created_at', '<=', Carbon::parse($end)->addDay()->format('Y-m-d'));

        }
    }

    public function scopefilterData($query, $end_product = null , $date_range = null)
    {

        if (!empty($end_product)) {
            $query->where('end_product_id','=',$end_product);
        }

        /* if (!empty($category)) {
             $query->where('category_id','=',$category);
         }

         if (!empty($agent_id)) {

             $query->where('agent_id','=',$agent_id);
         }*/

        if (!empty($date_range)) {
            $dates = explode(' - ', $date_range);
            $start = $dates[0];
            $end = $dates[1];
            $query->where('sales_return_data.created_at', '>=', Carbon::parse($start)->format('Y-m-d'))
                ->where('sales_return_data.created_at', '<=', Carbon::parse($end)->addDay()->format('Y-m-d'));

        }
    }



}
