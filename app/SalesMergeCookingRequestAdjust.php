<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesMergeCookingRequestAdjust extends Model
{
    protected $table='sales_merge_cooking_request_adjust';
}
