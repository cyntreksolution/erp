<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesEndProduct extends Model
{
    protected $connection = 'sales';

    protected $table='end_products';
}
