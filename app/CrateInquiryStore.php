<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use RawMaterialManager\Models\RawMaterial;
use SalesRepManager\Models\SalesRep;

class CrateInquiryStore extends Model
{
    protected $table ='crates_inquiry_stores';

    public function getReferenceCodeAttribute()
    {
        $data =null;
        switch ($this->reference_type){
            case 'App\LoadingHeader':
                $data= LoadingHeader::find($this->reference)->invoice_number;
                break;
            case 'App\SalesReturnHeader':
                $data= SalesReturnHeader::find($this->reference)->debit_note_number;
                break;
            case 'App\AgentPayment':
                $data= AgentPayment::find($this->reference)->serial;
                break;
        }
        return $data;

    }

    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query->orderBy($order_column, $order_by_str);
    }

    public function scopefilterData($query, $agent_id=null,$crate_id=null,$reference_type=null, $date_range=null)
    {
        if (!empty($agent_id)) {
            $query->where('crates_inquiry_stores.agent_id','=',$agent_id);
        }
        if (!empty($crate_id)) {
            $query->where('crates_inquiry_stores.crate_id','=',$crate_id);
        }


        if (!empty($date_range)) {
            $dates = explode(' - ', $date_range);
            $start = $dates[0];
            $end = $dates[1];
            $query->where('crates_inquiry_stores.created_at', '>=', Carbon::parse($start)->format('Y-m-d'))
                ->where('crates_inquiry_stores.created_at', '<=', Carbon::parse($end)->addDay()->format('Y-m-d'));
        }
    }

    public function crate(){
        return $this->belongsTo(RawMaterial::class,'crate_id','raw_material_id')->whereType(5);
    }
    public function agent(){
        return $this->belongsTo(SalesRep::class,'agent_id','id');
    }

    public function scopeIsInit($query){
        return $query->whereIsInit(1);
    }


}
