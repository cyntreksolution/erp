<?php

namespace App;

use EndProductBurdenManager\Models\EndProductBurden;
use Illuminate\Database\Eloquent\Model;

class SalesMergeEndProductBurden extends Model
{
    protected $table = 'sales_merge_end_product_burdens';

    public function endProductBurden(){
        return $this->belongsTo(EndProductBurden::class);
    }
}
