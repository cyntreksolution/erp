var ps = new PerfectScrollbar('#container');

$(document).ready(function () {
    $("#jq-validation-example-1").validate({
        rules: {
            name: "required",
            buffer_stock: {required: !0, number: !0,min:1},
        },
        messages: {
            name: "Please enter the semi finish  item name",
            buffer_stock: {
                required: "Please enter buffer stock",
                number: "Buffer stock must be number",
                min: "Buffer Stock  must be greater than 0"
            },

        },
        errorElement: "div",
        errorPlacement: function (e, r) {
            e.addClass("form-control-feedback"), "checkbox" === r.prop("type") ? e.insertAfter(r.parent(".checkbox")) : e.insertAfter(r)
        },
        highlight: function (e, r, a) {
            $(e).closest(".form-group").addClass("has-danger").removeClass("has-success")
        },
        unhighlight: function (e, r, a) {
            $(e).closest(".form-group").addClass("has-success").removeClass("has-danger")
        }
    })
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    }
});

$(".js-example-responsive").select2({
    width: 'resolve' // need to override the changed default
});


$('.delete').click(function (e) {
    e.preventDefault();
    id = $(this).attr("value");
    confirmAlert(id);

});

function confirmAlert(id) {
    swal({
            title: "Are you sure?",
            text: "Your will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        },
        function () {
            $.ajax({
                type: "delete",
                url: id,
                success: function (response) {
                    if (response == 'true') {
                        swal("Deleted!", "Deleted successfully", "success");
                        setTimeout(location.reload.bind(location), 900);
                    }
                    else {
                        swal("Error!", "Something went wrong", "error");
                    }
                }
            });

        }
    );

}
