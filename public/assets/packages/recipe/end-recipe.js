var ps = new PerfectScrollbar('#container');
var psr = new PerfectScrollbar('#rcontainer');
$(function () {
    !function () {
        function e() {
            return !!$("#end-recipe-form").valid() || (o.focusInvalid(), !1)
        }

        var o = $("#end-recipe-form").validate({
            rules: {
                end_product: {required: !0},
                recipe_name: {required: !0, minlength: 2},
                units_per_recipe: {required: !0, number: 1},
                semi_product: {required: true}
            },
            messages: {
                recipe_name: {
                    end_product: "Please Select End Product",
                    recipe_name: "Please enter a recipe name",
                    minlength: "Recipe name must consist of at least 2 characters"
                },
                units_per_recipe: {
                    required: "Please provide units per recipes",
                    number: "Units per recipes must be number",
                    notEqual: "Units perdd recipes must be number",
                },
                semi_product: {required: "Please select an semi product"}

            },
            errorElement: "div",
            errorPlacement: function (e, r) {
                e.addClass("form-control-feedback"), "checkbox" === r.prop("type") ? e.insertAfter(r.parent(".checkbox")) : e.insertAfter(r)
            },
            highlight: function (e, r, a) {
                $(e).closest(".form-group").addClass("has-danger").removeClass("has-success")
            },
            unhighlight: function (e, r, a) {
                $(e).closest(".form-group").addClass("has-success").removeClass("has-danger")
            }
        });


        $("#end-recipe-form").bootstrapWizard({
            tabClass: "nav-tabs",
            nextSelector: ".pager>.btn.next",
            previousSelector: ".pager>.btn.previous",
            onTabShow: function (e, o, r) {
                $(e).addClass("visited");
                var n = $("#finish-btn"), s = $("#next-btn"), t = o.find("li").length;
                r + 1 == t ? (n.show(), s.hide()) : (n.hide(), s.show())
            },
            onTabClick: function () {
                return e()
            },
            onPrevious: function (e, o, r) {
                $(e).removeClass("visited")
            },
            onNext: function (o, r, n) {
                return e()
            }
        }), $("#finish-btn").on("click", function (o) {
            // e() ? swal("Great!", "New Recipe has been added.", "success") : swal("Faild", "Please fill in all fields", "error")
        })

    }()
});

$(".js-example-responsive").select2({
    width: 'resolve',
    placeholder: 'Select Product'
});

$(function () {
    $(".column").sortable({
        connectWith: ".column",
        handle: ".handle",
        cancel: ".portlet-toggle",
        placeholder: "portlet-placeholder ui-corner-all"
    });

    $(".portlet")
        .addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
        .find(".portlet-header")
        .addClass("ui-widget-header ui-corner-all")
        .prepend("<span class='ui-icon ui-icon-minusthick portlet-toggle'></span>");

    $(".portlet-toggle").on("click", function () {
        var icon = $(this);
        icon.toggleClass("ui-icon-minusthick ui-icon-plusthick");
        icon.closest(".cardx").find(".portlet-content").toggle();
    });
});


function selectIngrediants(sel) {
    $abc = sel.options[sel.selectedIndex].text;
    $id = sel.options[sel.selectedIndex].value;
}


$("#floating-button-raw").click(function (e) {
    e.preventDefault();
    var x = document.getElementById('qty').value;
    var y = document.getElementById('single').value;

    if (y == "") {
        swal('Select semi product')
        return false;
    }
    if (x == "") {
        swal('enter quantity')
        return false;
    }
    $("#selecting-raw").append('<a>' +
        '<div class="media">' +
        '<div class="avatar avatar-circle avatar-md project-icon" data-plugin="firstLitter" data-target="#' + y + '"></div>' +
        '<div class="media-body"> ' +
        '<h6 class="project-name" id=" ' + y + ' "> ' + $("#single option:selected").text() + '</h6>' +
        '<small class="project-detail">' + $("#qty").val() + '</small>' +
        '</div>' +
        '<div class="section-2">' +
        '<div onclick="hiderecip(this)" class="delete btn btn-light btn-sm ml-2" value=" ' + y+ ' " data-toggle="tooltip" data-placement="left" title="DELETE"> ' +
        '<i class="zmdi zmdi-close"></i>' +
        '</div>' +
        '</div>' +
        '<input type="hidden" id="in" name="raw[]" value=" ' + y + ' ">' +
        '<input type="hidden" id="qt" name="raw_qty[]" value=" ' + $("#qty").val() + ' ">' +
        '</div>' +
        '</a>');
    reset();


});

$("#floating-button-semi").click(function (e) {
    e.preventDefault();
    let semi_id = document.getElementById('semi').value;
    let semi_name = $("#semi option:selected").text();
    let semi_qty = document.getElementById('semi-qty').value;

    if (semi_id == "") {
        swal('Select semi product');
        return false;
    }
    if (semi_qty == "") {
        swal('enter quantity');
        return false;
    }

    $("#selecting-semi").append('<a>' +
        '<div class="media">' +
        '<div class="avatar avatar-circle avatar-md project-icon" data-plugin="firstLitter" data-target="#' + semi_id + '"></div>' +
        '<div class="media-body"> ' +
        '<h6 class="project-name" id=" ' + semi_id + ' "> ' + semi_name + '</h6>' +
        '<small class="project-detail">' + semi_qty + '</small>' +
        '</div>' +
        '<div class="section-2">' +
        '<div onclick="hiderecip(this)" class="delete btn btn-light btn-sm ml-2" value=" ' + semi_id + ' " data-toggle="tooltip" data-placement="left" title="DELETE"> ' +
        '<i class="zmdi zmdi-close"></i>' +
        '</div>' +
        '</div>' +
        '<input type="hidden" id="in" name="semi[]" value=" ' + semi_id + ' ">' +
        '<input type="hidden" id="qt" name="semi_qty[]" value=" ' + semi_qty + ' ">' +
        '</div>' +
        '</a>');

    document.getElementById("semi").value = "";
    document.getElementById("semi-qty").value = "";
});


$("#floating-button-packing").click(function (e) {
    e.preventDefault();
    let packing_id = document.getElementById('packing').value;
    let packing_name = $("#packing option:selected").text();
    let packing_qty = document.getElementById('packing-qty').value;

    if (packing_id == "") {
        swal('Select Packing item');
        return false;
    }
    if (packing_qty == "") {
        swal('Enter Packing Quantity');
        return false;
    }

    $("#selecting-packing").append('<a>' +
        '<div class="media">' +
        '<div class="avatar avatar-circle avatar-md project-icon" data-plugin="firstLitter" data-target="#' + packing_id + '"></div>' +
        '<div class="media-body"> ' +
        '<h6 class="project-name" id=" ' + packing_id + ' "> ' + packing_name + '</h6>' +
        '<small class="project-detail">' + packing_qty + '</small>' +
        '</div>' +
        '<div class="section-2">' +
        '<div onclick="hiderecip(this)" class="delete btn btn-light btn-sm ml-2" value=" ' + packing_id + ' " data-toggle="tooltip" data-placement="left" title="DELETE"> ' +
        '<i class="zmdi zmdi-close"></i>' +
        '</div>' +
        '</div>' +
        '<input type="hidden" id="in" name="packing[]" value=" ' + packing_id + ' ">' +
        '<input type="hidden" id="qt" name="packing_qty[]" value=" ' + packing_qty + ' ">' +
        '</div>' +
        '</a>');

    document.getElementById("packing").value = "";
    document.getElementById("packing-qty").value = "";
});


function hiderecip(e) {
    $(e).closest(".media").remove();
}

function validateIngerdients() {
    var x = document.getElementById('qty').value;
    var y = document.getElementById('single').value;
    if (y == "") {
        swal('Select semi product')
        return false;
    }
    if (x == "") {
        swal('enter quantity')
        return false;
    } else {
        return true;
    }
}

function reset() {
    document.getElementById("single").value = "";
    document.getElementById("qty").value = "";
}
