
var ps = new PerfectScrollbar('#container');
var ps2 = new PerfectScrollbar('#container2');

$(document).ready(function () {



    $("#jq-validation-example-1").validate({
        rules: {
            name: "required",
            bstock: {required: !0, number: !0,min:1},
            grams:{required: !0},
            username: {required: !0, minlength: 2},
            password: {required: !0, minlength: 5},
            confirm_password: {required: !0, minlength: 5, equalTo: "#password"},
            email: {required: !0, email: !0},
            agree: "required"
        },
        messages: {
            name: "Please enter the raw material name",
            // bstock: "Please enter the bst",
            grams: "Please enter the weight in grames",
            bstock: {
                required: "Please enter Buffer Stock",
                number: "Please enter a valid number",
                min: "Buffer Stock  must be greater than 0"
            },
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            confirm_password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long",
                equalTo: "Please enter the same password as above"
            },
            email: "Please enter a valid email address",
            agree: "Please accept our policy"
        },
        errorElement: "div",
        errorPlacement: function (e, r) {
            e.addClass("form-control-feedback"), "checkbox" === r.prop("type") ? e.insertAfter(r.parent(".checkbox")) : e.insertAfter(r)
        },
        highlight: function (e, r, a) {
            $(e).closest(".form-group").addClass("has-danger").removeClass("has-success")
        },
        unhighlight: function (e, r, a) {
            $(e).closest(".form-group").addClass("has-success").removeClass("has-danger")
        }
    })
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    }
});

$('.delete').click(function (e) {
    e.preventDefault();
    id = $(this).attr("value");
    confirmAlert(id);

});

function confirmAlert(id) {
    swal({
            title: "Are you sure?",
            text: "Your will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        },
        function () {
            $.ajax({
                type: "delete",
                url: id,
                success: function (response) {
                    if (response == 'true') {
                        swal("Deleted!", "Deleted successfully", "success");
                        setTimeout(location.reload.bind(location), 600);
                    }
                    else {
                        swal("Error!", "Sorry !. This Ingredient Can not Delete !", "error");
                    }
                }
            });

        }
    );

}
function selectMeasurement(sel) {
    $abc = sel.options[sel.selectedIndex].text
    if ($abc=='Unit'){
        $("#unitx").removeClass('hide');
    }
    else {
        $("#unitx").addClass('hide');
    }

}

