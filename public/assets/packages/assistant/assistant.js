
$(document).ready(function () {
    $("#jq-validation-example-1").validate({
        rules: {
//                    name: "required",
            fn: {required: !0, minlength: 5},
            ln: {required: !0, minlength: 5},
            email: {required: !0},
            nic: {required: !0, minlength: 10, maxlength: 13},
            confirm_password: {required: !0, minlength: 5, equalTo: "#password"},
//                    email: {required: !0, email: !0},
            agree: "required"
        },
        messages: {

            fn: {
                required: "Please enter your first name",
                minlength: "Your first name must consist of at least 5 characters"
            },
            ln: {
                required: "Please enter your last name",
                minlength: "Your last name must consist of at least 5 characters"
            },
            nic: {
                required: "Please enter your NIC number",
                minlength: " NIC number must be minimum 10 characters long",
                maxlength:"NIC number must be maximum 13 characters long",
            },
            email: "Please enter a valid email address",

        },
        errorElement: "div",
        errorPlacement: function (e, r) {
            e.addClass("form-control-feedback"), "checkbox" === r.prop("type") ? e.insertAfter(r.parent(".checkbox")) : e.insertAfter(r)
        },
        highlight: function (e, r, a) {
            $(e).closest(".form-group").addClass("has-danger").removeClass("has-success")
        },
        unhighlight: function (e, r, a) {
            $(e).closest(".form-group").addClass("has-success").removeClass("has-danger")
        }
    })
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    }
});

$(".js-example-responsive").select2({
    width: 'resolve' // need to override the changed default
});


$('.delete').click(function (e) {
    e.preventDefault();
    id = $(this).attr("value");
    confirmAlert(id);

});

function confirmAlert(id) {
    swal({
            title: "Are you sure?",
            text: "Your will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        },
        function () {
            $.ajax({
                type: "delete",
                url: id,
                success: function (response) {
                    if (response == 'true') {
                        swal("Deleted!", "Deleted successfully", "success");
                        setTimeout(location.reload.bind(location), 900);
                    }
                    else {
                        swal("Error!", "Something went wrong", "error");
                    }
                }
            });

        }
    );

}