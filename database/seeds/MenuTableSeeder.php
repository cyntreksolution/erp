<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu')->insert([
            'label' => 'Root Menu',
            'link' => '#',
            'parent' => 0,
            'lft' => 1,
            'rgt' => 100,
            'depth' => 0,
        ]);
    }
}
