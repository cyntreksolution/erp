<?php

namespace LoadingManager\Controllers;

use App\AgentBuffer;
use App\AgentInquiry;
use App\CrateInquiryAgent;
use App\EndProductInquiry;
use App\LoadingData;
use App\LoadingDataRaw;
use App\LoadingDataVariant;
use App\LoadingHeader;
use App\CratesTransaction;
use App\SalesReturnData;
use App\Traits\AgentLog;
use App\Traits\CratesAgentLog;
use App\Traits\CratesStoreLog;
use App\Traits\EndProductLog;
use App\Traits\RawMaterialLog;
use App\Variant;

use Carbon\Carbon;
use EndProductManager\Models\Category;
use SalesOrderManager\Models\RawContent;
use SalesRepManager\Models\SalesRep;
use UserManager\Models\Users;

use Response;
use Illuminate\Support\Facades\Storage;
use EndProductManager\Models\EndProduct;
use LoadingManager\Models\Loading;
use PurchasingOrderManager\Models\Content;
use SalesOrderManager\Models\SalesOrder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use PurchasingOrderManager\Models\PurchasingOrder;
use RawMaterialManager\Models\RawMaterial;
use StockManager\Classes\StockTransaction;
use SupplierManager\Models\Supplier;
use function GuzzleHttp\Promise\all;
use Barryvdh\DomPDF\Facade as PDF;
use LoadingManager\Models\LoadingStatusChanges;
use SalesRepManager\Models\AgentCategory;

class LoadingController extends Controller
{
    use AgentLog, EndProductLog, RawMaterialLog, CratesAgentLog, CratesStoreLog;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:loading-index', ['only' => ['index','tableData','showTable']]);
        $this->middleware('permission:loading-list', ['only' => ['list']]);
        $this->middleware('permission:loading-preview', ['only' => ['preview']]);
        $this->middleware('permission:loading-delete', ['only' => ['delete']]);
        $this->middleware('permission:loading-show', ['only' => ['show']]);
        $this->middleware('permission:loading-create', ['only' => ['create']]);
        $this->middleware('permission:loading-allLoaded', ['only' => ['allLoaded']]);

    }
    public function index()
    {
        $orders = LoadingHeader::whereStatus(1)->get();
        return view('LoadingManager::loading.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = EndProduct::all();
        $supliers = Supplier::all();

        return view('LoadingManager::loading.create')->with([
            'products' => $products,
            'supliers' => $supliers,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function inv_correction(Request $request,$id)
    {

        $itemsum1 = LoadingData::selectRaw('COALESCE(sum(total), 0) as itemtotal')->whereLoadingHeaderId($id)->first();
        $itemsum2 = LoadingData::selectRaw('COALESCE(sum(unit_discount), 0) as itemdis')->whereLoadingHeaderId($id)->first();
        $itemsum3 = LoadingData::selectRaw('COALESCE(sum(commission), 0) as itemcom')->whereLoadingHeaderId($id)->first();

        $itemvari1 = LoadingDataVariant::selectRaw('COALESCE(sum(total), 0) as varitotal')->whereLoadingHeaderId($id)->first();
        $itemvari2 = LoadingDataVariant::selectRaw('COALESCE(sum(unit_discount), 0) as varidis')->whereLoadingHeaderId($id)->first();
        $itemvari3 = LoadingDataVariant::selectRaw('COALESCE(sum(commission), 0) as varicom')->whereLoadingHeaderId($id)->first();

        $totsum = $itemsum1->itemtotal + $itemvari1->varitotal;
        $totdis = $itemsum2->itemdis + $itemvari2->varidis;
        $totcom = $itemsum3->itemcom + $itemvari3->varicom;

        $ex = LoadingHeader::whereId($id)->first();
        $ex->total = $totsum;
        $ex->unit_discount = $totdis;
        $ex->commission = $totcom;
        $ex->total_discount = $totdis + $totcom;
        $ex->net_amount = $totsum - ($totdis + $totcom);
      //

        $reftype = 'App\\LoadingHeader';
        $reftype_adj = 'App\\LoadingHeader\\Adjust';

        $ext =AgentInquiry::where('reference','=',$id)->where('reference_type','=',$reftype)->first();


        $amodif = ($totsum - ($totdis + $totcom + $ext->amount));
        $desc = 'ADJ-'.$ext->sub_desc;

        if($amodif != 0) {
            $agent = SalesRep::find($ext->agent_id);
            $this->recordOutstanding($agent, $amodif, 'Sales Order Loading Adjustment', $id, 'App\\LoadingHeader\\Adjust', $desc);
            StockTransaction::updateAgentBalance(1, $ext->agent_id, $amodif);
        }

        $ex->save();

        $categories = Category::all()->pluck('name', 'id');
        $agents = SalesRep::all()->pluck('email', 'id');
        return view('order_list', compact('agents', 'categories'));

    }

    public function store(Request $request)
    {

//         return $request->all();
        $pdfStatus = $request->submit;
        $refOrder = $request->refOrder;
        $unit_price = $request->unit_price;
        $variant_unit_price = $request->variant_unit_price;
        $user = Auth::user()->id;
        $loadingHeaderId = 0;

//        if ($refOrder) {
        $item_id = $request->item_id;
        $product_id = $request->product_id;
        $qty = $request->qty;
        $id = $request->id;

        $item_id_r = $request->item_id_r;
        $raw_id = $request->raw_id;

//            DB::transaction(function () use ($item_id_r, $raw_id, $product_id, $user, $id, $item_id, $qty, $request, $pdfStatus, $unit_price, &$loadingHeaderId) {

        $loadingHeader = LoadingHeader::find($id);
        $cratesSent = new CratesTransaction();

        // $total = 0;
        // foreach ($loadingHeader->items as $item) {
        //     $total = $total + ($item->qty * $item->unit_value);
        // }


        $items = LoadingData::where('loading_header_id', '=', $id)->get();
        $total = 0;
        if (!empty($items)) {
            foreach ($items as $item) {
                $itemData = LoadingData::where('id', '=', $item->id)->first();
                $total = $total + ($itemData->qty * $itemData->unit_value);
                $itemData->total = $itemData->qty * $itemData->unit_value;
                $itemData->row_value = $itemData->qty * $itemData->unit_value;
                $itemData->save();
            }
        }

        $variants = LoadingDataVariant::where('loading_header_id', '=', $id)->get();

        if (!empty($variants)) {
            foreach ($variants as $key => $item) {
                $variantData = LoadingDataVariant::where('id', '=', $item->id)->first();
                $total = $total + ($variantData->weight * $variant_unit_price[$key]);
                $variantData->total = $variantData->weight * $variant_unit_price[$key];
                $variantData->row_value = $variantData->weight * $variant_unit_price[$key];
                $variantData->save();
            }
        }

        $itemsum1 = LoadingData::selectRaw('COALESCE(sum(total), 0) as itemtotal')->whereLoadingHeaderId($id)->first();
        $itemsum2 = LoadingData::selectRaw('COALESCE(sum(total_discount), 0) as itemdis')->whereLoadingHeaderId($id)->first();

        $itemvari1 = LoadingDataVariant::selectRaw('COALESCE(sum(total), 0) as varitotal')->whereLoadingHeaderId($id)->first();
        $itemvari2 = LoadingDataVariant::selectRaw('COALESCE(sum(total_discount), 0) as varidis')->whereLoadingHeaderId($id)->first();



        $tot = $total;

        $load = (($itemsum1->itemtotal)-($itemsum2->itemdis)) + (($itemvari1->varitotal)-($itemvari2->varidis));
        $dif = $tot - $load ;
        $diff = number_format(($dif),0);





        if($diff == 0) {
            $crates_data = [];
            if (!empty($request->crate) && sizeof($request->crate) > 0) {
                $isin = CrateInquiryAgent::where('reference', '=', $id)->where('desc_id', '=', 1)->first();

                if (empty($isin)) {
                    foreach ($request->crate as $key => $crate) {
                        $cra_r = RawMaterial::find($crate);
                        $agent = SalesRep::find($loadingHeader->salesOrder->salesRep->id);


                        $this->recordCratesAgentLogOutstanding($cra_r, $agent, $request->crate_qty[$key], 'Creates receive with invoice', $id, 'App\\LoadingHeader', 1, 1);
                        $this->recordCratesStoreLogOutstanding($cra_r, (($request->crate_qty[$key]) * (-1)), 'Creates issue with invoice', $id, 'App\\LoadingHeader', 3, 2, $agent->id);
                        $this->recordRawMaterialOutstanding($cra_r, (($request->crate_qty[$key]) * (-1)), 'Creates issue with invoice', $id, 'App\\LoadingHeader', 14, 2, $agent->id);

                        StockTransaction::updateRawMaterialAvailableQty(2, $crate, $request->crate_qty[$key]);
                        StockTransaction::rawMaterialStockTransaction(2, 0, $crate, $request->crate_qty[$key], $loadingHeader->id, 1);
                        StockTransaction::cratesTransaction(1, $crate, $request->crate_qty[$key], $loadingHeader->id, $loadingHeader->salesOrder->salesRep->id);


                        $crates_d = ['crate_id' => $crate, 'qty' => $request->crate_qty[$key]];
                        array_push($crates_data, $crates_d);
                    }

                    $cratesSent->type = 1;
                    $cratesSent->crates = json_encode($crates_data);
                    $cratesSent->agent_id = $loadingHeader->salesOrder->salesRep->id;
                    $cratesSent->ref_id = $loadingHeader->id;
                    $cratesSent->created_by = $loadingHeader->salesOrder->salesRep->id;

                    $available_crates_data = [];
                    //add crates to user
                    $salesCrates = Users::find($loadingHeader->salesOrder->salesRep->id);


                    if ($salesCrates->crates == NULL) {
                        $salesCrates->crates = json_encode($crates_data);
                    } else {
                        $arrayDe[] = json_decode($salesCrates->crates);
                        for ($i = 0; $i < sizeof($request->crate); $i++) {
                            $key = array_search($request->crate[$i], array_column($arrayDe[0], 'crate_id'));
                            if ($key === false) {
                                $available_qty = $request->crate_qty[$i];
                            } else {
                                $available_qty = (int)$arrayDe[0][$key]->qty + (int)$request->crate_qty[$i];
                            }
                            $available_crates_d = ['crate_id' => $request->crate[$i], 'qty' => $available_qty];
                            array_push($available_crates_data, $available_crates_d);
                        }
                        for ($i = 0; $i < sizeof($arrayDe[0]); $i++) {
                            $keyCheck = array_search($arrayDe[0][$i]->crate_id, $request->crate);
                            if ($keyCheck === false) {
                                $available_crates_d = ['crate_id' => $arrayDe[0][$i]->crate_id, 'qty' => $arrayDe[0][$i]->qty];
                                array_push($available_crates_data, $available_crates_d);
                            }
                        }
                        $salesCrates->crates = json_encode($available_crates_data);
                    }

                    $cratesSent->save();
                    $salesCrates->save();
                }

            }


            $loadingHeader->crate_sent = json_encode($crates_data);

            $loadingHeader->total = $total;
//        $loadingHeader->status = 2;
            $loadingHeader->shop_type = $loadingHeader->salesOrder->salesRep->desc1;
            $loadingHeader->extra_description = $request->description;
            $loadingHeader->loading_date = Carbon::now();
            $loadingHeader->save();

           // $upend = EndProductInquiry::where('so_id','=',$id)->whereIn('desc_id',[1,2,3,4])->get();
            $upend = EndProductInquiry::where('so_id','=',$id)->get();
            foreach ($upend as $upends){
                $chk = EndProductInquiry::find($upends->id);
                $chk->loading_date = Carbon::now();
                $chk->save();
            }


            //
            $salesOrder = SalesOrder::find($loadingHeader->so_id);
            $salesOrder->status = 3;
            $salesOrder->save();


            if ($request->loadDiscount == 1) {
                return $this->loadWithDiscount($loadingHeader);
            } else {
                // $isExists = LoadingStatusChanges::where('loading_header_id','=',$loadingHeader->id)->first();
                // if(empty($isExists))
                // {
                //   $loadingStatus = new LoadingStatusChanges();
                //   //change loading header->status = 2 and salesorder->status = 3
                //   $loadingStatus->loading_header_id = $loadingHeader->id;
                //   $loadingStatus->status = 2;
                //   $loadingStatus->agent_id = $salesOrder->created_by;
                //   $loadingStatus->total_amount = $total;
                //   $loadingStatus->save();
                // }

                $loading = $loadingHeader;
                $this->updateNetValue($loading->id);
                return view('discount-bill', compact('loading'));
            }


            //pdf
            $loadingHeaderId = $loadingHeader->id;
            $this->generateInvoicePdf($loadingHeaderId);

            return redirect(route('loading.index'))->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'New invoice generated',
                'pdf' => true,
                'pdf.type' => $pdfStatus,
                'pdf.file' => $loadingHeaderId
            ]);
        }else{
            return redirect(route('loading.index'))->with([
                'error' => true,
                'error.title' => 'Sorry !',
                'error.message' => 'Something Went Wrong . Refresh and try again...'
            ]);
        }

    }

//            });

//        }


//====================================================================================================
//        $end_products = $request->ingredients;
//        $qty = $request->qty;
//        try {
//            DB::transaction(function () use ($user, $end_products, $qty) {
//                foreach ($end_products as $key => $end_product) {
//                    StockTransaction::endProductStockTransaction(2, $end_product, 0, 0, 0, $qty[$key], 0, 1);
//                    StockTransaction::updateEndProductAvailableQty(2, $end_product, $qty[$key]);
//                }
//            });
//
//            return redirect(route('loading.index'))->with([
//                'success' => true,
//                'success.title' => 'Congratulations !',
//                'success.message' => 'New purchasing order has been added'
//            ]);
//        } catch (Exception $e) {
//            return redirect(route('loading.index'))->with([
//                'error' => true,
//                'error.title' => 'Sorry !',
//                'error.message' => 'Something Went Wrong '
//            ]);
//        }


    public function pdf(Request $request, LoadingHeader $loading)
    {
        $action = $request->type;
        $filename = $loading->id . '.pdf';
        return $this->getPDfInvoice($filename, $action);
    }

    public function generateInvoicePdf($invoiceId)
    {
        $invoice = LoadingHeader::whereId($invoiceId)->first();
        $pdf = PDF::loadView('LoadingManager::pdf.invoice', compact('invoice'))->setPaper('a4', 'portrait');
        $filename = $invoice->id . '.pdf';
        Storage::disk('local')->put('invoices/' . $filename, $pdf->output());
        return $filename;
    }

    public function getPDfInvoice($filename, $action = 'stream')
    {
//        if (Storage::disk('local')->exists('invoices/' . $filename)) {
//            $url = 'invoices/' . $filename;
//            return Storage::download($url);
//        } else {
        $invoice_no = str_replace('.pdf', '', $filename);
        $invoice_id = LoadingHeader::whereId($invoice_no)->first()->id;
        $filename = $this->generateInvoicePdf($invoice_id);
        return Storage::download('invoices/' . $filename);
//            return $this->getPDfInvoice($filename, $action);
//        }
    }

    public function debug(Request $request)
    {
        $invoice = LoadingHeader::whereId(42)->first();
        return view('LoadingManager::pdf.invoice', compact('invoice'));
//
    }

    public function allLoaded()
    {
        $orders = LoadingHeader::orderBy('updated_at', 'desc')->whereDate('updated_at', Carbon::today())->get();
        return view('LoadingManager::loading.loaded', compact('orders'));
    }

    public function show(Request $request, $so)
    {
        $endProducts = EndProduct::all();
        $so = LoadingHeader::whereId($so)->first();
        $salesOrder = SalesOrder::find($so->so_id);
        $category = $salesOrder->template->category_id;
        $agent_id = $salesOrder->template->agent_id;
        $agentCategory = AgentCategory::where('sales_ref_id', '=', $agent_id)->where('category_id', '=', $category)->first();

        //        dd($agentCategory->discount_mode);
        if (empty($agentCategory)) {
            $discountMode = 0;
        } else {
            $discountMode = $agentCategory->discount_mode;
        }
        return view('SalesOrderManager::so.show')->with([
            'order' => $so, 'endProducts' => $endProducts, 'discountMode' => $discountMode
        ]);
    }

    public function loadItem(Request $request)
    {

        $id = $request->data;
        $qty = $request->qty;
        $reason = $request->reason;
        $ag_id = $request->agent;
        $order_id = $request->order;
        $loadingData = LoadingData::find($id);
        $so = LoadingHeader::whereId($order_id)->first();

        $ord = SalesOrder::where('id','=',$so->so_id)->first();
        $isloaded = LoadingData::where('loading_header_id','=',$order_id)
            ->where('end_product_id','=',$loadingData->end_product_id)
            ->first();

        if ($loadingData->endProduct->available_qty >= $qty || $qty == 0) {

            if($isloaded->status == 0 && $ord->status != 1) {

                $loadingData->status = 1;
                $loadingData->qty = $qty;
                $loadingData->reason = $reason;

                $loadingData->save();
                StockTransaction::endProductStockTransaction(2, $loadingData->end_product_id, 0, 0, 0, $qty, 0, 1);
                StockTransaction::updateEndProductAvailableQty(2, $loadingData->end_product_id, $qty);
                $endP = EndProduct::find($loadingData->end_product_id);

                //  $ag_id = LoadingHeader::whereId($so->id)->first();

                $this->recordEndProductOutstanding($endP, 1, (($qty) * (-1)), 'Loading', $loadingData->id, 'App\\LoadingData', $ag_id, 1, 2, $order_id);


            }
            return 'true';
        } else {
            return 'false';
        }
    }

    public function loadItemVariant(Request $request)
    {
        $id = $request->data;
        $qty = $request->qty;
        $ag_id = $request->agent;
        $order_id = $request->order;
        $weight = $request->weight;
        $reason = $request->reason;
        $loadingData = LoadingDataVariant::find($id);
        $so = LoadingHeader::whereId($order_id)->first();
        $ord = SalesOrder::where('id','=',$so->so_id)->first();
        if ($loadingData->variant->product->available_qty >= $weight || $weight == 0) {
            /* $isloaded = LoadingDataVariant::where('loading_header_id','=',$order_id)
                //->where('variant_id','=',$loadingData->variant->product->id)
                ->where('variant_id','=',$loadingData->variant_id)
                ->first();*/

            if($ord->status != 1) {

                $loadingData->status = 1;
                $loadingData->qty = $qty;
                $loadingData->weight = $weight;
                $loadingData->reason = $reason;

                $loadingData->save();
                if (!empty($weight) && $weight != 0) {
                    $qty = $weight;
                }

                StockTransaction::endProductStockTransaction(2, $loadingData->variant->product->id, 0, 0, 0, $weight, 0, 1);
                StockTransaction::updateEndProductAvailableQty(2, $loadingData->variant->product->id, $weight);
                $endP = EndProduct::find($loadingData->variant->product->id);

                //  $ag_id = LoadingHeader::whereId($so->id)->first();
                $this->recordEndProductOutstanding($endP, 1, (($weight) * (-1)), 'Variant Loading', $loadingData->id, 'App\\LoadingDataVariant', $ag_id, 2, 2, $order_id);
            }

            return 'true';
        } else {
            return 'false';
        }
    }

    public function unloadItem(Request $request)
    {
        $id = $request->data;
        $qty = $request->qty;
        $ag_id = $request->agent;
        $order_id = $request->order;
        $so = LoadingHeader::whereId($order_id)->first();
        if ($so->status == 1) {
            $loadingData = LoadingData::find($id);
            $loadingData->status = 0;
            $loadingData->qty = 0;
            $loadingData->reason = null;
            $loadingData->save();
            StockTransaction::endProductStockTransaction(1, $loadingData->end_product_id, 0, 0, 0, $qty, 0, 1);
            StockTransaction::updateEndProductAvailableQty(1, $loadingData->end_product_id, $qty);
            $endP = EndProduct::find($loadingData->end_product_id);
           // $ag_id = LoadingHeader::whereId($so->id)->first();
            $this->recordEndProductOutstanding($endP, 1, $qty, 'Unloading', $loadingData->id, 'App\\LoadingData',$ag_id,3,1,$so->id);

            return 'true';
        } else {
            return 'false';
        }
    }

    public function unloadItemVariant(Request $request)
    {
        $id = $request->data;
        $qty = $request->qty;
        $ag_id = $request->agent;
        $order_id = $request->order;
        $so = LoadingHeader::whereId($order_id)->first();
        if ($so->status == 1) {
            $loadingData = LoadingDataVariant::find($id);
            $weight = $loadingData->weight;
            $loadingData->status = 0;
            $loadingData->qty = $qty;
            $loadingData->weight = 0;
            $loadingData->reason = null;
            $loadingData->save();

            if (!empty($weight) && $weight != 0) {
                $qty = $weight;
            }

            StockTransaction::endProductStockTransaction(1, $loadingData->variant->product->id, 0, 0, 0, $qty, 0, 1);
            StockTransaction::updateEndProductAvailableQty(1, $loadingData->variant->product->id, $qty);
            $endP = EndProduct::find($loadingData->variant->product->id);
          ////  $ag_id = LoadingHeader::whereId($so->id)->first();
            $this->recordEndProductOutstanding($endP, 1, $qty, 'Variant Unloading', $loadingData->id, 'App\\LoadingDataVariant',$ag_id,4,1,$so->id);

            return 'true';
        } else {
            return 'false';
        }
    }

  /*  public function loadItemRaw(Request $request)
    {
        $id = $request->data;
        $qty = $request->qty;
        $reason = $request->reason;
        $loadingData = LoadingDataRaw::find($id);
        if ($loadingData->rawMaterial->available_qty >= $qty) {
            $loadingData->status = 1;
            $loadingData->qty = $qty;
            $loadingData->reason = $reason;
            $loadingData->save();
            StockTransaction::rawMaterialStockTransaction(2, 0, $loadingData->raw_material_id, $qty, $loadingData->id, 1);
            StockTransaction::updateRawMaterialAvailableQty(2, $loadingData->raw_material_id, $qty);
            $this->recordRawMaterialOutstanding($loadingData->raw_material_id, -$qty, 'Shop Item Loading', $loadingData->id, 'APp\\LoadingDataRaw');

            return 'true';
        } else {
            return 'false';
        }
    }

    public function unloadItemRaw(Request $request)
    {
        $id = $request->data;
        $qty = $request->qty;
        $loadingData = LoadingDataRaw::find($id);
        $loadingData->status = 0;
        $loadingData->qty = $qty;
        $loadingData->reason = null;
        $loadingData->save();
        StockTransaction::rawMaterialStockTransaction(1, 0, $loadingData->raw_material_id, $qty, $loadingData->id, 1);
        StockTransaction::updateRawMaterialAvailableQty(1, $loadingData->raw_material_id, $qty);
        $this->recordRawMaterialOutstanding($loadingData->raw_material_id, $qty, 'Shop Item Unloading', $loadingData->id, 'APp\\LoadingDataRaw');
        return 1;
    }*/

    public function checkALLOrderItemStatus(Request $request, $loadingHeader)
    {
//        $data = collect(LoadingData::whereLoadingHeaderId($loadingHeader)->get()->pluck('status'));
//        $data_variant = collect(LoadingDataVariant::whereLoadingHeaderId($loadingHeader)->get()->pluck('status'));
//        $data_all = $data->merge($data_variant);
//        $data = $data_all->contains(0);

        $item_count = LoadingData::whereLoadingHeaderId($loadingHeader)->count();
        $variant_count = LoadingDataVariant::whereLoadingHeaderId($loadingHeader)->count();
        $total_count = $item_count + $variant_count;

        $item_count_loaded = LoadingData::whereLoadingHeaderId($loadingHeader)->whereStatus(1)->count();
        $variant_count_loaded = LoadingDataVariant::whereLoadingHeaderId($loadingHeader)->whereStatus(1)->count();
        $total_count_loaded = $item_count_loaded + $variant_count_loaded;

        if ($total_count == $total_count_loaded) {
            return 'true';
        } else {
            return 'false';
        }
    }


    public function preview(Request $request, LoadingHeader $loading)
    {

   /*     $total = 0;
        foreach ($loading->items as $item) {
            $total = $total + ($item->qty * $item->unit_value);
        }
        foreach ($loading->Variants as $item) {
            $total = $total + ($item->weight * $item->unit_value);
        }

        $loading->total = $total;
        $loading->save();*/

        $invoice = $loading;
//        return view('LoadingManager::pdf.preview', compact('invoice'));
        $pdf = PDF::loadView('LoadingManager::pdf.preview', compact('invoice'));
        $pdf->setPaper([0, 0, 220, 9999], 'portrait');
        return $pdf->stream();
    }

    // load with discount section
    public function loadWithDiscount(LoadingHeader $loading)
    {
        return view('SalesOrderManager::so.discount', compact('loading'));
    }

    public function loadWithDiscountX(Request $request)
    {
        $loading = LoadingHeader::find($request->loading);
        return view('SalesOrderManager::so.discount', compact('loading'));
    }


    public function showTable(Request $request)
    {
        $categories = Category::all()->pluck('name', 'id');
        $agents = SalesRep::all()->pluck('name_with_initials', 'id');
        return view('loading.table', compact('categories', 'agents'));
    }

    public function tableData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'serial', 'semi_finish_product_id', 'status', 'burden_size', 'recipe_id', 'created_at'];
        $order_column = $columns[$order_by[0]['column']];

        $invoiceCount = LoadingHeader::whereStatus(1);


        $invoices = LoadingHeader::tableData($order_column, $order_by_str, $start, $length)->whereStatus(1);

        $data = [];
        $i = 0;


        if ($request->filter == true) {
            $time = $request->time;
            $day = $request->day;
            $agent = $request->agent;
            $category = $request->category;
            $date_range = $request->date_range;

            $invoices = $invoices->filterData($date_range, $agent, $category, $day, null, $time)->get();
            $invoiceCount = $invoiceCount->count();

            foreach ($invoices as $key => $invoice) {
                $delete_btn = '';
                $flag = false;
                $loadi = LoadingData::where('loading_header_id','=',$invoice->id)->where('status', '=', 1)->first();
                $loadv = LoadingDataVariant::where('loading_header_id','=',$invoice->id)->where('status', '=', 1)->first();


                $loadings = LoadingHeader::where('so_id', '=', $invoice->so_id)->count();
                if ($loadings > 1) {
                    $flag = true;
                    if ($role = Auth::user()->hasRole(['Super Admin', 'Owner'])) {
                        $url = route('delete.loading', ['id' => $invoice->id]);
                        $delete_btn = "<a href='$url' class='btn btn-sm btn-danger ml-2'><i class='fa fa-recycle'></i> </a>";
                    } else {
                        if (empty($loadi) && empty($loadv)) {

                            $delete_btn = "<a class='btn btn-sm btn-danger ml-2' disabled='ture'><i class='fa fa-recycle'></i> </a>";
                        }
                    }
                }
                $st = $flag ? 'disabled' : '';

                $rep = $invoice->salesOrder->salesRep;
                $inv = LoadingHeader::find($invoice->id);
                $ord = SalesOrder::find($invoice->so_id);

                $statusBtn = null;
                $btn3 = null;
                $btn2 = null;
                $btn1 = null;
                $rvBtn = null;
                $crBtn = null;

                $btn1 = "<a title='Preview' target='_blank' href='" . route('loading.preview', $invoice->id) . "' class='ml-3 btn btn-outline-primary mr-2' ><i class='fa fa-file'></i> </a>";


                if (!$inv->isShopItemsLoaded()) {
                    if (!$flag) {
                        $btn3 = "<a title='Issue Shop Items'   href='#' class='btn btn-outline-primary ml-3 disabled' disabled='true'><i class='fa fa-plane'></i> </a>";
                    }
               }

                if ($inv->isShopItemsLoaded()) {
                    if (!$flag) {


                        $crapp = CratesTransaction::whereType('return')->where('crates_approve', '=', Null)->where('agent_id','=',$rep->id)->first();


                        if($ord->status == 1){
                            $rvBtn = " <button class ='btn btn-sm btn-danger'>Reversed</button>";
                        }else{
                            $btn2 = " <a  title='Load Now' target='_blank' href='" . route('loading.showLoading', $invoice->id) . "'  class='btn btn-outline-primary ml-2' ><i class='fa fa-truck'></i> </a>";

                        }




                        if(empty($crapp)){
                        }else{
                             $crBtn = " <button class ='btn btn-sm btn-warning'>Approve Crates</button>";
                        }

                    }
                }
                $shopreq = RawContent::where('sales_order_header_id', '=', $invoice->so_id)->first();
                $ordershop_btn = null;
                if(empty($shopreq)){
                    $ordershop_btn = "<button class ='btn btn-sm btn-warning'>Not Requested</button>";
                }else{
                    $ordershop_btn = "<button class ='btn btn-sm btn-success'>Loaded</button>";
                }





                $shop_item_load = ($inv->isShopItemsLoaded()) ? $ordershop_btn : "<button class ='btn btn-sm btn-info'>Not Loaded</button>";
                $reverse_btn = null;
                $loading_btn = null;

                if (empty($loadi) && empty($loadv)) {
                    if($ord->status != 1) {

                        $shop = RawContent::where('sales_order_header_id', '=', $invoice->so_id)->where('status','=',1)->first();
                        if(empty($shop)) {

                            if ($role = \Cartalyst\Sentinel\Laravel\Facades\Sentinel::check()->roles[0]->slug == 'owner') {
                                $reverse_btn = "<a title='Reverse' href='" . route('loading.revers', ['so_id' => $invoice->so_id, 'id' => $invoice->id]) . "' class='ml-3 btn btn-danger mr-2' ><i class='fa fa-angle-double-left'></i> </a>";
                            }
                        }
                    }
                }else{
                    $loading_btn = "<button class ='btn btn-sm btn-info'>Items Loading</button>";
                }


                $data[$i] = array(
                    $invoice->invoice_number,
                    Carbon::parse($invoice->created_at)->format('Y-m-d H:m:s'),
                    $invoice->salesOrder->template->category->name,
                    $invoice->salesOrder->template->day,
                    $invoice->salesOrder->template->time,
                    $invoice->salesOrder->salesRep->territory,
                    $rep->name_with_initials,
                    $shop_item_load . $reverse_btn,
                    $btn3 . $btn1 . $btn2 . $delete_btn  . $loading_btn . $crBtn . $rvBtn
                );
                $i++;
            }


        } elseif (is_null($search) || empty($search)) {
            $invoices = $invoices->get();
            $invoiceCount = LoadingHeader::whereStatus(1)->count();
        } else {
            $invoices = $invoices->searchData($search)->get();

            $invoiceCount = $invoices->count();
        }




        if ($invoiceCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($invoiceCount),
            "recordsFiltered" => intval($invoiceCount),
            "data" => $data
        ];

        return json_encode($json_data);
    }

    public function revers(Request $request)
    {
        $rvs = SalesOrder::find($request->so_id);

        $loadi = LoadingData::where('loading_header_id','=',$request->id)->where('status', '=', 1)->first();
        $loadv = LoadingDataVariant::where('loading_header_id','=',$request->id)->where('status', '=', 1)->first();
        if (empty($loadi) && empty($loadv)) {
            $rvs->status = 1;
            $rvs->save();

            return redirect()->back()->with([
                'success' => true,
                'success.title' => 'Updated !',
                'success.message' => 'Reversed Successfully!',
            ]);
        }else{
            return redirect()->back()->with([
                'success' => true,
                'success.title' => 'Something Went Wrong !',
                'success.message' => 'Already Loaded Invoice',
            ]);

        }
    }



    public function withDiscount(Request $request)
    {
//        return $request->all();
        $end_product_commission = $request->end_product_commission;
        $end_product_commission_value = $request->end_product_commission_value;

        $end_product_qty = $request->end_qty;
        $variant_qty = $request->variant_qty;

        $variant_commission = $request->variant_commission;
        $variant_commission_value = $request->variant_commission_value;

        $end_product_item_discount = $request->end_product_item_discount;
        $end_product_item_discount_value = $request->end_product_item_discount_value;

        $variant_item_discount = $request->variant_item_discount;
        $variant_item_discount_value = $request->variant_item_discount_value;

        $order_id = $request->order;


        $this->resetRowCal($order_id);


        if (!empty($end_product_commission)) {
            foreach ($end_product_commission as $commission) {
                $c_v = $end_product_commission_value[$commission];
                $end_item = LoadingData::whereLoadingHeaderId($order_id)->whereEndProductId($commission)->first();
                $end_item->commission = $c_v;
                $end_item->save();
                $this->rowCal($end_item);
            }
        }

        if (!empty($variant_commission)) {
            foreach ($variant_commission as $commission) {
                $c_v = $variant_commission_value[$commission];
                $end_item = LoadingDataVariant::whereLoadingHeaderId($order_id)->whereVariantId($commission)->first();
                $end_item->commission = $c_v;
                $end_item->save();
                $this->rowCalVariant($end_item);
            }
        }

        if (!empty($end_product_item_discount)) {
            foreach ($end_product_item_discount as $key => $commission) {
                $c_v = $end_product_item_discount_value[$key] * $end_product_qty[$key];
                $end_item = LoadingData::whereLoadingHeaderId($order_id)->whereEndProductId($commission)->first();
                $end_item->unit_discount = $c_v;
                $end_item->save();
                $this->rowCal($end_item);
            }
        }


        if (!empty($variant_item_discount)) {
            foreach ($variant_item_discount as $key => $commission) {
                $c_v = $variant_item_discount_value[$key] * $variant_qty[$key];
                $end_item = LoadingDataVariant::whereLoadingHeaderId($order_id)->whereVariantId($commission)->first();
                $end_item->unit_discount = $c_v;
                $end_item->save();
                $this->rowCalVariant($end_item);
            }
        }


        $this->updateNetValue($order_id);
        $loading = LoadingHeader::find($order_id);
        return view('discount-bill', compact('loading'));
    }

    protected function rowCal(LoadingData $loadingData)
    {
        $loadingData->total = $loadingData->qty * $loadingData->unit_value;
        $loadingData->total_discount = $loadingData->commission + $loadingData->unit_discount;
        $loadingData->row_value = ($loadingData->unit_value * $loadingData->qty) - $loadingData->total_discount;
        $loadingData->save();

    }

    protected function rowCalVariant(LoadingDataVariant $loadingData)
    {
        $variant = Variant::find($loadingData->variant_id);
        $variant_end_product = EndProduct::find($variant->end_product_id);

        $loadingData->total = $loadingData->weight * $variant_end_product->distributor_price;
        $loadingData->total_discount = $loadingData->commission + $loadingData->unit_discount;
        $loadingData->row_value = ($variant_end_product->distributor_price * $loadingData->weight) - $loadingData->total_discount;
        $loadingData->save();
    }

    protected function resetRowCal($loading)
    {
        $items = LoadingData::where('loading_header_id', '=', $loading)->get();
        $variants = LoadingDataVariant::where('loading_header_id', '=', $loading)->get();

        if (!empty($items)) {
            foreach ($items as $item) {
                $item->commission = 0;
                $item->unit_discount = 0;
                $item->row_value = $item->total;
                $item->total = 0;
                $item->total_discount = 0;
                $item->save();

            }
        }
        if (!empty($variants)) {
            foreach ($variants as $item) {
                $item->commission = 0;
                $item->unit_discount = 0;
                $item->row_value = $item->total;
                $item->total = 0;
                $item->total_discount = 0;
                $item->save();

            }
        }


    }


    protected function updateNetValue($id)
    {
        $loading = LoadingHeader::find($id);
        if ($loading->status != 2 ) {
            $loading_items = LoadingData::whereLoadingHeaderId($id)->get();
            $loading_variants = LoadingDataVariant::whereLoadingHeaderId($id)->get();

            if($loading->status == 1 ) {
                $loading->status = 2;
                $loading->save();
            }
            $net = 0;
            $total = 0;
            $total_commission = 0;
            $total_unit_discount = 0;
            $total_discount = 0;

            foreach ($loading_items as $item) {
                $net = $net + $item->row_value;
                $total = $total + $item->total;
                $total_commission = $total_commission + $item->commission;
                $total_unit_discount = $total_unit_discount + $item->unit_discount;
            }
            foreach ($loading_variants as $item) {
                $net = $net + $item->row_value;
                $total = $total + $item->total;
                $total_commission = $total_commission + $item->commission;
                $total_unit_discount = $total_unit_discount + $item->unit_discount;
            }

            $total_discount = $total_discount + $total_unit_discount + $total_commission;


            $loading->total = $total;
           // $loading->net_amount = ($total == 0) ? $net : $total;
            $loading->net_amount = $net;
            $loading->commission = $total_commission;
            $loading->unit_discount = $total_unit_discount;
            $loading->total_discount = $total_discount;
            $loading->is_paid = 0;
            $loading->save();
            $salesOrder = SalesOrder::find($loading->so_id);

            // $isExists = LoadingStatusChanges::where('loading_header_id','=',$loading->id)->first();
            // if(empty($isExists))
            // {
            //   $loadingStatus = new LoadingStatusChanges();
            //   //change loading header->status = 2 and salesorder->status = 3
            //   $loadingStatus->loading_header_id = $loading->id;
            //   $loadingStatus->status = 2;
            //   $loadingStatus->agent_id = $salesOrder->created_by;
            //   $loadingStatus->total_amount = $total;
            //   $loadingStatus->save();
            // }

            $rtype = "App\LoadingHeader";
            $ext =AgentInquiry::where('reference','=',$loading->id)->where('reference_type','=',$rtype)->get();
            $catid = EndProductInquiry::where('desc_id', '=', 1)
                ->where('so_id', '=', $loading->id)
                ->first();

            $catname = Category::find($catid->category_id);

            if(empty($ext)) {


                $subdesc = $loading->invoice_number . '-' . $catname->name;
                $ref = SalesRep::find($loading->salesOrder->created_by);
                $this->recordOutstanding($ref, $loading->net_amount, 'Sales Order Loading', $loading->id, 'App\\LoadingHeader', $subdesc);
                StockTransaction::updateAgentBalance(1, $loading->salesOrder->created_by, $loading->net_amount);
            }else{
                foreach ($ext as $key => $item) {
                  //  dd($item);
                    $ref1 = SalesRep::find($item->agent_id);
                    $subdesc1 = 'REV-'.$item->sub_desc;
                    $this->recordOutstanding($ref1, (($item->amount)*(-1)), 'Reverse Sales Order Loading', $loading->id, 'App\\LoadingHeader', $subdesc1);
                    StockTransaction::updateAgentBalance(2, $item->agent_id, $item->amount);
                }


                $subdesc = $catname->name.'|'.$loading->invoice_number;
                $ref = SalesRep::find($loading->salesOrder->created_by);
                $this->recordOutstanding($ref, $loading->net_amount, 'Sales Order Loading', $loading->id, 'App\\LoadingHeader', $subdesc);
                StockTransaction::updateAgentBalance(1, $loading->salesOrder->created_by, $loading->net_amount);


            }
        } else {
            return redirect()->back()->with(['error' => true,
                'error.message' => ' Already Loaded Please Contact System Admin',
                'error.title' => 'Sorry!']);
        }
    }

    public function printDiscount(Request $request)
    {

       $loadingHeaderId = $request->id;
        $filename = $loadingHeaderId . '.pdf';

        $loading = LoadingHeader::find($loadingHeaderId);

        return $this->getPDfInvoice($filename);
        /*
                $endProducts = EndProduct::all();
                $so = LoadingHeader::whereId($request->id)->first();
                $salesOrder = SalesOrder::find($request->so);

                $category = $salesOrder->template->category_id;
                $agent_id = $salesOrder->template->agent_id;
                $agentCategory = AgentCategory::where('sales_ref_id', '=', $agent_id)->where('category_id', '=', $category)->first();




                //        dd($agentCategory->discount_mode);
                if (empty($agentCategory)) {
                    $discountMode = 0;
                } else {
                    $discountMode = $agentCategory->discount_mode;
                }*/

     //   return redirect::route('close');

      /*  return view('SalesOrderManager::so.show')->with([
            'order' => $so, 'endProducts' => $endProducts, 'discountMode' => $discountMode
        ]);*/

//        if ($loading->status == 2) {
//            return redirect(route('loading.stable')) -> with([
//                    'error' => true,
//                    'error.title' => 'Sorry !',
//                    'error.message' => 'Invoice Already Generated'
//                ]);
//        }






    }




    public function delete(Request $request)
    {
        $loading = LoadingHeader::find($request->id);
        $loading->delete();
        return redirect()->back()->with([
            'success' => true,
            'success.title' => 'Deleted !',
            'success.message' => 'Deleted Successfully!',
        ]);
    }

    public function salesVsRerurn(Request $request)
    {
        $categories = Category::all()->pluck('name', 'id');
        $agents = SalesRep::all()->pluck('name_with_initials', 'id');
        return view('loading.sales-return-data', compact('categories', 'agents'));
    }

    public function SalesReturnData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
       // $order_by_str = $order_by[0]['dir'];

        $invoiceCount = 1;
        $data = [];

        if ($request->filter == true) {
            $agent = $request->agent;
            $category = $request->category;
            $date_range = $request->date_range;
            $dtt = $request->dtt;


            $desc1 = $request->desc1;


            $i = 0;
            $totqty = 0;
            $totSR   =  0;
            $totMR   =  0;
            $totMRAmount = 0;
            $soldAmo = 0;

                $endP = EndProduct::where('category_id','=',$category)->where('status','!=',0)->get();

                foreach ($endP as $endPro) {


                    $type = null;
                    $id1 = 1;

                    $ev = Variant::where('end_product_id','=',$endPro->id)->where('return_accepted','=',1)->first();
                   if($dtt == 2){
                       if (empty($ev)) {
                           $ItemData = EndProductInquiry::select(DB::raw("SUM(qty) as qty"), DB::raw("SUM(amount) as amount"))->where('end_product_id', '=', $endPro->id)->whereIn('desc_id', [1, 3])->TargetData($date_range, $agent, $desc1, $id1)->first();

                       } else {
                           $ItemData = EndProductInquiry::select(DB::raw("SUM(qty) as qty"), DB::raw("SUM(amount) as amount"))->where('end_product_id', '=', $endPro->id)->whereIn('desc_id', [2, 4])->TargetData($date_range, $agent, $desc1, $id1)->first();

                       }
                   }else {
                       if (empty($ev)) {
                           $ItemData = EndProductInquiry::select(DB::raw("SUM(qty) as qty"), DB::raw("SUM(amount) as amount"))->where('end_product_id', '=', $endPro->id)->whereIn('desc_id', [1, 3])->TargetData($date_range, $agent, $desc1, null)->first();

                       } else {
                           $ItemData = EndProductInquiry::select(DB::raw("SUM(qty) as qty"), DB::raw("SUM(amount) as amount"))->where('end_product_id', '=', $endPro->id)->whereIn('desc_id', [2, 4])->TargetData($date_range, $agent, $desc1, null)->first();

                       }
                   }




                    $ItemSR = EndProductInquiry::select(DB::raw("SUM(qty) as SRqty"),DB::raw("SUM(amount) as SRamount"))->where('end_product_id', '=', $endPro->id)->where('desc_id', '=', 12)->TargetData($date_range,$agent, $desc1 , $id1)->first();


                    $ItemMR = SalesReturnData::select(DB::raw("SUM(qty) as MRqty"),DB::raw("SUM(total) as MRamount"))->where('end_product_id', '=', $endPro->id)->where('is_system_generated', '!=', 1)->filterDataReturn(null,$agent,null,$date_range ,$desc1 )->first();

                    if(empty($ItemData->qty)){
                        $qty = 0;
                    }else{
                        $qty = $ItemData->qty;
                    }


                    if(empty($ev)) {
                        if (empty($ItemSR->SRqty)) {
                            $SRqty = 0;
                        } else {
                            $SRqty = $ItemSR->SRqty;
                        }
                    }else{
                        if (empty($ItemSR->SRqty)) {
                            $SRqty = 0;
                        } else {
                            $SRqty = (($ItemSR->SRqty) * ($ev->size));
                        }
                    }

                    if(empty($ev)) {
                        if (empty($ItemMR->MRqty)) {
                            $MRqty = 0;
                        } else {
                            $MRqty = $ItemMR->MRqty;
                        }
                    }else{
                        if (empty($ItemMR->MRqty)) {
                            $MRqty = 0;
                        } else {
                            $MRqty = (($ItemMR->MRqty) * ($ev->size));
                        }
                    }

                    $totqty = $totqty + $qty ;
                   $totSR   =  $totSR + $SRqty;
                    $totMR   =  $totMR + $MRqty;
                    $soldAmo1 = (($ItemData->amount * (-1)) - $ItemSR->SRamount );
                   $soldAmo = $soldAmo+ $soldAmo1;
                    $totMRAmount = $totMRAmount + $ItemMR->MRamount;




                    $data[$i] = array(
                       $endPro->name,
                       ($endPro->is_fractional == 1 ) ? number_format((($qty) * (-1)),3) : (($qty) * (-1)),
                       ($endPro->is_fractional == 1 ) ? number_format($SRqty,3) : $SRqty,
                       ($endPro->is_fractional == 1 ) ? number_format(((($qty) * (-1)) - $SRqty),3) : ((($qty) * (-1)) - $SRqty),
                       number_format(($soldAmo1),2),
                       ($endPro->is_fractional == 1 ) ? number_format($MRqty,3) : $MRqty,
                        number_format(($ItemMR->MRamount),2),
                        ($soldAmo1 != 0) ? number_format((($ItemMR->MRamount)*100/($soldAmo1)),2).'%' : 0,
                       null



                    );
                    $i++;

                    }


            $data[$i] = array(
                'Total',
                number_format(($totqty * (-1)),3),
                number_format(($totSR),3),
                number_format(($totqty * (-1))-($totSR),3),
                number_format(($soldAmo),2),
                number_format(($totMR),3),
                number_format(($totMRAmount),2),
                ($soldAmo != 0) ?number_format((($totMRAmount)*100/($soldAmo)),2).'%': 0,
                null

            );

        } elseif (is_null($search) || empty($search)) {

        } else {

        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($invoiceCount),
            "recordsFiltered" => intval($invoiceCount),
            "data" => $data
        ];

        return json_encode($json_data);
    }


}
