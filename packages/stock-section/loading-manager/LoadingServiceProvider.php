<?php

namespace LoadingManager;

use Illuminate\Support\ServiceProvider;

class LoadingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/Views', 'LoadingManager');
        require __DIR__ . '/web.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('LoadingManager', function($app){
            return new LoadingManager;
        });
    }
}
