<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */


Route::middleware(['web'])->group(function () {
    Route::prefix('loading')->group(function () {
        Route::get('', 'LoadingManager\Controllers\LoadingController@index')->name('loading.index');

        Route::get('list/all', 'LoadingManager\Controllers\LoadingController@allLoaded')->name('loading.all');

        Route::get('create', 'LoadingManager\Controllers\LoadingController@create')->name('loading.create');

        Route::get('{loading}', 'LoadingManager\Controllers\LoadingController@show')->name('loading.show');
        Route::get('{loading}/preview', 'LoadingManager\Controllers\LoadingController@preview')->name('loading.preview');

        Route::get('{loading}/edit', 'LoadingManager\Controllers\LoadingController@edit')->name('loading.edit');

        Route::get('debug/test', 'LoadingManager\Controllers\LoadingController@debug')->name('loading.debug');


        Route::post('', 'LoadingManager\Controllers\LoadingController@store')->name('loading.store');

        Route::post('{loading}', 'LoadingManager\Controllers\LoadingController@update')->name('loading.update');

        Route::delete('{loading}', 'LoadingManager\Controllers\LoadingController@destroy')->name('loading.destroy');

        Route::post('{loading}/pdf', 'LoadingManager\Controllers\LoadingController@pdf')->name('loading.pdf');

        Route::get('item/loading', 'LoadingManager\Controllers\LoadingController@loadItem')->name('loading.item');
        Route::get('item/loading/variant', 'LoadingManager\Controllers\LoadingController@loadItemVariant')->name('loading.item');
        Route::get('item/loading/check/{loadingHeader}', 'LoadingManager\Controllers\LoadingController@checkALLOrderItemStatus')->name('loading.check');
        Route::get('item/loading/raw', 'LoadingManager\Controllers\LoadingController@loadItemRaw')->name('loading.item');

        Route::get('item/un-loading', 'LoadingManager\Controllers\LoadingController@unloadItem')->name('loading.remove');
        Route::get('item/un-loading/raw', 'LoadingManager\Controllers\LoadingController@unloadItemRaw')->name('loading.remove');
        Route::get('item/un-loading/variant', 'LoadingManager\Controllers\LoadingController@unloadItemVariant')->name('loading.remove');

        //loading with discount route
        Route::get('loading/discount/{loding}', 'LoadingManager\Controllers\LoadingController@loadWithDiscount')->name('loading.discount');
        Route::get('loading/discountx/{loading}', 'LoadingManager\Controllers\LoadingController@loadWithDiscountX')->name('loading.discountx');

        Route::get('invoice/{filename}', function ($filename) {
            $filePath = storage_path('invoices/' . $filename);
            if (!File::exists($filePath)) {
                return Response::make("File does not exist.", 404);
            }
            $fileContents = File::get($filePath);
            return Response::make($fileContents, 200, array('Content-Type' => 'application/pdf'));
        });
    });
            Route::get('loading/show/{loading}', 'LoadingManager\Controllers\LoadingController@show')->name('loading.showLoading');

            Route::get('delete/loading/invoice/{id}', 'LoadingManager\Controllers\LoadingController@delete')->name('delete.loading');
});
