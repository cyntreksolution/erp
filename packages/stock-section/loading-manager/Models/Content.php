<?php

namespace LoadingManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Content extends Model
{
    use SoftDeletes;

    protected $table = 'purchasing_order_data';

    protected $dates = ['deleted_at'];

    public function purchasingOrder(){
        return $this->belongsTo('RecipeManager\Models\Recipe');
    }

    public function ingredients(){
        return $this->hasOne('RawMaterialManager\Models\RawMaterial','raw_material_id','raw_material_id')->withoutGlobalScopes();
    }
}
