<?php

namespace LoadingManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LoadingStatusChanges extends Model
{
    use SoftDeletes;

    protected $table = 'loading_status_changes';

    protected $dates = ['deleted_at'];

}
