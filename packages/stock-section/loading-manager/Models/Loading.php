<?php

namespace LoadingManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Loading extends Model
{
    use SoftDeletes;

    protected $table = 'purchasing_order_header';

    protected $dates = ['deleted_at'];

    public function supplier()
    {
        return $this->belongsTo('SupplierManager\Models\Supplier');
    }

    public function purchasingOrderContent(){
        return $this->hasMany('PurchasingOrderManager\Models\Content');
    }

}
