<html>
<head>
    <link href="/var/www/portal/public/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/var/www/portal/public/assets/css/invoice.css" rel="stylesheet">
    <style media="all">
        @page {
            margin: 30px 5px 30px 5px;
            size: 75mm 999mm;
        }
    </style>
</head>

<body style="margin-bottom: 100px">

<div class="row">

    <div class="col-xs-3">
        <img src="/var/www/portal/public/assets/img/bl.jpeg" style="width: 95px">
    </div>

    <div class="col-xs-6 text-right">
        <h6>
            <b>Ranjanas Holdings [Pvt] Ltd</b><br>
            Katukurunda<br>
            Habaraduwa<br>
            Hotline : +9477 330 4678<br>
        </h6>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-xs-3 text-nowrap">Sold Agent</div>
            <div class="col-xs-9">
                :{{$invoice->salesOrder->salesRep->type.' '.$invoice->salesOrder->salesRep->name_with_initials}}</div>
        </div>
        <div class="row">
            <div class="col-xs-3 text-nowrap">Territory</div>
            <div class="col-xs-9">:{{$invoice->salesOrder->salesRep->territory}}</div>
        </div>
        <div class="row">
            <div class="col-xs-3 text-nowrap">Invoice Number</div>
            <div class="col-xs-9">:{{$invoice->invoice_number}}</div>
        </div>
        <div class="row">
            <div class="col-xs-3 text-nowrap">Date</div>
            <div class="col-xs-9">:{{\Carbon\Carbon::now()->format('d-m-Y H:m')}}</div>
        </div>
        <div class="row">
            <div class="col-xs-3 text-nowrap">Time</div>
            <div class="col-xs-9">
                :{{$invoice->salesOrder->template->day.' '.$invoice->salesOrder->template->time}}</div>
        </div>
        <div class="row">
            <div class="col-xs-3 text-nowrap">Category</div>
            <div class="col-xs-9">:{{$invoice->salesOrder->template->category->name}}</div>
        </div>

    </div>
</div>
<br>


<div class="row">
    <div class="col-xs-12">
        <table class="table">
            <thead>
            <tr>
                <th class="col-xs-6">Item with price</th>
                <th class="col-xs-3 text-center">QTY</th>
                <th class="col-xs-3 text-right">Value</th>
            </tr>
            </thead>
            <tbody>
            @php $counter= 0;
            $totqty = 0;
            $totvqty = 0;

            @endphp
            @foreach($invoice->items as $key=> $item)
                @if ($item->qty != 0)
                    <tr>
                        <td>{{$item->endProduct->name}} [{{number_format((float)$item->unit_value, 2, '.', '')}}]</td>
                        <td class="text-center">{{$item->qty}}</td>
                        <td class="text-right">{{number_format((float)$item->unit_value*$item->qty, 2, '.', '')}}</td>
                    </tr>
                    @php  $counter = $counter+1 ;
                    $totqty = $totqty + $item->qty;
                    @endphp
                @endif
            @endforeach
            @foreach($invoice->variants as $key=> $item)
                @if ($item->weight != 0)
                    <tr>
                        <td>{{$item->variant->product->name}}
                            [{{number_format((float)$item->variant->product->selling_price, 2, '.', '')}}]
                            {{$item->variant->variant.' - '.$item->qty}}</td>
                        <td class="text-center">{{$item->weight}}</td>
                        <td class="text-right">{{number_format((float)$item->variant->product->selling_price*$item->weight, 2, '.', '')}}</td>
                    </tr>
                    @php  $counter = $counter+1 ;
                    $totvqty = $totvqty + $item->weight;
                    @endphp
                @endif
            @endforeach
            </tbody>
        </table>
    </div>

</div>
<div class="row">
    <div class="col-xs-12 pull-right text-right">
        <h4> Gross Bill Amount : {{number_format((float)$invoice->total, 2, '.', '')}} </h4>
        <h4> Discounts : {{number_format((float)$invoice->total_discount, 2, '.', '')}} </h4>
        <h4> Net Bill Amount : {{number_format((float)$invoice->total - $invoice->total_discount, 2, '.', '')}} </h4>
        <h4> Number Of Items : {{$counter}} </h4>
        <h4> Number Of Qty : {{$totqty}} </h4>
        <h4> Number Of Variants : {{number_format((float)$totvqty,3)}} </h4>
        <h4> Total Qty : {{number_format((float)(($totqty)+($totvqty)),3)}} </h4>

    </div>
</div>
<br>
{{--@if(!empty($invoice->crate_sent))--}}
{{--    <div class="row">--}}
{{--        <div class="col-xs-12">--}}
{{--            <table class="table">--}}
{{--                <thead>--}}
{{--                <tr>--}}
{{--                    <th>Crates</th>--}}
{{--                    <th>QTY</th>--}}
{{--                </tr>--}}
{{--                </thead>--}}
{{--                <tbody>--}}
{{--                @php $creats= json_decode($invoice->crate_sent); @endphp--}}
{{--                @foreach($creats as $creat)--}}
{{--                    @if($creat->qty == Null)--}}
{{--                        <tr>--}}
{{--                            <td>{{\RawMaterialManager\Models\RawMaterial::withoutGlobalScope('type')->whereRawMaterialId($creat->crate_id)->first()->name}}</td>--}}
{{--                            <td>{{0}}</td>--}}
{{--                        </tr>--}}
{{--                    @else--}}
{{--                        <tr>--}}
{{--                            <td>{{\RawMaterialManager\Models\RawMaterial::withoutGlobalScope('type')->whereRawMaterialId($creat->crate_id)->first()->name}}</td>--}}
{{--                            <td>{{$creat->qty}}</td>--}}
{{--                        </tr>--}}
{{--                    @endif--}}
{{--                @endforeach--}}
{{--                </tbody>--}}
{{--            </table>--}}
{{--        </div>--}}

{{--    </div>--}}
{{--@endif--}}
@if(!empty($invoice->extra_description))
    <div class="row">
        <div class="col-xs-12">
            Extra Description : {{$invoice->extra_description}}
        </div>
    </div>
@endif
<div class="row">
    <div class="col-xs-12 pull-right mt-5 text-right">
        {{$invoice->salesOrder->description}}
    </div>
</div>


@php

    $shopitem = \App\LoadingDataRaw::where('loading_header_id','=',$invoice->id)->get();
    $si = \App\LoadingDataRaw::where('loading_header_id','=',$invoice->id)->first();


@endphp
@if(!empty($si))
<div class="row">
    <div class="col-xs-12">
        <table class="table">
            <thead>
            <tr>
                <th>Shop Item[price]</th>
                <th class="text-center">QTY</th>
                <th class="text-right">Value</th>
            </tr>
            </thead>
            <tbody>
            @php $count_shop= 0;
            $shopval = 0;
            $shopqty = 0;
            @endphp
            @foreach($shopitem as $key=> $item)
               @php
               $raw = \RawMaterialManager\Models\RawMaterial::where('raw_material_id','=',$item->raw_material_id)->first();
                $unt = (($raw->price)/($raw->units_per_packs));
               @endphp
                    <tr>
                        <td>{{$raw->name}} [{{number_format((float)$unt, 2, '.', '')}}]</td>
                        <td class="text-center">{{$item->qty}}</td>
                        <td class="text-right">{{number_format((float)$unt*$item->qty, 2, '.', '')}}</td>
                    </tr>
                    @php
                        $count_shop = $count_shop+1 ;
                        $shopval = $shopval + $unt*$item->qty;
                        $shopqty = $shopqty + $item->qty;

                    @endphp

            @endforeach

            <tr>
                <td>Total Item(s) : [{{number_format((int)$count_shop)}}]</td>
                <td class="text-center">{{number_format((int)$shopqty)}}</td>
                <td class="text-right">{{number_format((float)$shopval,2)}}</td>
            </tr>

            </tbody>
        </table>
    </div>

</div>

@endif

<footer >
    <div class="row text-center center-block" style="margin-top: 20px;">
        <h4 class="text-center center-block">
            <h6 class="text-muted">Powered by Cyntrek Solutions<br>
                <h7 class="num">+9471 122 5489 / +9471 532 2262</h7>
            </h6>
        </h4>
    </div>
</footer>

</body>

</html>
