<html>
<head>
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/invoice.css')}}" rel="stylesheet">
    <style media="all">
        @page {
            margin: 30px 5px 30px 5px;
            size: 75mm 999mm;
        }
    </style>
</head>

<body style="margin-bottom: 100px">

{{--<div class="row">--}}

{{--    <div class="col-xs-3">--}}
{{--        <img src="{{asset('assets/img/buntak_grace.jpeg')}}" style="width: 100px">--}}
{{--    </div>--}}

{{--    <div class="col-xs-6 text-right">--}}
{{--        <h6>--}}
{{--            <b>Ranjanas Holdings [Pvt] Ltd</b><br>--}}
{{--            Katukurunda<br>--}}
{{--            Habaraduwa<br>--}}
{{--            Hotline : +9477 330 4678<br>--}}
{{--        </h6>--}}
{{--    </div>--}}
{{--</div>--}}
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-xs-3 text-nowrap">Sold Agent</div>
            <div class="col-xs-9">
                :{{$invoice->salesOrder->salesRep->type.' '.$invoice->salesOrder->salesRep->name_with_initials}}</div>
        </div>
        <div class="row">
            <div class="col-xs-3 text-nowrap">Territory</div>
            <div class="col-xs-9">:{{$invoice->salesOrder->salesRep->territory}}</div>
        </div>
        <div class="row">
            <div class="col-xs-3 text-nowrap">Invoice Number</div>
            <div class="col-xs-9">:{{$invoice->invoice_number}}</div>
        </div>
        <div class="row">
            <div class="col-xs-3 text-nowrap">Date</div>
            <div class="col-xs-9">:{{\Carbon\Carbon::now()->format('d-m-Y H:m')}}</div>
        </div>
        <div class="row">
            <div class="col-xs-3 text-nowrap">Time</div>
            <div class="col-xs-9">
                :{{$invoice->salesOrder->template->day.' '.$invoice->salesOrder->template->time}}</div>
        </div>
        <div class="row">
            <div class="col-xs-3 text-nowrap">Category</div>
            <div class="col-xs-9">:{{$invoice->salesOrder->template->category->name}}</div>
        </div>

    </div>
</div>
<br>


<div class="row">
    <div class="col-xs-12">
        <table class="table">
            <thead>
            <tr>
                <th>Item</th>
                <th class="text-center">QTY</th>
                <th>Return %</th>
            </tr>
            </thead>
            <tbody>
            @foreach($invoice->items as $key=> $item)
                @if (($item->requested_qty - $item->agent_available_qty + $item->extra_qty) != 0)
                    <tr>
                        <td>{{$item->endProduct->name}} </td>
                        <td class="text-center">{{(($item->requested_qty - $item->agent_available_qty)+$item->extra_qty)}}</td>
                        @php
                            $stat  = \App\SalesReturnData::where('end_product_id','=',$item->endProduct->id)->whereHas('header', function ($q) use ($invoice) {
                                $q->where('agent_id','=',$invoice->salesOrder->salesRep->id);
                            })->latest()->first();

                            $stat_val = !empty($stat)?$stat->return_stat:0;

                        @endphp
                        <td>{{number_format($stat_val,3)}} %</td>

                    </tr>
                @endif
            @endforeach
            @if(!empty($invoice->salesOrder->salesOrderVariants))
                @foreach($invoice->salesOrder->salesOrderVariants as $key=> $item)
                    @if (($item->qty - $item->available_qty + $item->extra_qty) != 0)
                        <tr>
                            <td>{{$item->variant->product->name.' '.$item->variant->variant}} </td>
                            <td class="text-center">{{(($item->qty - $item->available_qty)+$item->extra_qty)}}</td>
                            @php
                                $stat  = \App\SalesReturnData::where('end_product_id','=',$item->variant->product->id)->whereHas('header', function ($q) use ($invoice) {
                                    $q->where('agent_id','=',$invoice->salesOrder->salesRep->id);
                                })->latest()->first();

                                $stat_val = !empty($stat)?$stat->return_stat:0;

                            @endphp
                            <td>{{number_format($stat_val,3)}} %</td>
                        </tr>
                    @endif
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
</div>
@if(!empty($invoice->rawItems))
    <div class="row">
        <h6 class="text-center -underline"> Shop Items </h6>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <table class="table">
                <tbody>
                @foreach($invoice->rawItems as $key=> $item)
                    <tr>
                        <td>{{$item->rawMaterial->name}} </td>
                        <td class="text-center">{{($item->qty)}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endif
{{--<div class="row">--}}
{{--    <div class="col-xs-12 pull-right text-right">--}}
{{--        <h4> Gross Bill Amount : {{number_format((float)$invoice->total, 2, '.', '')}} </h4>--}}
{{--        <h4> Discounts : 00.00 </h4>--}}
{{--        <h4> Net Bill Amount : {{number_format((float)$invoice->total, 2, '.', '')}} </h4>--}}
{{--        <h4> Number Of Items : {{sizeof($invoice->items) + sizeof($invoice->salesOrder->salesOrderVariants)}} </h4>--}}
{{--    </div>--}}
{{--</div>--}}
{{--<div class="col-md-12">--}}
{{--    <div class="row">--}}
{{--        @if ($invoice->salesOrder->salesOrderVariants)--}}
{{--            <div class="row">--}}
{{--                @foreach($invoice->salesOrder->salesOrderVariants as $variant)--}}
{{--                    <div class="col-md-2">#</div>--}}
{{--                    <div class="col-md-2"> {{$variant->variant->product->name}}</div>--}}
{{--                    <div class="col-md-2">{{$variant->variant->variant}}</div>--}}
{{--                    <div class="col-md-2">Extra QTY : {{$variant->qty}}</div>--}}
{{--                    <div class="col-md-2">Available QTY : {{$variant->available_qty}}</div>--}}
{{--                @endforeach--}}
{{--            </div>--}}
{{--        @endif--}}
{{--    </div>--}}
{{--</div>--}}
<div class="row">
    <div class="col-xs-12 pull-right mt-5 text-right">
        {{$invoice->salesOrder->description}}
    </div>
</div>


<footer>
    <div class="row" style="margin-top: 20px;">
        <h4 align="center">
            <h6 class="text-muted">Powered by Cyntrek Solutions<br>
                <h7 class="num">+9471 122 5489 / +9471 532 2262</h7>
            </h6>
        </h4>
    </div>

    <p style="margin-top: 150px">--------------------------------------------------------------</p>
</footer>

</body>

</html>
