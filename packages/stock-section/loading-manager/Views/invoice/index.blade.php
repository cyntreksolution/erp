<html>
<head>
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/invoice.css')}}" rel="stylesheet">
    <style media="all">
        @page { margin:5px}
    </style>
</head>

<body>

<div class="row">
    <div class="col-md-6 text-right">
        <img src="{{asset('assets/img/buntak_grace.jpeg')}}" style="width: 100px">
        <h4>
            <b>Ranjanas[Pvt]Ltd</b><br>
            No.357 <br>
            Mathara Road<br>
            Habaraduwa<br>
            Mobile : +9477 330 4678
        </h4>
    </div>
    <div class="col-md-12">
        <h4>Sold Agent
            :<br><b>{{$invoice->salesOrder->salesRep->first_name.' '.$invoice->salesOrder->salesRep->last_name}}</b>
            <br>
        </h4>
        <p>Date : {{$invoice->salesOrder->template->date}}</p>
        <p>Time :{{$invoice->salesOrder->template->time}}</p>
        <p>Category :{{\EndProductManager\Models\Category::find($invoice->salesOrder->template->category)->first()->name}}</p>
    </div>
</div>
<br>
<div class="row">
    <div class="text-left col-xs-2">
        <h5>INVOICE NUMBER</h5>
        <h5>DATE OF INVOICE</h5>
        {{--<h5>DUE DATE : {{$item->due_date}}</h5>--}}
    </div>
    <div class="text-left col-xs-2">
        <h5>: {{$invoice->id}}</h5>
        <h5>: {{\Carbon\Carbon::parse($invoice->created_at)->format('Y M d')}}</h5>
        {{--<h5>DUE DATE : {{$item->due_date}}</h5>--}}
    </div>
    <div class="col-xs-4 pull-right text-right">
        <h5>AMOUNT DUE (LKR)</h5>
        <h4>{{number_format((float)$invoice->total, 2, '.', '')}} LKR</h4>
        {{--<h5>DUE DATE : {{$item->due_date}}</h5>--}}
    </div>
</div>


<div class="row">
    <div class="col-xs-12">
        <table class="table" style="margin-top: 50px;">
            <thead class="thead-inverse">
            <tr style="background-color: #656872; color: white">
                <th>No</th>
                <th>Product</th>
                <th class="text-center">Unit value</th>
                <th class="text-center">Qty.</th>
                <th class="text-right">Line Total (LKR)</th>
            </tr>
            </thead>
            <tbody>
            @foreach($invoice->items as $item)
                <tr>
                    <th scope="row">1</th>
                    <td>{{$item->endProduct->name}}</td>
                    <td class="text-center">{{number_format((float)$item->unit_value, 2, '.', '')}} LKR</td>
                    <td class="text-center">{{$item->qty}}</td>
                    <td class="text-right">{{number_format((float)$item->unit_value*$item->qty, 2, '.', '')}} LKR </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

</div>

<div class="col-xs-4 pull-right text-right">
    <h4> Amount Due : {{number_format((float)$invoice->total, 2, '.', '')}} LKR</h4>
</div>

<div class="row" style="margin-top: 80px;">
    <div class="col-xs-3">
        <h5>
            .........................................<br>
            Signature-Authorized
        </h5>
    </div>


    <div class="col-xs-3 text-right pull-right">
        <h5>
            .........................................<br>
            Signature-Received
        </h5>
    </div>

</div>
<footer>
    <div class="row" style="margin-top: 20px;">
        <h4 align="center">
            <h6 class="text-muted">powered by cyntrek<br>
                <h7 class="num">+9471 122 5489 / +9471 532 2262</h7>
            </h6>
        </h4>

    </div>
</footer>
</body>

</html>
