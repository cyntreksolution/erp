@extends('layouts.back.master')@section('title','End Product Loading')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .hide {
            display: none;
        }
    </style>
@stop
@section('content')
    <div class="app-wrapper">

        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">Last Loadings</h5>
            </div>


            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">
                    <div class="container">
                        <div class="projects-list">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Date</th>
                                    <th>Agent</th>
                                    <td class="text-right">Item Count</td>
                                    <th class="text-right">Value</th>
                                    <th class="text-right">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $key => $order)
                                    <tr>
                                        <td>{{++$key}}</td>
                                        <td>{{\Carbon\Carbon::parse($order->created_at)->format('Y M d')}}</td>
                                        <td>{{$order->salesOrder->salesRep->first_name.' '.$order->salesOrder->salesRep->last_name}}</td>
                                        <td class="text-center">{{sizeof($order->items)}}</td>
                                        <td class="text-right">{{number_format((float)$order->total, 2, '.', '')}}LKR
                                        </td>
                                        <td class="text-right">
                                            <a class="btn btn-info" target="_blank" href="/debug/{{$order->id}}">print</a>
                                            <button class="btn btn-info">view</button>
                                            <button class="btn btn-warning">compare</button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Date</th>
                                    <th>Agent</th>
                                    <th class="text-right">Item Count</th>
                                    <th class="text-right">Value</th>
                                    <th class="text-right">Action</th>
                                </tr>
                                </tfoot>
                            </table>
                            {{--                            {!! $orders->links() !!}--}}

                            {{--                            @foreach($orders as $order)--}}
                            {{--                                <a href="{{'so/'.$order->id}}">--}}
                            {{--                                    <div class="media">--}}
                            {{--                                        <div class="avatar avatar text-white avatar-md project-icon bg-primary"--}}
                            {{--                                             data-target="#project-1">--}}
                            {{--                                            <i class="fa fa-pause" aria-hidden="true"></i>--}}
                            {{--                                        </div>--}}
                            {{--                                        <div class="media-body">--}}
                            {{--                                            <h6 class="project-name"--}}
                            {{--                                                id="project-1">{{$order->salesRep->first_name.' '.$order->salesRep->last_name}}</h6>--}}
                            {{--                                            <small class="project-detail">{{\Carbon\Carbon::parse($order->created_at)->format('y-m-d')}}</small>--}}
                            {{--                                        </div>--}}
                            {{--                                        <button class="btn btn-outline-primary" style="width:6rem">Load Now</button>--}}
                            {{--                                    </div>--}}
                            {{--                                    <hr class="m-0">--}}
                            {{--                                </a>--}}
                            {{--                            @endforeach--}}
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $('.delete').click(function (e) {
            e.preventDefault();
            id = $(this).attr("value");
            confirmAlert(id);

        });

        function confirmAlert(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "delete",
                        url: id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            } else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

                @if(session('pdf'))
        var id = '{!! session('pdf.file') !!}'
        $.ajax({
            type: "POST",
            url: '/loading/' + id + '/pdf',
            success: function (response) {
                window.open(response);
                console.log(response);
                debugger
            }
        });
        @endif

    </script>

    <style>
        .pagination {
            display: inline-block;
        }

        .pagination a {
            color: black;
            float: left;
            padding: 8px 16px;
            text-decoration: none;
        }

        .pagination a.active {
            background-color: #4CAF50;
            color: white;
        }

        .pagination a:hover:not(.active) {
            background-color: #ddd;
        }
    </style>
@stop