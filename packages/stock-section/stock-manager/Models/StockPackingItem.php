<?php

namespace StockManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StockPackingItem extends Model
{
    use SoftDeletes;

    protected $table = 'stock_packing_item';

    protected $dates = ['deleted_at'];
}
