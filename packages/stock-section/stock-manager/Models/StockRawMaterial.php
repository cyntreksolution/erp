<?php

namespace StockManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StockRawMaterial extends Model
{
    use SoftDeletes;

    protected $table = 'stock_raw_material';

    protected $dates = ['deleted_at'];

    public function rawMaterials(){
        return $this->belongsTo('RawMaterialManager\Models\RawMaterial','raw_material_id','raw_material_id')->withoutGlobalScopes();
    }





}
