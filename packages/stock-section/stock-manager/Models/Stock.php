<?php

namespace StockManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stock extends Model
{
    use SoftDeletes;

    protected $table = 'stock';

    protected $primaryKey = 'stock_id';

    protected $dates = ['deleted_at'];
}
