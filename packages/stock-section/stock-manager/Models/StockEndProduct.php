<?php

namespace StockManager\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class StockEndProduct extends Model
{
    use SoftDeletes;

    protected $table = 'stock_end_product';

    protected $dates = ['deleted_at'];

    public function endProduct(){
        return $this->belongsTo('EndProductManager\Models\EndProduct','end_product_id','id');
    }

    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }

    public function scopeFilterData($query,$category=null,$date=null)
    {
        if (!empty($category)) {
            $query->whereHas('endProduct', function ($q) use ($category) {
                return $q->whereCategoryId($category);
            });
        }

        if (!empty($date)) {

            $query->Where('stock_end_product.created_at','like',$date.'%');

        }

    }



}
