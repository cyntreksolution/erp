<?php

namespace StockManager\Controllers;

use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Support\Facades\Auth;
use SchoolManager\Models\School;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use StockManager\Models\Stock;
use UserManager\Models\Users;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect(route('stocks.create'));
        return view('StockManager::stock.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $stock = Stock::all();
        return view('StockManager::stock.create')->with(['stocks'=>$stock]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $stock=new Stock();
        $stock->name=$request->name;
        $stock->address=$request->address;
        $stock->colour=rand() % 7;
        $stock->created_by=Auth::user()->id;
        $stock->save();
        return redirect(route('stocks.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \StockManager\Models\Stock $stock
     * @return \Illuminate\Http\Response
     */
    public function show(Stock $stock)
    {
        return view('StockManager::stock.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \StockManager\Models\Stock $stock
     * @return \Illuminate\Http\Response
     */
    public function edit(Stock $stock)
    {
        return view('StockManager::stock.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \StockManager\Models\Stock $stock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stock $stock)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \StockManager\Models\Stock $stock
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stock $stock)
    {
        $stock->delete();
        if ($stock->trashed()) {
            return true;
        }else{
            return false;
        }

    }
}
