@extends('layouts.back.master')@section('title','Schools')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }
    </style>
@stop
@section('content')
    <div class="app-wrapper">
        <div class="app-panel" id="app-panel">
            <div class="app-search"><input type="search" class="search-field" placeholder="Search">
                <i class="search-icon fa fa-search"></i>
            </div>
            <div class="app-panel-inner">
                <div class="scroll-container">
                    <div class="p-3">
                        <button class="btn btn-success py-3 btn-block btn-lg" data-toggle="modal"
                                data-target="#projects-task-modal">ADD NEW STOCK
                        </button>
                    </div>
                </div>
            </div>
            <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show"><i
                        class="fa fa-chevron-right"></i>
                <i class="fa fa-chevron-left"></i>
            </a>
        </div>
        <!-- /.app-panel -->
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">STOCK LIST</h5>
            </div><!-- /.app-main-header -->
            <div class="app-main-content">
                <div class="projects-list">
                    @foreach($stocks as $stock)
                        <a href="{{$stock->stock_id}}">
                            <div class="media">
                                @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                <div class="avatar avatar-circle avatar-md project-icon" data-plugin="firstLitter"
                                     data-target="#{{$stock->stock_id*754}}"></div>
                                <div class="media-body">
                                    <h6 class="project-name" id="{{$stock->stock_id*754}}">{{$stock->name}}</h6>
                                    <small class="project-detail">{{$stock->address}}</small>
                                </div>

                            </div>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="projects-task-modal" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'stock')) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="task-name-wrap">
                        <span><i class="zmdi zmdi-check"></i></span>
                        <input class="task-name-field" type="text" name="name" placeholder="Stock Name">
                    </div>
                    <hr>
                    <div class="task-desc-wrap">
                        <textarea class="task-desc-field" name="address" id="" placeholder="Address"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

@stop
@section('js')
@stop