<?php
/**
 * Created by PhpStorm.
 * User: ccdil
 * Date: 12/16/2017
 * Time: 8:04 AM
 */

namespace StockManager\Classes;


use EndProductManager\Models\EndProduct;
use PackingItemManager\Models\PackingItem;
use PurchasingOrderManager\Models\Content;
use RawMaterialManager\Models\RawMaterial;
use SemiFinishProductManager\Models\SemiFinishProduct;
use StockManager\Models\StockEndProduct;
use StockManager\Models\StockPackingItem;
use StockManager\Models\StockRawMaterial;
use StockManager\Models\StockSemiFinish;

class CStockValue
{
    public static function getLastQuantityRawMaterial($raw_material,$type)
    {
        $raw = StockRawMaterial::where('raw_material_id', '=', $raw_material)
            ->where('type', '=', 1)
            ->orderBy('created_at', 'desc')
            ->first();
        return $raw;
    }

//    public static function
}