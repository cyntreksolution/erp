<?php
/**
 * Created by PhpStorm.
 * User: ccdil
 * Date: 12/16/2017
 * Time: 8:04 AM
 */

namespace StockManager\Classes;


use App\AgentCrate;
use EndProductManager\Models\EndProduct;
use PackingItemManager\Models\PackingItem;
use PurchasingOrderManager\Models\Content;
use RawMaterialManager\Models\RawMaterial;
use SalesRepManager\Models\SalesRep;
use SemiFinishProductManager\Models\SemiFinishProduct;
use StockManager\Models\StockEndProduct;
use StockManager\Models\StockPackingItem;
use StockManager\Models\StockRawMaterial;
use StockManager\Models\StockSemiFinish;

use App\Crates;
use SupplierManager\Models\Supplier;

class StockTransaction
{

    public static function updateRawMaterialAvailableQty($type, $raw_material, $qty)
    {
        if ($type == 1) {
            $qty = $qty;
        } else {
            $qty = (($qty)*(-1));
        }
        $raw = RawMaterial::withoutGlobalScopes()->where('raw_material_id', '=', $raw_material)->first();
        $currant_stock = $raw->available_qty;
        $raw->available_qty = $currant_stock + $qty;
        $raw->save();

    }

    public static function rawMaterialStockTransaction($type, $price, $raw_material, $qty, $ref_no, $stock)
    {
        if ($type == 1) {
            $qty = $qty;
        } else {
            $qty = ($qty)*(-1);
        }

        $raw = StockRawMaterial::where('raw_material_id', '=', $raw_material)
                        ->where('ref_no','=',$ref_no)
                        ->where('type','=',2)
                        ->first();
        if(!empty($raw)){
            $raw->qty = $qty;
            $raw->price = $price;

            $raw->save();

        }else {

            $trans = new StockRawMaterial();
            $trans->stock_id = $stock;
            $trans->raw_material_id = $raw_material;
            $trans->qty = $qty;
            $trans->price = $price;
            $trans->type = $type;
            $trans->ref_no = $ref_no;
            $trans->save();
        }

    }

    public static function updatePackingItemAvailableQty($type, $packing_item, $qty)
    {
        if ($type == 1) {
            $qty = $qty;
        } else {
            $qty = -$qty;
        }
        $pack = PackingItem::where('id', '=', $packing_item)->first();
        $currant_stock = $pack->available_qty;
        $pack->available_qty = $currant_stock + $qty;
        $pack->save();

    }

    public static function PackingItemStockTransaction($type, $packing_item, $qty, $ref_no, $stock)
    {
        if ($type == 1) {
            $qty = $qty;
        } else {
            $qty = -$qty;
        }

        $trans = new StockPackingItem();
        $trans->stock_id = $stock;
        $trans->packing_item_id = $packing_item;
        $trans->qty = $qty;
        $trans->type = $type;
        $trans->ref_no = $ref_no;
        $trans->save();

    }


    public static function updateSemiFinishAvailableQty($type, $semi_finish, $qty)
    {
        if ($type == 1) {
            $qty = $qty;
        } else {
            $qty = -$qty;
        }

        $semi = SemiFinishProduct::find($semi_finish);
        $currant_stock = $semi->available_qty;
        $semi->available_qty = $currant_stock + $qty;
        $semi->save();

    }

    public static function semiFinishStockTransaction($type, $semi_finish, $grade, $qty, $ref_no, $stock)
    {
        if ($type == 1) {
            $qty = $qty;
        } else {
            $qty = ($qty)*(-1);
        }

        $trans = new StockSemiFinish();
        $trans->stock_id = $stock;
        $trans->semi_finish_product_id = $semi_finish;
        $trans->grade_id = $grade;
        $trans->qty = $qty;
        $trans->type = $type;
        $trans->ref_no = $ref_no;
        $trans->save();

    }

    public static function updateEndProductAvailableQty($type, $endProduct, $qty)
    {
        if ($type == 1) {
            $qty = $qty;
        } else {
            $qty = ($qty)*(-1);
        }
        $end = EndProduct::find($endProduct);
        $currant_stock = $end->available_qty;
        $end->available_qty = $currant_stock + $qty;
        $end->save();

    }

    public static function getLastRawMaterialSupplier($raw_material)
    {

        $raw = Content::where('raw_material_id', '=', $raw_material)
            ->orderBy('created_at', 'desc')
            ->first();

        return $raw;


    }

    public static function endProductStockTransaction($type, $end_product, $cost_price, $agent_price, $selling_price, $qty, $ref_no, $stock)
//    public static function endProductStockTransaction($type, $end_product,$team, $cost_price, $agent_price, $selling_price, $qty, $ref_no, $stock)
    {
        if ($type == 1) {
            $qty = $qty;
        } else {
            $qty = -$qty;
        }

        $trans = new StockEndProduct();
        $trans->stock_id = $stock;
        $trans->end_product_id = $end_product;
        $trans->cost_price = $cost_price;
        $trans->agent_price = $agent_price;
        $trans->selling_price = $selling_price;
        $trans->qty = $qty;
        $trans->team = 0;
        $trans->type = $type;
        $trans->ref_no = $ref_no;
        $trans->save();

    }

    //crates Functions

    public static function cratesTransaction($type, $raw_material, $qty, $ref_no, $agent_id)
    {
        if ($type == 1) {
            $qty = $qty;
        } else {
            $qty = -$qty;
        }

        $craw = Crates::where('raw_material_id','=',$raw_material)
                        ->where('ref_no','=',$ref_no)
                        ->where('type','=',$type)
                        ->first();

        if(!empty($craw)) {

            $craw->qty = $qty;
            $craw->save();
        }else{
            $trans = new Crates();
            $trans->raw_material_id = $raw_material;
            $trans->qty = $qty;
            $trans->type = $type;
            $trans->ref_no = $ref_no;
            $trans->agent_id = $agent_id;
            $trans->save();
        }

    }

    public static function agentsCratesTransaction($type, $agent_id, $raw_material, $qty)
    {
        if ($type == 1) {
            $qty = $qty;
        } else {
            $qty = -$qty;
        }

        $trans = AgentCrate::where('crate_id', '=', $raw_material)->where('agent_id', '=', $agent_id)->first();
        if (!empty($trans) && $trans->count() > 0) {
            $trans->balance = $trans->balance + $qty;
            $trans->save();
        } else {
            $trans = new Crates();
            $trans->crate_id = $raw_material;
            $trans->agent_id = $agent_id;
            $trans->balance = $qty;
            $trans->save();
        }

    }

    public static function updateAgentBalance($type, $agent_id, $value)
    {
        if ($type == 1) {
            $qty = $value;
        } else {
            $qty = -$value;
        }

        $agent = SalesRep::find($agent_id); //users table
        $agent->balance = $agent->balance + $qty;
        $agent->save();
    }

    public static function updateSupplierBalance($type, $agent_id, $value)
    {
        if ($type == 1) {
            $qty = $value;
        } else {
            $qty = -$value;
        }

        $agent = Supplier::find($agent_id);
        $agent->balance = $agent->balance + $qty;
        $agent->save();
    }


}
