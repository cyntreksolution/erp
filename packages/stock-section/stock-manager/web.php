<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web','log'])->group(function () {
    Route::prefix('stock')->group(function () {
        Route::get('', 'StockManager\Controllers\StockController@index')->name('stocks.index');

        Route::get('create', 'StockManager\Controllers\StockController@create')->name('stocks.create');

        Route::get('{stock}', 'StockManager\Controllers\StockController@show')->name('stocks.show');

        Route::get('{stock}/edit', 'StockManager\Controllers\StockController@edit')->name('stocks.edit');


        Route::post('', 'StockManager\Controllers\StockController@store')->name('stocks.store');

        Route::put('{stock}', 'StockManager\Controllers\StockController@update')->name('stocks.update');

        Route::delete('{stock}', 'StockManager\Controllers\StockController@destroy')->name('stocks.destroy');
    });
});
