<?php

namespace PackingPurchasingOrderManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use PackingItemManager\Models\PackingItem;

class PackingOrderContent extends Model
{
    use SoftDeletes;

    protected $table = 'packing_order_data';

    protected $dates = ['deleted_at'];


    public function packingOrder(){
        return $this->belongsTo(PackingOrder::class);
    }

    public function packingItem(){
        return $this->belongsTo(PackingItem::class);
    }
}
