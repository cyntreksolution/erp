<?php

namespace PackingPurchasingOrderManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PackingOrder extends Model
{
    use SoftDeletes;

    protected $table = 'packing_order_header';

    protected $dates = ['deleted_at'];

    public function supplier()
    {
        return $this->belongsTo('SupplierManager\Models\Supplier');
    }

    public function items(){
        return $this->hasMany(PackingOrderContent::class);
    }

}
