<?php

namespace PackingPurchasingOrderManager;

use Illuminate\Support\ServiceProvider;

class PackingPurchasingOrderServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/Views', 'PackingPurchasingOrderManager');
        require __DIR__ . '/web.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('PackingPurchasingOrderManager', function($app){
            return new PackingPurchasingOrderManager;
        });
    }
}
