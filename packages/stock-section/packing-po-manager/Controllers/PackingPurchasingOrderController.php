<?php

namespace PackingPurchasingOrderManager\Controllers;

use Carbon\Carbon;
use CashLockerManager\Classes\CashTransaction;
use CashLockerManager\Models\CashBookTransaction;
use CashLockerManager\Models\CashLocker;
use Illuminate\Support\Facades\Auth;
use PackingItemManager\Models\PackingItem;
use PackingPurchasingOrderManager\Models\PackingOrder;
use PackingPurchasingOrderManager\Models\PackingOrderContent;
use PassBookManager\Classes\BankTransaction;
use PassBookManager\Models\PassBook;
use PackingPurchasingOrderManager\Models\Content;
use Sentinel;
use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use PackingPurchasingOrderManager\Models\PurchasingOrder;
use RawMaterialManager\Models\RawMaterial;
use StockManager\Classes\StockTransaction;
use SupplierManager\Models\Supplier;

class PackingPurchasingOrderController extends Controller

{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $supliers = Supplier::orderBy('supplier_name')->take(4)->get();
        $pos = PackingOrder::with('supplier', 'items.packingItem')
            ->orderBy('status')
            ->get();
        return view('PackingPurchasingOrderManager::packing-po.index')->with([
            'supliers' => $supliers,
            'pos' => $pos
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = PackingItem::all();
        $supliers = Supplier::all();

        return view('PackingPurchasingOrderManager::packing-po.create')->with([
            'products' => $products,
            'supliers' => $supliers,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        return $request;
        $supplier_id = $request->supplier_id;
        $user = Auth::user()->id;
        $end_products = $request->ingredients;
        $qty = $request->qty;
        $price = $request->price;
        $date = date('Y-m-d');
        $total = null;
        try {
            DB::transaction(function () use ($supplier_id, $user, $end_products, $qty, $price, $total, $date) {
                $in = DB::select("show table status like 'packing_order_header'");
                $in = $in[0]->Auto_increment;
                $po = new PackingOrder();
                $po->invoice_no = date('ymd') . $in;
                $po->supplier_id = $supplier_id;
                $po->created_by = $user;
                $po->received_date = $date;
                $po->save();

                foreach ($end_products as $key => $end_product) {
                    $pod = new PackingOrderContent();
                    $pod->packing_order_id = $po->id;
                    $pod->packing_item_id = $end_product;
                    $pod->ordered_price = $price[$key];
                    $total = $total + ($price[$key] * $qty[$key]);
                    $pod->requested_qty = $qty[$key];
                    $pod->save();
                }
                $po->total = $total;
                $po->save();

            });

            return redirect(route('packing-po.index'))->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'New packing order has been added'
            ]);
        } catch (Exception $e) {
            return $e;
            return redirect(route('packing-po.index'))->with([
                'error' => true,
                'error.title' => 'Sorry !',
                'error.message' => 'Something Went Wrong '
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \RecipeManager\Models\Recipe $recipe
     * @return \Illuminate\Http\Response
     */
    public function show($purchasingOrder)
    {
        $po = PackingOrder::with('supplier', 'items.packingItem')
            ->where('id', $purchasingOrder)
            ->get();
        return view('PackingPurchasingOrderManager::packing-po.show')->with([
            'po' => $po,
        ]);
    }

    public function payment($purchasingOrder)
    {
//        return "jgvhghg";
        $bank = PassBook::all();
        $po = PackingOrder::with('supplier', 'items.packingItem')
            ->where('id', $purchasingOrder)
            ->get();
        return view('PackingPurchasingOrderManager::packing-po.payment')->with([
            'po' => $po,
            'accounts' => $bank
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \RecipeManager\Models\Recipe $recipe
     * @return \Illuminate\Http\Response
     */
    public function edit($purchasingOrder)
    {
        $raw_materials = RawMaterial::all();
        $po = PackingOrder::with('supplier', 'items.packingItem')
            ->where('id', $purchasingOrder)
            ->get();
        return view('PackingPurchasingOrderManager::packing-po.edit')->with([
            'po' => $po,
            'raw_materials' => $raw_materials,
            'po_id' => $purchasingOrder,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \RecipeManager\Models\Recipe $recipe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $po_id = $request->po_id;
        $pod_ids = $request->pd_ids;
        $raw_materials = $request->raw_materials;
        $received_qty = $request->receieved_qty;
        $received_price = $request->recieved_price;
        $date = date('Y-m-d');
        $new_raw = $request->new_raw;
        $new_qty = $request->new_qty;
        $new_price = $request->new_price;
        $total = null;

        try {
            DB::transaction(function () use ($total, $po_id, $pod_ids, $raw_materials, $received_qty, $received_price, $new_raw, $new_qty, $new_price, $date) {

                foreach ($pod_ids as $key => $pod_id) {
                    $pod = PackingOrderContent::where('id', '=', $pod_id)->first();
                    $pod->received_price = $received_price[$key];
                    $raw_material = PackingItem::where('id', '=', $raw_materials[$key])->first();
                    $qty = ($received_qty[$key]);
                    $pod->recieved_qty = $qty;
                    $pod->save();

                    $total = $total + ($received_price[$key] * $received_qty[$key]);

                    StockTransaction::PackingItemStockTransaction(1,$pod->packing_item_id, $qty,'', 1);
                    StockTransaction::updatePackingItemAvailableQty(1, $pod->packing_item_id, $qty);
                }


                $purchasingOrder = PackingOrder::find($po_id);
                $purchasingOrder->total = $total;
                $purchasingOrder->received_date = $date;
                $purchasingOrder->status = 2;
                $purchasingOrder->save();
            });

            return redirect(route('packing-po.index'))->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'New po has been recieved'
            ]);
        } catch (Exception $e) {
            return $e;
            return redirect(route('packing-po.index'))->with([
                'error' => true,
                'error.title' => 'Sorry !',
                'error.message' => 'Something Went Wrong '
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \RecipeManager\Models\Recipe $recipe
     * @return \Illuminate\Http\Response
     */
    public function destroy(PurchasingOrder $purchasingOrder)
    {
        $purchasingOrder->delete();
        if ($purchasingOrder->trashed()) {
            return 'true';
        } else {
            return 'false';
        }
    }

    public function getlastprice(Request $request)
    {
        $lastprice = Content::where('raw_material_id', '=', $request->id)
            ->where('received_price', '>', 0)
            ->orderBy('created_at', 'desc')
            ->first();
        return $lastprice;


    }

    public function dateReport()
    {

        $raw = RawMaterial::all();
        $sup = Supplier::all();
        return view('PackingPurchasingOrderManager::packing-po.date')->with([
            'raw_materials' => $raw,
            'suppliers' => $sup,

        ]);


    }

    public function getReport(Request $request)
    {
        $start_date = substr($request->date, 0, 10);
        $start_date = date_create($start_date);
        $start_date = date_format($start_date, "Y-m-d");

        $end_date = substr($request->date, 12, 23);
        $end_date = date_create($end_date);
        $end_date = date_format($end_date, "Y-m-d");

        $product = $request->product;
        if ($product == 0) {
            $burdens = Burden::whereBetween('created_at', array($start_date, $end_date))->with('semiProdcuts')->get();
            $array = [];
            foreach ($burdens as $burden) {
                $burdenx = [
                    'id' => $burden['id'],
                    'burden_size' => $burden['burden_size'],
                    'expected_semi_product_qty' => $burden['expected_semi_product_qty'],
                    'name' => $burden['semiProdcuts']->name,
                ];
                array_push($array, $burdenx);
            }
            return $array;

        } else {
            $burdens = Burden::whereBetween('created_at', array($start_date, $end_date))->with('semiProdcuts')
                ->where('semi_finish_product_id', '=', $product)->get();
            $array = [];
            foreach ($burdens as $burden) {
                $burdenx = [
                    'id' => $burden['id'],
                    'burden_size' => $burden['burden_size'],
                    'expected_semi_product_qty' => $burden['expected_semi_product_qty'],
                    'name' => $burden['semiProdcuts']->name,
                ];
                array_push($array, $burdenx);
            }
            return $array;
        }
    }

    public function pdfViewPO($id)
    {
        $data = DB::table('purchasing_order_header')
            ->select('supplier.supplier_name AS customer_name', 'supplier.*', 'raw_material.*', 'purchasing_order_header.*', 'purchasing_order_data.*')
            ->leftJoin('supplier', 'purchasing_order_header.supplier_id', '=', 'supplier.id')
            ->rightJoin('purchasing_order_data', 'purchasing_order_data.purchasing_order_id', '=', 'purchasing_order_header.id')
            ->rightJoin('raw_material', 'raw_material.raw_material_id', '=', 'purchasing_order_data.raw_material_id')
            ->where('purchasing_order_header.id', '=', $id)
            ->get();

        $iNo = PurchasingOrder::where('id', '=', $id)->first();
        $pdf = PDF::loadView('PackingPurchasingOrderManager::packing-po.po', compact('data'))->setPaper('a4', 'portrait');
        return $pdf->stream();
        return $pdf->download($iNo['invoice_no'] . '-PO.pdf');
    }

    public function pdfDownloadPO($id)
    {
        $data = DB::table('purchasing_order_header')
            ->select('supplier.supplier_name AS customer_name', 'supplier.*', 'raw_material.*', 'purchasing_order_header.*', 'purchasing_order_data.*')
            ->leftJoin('supplier', 'purchasing_order_header.supplier_id', '=', 'supplier.id')
            ->rightJoin('purchasing_order_data', 'purchasing_order_data.purchasing_order_id', '=', 'purchasing_order_header.id')
            ->rightJoin('raw_material', 'raw_material.raw_material_id', '=', 'purchasing_order_data.raw_material_id')
            ->where('purchasing_order_header.id', '=', $id)
            ->get();

        $iNo = PurchasingOrder::where('id', '=', $id)->first();
        $pdf = PDF::loadView('PackingPurchasingOrderManager::packing-po.po', compact('data'))->setPaper('a4', 'portrait');
        return $pdf->download($iNo['invoice_no'] . '-PO.pdf');
    }

    public function pdfViewGRN($id)
    {
        $data = DB::table('purchasing_order_header')
            ->select('supplier.supplier_name AS customer_name', 'supplier.*', 'raw_material.*', 'purchasing_order_header.*', 'purchasing_order_data.*')
            ->leftJoin('supplier', 'purchasing_order_header.supplier_id', '=', 'supplier.id')
            ->rightJoin('purchasing_order_data', 'purchasing_order_data.purchasing_order_id', '=', 'purchasing_order_header.id')
            ->rightJoin('raw_material', 'raw_material.raw_material_id', '=', 'purchasing_order_data.raw_material_id')
            ->where('purchasing_order_header.id', '=', $id)
            ->get();

        $iNo = PurchasingOrder::where('id', '=', $id)->first();
        $pdf = PDF::loadView('PackingPurchasingOrderManager::packing-po.grn', compact('data'))->setPaper('a4', 'portrait');
        return $pdf->stream();
        return $pdf->download($iNo['invoice_no'] . '-GRN.pdf');
    }

    public function pdfDownloadGRN($id)
    {
        $data = DB::table('purchasing_order_header')
            ->select('supplier.supplier_name AS customer_name', 'supplier.*', 'raw_material.*', 'purchasing_order_header.*', 'purchasing_order_data.*')
            ->leftJoin('supplier', 'purchasing_order_header.supplier_id', '=', 'supplier.id')
            ->rightJoin('purchasing_order_data', 'purchasing_order_data.purchasing_order_id', '=', 'purchasing_order_header.id')
            ->rightJoin('raw_material', 'raw_material.raw_material_id', '=', 'purchasing_order_data.raw_material_id')
            ->where('purchasing_order_header.id', '=', $id)
            ->get();

        $iNo = PurchasingOrder::where('id', '=', $id)->first();
        $pdf = PDF::loadView('PackingPurchasingOrderManager::packing-po.grn', compact('data'))->setPaper('a4', 'portrait');
        return $pdf->download($iNo['invoice_no'] . '-GRN.pdf');
    }

    public function pdfViewPaid($id)
    {
        $data = DB::table('purchasing_order_header')
            ->select('supplier.supplier_name AS customer_name', 'supplier.*', 'raw_material.*', 'purchasing_order_header.*', 'purchasing_order_data.*')
            ->leftJoin('supplier', 'purchasing_order_header.supplier_id', '=', 'supplier.id')
            ->rightJoin('purchasing_order_data', 'purchasing_order_data.purchasing_order_id', '=', 'purchasing_order_header.id')
            ->rightJoin('raw_material', 'raw_material.raw_material_id', '=', 'purchasing_order_data.raw_material_id')
            ->where('purchasing_order_header.id', '=', $id)
            ->get();

        $iNo = PurchasingOrder::where('id', '=', $id)->first();
        $pdf = PDF::loadView('PackingPurchasingOrderManager::packing-po.paid', compact('data'))->setPaper('a4', 'portrait');
//        return view('PackingPurchasingOrderManager::packing-po.paid')->with(['data'=>$data]);
        return $pdf->stream();
        return $pdf->download($iNo['invoice_no'] . '-PO-PAID.pdf');
    }

    public function pdfDownloadPaid($id)
    {
        $data = DB::table('purchasing_order_header')
            ->select('supplier.supplier_name AS customer_name', 'supplier.*', 'raw_material.*', 'purchasing_order_header.*', 'purchasing_order_data.*')
            ->leftJoin('supplier', 'purchasing_order_header.supplier_id', '=', 'supplier.id')
            ->rightJoin('purchasing_order_data', 'purchasing_order_data.purchasing_order_id', '=', 'purchasing_order_header.id')
            ->rightJoin('raw_material', 'raw_material.raw_material_id', '=', 'purchasing_order_data.raw_material_id')
            ->where('purchasing_order_header.id', '=', $id)
            ->get();

        $iNo = PurchasingOrder::where('id', '=', $id)->first();
        $pdf = PDF::loadView('PackingPurchasingOrderManager::packing-po.paid', compact('data'))->setPaper('a4', 'portrait');
        return $pdf->download($iNo['invoice_no'] . '-PO-PAID.pdf');
    }

    public function pay(Request $request)
    {
        $po = $request->po;
        $total = $request->total;
        $poh = PackingOrder::where('id', $po)->with('supplier')->first();
        if ($request->pmethod) {
            $acc = $request->account;
            $chequeNo = $request->chequeNo;
            $chequeDate = $request->chequeDate;
            BankTransaction::issuCheque(2, $total, $acc, $chequeNo, $chequeDate, Carbon::now(), $poh->supplier_id, '');
            $poh->status = 3;
            $poh->save();

        } else {
            CashTransaction::updateCashLocker(2, $total);
            CashTransaction::updateCashTransaction(2, $total, $poh->id, 1, $poh['supplier']->supplier_name . ' invoice no ' . $poh->invoice_no . ' paid');
            $poh->status = 3;
            $poh->save();
        }

        return redirect(route('packing-po.index'));
    }

}
