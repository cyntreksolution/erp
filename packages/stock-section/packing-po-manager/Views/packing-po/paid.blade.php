<html>
<head>
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/invoice.css')}}" rel="stylesheet">

</head>
<body>
@foreach($data as $key => $item)
    <div class="row">
        <h4 class="inv">
            PAID PURCHASING ORDER
        </h4>
    </div>

    <div class="row">
        <div class="col-xs-8">
            @if($item->stock_id == 1)
                <h4>
                    <b>Ranjanas[Pvt]Ltd</b><br>
                    No.357 <br>
                    Mathara Road<br>
                    Habaraduwa<br>
                    Mobile : +9477 330 4678
                </h4>
            @endif
        </div>
        {{--<br>--}}
        <div class="col-sx-4">
            <h4>Order To :<br><b>{{$item->customer_name}}</b><br>{!! str_replace(',', '<br>', $item->address) !!}</h4>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="text-left col-xs-2">
            <h5>PO NUMBER</h5>
            <h5>DATE OF PO</h5>
            {{--<h5>DUE DATE : {{$item->due_date}}</h5>--}}
        </div>
        <div class="text-left col-xs-2">
            <h5>: {{$item->invoice_no}}</h5>
            <h5>: {{$item->received_date}}</h5>
            {{--<h5>DUE DATE : {{$item->due_date}}</h5>--}}
        </div>
        <div class="col-xs-4 pull-right text-right">
            <h5>AMOUNT DUE (LKR)</h5>
            <h4>{{$item->total}}</h4>
            {{--<h5>DUE DATE : {{$item->due_date}}</h5>--}}
        </div>
    </div>


    <div class="row">
        <div class="col-xs-12">
            <table class="table" style="margin-top: 50px;">
                <thead class="thead-inverse">
                <tr style="background-color: #656872; color: white">
                    <th>No</th>
                    <th>Product</th>
                    <th class="text-center">Unit value</th>
                    <th class="text-center">Qty.</th>
                    <th class="text-right">Line Total (LKR)</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $key => $item)
                    <tr>
                        <th scope="row">{{$key+1}}</th>
                        <td>{{$item->name}}</td>
                        <td class="text-center">{{$item->received_price}}</td>

                        @if($item->measurement=='gram')
                            @if($item->recieved_qty > 1000)
                                <td class="text-center">{{$item->recieved_qty/1000}} kg</td>
                                <td class="text-right">{{$item->received_price*($item->recieved_qty / 1000)}}</td>
                            @else
                                <td class="text-center">{{$item->recieved_qty}} g</td>
                                <td class="text-right">{{$item->received_price*($item->recieved_qty / 1000)}}</td>
                            @endif

                        @elseif($item->measurement=='milliliter')
                            @if($item->recieved_qty > 1000)
                                <td class="text-center"> {{$item->recieved_qty/1000}} l</td>
                                <td class="text-right">{{$item->received_price*($item->recieved_qty / 1000)}}</td>
                            @else
                                <td class="text-center"> {{$item->recieved_qty}} ml</td>
                                <td class="text-right">{{$item->received_price*($item->recieved_qty / 1000)}}</td>
                            @endif
                        @elseif($item->measurement=='unit')
                            <td class="text-center">{{$item->recieved_qty}} units</td>
                            <td class="text-right">{{$item->received_price*($item->recieved_qty)}}</td>
                        @endif

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>

    <div class="col-xs-4 pull-right text-right">
        <h4> Amount Due (LKR):{{$item->total}}</h4>
    </div>

    <div class="row" style="margin-top: 80px;">
        <div class="col-xs-3">
            <h5>
                .........................................<br>
                Signature-Authorized
            </h5>
        </div>


        <div class="col-xs-3 text-right pull-right">
            <h5>
                .........................................<br>
                Signature-Received
            </h5>
        </div>

    </div>

    <div>
        <img src="{{asset('assets/img/paid-stamp.png')}}" width="200" height="120" style="margin-left: 250px">
    </div>
    <footer>
        <div class="row" style="margin-top: 20px;">
            <h4 align="center">
                <h6 class="text-muted">powered by cyntrek<br>
                    <h7 class="num">+9471 122 5489 / +9471 532 2262</h7>
                </h6>
            </h4>

        </div>
    </footer>
    @break
@endforeach
</body>

</html>