@extends('layouts.back.master')@section('title','Purchasing Order')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .hide {
            display: none;
        }

        .app-main-content {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .portlet-placeholder {
            border: 1px dotted black;
            margin: 0 1em 1em 0;
            height: 100%;
        }

        #projects-task-modal .modal-bg {
            padding: 50px;
        }

        #projects-task-modal .modal-content {
            box-shadow: none;
            border: none
        }

        #projects-task-modal .close {
            border: none;
            outline: none;
            box-shadow: none;
            position: absolute;
            right: 1rem;
            top: 1rem;
            width: 25px;
            height: 25px;
            line-height: 25px;
            text-align: center;
            font-size: 1.13rem;
            border-radius: 500px;
            border: 1px solid #868e96
        }

        #floating-button {
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #db4437;
            position: fixed;
            top: 13rem;
            right: 90px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .plus {
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }


    </style>
@stop
@section('content')
    <div class="app-wrapper">
        <div class="app-panel show" id="app-panel">
            <div class="app-search">
                <input type="search" class="search-field" placeholder="Search">
                <i class="search-icon fa fa-search"></i>
            </div>
            <div class="app-panel-inner">
                <div class="scroll-container ps-container ps-theme-default"
                     data-ps-id="dd5b112a-b6dd-36cb-37c4-c6b5e2d1bfdf">

                    <div class="projects-list">
                        @foreach($supliers as $suplier)
                            <a value="{{$suplier->id}}" class="media">
                                @if(isset($suplier->image))
                                    <div class="avatar avatar-circle avatar-md project-icon">
                                        <img src="{{asset($suplier->image_path.$suplier->image)}}" alt="">
                                    </div>
                                @else
                                    @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp

                                    <div class="avatar avatar-circle avatar-md project-icon bg-{{$color[$suplier->colour]}} "
                                         data-plugin="firstLitter"
                                         data-target="#{{$suplier->id}}" data-toggle="tooltip">
                                    </div>
                                @endif
                                <div class="media-body">
                                    <h6 class="project-name" id="{{$suplier->id}}">{{$suplier->supplier_name}}</h6>
                                    <small class="project-detail">{{$suplier->address}}</small>
                                    <br>
                                    <small class="project-detail">{{$suplier->mobile}}</small>
                                </div>
                            </a><!-- /.media -->
                        @endforeach


                    </div>
                    <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;">
                        <div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;">
                        </div>
                    </div>
                    <div class="ps-scrollbar-y-rail" style="top: 0px; right: 0px;">
                        <div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                    </div>
                </div>
            </div>

            <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show">
                <i class="fa fa-chevron-right"></i> <i class="fa fa-chevron-left"></i></a></div>

        <div class="app-main">
            <div class="app-main-header">
                <h5 class="aa app-main-heading text-center"><span>NEW PACKING ITEM ORDER</span></h5>
            </div>


            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">
                    <div class="container">
                        {{ Form::open(array('url' => 'packing-purchase' ,'id'=>'frmOrder'))}}
                        <input type="hidden" name="supplier_id" id="supplier_id">
                        <div class="projects-list">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="d-flex mr-auto">
                                        <div class="form-group">
                                            <label for="single">Packing Items</label>
                                            <select id="single" name="Single"
                                                    class="form-control select2 " ui-jp="select2"
                                                    style="width: 12rem"
                                                    ui-options="{theme: 'bootstrap'}">
                                                <option>Select Packing Item</option>
                                                @foreach($products as $product)
                                                    <option value=" {{$product->id}}">{{$product->name}}</option>
                                                @endforeach

                                            </select>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="d-flex activity-counters justify-content-between">

                                        <div class="text-center ">
                                            <label for="single">Last Price(LKR)</label><br>
                                            <div class="my-2 lastprice" id="lastprice">0
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="d-flex activity-counters justify-content-between">

                                        <div class="text-center ">
                                            <label for="single">Price(LKR)</label>
                                            <input class="form-control" id="price" name="price"
                                                   placeholder="" type="text" style="width:90px">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="d-flex activity-counters justify-content-between">

                                        <div class="text-center ">
                                            <label for="single">Quantity</label>
                                            <input class="form-control" id="qty" name="qty"
                                                   placeholder="" type="text" style="width:90px">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="d-flex activity-counters justify-content-between">

                                        <div class="text-center ">
                                            <div class="col-md-2 py-4 form-group px-1">
                                                <div class="btn btn-outline-primary" id="a">Add Item</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <hr>
                            <div class=" abc projects-list">
                            </div>


                        </div>


                        <button type="submit" class="btn btn-primary btn-lg btn-block submit"> Order Now</button>
                    </div>
                    {!!Form::close()!!}


                </div>
            </div>

        </div>
    </div>

@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $('.delete').click(function (e) {
            e.preventDefault();
            id = $(this).attr("value");
            confirmAlert(id);

        });

        function confirmAlert(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "delete",
                        url: id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            }
                            else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

        $(document).ready(function () {
            $("#unit").hide();
            $('#pj-task-1').change(function () {
                if (this.checked)
                    $("#unit").show();
                else
                    $("#unit").hide();

            });
        });

        $("#a").click(function () {

            var a = $("#single option:selected").val();
            var qty = $('#qty').val();
            var price = $('#price').val();
            console.log(a)
            $.ajax({
                type: "get",
                url: '../packing/get/packingitem',
                data: {id: a},

                success: function (response) {
                    console.log(response)
                    $(".abc").append(' <div class=" card-body d-flex  align-items-center p-0" >' +
                        '<div class="d-flex mr-auto">' +
                        '<div>' +
                        '<div class="avatar avatar text-white avatar-md project-icon bg-primary">' +
                        '<img class="card-img-top" src="" alt="">' +
                        '</div>' +
                        '</div>' +
                        '<h5 class="mt-3 mx-3" >' + response['name'] + '</h5>' +
                        '<input type="hidden" name="ingredients[]" value=" ' + response['id'] + ' "/>' +
                        '<input type="hidden" name="price[]" value=" ' + price + ' "/>' +
                        '<input type="hidden" name="qty[]" value=" ' + qty + ' "/>' +
                        '</div>' +
                        '<div class="d-flex activity-counters justify-content-between">' +
                        '<div class="text-center px-5">' +
                        '<div class="text-primary"><h6 >' + price + " LKR" + '</h6>' +
                        '</div>' +
                        '</div>' +
                        '</div> ' +
                        '<div class="d-flex activity-counters justify-content-between">' +
                        '<div class="text-center px-5">' +
                        '<div class="text-primary"><h6 >' +" X "+ '</h6>' +
                        '</div>' +
                        '</div>' +
                        '</div> ' +
                        '<div class="d-flex activity-counters justify-content-between">' +
                        '<div class="text-center px-5">' +
                        '<div class="text-primary"><h6 >' + qty + '</h6>' +
                        '</div>' +
                        '</div>' +
                        '</div> ' +
                        '</div>');
                }
            });

            reset();
        });

        function reset() {

            document.getElementById("single").value = "";
            document.getElementById("qty").value = "";
            document.getElementById("price").value = "";
        }

        $(".media").click(function () {

            sid = $(this).attr("value");
            var name = $('.project-name').text();

            $.ajax({
                type: "post",
                url: '../supplier/getsupplier',
                data: {id: sid},

                success: function (response) {
                    $(".aa").html('');
                    $(".aa").append(response['supplier_name'] + ' Purchasing Order');
                    $("#supplier_id").val(response['id']);
                }
            });

        });

        $("#single").change(function () {

            var a = $("#single option:selected").val();

            $.ajax({
                type: "post",
                url: '../po/getlastprice',
                data: {id: a},

                success: function (response) {
                    $(".lastprice").empty();
                    $(".lastprice").append(response['received_price'])
                }
            });

        });

    </script>
    <script>
        $(".submit").click(function (e) {
            e.preventDefault();
            let suplier = document.getElementById('supplier_id').value;
            if(suplier==null || suplier==''){
                alert("please select a supplier")
            }else{
                $("#frmOrder").submit()
            }
        });
    </script>
@stop
