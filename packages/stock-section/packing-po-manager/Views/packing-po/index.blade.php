@extends('layouts.back.master')@section('title','Purchasing Order Packing Items')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .hide {
            display: none;
        }
    </style>
@stop
@section('content')
    <div class="app-wrapper">
        <div class="app-panel" id="app-panel">
            <div class="app-search">
                <input type="search" class="search-field" placeholder="Search"> <i class="search-icon fa fa-search"></i>
            </div>
            <div class="scroll-container" id="scroll-container">
                <div class="app-panel-inner">
                    <div class="scroll-container ps-container">
                        <div class="p-3">
                            <a class="btn btn-success py-3 btn-block btn-lg text-white" href="{{route('packing-po.create')}}" >New Packing Items Order
                            </a>
                        </div>
                        <hr class="m-0">
                        {{--<div class="people-list d-flex justify-content-start flex-wrap p-3">--}}

                            {{--<a href="po/date/view">--}}
                                {{--<button class="avatar-sm avatar-circle fz-base font-weight-bold add-people-btn mx-1">--}}
                                    {{--<i data-toggle="tooltip" data-placement="bottom" title="Check Date"--}}
                                       {{--class="zmdi zmdi-time-interval"></i>--}}
                                {{--</button>--}}
                            {{--</a>--}}
                        {{--</div>--}}
                        {{--<hr class="m-0">--}}
                        <div class="media-list">
                            @foreach($supliers as $suplier)
                                <div class="media">
                                    @if(isset($suplier->image))
                                        <a href="#" class="avatar avatar-circle avatar-sm">
                                            <img src="{{asset($suplier->image_path.$suplier->image)}}" alt="">
                                        </a>
                                    @else
                                        @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                        <a href="#"
                                           class="avatar avatar-circle avatar-sm bg-{{$color[$suplier->colour]}} "
                                           data-plugin="firstLitter"
                                           data-target="#sup-{{$suplier->id*874}}">
                                        </a>
                                    @endif

                                    <div class="media-body">
                                        <h6 class="project-name"
                                            id="sup-{{$suplier->id*874}}">{{$suplier->supplier_name}}</h6>
                                        <small class="project-detail">{{$suplier->address}}</small>
                                        <br>
                                        <small class="project-detail">{{$suplier->mobile}}</small>
                                    </div>
                                </div>
                            @endforeach

                        </div>

                    </div>
                </div>
            </div>
            <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show">
                <i class="fa fa-chevron-right"></i>
                <i class="fa fa-chevron-left"></i>
            </a>

        </div>

        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">PURCHASING ORDERS | Packing Items</h5>
            </div>


            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">
                    <div class="container">


                        <div class="projects-list">
                            @foreach($pos as $po)
                                @if($po->status==1)
                                    <a href="{{'/packing-purchase/'.$po->id}}">
                                        <div class="media">
                                            <div class="avatar avatar text-white avatar-md project-icon bg-primary"
                                                 data-target="#project-1">
                                                <i class="fa fa-pause" aria-hidden="true"></i>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="project-name" id="project-1">{{$po->invoice_no}}</h6>
                                                <small class="project-detail">{{$po['supplier']->supplier_name}}</small>
                                                <div>
                                                    {{--<small class="project-detail">{{$po->total}}</small>--}}
                                                </div>

                                            </div>
                                            <button class="btn btn-outline-primary" style="width:6rem">Pending</button>
                                        </div>
                                        <hr class="m-0">
                                    </a>
                                @elseif($po->status==2)
                                    <a href="{{'/packing-purchase/'.$po->id}}">
                                        <div class="media">
                                            <div class="avatar avatar text-white avatar-md project-icon bg-info"
                                                 data-target="#project-2">
                                                <i class="fa fa-get-pocket" aria-hidden="true"></i>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="project-name" id="project-1">{{$po->invoice_no}}</h6>
                                                <small class="project-detail">{{$po['supplier']->supplier_name}}</small>
                                                <div>
                                                    <small class="project-detail">{{$po->total}}</small>
                                                </div>

                                            </div>
                                            <button class="btn btn-outline-info" style="width:6rem">Recieved</button>
                                        </div>
                                        <hr class="m-0">
                                    </a>
                                @elseif($po->status==3)
                                    <a href="{{'/packing-purchase/'.$po->id}}">
                                        <div class="media">
                                            <div class="avatar avatar text-white avatar-md project-icon bg-success"
                                                 data-target="#project-3">
                                                <i class="fa fa-dollar" aria-hidden="true"></i>
                                            </div>
                                            <div class="media-body">
                                                <h6 class="project-name" id="project-1">{{$po->invoice_no}}</h6>
                                                <small class="project-detail">{{$po['supplier']->supplier_name}}</small>
                                                <div>
                                                    <small class="project-detail">{{$po->total}}</small>
                                                </div>

                                            </div>
                                            <button class="btn btn-outline-success" style="width:6rem">Paid</button>
                                        </div>
                                        <hr class="m-0">
                                    </a>
                                @endif
                            @endforeach
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $('.delete').click(function (e) {
            e.preventDefault();
            id = $(this).attr("value");
            confirmAlert(id);

        });

        function confirmAlert(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "delete",
                        url: id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            }
                            else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

    </script>
@stop