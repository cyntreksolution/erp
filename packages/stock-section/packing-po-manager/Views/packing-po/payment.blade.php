@extends('layouts.back.master')@section('title','Purchasing order')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}">


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .app-wrapper {
            font-family: Raleway, sans-serif;
            width: 100%;
            border: 1px solid #eee;
        }

        .hide {
            display: none;
        }
    </style>
@stop
@section('content')
    <div class="app-wrapper">
        <div class="profile-section-user" id="app-panel">

            @foreach($po as $p)
                <div class="profile-cover-img"><img
                            src="{{asset('assets\img\supp.jpg')}}" alt="">
                </div>
                <div class="profile-info-brief p-3">
                    @if(isset($p->image))
                        <div class="user-profile-avatar avatar-circle avatar avatar-sm">
                            <img src="{{asset($p->image_path.$p->image)}}" alt="">
                        </div>
                    @else
                        @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                        <div class="user-profile-avatar avatar avatar-circle avatar-md project-icon bg-{{$color[$p['supplier']->colour]}}"
                             data-plugin="firstLitter"
                             data-target="#{{$p->id*754}}"></div>
                    @endif
                    <div class="text-center"><h5 class="text-uppercase mb-1"
                                                 id="{{$p->id*754}}">{{$p['supplier']->supplier_name}}</h5>
                        <div class="hidden-sm-down ">
                            <div>{{$p['supplier']->mobile}}</div>
                            <div>NO #{{$p->id}}</div>
                            <div>Total : {{$p->total}}</div>
                            <div>{{$p->created_at}}</div>
                            {{--<button class="btn btn-outline-primary btn-sm m-2 btn-rounded " disabled>--}}
                            {{--<i class="fa fa-pause px-1" aria-hidden="true"></i>--}}
                            {{--PENDING--}}
                            {{--</button>--}}
                        </div>
                        {{--@if($p->status==1)--}}
                        {{--<hr class="m-0 py-2">--}}
                        {{--<div class="d-flex justify-content-center flex-wrap p-2">--}}
                        {{--<a href="{{$p->id}}/edit" class="btn btn-block btn-success btn-sm m-2 text-white">--}}
                        {{--RECIEVE</a>--}}
                        {{--</div>--}}

                        {{--@elseif($p->status==2)--}}
                        {{--<hr class="m-0 py-2">--}}
                        {{--<div class="d-flex justify-content-center flex-wrap p-2">--}}
                        {{--<a href="{{$p->id}}/edit" class="btn btn-block btn-success btn-sm m-2 text-white">--}}
                        {{--PAY</a>--}}
                        {{--</div>--}}
                        {{--@endif--}}

                    </div>
                </div>
                <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show"><i
                            class="fa fa-chevron-right"></i>
                    <i class="fa fa-chevron-left"></i>
                </a>
            @endforeach
        </div>
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">PAYMENTS</h5>
            </div>
            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">
                    <div class="container">
                        {{--<div class="projects-list">--}}

                        {{--<div class="row">--}}
                        {{--<div class="col-lg-12 col-md-6">--}}
                        {{--<div class="card p-1 bg-faded">--}}
                        {{--<div class="card-body d-flex border-b-1 flex-wrap align-items-center">--}}
                        {{--<div class="d-flex mr-auto">--}}
                        {{--<h6 class="float-left mt-1">--}}
                        {{--Payment Type</h6>--}}
                        {{--</div>--}}
                        {{--<div class="d-flex activity-counters justify-content-between">--}}
                        {{--<div class="text-center px-2">--}}
                        {{--<div class=" mr-4">--}}
                        {{--<small class="m-2" style="font-size: 100%">Cash</small>--}}
                        {{--<input type="checkbox" data-plugin="switchery"--}}
                        {{--data-color="#10c469">--}}
                        {{--<small class="m-2 text-success" style="font-size: 100%">--}}
                        {{--Check--}}
                        {{--</small>--}}
                        {{--</div>--}}
                        {{--</div>--}}

                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        <div class="row">
                            <div class="col-lg-12 col-sm-6 my-2">
                                {{ Form::open(array('url' => 'packing-purchase/'.$p->id.'/pay'))}}
                                <div class="card">
                                    <header class="card-header"><h6 class="card-heading">Payment Type</h6>
                                        <div class="d-flex activity-counters justify-content-between ">
                                            <div class="text-center px-2">
                                                <div class=" mr-4">
                                                    <small class="m-2" style="font-size: 100%">Cash</small>
                                                    <input name="pmethod" type="checkbox" data-plugin="switchery"
                                                           data-color="#10c469" class="cheque" id="cheque">
                                                    <small class="m-2 text-success" style="font-size: 100%">
                                                        Cheque
                                                    </small>
                                                </div>
                                            </div>

                                        </div>
                                        <!-- /.card-toolbar --></header>

                                    <input type="hidden" name="po" value="{{$p->id}}">
                                    <div class="card-body d-flex px-5" id="cashdiv" style="text-align: center">
                                        <div class="mr-auto text-primary ml-5">
                                            <h5>Rs. <span data-plugin="counterUp">{{$po[0]->total}}</span></h5>
                                            <input type="hidden" name="total" value="{{$po[0]->total}}">
                                            <span class="" style="text-align: center">Total Payment</span>
                                        </div>
                                        <div>
                                            <button class="btn btn-sm btn-primary">Pay</button>
                                        </div>
                                    </div>
                                    <div class="hide card-body  px-5" id="chequediv">

                                        {{--<div class="card-body d-flex px-3">--}}
                                        {{--<div class="mr-auto text-primary">--}}
                                        {{--<h5 class="mr-5">Total Payment</h5></div>--}}
                                        {{--<div>--}}
                                        {{--<h5>Rs.<span--}}
                                        {{--data-plugin="counterUp">{{$po[0]->total}}</span></h5>--}}
                                        {{--</div>--}}
                                        {{--</div>--}}

                                        <div class="form-group">
                                            <select id="single" name="account"
                                                    {{--onChange="selectIngredients(this);"--}}
                                                    class="form-control select2 " ui-jp="select2"
                                                    ui-options="{theme: 'bootstrap'}">
                                                <option>Select Account Holder</option>
                                                @foreach($accounts as $account)
                                                    <option value=" {{$account->id}}">{{$account->holder_name}}</option>
                                                @endforeach

                                            </select>

                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-12 col-sm-8"><input type="text" class="form-control"
                                                                                   id="lastname" name="chequeNo"
                                                                                   placeholder="Cheque number">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-12 col-sm-8"><input type="text" class="form-control"
                                                                                   id="username" name="chequeDate"
                                                                                   placeholder="Cheque date"></div>
                                        </div>


                                        {{--<div class="form-group row">--}}
                                        {{--<div class="col-md-5 col-sm-8 offset-sm-4">--}}
                                        {{--<div><a href="#" class="btn btn-sm btn-primary" style="width: 50px">Pay</a>--}}
                                        {{--</div>--}}
                                        {{--</div>--}}
                                        {{--</div>--}}

                                    </div>

                                </div>
                                {!!Form::close()!!}
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script>

        $('#cheque').change(function (e) {
            var id = $(this).attr("value");
            if ($(this).is(":checked")) {
                $("#chequediv").removeClass("hide");
                disableButton("#cash");
            }
            else {
                $("#chequediv").addClass("hide");

            }
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $('.delete').click(function (e) {
            e.preventDefault();
            id = $(this).attr("value");
            confirmAlert(id);

        });

        function confirmAlert(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "delete",
                        url: id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            }
                            else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

    </script>
@stop