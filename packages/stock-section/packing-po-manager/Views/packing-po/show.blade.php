@extends('layouts.back.master')@section('title','Purchasing order')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}">


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .app-wrapper {
            font-family: Raleway, sans-serif;
            width: 100%;
            border: 1px solid #eee;
        }
    </style>
@stop
@section('content')
    <div class="app-wrapper">
        <div class="profile-section-user" id="app-panel">
            @foreach($po as $p)
                <div class="profile-cover-img"><img src="{{asset($p['supplier']->image_path.$p['supplier']->image)}}"
                                                    alt="">
                </div>
                <div class="profile-info-brief p-3"><img class="img-fluid user-profile-avatar"
                                                         src="{{asset($p['supplier']->image_path.$p['supplier']->image)}}"
                                                         alt="">
                    <div class="text-center"><h5 class="text-uppercase mb-1">{{$p['supplier']->supplier_name}}</h5>

                        @if($p->status==1)
                            <div class="hidden-sm-down ">
                                <div>{{$p['supplier']->mobile}}</div>
                                <div>NO #{{$p->id}}</div>
                                <div>{{$p->created_at}}</div>
                                <button class="btn btn-outline-primary btn-sm m-2 btn-rounded " disabled>
                                    <i class="fa fa-pause px-1" aria-hidden="true"></i>
                                    PENDING
                                </button>
                            </div>
                            <button class="btn btn-outline-primary btn-sm m-2 btn-rounded ">

                                <a href="{{$p->id}}/pdfViewPO" aria-hidden="true">
                                    <i class="fa fa-eye px-1"></i>
                                    PO-VIEW
                                </a>
                            </button>
                            <button class="btn btn-outline-primary btn-sm m-2 btn-rounded ">

                                <a href="{{$p->id}}/pdfDownloadPO" aria-hidden="true">
                                    <i class="fa fa-print px-1"></i>
                                    DOWNLOAD
                                </a>
                            </button>
                            <hr class="m-0 py-2">
                            <div class="d-flex justify-content-center flex-wrap p-2">
                                <a href="{{$p->id}}/edit" class="btn btn-block btn-success btn-sm m-2 text-white">
                                    RECIEVE</a>
                            </div>

                        @elseif($p->status==2)
                            <div class="hidden-sm-down ">
                                <div>{{$p['supplier']->mobile}}</div>
                                <div>NO #{{$p->id}}</div>
                                <div>{{$p->created_at}}</div>
                                <button class="btn btn-outline-primary btn-sm m-2 btn-rounded " disabled>
                                    <i class="fa fa-pause px-1" aria-hidden="true"></i>
                                    RECIEVED
                                </button>
                            </div>
                            <button class="btn btn-outline-primary btn-sm m-2 btn-rounded ">

                                <a href="{{$p->id}}/pdfViewGRN" aria-hidden="true">
                                    <i class="fa fa-eye px-1"></i>
                                    GRN-VIEW
                                </a>
                            </button>
                            <button class="btn btn-outline-primary btn-sm m-2 btn-rounded ">

                                <a href="{{$p->id}}/pdfDownloadGRN" aria-hidden="true">
                                    <i class="fa fa-print px-1"></i>
                                    DOWNLOAD
                                </a>
                            </button>
                            <hr class="m-0 py-2">
                            <div class="d-flex justify-content-center flex-wrap p-2">
                                <a href="{{$p->id}}/payment" class="btn btn-block btn-success btn-sm m-2 text-white">
                                    PAY</a>
                            </div>

                        @elseif($p->status==3)
                            <div class="hidden-sm-down ">
                                <div>{{$p['supplier']->mobile}}</div>
                                <div>NO #{{$p->id}}</div>
                                <div>{{$p->created_at}}</div>
                                <button class="btn btn-outline-primary btn-sm m-2 btn-rounded " disabled>
                                    <i class="fa fa-pause px-1" aria-hidden="true"></i>
                                    PAID
                                </button>
                            </div>
                            <button class="btn btn-outline-primary btn-sm m-2 btn-rounded ">

                                <a href="{{$p->id}}/pdfViewPaid" aria-hidden="true">
                                    <i class="fa fa-eye px-1"></i>
                                    PAID-PO-VIEW
                                </a>
                            </button>
                            <button class="btn btn-outline-primary btn-sm m-2 btn-rounded ">

                                <a href="{{$p->id}}/pdfDownloadPaid" aria-hidden="true">
                                    <i class="fa fa-print px-1"></i>
                                    DOWNLOAD
                                </a>
                            </button>

                        @endif

                    </div>
                </div>
                <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show"><i
                            class="fa fa-chevron-right"></i>
                    <i class="fa fa-chevron-left"></i>
                </a>
            @endforeach
        </div>
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">ORDERED PRODUCTS</h5>
            </div>
            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">
                    <div class="container">
                        <div class="projects-list">

                            <div class="row">
                                @foreach($po as $p)
                                    @foreach($p['items'] as $item)
                                        <div class="col-lg-12 card-body d-flex  align-items-center p-0">

                                            <div class="d-flex mr-auto">
                                                <div>
                                                    <div class="avatar avatar text-white avatar-md project-icon bg-primary">
                                                        <img class="card-img-top"
                                                             src="{{asset($item['packingItem']->image_path.$item['packingItem']->image)}}"
                                                             alt="">
                                                    </div>
                                                </div>
                                                <h5 class="mt-3 mx-3"> {{$item['packingItem']->name}}</h5>
                                            </div>

                                            <div class="d-flex activity-counters justify-content-between">
                                                <div class="text-center px-5">
                                                    <div class="text-primary">
                                                        <h6>{{$item->requested_qty}}</h6>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        {{--<hr class="m-1">--}}
                                    @endforeach
                                @endforeach
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $('.delete').click(function (e) {
            e.preventDefault();
            id = $(this).attr("value");
            confirmAlert(id);

        });

        function confirmAlert(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "delete",
                        url: id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            } else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

    </script>
@stop