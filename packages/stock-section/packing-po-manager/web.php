<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web','log'])->group(function () {
    Route::prefix('packing-purchase')->group(function () {
        Route::get('', 'PackingPurchasingOrderManager\Controllers\PackingPurchasingOrderController@index')->name('packing-po.index');

        Route::get('create', 'PackingPurchasingOrderManager\Controllers\PackingPurchasingOrderController@create')->name('packing-po.create');

        Route::get('{po}', 'PackingPurchasingOrderManager\Controllers\PackingPurchasingOrderController@show')->name('packing-po.show');

        Route::get('{po}/payment', 'PackingPurchasingOrderManager\Controllers\PackingPurchasingOrderController@payment')->name('packing-po.payment');
        Route::post('{po}/pay', 'PackingPurchasingOrderManager\Controllers\PackingPurchasingOrderController@pay')->name('packing-po.pay');

        Route::get('{po}/edit', 'PackingPurchasingOrderManager\Controllers\PackingPurchasingOrderController@edit')->name('packing-po.edit');

        Route::get('{po}/pdfViewPO', 'PackingPurchasingOrderManager\Controllers\PackingPurchasingOrderController@pdfViewPO')->name('packing-po.po');
        Route::get('date/view', 'PackingPurchasingOrderManager\Controllers\PackingPurchasingOrderController@dateReport')->name('packing-po.date');

        Route::get('{po}/pdfDownloadPO', 'PackingPurchasingOrderManager\Controllers\PackingPurchasingOrderController@pdfDownloadPO')->name('packing-po.po');
        Route::post('date', 'PackingPurchasingOrderManager\Controllers\PackingPurchasingOrderController@getReport')->name('packing-po.date');

        Route::get('{po}/pdfViewGRN', 'PackingPurchasingOrderManager\Controllers\PackingPurchasingOrderController@pdfViewGRN')->name('packing-po.grn');

        Route::get('{po}/pdfDownloadGRN', 'PackingPurchasingOrderManager\Controllers\PackingPurchasingOrderController@pdfDownloadGRN')->name('packing-po.grn');

        Route::get('{po}/pdfViewPaid', 'PackingPurchasingOrderManager\Controllers\PackingPurchasingOrderController@pdfViewPaid')->name('packing-po.paid');

        Route::get('{po}/pdfDownloadPaid', 'PackingPurchasingOrderManager\Controllers\PackingPurchasingOrderController@pdfDownloadPaid')->name('packing-po.paid');


        Route::post('', 'PackingPurchasingOrderManager\Controllers\PackingPurchasingOrderController@store')->name('packing-po.store');

        Route::post('getlastprice', 'PackingPurchasingOrderManager\Controllers\PackingPurchasingOrderController@getlastprice')->name('packing-po.getlastprice');

        Route::post('{po}', 'PackingPurchasingOrderManager\Controllers\PackingPurchasingOrderController@update')->name('packing-po.update');

        Route::delete('{po}', 'PackingPurchasingOrderManager\Controllers\PackingPurchasingOrderController@destroy')->name('packing-po.destroy');
    });
});
