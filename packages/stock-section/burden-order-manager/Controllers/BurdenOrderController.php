<?php

namespace BurdenOrderManager\Controllers;

use App\RawInquiry;
use App\Traits\RawMaterialLog;
use BurdenManager\Models\Burden;
use EmployeeManager\Models\Employee;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use RawMaterialManager\Models\RawMaterial;
use RecipeManager\Models\Recipe;
use RecipeManager\Models\RecipeSemiProduct;
use ShortEatsManager\Models\CookingRequest;
use StockManager\Classes\StockTransaction;

class BurdenOrderController extends Controller
{
    use RawMaterialLog;
    function __construct()
    {
        $this->middleware('permission:burden_order-index', ['only' => ['index']]);
        $this->middleware('permission:burden_order-create', ['only' => ['create','store']]);
        $this->middleware('permission:burden_order-edit', ['only' => ['edit']]);
        $this->middleware('permission:burden_order-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $burden = Burden::where('status', '=', 1)
            ->with('semiProdcuts', 'recipe.recipeContents.materials')
            ->get();
        $cookingRequests = CookingRequest::where('status', '=', 1)
            ->with('semiProdcuts')
            ->get();
        $employees = Employee::get()->pluck('full_name', 'id');
        return view('BurdenOrderManager::burden_order.index')->with([
            'employees' => $employees,
            'burdens' => $burden,
            'cookingRequests' => $cookingRequests
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $production_order_details_id = 2;
        return 'k';
        return view('BurdenOrderManager::burden_order.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $burden_id = $request->burden_id;
        $burden = Burden::where('id', '=', $burden_id)
            ->with('semiProdcuts', 'recipe.recipeContents.materials')
            ->first();


        $raws = $burden['recipe']->recipeContents;


        $arr = $raws->pluck('raw_material_id')->toArray();
        $ua = array_unique($arr);
        $is_duplicated = sizeof($arr) != sizeof($ua);


        if ($is_duplicated) {
            $contents = $burden['recipe']->recipeContents;
            $new_list = [];
            foreach ($contents as $key => $content) {
                $currantAvailableQty = $content['materials']->available_qty;

               /* if (!empty($new_list)) {
                    foreach ($new_list as $new_key => $new_content) {
                        if ($new_content->raw_material_id == $content->raw_material_id) {
                            $new_content->qty = $new_content->qty + $content->qty;
                        } else {
                            array_push($new_list, $content);
                        }
                    }

                } else {
                    array_push($new_list, $content);
                }*/
               array_push($new_list, $content);
            }


            foreach ($new_list as $content) {

                $currantAvailableQty = $content['materials']->available_qty;
                $neededQty = $content['qty'] * $burden['burden_size'];

                if ($currantAvailableQty < $neededQty) {
                    return redirect(route('burden_order.index'))->with([
                        'error' => true,
                        'error.title' => 'Sorry !',
                        'error.message' => 'Not Raw Material in  Stock : ' . $content['materials']->name
                    ]);
                }
            }


        } else {

         /*   foreach ($burden['recipe']->recipeContents as $content) {

                $currantAvailableQty = $content['materials']->available_qty;
                $neededQty = $content['qty'] * $burden['burden_size'];

                if ($currantAvailableQty < $neededQty) {
                    return redirect(route('burden_order.index'))->with([
                        'error' => true,
                        'error.title' => 'Sorry !',
                        'error.message' => 'Not Raw Material in  Stock : ' . $content['materials']->name
                    ]);
                }
            }*/
        }

        $usage_price = 0;
        foreach ($burden['recipe']->recipeContents as $content) {

            $currantAvailableQty = $content['materials']->available_qty;
            $neededQty = $content['qty'] * $burden['burden_size'];
           // if($content->raw_material_id == 80) {
            //    dd($neededQty);
           // }
            if ($currantAvailableQty < $neededQty) {
                return redirect(route('burden_order.index'))->with([
                    'error' => true,
                    'error.title' => 'Sorry !',
                    'error.message' => 'Not Raw Material in  Stock : ' . $content['materials']->name
                ]);
            }

            if ($currantAvailableQty >= $neededQty) {
                $raw = RawMaterial::find($content['raw_material_id']);
                $raw_material_price = $raw->price / $raw->units_per_packs;
                $usage_price = $usage_price + $raw_material_price * $neededQty;
                try {
                    DB::transaction(function () use ($burden, $burden_id, $content, $neededQty, $raw) {

                     //   $chkdouble = RawInquiry::where('raw_material_id','=',$raw->raw_material_id)->where('desc_id','=',2)->where('reference','=',$burden->id)->where('Issued_ref_id','=',$burden->semi_finish_product_id)->first();
                     //   if(empty($chkdouble)) {
                            StockTransaction::updateRawMaterialAvailableQty(2, $content['raw_material_id'], $neededQty);
                            StockTransaction::rawMaterialStockTransaction(2, 0, $content['raw_material_id'], $neededQty, $burden_id, 1);
                            $raw_material = RawMaterial::find($raw->raw_material_id);
                            $this->recordRawMaterialOutstanding($raw_material, (($neededQty) * (-1)), 'Raw Material Issue for Burden', $burden->id, 'BurdenManager\\Models\\Burden', 2, 2, $burden->semi_finish_product_id);

                      //  }

                    });
                } catch (Exception $e) {
                    return $e;
                    return redirect(route('burden_order.index'))->with([
                        'error' => true,
                        'error.title' => 'Sorry !',
                        'error.message' => 'Something Went Wrong '
                    ]);
                }

            } else {
                return redirect(route('burden_order.index'))
                    ->with([
                        'error' => true,
                        'error.title' => ' Sorry !',
                        'error.message' => 'Not Enough Ingredients'
                    ]);
            }
        }


        $rcp = Recipe::find($burden->recipe_id);
        $burdenB = Burden::find($burden_id);
        $burdenB->emp_measurer_id = $request->employee;
        $burdenB->raw_count = $rcp->recipe_item;
        $burdenB->status = 2;
        $burdenB->value = $usage_price;
        $burdenB->save();

        return redirect(route('burden_order.index'))
            ->with([
                'info' => true,
                'info.title' => ' Congratulations !',
                'info.message' => 'Sent Ingredients'
            ]);
    }

    public function cookingRequestStore(Request $request)
    {

        $burden_id = $request->burden_id;
        $burden = CookingRequest::where('id', '=', $burden_id)
            ->with('semiProdcuts', 'recipe.recipeContents.materials')
            ->first();

        foreach ($burden['recipe']->recipeContents as $content) {
            $currantAvailableQty = $content['materials']->available_qty;
            $neededQty = $content['qty'] * $burden['burden_size'];
            if ($currantAvailableQty < $neededQty) {
                return redirect(route('burden_order.index'))->with([
                    'error' => true,
                    'error.title' => 'Sorry !',
                    'error.message' => 'Something Went Wrong '
                ]);
            }
        }

        $usage_price = 0;
        foreach ($burden['recipe']->recipeContents as $content) {
            $currantAvailableQty = $content['materials']->available_qty;
            $neededQty = $content['qty'] * $burden['burden_size'];


            if ($currantAvailableQty >= $neededQty) {
                $raw = RawMaterial::find($content['raw_material_id']);
                $raw_material_price = $raw->price / $raw->units_per_packs;
                $usage_price = $usage_price + $raw_material_price * $neededQty;

                try {

                    DB::transaction(function () use ($burden, $burden_id, $content, $neededQty, $raw_material_price, $raw) {

                   //     $chkdouble = RawInquiry::where('raw_material_id','=',$raw->raw_material_id)->where('desc_id','=',3)->where('reference','=',$burden->id)->where('Issued_ref_id','=',$burden->semi_finish_product_id)->first();
                   //     if(empty($chkdouble)) {
                            StockTransaction::updateRawMaterialAvailableQty(2, $content['raw_material_id'], $neededQty);
                            StockTransaction::rawMaterialStockTransaction(2, $raw_material_price * $neededQty, $content['raw_material_id'], $neededQty, $burden_id, 1);
                            $raw_material = RawMaterial::find($raw->raw_material_id);
                            $this->recordRawMaterialOutstanding($raw_material, (($neededQty) * (-1)), 'Raw Material Issue for Cooking Request', $burden->id, 'ShortEatsManager\\Models\\CookingRequest', 3, 2, $burden->semi_finish_product_id);
                    //    }

                    });
                } catch (Exception $e) {
                    return $e;
                    return redirect(route('burden_order.index'))->with([
                        'error' => true,
                        'error.title' => 'Sorry !',
                        'error.message' => 'Something Went Wrong '
                    ]);
                }


            } else {
                return redirect(route('burden_order.index'))
                    ->with([
                        'error' => true,
                        'error.title' => ' Sorry !',
                        'error.message' => 'Not Enough Ingredients'
                    ]);
            }
        }
        $rcp = Recipe::find($burden->recipe_id);
        $burdenC = CookingRequest::find($burden_id);
        $burdenC->emp_measurer_id = $request->employee;
        $burdenC->raw_count = $rcp->recipe_item;
        $burdenC->status = 2;
        $burdenC->value = $usage_price;
        $burdenC->save();

        return redirect(route('burden_order.index'))
            ->with([
                'info' => true,
                'info.title' => ' Congratulations !',
                'info.message' => 'Sent Ingredients'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \BurdenOrderManager\Models\Burden $burden_order
     * @return \Illuminate\Http\Response
     */
    public function show(Burden $burden_order)
    {
        return view('BurdenOrderManager::burden_order.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \BurdenOrderManager\Models\Burden $burden_order
     * @return \Illuminate\Http\Response
     */
    public function edit(Burden $burden_order)
    {
        return view('BurdenOrderManager::burden_order.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \BurdenOrderManager\Models\Burden $burden_order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Burden $burden_order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \BurdenOrderManager\Models\Burden $burden_order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Burden $burden_order)
    {
        $burden_order->delete();
        if ($burden_order->trashed()) {
            return true;
        } else {
            return false;
        }
    }
}
