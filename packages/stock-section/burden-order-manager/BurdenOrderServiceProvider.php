<?php

namespace BurdenOrderManager;

use Illuminate\Support\ServiceProvider;

class BurdenOrderServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/Views', 'BurdenOrderManager');
        require __DIR__ . '/web.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('BurdenOrderManager', function($app){
            return new BurdenOrderManager;
        });
    }
}
