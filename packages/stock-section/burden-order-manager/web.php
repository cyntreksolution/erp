<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web','auth'])->group(function () {
    Route::prefix('stock/requested/order')->group(function () {
        Route::get('', 'BurdenOrderManager\Controllers\BurdenOrderController@index')->name('burden_order.index');

        Route::get('create', 'BurdenOrderManager\Controllers\BurdenOrderController@create')->name('burden_order.create');

        Route::get('{requested_order}', 'BurdenOrderManager\Controllers\BurdenOrderController@show')->name('burden_order.show');

        Route::get('{requested_order}/edit', 'BurdenOrderManager\Controllers\BurdenOrderController@edit')->name('burden_order.edit');


        Route::post('', 'BurdenOrderManager\Controllers\BurdenOrderController@store')->name('burden_order.store');
        Route::post('cooking', 'BurdenOrderManager\Controllers\BurdenOrderController@cookingRequestStore')->name('cr.store');

        Route::put('{requested_order}', 'BurdenOrderManager\Controllers\BurdenOrderController@update')->name('burden_order.update');

        Route::delete('{requested_order}', 'BurdenOrderManager\Controllers\BurdenOrderController@destroy')->name('burden_order.destroy');
    });
});