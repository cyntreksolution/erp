@extends('layouts.back.master')@section('title','Burden Order')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/inbox.css')}}"/>

    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .hide {
            display: none;
        }

        .ml-3 {
            margin-left: 1rem !important;
        }

        .mr-3 {
            margin-right: 10px !important;
        }
    </style>
@stop
@section('content')
    <div class="site-content">
        <div class="app-wrapper">
            <div class="app-panel" id="app-panel">
                <div class="app-search">
                    <input type="search" class="search-field" placeholder="Search" disabled>
                    <i class="search-icon fa fa-search"></i>
                </div><!-- /.app-search -->
                <div class="app-panel-inner py-2">
                    <div class="scroll-container px-2">
                        <div class="inbox-cat-list" style=" height:400px; overflow: auto">
                            @foreach($burdens as $burden)
                                <a href="#" class="btn panel-item " data-target="{{$burden['id']}}"
                                   id="dash1-widget-activities"
                                   data-toggle="tooltip" data-placement="top"
                                   data-original-title="{{$burden['semiProdcuts']->name}}">
                                    <div class="d-flex mr-auto">
                                        <div id="dash1-easypiechart-1" class="dash1-easypiechart-1 chart easypiechart"
                                             data-percent="{{$burden['kg_per_burden']*2}}">
                                            @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                            <div class="svg-chart-icon center-icon avatar avatar-circle avatar-sm bg-{{$color[$burden['semiProdcuts']->colour]}} "
                                                 data-plugin="firstLitter"
                                                 data-target="#raw-{{$burden['semiProdcuts']->semi_finish_product_id*874}}">
                                            </div>
                                        </div>
                                        <p class="display-4 ml-3"
                                           data-plugin="counterUp">{{$burden['burden_size']}}</p>
                                        <p class="mr-3" style="font-size: 1rem;padding-top: 6%">KG</p>
                                        <p id="raw-{{$burden['semiProdcuts']->semi_finish_product_id*874}}"
                                           class="float-left mt-2"
                                           style="line-height: 1.25">{{ str_limit($burden['semiProdcuts']->name, $limit = 10, $end = '...') }}
                                            <br>{{$burden['expected_semi_product_qty']}} units
                                            <br>{{$burden['serial']}}  </p>
                                    </div>
                                </a>
                            @endforeach

                            @foreach($cookingRequests as $burden)
                                <a href="#" class="btn panel-item " data-target="{{$burden['id']}}"
                                   id="dash1-widget-activities"
                                   data-toggle="tooltip" data-placement="top"
                                   data-original-title="{{$burden['semiProdcuts']->name}}">
                                    <div class="d-flex mr-auto">
                                        <div id="dash1-easypiechart-1" class="dash1-easypiechart-1 chart easypiechart"
                                             data-percent="{{$burden['kg_per_burden']*2}}">
                                            @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                            <div class="svg-chart-icon center-icon avatar avatar-circle avatar-sm bg-{{$color[$burden['semiProdcuts']->colour]}} "
                                                 data-plugin="firstLitter"
                                                 data-target="#raw-{{$burden['semiProdcuts']->semi_finish_product_id*874}}">
                                            </div>
                                        </div>
                                        <p class="display-4 ml-3"
                                           data-plugin="counterUp">{{$burden['burden_size']}}</p>
                                        <p class="mr-3" style="font-size: 1rem;padding-top: 6%">KG</p>
                                        <p id="raw-{{$burden['semiProdcuts']->semi_finish_product_id*874}}"
                                           class="float-left mt-2"
                                           style="line-height: 1.25">{{ str_limit($burden['semiProdcuts']->name, $limit = 10, $end = '...') }}
                                            <br>{{$burden['expected_semi_product_qty']}} units
                                            <br>{{$burden['serial']}}  </p>
                                    </div>
                                </a>
                            @endforeach




                            {{--<a href="#" class="btn panel-item" id="aa" data-target="123">--}}
                            {{--<div class="media">--}}
                            {{--<img class="avatar avatar-circle avatar-md"--}}
                            {{--src="{{asset('assets/global/images/Beef_Bun.jpg')}}" alt="">--}}
                            {{--<div class="media-body">--}}
                            {{--<h4 class="project-name" id="">Single Bun</h4>--}}
                            {{--<h1 class="project-detail">28kg</h1>--}}
                            {{--<h6 class="project-detail">3 Burdens</h6>--}}
                            {{--</div>--}}
                            {{--<button class="btn btn-outline-primary">Send</button>--}}
                            {{--</div>--}}

                            {{--</a><!-- /.panel-item -->--}}

                            {{--<a href="#" class="btn panel-item" data-target="456">--}}
                            {{--<div class="media">--}}
                            {{--<img class="avatar avatar-circle avatar-md"--}}
                            {{--src="{{asset('assets/global/images/butter-cake.jpg')}}"--}}
                            {{--alt="">--}}
                            {{--<div class="media-body">--}}
                            {{--<h4 class="project-name" id="">Butter Cake</h4>--}}
                            {{--<h5 class="project-detail">50kg</h5>--}}
                            {{--<h6 class="project-detail">3 Burdens</h6>--}}
                            {{--</div>--}}

                            {{--</div>--}}
                            {{--</a>--}}
                        </div>
                    </div>
                </div>
                <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show">
                    <i class="fa fa-chevron-right"></i>
                    <i class="fa fa-chevron-left"></i>
                </a>
            </div>

            <div class="app-main">
                <header class="app-main-header"><h5 class="app-main-heading text-center">INGREDIENTS FOR THE
                        BURDEN </h5></header>

                <div class="app-main-content p-3">
                    <div id="content">

                        @foreach($burdens as $burden)
                            {{ Form::open(array('url' => 'stock/requested/order','enctype'=>'multipart/form-data'))}}
                            <div class="abc mail-list hide" id="{{$burden['id']}}"
                                 style=" height:400px; overflow: auto">
                                @foreach($burden['recipe']->recipeContents as $item)
                                    @php $err = ($item->materials->available_qty < $item['qty']*$burden['burden_size'])  ?1:0@endphp
                                    <div class="" style="padding-bottom: 1px"></div>
                                    <div class="card p-1 bg-faded" style="width: 100%; margin-bottom:5px">
                                        <div class="media {{($err)?'bg-warning':'bg-white'}} p-3">
                                            @if(isset($item['materials']->image))
                                                <div class="avatar avatar avatar-sm">
                                                    <img src="{{asset($item['materials']->image_path.$item['materials']->image)}}"
                                                         alt="">
                                                </div>
                                            @else
                                                @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                                <div
                                                        class="avatar avatar-sm project-icon bg-{{$color[$item['materials']->colour]}} "
                                                        data-plugin="firstLitter"
                                                        data-target="#raw-{{$item['materials']->raw_material_id*874}}">
                                                </div>
                                            @endif
                                            <div class="media-body">
                                                <h6 id="raw-{{$item['materials']->raw_material_id*874}}">{{$item['materials']->name}} {{($err)?'- insufficient ingredients':null}}</h6>
                                            </div>
                                            <div class="d-flex activity-counters justify-content-between">
                                                <div class="text-center px-2">
                                                    <h6 class="text-primary">Current Stock</h6>
                                                    <h6>
                                                        @if($item['materials']->measurement=='gram')
                                                            @if($item->materials->available_qty >= 1000)
                                                                <h6>{{$item->materials->available_qty /1000}} kg</h6>
                                                            @else
                                                                <h6>{{$item->materials->available_qty}} g</h6>
                                                            @endif

                                                        @elseif($item['materials']->measurement=='milliliter')
                                                            @if($item->materials->available_qty >= 1000)
                                                                <h6>{{$item->materials->available_qty/1000}} l</h6>
                                                            @else
                                                                <h6>{{$item->materials->available_qty}} ml</h6>
                                                            @endif
                                                        @elseif($item['materials']->measurement=='unit')
                                                            <h6>{{$item->materials->available_qty}} units</h6>
                                                        @endif
                                                    </h6>

                                                </div>
                                            </div>
                                            <div class="d-flex activity-counters justify-content-between">
                                                <div class="text-center px-2">
                                                    <h6 class="text-warning">QTY per Burden</h6>
                                                    <h6>
                                                        @if($item['materials']->measurement=='gram')
                                                            @if($item['qty']*$burden['burden_size'] >= 1000)
                                                                <h6>{{$item['qty']*$burden['burden_size']/1000}} kg</h6>
                                                            @else
                                                                <h6>{{$item['qty']*$burden['burden_size']}} g</h6>
                                                            @endif

                                                        @elseif($item['materials']->measurement=='milliliter')
                                                            @if($item['qty']*$burden['burden_size'] >= 1000)
                                                                <h6>{{$item['qty']*$burden['burden_size']/1000}} l</h6>
                                                            @else
                                                                <h6>{{$item['qty']*$burden['burden_size']}} ml</h6>
                                                            @endif
                                                        @elseif($item['materials']->measurement=='unit')
                                                            <h6>{{$item['qty']*$burden['burden_size']}} units</h6>
                                                        @endif
                                                    </h6>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                                    <div class="col-sm-12">
                                        {!! Form::select('employee', $employees , null , ['class' => 'form-control','required','placeholder'=>'select Measurer']) !!}

                                    </div>
                                <div class="p-3">
                                    <input type="submit" class="btn btn-success py-3 btn-block btn-lg"
                                           data-toggle="modal"
                                           data-target="#projects-task-modal"
                                           value="SEND ALL INGREDIENTS TO PRODUCT SECTION">
                                    </input>
                                </div>
                            </div>

                            <input type="hidden" name="burden_id" value="{{$burden['id']}}">

                            {!!Form::close()!!}
                        @endforeach

                        @foreach($cookingRequests as $burden)
                            {{ Form::open(array('url' => 'stock/requested/order/cooking','enctype'=>'multipart/form-data'))}}
                            <div class="abc mail-list hide" id="{{$burden['id']}}"
                                 style=" height:400px; overflow: auto;">

                                @foreach($burden['recipe']->recipeContents as $item)
                                    @php $err = ($item->materials->available_qty < $item['qty']*$burden['burden_size'])  ?1:0@endphp
                                    <div class="" style="padding-bottom: 1px"></div>
                                    <div class="card p-1 bg-faded" style="width: 100%; margin-bottom:5px;">
                                        <div class="media  {{($err)?'bg-warning':'bg-white'}} p-3">
                                            @if(isset($item['materials']->image))
                                                <div class="avatar avatar avatar-sm">
                                                    <img src="{{asset($item['materials']->image_path.$item['materials']->image)}}"
                                                         alt="">
                                                </div>
                                            @else
                                                @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                                <div
                                                        class="avatar avatar-sm project-icon bg-{{$color[$item['materials']->colour]}} "
                                                        data-plugin="firstLitter"
                                                        data-target="#raw-{{$item['materials']->raw_material_id*874}}">
                                                </div>
                                            @endif
                                            <div class="media-body">
                                                <h6 id="raw-{{$item['materials']->raw_material_id*874}}">{{$item['materials']->name}}</h6>
                                                {{--<div>Android Developer</div>--}}
                                            </div>
                                            <div class="d-flex activity-counters justify-content-between">
                                                <div class="text-center px-2">
                                                    <h6 class="text-primary">Current Stock</h6>
                                                    <h6>
                                                        @if($item['materials']->measurement=='gram')
                                                            @if($item->materials->available_qty >= 1000)
                                                                <h6>{{$item->materials->available_qty /1000}} kg</h6>
                                                            @else
                                                                <h6>{{$item->materials->available_qty}} g</h6>
                                                            @endif

                                                        @elseif($item['materials']->measurement=='milliliter')
                                                            @if($item->materials->available_qty >= 1000)
                                                                <h6>{{$item->materials->available_qty/1000}} l</h6>
                                                            @else
                                                                <h6>{{$item->materials->available_qty}} ml</h6>
                                                            @endif
                                                        @elseif($item['materials']->measurement=='unit')
                                                            <h6>{{$item->materials->available_qty}} units</h6>
                                                        @endif
                                                    </h6>

                                                </div>
                                            </div>
                                            <div class="d-flex activity-counters justify-content-between">
                                                <div class="text-center px-2">
                                                    <h6 class="text-warning">QTY per Burden</h6>
                                                    <h6>
                                                        @if($item['materials']->measurement=='gram')
                                                            @if($item['qty']*$burden['burden_size'] >= 1000)
                                                                <h6>{{$item['qty']*$burden['burden_size']/1000}} kg</h6>
                                                            @else
                                                                <h6>{{$item['qty']*$burden['burden_size']}} g</h6>
                                                            @endif

                                                        @elseif($item['materials']->measurement=='milliliter')
                                                            @if($item['qty']*$burden['burden_size'] >= 1000)
                                                                <h6>{{$item['qty']*$burden['burden_size']/1000}} l</h6>
                                                            @else
                                                                <h6>{{$item['qty']*$burden['burden_size']}} ml</h6>
                                                            @endif
                                                        @elseif($item['materials']->measurement=='unit')
                                                            <h6>{{$item['qty']*$burden['burden_size']}} units</h6>
                                                        @endif
                                                    </h6>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                    <div class="col-sm-12">
                                        <div class="col-sm-12">
                                            {!! Form::select('employee', $employees , null , ['class' => 'form-control','required','placeholder'=>'select Measurer']) !!}

                                        </div>
                                    </div>
                                <div class="p-3">
                                    <input type="submit" class="btn btn-success py-3 btn-block btn-lg"
                                           data-toggle="modal"
                                           data-target="#projects-task-modal"
                                           value="SEND ALL BURDENS TO PRODUCT SECTION">
                                    </input>
                                </div>
                            </div>

                            <input type="hidden" name="burden_id" value="{{$burden['id']}}">

                            {!!Form::close()!!}
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>


@stop
@section('js')
    <script>
        $(document).ready(function () {

            $(".dash1-easypiechart-1").easyPieChart({
                barColor: $.colors.primary,
                scaleLength: !1,
                trackColor: "#f7f7f7",
                size: 80
            })
//            $("#content").hide();

//            $('#aa').click(function(){
//
//                    $("#unit").show();
//
//            });
        });

        $('.btn').click(function () {
//            $('#content').hide();
            jQuery('.abc').addClass('hide')
            var target = '#' + $(this).data('target');
            $(target).removeClass('hide');

        });
    </script>
@stop
