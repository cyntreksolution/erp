<?php

namespace PurchasingOrderManager\Controllers;

use App\LoadingHeader;
use App\RawMaterialPrice;
use App\Traits\CratesStoreLog;
use App\Traits\RawMaterialLog;
use App\Traits\SupplierLog;
use Carbon\Carbon;
use CashLockerManager\Classes\CashTransaction;
use CashLockerManager\Models\CashBookTransaction;
use CashLockerManager\Models\CashLocker;
use Illuminate\Support\Facades\Storage;
use PassBookManager\Classes\BankTransaction;
use PassBookManager\Models\PassBook;
use PurchasingOrderManager\Models\Content;
use SalesRepManager\Models\SalesRep;
use Sentinel;
use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use PurchasingOrderManager\Models\PurchasingOrder;
use RawMaterialManager\Models\RawMaterial;
use StockManager\Classes\StockTransaction;
use SupplierManager\Models\Supplier;

class PurchasingOrderController extends Controller
{
    use RawMaterialLog,SupplierLog,CratesStoreLog;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:purchasing_order-index', ['only' => ['index']]);
        $this->middleware('permission:EditPriceChange-list', ['only' => ['approvePrice']]);
        $this->middleware('permission:SettlePurchasing-list', ['only' => ['list']]);
        $this->middleware('permission:ApprovePrice-list', ['only' => ['list']]);
        $this->middleware('permission:purchasing_order-create', ['only' => ['create','store']]);
        $this->middleware('permission:purchasing_order-show', ['only' => ['show']]);


    }
    public function index()
    {

        $materials = RawMaterial::all();
        $supliers1 = Supplier::orderBy('supplier_name')->get();
        $datex = Carbon::now()->format('Y-m-d') . '%';
        $supliers = Supplier::orderBy('supplier_name')->take(4)->get();
        $pos = PurchasingOrder::with('supplier', 'purchasingOrderContent.ingredients')
            ->whereRaw("status < 3 or status = 3 and updated_at like '$datex'")
            ->orderBy('status')
            ->orderBy('created_at', 'DESC')
            ->get();
        return view('PurchasingOrderManager::po.index')->with([
            'supliers' => $supliers,
            'supliers1' => $supliers1,
            'materials' => $materials,
            'pos' => $pos
        ]);
    }


    public function approvePrice()
    {
        $supliers1 = Supplier::orderBy('supplier_name')->get();
        $pos = RawMaterialPrice::where('status','=',1)->get();
        return view('PurchasingOrderManager::po.approve-price')->with([
            'pos' => $pos,
            'supliers1' => $supliers1
        ]);

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $products = RawMaterial::where('type','=',0)->withoutGlobalScope('type')->get();
        $productsall = RawMaterial::get();
        $supliers = Supplier::all();

        return view('PurchasingOrderManager::po.create')->with([
            'products' => $products,
            'productsall' => $productsall,
            'supliers' => $supliers,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $supplier_id = $request->supplier_id;
        $user = Auth::user()->id;
        $end_products = $request->ingredients;
        $qty = $request->qty;
        $price = $request->price;
        $date = date('Y-m-d');
        $total = null;
        try {
            DB::transaction(function () use ($supplier_id, $user, $end_products, $qty, $price, $total, $date) {
                $in = DB::select("show table status like 'purchasing_order_header'");
                $in = $in[0]->Auto_increment;
                $po = new PurchasingOrder();
                $po->invoice_no = date('ymd') . $in;
                $po->supplier_id = $supplier_id;
                $po->created_by = $user;
                $po->save();

                foreach ($end_products as $key => $end_product) {
                    $pod = new Content();
                    $pod->purchasing_order_id = $po->id;
                    $pod->raw_material_id = $end_product;
                    $pod->ordered_price = $price[$key];
                    $total = $total + ($price[$key] * $qty[$key]);
                    $raw_material = RawMaterial::withoutGlobalScope('type')->where('raw_material_id', '=', $end_product)->first();
                    $pod->requested_qty = $qty[$key];
                    $pod->save();

                    $rw = RawMaterial::where('raw_material_id','=',$end_product)->first();
                    $rw->is_approved = 3;
                    $rw->save();
                }
                $po->total = $total;
                $po->save();

            });

            return redirect(route('po.index'))->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'New purchasing order has been added'
            ]);
        } catch (Exception $e) {
            return $e;
            return redirect(route('po.index'))->with([
                'error' => true,
                'error.title' => 'Sorry !',
                'error.message' => 'Something Went Wrong '
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \RecipeManager\Models\Recipe $recipe
     * @return \Illuminate\Http\Response
     */
    public function show($purchasingOrder)
    {
        $po = PurchasingOrder::with('supplier', 'purchasingOrderContent.ingredients')
            ->where('id', $purchasingOrder)
            ->get();
        return view('PurchasingOrderManager::po.show')->with([
            'po' => $po,
        ]);
    }

    public function payment($purchasingOrder)
    {
//        return "jgvhghg";
        $bank = PassBook::all();
        $po = PurchasingOrder::with('supplier', 'purchasingOrderContent.ingredients')
            ->where('id', $purchasingOrder)
            ->get();
        return view('PurchasingOrderManager::po.payment')->with([
            'po' => $po,
            'accounts' => $bank
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \RecipeManager\Models\Recipe $recipe
     * @return \Illuminate\Http\Response
     */
    public function edit($purchasingOrder)
    {
        $raw_materials = RawMaterial::all();
        $po = PurchasingOrder::with('supplier', 'purchasingOrderContent.ingredients')
            ->where('id', $purchasingOrder)
            ->get();
        return view('PurchasingOrderManager::po.edit')->with([
            'po' => $po,
            'raw_materials' => $raw_materials,
            'po_id' => $purchasingOrder,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \RecipeManager\Models\Recipe $recipe
     * @return \Illuminate\Http\Response
     */

    public function changePrice(Request $request)
    {


        $price = new RawMaterialPrice();
        $price->raw_material_id = $request->raw_id;
        $price->price = $request->price;
        $price->units_per_packs = $request->unitpack;
        $price->supplier_id = $request->main_sup;
        $price->status = 1;
        $price->save();

        $materials = RawMaterial::all();
        $supliers1 = Supplier::orderBy('supplier_name')->get();
        $datex = Carbon::now()->format('Y-m-d') . '%';
        $supliers = Supplier::orderBy('supplier_name')->take(4)->get();
        $pos = PurchasingOrder::with('supplier', 'purchasingOrderContent.ingredients')
            ->whereRaw("status < 3 or status = 3 and updated_at like '$datex'")
            ->orderBy('status')
            ->orderBy('created_at', 'DESC')
            ->get();
        return view('PurchasingOrderManager::po.index')->with([
            'supliers' => $supliers,
            'supliers1' => $supliers1,
            'materials' => $materials,
            'pos' => $pos
        ]);

    }

    public function updatePrice(Request $request,$po)
    {

        $nprice = RawMaterialPrice::where('id','=',$po)->first();

        $price = RawMaterial::where('raw_material_id','=',$nprice->raw_material_id)->first();
        $price->units_per_packs = $nprice->units_per_packs;
        $price->price = $nprice->price;
        $price->main_supplier_id = $nprice->supplier_id;
        $price->save();


        $nprice->status = 2;
        $nprice->save();

        $supliers1 = Supplier::orderBy('supplier_name')->get();
        $pos = RawMaterialPrice::where('status','=',1)->get();
        return view('PurchasingOrderManager::po.approve-price')->with([
            'pos' => $pos,
            'supliers1' => $supliers1
        ]);

    }

    public function editchangePrice(Request $request)
    {

        $nprice = RawMaterialPrice::where('id','=',$request->po_id)->first();

        $nprice->units_per_packs = $request->unitpack;
        $nprice->price = $request->price;
        $nprice->supplier_id = $request->main_sup;
        $nprice->save();

        $supliers1 = Supplier::orderBy('supplier_name')->get();
        $pos = RawMaterialPrice::where('status','=',1)->get();
        return view('PurchasingOrderManager::po.approve-price')->with([
            'pos' => $pos,
            'supliers1' => $supliers1
        ]);

    }

    public function update(Request $request)
    {


        $po_id = $request->po_id;
        $pod_ids = $request->pd_ids;
        $raw_materials = $request->raw_materials;
        $received_qty = $request->receieved_qty;
        $received_price = $request->recieved_price;

        $date = date('Y-m-d');
        $new_raw = $request->new_raw;
        $new_qty = $request->new_qty;
        $new_price = $request->new_price;
        $total = null;
        $purchasingOrder = null;

        try {
            DB::transaction(function () use ($total, $po_id, $pod_ids, $raw_materials, $received_qty, $received_price, $new_raw, $new_qty, $new_price, $date, &$purchasingOrder) {

                $purchasingOrder = PurchasingOrder::find($po_id);

                $supplier = Supplier::find($purchasingOrder->supplier_id);




                foreach ($pod_ids as $key => $pod_id) {
                    $pod = Content::where('id', '=', $pod_id)->first();
                    $raw_material = RawMaterial::withoutGlobalScopes()->where('raw_material_id', '=', $raw_materials[$key])->first();
                    $pod->received_price = $raw_material->price;
                    $qty = $received_qty[$key];
                    $pod->recieved_qty = $qty;

                    if (!empty($raw_material->units_per_packs)) {
                        $qty = $qty * $raw_material->units_per_packs;
                    }

                    $pod->save();

                    $total = $total + ($pod->received_price * $received_qty[$key]);

                 //   StockTransaction::rawMaterialStockTransaction(1, $pod->received_price, $raw_materials[$key], $qty, $pod_id, 1);
                 //   StockTransaction::updateRawMaterialAvailableQty(1, $raw_materials[$key], $qty);

                 //   if ($raw_material->type==5){
                 //       $this->recordCratesStoreLogOutstanding($raw_material,0,$qty,'Crates Purchasing',$purchasingOrder->id, 'PurchasingOrderManager\\Models\\PurchasingOrder');
                 //   }else{
                 //       $this->recordRawMaterialOutstanding($raw_material, $qty, 'Raw Material Purchasing', $purchasingOrder->id, 'PurchasingOrderManager\\Models\\PurchasingOrder',1,1,);
                 //   }

                }


          //      $purchasingOrder->total = $total;
          //      $purchasingOrder->received_date = $date;
//                $purchasingOrder->status = 2;
          //      $purchasingOrder->save();

          //      StockTransaction::updateSupplierBalance(1,$purchasingOrder->supplier_id,$total);
          //      $this->recordSupplierOutstanding($supplier,$total,'Purchasing Order',$purchasingOrder->id,'PurchasingOrderManager\\Model\\PurchasingOrder');


            });

            return redirect()->route('po.preview', $purchasingOrder->id);



        } catch (Exception $e) {
            return $e;
            return redirect(route('po.index'))->with([
                'error' => true,
                'error.title' => 'Sorry !',
                'error.message' => 'Something Went Wrong '
            ]);
        }
    }


    public function preview(Request $request, PurchasingOrder $po)
    {
        return view('PurchasingOrderManager::po.preview', compact('po'));
    }

    public function updateQTN($purchasingOrder)
    {

    $data = Content::where('purchasing_order_id','=',$purchasingOrder)->get();
        foreach ($data as $key => $datas) {
            $data1 = Content::find($datas->id);
                $raw = RawMaterial::where('raw_material_id','=',$data1->raw_material_id)->first();

                $data1->ordered_price = $raw->price;
                $data1->save();
        }

        return redirect(route('po.index'))->with([
            'success' => true,
            'success.title' => 'Wow !',
            'success.message' => 'Purchasing Order Has Received ! '
        ]);

    }

    public function confirm(Request $request)
    {

        $po_id = $request->po_id;
        $nextdate = $request->ndate;
        $ref = $request->ref;

        $pod_ids = $request->pd_ids;
        $raw_materials = $request->raw_materials;
        $received_qty = $request->receieved_qty;
        $received_price = $request->recieved_price;

        $date = date('Y-m-d');
        $new_raw = $request->new_raw;
        $new_qty = $request->new_qty;
        $new_price = $request->new_price;
        $total = null;
        $purchasingOrder = null;

        try {
            DB::transaction(function () use ($total, $po_id, $pod_ids, $raw_materials, $received_qty,$nextdate,$ref, $received_price, $new_raw, $new_qty, $new_price, $date, $purchasingOrder) {

                $purchasingOrder = PurchasingOrder::find($po_id);

                $supplier = Supplier::find($purchasingOrder->supplier_id);

              //  $agent = SalesRep::first();



                foreach ($pod_ids as $key => $pod_id) {
                    $pod = Content::where('id', '=', $pod_id)->first();
                    $raw_material = RawMaterial::withoutGlobalScopes()->where('raw_material_id', '=', $raw_materials[$key])->first();
                    $pod->received_price = $raw_material->price;
                    $qty = $received_qty[$key];
                    $pod->recieved_qty = $qty;

                    if (!empty($raw_material->units_per_packs)) {
                        $qty = $qty * $raw_material->units_per_packs;
                    }

                   // $pod->save();

                    $rw = RawMaterial::where('raw_material_id','=',$raw_materials[$key])->first();
                    $rw->is_approved = 0;
                    $rw->save();


                    $total = $total + ($pod->received_price * $received_qty[$key]);

                       StockTransaction::rawMaterialStockTransaction(1, $pod->received_price, $raw_materials[$key], $qty, $pod_id, 1);
                       StockTransaction::updateRawMaterialAvailableQty(1, $raw_materials[$key], $qty);

                       if ($raw_material->type==5){


                           $this->recordCratesStoreLogOutstanding($raw_material,$qty,'Crates Purchasing',$purchasingOrder->id, 'PurchasingOrderManager\\Models\\PurchasingOrder',1,1,0);
                           $this->recordRawMaterialOutstanding($raw_material, $qty, 'Crates Purchasing', $purchasingOrder->id, 'PurchasingOrderManager\\Models\\PurchasingOrder',11,1,$purchasingOrder->supplier_id);

                       }else{
                           $this->recordRawMaterialOutstanding($raw_material, $qty, 'Raw Material Purchasing', $purchasingOrder->id, 'PurchasingOrderManager\\Models\\PurchasingOrder',1,1,$purchasingOrder->supplier_id);
                       }

                }

                $purchasingOrder->next_visit = $nextdate;
                $purchasingOrder->sup_invoice_no = $ref;
                        $purchasingOrder->total = $total;
                        $purchasingOrder->received_date = $date;
                        if (!$purchasingOrder->is_paid) {
                        $purchasingOrder->status = 2;
                        }
                        $purchasingOrder->save();

                      StockTransaction::updateSupplierBalance(1,$purchasingOrder->supplier_id,$total);
                      $this->recordSupplierOutstanding($supplier,$total,'Purchasing Order',$purchasingOrder->id,'PurchasingOrderManager\\Model\\PurchasingOrder');


            });

            return redirect(route('po.index'))->with([
                'success' => true,
                'success.title' => 'Wow !',
                'success.message' => 'Purchasing Order Has Received ! '
            ]);


        } catch (Exception $e) {
            return $e;
            return redirect(route('po.index'))->with([
                'error' => true,
                'error.title' => 'Sorry !',
                'error.message' => 'Something Went Wrong '
            ]);
        }


    }


    /**
     * Remove the specified resource from storage.
     *
     * @param \RecipeManager\Models\Recipe $recipe
     * @return \Illuminate\Http\Response
     */
    public function destroy(PurchasingOrder $purchasingOrder)
    {
        $purchasingOrder->delete();
        if ($purchasingOrder->trashed()) {
            return 'true';
        } else {
            return 'false';
        }
    }

    public function getlastprice(Request $request)
    {
        $lastprice = RawMaterial::find($request->id);
        return $lastprice;
    }

    public function dateReport()
    {

        $raw = RawMaterial::all();
        $sup = Supplier::all();
        return view('PurchasingOrderManager::po.date')->with([
            'raw_materials' => $raw,
            'suppliers' => $sup,

        ]);


    }

    public function getReport(Request $request)
    {
        $start_date = substr($request->date, 0, 10);
        $start_date = date_create($start_date);
        $start_date = date_format($start_date, "Y-m-d");

        $end_date = substr($request->date, 12, 23);
        $end_date = date_create($end_date);
        $end_date = date_format($end_date, "Y-m-d");

        $product = $request->product;
        if ($product == 0) {
            $burdens = Burden::whereBetween('created_at', array($start_date, $end_date))->with('semiProdcuts')->get();
            $array = [];
            foreach ($burdens as $burden) {
                $burdenx = [
                    'id' => $burden['id'],
                    'burden_size' => $burden['burden_size'],
                    'expected_semi_product_qty' => $burden['expected_semi_product_qty'],
                    'name' => $burden['semiProdcuts']->name,
                ];
                array_push($array, $burdenx);
            }
            return $array;

        } else {
            $burdens = Burden::whereBetween('created_at', array($start_date, $end_date))->with('semiProdcuts')
                ->where('semi_finish_product_id', '=', $product)->get();
            $array = [];
            foreach ($burdens as $burden) {
                $burdenx = [
                    'id' => $burden['id'],
                    'burden_size' => $burden['burden_size'],
                    'expected_semi_product_qty' => $burden['expected_semi_product_qty'],
                    'name' => $burden['semiProdcuts']->name,
                ];
                array_push($array, $burdenx);
            }
            return $array;
        }
    }

    public function pdfViewPO($id)
    {
        $data = DB::table('purchasing_order_header')
            ->select('supplier.supplier_name AS customer_name', 'supplier.*', 'raw_material.*', 'purchasing_order_header.*', 'purchasing_order_data.*')
            ->leftJoin('supplier', 'purchasing_order_header.supplier_id', '=', 'supplier.id')
            ->rightJoin('purchasing_order_data', 'purchasing_order_data.purchasing_order_id', '=', 'purchasing_order_header.id')
            ->rightJoin('raw_material', 'raw_material.raw_material_id', '=', 'purchasing_order_data.raw_material_id')
            ->where('purchasing_order_header.id', '=', $id)
            ->get();

        $iNo = PurchasingOrder::where('id', '=', $id)->first();
        $pdf = PDF::loadView('PurchasingOrderManager::po.po', compact('data'))->setPaper('a4', 'portrait');
        return $pdf->stream();
        return $pdf->download($iNo['invoice_no'] . '-PO.pdf');
    }

    public function pdfDownloadPO($id)
    {
        $data = DB::table('purchasing_order_header')
            ->select('supplier.supplier_name AS customer_name', 'supplier.*', 'raw_material.*', 'purchasing_order_header.*', 'purchasing_order_data.*')
            ->leftJoin('supplier', 'purchasing_order_header.supplier_id', '=', 'supplier.id')
            ->rightJoin('purchasing_order_data', 'purchasing_order_data.purchasing_order_id', '=', 'purchasing_order_header.id')
            ->rightJoin('raw_material', 'raw_material.raw_material_id', '=', 'purchasing_order_data.raw_material_id')
            ->where('purchasing_order_header.id', '=', $id)
            ->get();

        $iNo = PurchasingOrder::where('id', '=', $id)->first();
        $pdf = PDF::loadView('PurchasingOrderManager::po.po', compact('data'))->setPaper('a4', 'portrait');
        return $pdf->download($iNo['invoice_no'] . '-PO.pdf');
    }

    public function pdfViewGRN($id)
    {
        $data = DB::table('purchasing_order_header')
            ->select('supplier.supplier_name AS customer_name', 'supplier.*', 'raw_material.*', 'purchasing_order_header.*', 'purchasing_order_data.*')
            ->leftJoin('supplier', 'purchasing_order_header.supplier_id', '=', 'supplier.id')
            ->rightJoin('purchasing_order_data', 'purchasing_order_data.purchasing_order_id', '=', 'purchasing_order_header.id')
            ->rightJoin('raw_material', 'raw_material.raw_material_id', '=', 'purchasing_order_data.raw_material_id')
            ->where('purchasing_order_header.id', '=', $id)
            ->get();

        $iNo = PurchasingOrder::where('id', '=', $id)->first();
        $pdf = PDF::loadView('PurchasingOrderManager::po.grn', compact('data'))->setPaper('a4', 'portrait');
        return $pdf->stream();
        return $pdf->download($iNo['invoice_no'] . '-GRN.pdf');
    }

    public function pdfDownloadGRN($id)
    {
        $data = DB::table('purchasing_order_header')
            ->select('supplier.supplier_name AS customer_name', 'supplier.*', 'raw_material.*', 'purchasing_order_header.*', 'purchasing_order_data.*')
            ->leftJoin('supplier', 'purchasing_order_header.supplier_id', '=', 'supplier.id')
            ->rightJoin('purchasing_order_data', 'purchasing_order_data.purchasing_order_id', '=', 'purchasing_order_header.id')
            ->rightJoin('raw_material', 'raw_material.raw_material_id', '=', 'purchasing_order_data.raw_material_id')
            ->where('purchasing_order_header.id', '=', $id)
            ->get();

        $iNo = PurchasingOrder::where('id', '=', $id)->first();
        $pdf = PDF::loadView('PurchasingOrderManager::po.grn', compact('data'))->setPaper('a4', 'portrait');
        return $pdf->download($iNo['invoice_no'] . '-GRN.pdf');
    }

    public function pdfViewPaid($id)
    {
        $data = DB::table('purchasing_order_header')
            ->select('supplier.supplier_name AS customer_name', 'supplier.*', 'raw_material.*', 'purchasing_order_header.*', 'purchasing_order_data.*')
            ->leftJoin('supplier', 'purchasing_order_header.supplier_id', '=', 'supplier.id')
            ->rightJoin('purchasing_order_data', 'purchasing_order_data.purchasing_order_id', '=', 'purchasing_order_header.id')
            ->rightJoin('raw_material', 'raw_material.raw_material_id', '=', 'purchasing_order_data.raw_material_id')
            ->where('purchasing_order_header.id', '=', $id)
            ->get();

        $iNo = PurchasingOrder::where('id', '=', $id)->first();
        $pdf = PDF::loadView('PurchasingOrderManager::po.paid', compact('data'))->setPaper('a4', 'portrait');
//        return view('PurchasingOrderManager::po.paid')->with(['data'=>$data]);
        return $pdf->stream();
        return $pdf->download($iNo['invoice_no'] . '-PO-PAID.pdf');
    }

    public function pdfDownloadPaid($id)
    {
        $data = DB::table('purchasing_order_header')
            ->select('supplier.supplier_name AS customer_name', 'supplier.*', 'raw_material.*', 'purchasing_order_header.*', 'purchasing_order_data.*')
            ->leftJoin('supplier', 'purchasing_order_header.supplier_id', '=', 'supplier.id')
            ->rightJoin('purchasing_order_data', 'purchasing_order_data.purchasing_order_id', '=', 'purchasing_order_header.id')
            ->rightJoin('raw_material', 'raw_material.raw_material_id', '=', 'purchasing_order_data.raw_material_id')
            ->where('purchasing_order_header.id', '=', $id)
            ->get();

        $iNo = PurchasingOrder::where('id', '=', $id)->first();
        $pdf = PDF::loadView('PurchasingOrderManager::po.paid', compact('data'))->setPaper('a4', 'portrait');
        return $pdf->download($iNo['invoice_no'] . '-PO-PAID.pdf');
    }

    public function pay(Request $request)
    {
        $po = $request->po;
        $total = $request->total;
        $poh = PurchasingOrder::where('id', $po)->with('supplier')->first();

        $supplier = Supplier::find($poh->supplier->id);

        if ($request->pmethod) {
            $acc = $request->account;
            $chequeNo = $request->chequeNo;
            $chequeDate = $request->chequeDate;
            $data = BankTransaction::issuCheque(2, $total, $acc, $chequeNo, $chequeDate, Carbon::now(), $poh->supplier_id, '');
            $poh->status = 3;
            $poh->is_paid = 1;
            $poh->paid_at = Carbon::now()->format('Y-m-d H:i:s');
            $poh->save();
            $this->recordSupplierOutstanding($supplier,-$total,'Supplier Payment Cheque',$data->id,'PassBookManager\\Models\\Cheque');

        } else {
            $description = $poh['supplier']->supplier_name . ' Invoice No ' . $poh->invoice_no . ' Paid';
            CashTransaction::updateCashLocker(2, $total);
            $data = CashTransaction::updateCashTransaction(2, $total, $poh->id, 1, $description, -2, $supplier->id,'SupplierPayments','Supliers');
            $poh->status = 3;
            $poh->is_paid = 1;
            $poh->paid_at = Carbon::now()->format('Y-m-d H:i:s');
            $poh->save();

            $this->recordSupplierOutstanding($supplier,-$total,'Supplier Payment Cash',$data->id,'CashLockerManager\\Models\\CashBookTransaction');

        }

        StockTransaction::updateSupplierBalance(2,$poh->supplier_id,$total);

        return redirect(route('po.index'));
    }

    public function getSupplierMaterial(Request $request){
        $supplier_id =$request->id;
        $raw = RawMaterial::withoutGlobalScopes()->where('main_supplier_id',$supplier_id)->where('is_approved','=',2)->get();
        return $raw;
    }


    public function printGRN($purchasingOrder)
    {

        $invoice = PurchasingOrder::whereId($purchasingOrder)->first();
        $items = Content::where('purchasing_order_id', '=', $invoice->id)->get();
      //  return view('po.show-grn', compact('items', 'invoice'));

        return view('PurchasingOrderManager::po.show-grn')->with([
            'invoice' => $invoice,
            'items' => $items,

        ]);
        //  $purchasedHeaderId = $purchasingOrder;
      //  $filename = $purchasedHeaderId . '.pdf';

        // $loading = LoadingHeader::find($loadingHeaderId);

      //  return $this->getPDFGRN($filename);
    }

    public function printQTN($purchasingOrder)
    {

        $invoice = PurchasingOrder::whereId($purchasingOrder)->first();
        $items = Content::where('purchasing_order_id', '=', $invoice->id)->get();
        //  return view('po.show-grn', compact('items', 'invoice'));

        return view('PurchasingOrderManager::po.show-qtn')->with([
            'invoice' => $invoice,
            'items' => $items,

        ]);
        //  $purchasedHeaderId = $purchasingOrder;
        //  $filename = $purchasedHeaderId . '.pdf';

        // $loading = LoadingHeader::find($loadingHeaderId);

        //  return $this->getPDFGRN($filename);
    }



}
