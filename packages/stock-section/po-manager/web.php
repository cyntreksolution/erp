<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web','auth'])->group(function () {
    Route::prefix('po')->group(function () {
        Route::get('', 'PurchasingOrderManager\Controllers\PurchasingOrderController@index')->name('po.index');

        Route::get('approvePrice', 'PurchasingOrderManager\Controllers\PurchasingOrderController@approvePrice')->name('po.approve-price');


        Route::get('create', 'PurchasingOrderManager\Controllers\PurchasingOrderController@create')->name('po.create');
        Route::post('cngPrice', 'PurchasingOrderManager\Controllers\PurchasingOrderController@changePrice')->name('po.cngPrice');
        Route::post('editcngPrice', 'PurchasingOrderManager\Controllers\PurchasingOrderController@editchangePrice')->name('po.edit-cngPrice');


        Route::get('/price/{po}', 'PurchasingOrderManager\Controllers\PurchasingOrderController@updatePrice')->name('po.updatePrice');

        Route::get('{po}', 'PurchasingOrderManager\Controllers\PurchasingOrderController@show')->name('po.show');
        Route::get('preview/{po}', 'PurchasingOrderManager\Controllers\PurchasingOrderController@preview')->name('po.preview');

        Route::get('{po}/payment', 'PurchasingOrderManager\Controllers\PurchasingOrderController@payment')->name('po.payment');
        Route::post('{po}/pay', 'PurchasingOrderManager\Controllers\PurchasingOrderController@pay')->name('po.pay');

        Route::get('{po}/edit', 'PurchasingOrderManager\Controllers\PurchasingOrderController@edit')->name('po.edit');

        Route::get('{po}/pdfViewPO', 'PurchasingOrderManager\Controllers\PurchasingOrderController@pdfViewPO')->name('po.po');
        Route::get('date/view', 'PurchasingOrderManager\Controllers\PurchasingOrderController@dateReport')->name('po.date');

        Route::get('{po}/pdfDownloadPO', 'PurchasingOrderManager\Controllers\PurchasingOrderController@pdfDownloadPO')->name('po.po');
        Route::post('date', 'PurchasingOrderManager\Controllers\PurchasingOrderController@getReport')->name('po.date');

        Route::get('{po}/pdfViewGRN', 'PurchasingOrderManager\Controllers\PurchasingOrderController@pdfViewGRN')->name('po.grn');


        Route::get('{po}/pdfDownloadGRN', 'PurchasingOrderManager\Controllers\PurchasingOrderController@pdfDownloadGRN')->name('po.grn');

        Route::get('{po}/pdfViewPaid', 'PurchasingOrderManager\Controllers\PurchasingOrderController@pdfViewPaid')->name('po.paid');

        Route::get('{po}/pdfDownloadPaid', 'PurchasingOrderManager\Controllers\PurchasingOrderController@pdfDownloadPaid')->name('po.paid');

        Route::get('{po}/grnview', 'PurchasingOrderManager\Controllers\PurchasingOrderController@printGRN')->name('po.show-grn');

        Route::get('{po}/updateqtn', 'PurchasingOrderManager\Controllers\PurchasingOrderController@updateQTN')->name('po.update-qtn');


        Route::get('{po}/qtnview', 'PurchasingOrderManager\Controllers\PurchasingOrderController@printQTN')->name('po.show-qtn');


        Route::post('', 'PurchasingOrderManager\Controllers\PurchasingOrderController@store')->name('po.store');

        Route::post('getlastprice', 'PurchasingOrderManager\Controllers\PurchasingOrderController@getlastprice')->name('po.getlastprice');

        Route::post('{po}', 'PurchasingOrderManager\Controllers\PurchasingOrderController@update')->name('po.update');
        Route::post('confirm/{po}', 'PurchasingOrderManager\Controllers\PurchasingOrderController@confirm')->name('po.confirm');

        Route::delete('{po}', 'PurchasingOrderManager\Controllers\PurchasingOrderController@destroy')->name('po.destroy');
    });
});
