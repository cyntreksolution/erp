<?php

namespace PurchasingOrderManager;

use Illuminate\Support\ServiceProvider;

class PurchasingOrderServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/Views', 'PurchasingOrderManager');
        require __DIR__ . '/web.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('PurchasingOrderManager', function($app){
            return new PurchasingOrderManager;
        });
    }
}
