<html>
<head>
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/invoice.css')}}" rel="stylesheet">
    <style media="all">
        @page {
            margin: 30px 5px 30px 5px;
            size: 75mm 999mm;
        }
    </style>
</head>

<body style="margin-bottom: 100px">

<div class="row">
    <div class="row" style="margin-left: 50px">
        <div class="col-xs-5">
            <img src="{{asset('assets/img/buntak_grace.jpeg')}}" style="width: 130px">
        </div>

    <!--div class="col-xs-6">
        <img src="{{--asset('assets/img/buntak_grace.jpeg')--}}" style="width: 150px">
    </div-->

        <div class="col-xs-7 text-right">
            <h6>
                <b>Ranjanas Holdings [Pvt] Ltd</b><br>
                Katukurunda<br>
                Habaraduwa<br>
                Hotline : +9477 330 4678<br>
                E-mail : buntalksl@icloud.com<br>
            </h6>
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-12">
        @php $sup = \SupplierManager\Models\Supplier::whereId($invoice->supplier_id)->first();


        @endphp

        <div class="row">
            <div class="col-xs-6 text-nowrap"><b>Supplier Name</b></div>
            <div class="col-xs-6">:{{$sup->supplier_name}}</div>
        </div>
        <div class="row">
            <div class="col-xs-6 text-nowrap"><b>Address</b></div>
            <div class="col-xs-6">:{{$sup->address}}</div>
        </div>
        <div class="row">
            <div class="col-xs-6 text-nowrap"><b>Contact No</b></div>
            <div class="col-xs-6">:{{$sup->mobile}}</div>
        </div>
        <div class="row">
            <div class="col-xs-6 text-nowrap"><b>Order Date</b></div>
            <div class="col-xs-6">:{{$invoice->created_at}}</div>
        </div>
        @if(!empty($invoice->received_date))
        <div class="row">
            <div class="col-xs-6 text-nowrap"><b>GRN Date</b></div>
            <div class="col-xs-6">:{{$invoice->received_date}}</div>
        </div>
        @endif
        <div class="row">
            <div class="col-xs-6 text-nowrap"><b>Invoice Number</b></div>
            <div class="col-xs-6">:{{$invoice->sup_invoice_no}}</div>
        </div>
        <div class="row">
            <div class="col-xs-6 text-nowrap"><b>Refference Number</b></div>
            <div class="col-xs-6">:{{$invoice->invoice_no}}</div>
        </div>
        @if(!empty($invoice->next_visit))
        <div class="row">
            <div class="col-xs-6 text-nowrap"><b>Next Visit Date</b></div>
            <div class="col-xs-6">:{{$invoice->next_visit}}</div>
        </div>
        @endif


    </div>
</div>
<br>

<div class="col-xl-12 text-center">
    @if($invoice->status == 1)
    <h3><b><u>

                Order
            </u></b></h3>
    @else
        <h3><b><u>

                    Invoice
                </u></b></h3>
    @endif
</div>

<div class="row">
    <div class="col-xs-12">
        <table class="table">
            <thead>
            <tr>
                <th>Item with price</th>
                <th class="text-center">QTY</th>
                <th class="text-right">Value</th>

            </tr>
            </thead>
            <tbody>
            @php $counter= 0;  $ordval = 0 ;  @endphp
            @foreach($items as $key => $product)

                    <tr>

                        @php $raw = \RawMaterialManager\Models\RawMaterial::where('raw_material_id','=',$product->raw_material_id)->first(); @endphp

                        @if($invoice->status == 1)

                            <td>{{$raw->name." (".number_format((float)$product->ordered_price, 2, '.', '').")"}}</td>
                        <td class="text-center">{{$product->requested_qty}} - {{$raw->pack_type}}</td>
                        <td class="text-right">{{number_format((($product->ordered_price)*($product->requested_qty)),2)}}</td>
                            @php    $ordval1 = (($product->ordered_price)*($product->requested_qty));  @endphp
                        @else
                            @if(empty($invoice->received_date))
                                <td>{{$raw->name." (".number_format((float)$product->ordered_price, 2, '.', '').")"}}</td>
                                <td class="text-center">{{$product->requested_qty}} - {{$raw->pack_type}}</td>
                                <td class="text-right">{{number_format((($product->ordered_price)*($product->requested_qty)),2)}}</td>
                                @php    $ordval1 = (($product->ordered_price)*($product->requested_qty));  @endphp

                            @else
                                <td>{{$raw->name." (".number_format((float)$product->received_price, 2, '.', '').")"}}</td>
                                <td class="text-center">{{$product->recieved_qty}} - {{$raw->pack_type}}</td>
                                <td class="text-right">{{number_format((($product->received_price)*($product->recieved_qty)),2)}}</td>
                                @php   $ordval1 = (($product->received_price)*($product->recieved_qty));  @endphp
                            @endif
                        @endif
                        <!--td align="right">
                            <div style="width:150px;height:30px;border:1px solid #000;"></div>
                        </td-->


                @php  $counter = $counter+1 ; $ordval = $ordval1 + $ordval;  @endphp
                    </tr>
            @endforeach
            <tr>
            <td></td>
                @if($invoice->status == 1)
                <td class="text-left"><b>Ordered Value</b></td>
                @elseif($invoice->status == 2)
                    <td class="text-left"><b>Amount Due</b></td>
                @else
                    <td class="text-left"><b>Invoice Value</b></td>
                @endif
            <td class="text-right"><b>{{number_format(($ordval),2)}}</b></td>
            </tr>
            </tbody>
        </table>
    </div>

</div>
<div class="row">
    <div class="col-xs-12 pull-right text-right">
        <h4> Number Of Items : {{$counter}} </h4>
    </div>
</div>
<br>

<footer>

    <div class="row" style="margin-top: 80px; margin-left: 50px">

        <div class="col-xs-9">
            <h5>
                .........................................<br>
                           Signature-Supplier
            </h5>
        </div>
    </div>
    <div class="row" style="margin-top: 80px; margin-left: 50px">
        <div class="col-xs-9">
            <h5>
                .........................................<br>
                          Signature-Checked By
            </h5>
        </div>
    </div>
    <div class="row" style="margin-top: 80px; margin-left: 50px">

        <div class="col-xs-9">
            <h5>
                .........................................<br>
                         Signature-Authorized By
            </h5>
        </div>

    </div>


        @if (!empty($invoice->received_date) && $invoice->is_paid)
        <div class="row" style="margin-left: 50px">
            <div class="col-xs-9">
                <img src="{{asset('assets/img/Recieved.png')}}" width="600" height="200">
            </div>
        </div>

        @elseif(!empty($invoice->received_date) && $invoice->is_paid == 0)
        <div class="row" style="margin-left: 50px">
            <div class="col-xs-9 float-left">
                <img src="{{asset('assets/img/Recieved.png')}}" width="600" height="200">
            </div>
        </div>
        @endif
        @if ($invoice->is_paid)
        <div class="row" style="margin-left: 50px">
            <div class="col-xs-9">
                <img src="{{asset('assets/img/paid-stamp.png')}}" width="600" height="200">
            </div>
        </div>
        @endif



    <!--p style="margin-top: 150px">--------------------------------------------------------------</p-->
</footer>

</body>

</html>
