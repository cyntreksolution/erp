@extends('layouts.back.master')@section('title','Purchasing order')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}">


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .app-wrapper {
            font-family: Raleway, sans-serif;
            width: 100%;
            border: 1px solid #eee;
        }
    </style>
@stop
@section('content')
    <div class="app-wrapper">
        <div class="profile-section-user" id="app-panel">
            @foreach($po as $p)
                <div class="profile-cover-img">
                </div>
                <div class="profile-info-brief p-3">
                        <div class="text-center"><h5 class="text-uppercase mb-1">{{$p['supplier']->supplier_name}}</h5>

                            @if($p->status==1)
                                <div class="hidden-sm-down ">
                                    <div>{{$p['supplier']->mobile}}</div>
                                    <div>NO #{{$p->id}}</div>
                                    <div>{{$p->created_at}}</div>
                                    <button class="btn btn-outline-primary btn-sm m-2 btn-rounded " disabled>
                                        <i class="fa fa-pause px-1" aria-hidden="true"></i>
                                        PENDING
                                    </button>
                                </div>
                                <button class="btn btn-outline-primary btn-sm m-2 btn-rounded ">

                                    <a href="{{$p->id}}/pdfViewPO" aria-hidden="true">
                                        <i class="fa fa-eye px-1"></i>
                                        PO-VIEW
                                    </a>
                                </button>
                                <button class="btn btn-outline-primary btn-sm m-2 btn-rounded ">

                                    <a href="{{$p->id}}/pdfDownloadPO" aria-hidden="true">
                                        <i class="fa fa-print px-1"></i>
                                        DOWNLOAD
                                    </a>
                                </button>
                                <hr class="m-0 py-2">
                                <div class="d-flex justify-content-center flex-wrap p-2">
                                    <a href="{{$p->id}}/edit" class="btn btn-block btn-success btn-sm m-2 text-white">
                                        RECEIVE</a>
                                </div>
                                @if($role = Sentinel::check()->roles[0]->slug=='accountant' || $role = Sentinel::check()->roles[0]->slug=='owner')
                                    @if (!$p->is_paid)

                                        <div class="d-flex justify-content-center flex-wrap p-2">
                                            <a href="{{$p->id}}/payment"
                                               class="btn btn-block btn-danger btn-sm m-2 text-white">
                                                PAY</a>
                                        </div>
                                    @endif
                                @endif



                                <div class="d-flex justify-content-center flex-wrap p-2">
                                    <a href="{{$p->id}}/grnview"
                                       class="btn btn-block btn-warning btn-sm m-2 text-white">
                                        GRN View</a>
                                    <!--input type="hidden" id="grnid" value="{{$p->id}}">
                                    <a onclick="grnPrint()" class="btn btn-block btn-warning btn-sm m-2 text-white" id="grnprintBTN" target="_blank">GRN View</a-->
                                </div>

                                <div class="d-flex justify-content-center flex-wrap p-2">

                                    <a href="{{$p->id}}/qtnview"
                                       class="btn btn-block btn-info btn-sm m-2 text-white">
                                        Call Quotation</a>

                                    <!--a onclick="qtnPrint()" class="btn btn-block btn-info btn-sm m-2 text-white" id="qtnprintBTN" target="_blank">Call Quotation</a-->


                                </div>

                            @elseif($p->status==2)
                                <div class="hidden-sm-down ">
                                    <div>{{$p['supplier']->mobile}}</div>
                                    <div>NO #{{$p->id}}</div>
                                    <div>{{$p->created_at}}</div>
                                    <button class="btn btn-outline-primary btn-sm m-2 btn-rounded " disabled>
                                        <i class="fa fa-pause px-1" aria-hidden="true"></i>
                                        RECEIVED
                                    </button>
                                </div>
                                <button class="btn btn-outline-primary btn-sm m-2 btn-rounded ">

                                    <a href="{{$p->id}}/pdfViewGRN" aria-hidden="true">
                                        <i class="fa fa-eye px-1"></i>
                                        GRN-VIEW
                                    </a>
                                </button>
                                <button class="btn btn-outline-primary btn-sm m-2 btn-rounded ">

                                    <a href="{{$p->id}}/pdfDownloadGRN" aria-hidden="true">
                                        <i class="fa fa-print px-1"></i>
                                        DOWNLOAD
                                    </a>
                                </button>
                                @if($role = Sentinel::check()->roles[0]->slug=='accountant' || $role = Sentinel::check()->roles[0]->slug=='owner')
                                    @if (!$p->is_paid)
                                        <hr class="m-0 py-2">
                                        <div class="d-flex justify-content-center flex-wrap p-2">
                                            <a href="{{$p->id}}/payment"
                                               class="btn btn-block btn-success btn-sm m-2 text-white">
                                                PAY</a>
                                        </div>


                                    @endif
                                @endif

                                <div class="d-flex justify-content-center flex-wrap p-2">
                                    <a href="{{$p->id}}/grnview"
                                       class="btn btn-block btn-warning btn-sm m-2 text-white">
                                        GRN View</a>
                                </div>

                               {{-- $btn_download = "<a href='" . route('loading.gpdf', $record->loadingHeader->id) . "' class='btn btn-sm btn-outline-danger' style='margin-left:10px;'>Print</a>"; --}}

                            @elseif($p->status==3)


                                <div class="hidden-sm-down ">
                                    <div>{{$p['supplier']->mobile}}</div>
                                    <div>NO #{{$p->id}}</div>
                                    <div>{{$p->created_at}}</div>
                                    <button class="btn btn-outline-primary btn-sm m-2 btn-rounded " disabled>
                                        <i class="fa fa-pause px-1" aria-hidden="true"></i>PAID
                                    </button>

                                    @if ($p->received_date==null)
                                        <button class="btn btn-outline-danger btn-sm m-2 btn-rounded " disabled>
                                          PENDING
                                        </button>
                                    @else
                                        <button class="btn btn-outline-primary btn-sm m-2 btn-rounded " disabled>
                                           RECEIVED
                                        </button>
                                    @endif


                                </div>
                                <button class="btn btn-outline-primary btn-sm m-2 btn-rounded ">

                                    <a href="{{$p->id}}/pdfViewPaid" aria-hidden="true">
                                        <i class="fa fa-eye px-1"></i>
                                        PAID-PO-VIEW
                                    </a>
                                </button>
                                <button class="btn btn-outline-primary btn-sm m-2 btn-rounded ">

                                    <a href="{{$p->id}}/pdfDownloadPaid" aria-hidden="true">
                                        <i class="fa fa-print px-1"></i>
                                        DOWNLOAD
                                    </a>
                                </button>
                                @if ($p->received_date==null)
                                    <hr class="m-0 py-2">
                                    <div class="d-flex justify-content-center flex-wrap p-2">
                                        <a href="{{$p->id}}/edit" class="btn btn-block btn-success btn-sm m-2 text-white">
                                            RECEIVE</a>
                                    </div>
                                @endif

                                <div class="d-flex justify-content-center flex-wrap p-2">
                                    <a href="{{$p->id}}/grnview"
                                       class="btn btn-block btn-warning btn-sm m-2 text-white">
                                        GRN View</a>
                                <!--input type="hidden" id="grnid" value="{{$p->id}}">
                                    <a onclick="grnPrint()" class="btn btn-block btn-warning btn-sm m-2 text-white" id="grnprintBTN" target="_blank">GRN View</a-->
                                </div>

                            @endif

                        </div>
                </div>
                <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show"><i
                            class="fa fa-chevron-right"></i>
                    <i class="fa fa-chevron-left"></i>
                </a>
            @endforeach
        </div>
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">ORDERED PRODUCTS</h5>
            </div>
            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">
                    <div class="container">
                        <div class="projects-list">

                            <!--div class="row"-->
                            <table><tr>
                                    <td><h5><b>Name</b></h5></td>
                                    <td><h5><b>Pack Type</b></h5></td>
                                    <td><h5><b>Ordered Qty</b></h5></td>
                                    <td><h5><b>Ordered Price</b></h5></td>
                                    <td><h5><b>Received Qty</b></h5></td>
                                    <td><h5><b>Received Price</b></h5></td>

                                </tr>
                                @php $oqty=0; $oprice=0; $rqty=0; $rprice=0;   @endphp

                                @foreach($po as $p)
                                    @foreach($p['purchasingOrderContent'] as $item)


                                            <tr><td>
                                                <h5 class="mt-3 mx-3 text-left"> {{$item['ingredients']->name}}</h5>
                                            </td>
                                            <td>
                                                <h5 class="mt-1 mx-1 text-center"> {{$item['ingredients']->pack_type}}</h5>
                                            </td>
                                            <td>
                                                <h5 class="mt-2 mx-2 text-center"> {{number_format($item->requested_qty,3)}}</h5>
                                            </td>

                                            <td>
                                                <h5 class="mt-2 mx-2 text-right"> {{number_format($item->ordered_price,2)}}</h5>
                                            </td>
                                            <td>
                                                <h5 class="mt-2 mx-2 text-center"> {{number_format($item->recieved_qty,3)}}</h5>
                                            </td>
                                            <td>
                                                <h5 class="mt-2 mx-2 text-right"> {{number_format($item->received_price,2)}}</h5>
                                            </td></tr>

                                        @php

                                        $oqty = $oqty + $item->requested_qty;
                                        $oprice = $oprice + $item->ordered_price;
                                        $rqty = $rqty + $item->recieved_qty;
                                        $rprice = $rprice + $item->received_price;

                                        @endphp
                                        {{--<hr class="m-1">--}}
                                    @endforeach
                                @endforeach

                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td class="text-center"><h5><b>{{number_format($oqty,2)}}</b></h5></td>
                                    <td class="text-right"><h5><b>{{number_format($oprice,3)}}</b></h5></td>
                                    <td class="text-center"><h5><b>{{number_format($rqty,2)}}</b></h5></td>
                                    <td class="text-right"><h5><b>{{number_format($rprice,3)}}</b></h5></td>

                                </tr>
                            </table>
                        </div>
                        @if($p->status==1)
                        <div class="d-flex justify-content-center flex-wrap p-2">
                            <a href="{{$p->id}}/updateqtn"
                               class="btn btn-block btn-danger btn-lg m-2 text-white">
                                Update Quotation Price</a>
                        <!--input type="hidden" id="grnid" value="{{$p->id}}">
                                    <a onclick="grnPrint()" class="btn btn-block btn-warning btn-sm m-2 text-white" id="grnprintBTN" target="_blank">GRN View</a-->
                        </div>
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </div>

@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $('.delete').click(function (e) {
            e.preventDefault();
            id = $(this).attr("value");
            confirmAlert(id);

        });

        function grnPrint()
        {

            let grnid = $("#grnid").val();
              window.location.href = "/grn/print/download?grnid="+ grnid ;


         //   window.location.href = "/crates-inquiry-agent/print/download?agent="+ agent + "&date=" + date_range + "&ref=" + ref + "&filter=" + true;
        }

        function confirmAlert(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "delete",
                        url: id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            } else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

    </script>
@stop