@extends('layouts.back.master')@section('title','Purchasing Order')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .hide {
            display: none;
        }
    </style>
@stop
@section('content')
    <div class="app-wrapper">


        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">PRICE CHANGING REQUEST</h5>
            </div>


            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">
                    <div class="container">


                        <div class="projects-list">
                            @foreach($pos as $po)
                                @php
                                $rw = \RawMaterialManager\Models\RawMaterial::where('raw_material_id','=',$po->raw_material_id)->first();

                                $sp =\SupplierManager\Models\Supplier::where('id','=',$po->supplier_id)->first();


                                @endphp

                                    <div class="media">
                                        <div class="avatar avatar text-white avatar-md project-icon bg-primary"
                                             data-target="#project-1">
                                            <i class="fa fa-pause" aria-hidden="true"></i>
                                        </div>
                                        <div class="media-body">

                                            <h6 class="project-name" id="project-1">{{!empty($rw->name)?$rw->name:''}}</h6>
                                            <small class="project-detail">Current-> Rs.{{$rw->price}} -[{{$rw->units_per_packs}} Units In {{$rw->pack_type}}]
                                                @if($rw->main_supplier_id != 0)
                                                     -Supplier Name: {{$sp->supplier_name}}
                                            @endif
                                            </small>
                                            <h6 class="project-name" class="font-weight-bold text-danger">Requested-> Rs.{{$po->price}} -[{{$po->units_per_packs}} Units In {{$rw->pack_type}}] -Supplier Name: {{$sp->supplier_name}}</h6>



                                        </div>
                                        @if(Auth::user()->hasRole(['Super Admin']))

                                        <a href="{{'po/price/'.$po->id}}" class="btn btn-danger" style="width:6rem">UPDATE</a>
                                        <!--button class="btn btn-outline-primary" style="width:6rem">UPDATE</button-->
                                        @else
                                            <button button class="btn btn-danger" style="width:6rem" data-toggle="modal"
                                                    data-target="#edit_price{{$po->id}}">CHANGE
                                            </button>
                                            <!--button class="btn btn-outline-primary" style="width:6rem">CHANGE</button-->

                                            <div class="modal fade" id="edit_price{{$po->id}}" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
                                                 aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        {{ Form::open(array('route' => 'po.edit-cngPrice','enctype'=>'multipart/form-data','id'=>'jq-validation-example-1'))}}
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                                                                <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <span style="top: 15px;"></span>
                                                                    <label for="single">Name Of Raw Meterial : <h5><b>{{!empty($rw->name)?$rw->name:''}}</b></h5> [{{$rw->pack_type}}]</label>

                                                                </div>
                                                            </div>

                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <span style="top: 15px;"></span><input type="hidden" name="raw_id" value="{{$po->raw_material_id}}"><input type="hidden" name="po_id" value="{{$po->id}}">
                                                                    <label for="single">Edited Units per pack : {{$po->units_per_packs}}</label>
                                                                    <input class="form-control" type="text" name="unitpack" value="{{$po->units_per_packs}}" required >
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <span style="top: 15px;"></span>
                                                                    <label for="single">Edited Price : {{$po->price}}</label>
                                                                    <input class="form-control" type="text" name="price" value="{{$po->price}}" required >
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <span style="top: 15px;"></span>
                                                                    @php
                                                                        $esup = \SupplierManager\Models\Supplier::where('id','=',$po->supplier_id)->first();

                                                                    @endphp
                                                                    <label for="single">Edited Supplier : {{$esup->supplier_name}}</label>
                                                                    <select class="form-control" id="main_sup" name="main_sup" required>
                                                                        <option value=''>Select New Main Supplier</option>
                                                                        @foreach($supliers1 as $suplier)
                                                                            <option value="{{$suplier->id}}">{{$suplier->supplier_name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <hr>



                                                        </div>
                                                        <div class="modal-footer">
                                                            <input type="submit" class="btn btn-success btn-block" id="CHANGE" value="EDIT PRICE CHANGE">
                                                        </div>
                                                        {!!Form::close()!!}
                                                    </div>
                                                </div>
                                            </div>

                                        @endif
                                    </div>
                                    <hr class="m-0">
                                </a>



                            @endforeach
                        </div>

                    </div>
                </div>

            </div>


        </div>




    </div>

@stop

