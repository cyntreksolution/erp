@extends('layouts.back.master')@section('title','Purchasing order')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.wizard.css')}}">



    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        #floating-button {
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #db4437;
            position: fixed;
            top: 5rem;
            right: 20px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .plus {
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }


    </style>
@stop
@section('content')
    <div class="row">
        <div class="col-md-12 p-3">
            {{ Form::open(array('url' => '/po/confirm/'.$po->id )) }}
            <div class="text-center"><h5 class="text-uppercase mb-2">{{$po['supplier']->supplier_name}}</h5>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 p-3">
            <div class="row p-3" style="border-bottom: 1px solid black">
                <div class="col-md-3">Name</div>
                <div class="col-md-3 text-right">QTY</div>
                <div class="col-md-3 text-right">Received Price</div>
                <div class="col-md-3 text-right">Total Price</div>
                <input type="hidden" name="po_id" value="{{$po->id}}">
            </div>
            @php
            $total = 0;

            @endphp
            @foreach($po->purchasingOrderContent as $content)

                <div class="row p-2">
                    <input type="hidden" name="pd_ids[]" value="{{$content->id}}">
                    <input type="hidden" name="raw_materials[]"
                           value="{{$content->raw_material_id}}">
                    <div class="col-md-3">{{$content->ingredients->name}}</div>
                    <div class="col-md-3 text-right">{{$content->recieved_qty}}</div><input type="hidden" name="receieved_qty[]" value="{{$content->recieved_qty}}">
                    <div class="col-md-3 text-right">{{number_format($content->received_price,2)}}</div>
                    <div class="col-md-3 text-right">{{number_format($content->received_price * $content->recieved_qty,2)}}</div>
                </div>
                <hr>

                @php

                    $total = ($content->received_price * $content->recieved_qty) + $total;
                @endphp
            @endforeach
        </div>
    </div>

    <div class="row">
        <div class="col-md-6"></div>
        <div class="col-md-6 float-right text-right">
            <label class="text-right">Total Value : {{number_format($total,2)}}</label>
        </div>
    </div>
@php

@endphp
    <div class="row">
        <div class="col-md-8">
            <div class="col-sm-3">
                <div class="form-group">
                    {!! Form::text('ndate',null,['class' => 'form-control','id'=>'ndate','placeholder'=>'Next Visit Date']) !!}
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    {!! Form::text('ref',null,['class' => 'form-control','id'=>'ref','placeholder'=>'Supplier Invoice Number']) !!}
                </div>
            </div>
            <button class="btn btn-block btn-success">
                Confirm
            </button>
            {{Form::close()}}
        </div>
        <div class="col-md-4">
            {{ Form::open(['url' => '/po/'.$po->id.'/edit','method'=>'get']) }}
            <button class="btn btn-block btn-danger">
                Back
            </button>
            {{Form::close()}}
        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script src="{{asset('assets/vendor/dtable/Buttons-1.5.1/js/dataTables.buttons.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/buttons/1.5.1/js/buttons.jqueryui.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/JSZip-2.5.0/jszip.min.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/pdfmake-0.1.32/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/Buttons-1.5.1/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js')}}"></script>


    <script>
        $('#ndate').daterangepicker({
            "showDropdowns": true,
            "timePicker": false,
            "singleDatePicker": true,
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019"
        });
        $('#ndate').val('')
    </script>
@stop