<html>
<head>
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/invoice.css')}}" rel="stylesheet">
    <style media="all">
        @page {
            margin: 30px 5px 30px 5px;
            size: 75mm 999mm;
        }
    </style>
</head>

<body style="margin-bottom: 100px">

<div class="row">

    <div class="col-xs-6">
        <img src="{{asset('assets/img/buntak_grace.jpeg')}}" style="width: 150px">
    </div>

    <div class="col-xs-6 text-right">
        <h6>
            <b>Ranjanas Holdings [Pvt] Ltd</b><br>
            Katukurunda<br>
            Habaraduwa<br>
            Hotline : +9477 330 4678<br>
            E-mail : buntalksl@icloud.com<br>
        </h6>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        @php $sup = \SupplierManager\Models\Supplier::whereId($invoice->supplier_id)->first();


        @endphp

        <div class="row">
            <div class="col-xs-6 text-nowrap"><b>Supplier Name</b></div>
            <div class="col-xs-6">:{{$sup->supplier_name}}</div>
        </div>
        <div class="row">
            <div class="col-xs-6 text-nowrap"><b>Address</b></div>
            <div class="col-xs-6">:{{$sup->address}}</div>
        </div>
        <div class="row">
            <div class="col-xs-6 text-nowrap"><b>Contact No</b></div>
            <div class="col-xs-6">:{{$sup->mobile}}</div>
        </div>
        <div class="row">
            <div class="col-xs-6 text-nowrap"><b>Order Date</b></div>
            <div class="col-xs-6">:{{$invoice->created_at}}</div>
        </div>
        <div class="row">
            <div class="col-xs-6 text-nowrap"><b>Invoice Number</b></div>
            <div class="col-xs-6">:{{$invoice->invoice_no}}</div>
        </div>



    </div>
</div>
<br>

<div class="row" style="margin-top: 20px;">

    <div class="col-xl-12 text-center">
        <h3><b><u>

            Quotation
                </u></b></h3>
    </div>

</div>
<div class="row">
    <div class="col-xs-12">
        <table class="table">
            <thead>
            <tr>
                <th>Item Name</th>
                <th class="text-center">QTY</th>
                <th class="text-right">Value</th>

            </tr>
            </thead>
            <tbody>
            @php $counter= 0;  $ordval = 0 ;  @endphp
            @foreach($items as $key => $product)

                <tr>

                    @php $raw = \RawMaterialManager\Models\RawMaterial::where('raw_material_id','=',$product->raw_material_id)->first(); @endphp

                    @if($invoice->status == 1)

                        <td>{{$raw->name}}</td>
                        <td class="text-center">{{$product->requested_qty}} - {{$raw->pack_type}}</td>
                        <td align="right">
                            <div style="width:100px;height:30px;border:1px solid #000;"></div>
                        </td>
                    @endif



                    @php  $counter = $counter+1 ;  @endphp
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>

</div>
<div class="row">
    <div class="col-xs-12 pull-right text-right">
        <h4> Number Of Items : {{$counter}} </h4>
    </div>
</div>
<br>

<footer>


    <div class="row" style="margin-top: 60px;">

        <div class="text-center">
            <h5>
                .........................................<br>
                        Signature-Authorized By
            </h5>
        </div>

    </div>


</footer>

</body>

</html>
