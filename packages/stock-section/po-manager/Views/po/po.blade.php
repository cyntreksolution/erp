<html>
<head>
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/invoice.css')}}" rel="stylesheet">
</head>
<body>
@foreach($data as $key => $item)
    <div class="row">
        <h4 class="inv">
            PURCHASING ORDER
        </h4>
    </div>

    <div class="row">
        <div class="col-xs-8">
            @if($item->stock_id == 1)
                <h4>
                    <b>Ranjanas Holdings [Pvt] Ltd</b><br>
                    Katukurunda<br>
                    Habaraduwa<br>
                    Mobile : +9477 330 4678
                </h4>
            @endif
        </div>
        {{--<br>--}}
        <div class="col-sx-4">
            <h4>Order To :<br><b>{{$item->customer_name}}</b><br>{!! str_replace(',', '<br>', $item->address) !!}</h4>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="text-left col-xs-2">
            <h5>PO NUMBER</h5>
            <h5>DATE OF PO</h5>
            {{--<h5>DUE DATE : {{$item->due_date}}</h5>--}}
        </div>
        <div class="text-left col-xs-2">
            <h5>: {{$item->invoice_no}}</h5>
            <h5>: {{$item->received_date}}</h5>
            {{--<h5>DUE DATE : {{$item->due_date}}</h5>--}}
        </div>
        <div class="col-xs-4 pull-right text-right">
            <h5>AMOUNT DUE (LKR)</h5>
            <h4>{{$item->total}}</h4>
            {{--<h5>DUE DATE : {{$item->due_date}}</h5>--}}
        </div>
    </div>


    <div class="row">
        <div class="col-xs-12">
            <table class="table" style="margin-top: 50px;">
                <thead class="thead-inverse">
                <tr style="background-color: #656872; color: white">
                    <th>No</th>
                    <th>Product</th>
                    <th class="text-center">Unit value</th>
                    <th class="text-center">Qty.</th>
                    <th class="text-right">Line Total (LKR)</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $key => $item)
                    <tr>
                        <th scope="row">{{$key+1}}</th>
                        <td>{{$item->name}}</td>
                        <td class="text-center">{{$item->ordered_price}}</td>
                        <td class="text-center">{{$item->recieved_qty}} {{$item->pack_type}}</td>
                        <td class="text-right">{{number_format($item->received_price*$item->recieved_qty,2)}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>

    <div class="col-xs-4 pull-right text-right">
        <h4> Amount Due (LKR):{{$item->total}}</h4>
    </div>

    <div class="row" style="margin-top: 80px;">
        <div class="col-xs-3">
            <h5>
                .........................................<br>
                Signature-Authorized
            </h5>
        </div>


        <div class="col-xs-3 text-right pull-right">
            <h5>
                .........................................<br>
                Signature-Received
            </h5>
        </div>

    </div>
    <footer>
        <div class="row" style="margin-top: 20px;">
            <h4 align="center">
                <h6 class="text-muted">powered by Cyntrek Solutions<br>
                    <h7 class="num">+9471 122 5489 / +9471 532 2262</h7>
                </h6>
            </h4>

        </div>
    </footer>
    @break
@endforeach
</body>

</html>