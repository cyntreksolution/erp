<?php

namespace PurchasingOrderManager\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use SalesRepManager\Models\SalesRep;


class Content extends Model
{
    use SoftDeletes;

    protected $table = 'purchasing_order_data';

    protected $dates = ['deleted_at'];

//    public function purchasingOrder(){
//        return $this->belongsTo('RecipeManager\Models\Recipe');
//    }

    public function purchasingOrder(){
        return $this->belongsTo('PurchasingOrderManager\Models\PurchasingOrder');
    }

    public function ingredients(){
        return $this->hasOne('RawMaterialManager\Models\RawMaterial','raw_material_id','raw_material_id')->withoutGlobalScopes();;
    }

    public function scopeSearchData($query, $term)
    {
        return $query
            ->Where('id', 'like', "%" . $term . "%")
            ->orWhere('serial', 'like', "%" . $term . "%")
            ->orWhere('status', 'like', "%" . $term . "%")
            ->orWhere('burden_size', 'like', "%" . $term . "%")
            ->orWhere('created_at', 'like', "%" . $term . "%")
            ->orWhereHas('semiProdcuts', function ($query) use ($term) {
                $query->where('name', 'like', "%" . $term . "%");
            })
            ->orWhereHas('recipe', function ($query) use ($term) {
                $query->where('name', 'like', "%" . $term . "%");
            });
    }
    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }

    public function scopeDataTable($query, $date_range, $supplier,$item)
    {
        if (!empty($supplier)) {
            $query = $query->whereHas('purchasingOrder', function ($q) use ($supplier) {
                return $q->where('supplier_id', '=', $supplier);
            });
        }



        if (!empty($item)) {

            $query = $query->where('raw_material_id', '=', $item);

        }

        if (!empty($date_range)) {

            $dates = explode(' - ', $date_range);
            $start = $dates[0];
            $end = $dates[1];

            $query->where('created_at','>=',Carbon::parse($start)->format('Y-m-d'))
                ->where('created_at','<',Carbon::parse($end)->addDay()->format('Y-m-d'));
        }
    }


}
