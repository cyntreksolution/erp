<?php

namespace PurchasingOrderManager\Models;

use App\AgentBuffer;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchasingOrder extends Model
{
    use SoftDeletes;

    protected $table = 'purchasing_order_header';

    protected $dates = ['deleted_at'];

    public function supplier()
    {
        return $this->belongsTo('SupplierManager\Models\Supplier');
    }

    public function purchasingOrderContent(){
        return $this->hasMany('PurchasingOrderManager\Models\Content');
    }

    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }

    public function scopeSearchData($query, $term)
    {
        return $query
            ->Where('id', 'like', "%" . $term . "%")
            ->orWhere('serial', 'like', "%" . $term . "%")
            ->orWhere('status', 'like', "%" . $term . "%")
            ->orWhere('burden_size', 'like', "%" . $term . "%")
            ->orWhere('created_at', 'like', "%" . $term . "%")
            ->orWhereHas('semiProdcuts', function ($query) use ($term) {
                $query->where('name', 'like', "%" . $term . "%");
            })
            ->orWhereHas('recipe', function ($query) use ($term) {
                $query->where('name', 'like', "%" . $term . "%");
            });
    }


    public function scopeFilterData($query, $date_range, $supplier,$payment,$status)
    {
        if (!empty($supplier)) {
            $query = $query->where('supplier_id', '=', $supplier);
        }

        if (!empty($status)) {
            switch ($status){
                case 1:
                    $query = $query->where('received_date', '=', null);
                    break;
                case 2:
                    break;
                case 3:
                    $query =$query->where('received_date', '<>', null);
                    break;

            }

        }

        if (!empty($payment)) {
            if ($payment==1) {
                $query = $query->where('is_paid', '=', 1);
            }else{
                $query = $query->where('is_paid', '=', 0);
            }
        }

        if (!empty($date_range)) {
            $column ='updated_at';

            if (!empty($status)) {
                switch ($status){
                    case 1:
                        $column = 'created_at';
                        break;
                    case 3:
                        $column = 'received_date';
                        break;
                }
            }

            if (!empty($payment)) {
                if ($payment==1) {
                    $column ='paid_at';
                }
            }


            $dates = explode(' - ', $date_range);
            $start = $dates[0];
            $end = $dates[1];

            $query->where($column,'>=',Carbon::parse($start)->format('Y-m-d'))
                ->where($column,'<',Carbon::parse($end)->addDay()->format('Y-m-d'));
        }
    }

}
