<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */
Route::middleware(['web','log'])->group(function () {
    Route::prefix('permission')->group(function () {
        Route::get('', 'PermissionManager\Controllers\PermissionController@index')->name('permission.index');

        Route::get('json', 'PermissionManager\Controllers\PermissionController@json')->name('permission.json');

        Route::get('create', 'PermissionManager\Controllers\PermissionController@create')->name('permission.create');

        Route::get('{permission}', 'PermissionManager\Controllers\PermissionController@show')->name('permission.show');

        Route::get('{permission}/edit', 'PermissionManager\Controllers\PermissionController@edit')->name('permission.edit');


        Route::post('', 'PermissionManager\Controllers\PermissionController@store')->name('permission.store');

        Route::put('{permission}', 'PermissionManager\Controllers\PermissionController@update')->name('permission.update');

        Route::delete('{permission}', 'PermissionManager\Controllers\PermissionController@destroy')->name('permission.destroy');
    });
});