<?php namespace PermissionManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;


class Permission extends Model
{
    use SoftDeletes;
    use SearchableTrait;

    protected $table = 'permission';

    protected $primaryKey='permission_id';

    protected $fillable = ['name'];

    protected $dates = ['deleted_at'];

    protected $searchable = [
        'columns' => [
            'name' => 10,
            'description' => 5,
        ]
    ];

}
