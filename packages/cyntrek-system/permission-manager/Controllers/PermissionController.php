<?php

namespace PermissionManager\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PermissionManager\Models\Permission;
use Response;
use Sentinel;

class PermissionController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Permission Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders the "marketing page" for the application and
    | is configured to only allow guests. Like most of the other sample
    | controllers, you are free to modify or remove it as you desire.
    |
    */
    public function index()
    {
        return view('PermissionManager::permission.list');
    }

    public function find(Request $request)
    {
        return Permission::search($request->get('q'))->get();
    }

    public function json()
    {
        $a = DB::table('permission')
            ->select(DB::raw('name as text, name as value ,name as continent'))
            ->get();

        foreach ($a as $entries) {
            $entries->continent = substr($entries->continent, strpos($entries->continent, ".") + 1);
        }

        return $a;
    }


    public function create()
    {
        return view('PermissionManager::permission.add');
    }

    public function store(Request $request)
    {
        $name = $request->label;
        $count = Permission::where('name', '=', $name)->count();
        if ($count == 0) {
            $permission = new Permission();
            $permission->name = $name;
//            $permission->created_by = Sentinel::getUser()->id;
            $permission->save();

            return redirect('permission/create')->with(['success' => true,
                'success.message' => 'Permission added successfully!',
                'success.title' => 'Well Done!']);
        } else {

            return redirect('permission/create')->with(['error' => true,
                'error.message' => 'Permission Already Exsist!',
                'error.title' => 'Duplicate!']);
        }

    }


    public function jsonList(Request $request)
    {
        if ($request->ajax()) {
            $permissions = Permission::where('name', '!=', 'admin')->where('name', '!=', 'index')->get();
            $user = Auth::user();
            $jsonList = array();
            $i = 1;
            foreach ($permissions as $key => $permission) {
                $dd = array();
                array_push($dd, $i);

                array_push($dd, $permission->name);
                array_push($dd, ($permission->description != null || $permission->description != '') ? $permission->description : '-');
                $permissions = Permission::whereIn('name', ['permission.edit', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<a href="#" class="blue" onclick="window.location.href=\'' . url('permission/edit/' . $permission->id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit Permission"><i class="fa fa-pencil"></i></a>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
                }

                $permissions = Permission::whereIn('name', ['permission.delete', 'admin'])->where('status', '=', 1)->lists('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<a href="#" class="red permission-delete" data-id="' . $permission->id . '" data-toggle="tooltip" data-placement="top" title="Delete Permission"><i class="fa fa-trash-o"></i></a>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
                }

                array_push($jsonList, $dd);
                $i++;
            }
            return Response::json(array('data' => $jsonList));
        } else {
            return Response::json(array('data' => []));
        }
    }

    public function destroy(Permission $permission)
    {
        $permission->delete();
        if ($permission->trashed()) {
            return true;
        } else {
            return false;
        }
    }

    public function editView($id)
    {
        $permission = Permission::find($id);
        if ($permission) {
            return view('PermissionManager::permission.edit')->with([
                'curPermission' => $permission

            ]);
        } else {
            return view('errors.404');
        }

    }

    public function edit(Request $request, $id)
    {
        $name = $request->get('label');
        $description = $request->get('description');

        $count = Permission::where('id', '!=', $id)->where('name', '=', $name)->count();

        if ($count == 0) {
            $curPermission = Permission::find($id);
            $curPermission->name = $name;
            $curPermission->description = $description;
            $curPermission->save();

            return redirect('permission/edit/' . $id)->with(['success' => true,
                'success.message' => 'Permission updated successfully!',
                'success.title' => 'Good Job!']);
        } else {
            return redirect('permission/edit/' . $id)->with(['error' => true,
                'error.message' => 'Permission Already Exsist!',
                'error.title' => 'Duplicate!']);
        }
    }
}
