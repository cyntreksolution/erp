<?php
namespace PermissionManager\Requests;

use Illuminate\Http\Request;

class PermissionRequest extends Request {

	public function authorize(){
		return true;
	}

	public function rules(){
		$rules = [
			'label' => 'required'			
			];
		return $rules;
	}

}
