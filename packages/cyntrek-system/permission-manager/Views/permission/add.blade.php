@extends('layouts.back.master')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/select2-3.5.2/select2.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/vendor/select2-bootstrap/select2-bootstrap.css')}}"/>
@stop

@section('content')

    <div class="col-md-10 col-md-offset-1">
        <div class="card">
            <div class="card-header card-header-icon" data-background-color="rose">
                <i class="material-icons">airport_shuttle</i>
                New Permission
            </div>
            {{ Form::open(array('url' => 'permission')) }}
            {!!Form::token()!!}


            <div class="col-sm-6">
                <label>Name</label>
                <input type="text" class="form-control" name="label">
            </div>

            {{--<div class="form-group"><label class="col-sm-2 control-label">DESCRIPTION</label>--}}
            {{--<div class="col-sm-10"><textarea class="form-control" name="description"></textarea></div>--}}
            {{--</div>--}}

            <div class="card-footer">
                <div class="pull-right">
                    <input type='submit' class='btn btn-finish btn-fill btn-rose btn-wd' value='SAVE'/>
                </div>
            </div>

            {{ Form::close() }}
        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('assets/vendor/select2-3.5.2/select2.min.js')}}"></script>
    <script src="{{asset('assets/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".js-source-states").select2();

            $("#form").validate({
                rules: {
                    label: {
                        required: true

                    },
                    menu_url: {
                        required: true
                    },
                    parent_menu: {
                        required: true
                    },
                    tel: {
                        digits: true
                    }
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        });


    </script>
@stop