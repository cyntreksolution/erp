@extends('layouts.master') @section('current_title','All Role')
@section('css')
    <style type="text/css">
        #floating-button {
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #db4437;
            position: fixed;
            bottom: 50px;
            right: 30px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .plus {
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }
    </style>

@stop
@section('current_path')
    <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
            <li><a href="{{url('user/list')}}">User Management</a></li>

            <li class="active">
                <span>Role List</span>
            </li>
        </ol>
    </div>
@stop
@section('content')
    <div id="floating-button" data-toggle="tooltip" data-placement="left" data-original-title="Create"
         onclick="location.href = '{{url('user/role/add')}}';">
        <p class="plus">+</p>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="rose">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Main Stock List</h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover"
                                   cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>

                                    <th>#</th>
                                    <th>Role</th>
                                    <th>Permissions</th>
                                    <th width="1%">Edit</th>
                                    <th width="1%">Delete</th>

                                </tr>

                                </thead>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
@stop
@section('js')
    <script type="text/javascript">
        var table;
        $(document).ready(function () {
            table = $('#datatables').dataTable({
                "ajax": '{{url('user/role/json/list')}}',
                dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                buttons: [
                    {extend: 'copy', className: 'btn-sm'},
                    {extend: 'csv', title: 'Menu List', className: 'btn-sm'},
                    {extend: 'pdf', title: 'Menu List', className: 'btn-sm'},
                    {extend: 'print', className: 'btn-sm'}
                ],
                "autoWidth": false,

            });

            table.on('draw.dt', function () {
                $('.role-delete').click(function (e) {
                    e.preventDefault();
                    id = $(this).data('id');
                    confirmAlert(id);

                });

            });


        });

        function confirmAction(id) {
            console.log(table);

            $.ajax({
                method: "POST",
                url: '{{url('user/role/delete')}}',
                data: {'id': id}
            })
                .done(function (msg) {
                    table.fnReloadAjax();
                });

        }


    </script>
@stop