@extends('layouts.back.master')
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
@stop
@section('content')

    <div class="row">
        <div class="col-sm-6">
            {{ Form::open(array('url' => 'role')) }}
            {!!Form::token()!!}
            <div class="box">
                <div class="box-header">
                    <h2>NEW USER ROLE</h2>
                </div>
                <div class="box-body">

                    <div class="form-group">
                        <label for="default">USER TYPE </label>
                        <input id="default" name="name" type="text" class="form-control" placeholder="Placeholder text">
                    </div>

                    {{--<div class="form-group">--}}
                    {{--<input multiple type="search" name="q" class="form-control search-input" placeholder="Search" autocomplete="off">--}}
                    {{--</div>--}}

                    <select class="js-example-responsive" name="permissions[]" multiple="multiple" style="width: 75%">
                        @foreach($permissionArr as $item)
                            <option value="{{$item}}">{{$item}}</option>
                        @endforeach
                    </select>

                    <div class="dker p-a text-right">
                        <input type="submit" class="btn info" value="Save"/>
                    </div>
                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>

@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>

    <!-- Bootstrap JS -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <!-- Typeahead.js Bundle -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
    <!-- Typeahead Initialization -->

    <script>

        $(".js-example-responsive").select2({
            width: 'resolve' // need to override the changed default
        });

        jQuery(document).ready(function($) {
            // Set the Options for "Bloodhound" suggestion engine
            var engine = new Bloodhound({
                remote: {
                    url: 'find?q=%QUERY%',
                    wildcard: '%QUERY%'
                },
                datumTokenizer: Bloodhound.tokenizers.whitespace('q'),
                queryTokenizer: Bloodhound.tokenizers.whitespace
            });

            $(".search-input").typeahead({
                hint: true,
                display:'name',
                highlight: true,
                minLength: 1
            }, {
                source: engine.ttAdapter(),

                // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
                name: 'usersList',

                // the key from the array we want to display (name,id,email,etc...)
                templates: {
                    empty: [
                        '<div class="list-group search-results-dropdown"><div class="list-group-item">Nothing found.</div></div>'
                    ],
                    header: [
                        '<div class="list-group search-results-dropdown">'
                    ],
                    suggestion: function (data) {
                        return '<span  class="list-group-item">' + data.name + ' </span>'
                    }
                }
            });
        });
    </script>

@stop