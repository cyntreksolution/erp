@extends('layouts.back.master') @section('current_title','Update Role')
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
@stop
@section('current_path')
<div id="hbreadcrumb" class="pull-right">
     <ol class="hbreadcrumb breadcrumb">
        <li><a href="{{url('user/list')}}">User Management</a></li>
        <li><a href="{{url('user/role/list')}}">Role Management</a></li>
       
        <li class="active">
            <span>Update Role</span>
        </li>
    </ol>
</div>
@stop
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="hpanel">
            <div class="panel-body">
                {{ Form::open(array('url' =>'role/'.$role->id,'method'=>'PUT')) }}
                	{!!Form::token()!!}
                	<div class="form-group"><label class="col-sm-2 control-label">NAME</label>
                    	<div class="col-sm-10"><input type="text" class="form-control" name="name" value="{{$role->name}}"></div>
                	</div>
                	<div class="form-group"><label class="col-sm-2 control-label">PERMISSION</label>
                    	<div class="col-sm-10">
                            <select class="js-example-responsive" name="permissions[]" multiple="multiple" style="width: 75%">
                    		 	 <?php foreach ($permissionArr as $key => $value): 
                                	echo $value;                                    
                                endforeach ?>
		                        
		                    </select>
                    	</div>
                	</div>               	

                	<div class="hr-line-dashed"></div>
	                <div class="form-group">
	                    <div class="col-sm-8 col-sm-offset-2">
	                        <button class="btn btn-default" type="button" onclick="location.reload();">Cancel</button>
	                        <button class="btn btn-primary" type="submit">Save Changes</button>
	                    </div>
	                </div>
                </form>
            </div>
        </div>
    </div>
@stop
@section('js')
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>

        <script>

            $(".js-example-responsive").select2({
                width: 'resolve' // need to override the changed default
            });
        </script>
@stop