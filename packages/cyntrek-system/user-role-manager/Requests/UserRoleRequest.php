<?php
namespace UserRoleManager\Requests;

use Illuminate\Http\Request;

class UserRoleRequest extends Request {

	public function authorize(){
		return true;
	}

	public function rules(){
		$rules = [
			'name' => 'required',
			'permissions' => 'required'
		];
		return $rules;
	}

}
