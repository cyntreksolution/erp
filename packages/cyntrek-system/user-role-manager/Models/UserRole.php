<?php
namespace UserRoleManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class UserRole extends Model{

    use SoftDeletes;

	protected $table = 'roles';

	protected $fillable = ['name', 'slug', 'permissions','created_at','updated_at'];

    protected $dates = ['deleted_at'];


}
