<?php

namespace UserRoleManager\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PermissionManager\Models\Permission;
use Response;
use Sentinel;
use Carbon\Carbon;
use DB;
use UserRoleManager\Models\UserRole;

class UserRoleController extends Controller
{
    public function create()
    {
        $permissions = Permission::all()->pluck('name', 'name');
        $permissions->pull('admin');
        // $permissions->pull( 'index' );
        $role = UserRole::all();
        return view('UserRoleManager::role.create')->with([
            'permissionArr' => $permissions,
            'roles' => $role,
        ]);
    }

    public function store(Request $request)
    {
        $user = Auth::user();
        $permissions = $request->get('permissions');

        $role = new UserRole();
        $role->name = $request->name;
        $role->slug = str_slug($request->name);
        $role->created_by = $user->id;
        $perm = array();
        $perm['admin'] = true;
        foreach ($permissions as $key => $value) {
            $perm[$value] = true;
        }

        $role->permissions = json_encode($perm);
        $role->save();


        return redirect('role')->with(['success' => true,
            'success.message' => 'Role added successfully!',
            'success.title' => 'Well Done!']);

    }

    public function index()
    {
        return redirect(route('role.create'));
        return view('UserRoleManager::role.list');
    }

    public function jsonList(Request $request)
    {
        if ($request->ajax()) {
            $data = UserRole::all();
            $user = Auth::user();
            $jsonList = array();
            $i = 1;
            foreach ($data as $key => $role) {
                $dd = array();
                array_push($dd, $i);

                array_push($dd, $role->name);

                $json = array_keys(json_decode($role->permissions, true));
                $pl = str_replace(',', ' ,', json_encode($json));
//                $pl=str_replace('"', ' ', json_encode($json));

                array_push($dd, $pl);

                $permissions = Permission::whereIn('name', ['user.role.edit', 'admin'])->where('status', '=', 1)->pluck('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<a href="#" class="blue" onclick="window.location.href=\'' . url('user/role/edit/' . $role->id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit Role"><i class="fa fa-pencil"></i></a>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
                }

                $permissions = Permission::whereIn('name', ['user.role.delete', 'admin'])->where('status', '=', 1)->pluck('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<a href="#" class="red role-delete" data-id="' . $role->id . '" data-toggle="tooltip" data-placement="top" title="Delete Role"><i class="fa fa-trash-o"></i></a>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
                }

                array_push($jsonList, $dd);
                $i++;
            }
            return Response::json(array('data' => $jsonList));
        } else {
            return Response::json(array('data' => []));
        }
    }

    public function destroy(UserRole $user_role)
    {
        $user_role->delete();
        if ($user_role->trashed()) {
            return true;
        } else {
            return false;
        }
    }

    public function edit(UserRole $role)
    {
        $permissions = Permission::all()->pluck('name', 'name');
        $permissions->pull('admin');
        // $permissions->pull( 'index' )

        $cur_permission = $role->permissions;
        $selected_permission = array();
        $cur_permission = array_keys(json_decode($role->permissions, true));
        foreach ($permissions as $key => $value) {
            if (in_array($value, $cur_permission, true)) {
                array_push($selected_permission, '<option selected value="' . $value . '">' . $value . '</option>');
            } else {
                array_push($selected_permission, '<option  value="' . $value . '">' . $value . '</option>');
            }
        }

        if ($role) {
            return view('UserRoleManager::role.edit')->with([
                'permissionArr' => $selected_permission,
                'role' => $role
            ]);
        } else {
            return view('errors.404');
        }
    }

    public function update(Request $request, UserRole $role)
    {
        $user = Sentinel::getUser();
        $permissions = $request->get('permissions');


        $role->name = $request->get('name');
        $role->slug = str_slug($request->get('name'));
//            $role->created_by = $user->id;

        $perm = array();
        foreach ($permissions as $key => $value) {
            $perm[$value] = true;
        }

        $role->permissions = json_encode($perm);
        $role->save();


        return redirect(route('role.index'))->with(['success' => true,
            'success.message' => 'Role added successfully!',
            'success.title' => 'Well Done!']);

    }
}
