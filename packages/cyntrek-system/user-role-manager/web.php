<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */
Route::middleware(['web','log'])->group(function () {
    Route::prefix('role')->group(function () {

        Route::get('', 'UserRoleManager\Controllers\UserRoleController@index')->name('role.index');

        Route::get('create', 'UserRoleManager\Controllers\UserRoleController@create')->name('role.create');

        Route::get('{role}', 'UserRoleManager\Controllers\UserRoleController@show')->name('role.show');

        Route::get('{role}/edit', 'UserRoleManager\Controllers\UserRoleController@edit')->name('role.edit');


        Route::post('', 'UserRoleManager\Controllers\UserRoleController@store')->name('role.store');

        Route::put('{role}', 'UserRoleManager\Controllers\UserRoleController@update')->name('role.update');

        Route::delete('{role}', 'UserRoleManager\Controllers\UserRoleController@destroy')->name('role.destroy');
    });
});