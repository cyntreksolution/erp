<?php
namespace MenuManager\Models;

use Illuminate\Database\Eloquent\Model;
use Baum\Node;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Node{

    use SoftDeletes;

	protected $table = 'menu';

	protected $primaryKey='menu_id';


	protected $parentColumn = 'parent';


	protected $leftColumn = 'lft';


	protected $rightColumn = 'rgt';


	protected $depthColumn = 'depth';


	protected $guarded = array('menu_id', 'parent', 'lft', 'rgt', 'depth');


	protected $fillable = ['label', 'link', 'icon', 'permissions'];

	public function parentMenu()
	{
		return $this->belongsTo($this,'parent','menu_id');
	}

    protected $dates = ['deleted_at'];

}
