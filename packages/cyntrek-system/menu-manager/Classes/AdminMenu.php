<?php

namespace MenuManager\Classes;

use PermissionManager\Models\Permission;
use Sentinel;

class AdminMenu
{
    static function generateMenu($parent, $menu, $level, $currentUrl, $userId)
    {
        $user = Sentinel::findUserById($userId);
        $html = "";
        if (!empty($menu)) {
            foreach ($menu as $key => $element) {
                $permissions = Permission::whereIn('name', json_decode($element->permissions))->where('status', '=', 1)->pluck('name');
                if ($user->hasAnyAccess($permissions) && $element->status == 1) {
                    if (count($element->children) == 0) {

                        if ($currentUrl && $currentUrl->id == $element->id) {
                            $html .= "<li class=\"active\">";
                        } else {
                            $html .= "<li>";
                        }
                        $html .= "<a href=\"" . url($element->link) . "\">";
                        $html .= "<i class=\"menu-icon fa fa-$element->icon zmdi-hc-lg\"></i>";
                        $html .= "<span class='menu-text'>" . $element->label . "</span>";
                        $html .= "</a></li>";

                    } else {
                        if ($currentUrl && $element->isAncestorOf($currentUrl)) {
                            $html .= "<li class=\"active\">";
                        } else {
                            $html .= "<li >";
                        }

                        $html .= " <a href=\"javascript:void(0)\" class=\"submenu-toggle\">";
                        $html .= "<i class=\"menu-icon fa fa-$element->icon zmdi-hc-lg\"></i>";
                        $html .= "<span class='menu-text'>" . $element->label . "</span>";
                        $html .= "<i class=\"menu-caret zmdi zmdi-hc-sm zmdi-chevron-right\"></i>";
                        $html .= "</a>";

                        $html .= "<ul class=\"submenu\">";
                        $html .= AdminMenu::generateMenu($element->id, $element->children, $element->getLevel(), $currentUrl, $userId);
                        $html .= "</ul>";

                        $html .= "</li>";
                    }
                }
            }
        }

        return $html;
    }
}
