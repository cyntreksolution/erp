<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web','log'])->group(function () {
    Route::prefix('menu')->group(function () {
        Route::get('', 'MenuManager\Controllers\MenuController@index')->name('menu.index');

        Route::get('create', 'MenuManager\Controllers\MenuController@create')->name('menu.create');

        Route::get('{menu}', 'MenuManager\Controllers\MenuController@show')->name('menu.show');

        Route::get('{menu}/edit', 'MenuManager\Controllers\MenuController@edit')->name('menu.edit');


        Route::post('', 'MenuManager\Controllers\MenuController@store')->name('menu.store');

        Route::post('{menu}', 'MenuManager\Controllers\MenuController@update')->name('menu.update');

        Route::delete('{menu}', 'MenuManager\Controllers\MenuController@destroy')->name('menu.destroy');
    });
});