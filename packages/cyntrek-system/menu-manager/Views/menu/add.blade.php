@extends('layouts.back.master') @section('current_title','New Menu')
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
@stop
@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="card">
            <div class="card-header card-header-icon" data-background-color="rose">
                <i class="material-icons">airport_shuttle</i>
            </div>
            {{ Form::open(array('url' => 'menu')) }}
            {!!Form::token()!!}
                <div class="card-content">
                    <h4 class="card-title">NEW Menu</h4>

                    <div class="col-sm-6">
                        <label>Label</label>
                        <input type="text" class="form-control" name="label">
                    </div>

                    <div class="col-sm-6">
                        <label>Label</label>
                        <input type="text" class="form-control" name="menu_url">
                    </div>


                    <div class="form-group">
                        <label for="single">parent</label>
                        <select id="single" name="parent_menu" class="form-control select2" ui-jp="select2" ui-options="{theme: 'bootstrap'}">
                            @foreach($menus as $item)
                                <option value="{{$item->menu_id}}">{{$item->label}}</option>
                            @endforeach
                        </select>
                    </div>

                    <label for="single">permission</label>
                    <select class="js-example-responsive" name="permissions[]" multiple="multiple" style="width: 75%">
                        @foreach($permissionArr as $item)
                            <option value="{{$item->name}}">{{$item->name}}</option>
                        @endforeach
                    </select>



                    <div class="form-group">
                        <label for="single">order</label>
                        <select id="single"name="menu_order" class="form-control select2" ui-jp="select2" ui-options="{theme: 'bootstrap'}">
                            @php $menus->pull('0') @endphp
                            @foreach($menus as $item)
                                <option value="{{$item->menu_id}}">{{$item->label}}</option>
                            @endforeach
                        </select>
                    </div>



                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit">Done</button>
                        </div>
                    </div>
                </div>
            {{ Form::close() }}
        </div>
    </div>
@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>

    <script>
        $(".js-example-responsive").select2({
            width: 'resolve' // need to override the changed default
        });
    </script>



@stop