<?php

namespace UserManager\Controllers;

use App\Http\Controllers\Controller;
use App\Traits\SendSMS;
use App\User;
use Ichtrojan\Otp\Otp;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PermissionManager\Models\Permission;
use Session;
use Sentinel;
use UserManager\Models\Users;
use UserRoleManager\Models\UserRole;

class UserController extends Controller
{
    use SendSMS, AuthenticatesUsers;

    public function home()
    {
        if (Auth::user()->hasRole(['Super Admin'])) {
            return redirect()->route('dashboard.superadministrator');
        } elseif (Auth::user()->hasRole(['Owner'])) {
            return redirect()->route('dashboard.owner');
        } elseif (Auth::user()->hasRole(['Assistant'])) {
            return redirect()->route('dashboard.assistant');
        } elseif (Auth::user()->hasRole(['Production Manager'])) {
            return redirect()->route('dashboard.production-manager');
        } elseif (Auth::user()->hasRole(['Stock Manager'])) {
            return redirect()->route('dashboard.stock-manager');
        } elseif (Auth::user()->hasRole(['Sales Agent'])) {
            return redirect()->route('dashboard.sales-rep-manager');
        } elseif (Auth::user()->hasRole(['Sales Manager'])) {
            return redirect()->route('dashboard.sales-head-manager');
        } elseif (Auth::user()->hasRole(['Stores End Product'])) {
            return redirect()->route('dashboard.end');
        }

    }

    public function loginView()
    {
        return view('UserManager::user.login');
    }

    public function registerView()
    {
        return view('UserManager::user.register');
    }

    public function profile()
    {
        if (Auth::check()) {
            $user_id= Auth::user()->id;
            $user = User::find($user_id);
        }

        return view('UserManager::user.profile')->with([
            'user'=>$user
        ]);
    }

    public function otpView(Request $request)
    {
        $credentials = $request->credentials;
        return view('otp', compact('credentials'));
    }

    public function otpSubmit(Request $request)
    {

        $credentials = [
            'email' => $request->un,
            'password' => $request->pw,
        ];
        $otp = new Otp();
        $response = $otp->validate($request->un, $request->otp);

        $user = Users::whereEmail($request->un)->first();

        if ($response->status == false) {
          $wrongOtpCount =  \Illuminate\Support\Facades\Session::get('wrong_otp');
            $wrongOtpCount = !empty($wrongOtpCount)?$wrongOtpCount:1;
            \Illuminate\Support\Facades\Session::put('wrong_otp',$wrongOtpCount+1);
            if ($wrongOtpCount>3){
                return redirect()->route('login')->with([
                    'error' => true,
                    'error.title' => 'Sorry',
                    'error.message' => $response->message,
                ]);
            }else {
                return redirect()->route('users.otp')->with([
                    'error' => true,
                    'error.title' => 'Sorry',
                    'error.message' => $response->message,
                ]);
            }

        }else {
            Auth::attempt($credentials);
            if (Auth::user()->hasRole(['Super Admin'])) {
                return redirect()->to('/dsuper')->with(['user' => $user,
                    'success' => true,
                    'success.title' => 'Hi ' . $user->first_name . ',',
                    'success.message' => 'Welcome to Buntalk !']);
            } elseif (Auth::user()->hasRole(['Owner'])) {
                return redirect()->to('/downer')->with(['user' => $user,
                    'success' => true,
                    'success.title' => 'Hi ' . $user->first_name . ',',
                    'success.message' => 'Welcome to Buntalk !']);
            }elseif (Auth::user()->hasRole(['Assistant'])) {
                return redirect()->to('/dassisatant')->with(['user' => $user,
                    'success' => true,
                    'success.title' => 'Hi ' . $user->first_name . ',',
                    'success.message' => 'Welcome to Buntalk !']);
            }elseif (Auth::user()->hasRole(['Production Manager'])) {
                return redirect()->to('/dpm')->with(['user' => $user,
                    'success' => true,
                    'success.title' => 'Hi ' . $user->first_name . ',',
                    'success.message' => 'Welcome to Buntalk !']);
            }elseif (Auth::user()->hasRole(['Stock Manager'])) {
                return redirect()->to('/dsm')->with(['user' => $user,
                    'success' => true,
                    'success.title' => 'Hi ' . $user->first_name . ',',
                    'success.message' => 'Welcome to Buntalk !']);
            }elseif (Auth::user()->hasRole(['Sales Agent'])) {
                return redirect()->to('/dsr')->with(['user' => $user,
                    'success' => true,
                    'success.title' => 'Hi ' . $user->first_name . ',',
                    'success.message' => 'Welcome to Buntalk !']);
            }elseif (Auth::user()->hasRole(['Sales Manager'])) {
                return redirect()->to('/dsalesm')->with(['user' => $user,
                    'success' => true,
                    'success.title' => 'Hi ' . $user->first_name . ',',
                    'success.message' => 'Welcome to Buntalk !']);
            }elseif (Auth::user()->hasRole(['Stores End Product'])) {
                return redirect()->to('/end')->with(['user' => $user,
                    'success' => true,
                    'success.title' => 'Hi ' . $user->first_name . ',',
                    'success.message' => 'Welcome to Buntalk !']);
            }
            else {
                return redirect()->to('/dassisatant')->with(['user' => $user,
                    'success' => true,
                    'success.title' => 'Hi ' . $user->first_name . ',',
                    'success.message' => 'Welcome to Buntalk !']);


            }
        }


    }


    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->un,
            'password' => $request->pw,
        ];

        if (Auth::attempt([
            'email' => $request->un,
            'password' => $request->pw,
        ])) {

            $user = Users::whereEmail($request->un)->first();

                if (!empty($user) && !empty($user->numbers) && $user->numbers->count() > 0) {
                    $otp = new Otp();
                    $otp_code = $otp->generate($user->email, 5, 3);
                    foreach ($user->numbers as $number) {
                        $msg = "Dear $user->first_name , Please use $otp_code->token as the Buntalk OTP for current login";
                        $this->sendSMS($number->number, $msg);
                    }
                    $email = $request->un;
                    $pw = $request->pw;
                    return view('otp', compact('email', 'pw'));
                }
            if (Auth::check()) {
                return redirect()->route('dashboard.superadministrator')->with(['user' => $user,
                    'success' => true,
                    'success.title' => 'Hi ' . $user->first_name . ',',
                    'success.message' => 'Welcome to Buntalk !']);
            }
            return redirect()->back();

        } else {
            $user = Users::whereEmail($request->un)->first();
            return view('UserManager::user.login')->with([
                'user' => $user,
                'error' => true,
                'error.title' => 'ACCESS DENIED !',
                'error.message' => 'Invalid User Name or Password !'
            ]);
        }
//        $g_user = Users::whereEmail($request->un)->first();
//
//        $user = Sentinel::validateCredentials($g_user, $credentials);

//        if ($user) {
//            if (!empty($g_user) && !empty($g_user->numbers) && $g_user->numbers->count() > 0) {
//                $otp = new Otp();
//                $otp_code = $otp->generate($g_user->email, 5, 3);
//                foreach ($g_user->numbers as $number) {
//                    $msg = "Dear $g_user->first_name , Please use $otp_code->token as the Buntalk OTP for current login";
//                    $this->sendSMS($number->number, $msg);
//                }
//                $email = $request->un;
//                $pw = $request->pw;
//                return view('otp', compact('email', 'pw'));
//            }
//        }

//        $user = Sentinel::authenticate($credentials);
//
//        if ($user) {
//            if ($user == Sentinel::inRole('superadministrator')) {
//                return redirect()->route('dashboard.superadministrator')->with(['user' => $user,
//                    'success' => true,
//                    'success.title' => 'Hi ' . $user->first_name . ',',
//                    'success.message' => 'Welcome to Buntalk !']);
//            }
// elseif
//            ($user == Sentinel::inRole('owner')) {
//                return redirect()->route('dashboard.owner')->with([
//                    'user' => $user,
//                    'success' => true,
//                    'success.title' => 'Hi ' . $user->first_name . ',',
//                    'success.message' => 'Welcome to Buntalk !'
//                ]);
//            } elseif ($user == Sentinel::inRole('assistant')) {
//                return redirect()->route('dashboard.assistant')->with([
//                    'user' => $user,
//                    'success' => true,
//                    'success.title' => 'Hi ' . $user->first_name . ',',
//                    'success.message' => 'Welcome to Buntalk !'
//                ]);
//            } elseif ($user == Sentinel::inRole('production-manager')) {
//                return redirect()->route('dashboard.production-manager')->with([
//                    'user' => $user,
//                    'success' => true,
//                    'success.title' => 'Hi ' . $user->first_name . ',',
//                    'success.message' => 'Welcome to Buntalk !'
//                ]);
//            } elseif ($user == Sentinel::inRole('stock-manager')) {
//                return redirect()->route('dashboard.stock-manager')->with([
//                    'user' => $user,
//                    'success' => true,
//                    'success.title' => 'Hi ' . $user->first_name . ',',
//                    'success.message' => 'Welcome to Buntalk !'
//                ]);
//            } elseif ($user == Sentinel::inRole('sales-ref')) {
//                return redirect()->route('dashboard.sales-rep-manager')->with([
//                    'user' => $user,
//                    'success' => true,
//                    'success.title' => 'Hi ' . $user->first_name . ',',
//                    'success.message' => 'Welcome to Buntalk !'
//                ]);
//            } elseif ($user == Sentinel::inRole('sales-manager')) {
//                return redirect()->route('dashboard.sales-head-manager')->with([
//                    'user' => $user,
//                    'success' => true,
//                    'success.title' => 'Hi ' . $user->first_name . ',',
//                    'success.message' => 'Welcome to Buntalk !'
//                ]);
//            } elseif ($user == Sentinel::inRole('packing')) {
//                return redirect()->route('dashboard.packing')->with([
//                    'user' => $user,
//                    'success' => true,
//                    'success.title' => 'Hi ' . $user->first_name . ',',
//                    'success.message' => 'Welcome to Buntalk !'
//                ]);
//            } elseif ($user == Sentinel::inRole('human-resource')) {
//                return redirect()->route('dashboard.human-resource')->with([
//                    'user' => $user,
//                    'success' => true,
//                    'success.title' => 'Hi ' . $user->first_name . ',',
//                    'success.message' => 'Welcome to Buntalk !'
//                ]);
//            } elseif ($user == Sentinel::inRole('accountant')) {
//                return redirect()->route('dashboard.accountant')->with([
//                    'user' => $user,
//                    'success' => true,
//                    'success.title' => 'Hi ' . $user->first_name . ',',
//                    'success.message' => 'Welcome to Buntalk !'
//                ]);
//            } elseif ($user == Sentinel::inRole('stores-end-product')) {
//                return redirect()->route('dashboard.end')->with([
//                    'user' => $user,
//                    'success' => true,
//                    'success.title' => 'Hi ' . $user->first_name . ',',
//                    'success.message' => 'Welcome to Buntalk !'
//                ]);
//            }
//        } else {
//            return view('UserManager::user.login')->with([
//                'user' => $user,
//                'error' => true,
//                'error.title' => 'ACCESS DENIED !',
//                'error.message' => 'Invalid User Name or Password !'
//            ]);
//        }
    }

    public function logout(Request $request)
    {
        if ($request->ajax()) {
            dd(1);
            Auth::logout();
            \Session::flash('success',"logout");
            return redirect()->route('user.login')->with('true');


        } else {

                return redirect('/');

        }


    }

    public function register(Request $request)
    {
//        $credentials = [
//            'email' => $request->un,
//            'password' => $request->pw,
//        ];

        $credentials = [
            'email' => 'user',
            'password' => '123',
        ];

        $user = Sentinel::registerAndActivate($credentials);
        return redirect('/');
    }

    public function index()
    {

        $user = Users::with('roles')->get();
        $roles = UserRole::all();
        return view('UserManager::user.create')->with([
            'users' => $user,
            'roles' => $roles
        ]);
    }

    public function create()
    {
        $roles = UserRole::orderBy('name', 'asc')->get();
        return view('UserManager::user.add')->with([
            'roles' => $roles,
        ]);
    }

    public function save(Request $request)
    {
        $username_submitted = Users::where('email', '=', $request->get('username'))->get();
        if (isset($username_submitted[0])) {
            return redirect('user')->with(['error' => true,
                'error.message' => 'Already EXSIST User!',
                'error.title' => 'Try Again!']);
        } else {
            $credentials = [
                'first_name' => $request->fn,
                'last_name' => $request->ln,
                'email' => $request->un,
                'password' => $request->nic
            ];
            $user = Sentinel::registerAndActivate($credentials);
            $role = Sentinel::findRoleById($request->role);
            $role->users()->attach($user);
            return redirect('user')->with(['success' => true,
                'success.message' => 'User Created successfully!',
                'success.title' => 'Well Done!']);

        }


    }

    public function listView()
    {
        return view('UserPackage::user.list');
    }

    public function jsonList(Request $request)
    {
        if ($request->ajax()) {
            $data = Users::with(['roles'])->get();
            $jsonList = array();
            $i = 1;
            foreach ($data as $key => $user) {
                $roles = '';
                foreach ($user->roles as $key_role => $value_role) {
                    $roles .= $value_role->name . ' ';
                }

                $dd = array();
                array_push($dd, $i);

                if ($user->first_name != "") {
                    array_push($dd, $user->first_name . ' ' . $user->last_name);
                } else {
                    array_push($dd, "-");
                }

                if ($user->email != "") {
                    array_push($dd, $user->email);
                } else {
                    array_push($dd, "-");
                }
                if (count($user->roles) > 0) {
                    array_push($dd, $roles);
                } else {
                    array_push($dd, "-");
                }

                $permissions = Permission::whereIn('name', ['user.edit', 'admin'])->where('status', '=', 1)->pluck('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="blue" onclick="window.location.href=\'' . url('user/edit/' . $user->id) . '\'" data-toggle="tooltip" data-placement="top" title="Edit User"><i class="fa fa-pencil"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Edit Disabled"><i class="fa fa-pencil"></i></a>');
                }

                $permissions = Permission::whereIn('name', ['user.delete', 'admin'])->where('status', '=', 1)->pluck('name');
                if (Sentinel::hasAnyAccess($permissions)) {
                    array_push($dd, '<center><a href="#" class="red user-delete" data-id="' . $user->id . '" data-toggle="tooltip" data-placement="top" title="Delete User"><i class="fa fa-trash-o"></i></a></center>');
                } else {
                    array_push($dd, '<a href="#" class="disabled" data-toggle="tooltip" data-placement="top" title="Delete Disabled"><i class="fa fa-trash-o"></i></a>');
                }

                array_push($jsonList, $dd);
                $i++;
            }
            return Response::json(array('data' => $jsonList));
        } else {
            return Response::json(array('data' => []));
        }
    }

    public function status(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->input('id');
            $status = $request->input('status');

            $user = User::find($id);
            if ($user) {
                $user->status = $status;
                $user->save();
                return response()->json(['status' => 'success']);
            } else {
                return response()->json(['status' => 'invalid_id']);
            }
        } else {
            return response()->json(['status' => 'not_ajax']);
        }
    }

    public function destroy(Users $users)
    {
        $users->delete();
        if ($users->trashed()) {
            return true;
        } else {
            return false;
        }
    }

    public function editView($id)
    {
        $branch = Branch::where('status', '=', 1)->get();
        $user = User::where('status', '=', 1)->get();
        $curUser = User::with(['roles'])->find($id);
        $srole = array();
        foreach ($curUser->roles as $key => $value) {
            array_push($srole, $value->id);
        }

        $roles = UserRole::orderBy('name', 'asc')->get();
        $roles_array = array();
        foreach ($roles as $key => $value) {
            if (in_array($value->id, $srole, true)) {
                array_push($roles_array, '<option selected value="' . $value->id . '">' . $value->name . '</option>');
            } else {
                array_push($roles_array, '<option  value="' . $value->id . '">' . $value->name . '</option>');
            }
        }

        if ($curUser) {

            return view('userManage::user.edit')->with([
                'curUser' => $curUser,
                'users' => $user,
                'roles' => $roles_array,
                'branch' => $branch
            ]);
        } else {
            return view('errors.404');
        }
    }

    public function edit(Request $request, $id)
    {
        // return $request->get( 'supervisor' );

        $usercount = User::where('id', '!=', $id)->where('username', '=', $request->get('username'))->count();
        if ($usercount == 0) {
            $userOld = User::with(['roles'])->where('id', $id)->take(1)->get();
            $user = $userOld[0];
            $user->first_name = $request->get('first_name');
            $user->last_name = $request->get('last_name');
            $user->username = $request->get('username');
            $user->branch = $request->get('branch');
            $user->makeChildOf(Sentinel::findById($request->get('supervisor')));

            foreach ($user->roles as $key => $value) {
                $role = Sentinel::findRoleById($value->id);
                $role->users()->detach($user);
            }

            $user->save();
            //attach user for role
            foreach ($request->get('roles') as $key => $value) {
                $role = Sentinel::findRoleById($value);
                $role->users()->attach($user);
            }
            return redirect('user/list')->with(['success' => true,
                'success.message' => 'User updated successfully!',
                'success.title' => 'Good Job!']);
        } else {
            return redirect('user/edit/' . $id)->with(['error' => true,
                'error.message' => 'User Already Exsist!',
                'error.title' => 'Duplicate!']);
        }


    }

}
