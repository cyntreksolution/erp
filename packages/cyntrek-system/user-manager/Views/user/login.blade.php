<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>LOGIN | BUNTALK </title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <link rel="apple-touch-icon" href="apple-touch-icon.html">


    <link rel="stylesheet" href="{{asset('assets/vendor/css/bootstrap.css')}}">

    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/material-design-iconic-font/css/material-design-iconic-font.min.css')}}">

    <link rel="stylesheet" href="{{asset('assets/vendor/owl-carousel/owl.carousel.css')}}">
    <!-- / plugins for the current page -->
    <!-- site-wide stylesheets -->
    <link rel="stylesheet" href="{{asset('assets/css/site.css')}}">
    <!-- styles for the current page -->
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/login.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/toastr/toastr.css')}}">
    <!-- / styles for the current page -->
</head>

<body class="simple-page page-login"><!--[if lt IE 10]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->


<header class="login-page-header d-flex">
    <div class="mr-auto">
        <a href="/" class="d-flex align-items-center">
{{--            <h5 class="text-white m-0">BUNTALK</h5>--}}
        </a>
    </div>
    {{--    <div id="form-switch-btns" class="btn-group switch-btns">--}}
    {{--        <button data-target="#signup-form-wrap" class="btn">Sign up</button>--}}
    {{--        <button data-target="#signin-form-wrap" class="btn" disabled="disabled">Sign in</button>--}}
    {{--    </div>--}}
</header><!-- /.login-page-header -->
<div class="login-page-wrap">
    <div class="side first-side">
        <div class="side-content text-center">
            <img class="mb-5" style="width: 450px" src="{{asset('assets/img/buntalk-logo.jpg')}}" alt="">
            <div id="login-page-slider" data-plugin="owlCarousel">
                <div class="item">
                    <h4 style="color:#501310;font-weight: 900; ">RANJANAS HOLDINGS (PVT) LTD </h4>
                     <h5 style="color:#501310;font-weight: 900; ">   katukurunda,Habaraduwa,Sri Lanka<br>
                 buntalksl@icloud.com<br>
                 +94 77 330 4678</h5>
                </div>
            </div>
        </div>
    </div><!-- /.first-side -->
    <div class="side second-side w-100 location">
        <div class="side-content">
            <img class="mb-5 d-md-none d-sm-block" style="width: 100%" src="{{asset('assets/img/buntalk-logo.jpg')}}" alt="">
            <div id="signin-form-wrap" class="form-wrap show">
                <h4 class="my-5 font-weight-light text-center welcome-msg">Warmly Welcome to Buntalk Management System !</h4>
                {{ Form::open(array('route' => 'login','id'=>'signin-form','class'=>'form')) }}
                {{ csrf_field() }}
                <div class="form-group">
                    {!! Form::text('email','', array('class' => 'form-control','placeholder'=>'Your username')) !!}
                </div>
                <div class="form-group">
                    {!! Form::password('password', array('class' => 'form-control','placeholder'=>'Your password')) !!}
                </div>
                {!! Form::submit('Sign in',array('class' => 'btn btn-outline-success-theme py-2 mt-5','style'=>'width:200px','value'=>'Sign in')) !!}
                {{ Form::close() }}
            </div><!-- /#signin-form-wrap -->
            <div id="signup-form-wrap" class="form-wrap"><h4 class="my-5 font-weight-light text-uppercase">Sing Up to
                    benefit from our services</h4>
                <form id="signup-form" class="form" action="#">
                    <div class="form-group"><input type="email" class="form-control" placeholder="Eamil"></div>
                    <div class="form-group"><input type="text" class="form-control" placeholder="Username"></div>
                    <div class="form-group"><input type="password" class="form-control" placeholder="Password"></div>
                    <div class="form-group"><input type="password" class="form-control" placeholder="Confirm password">
                    </div>
                    <input type="submit" class="btn btn-outline-success py-2 mt-5" style="width: 200px" value="Sign up">
                </form>
            </div>
        </div>
    </div>
</div>

<script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/tether/dist/js/tether.min.j')}}s"></script>

<script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- plugins for the current page -->
<script src="{{asset('assets/vendor/owl-carousel/owl.carousel.min.js')}}"></script>
<!-- / plugins for the current page -->
<!-- scripts for the current page -->
<script src="{{asset('assets/examples/js/pages/login.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/toastr/toastr.js')}}"></script>

<script>
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }

    @if(session('success'))
        Command: toastr["success"]("{{session('success.message')}} ", "{{session('success.title')}}")
    @elseif(session('error'))
        Command: toastr["error"]("{{session('error.message')}} ", "{{session('error.title')}}")
    @elseif(session('warning'))
        Command: toastr["warning"]("{{session('warning.message')}} ", "{{session('warning.title')}}")
    @elseif(session('info'))
        Command: toastr["info"]("{{session('info.message')}} ", "{{session('info.title')}}")
    @endif
</script>
</body>
</html>


{{--<html lang="en">--}}
{{--<head>--}}
{{--<meta charset="utf-8" />--}}
{{--<title>Login - PSMS </title>--}}
{{--<meta name="description" content="" />--}}
{{--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />--}}
{{--<meta http-equiv="X-UA-Compatible" content="IE=edge">--}}

{{--<!-- for ios 7 style, multi-resolution icon of 152x152 -->--}}
{{--<meta name="apple-mobile-web-app-capable" content="yes">--}}
{{--<meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">--}}
{{--<link rel="apple-touch-icon" href="../assets/images/logo.png">--}}
{{--<meta name="apple-mobile-web-app-title" content="Flatkit">--}}
{{--<!-- for Chrome on Android, multi-resolution icon of 196x196 -->--}}
{{--<meta name="mobile-web-app-capable" content="yes">--}}
{{--<link rel="shortcut icon" sizes="196x196" href="../assets/images/logo.png">--}}

{{--<!-- style -->--}}
{{--<link rel="stylesheet" href="../assets/animate.css/animate.min.css" type="text/css" />--}}
{{--<link rel="stylesheet" href="../assets/glyphicons/glyphicons.css" type="text/css" />--}}
{{--<link rel="stylesheet" href="../assets/font-awesome/css/font-awesome.min.css" type="text/css" />--}}
{{--<link rel="stylesheet" href="../assets/material-design-icons/material-design-icons.css" type="text/css" />--}}

{{--<link rel="stylesheet" href="../assets/bootstrap/dist/css/bootstrap.min.css" type="text/css" />--}}
{{--<link rel="stylesheet" href="../assets/styles/app.min.css">--}}
{{--<link rel="stylesheet" href="../assets/styles/font.css" type="text/css" />--}}
{{--</head>--}}
{{--<body>--}}
{{--<div class="app" id="app">--}}

{{--<!-- ############ LAYOUT START-->--}}
{{--<div class="center-block w-xxl w-auto-xs p-y-md">--}}
{{--<div class="navbar">--}}
{{--<div class="pull-center">--}}
{{--<a class="navbar-brand">--}}
{{--<img src="{{asset('assets/images/logo.png') }}">--}}
{{--<span class="hidden-folded inline">PSMS</span>--}}
{{--</a>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="p-a-md box-color r box-shadow-z1 text-color m-a">--}}
{{--<div class="m-b text-md text-center">--}}
{{--Sign in with your PSMS Account--}}
{{--</div>--}}
{{--{{ Form::open(array('url' => 'user/login')) }}--}}
{{--{{ csrf_field() }}--}}
{{--<div class="md-form-group float-label">--}}
{{--{!! Form::text('un','', array('class' => 'md-input')) !!}--}}
{{--{!! Form::label('un', 'E Mail') !!}--}}
{{--</div>--}}
{{--<div class="md-form-group float-label">--}}
{{--{!! Form::password('pw',array('class' => 'md-input')) !!}--}}
{{--{!! Form::label('pw', 'Password') !!}--}}
{{--</div>--}}
{{--<div class="m-b-md">--}}
{{--<label class="md-check">--}}
{{--{!! Form::checkbox('lg', 'keep') !!}--}}
{{--<i class="primary"></i> Keep me signed in--}}
{{--</label>--}}
{{--</div>--}}
{{--{!! Form::submit('Sign in',array('class' => 'btn primary btn-block p-x-md')) !!}--}}
{{--{{ Form::close() }}--}}
{{--</div>--}}

{{--<div class="p-v-lg text-center">--}}
{{--<div class="m-b"><a ui-sref="access.forgot-password" href="forgot-password.html" class="text-primary _600">Forgot password?</a></div>--}}
{{--<div>Do not have an account? <a ui-sref="access.signup" href="signup.html" class="text-primary _600">Sign up</a></div>--}}
{{--</div>--}}
{{--</div>--}}

{{--<!-- ############ LAYOUT END-->--}}

{{--</div>--}}
{{--<script src="scripts/app.html.js"></script>--}}
{{--</body>--}}

{{--</html>--}}
