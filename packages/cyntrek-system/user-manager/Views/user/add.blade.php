@extends('layouts.back.master') @section('current_title','NEW USER')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-3.5.2/select2.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/back/vendor/select2-bootstrap/select2-bootstrap.css')}}"/>

@stop
@section('current_path')
    <div id="hbreadcrumb" class="pull-right">
        <ol class="hbreadcrumb breadcrumb">
            <li><a href="{{url('user/list')}}">User Management</a></li>
            <li class="active">
                <span>New User</span>
            </li>
        </ol>
    </div>
@stop
@section('content')

            <form method="POST" class="form-horizontal" id="form">
                <div class="card-content">
                    <h4 class="card-title">NEW USER</h4>
                    {{ Form::open(array('url' => 'user/ad','method'=>'post')) }}


                            <input name="first_name" type="text" class="form-control">



                            <input name="last_name" type="text" class="form-control">


                <div class="col-md-12">

                        <select id="type" name="role" class="selectpicker" data-style="select-with-transition"
                                title="Select Role">
                            @foreach($roles as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>

                </div>

                <div class="col-md-6">

                            <input name="username" type="text" class="form-control">

                </div>

                <div class="col-md-6">

                            <input name="password" type="password" class="form-control">

                </div>

                <div class="form-group">
                    <div class="col-sm-8">
                        <button class="btn btn-primary" type="submit">Done</button>
                    </div>
                </div>
                </div>
                {!!Form::close()!!}



@stop
@section('js')
    <script src="{{asset('assets/back/vendor/select2-3.5.2/select2.min.js')}}"></script>
    <script src="{{asset('assets/back/vendor/jquery-validation/jquery.validate.min.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(".js-source-states").select2();

            $("#form").validate({
                rules: {
                    first_name: {
                        required: true
                    },
                    last_name: {
                        required: true
                    },
                    roles: {
                        required: true,

                    },
                    branch: {
                        required: true
                    },
                    username: {
                        required: true
                    },
                    password: {
                        required: true
                    }
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        });


    </script>
@stop