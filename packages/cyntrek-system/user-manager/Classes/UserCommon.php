<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/24/2017
 * Project : psms
 */

namespace UserManager\Classes;


use App\Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Sentinel;

class UserCommon
{

    public static function isOnline($type)
    {
        $school = \Request::get('school');
        return $online = DB::table('users')
            ->select('roles.name', 'users.*', 'sessions.last_activity')
            ->leftJoin('sessions', 'users.id', '=', 'sessions.user_id')
            ->join('role_users', 'users.id', '=', 'role_users.user_id')
            ->join('roles', 'role_users.role_id', '=', 'roles.id')
            ->where('roles.slug', '=', $type)
            ->where('users.school', '=', $school)
            ->orderBy('sessions.last_activity', 'desc')
            ->groupBy('users.id')
            ->get();
    }

}