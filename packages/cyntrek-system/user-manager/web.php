<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */


Route::middleware(['web'])->group(function () {

    Route::get('otp', 'UserManager\Controllers\UserController@otpView')->name('users.otp');
    Route::post('otp', 'UserManager\Controllers\UserController@otpSubmit')->name('users.otp');
    Route::prefix('user')->group(function () {

        Route::get('logouts', 'UserManager\Controllers\UserController@logout')->name('user.logout');

//        Route::post('login', 'UserManager\Controllers\UserController@login')->name('user.login');
    });
});

Route::middleware(['web'])->group(function () {
    Route::prefix('user')->group(function () {
        Route::get('profile', 'UserManager\Controllers\UserController@profile')->name('user.profile');
    });
});

Route::middleware(['web'])->group(function () {
    Route::prefix('user')->group(function () {
        Route::get('', 'UserManager\Controllers\UserController@index')->name('user.index');
//
        Route::get('create', 'UserManager\Controllers\UserController@create')->name('user.create');
//
//        Route::get('{user}', 'UserManager\Controllers\UserController@show')->name('user.show');
//
//        Route::get('{user}/edit', 'UserManager\Controllers\UserController@edit')->name('user.edit');
//
//
        Route::post('', 'UserManager\Controllers\UserController@save')->name('user.store');
//
//        Route::put('{user}', 'UserManager\Controllers\UserController@update')->name('user.update');
//
//        Route::delete('{user}', 'UserManager\Controllers\UserController@destroy')->name('user.destroy');
    });
});