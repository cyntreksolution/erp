<?php

namespace CashLockerManager\Controllers;

use App\EndProductInquiry;
use App\LoadingHeader;
use App\UtilityBill;
use App\Outlet;
use EmployeeManager\Models\Employee;
use CashLockerManager\Models\equipment;
use CashLockerManager\Models\loanlease;
use CashLockerManager\Models\main_desc_cashlocker;
use Carbon\Carbon;
use CashLockerManager\Classes\CashTransaction;
use CashLockerManager\Models\CashBookTransaction;
use CashLockerManager\Models\CashLocker;
use CashLockerManager\Models\VehicleEquipment;
use PurchasingOrderManager\Models\PurchasingOrder;
use RawMaterialManager\Models\RawAdjust;
use RecipeManager\Models\RecipeContent;
use Response;
use SalesOrderManager\Models\SalesOrder;
use SalesRepManager\Models\SalesRep;
use Sentinel;
use RawMaterialManager\Models\RawMaterial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use StockManager\Classes\StockTransaction;
use StockManager\Models\Stock;
use StockManager\Models\StockRawMaterial;
use SupplierManager\Models\Supplier;
use VehicleManager\Models\Vehicle;

class CashLockerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:cash_locker-index', ['only' => ['index']]);
        $this->middleware('permission:CashLocker-list', ['only' => ['list']]);
        $this->middleware('permission:Description-list', ['only' => ['list']]);
        $this->middleware('permission:AddLoanLeasing-list', ['only' => ['list']]);
        $this->middleware('permission:AddEquipments-list', ['only' => ['list']]);
        $this->middleware('permission:cash_locker-create', ['only' => ['create','store']]);
        $this->middleware('permission:cash_locker-cashIn', ['only' => ['cashIn']]);
        $this->middleware('permission:cash_locker-addDescription', ['only' => ['cashIn']]);
        $this->middleware('permission:cash_locker-viewDescription', ['only' => ['descshow']]);
        $this->middleware('permission:cash_locker-descActive', ['only' => ['descactive','descinactive']]);
        $this->middleware('permission:cash_locker-descdelete', ['only' => ['descdelete']]);
        $this->middleware('permission:cash_locker-descupdate', ['only' => ['descupdate']]);
        $this->middleware('permission:cash_locker-loanview', ['only' => ['loanview']]);
        $this->middleware('permission:cash_locker-newLoan', ['only' => ['loan']]);
        $this->middleware('permission:cash_locker-equipview', ['only' => ['equipview']]);
        $this->middleware('permission:cash_locker-newEquip', ['only' => ['equip']]);


    }
    public function index()
    {
        return redirect(route('cash_locker.create'));
        return view('CashLockerManager::cash_locker.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cashinDesc = main_desc_cashlocker::groupBy('main_desc')->get();

        $cashIn = CashBookTransaction::selectRaw(' COALESCE(sum(amount)) as amount ')
                                        ->whereDate('created_at', '=', Carbon::today()->format('Y-m-d'))
                                        ->where('type','=',1)
                                        ->get();

        $cashOut = CashBookTransaction::selectRaw(' COALESCE(sum(amount)) as amount ')
                                        ->whereDate('created_at', '=', Carbon::today()->format('Y-m-d'))
                                        ->where('type','=',2)
                                        ->get();

        $emp = Employee::all();
        $balance = CashLocker::where('id', 1)->first();
        $transaction = CashBookTransaction::where('created_at', '>', Carbon::today())->get();
        return view('CashLockerManager::cash_locker.create')->with([
            'balance' => $balance,
            'transaction' => $transaction,
            'cashinDescs' => $cashinDesc,
            'emps' => $emp,
            'cashIn' => $cashIn,
            'cashOut' => $cashOut,
        ]);
    }

    public function editCashInHand(Request $request)
    {

        $newAmount = $request->new_cash;
        $cash = CashLocker::where('id', 1)->first();

        $cashDif = $newAmount-$cash->balance;
        $description = 'Admin Cash Adjustment';
        $main_desc = 'Admin';


        if ($cashDif > 0) {
            $amount = $cashDif;
            CashTransaction::updateCashLocker(1, $amount);
            CashTransaction::updateCashTransaction(1, $amount, 0, 1, $description,-4,-4,'CashLockerChange','Other');
        } else {
            $amount = $cashDif * (-1);
            CashTransaction::updateCashLocker(2, $amount);
            CashTransaction::updateCashTransaction(2, $amount, 0, 0, $description,-4,-4,'CashLockerChange','Other');
        }

        $cashinDesc = main_desc_cashlocker::groupBy('main_desc')->get();

        $cashIn = CashBookTransaction::selectRaw(' COALESCE(sum(amount)) as amount ')
            ->whereDate('created_at', '=', Carbon::today()->format('Y-m-d'))
            ->where('type','=',1)
            ->get();

        $cashOut = CashBookTransaction::selectRaw(' COALESCE(sum(amount)) as amount ')
            ->whereDate('created_at', '=', Carbon::today()->format('Y-m-d'))
            ->where('type','=',2)
            ->get();

        return redirect(route('cash_locker.create'));
        return view('CashLockerManager::cash_locker.index');
    }


    public function status()
    {
        $raws = RawMaterial::orderBy('name', 'asc')->get();
        return view('CashLockerManager::cash_locker.stock')->with([
            'raws' => $raws
        ]);
    }


    public function store(Request $request)
    {
        $item = main_desc_cashlocker::find($request->category_id);


        $data = null;
        switch ($item->category) {
            case 'Agents';
                $data1 = SalesRep::where('id','=',$request->use_type)->first();//pluck('name_with_initials','id');
                $data = $data1->name_with_initials;
                break;
            case 'Vehicle';
                $data1 = Vehicle::where('id','=',$request->use_type)->first();//pluck('name','id');
                $data = $data1->name;
                break;
            case 'Employees';
                $data1 =Employee::where('id','=',$request->use_type)->first();//pluck('full_name','id');
                $data = $data1->full_name;

                break;
            case 'Equipments';
                $data1 =VehicleEquipment::where('id','=',$request->use_type)->first();//pluck('eq_desc','id');
                $data = $data1->eq_desc;
                break;
            case 'LoanLease';
                $data1 =loanlease::where('id','=',$request->use_type)->first();//pluck('loan_desc','id');
                $data = $data1->loan_desc;
                break;
            case 'Supliers';
                $data1 =Supplier::where('id','=',$request->use_type)->first();//pluck('supplier_name','id');
                $data = $data1->supplier_name;
                break;
            case 'UtilityBills';
                $data1 =UtilityBill::where('id','=',$request->use_type)->first();//pluck('description','id');
                $data = $data1->description;
                break;
            case 'Outlet';
                $data1 =Outlet::where('id','=',$request->use_type)->first();//pluck('name','id');
                $data = $data1->name;
                break;
        }
        $lastData = null;


        if(!empty($request->type_data_text))
        {
            $lastdata = $request->type_data_text;
        }else
        {
            $lastdata = $data;
        }

        $subdesc = main_desc_cashlocker::find($request->category_id);


        $amount = $request->amount;
        $use_type = !empty($request->type_data_text)?$request->type_data_text:$request->use_type;
        $flow = $request->cash_type;
        //$description = $subdesc->sub_desc.'-'.$request->remark.' For '.$use_type;
        $description = $subdesc->sub_desc.'-'.$request->remark.' For '.$lastdata;
        $main_desc_id = $request->category_id;
        $main_desc = $subdesc->main_desc;

        $main_desc_category = $subdesc->category;

        if ($flow == 'CashIn') {
            CashTransaction::updateCashLocker(1, $amount);
            CashTransaction::updateCashTransaction(1, $amount, 0, 1, $description,$main_desc_id,$use_type,$main_desc,$main_desc_category);
        } else {
            CashTransaction::updateCashLocker(2, $amount);
            CashTransaction::updateCashTransaction(2, $amount, 0, 0, $description,$main_desc_id,$use_type,$main_desc,$main_desc_category);
        }
        return redirect(route('cash_locker.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param \RawMaterialManager\Models\RawMaterial $raw_material
     * @return \Illuminate\Http\Response
     */
    public function show(CashLocker $cashLocker)
    {
        return view('CashLockerManager::cash_locker.show')->with([
            'raw_material' => $cashLocker
        ]);
    }

    public function details(CashLocker $cashLocker)
    {
        return view('CashLockerManager::cash_locker.details')->with([
            'raw_material' => $cashLocker,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \RawMaterialManager\Models\RawMaterial $raw_material
     * @return \Illuminate\Http\Response
     */
    public function edit(CashLocker $cashLocker)
    {
        return view('CashLockerManager::cash_locker.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \RawMaterialManager\Models\RawMaterial $raw_material
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CashLocker $cashLocker)
    {
//        if (isset($request->Name)) {
//            $raw_material->name = $request->Name;
//            $raw_material->save();
//            return redirect('raw_material/' . $raw_material->raw_material_id)->with([
//                'success' => true,
//                'success.title' => 'Congratulations !',
//                'success.message' => 'name has been updated'
//            ]);
//        }
//
//        if (isset($request->desc)) {
//            $raw_material->desc = $request->desc;
//            $raw_material->save();
//            return redirect('raw_material/' . $raw_material->raw_material_id)->with([
//                'success' => true,
//                'success.title' => 'Congratulations !',
//                'success.message' => 'Description has been updated'
//            ]);
//        }
//
//        if (isset($request->buffer_stock)) {
//            $raw_material->buffer_stock = $request->buffer_stock;
//            $raw_material->save();
//            return redirect('raw_material/' . $raw_material->raw_material_id)->with([
//                'success' => true,
//                'success.title' => 'Congratulations !',
//                'success.message' => 'Buffer stock has been updated'
//            ]);
//        }
//
//        if (isset($request->stock)) {
//            $currentStock = $raw_material->available_qty;
//            if ($raw_material->measurement == 'gram') {
//                $diffrence = $currentStock - ($request->stock * 1000);
//                $ajustedQty = ($request->stock * 1000);
//            } elseif ($raw_material->measurement == 'milliliter') {
//                $diffrence = $currentStock - ($request->stock * 1000);
//                $ajustedQty = ($request->stock * 1000);
//            } else {
//                $diffrence = $currentStock - $request->stock;
//                $ajustedQty = $request->stock;
//            }
//            $raw_material->available_qty = $ajustedQty;
//            $raw_material->save();
//
//            $adjusted = new RawAdjust();
//            $adjusted->raw_material_raw_material_id = $raw_material->raw_material_id;
//            $adjusted->current_stock = $currentStock;
//            $adjusted->difference = $diffrence;
//            $adjusted->created_by = Sentinel::getUser()->id;
//            $adjusted->save();
//
//            StockTransaction::rawMaterialStockTransaction(3, $raw_material->raw_material_id,$ajustedQty,$adjusted->id,1);
//            return redirect('raw_material/stock/status/' . $raw_material->raw_material_id)->with([
//                'info' => true,
//                'info.title' => 'Congratulations !',
//                'info.message' => 'Current Stock Adjusted'
//            ]);
//        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \RawMaterialManager\Models\RawMaterial $raw_material
     * @return \Illuminate\Http\Response
     */
    public function destroy(CashLocker $cashLocker)
    {
        $rc = RecipeContent::where('raw_material_id', '=', $cashLocker->raw_material_id)->count();
        if ($rc == 0) {
            $cashLocker->delete();
            if ($cashLocker->trashed()) {
                return 'true';
            } else {
                return 'false';
            }
        } else {
            return 'false';
        }

    }

    public function getraw(Request $request)
    {
        return $raw = RawMaterial::where('raw_material_id', '=', $request->id)->first();

    }

    public function stock(Request $request)
    {
//        $data = RawMaterial::all();
//        $jsonList = array();
//        $i = 1;
//
//        foreach ($data as $key => $item) {
//            $dd = array();
//            array_push($dd, $i);
//            array_push($dd, $item->name);
//            $qty = $item->available_qty / 1000;
//            array_push($dd, $item->available_qty);
//            array_push($dd, $item->available_qty);
//            array_push($dd, $item->available_qty);
//
//
////            array_push($dd, '<a href="edit/' . $item->stockpile_id . '" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons">dvr</i></a>');
////            array_push($dd, ' <a href="#" class="btn btn-simple btn-danger btn-icon remove" onclick="del(' . $item->stockpile_id . ')"><i class="material-icons">close</i></a>');
//            array_push($jsonList, $dd);
//            $i++;
//        }
//        return Response::json(array('data' => $jsonList));
//
//
    }

//******************************************    new entries        ************************
    public function cashIn(Request $request)
    {

        $main = new main_desc_cashlocker();
        $main->main_desc = $request->maindesc;
        $main->sub_desc = $request->subdesc;
        $main->category = $request->category;

        $main->type = $request->paytype;
        $main->save();

        $data = main_desc_cashlocker::all();
        return redirect()->back();
        // return view('mainDescription')->with('cashdata',$data);


        //$endProduct = EndProduct::find($request->end_product);
        //return view('mainDescription::cashLocker.cashdesc',compact('data'));


        // dd($request->all());

    }


    public function descshow()
    {
        $data = main_desc_cashlocker::all();

        return view('CashLockerManager::cash_locker.mainDescription')->with([

            'cashdata' => $data
        ]);

    }

    public function descactive($id)
    {
        $data = main_desc_cashlocker::find($id);
        $data->isInactive = 0;
        $data->save();
        return redirect()->back();
    }

    public function descinactive($id)
    {
        $data = main_desc_cashlocker::find($id);
        $data->isInactive = 1;
        $data->save();
        return redirect()->back();
    }

    public function descdelete($id)
    {
        $data = main_desc_cashlocker::find($id);
        $data->delete();
        return 'true';
    }

    public function descupdateview($id)
    {
        $data = main_desc_cashlocker::find($id);

        return view('CashLockerManager::cash_locker.descUpdate')->with([

            'cashdata' => $data
        ]);
        //return view('descUpdate')->with('cashdata',$data);

    }

    public function descupdate(Request $request)
    {
        $id = $request->id;
        $subdesc = $request->updatesubdesc;

        $data = main_desc_cashlocker::find($id);
        $data->sub_desc = $subdesc;

        $data->save();


    /*    $td = Carbon::today();

        $veri = EndProductInquiry::where('desc_id', '=', 2)->where('updated_at','<',$td)->get();
      //  dd($veri->count());
        foreach ($veri as $veris) {

            $inv = LoadingHeader::whereId($veris->so_id)->first();

            $so = SalesOrder::whereId($inv->so_id)->first();

            $veris->agent_id = $so->created_by;
            $veris->save();


        }*/


        return redirect(route('cash_locker.mdesc'));
    }


    public function loanview()
    {
        $data = loanlease::all();

        return view('CashLockerManager::cash_locker.loan')->with([
            'loan' => $data
        ]);
    }


    public function loan(Request $req)
    {


        $main = new loanlease();

        $main->loan_no = $req->loanNo;
        $main->loan_desc = $req->description;
        $main->amount = $req->amount;

        $main->save();

        $loan = loanlease::all();

        return view('CashLockerManager::cash_locker.loan')->with([
            'loan' => $loan
        ]);
    }

    public function equipview()
    {


        $data = VehicleEquipment::all();
        return view('CashLockerManager::cash_locker.equip')->with([
            'data' => $data
        ]);
    }


    public function equip(Request $req)
    {


        $main = new VehicleEquipment();

        $main->serial_no = $req->serialNo;
        $main->eq_desc = $req->eqdesc;
        $main->amount = $req->amount;

        $main->save();

        $data = VehicleEquipment::all();

        return view('CashLockerManager::cash_locker.equip')->with([
            'data' => $data
        ]);
    }


    public function getSubDescription(Request $request)
    {
        $main = main_desc_cashlocker::find($request->description_id);
        $subs = main_desc_cashlocker::where('main_desc', '=', $main->main_desc)->where('type', '=', $request->type)->pluck('sub_desc', 'id');
        return $subs;
    }
    public function getSubDescriptionID(Request $request)
    {

        $main = main_desc_cashlocker::find($request->description_id);
        $subs = main_desc_cashlocker::where('main_desc', '=', $main->main_desc)->where('type', '=', $request->type)->first();
        return $subs->id;
    }

    public function getCategoryData(Request $request)
    {

        $type = $request->type;
        $sub = $request->sub;
        $main = main_desc_cashlocker::find($request->description_id);
       // $item = main_desc_cashlocker::where('main_desc', '=', $main->main_desc)->where('type', '=', $type)->where('sub_desc', '=', $sub)->first();
        $item = main_desc_cashlocker::where('main_desc', '=', $main->main_desc)->where('type', '=', $type)->where('id', '=', $sub)->first();

        $data = null;
        switch ($item->category) {
            case 'Agents';
                $data = SalesRep::pluck('name_with_initials','id');
                break;
            case 'Vehicle';
                $data = Vehicle::pluck('name','id');
                break;
            case 'Employees';
                $data =Employee::pluck('full_name','id');
                break;
            case 'Equipments';
                $data =VehicleEquipment::pluck('eq_desc','id');
                break;
            case 'LoanLease';
                $data =loanlease::pluck('loan_desc','id');
                break;
            case 'Supliers';
                $data =Supplier::pluck('supplier_name','id');
                break;
            case 'UtilityBills';
                $data =UtilityBill::pluck('description','id');
                break;
            case 'Outlet';
                $data =Outlet::pluck('name','id');
                break;
        }
        return !empty($data)?$data:'1';
    }

}


