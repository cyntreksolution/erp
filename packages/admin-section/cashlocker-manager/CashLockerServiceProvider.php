<?php

namespace CashLockerManager;

use Illuminate\Support\ServiceProvider;

class CashLockerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/Views', 'CashLockerManager');
        require __DIR__ . '/web.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('CashLockerManager', function($app){
            return new CashLockerManager;
        });
    }
}
