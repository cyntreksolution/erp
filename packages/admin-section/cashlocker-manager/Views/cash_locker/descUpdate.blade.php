<!doctype html>
<html>

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">


    <title> Payment Type </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css"/>
    <script src="main.js"></script>
</head>
<body>
<div class="container">
    <div class="text-center">
        <br>
        <h5>Update Main Payment Type</h5>


        <div class="row">

            <div class="col-md-12">
                <form method="post" action="/descUpdatedata?{{$cashdata->id}}">
                    {{csrf_field()}}

                    <input type="text" class="form-control" name="maindesc" value="{{$cashdata->main_desc}}" readonly>
                    </br>
                    <input type="text" class="form-control" name="updatesubdesc" value="{{$cashdata->sub_desc}}">
                    </br>
                    <input type="text" class="form-control" name="category" value="{{$cashdata->category}}" readonly>
                    </br>
                    <input type="text" class="form-control" name="paytype" value="{{$cashdata->type}}" readonly>

                    <input type="hidden" name="id" value="{{$cashdata->id}}">
                    </br>

                    <input type="submit" class="btn btn-warning btn-block" style="width: 49%;" value="UPDATE">
                    <input type="button" class="btn btn-primary btn-block" style="width: 49%;" onclick="history.back()"
                           value="BACK">

                </form>
                </br>
                </br>
            </div>
        </div>
    </div>
</div>
</body>

