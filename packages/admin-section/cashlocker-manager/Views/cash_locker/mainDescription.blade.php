@extends('layouts.back.master')@section('title','Cash Book')
@section('css')

    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/modal.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.wizard.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.validation.css')}}">
    <link rel="stylesheet" href="{{asset('assets/packages/raw_materials/raw_material.css')}}">

    <style>


    </style>
@stop
{{--@section('current','Cash Book')--}}
{{--@section('current_url',route('raw_materials.create'))--}}

@section('content')
    <html>

    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
              crossorigin="anonymous">


    <title> Payment Type </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>



<div class="container">
    <div class="text-center">
        <br>
        <h5>Main Payment Type</h5>


            <div class="row">

                <div class="col-md-12" class="input-group">

                    <form method="post" action="/saveDesc">
                        {{csrf_field()}}

                        <select name="maindesc" required
                                class="form-control select2 ">

                        <option value="">Select Main Description</option>
                        <option value="Wastage">Wastage</option>
                        <option value="SalaryAndWages">Salary & Wages</option>
                        <option value="Capital">Capital</option>
                        <option value="Commission">Commission</option>
                        <option value="Maintanance">Maintanance</option>
                        <option value="UtilityBills">Utility Bills</option>
                        <option value="Rent">Rent</option>
                        <option value="RunningCost">Running Cost</option>
                        <option value="Taxes">Taxes</option>
                        <option value="Private">Private</option>
                        <option value="Responsibility">Responsibility</option>
                        <option value="Other">Other</option>
                    </select>
                    </br>
                    <input type="text" class="form-control" name="subdesc" placeholder="Type Sub Description Here">
                    </br>
                    <select name="paytype" required
                            class="form-control select2 ">

                            <option value="">Select Payment Type</option>
                            <option value="CashIn">Cash In</option>
                            <option value="CashOut">Cash Out</option>
                        </select>


                        </br>
                        <select name="category" required
                                class="form-control select2 ">

                            <option value="">Select Due Catogory</option>
                            <option value="Agents">Agents</option>
                            <option value="Vehicle">Vehicle</option>
                            <option value="Employees">Employees</option>
                            <option value="Equipments">Equipments</option>
                            <option value="LoanLease">Loan & Lease</option>
                            <option value="Supliers">Supliers</option>
                            <option value="Outlet">Outlet</option>
                            <option value="UtilityBills">Utility Bills</option>
                            <option value="Other">Other</option>

                        </select>


                        </br>
                        <input type="submit" class="btn btn-warning btn-block" style="width: 100%;" value="SUBMIT"
                               name="CashIn">
                    </form>
                    </br>
                    </br>

                </div>
                <br>
                <br>
                <div class="col-md-12">
                    <table class="table table-dark">
                        <thead class="thead-light">
                        <th style="">ID</th>
                        <th>Main Description</th>
                        <th>Sub Description</th>
                        <th>Category</th>
                        <th>Type</th>
                        <th>Position</th>
                        <th>Action</th>
                        </thead>
                        <tr>
                        @foreach($cashdata as $cashdt)
                            <tr>
                                <td>{{$cashdt->id}}</td>
                                <td>{{$cashdt->main_desc}}</td>
                                <td>{{$cashdt->sub_desc}}</td>
                                <td>{{$cashdt->category}}</td>
                                <td>{{$cashdt->type}}</td>
                                <td>
                                    @if($cashdt->isinactive)
                                        <button class="btn btn-danger">Inactive</button>

                                    @else
                                        <button class="btn btn-success">Active</button>
                                    @endif
                                </td>

                                <td>
                                    @if($cashdt->isinactive)
                                        @can('cash_locker-descActive')
                                            <a href="/descActive/{{$cashdt->id}}" class="btn btn-warning">Active</a>
                                        @endcan
                                        @can('cash_locker-descdelete')
                                            <a href="/descDelete/{{$cashdt->id}}" class="btn btn-danger">Delete</a>
                                        @endcan
                                        @can('cash_locker-descupdate')
                                            <a href="/descUpdate/{{$cashdt->id}}" class="btn btn-primary">Update</a>
                                        @endcan
                                    @else
                                        @can('cash_locker-descActive')
                                            <a href="/descInactive/{{$cashdt->id}}" class="btn btn-light">Inactive</a>
                                        @endcan
                                        @can('cash_locker-descdelete')
                                            <button class="btn btn-danger" onclick="confirmAlert({{$cashdt->id}})">
                                                Delete
                                            </button>
                                        @endcan
                                    <!--a href="/addSubDesc/{{$cashdt->id}}" class="btn btn-warning">Add Sub Description</a>
                                    <a href="/viewsubdesc/{{$cashdt->id}}" class="btn btn-danger">View</a-->
                                        @can('cash_locker-descupdate')
                                            <a href="/descUpdate/{{$cashdt->id}}" class="btn btn-warning">Update</a>
                                        @endcan
                                    @endif

                                </td>

                            </tr>
                            @endforeach

                            </tr>

                    </table>
                </div>
            </div>

        </div>

    </div>


    </body>
    </html>
@stop
@section('js')
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="{{asset('assets/examples/js/demos/form.wizard.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/validate.min.js')}}"></script>
    <script src="{{asset('assets/packages/raw_materials/raw_material.js')}}"></script>

    <script>
        function confirmAlert(id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "delete",
                        url: '/descDelete/' + id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            } else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

    </script>

@stop

