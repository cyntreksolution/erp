@extends('layouts.back.master')@section('title','Cash Book')
@section('css')

    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/modal.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.wizard.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.validation.css')}}">
    <link rel="stylesheet" href="{{asset('assets/packages/raw_materials/raw_material.css')}}">

    <style>


    </style>
@stop
{{--@section('current','Cash Book')--}}
{{--@section('current_url',route('raw_materials.create'))--}}

@section('content')
    <div class="app-wrapper">
        <div class="app-panel" id="app-panel">
            <div class="app-search"><input type="search" class="search-field" placeholder="Search">
                <i class="search-icon fa fa-search"></i></div>
            <div class="app-panel-inner">
                <div class="scroll-container">
                    <div class="p-3">
                        @can('cash_locker-create')
                        <button class="btn btn-success py-3 btn-block btn-lg" data-toggle="modal"
                                data-target="#inflow">Cash Flow
                        </button>
                        @endcan
                    </div>
                    <hr class="m-0">
                    <!--div class="p-3">
                        <button class="btn btn-danger py-3 btn-block btn-lg" data-toggle="modal"
                                data-target="#outflow">Outflow
                        </button>
                    </div-->
                    <hr class="m-0">
                    <div class="people-list d-flex justify-content-start flex-wrap p-3">
                        {{--<button class="avatar-sm avatar-circle fz-base font-weight-bold add-people-btn mx-1"--}}
                        {{--data-toggle="modal" data-target="#supplier">--}}
                        {{--<i data-toggle="tooltip"--}}
                        {{--data-placement="bottom" title="New Supplier" class="zmdi zmdi-plus"></i>--}}
                        {{--</button>--}}

                        {{--<a href="stock\status">--}}
                        {{--<button class="avatar-sm avatar-circle fz-base font-weight-bold add-people-btn mx-1">--}}
                        {{--<i data-toggle="tooltip" data-placement="bottom" title="Stock Status"--}}
                        {{--class="zmdi zmdi-view-list"></i>--}}
                        {{--</button>--}}
                        {{--</a>--}}

                    </div>
                    <hr class="m-0">
                    <div class="media-list" id="container2">
                        <a>
                            <div class="media content2">
                                @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                <div class="avatar avatar-sm bg-{{$color[5]}}">$$
                                </div>

                                <div class="media-body">
                                    <h5 class="text mb-lg-1">Current Cash in hand Rs. {{number_format($balance['balance'],2)}}
                                        <span class="section-1">
                                       @if(Auth::user()->hasRole(['Owner']) )
                                        <a href="#" class="btn btn-light btn-sm" data-toggle="modal"
                                           data-target="#cashInhand">
                                            <i class="zmdi zmdi-edit" data-toggle="tooltip"
                                               data-placement="right" title="Edit Cash In Hand"></i>
                                        </a>
                                        @endif
                                    </span>
                                    </h5>

                                </div>
                            </div>
                        </a>
                        <hr>

                        <h6 class="media-heading my-1">
                            <h6 class="app-main-heading text-center">Today Total Cash in. {{$cashIn[0]->amount}} </h6>
                        </h6>

                        <h6 class="media-heading my-1">
                            <h6 class="app-main-heading text-center">Today Total Cash Out. {{$cashOut[0]->amount}} </h6>

                        </h6>
                    </div>
                </div>
                <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show">
                    <i class="fa fa-chevron-right"></i> <i class="fa fa-chevron-left"></i></a></div>
        </div>
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">Cash Flow</h5>
            </div>

            <div class="app-main-content px-3 py-4" id="container">
                <div class="content ">
                    <div class="contacts-list">
                        <div class="media-list">
                            @foreach($transaction as $t)
                                @if($t->type==1)
                                    <div class="media">
                                        <div class="avatar avatar-sm bg-success">+
                                        </div>
                                        <div class="media-body">
                                            <h6 class="media-heading" id="media-list-item-3">{{$t->description}}</h6>
                                            <small>{{$t->created_at->format('d - M - Y @ h :i: s')}}</small>
                                        </div>
                                        <h6 class="media-heading">{{$t->amount}}</h6>
                                    </div>
                                @else
                                    @if($t->category==0)
                                        <div class="media">
                                            <div class="avatar avatar-sm bg-warning">-
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading"
                                                    id="media-list-item-3">{{$t->description}}</h6>
                                                <small>{{$t->created_at->format('d - M - Y @ h :i: s')}}</small>
                                            </div>
                                            <h6 class="media-heading">{{$t->amount}}</h6>
                                        </div>
                                    @elseif($t->category==1)
                                        <div class="media">
                                            <div class="avatar avatar-sm bg-danger">-
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading"
                                                    id="media-list-item-3">{{$t->description}}</h6>
                                                <small>{{$t->created_at->format('d - M - Y @ h :i: s')}}</small>
                                            </div>
                                            <h6 class="media-heading">{{$t->amount}}</h6>
                                        </div>
                                    @endif
                                @endif
                            @endforeach
                        </div>
                    </div>

                </div>
                {{--</div>--}}
            </div>
        </div>
    </div>


    <!--div class="modal fade" id="outflow" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'cash_locker','enctype'=>'multipart/form-data'))}}
            <input type="hidden" name="flowType" value="outflow">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                    <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="task-name-wrap">
                    <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                    <input class="task-name-field" type="number" name="amount"
                           placeholder="amount">
                </div>
                <hr>
                <div class="task-name-wrap">
                    <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                    <input class="task-name-field" type="text" name="description"
                           placeholder="Description">
                </div>

            </div>
            <div class="modal-footer">

                <input type="submit" id="finish-btn" class="finish btn btn-block btn-danger "
                       value="Get Cash"/>

            </div>
{!!Form::close()!!}
            </div>
        </div>
    </div-->

    <div class="modal fade" id="cashInhand" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'cashInHand/'.$balance->id)) }}
                {{-- Form::open(array('url' =>  'cashInHand/'.$balance->id)),'enctype'=>'multipart/form-data' ,'id'=>'bootstrap-wizard-form'))--}}

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <label class="col-8 col-form-label">Actual Cash in Hand</label>
                            <input type="hidden" id="cid" name="cid" value="{{$balance->id}}">
                            <input name="new_cash" type="text" class="task-name-field"
                                   value="{{$balance->balance}}" required>
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>


    <div class="modal fade" id="inflow" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <input type="hidden" name="flowType" value="inflow">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12">
                        <div class="card">
                            {{ Form::open(array('url' =>  'cash_locker','enctype'=>'multipart/form-data' ,'id'=>'bootstrap-wizard-form'))}}

                            <input type="hidden" id="main_desc_id" name="main_desc_id">
                            <div class="wizard p-4" id="bootstrap-wizard-1">
                                <ul class="nav nav-tabs vertices keep-prefix-suffix" role="tablist">
                                    <li class="nav-item">
                                        <a class=" nav-link active" data-toggle="tab" href="#ex5-step-1"
                                           role="tab">
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#ex5-step-2"
                                           role="tab">
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#ex5-step-3"
                                           role="tab">
                                        </a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="ex5-step-1" role="tabpanel">

                                        <div class="task-name-wrap">
                                            <select onchange="changeMainDesc()" id="maindesc_id" name="maindesc_id"
                                                    required
                                                    class="form-control select2 ">
                                                <option value="">Select Main Description</option>
                                                @foreach($cashinDescs as $cashinDesc)
                                                    <option value="{{$cashinDesc->id}}">{{$cashinDesc->main_desc}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <hr>
                                        <div class="task-name-wrap">
                                            <select name="cash_type" id="cash_type" required
                                                    class="form-control select2 ">
                                                <option value="">Cash Flow Type</option>

                                                <option value="CashIn">Cash In</option>
                                                <option value="CashOut">Cash Out</option>

                                            </select>
                                        </div>
                                        <hr>


                                    </div>
                                    <div class="tab-pane" class="row" id="ex5-step-2" role="tabpanel">

                                        <div class="task-name-wrap">
                                            <select name="category_id" required
                                                    class="form-control select2 " id="category_id">
                                                <option value="">Sub Description</option>
                                                @foreach($cashinDescs as $cashinDesc)
                                                    <option value="{{$cashinDesc->id}}">{{$cashinDesc->sub_desc}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <hr>
                                    </div>

                                    <div class="tab-pane" class="row" id="ex5-step-3" role="tabpanel">
                                        <div class="task-name-wrap" id="use_">
                                            <select name="use_type" id="use_type" required
                                                    class="form-control select2 ">
                                            </select>
                                        </div>
                                        <div class="task-name-wrap d-none" id="type_text">
                                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="type_data_text"
                                                   placeholder="Type">
                                        </div>

                                        <hr>
                                        <div class="task-name-wrap">
                                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="remark"
                                                   placeholder="Remark">
                                        </div>
                                        <hr>
                                        <div class="task-name-wrap">
                                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="number" name="amount"
                                                   placeholder="Amount">
                                        </div>
                                        <hr>
                                    </div>
                                </div>
                                <div class="pager d-flex justify-content-center">
                                    <input type="submit" id="finish-btn" class="finish btn btn-success w-50"
                                           value="Finish">

                                    <button type="button" id="next-btn" class="next btn btn-success w-50">Next
                                    </button>
                                </div>
                            </div>


                            {!!Form::close()!!}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('js')
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="{{asset('assets/examples/js/demos/form.wizard.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/validate.min.js')}}"></script>
    <script src="{{asset('assets/packages/raw_materials/raw_material.js')}}"></script>


    <script>


        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = $(e.target).attr("href") // activated tab

            if (target == '#ex5-step-2') {
                let selected = $('#maindesc_id').val();
                let type = $('#cash_type').val();
                $.ajax({
                    url: "/cash-locker/get-sub-description",
                    type: 'get',
                    data: {
                        description_id: selected,
                        type: type,
                        _token: "{{ csrf_token() }}",
                    },
                    success: function (response) {
                        $('#category_id').empty();
                        $.each(response, function (index, value) {
                            $('#category_id').append('<option value="' + index + '">' + value + '</option>');
                        })
                    }
                });
                $.ajax({
                    url: "/cash-locker/get-main-desc-id",
                    type: 'get',
                    data: {
                        description_id: selected,
                        type: type,
                        _token: "{{ csrf_token() }}",
                    },
                    success: function (response) {
                        document.getElementById("main_desc_id").value = response;
                    }
                });
            }

            if (target == '#ex5-step-3') {
                let selected = $('#maindesc_id').val();
                let type = $('#cash_type').val();
                let sub = $('#category_id').val();
                $.ajax({
                    url: "/cash-locker/get-category-data",
                    type: 'get',
                    data: {
                        description_id: selected,
                        type: type,
                        sub: sub,
                        _token: "{{ csrf_token() }}",
                    },
                    success: function (response) {
                        if (response == '1') {
                            $('#use_').addClass('d-none');
                            $('#type_text').removeClass('d-none');
                        } else {
                            $('#use_').removeClass('d-none');
                            $('#type_text').addClass('d-none');

                            $('#use_type').empty();
                            $.each(response, function (index, value) {
                                $('#use_type').append('<option value="' + index + '">' + value + '</option>');
                            })
                        }

                    }
                });
            }


        });

    </script>

@stop
