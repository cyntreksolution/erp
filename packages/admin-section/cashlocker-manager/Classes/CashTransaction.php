<?php
/**
 * Created by PhpStorm.
 * User: Isuru Sithmal
 * Date: 2/28/2018
 * Time: 11:44 AM
 */

namespace CashLockerManager\Classes;


use App\UtilityBill;
use CashLockerManager\Models\CashBookTransaction;
use CashLockerManager\Models\CashLocker;
use CashLockerManager\Models\loanlease;
use CashLockerManager\Models\main_desc_cashlocker;
use CashLockerManager\Models\VehicleEquipment;
use EmployeeManager\Models\Employee;
use SalesRepManager\Models\SalesRep;
use SupplierManager\Models\Supplier;
use VehicleManager\Models\Vehicle;

class CashTransaction
{
    public static function updateCashLocker($type, $amount)
    {
        if ($type == 2) {
            $amount = ($amount)*(-1);
        }
        $cash = CashLocker::where('id', 1)->first();
        $cash->balance = $cash->balance + $amount;
        $cash->save();
    }

    public static function updateCashTransaction($type, $amount, $ref, $category, $description, $main_desc_id = null, $category_reference_id = null ,$main_desc=null,$main_desc_category=null)
    {
        if ($type == 2) {
            $amount = -$amount;
        }
        $category_reference = null;

        if ($main_desc_id > 0 ) {
            $item = main_desc_cashlocker::find($main_desc_id);
            switch ($item->category) {
                case 'Agents';
                    $data = 'SalesRepManager\Models\SalesRep';
                    break;
                case 'Vehicle';
                    $data = 'VehicleManager\Models\Vehicle';
                    break;
                case 'Employees';
                    $data = 'EmployeeManager\Models\Employee';
                    break;
                case 'Equipments';
                    $data = 'CashLockerManager\Models\VehicleEquipment';
                    break;
                case 'LoanLease';
                    $data = 'CashLockerManager\Models\loanlease';
                    break;
                case 'Supliers';
                    $data = 'SupplierManager\Models\Supplier';
                    break;
                case 'UtilityBills';
                    $data = 'App\UtilityBill';
                    break;
                default:
                    $data = 'Other';
            }
        } elseif ($main_desc_id == -1) {
            //agent payment
            $data = 'SalesRepManager\Models\SalesRep';
        }elseif ($main_desc_id == -3) {
            //agent payment delete Admin
            $data = 'SalesRepManager\Models\SalesRep';
        }elseif ($main_desc_id == -2) {
            //supplier payment
            $data = 'SupplierManager\Models\Supplier';
        }


        if(!empty($data)){

            $category_reference = $data;
        }else {
            $category_reference = 'Admin';
        }

        $cash = CashLocker::where('id', 1)->first();


        $ct = new CashBookTransaction();
        $ct->type = $type;
        $ct->amount = $amount;
        $ct->ref_no = $ref;
        $ct->category = $category;
        $ct->description = $description;
        $ct->main_desc_id = $main_desc_id;
        $ct->main_desc = $main_desc;
        $ct->category_reference_id = $category_reference_id;
        $ct->category_reference = $category_reference;
        //$ct->balance = $cash->balance + $amount;
        $ct->balance = $cash->balance;
        $ct->main_desc_category = $main_desc_category;
        $ct->save();

        return $ct;
    }
}
