<?php

namespace CashLockerManager\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class main_desc_cashlocker extends Model
{
    use SoftDeletes;
}
