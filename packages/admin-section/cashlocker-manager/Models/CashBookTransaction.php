<?php

namespace CashLockerManager\Models;

use App\AgentBuffer;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CashBookTransaction extends Model
{
    use SoftDeletes;

    protected $table = 'cash_transaction';

    protected $primaryKey = 'id';

    protected $dates = ['deleted_at'];

    public function mainDescCategory(){
        return $this->belongsTo(main_desc_cashlocker::class,'main_desc_id');
    }
    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }

    public function scopeSearchData($query, $term)
    {
        return $query
            ->Where('id', 'like', "%" . $term . "%")
            ->orWhere('serial', 'like', "%" . $term . "%")
            ->orWhere('status', 'like', "%" . $term . "%")
            ->orWhere('burden_size', 'like', "%" . $term . "%")
            ->orWhere('created_at', 'like', "%" . $term . "%")
            ->orWhereHas('semiProdcuts', function ($query) use ($term) {
                $query->where('name', 'like', "%" . $term . "%");
            })
            ->orWhereHas('recipe', function ($query) use ($term) {
                $query->where('name', 'like', "%" . $term . "%");
            });
    }


    public function scopeFilterData($query, $date_range,$main_desc,$sub_desc,$paytype,$category,$desc1)
    {
        if (!empty($date_range)) {
            $dates = explode(' - ', $date_range);
            $start = $dates[0];
            $end = $dates[1];
            $query->where('created_at', '>=', Carbon::parse($start)->format('Y-m-d'))
                ->where('created_at', '<=', Carbon::parse($end)->addDay()->format('Y-m-d'));
        }


        if (!empty($main_desc)) {
            $query->where('main_desc','like',"$main_desc%");
        }
        if (!empty($sub_desc)) {
        //    $query->where('category_reference_id','=',$sub_desc);
            $query->where('main_desc_id','=',$sub_desc);
        }
        if (!empty($paytype)) {
            $paytype = $paytype=="CashIn"?1:2;

            $query->where('type','=',$paytype);
        }

        if (!empty($category)) {
            $query->where('main_desc_category','like',"$category");
//            $query->whereHas('mainDescCategory', function ($q)  use ($category){
//                $q->where('category','like',"%$category%");
//            });
        }
        if (!empty($desc1)) {
            //    $query->where('category_reference_id','=',$sub_desc);
            $query->where('category_reference_id','=',$desc1);
        }

    }


}
