<?php

namespace CashLockerManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CashLocker extends Model
{
    use SoftDeletes;

    protected $table = 'cash_locker';

    protected $primaryKey = 'id';

    protected $dates = ['deleted_at'];

}
