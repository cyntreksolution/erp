<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web'])->group(function () {
    Route::prefix('cash_locker')->group(function () {
        Route::get('', 'CashLockerManager\Controllers\CashLockerController@index')->name('cash_locker.index');

        Route::get('create', 'CashLockerManager\Controllers\CashLockerController@create')->name('cash_locker.create');
       // Route::post('createSub','CashLockerManager\Controllers\CashLockerController@createSub')->name('cash_locker.createSub');

        Route::get('{cash_locker}', 'CashLockerManager\Controllers\CashLockerController@show')->name('cash_locker.show');

        Route::get('{cash_locker}/edit', 'CashLockerManager\Controllers\CashLockerController@edit')->name('cash_locker.edit');


        Route::post('', 'CashLockerManager\Controllers\CashLockerController@store')->name('cash_locker.store');

        Route::post('{cash_locker}', 'CashLockerManager\Controllers\CashLockerController@update')->name('cash_locker.update');

        Route::delete('{cash_locker}', 'CashLockerManager\Controllers\CashLockerController@destroy')->name('cash_locker.destroy');

    });
});
Route::get('/desc','CashLockerManager\Controllers\CashLockerController@descshow')->name('cash_locker.mdesc');

Route::get('/descActive/{id}','CashLockerManager\Controllers\CashLockerController@descactive');
Route::get('/descInactive/{id}','CashLockerManager\Controllers\CashLockerController@descinactive');
Route::delete('/descDelete/{id}','CashLockerManager\Controllers\CashLockerController@descdelete');
Route::get('/descUpdate/{id}','CashLockerManager\Controllers\CashLockerController@descupdateview');

Route::get('/loan','CashLockerManager\Controllers\CashLockerController@loanview')->name('cash_locker.loan');
Route::get('/equip','CashLockerManager\Controllers\CashLockerController@equipview')->name('cash_locker.equip');


Route::post('/saveDesc','CashLockerManager\Controllers\CashLockerController@cashIn');

Route::post('/descUpdatedata','CashLockerManager\Controllers\CashLockerController@descupdate');


Route::post('/equip','CashLockerManager\Controllers\CashLockerController@equip');
Route::get('cash-locker/get-sub-description','CashLockerManager\Controllers\CashLockerController@getSubDescription');
Route::get('cash-locker/get-category-data','CashLockerManager\Controllers\CashLockerController@getCategoryData');
Route::get('cash-locker/get-main-desc-id','CashLockerManager\Controllers\CashLockerController@getSubDescriptionID');
Route::post('cashInHand/{id}','CashLockerManager\Controllers\CashLockerController@editCashInHand');
