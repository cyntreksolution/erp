<?php

namespace OwnerManager\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use OwnerManager\Models\Owner;
use RecipeManager\Models\Recipe;
use SemiFinishProductManager\Models\SemiFinishProduct;
use function Sodium\add;

class OwnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect(route('owner.create'));
        return view('OwnerManager::owner.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('OwnerManager::owner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return redirect(route('owner.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \OwnerManager\Models\Owner $owner
     * @return \Illuminate\Http\Response
     */
    public function show(Owner $owner)
    {
        return view('OwnerManager::owner.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \OwnerManager\Models\Owner $owner
     * @return \Illuminate\Http\Response
     */
    public function edit(Owner $owner)
    {
        return view('OwnerManager::owner.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \OwnerManager\Models\Owner $owner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Owner $owner)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \OwnerManager\Models\Owner $owner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Owner $owner)
    {
        $owner->delete();
        if ($owner->trashed()) {
            return true;
        }else{
            return false;
        }
    }
}
