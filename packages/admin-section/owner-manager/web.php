<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web','log'])->group(function () {
    Route::prefix('owner')->group(function () {
        Route::get('', 'OwnerManager\Controllers\OwnerController@index')->name('owner.index');

        Route::get('create', 'OwnerManager\Controllers\OwnerController@create')->name('owner.create');

        Route::get('{owner}', 'OwnerManager\Controllers\OwnerController@show')->name('owner.show');

        Route::get('{owner}/edit', 'OwnerManager\Controllers\OwnerController@edit')->name('owner.edit');


        Route::post('', 'OwnerManager\Controllers\OwnerController@store')->name('owner.store');

        Route::put('{owner}', 'OwnerManager\Controllers\OwnerController@update')->name('owner.update');

        Route::delete('{owner}', 'OwnerManager\Controllers\OwnerController@destroy')->name('owner.destroy');
    });
});
