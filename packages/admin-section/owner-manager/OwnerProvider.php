<?php

namespace OwnerManager;

use Illuminate\Support\ServiceProvider;

class OwnerProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/Views', 'OwnerManager');
        require __DIR__ . '/web.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('OwnerManager', function($app){
            return new OwnerManager;
        });
    }
}
