@extends('layouts.back.master')@section('title','Sales Rep')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/modal.css')}}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}">


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .app-wrapper {
            font-family: Raleway, sans-serif;
            width: 100%;
            border: 1px solid #eee;
        }

        .hide {
            display: none;
        }

        /* .flipswitch {
        position: relative;
        background: blue;
        width: 60px;
        height: 38px;
        -webkit-appearance: initial;
        border-radius: 100px;
        -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
        outline: none;
        font-size: 14px;
        font-family: Trebuchet, Arial, sans-serif;
        font-weight: bold;
        cursor: pointer;
        border: 1px solid #ddd;
      }

      .flipswitch:after {
        position: absolute;
        top: 5%;
        display: block;
        line-height: 32px;
        width: 50%;
        height: 90%;
        background: #fff;
        box-sizing: border-box;
        text-align: center;
        transition: all 0.3s ease-in 0s;
        color: black;
        border: #888 1px solid;
        border-radius: 100px;
      }

      .flipswitch:after {
        left: 2%;
        font-size: 10px;
        content: "NO";
      }

      .flipswitch:checked:after {
        left: 53%;
        background: white;
        content: "YES";
      } */
    </style>
@stop
@section('content')
    <div class="app-wrapper">
        <div class="profile-section-user" id="app-panel">

            <div class="profile-cover-img"><img
                        src="{{asset('assets\img\supp.jpg')}}" alt="">
            </div>
            <div class="profile-info-brief p-3">
                @if(isset($rep->image))
                    <div class="user-profile-avatar avatar-circle avatar avatar-sm">
                        <img src="{{asset($rep->image_path.$rep->image)}}" alt="">
                    </div>
                @else
                    @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                    <div class="user-profile-avatar avatar avatar-circle avatar-md project-icon bg-{{$color[1]}}"
                         data-plugin="firstLitter"
                         data-target="#{{$rep->id*754}}"></div>
                @endif
                <div class="text-center"><h5 class="text-uppercase mb-1"
                                             id="{{$rep->id*754}}">{{$rep->first_name.' '.$rep->last_name}}</h5>
                    <div class="hidden-sm-down ">
                        <div>{{$rep->mobile}}</div>
                        <div>NO #{{$rep->id}}</div>
                        {{--<button class="btn btn-outline-primary btn-sm m-2 btn-rounded " disabled>--}}
                        {{--<i class="fa fa-pause px-1" aria-hidden="true"></i>--}}
                        {{--PENDING--}}
                        {{--</button>--}}
                    </div>

                </div>
            </div>
            <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show"><i
                        class="fa fa-chevron-right"></i>
                <i class="fa fa-chevron-left"></i>
            </a>
            <div class="container">
                <div class="form-group">
                    <label class="col-4 col-form-label">Category </label>
                    <div class="col-12">
                        {!! Form::select('category',$categories , null , ['class' => 'form-control','placeholder'=>'Select Category','id'=>'category']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-8 col-form-label">Discount Mode </label>
                    <div class="col-12">
                        <div class="radio radio-inline radio-primary mr-3">
                            <input type="radio" name="discountMode" value="1" id="discountMode"><label>Yes</label>
                        </div>
                        <div class="radio radio-inline radio-primary">
                            <input type="radio" name="discountMode" value="2" checked
                                   id="discountMode"><label>No</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-8 col-form-label">Return Mode </label>
                    <div class="col-12">
                        <select class="form-control" name="returnMode" id="returnMode">
                            <option value="null">Select Return Mode</option>
                            <option value="1">Reuse Only</option>
                            <option value="2">All</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-8 col-form-label">Loading Mode </label>
                    <div class="col-12">
                        <select class="form-control" name="loadingMode" id="loadingMode">
                            <option value="null">Select Loading Mode</option>
                            <option value="1">Priority</option>
                            <option value="2">None</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-6 col-form-label">Status </label>
                    <input type="checkbox" id="status" class="one" data-plugin="switchery" data-size="medium" value=""
                           checked>
                </div>
                <div class="form-group">
                    <input type="hidden" value="{{$rep->id}}" id="refId">
                    <button class="btn btn-outline-primary btn-block mt-3" onclick="insertAgentCategory()">Enter
                    </button>
                </div>

            </div>
        </div>
        <div class="app-main">
            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">
                    <div class="container">
                        {!! Form::open(['route' => ['sales_rep.update', $rep->id], 'method' => 'post']) !!}
                        <div class="p-5">
                            <div class="form-group row">
                                <label class="col-4 col-form-label">Type:</label>
                                <div class="col-8">
                                    {!! Form::select('type', ['Mr'=>'Mr','Miss'=>'Miss','Mrs'=>'Mrs'] , $rep->type , ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-4 col-form-label">First Name *:</label>
                                <div class="col-8">
                                    <input type="text" name="first_name" value="{{$rep->first_name}}"
                                           class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-4 col-form-label">Middle Name *:</label>
                                <div class="col-8">
                                    <input type="text" name="second_name" value="{{$rep->second_name}}"
                                           class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-4 col-form-label">Last Name *:</label>
                                <div class="col-8">
                                    <input type="text" name="last_name" value="{{$rep->last_name}}" class="form-control"
                                           required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-4 col-form-label">Name with initials *:</label>
                                <div class="col-8">
                                    <input type="text" name="name_with_initials" value="{{$rep->name_with_initials}}"
                                           class="form-control" required>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-4 col-form-label">Gender *:</label>
                                <div class="col-8">
                                    <div class="radio radio-inline radio-primary mr-3">
                                        <input type="radio" name="gender"
                                               value="male" {{($rep->gender=='male'?'checked':null)}} ><label>Male</label>
                                    </div>
                                    <div class="radio radio-inline radio-primary">
                                        <input type="radio" name="gender"
                                               value="female" {{($rep->gender=='female'?'checked':null)}}><label>Female</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-4 col-form-label">Address:</label>
                                <div class="col-8">
                                    <textarea class="form-control" name="address"
                                              placeholder=""> {{$rep->address}}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-4 col-form-label">Telephone 1 *:</label>
                                <div class="col-8">
                                    <input type="text" name="telephone1" value="{{$rep->telephone1}}"
                                           class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-4 col-form-label">Telephone 2 :</label>
                                <div class="col-8">
                                    <input type="text" name="telephone2" value="{{$rep->telephone2}}"
                                           class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-4 col-form-label">NIC :</label>
                                <div class="col-8">
                                    <input type="text" name="nic" value="{{$rep->nic}}" class="form-control">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-4 col-form-label">First Appoint Date :</label>
                                <div class="col-8">
                                    <input type="date" name="appoint_date" value="{{$rep->appoint_date}}"
                                           class="form-control">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-4 col-form-label">Territory :</label>
                                <div class="col-8">
                                    <input type="text" name="territory" value="{{$rep->territory}}"
                                           class="form-control">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-4 col-form-label">User Name :</label>
                                <div class="col-8">
                                    <input type="text" name="email" value="{{$rep->email}}" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-4 col-form-label">Currant Outstanding :</label>
                                <div class="col-6">
                                    <input type="number" step="0.01" name="balance" value="{{$rep->balance}}"
                                           class="form-control">

                                </div>
                                <div class="col-md-2">
                                    <a href="#" class="btn btn-light btn-sm" data-toggle="modal"
                                       data-target="#EditBalance{{$rep->id}}">
                                        <i class="zmdi zmdi-edit"></i>
                                    </a>

                                <!--button class="btn btn-success btn-block btn-sm" data-toggle="modal"
                                        data-target="#EditBalance">Edit
                                </button-->
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-4 col-form-label">Initial Balance:</label>
                                <div class="col-8">
                                    @php $init = $rep->initInquiry->first(); @endphp
                                    <input type="number" step="0.01" name="init_balance" value="{{!empty($init)?$init->end_balance:0}}"
                                           class="form-control">
                                </div>
                            </div>

                            <br>
                            <a href="{{route('crates.adjust',[$rep->id])}}" class="btn btn-warning btn-block mt-1">Crates Adjustment</a><button class="btn btn-primary btn-block mt-1">Update</button>
                        </div>


                        {{ Form::close() }}
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="app-wrapper">
        <div class="app-main">
            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">
                    <hr>
                    <div class="container" id="all_Details">

                    </div>
                    <div class="container" id="all_Models">
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="EditBalance{{$rep->id}}" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {{ Form::open(array('route' => 'sales_rep.editBal','enctype'=>'multipart/form-data' ,'id'=>'bootstrap-wizard-form'))}}
                <input type="hidden" value="{{$rep->id}}" name="rep_id">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">


                    <div class="task-name-wrap">
                        <label class="col-4 col-form-label">Adjust Amount</label>
                        <input class="task-name-field" type="number" name="Adjust" value="0" step="0.01" required>

                    </div>
                    <hr>
                    <div class="task-name-wrap">

                        <input class="task-name-field" type="text" name="desc"  placeholder="Description Here[Reverse(-) or Add(+)]" required>

                    </div>
                    <hr>





                <div class="pager d-flex justify-content-center">
                    <input type="submit" id="finish-btn" class="finish btn btn-warning btn-block"
                           value="Finish">
                </div>



                {!!Form::close()!!}
            </div>
        </div>
    </div>

@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            let agentId = $("#refId").val();
            $.ajax({
                url: "/sales_rep/category/data/" + agentId,
                type: "GET",
                success: function (data) {
                    // console.log(data);
                    appendRawList(data);
                },
                error: function (xhr, status) {

                }
            });
        });


        function appendRawList(rawList) {
            $('#all_Details').empty();
            let models = '';
            let tableData = '';
            // $('#all_Details').append();
            tableData = ' <table id="example" class="display text-center" style="width:100%">\n' +
                '                                            <thead>\n' +
                '                                            <tr>\n' +
                '                                                <th class="text-center"><b>Category</b></th>\n' +
                '                                                <th class="text-center"><b>Discount Mode</b></th>\n' +
                '                                                <th class="text-center"><b>Return Mode</b></th>\n' +
                '                                                <th class="text-center"><b>Loading Mode</b></th>\n' +
                '                                                <th class="text-center"><b>Status</b></th>\n' +
                '                                                <th class="text-center"><b>Action</b></th>\n' +
                '                                            </tr>\n' +
                '                                            </thead>\n' +
                '                                        <tbody>';
            for (var i = 0; i < rawList.length; i++) {
                let active = '<i class="fa fa-square" aria-hidden="true"></i>';
                if (rawList[i]['status'] == 'Active') {
                    active = '<i class="fa fa-check-square" aria-hidden="true"></i>';
                }
                tableData = tableData + '                                            <tr>\n' +
                    '                                                <td>' + rawList[i]['category_id'] + '</td>\n' +
                    '                                                <td>' + rawList[i]['discount_mode'] + '</td>\n' +
                    '                                                <td>' + rawList[i]['return_mode'] + '</td>\n' +
                    '                                                <td>' + rawList[i]['loading_mode'] + '</td>\n' +
                    '                                                <td>' +
                    active
                    + '</td>\n' +
                    '                                                <td><a href="" class="btn btn-light btn-md" data-target="#EditReusableType-' + rawList[i]['sales_ref_category_id'] + '" data-toggle="modal"><i class="zmdi zmdi-edit"></i></a></td>\n' +
                    '                                            </tr>';
                models = models + '<div class="modal fade" id="EditReusableType-' + rawList[i]['sales_ref_category_id'] + '" tabindex="-1" role="dialog" aria-divledby="myModaldiv" aria-hidden="true">\n' +
                    '                <div class="modal-dialog" style="margin-top: 200px" role="document">\n' +
                    '                        <div class="modal-content">\n' +
                    '                        <div class="modal-header">\n' +
                    '                                        <button type="button" class="close" data-dismiss="modal" aria-div="Close">\n' +
                    '                                                            <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>\n' +
                    '                                                        </button>\n' +
                    '                                                    </div>\n' +
                    '                                                    <div class="modal-body">\n' +
                    '                                                        <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">\n' +
                    '                                                            <div class="row task-name-wrap">\n' +
                    '                                                                <div class="col-lg-12">\n' +
                    '                                                                <label>Category</label>\n' +
                    '                                                                    <select class="form-control" id="reusableType-' + rawList[i]['sales_ref_category_id'] + '">';
                for (var x = 0; x < rawList[i]['categories'].length; x++) {
                    models = models + '<option value="' + rawList[i]['categories'][x]['id'] + '"';
                    if (rawList[i]['category_id'] == rawList[i]['categories'][x]['name']) {
                        models = models + ' selected="selected"';
                    }
                    models = models + '>' + rawList[i]['categories'][x]['name'] + '</option>';
                }
                models = models + '                                                                    </select>\n' +
                    '                                        <label class="col-8 col-form-label">Discount Mode </label>\n' +
                    '                                            <select class="form-control" name="returnMode" id="discountMode-' + rawList[i]['sales_ref_category_id'] + '">\n' +
                    '                                              <option value="1"';
                if (rawList[i]['discount_mode'] == 'Yes') {
                    models = models + ' selected="selected"';
                }
                models = models + '>Yes</option>\n' +
                    '                                              <option value="2"';
                if (rawList[i]['discount_mode'] == 'No') {
                    models = models + ' selected="selected"';
                }
                models = models + '>No</option>\n' +
                    '                                            </select>\n' +
                    '                                        <label class="col-8 col-form-label">Return Mode </label>\n' +
                    '                                            <select class="form-control" name="returnMode" id="returnMode-' + rawList[i]['sales_ref_category_id'] + '">\n' +
                    '                                              <option value="1"';
                if (rawList[i]['return_mode'] == 'Reuse Only') {
                    models = models + ' selected="selected"';
                }
                models = models + '>Reuse Only</option>\n' +
                    '                                              <option value="2"';
                if (rawList[i]['return_mode'] == 'All') {
                    models = models + ' selected="selected"';
                }
                models = models + '>All</option>\n' +
                    '                                            </select>\n' +

                    '                                            <label class="col-8 col-form-label">Loading Mode </label>\n' +
                    '                                                <select class="form-control" name="loadingMode" id="loadingMode-' + rawList[i]['sales_ref_category_id'] + '">\n' +
                    '                                                  <option value="1"';
                if (rawList[i]['loading_mode'] == 'Priority') {
                    models = models + ' selected="selected"';
                }
                models = models + '>Priority</option>\n' +
                    '                                                  <option value="2"';
                if (rawList[i]['loading_mode'] == 'None') {
                    models = models + ' selected="selected"';
                }
                models = models + '>None</option>\n' +
                    '                                                </select><br>\n' +
                    '                                                <label class="col-6 col-form-label">Status </label>\n' +
                    '                                                <input type="checkbox" id="status-' + rawList[i]['sales_ref_category_id'] + '" class="one" data-plugin="switchery" data-size="medium" value="" '
                if (rawList[i]['status'] == 'Active') {
                    models = models + ' checked';
                }
                models = models + '                                         ></div>\n' +
                    '                                                            </div>\n' +

                    '                                                      </div>\n' +
                    '                                                    </div>\n' +

                    '                                                    <div class="modal-footer">\n' +
                    '                                                        <input type="submit" class="btn btn-success btn-block" value="UPDATE" onclick="updateFunction(' + rawList[i]['sales_ref_category_id'] + ')">\n' +
                    '                                                    </div>\n' +
                    '                                                </div>\n' +
                    '                                                </div>\n' +
                    '                                            </div>';
            }
            tableData = tableData + ' </tbody>\n' +
                '                                        </table>';
            $('#all_Details').append(tableData);
            $('#all_Models').append(models);
        }

        function updateFunction(value) {
            let agentId = $("#refId").val();
            let categoryName = 'reusableType-' + value;
            let discountModeName = 'discountMode-' + value;
            let returnModeName = 'returnMode-' + value;
            let loadingModeName = 'loadingMode-' + value;
            let statusName = 'status-' + value;
            let category = document.getElementById(categoryName).value;
            let discountMode = document.getElementById(discountModeName).value;
            let returnMode = document.getElementById(returnModeName).value;
            let loadingMode = document.getElementById(loadingModeName).value;
            let status = document.getElementById(statusName).checked;
            // alert(category+' | '+discountMode+' | '+returnMode+' | '+loadingMode+' | '+status);
            if (category == '') {
                noticeMessage("Check Category!!!", "Category is Required", "warning", "no", agentId);
            } else if (returnMode == 'null') {
                noticeMessage("Check Return Mode!!!", "Return Mode is Required", "warning", "no", agentId);
            } else if (loadingMode == 'null') {
                noticeMessage("Check Loading Mode!!!", "Loading Mode is Required", "warning", "no", agentId);
            } else {
                $.ajax({
                    url: "/sales_rep/category/update/" + value,
                    type: "POST",
                    data: {
                        agent_id: agentId,
                        category: category,
                        discountMode: discountMode,
                        returnMode: returnMode,
                        loadingMode: loadingMode,
                        status: status,
                        _token: '{{csrf_token()}}'
                    },
                    success: function (data) {
                        if (data == 'true') {
                            noticeMessage("Update Success!!!", "Agent Category Update successfully", "success", "yes", agentId);
                        } else {
                            noticeMessage("Category Exists!!!", "Agent Category is already exists!", "warning", "no", agentId);
                        }
                    }
                });
            }
        }

        function insertAgentCategory() {
            let agentId = $("#refId").val();
            let category = $("#category option:selected").val();
            let discountMode = document.getElementById("discountMode").checked;
            let returnMode = $("#returnMode option:selected").val();
            let loadingMode = $("#loadingMode option:selected").val();
            let status = document.getElementById("status").checked;
            if (discountMode == true) {
                discountMode = 1;
            } else {
                discountMode = 2;
            }
            if (category == '') {
                noticeMessage("Check Category!!!", "Category is Required", "warning", "no", agentId);
            } else if (returnMode == 'null') {
                noticeMessage("Check Return Mode!!!", "Return Mode is Required", "warning", "no", agentId);
            } else if (loadingMode == 'null') {
                noticeMessage("Check Loading Mode!!!", "Loading Mode is Required", "warning", "no", agentId);
            } else {
                $.ajax({
                    url: "/sales_rep/category/" + agentId,
                    type: "POST",
                    data: {
                        category: category,
                        discountMode: discountMode,
                        returnMode: returnMode,
                        loadingMode: loadingMode,
                        status: status,
                        _token: '{{csrf_token()}}'
                    },
                    success: function (data) {
                        if (data == 'true') {
                            noticeMessage("Success!!!", "Agent Category created successfully", "success", "yes", agentId);
                        } else {
                            noticeMessage("Already Exists!!!", "Agent Category is alrealy exists!", "warning", "no", agentId);
                        }
                    }
                });
            }

        }


        function noticeMessage(title, text, type, url, agentId) {
            if (url == 'yes') {
                swal({
                        title: title,
                        text: text,
                        type: type,
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "Confirm",
                        closeOnConfirm: false
                    }, function () {
                        location.reload();
                    }
                );
            } else {
                swal({
                    title: title,
                    text: text,
                    type: type,
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Confirm",
                    closeOnConfirm: false
                });
            }
        }

    </script>
@stop
