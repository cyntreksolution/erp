@extends('layouts.back.master')@section('title','Crate Adjustment')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}">


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .app-wrapper {
            font-family: Raleway, sans-serif;
            width: 100%;
            border: 1px solid #eee;
        }
    </style>
@stop
@section('content')
    <div class="app-wrapper">
        {{--        <div class="profile-section-user" id="app-panel">--}}
        {{--@foreach($po as $p)--}}
        {{--<div class="profile-cover-img"><img src="{{asset($p['supplier']->image_path.$p['supplier']->image)}}"--}}
        {{--alt="">--}}
        {{--</div>--}}
        {{--<div class="profile-info-brief p-3"><img class="img-fluid user-profile-avatar"--}}
        {{--src="{{asset($p['supplier']->image_path.$p['supplier']->image)}}"--}}
        {{--alt="">--}}
        {{--<div class="text-center"><h5 class="text-uppercase mb-1">{{$p['supplier']->supplier_name}}</h5>--}}
        {{--<div class="hidden-sm-down ">--}}
        {{--<div>{{$p['supplier']->mobile}}</div>--}}
        {{--<div>NO #{{$p->id}}</div>--}}
        {{--<div>{{$p->created_at}}</div>--}}
        {{--<button class="btn btn-outline-primary btn-sm m-2 btn-rounded " disabled>--}}
        {{--<i class="fa fa-pause px-1" aria-hidden="true"></i>--}}
        {{--PENDING--}}
        {{--</button>--}}
        {{--</div>--}}
        {{--<hr class="m-0 py-2">--}}
        {{--<div class="d-flex justify-content-center flex-wrap p-2">--}}
        {{--<a href="{{$p->id}}/edit" class="btn btn-block btn-success btn-sm m-2 text-white"> RECIEVE</a>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show"><i--}}
        {{--class="fa fa-chevron-right"></i>--}}
        {{--<i class="fa fa-chevron-left"></i>--}}
        {{--</a>--}}
        {{--@endforeach--}}
        {{--        </div>--}}
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center"> Agent Crate Adjustment </h5>
            </div>
            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">
                    <div class="container">


                        {!! Form::open(['method' => 'POST','route' => ['cratesAdjust.store']]) !!}
                        <input type="hidden" name="AgentId"value="{{$sales_reps}}">

                        <div class="col-md-12">
                            @foreach($crates as $key => $crate)

                                @php
//dd($crate->raw_material_id);
                                    $maxid =\App\CrateInquiryAgent::where('crate_id','=',$crate->raw_material_id)
                                                                ->where('agent_id','=',$sales_reps)
                                                                ->orderByDesc('id')
                                                                ->first();

                                    $cbal = \App\CrateInquiryAgent::where('id','=',$maxid->id)
                                                                ->first();


                                   // dd($cbal);
                                @endphp

                                <div class="card-body d-flex  align-items-center p-0 row">
                                    <div class="col-md-4">
                                        <div class="text-primary">
                                            <?php $createName = \RawMaterialManager\Models\RawMaterial::withoutGlobalScopes()->whereRawMaterialId($crate->raw_material_id)->get(); ?>
                                            <h5 class="mt-3">{{$createName[0]->name}}</h5>
                                        </div>
                                    </div>

                                    <div class="col-md-3 text-center">
                                        <div class="text-primary">
                                            <input type="hidden" name="update_return_crate[]" value="{{$crate->raw_material_id}}">
                                            <input type="text" name="update_return_crate_qty[]" class="form-control"
                                                   value="0" required>
                                        </div>
                                    </div>

                                    <div class="col-md-3 text-center">
                                        <div class="text-primary">
                                            <h6>Balance {{$createName[0]->name}} = {{$cbal->end_balance}} </h6>
                                        </div>

                                    </div>
                                <!-- <div class="col-md-3 text-center">
                                            <div class="text-primary">
                                                <input type="hidden" name="return_approve_crate[]" value="{{$crate->crate_id}}">
                                                <input type="number" name="return_approve_qty[]" class="form-control" value="{{$crate->qty}}">
                                            </div>
                                        </div> -->
                                </div>
                            @endforeach
                        </div>


                        <div id="appendDiv"></div>
                        <div class="mt-5 mb-5">
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" id="btnSave" name="submit"
                                            class="btn btn-success btn-lg btn-block"
                                            value="stream"> Adjust Crates
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>

            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>



@stop
