<?php

namespace SalesRepManager;

use Illuminate\Support\ServiceProvider;

class SalesRepProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/Views', 'SalesRepManager');
        require __DIR__ . '/web.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('SalesRepManager', function($app){
            return new SalesRepManager;
        });
    }
}
