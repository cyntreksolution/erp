<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web','log'])->group(function () {
    Route::prefix('sales_rep')->group(function () {
        Route::get('', 'SalesRepManager\Controllers\SalesRepController@index')->name('sales_rep.index');

        Route::get('create', 'SalesRepManager\Controllers\SalesRepController@create')->name('sales_rep.create');

        Route::get('{sales_rep}', 'SalesRepManager\Controllers\SalesRepController@show')->name('sales_rep.show');

        Route::get('{rep}/crate/adjustment', 'SalesRepManager\Controllers\SalesRepController@adjcrates')->name('crates.adjust');

        Route::get('{rep}/edit', 'SalesRepManager\Controllers\SalesRepController@edit')->name('sales_rep.edit');

        Route::get('{sales_rep}/remove', 'SalesRepManager\Controllers\SalesRepController@remove')->name('sales_rep.remove');

        Route::get('{sales_rep}/removerts', 'SalesRepManager\Controllers\SalesRepController@removerts')->name('sales_rep.removerts');

        Route::post('/crates/adjustment', 'SalesRepManager\Controllers\SalesRepController@AdjcrateStore')->name('cratesAdjust.store');

        Route::post('', 'SalesRepManager\Controllers\SalesRepController@store')->name('sales_rep.store');

        Route::post('edit-balance', 'SalesRepManager\Controllers\SalesRepController@editbal')->name('sales_rep.editBal');

        Route::post('{rep}/update', 'SalesRepManager\Controllers\SalesRepController@update')->name('sales_rep.update');

        Route::post('{sales_rep}/change', 'SalesRepManager\Controllers\SalesRepController@change')->name('sales_rep.change');

        Route::delete('{sales_rep}', 'SalesRepManager\Controllers\SalesRepController@destroy')->name('sales_rep.destroy');

        Route::post('category/{sales_rep}', 'SalesRepManager\Controllers\SalesRepController@categoryCreate')->name('sales_rep.categoryCreate');

        Route::get('category/data/{sales_rep}', 'SalesRepManager\Controllers\SalesRepController@categoryTableData')->name('sales_rep.category');

        Route::post('category/update/{sales_category}', 'SalesRepManager\Controllers\SalesRepController@categoryUpdate')->name('sales_rep.categoryUpdate');

        Route::post('category/delete/{sales_category}', 'SalesRepManager\Controllers\SalesRepController@categoryDelete')->name('sales_rep.categoryDelete');

//        Route::get('{sales_rep/remove}', 'SalesRepManager\Controllers\SalesRepController@remove')->name('sales_rep.remove');
    });
});
