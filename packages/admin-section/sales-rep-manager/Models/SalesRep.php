<?php

namespace SalesRepManager\Models;

use App\AgentInquiry;
use App\Role;
use EndProductManager\Models\Category;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use App\User;
use Spatie\Permission\Traits\HasRoles;

class SalesRep extends User
{
    use SoftDeletes;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('ref', function (\Illuminate\Database\Eloquent\Builder $builder) {
            $builder->whereHas('customRoleRelation', function ($q) {
                $q->where('name', '=', 'Sales Agent');
            });
        });
    }

    protected $table = 'users';

    protected $primaryKey = 'id';

    protected $dates = ['deleted_at'];

    public function vehicle()
    {
        return $this->hasMany('SalesRepManager\Models\SalesVehicle', 'agent_id', 'id');
    }

    public function route()
    {
        return $this->hasMany('SalesRepManager\Models\SalesRoute', 'agent_id', 'id');
    }

    public function inquiries(){
        return $this->hasMany(AgentInquiry::class,'agent_id');
    }

    public function initInquiry(){
        return $this->hasMany(AgentInquiry::class,'agent_id')->whereIsInit(1);
    }

    public function categories(){
        return $this->belongsToMany(Category::class,'agent_category','sales_ref_id');
    }
}
