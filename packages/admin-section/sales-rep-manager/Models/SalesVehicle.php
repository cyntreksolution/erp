<?php

namespace SalesRepManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalesVehicle extends Model
{
    use SoftDeletes;

    protected $table = 'sales_vehicle';

    protected $primaryKey = 'id';

    protected $dates = ['deleted_at'];

    public function getVehicle(){
        return $this->belongsTo('VehicleManager\Models\Vehicle','vehicle_id','id');
    }
}
