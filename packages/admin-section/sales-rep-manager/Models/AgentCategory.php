<?php

namespace SalesRepManager\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use UserManager\Models\Users;

class AgentCategory extends Users
{
    use SoftDeletes;

    protected $table = 'agent_category';

    protected $primaryKey = 'id';

    protected $dates = ['deleted_at'];

    
}
