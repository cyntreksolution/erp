<?php

namespace SalesRepManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalesRoute extends Model
{
    use SoftDeletes;

    protected $table = 'sales_route';

    protected $primaryKey = 'id';

    protected $dates = ['deleted_at'];

    public function getRoute(){
        return $this->belongsTo('RouteManager\Models\Route','route_id','id');
    }

}
