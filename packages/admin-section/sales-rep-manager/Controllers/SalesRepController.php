<?php

namespace SalesRepManager\Controllers;

use App\CratesTransaction;
use App\SalesAgentAPI;
use App\Traits\AgentLog;
use App\Traits\CratesAgentLog;
use App\Traits\RawMaterialLog;
use AssistantManager\Models\Assistant;
use Illuminate\Support\Facades\Log;
use RawMaterialManager\Models\RawMaterial;
use RouteManager\Models\Route;
use SalesRepManager\Models\SalesRep;
use SalesRepManager\Models\SalesRoute;
use SalesRepManager\Models\SalesVehicle;
use Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use StockManager\Classes\StockTransaction;
use StockManager\Models\Stock;
use VehicleManager\Models\Vehicle;
use EndProductManager\Models\Category;
use SalesRepManager\Models\AgentCategory;

class SalesRepController extends Controller
{
    use AgentLog, RawMaterialLog, CratesAgentLog;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect(route('sales_rep.create'));
        return view('SalesRepManager::sales_rep.index');
    }


    public function adjcrates(SalesRep $rep)
    {
        //  dd($rep->id);
        $sales_rep = $rep->id;
        $crates = RawMaterial::whereType(5)->get();

        // return redirect(route('sales_rep.create'));
        return view('SalesRepManager::sales_rep.crate-adjustment')->with(['sales_reps' => $sales_rep, 'crates' => $crates]);
    }

    public function AdjcrateStore(Request $request)
    {
        $crates_data = [];

        foreach ($request->update_return_crate as $key => $crates) {


            $crates_d = ['crate_id' => $crates, 'qty' => $request->update_return_crate_qty[$key]];
            array_push($crates_data, $crates_d);

            $cra_r = RawMaterial::find($crates);
            $agent = SalesRep::find($request->AgentId);


            $this->recordCratesAgentLogOutstanding($cra_r, $agent, $request->update_return_crate_qty[$key], 'Admin Crates Adjustment', 0, 'App\\SalesRepController', 21, 1);
        }


        return redirect(route('sales_rep.create'))->with([
            'success' => true,
            'success.title' => 'Adjustment Successfully Updated',
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sales_rep = SalesRep::whereHas('roles', function ($query) {
            $query->where('slug', 'like', 'sales-ref');
        })->get();

        $fromAPIDB = SalesAgentAPI::get();


        $flagSalesRepAlreadyWithAPI = $sales_rep->map(function ($item) use ($fromAPIDB) {
            $item->flag = $fromAPIDB->contains('email', $item->email);
            return $item;
        });


        return view('SalesRepManager::sales_rep.create')->with(['sales_reps' => $sales_rep,]);
    }


    public function editbal(Request $request)
    {
        $subdesc = 'Ajd-' . $request->desc;
        $ref = SalesRep::find($request->rep_id);
        $this->recordOutstanding($ref, $request->Adjust, 'Adjustment', 1, 'admin-section\\SalesRepControllers', $subdesc);
        StockTransaction::updateAgentBalance(1, $request->rep_id, $request->Adjust);


        return redirect(route('sales_rep.index'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => $ref->first_name . ' ' . $ref->second_name . ' ' . $ref->last_name . ' Outstanding has been Updated.'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        return $request;
        $credentials = [
            'first_name' => $request->fn,
            'last_name' => $request->ln,
            'email' => $request->email,
            'password' => $request->nic,
            'created_by' => Auth::user()->id,
            'color' => rand() % 7,
        ];
        $user = Sentinel::registerAndActivate($credentials);
        $role = Sentinel::findRoleBySlug('sales-ref');
        $role->users()->attach($user);

        return redirect(route('sales_rep.create'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => $request->fn . ' ' . $request->ln . ' has been added as an Buntalk SalesRep.'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \SalesRepManager\Models\SalesRep $sales_rep
     * @return \Illuminate\Http\Response
     */
    public function show(SalesRep $sales_rep)
    {
        return view('SalesRepManager::sales_rep.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \SalesRepManager\Models\SalesRep $sales_rep
     * @return \Illuminate\Http\Response
     */
    public function edit(SalesRep $rep)
    {
        $categories = Category::all()->pluck('name', 'id');
        return view('SalesRepManager::sales_rep.edit', compact('rep', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \SalesRepManager\Models\SalesRep $sales_rep
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SalesRep $rep)
    {
        if ($rep->balance != $request->balance) {
            $dif = ($rep->balance > $request->balance) ? -($rep->balance - $request->balance) : $request->balance - $rep->balance;
            $this->recordOutstanding($rep, $dif, "Agent's Outstanding Adjustment");

        }


        if (!empty($request->init_balance)) {
            $init = $rep->initInquiry->first();

            if (!empty($init)) {
                $this->initInquiryEdit($rep, $init, $request->init_balance);
            } else {
                $this->initInquiry($rep, $request->init_balance);
            }
        }

        $rep->type = $request->type;
        $rep->first_name = $request->first_name;
        $rep->second_name = $request->second_name;
        $rep->last_name = $request->last_name;
        $rep->name_with_initials = $request->name_with_initials;
        $rep->gender = $request->gender;
        $rep->address = $request->address;
        $rep->telephone1 = $request->telephone1;
        $rep->telephone2 = $request->telephone2;
        $rep->nic = $request->nic;
        $rep->appoint_date = $request->appoint_date;
        $rep->territory = $request->territory;
        $rep->email = $request->email;
        $rep->balance = $request->balance;
        $rep->save();


        //update sales api db
        $salesAgentAPI = SalesAgentAPI::where('email', $rep->email)->first();
        $salesAgentAPI->type = $request->type;
        $salesAgentAPI->first_name = $request->first_name;
        $salesAgentAPI->second_name = $request->second_name;
        $salesAgentAPI->last_name = $request->last_name;
        $salesAgentAPI->name_with_initials = $request->name_with_initials;
        $salesAgentAPI->gender = $request->gender;
        $salesAgentAPI->address = $request->address;
        $salesAgentAPI->telephone1 = $request->telephone1;
        $salesAgentAPI->telephone2 = $request->telephone2;
        $salesAgentAPI->nic = $request->nic;
        $salesAgentAPI->appoint_date = $request->appoint_date;
        $salesAgentAPI->territory = $request->territory;
        $salesAgentAPI->email = $request->email;
        $salesAgentAPI->balance = $request->balance;
        $salesAgentAPI->save();




        return redirect(route('sales_rep.index'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => $rep->first_name . ' ' . $rep->second_name . ' ' . $rep->last_name . ' has been Updated.'
        ]);

    }

    public function change(Request $request, SalesRoute $salesRoute)
    {
        $finds = Route::where('status', '0')->get()->count();
        if ($finds > 0) {
            $routs = Route::where('status', '0')->get();

            foreach ($routs as $rts) {
                $rts->status = 1;
                $rts->save();
            }
        }
//        return $request;
        $finds = Route::where('id', $request->route)->get();
        if (isset($request->route)) {

            foreach ($finds as $find) {
                $find->status = 0;
                $find->save();
            }
        }

        $status = SalesRoute::where('status', '1')->get()->count();

        if ($status > 0) {
            $routes = SalesRoute::where('status', '1')->get();

            foreach ($routes as $route) {
                $route->status = 0;
                $route->save();
            }

            if (isset($request->route)) {
                $salesRoute->route_id = $request->route;
                $salesRoute->status = 1;
                $salesRoute->agent_id = $request->rep;

                $salesRoute->save();
                return redirect('sales_rep/' . $request->rep . '/edit');
            }

        }


        if (isset($request->route)) {
            $salesRoute->route_id = $request->route;
            $salesRoute->status = 1;
            $salesRoute->agent_id = $request->rep;

            $salesRoute->save();
            return redirect('sales_rep/' . $request->rep . '/edit');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \SalesRepManager\Models\SalesRep $sales_rep
     * @return \Illuminate\Http\Response
     */
    public function destroy(SalesRep $sales_rep)
    {

        $sales_rep->delete();
        if ($sales_rep->trashed()) {
            return 'true';
        } else {
            return 'false';
        }

    }

    public function remove(SalesRep $sales_rep)
    {
        $vehicles = SalesRep::with('vehicle.getVehicle')
            ->where('id', $sales_rep->id)->first();

        foreach ($vehicles['vehicle'] as $item) {
            if ($item->status == 1) {
                $item->status = 0;
                $item->save();
            }

            $vehi = Vehicle::where('id', $item->vehicle_id)->first();
            $vehi->status = 1;
            $vehi->save();
        }
        return redirect('sales_rep/' . $sales_rep->id . '/edit');

    }

    public function removerts(SalesRep $sales_rep)
    {


        $routes = SalesRep::with('route.getRoute')
            ->where('id', $sales_rep->id)->first();

        foreach ($routes['route'] as $rts) {
            if ($rts->status == 1) {
                $rts->status = 0;
                $rts->save();
            }

            $r = Route::where('id', $rts->route_id)->first();
            $r->status = 1;
            $r->save();
        }
        return redirect('sales_rep/' . $sales_rep->id . '/edit');

    }

    public function categoryCreate(Request $request, $sales_rep)
    {
        $status = 0;
        if ($request->status == 'true') {
            $status = 1;
        }
        if ($request->status == 'false') {
            $status = 2;
        }
        $checkAgentCategory = AgentCategory::where('sales_ref_id', '=', $sales_rep)->where('category_id', '=', $request->category)->first();
        if (empty($checkAgentCategory)) {
            $agentCategory = new AgentCategory();
            $agentCategory->sales_ref_id = $sales_rep;
            $agentCategory->category_id = $request->category;
            $agentCategory->discount_mode = $request->discountMode;
            $agentCategory->return_mode = $request->returnMode;
            $agentCategory->loading_mode = $request->loadingMode;
            $agentCategory->status = $status;
            $agentCategory->save();
            return 'true';
        } else {
            return 'false';
        }
    }

    public function categoryTableData($sales_rep)
    {
        $agentCategoryArray = [];
        $discount_mode = null;
        $return_mode = null;
        $loading_mode = null;
        $status = null;
        $categories = Category::all();
        $categoryArray = [];
        foreach ($categories as $key => $categoryData) {
            $id = $categoryData->id;
            $name = $categoryData->name;
            array_push($categoryArray, ['id' => $id, 'name' => $name]);
        }
        $agentCategorys = AgentCategory::where('sales_ref_id', '=', $sales_rep)->get();
        foreach ($agentCategorys as $key => $agentCategory) {
            $category = Category::find($agentCategory->category_id);
            $categoryName = $category->name;

            if ($agentCategory->discount_mode == 1) {
                $discount_mode = 'Yes';
            } else {
                $discount_mode = 'No';
            }

            if ($agentCategory->return_mode == 1) {
                $return_mode = 'Reuse Only';
            } else {
                $return_mode = 'All';
            }

            if ($agentCategory->loading_mode == 1) {
                $loading_mode = 'Priority';
            } else {
                $loading_mode = 'None';
            }

            if ($agentCategory->status == 1) {
                $status = 'Active';
            } else {
                $status = 'Deactive';
            }
            $id = $agentCategory->id;
            array_push($agentCategoryArray, ['sales_ref_category_id' => $id, 'sales_ref_id' => $sales_rep, 'category_id' => $categoryName, 'discount_mode' => $discount_mode,
                'return_mode' => $return_mode, 'loading_mode' => $loading_mode, 'status' => $status, 'categories' => $categoryArray]);
        }
        return $agentCategoryArray;
    }

    public function categoryUpdate(Request $request, $sales_category)
    {
        $status = 0;
        if ($request->status == 'true') {
            $status = 1;
        }
        if ($request->status == 'false') {
            $status = 2;
        }
        $agentCategory = AgentCategory::where('sales_ref_id', '=', $request->agent_id)->where('category_id', '=', $request->category)->first();
        if (!empty($agentCategory)) {
            if ($agentCategory->id == $sales_category) {
                $agentCategory->category_id = $request->category;
                $agentCategory->discount_mode = $request->discountMode;
                $agentCategory->return_mode = $request->returnMode;
                $agentCategory->loading_mode = $request->loadingMode;
                $agentCategory->status = $status;
                $agentCategory->save();
                return 'true';
            } else {
                return 'false';
            }
        } else {
            $agentCategorys = AgentCategory::where('id', '=', $sales_category)->first();
            $agentCategorys->category_id = $request->category;
            $agentCategorys->discount_mode = $request->discountMode;
            $agentCategorys->return_mode = $request->returnMode;
            $agentCategorys->loading_mode = $request->loadingMode;
            $agentCategorys->status = $status;
            $agentCategorys->save();
            return 'true';
        }

    }

    public function syncUser(Request $request, $id)
    {
        $buntalkUser = SalesRep::where('id', $id)->first();

        try {
            $apiUser = new SalesAgentAPI();
            $apiUser->id = $buntalkUser->id;
            $apiUser->email = $buntalkUser->email;
            $apiUser->password = $buntalkUser->password;
            $apiUser->type = $buntalkUser->type;
            $apiUser->first_name = $buntalkUser->first_name;
            $apiUser->second_name = $buntalkUser->second_name;
            $apiUser->last_name = $buntalkUser->last_name;
            $apiUser->name_with_initials = $buntalkUser->name_with_initials;
            $apiUser->telephone1 = $buntalkUser->telephone1;
            $apiUser->telephone2 = $buntalkUser->telephone2;
            $apiUser->appoint_date = $buntalkUser->appoint_date;
            $apiUser->appoint_date = $buntalkUser->appoint_date;
            $apiUser->gender = $buntalkUser->gender;
            $apiUser->territory = $buntalkUser->territory;
            $apiUser->address = $buntalkUser->address;
            $apiUser->nic = $buntalkUser->nic;
            $apiUser->created_by = $buntalkUser->created_by;
            $apiUser->color = $buntalkUser->color;
            $apiUser->otp_status = $buntalkUser->otp_status;
            $apiUser->crates = $buntalkUser->crates;
            $apiUser->balance = $buntalkUser->balance;
            $apiUser->save();
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return redirect(route('sales_rep.create'))->with([
                'error' => true,
                'error.title' => 'Error !',
                'error.message' => 'Something went wrong. Please Contact admin'
            ]);
        }
        return redirect(route('sales_rep.create'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => $request->fn . ' ' . $request->ln . ' has been synced with mobile app'
        ]);
    }
}
