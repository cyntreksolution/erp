<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web','log'])->group(function () {
    Route::prefix('sales_head')->group(function () {
        Route::get('', 'SalesHeadManager\Controllers\SalesHeadController@index')->name('sales_head.index');

        Route::get('create', 'SalesHeadManager\Controllers\SalesHeadController@create')->name('sales_head.create');

        Route::get('{sales_head}', 'SalesHeadManager\Controllers\SalesHeadController@show')->name('sales_head.show');

        Route::get('{sales_head}/edit', 'SalesHeadManager\Controllers\SalesHeadController@edit')->name('sales_head.edit');


        Route::post('', 'SalesHeadManager\Controllers\SalesHeadController@store')->name('sales_head.store');

        Route::put('{sales_head}', 'SalesHeadManager\Controllers\SalesHeadController@update')->name('sales_head.update');

        Route::delete('{sales_head}', 'SalesHeadManager\Controllers\SalesHeadController@destroy')->name('sales_head.destroy');
    });
});
