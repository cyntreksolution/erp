<?php

namespace SalesHeadManager\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use UserManager\Models\Users;

class SalesHead extends Users
{
    use SoftDeletes;

    protected $table = 'users';

    protected $primaryKey = 'id';

    protected $dates = ['deleted_at'];
}
