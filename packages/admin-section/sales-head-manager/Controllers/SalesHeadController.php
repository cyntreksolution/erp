<?php

namespace SalesHeadManager\Controllers;

use AssistantManager\Models\Assistant;
use Illuminate\Support\Facades\Auth;
use SalesHeadManager\Models\SalesHead;
use SalesRepManager\Models\SalesRep;
use Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use StockManager\Models\Stock;

class SalesHeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect(route('sales_head.create'));
        return view('SalesHeadManager::sales_head.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sales_head = SalesHead::whereHas('roles', function ($query) {
            $query->where('slug', 'like', 'sales-manager');
        })->get();
        return view('SalesHeadManager::sales_head.create')->with(['sales_heads' => $sales_head]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $credentials = [
            'first_name' => $request->fn,
            'last_name' => $request->ln,
            'email' => $request->email,
            'password' => $request->nic,
            'created_by' => Auth::user()->id,
            'color' => rand() % 7,
        ];
        $user = Sentinel::registerAndActivate($credentials);
        $role = Sentinel::findRoleBySlug('sales-manager');
        $role->users()->attach($user);

        return redirect(route('sales_head.create'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => $request->fn.' '.$request->ln.' has been added as an Buntalk Sales Manager.'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \SalesHeadManager\Models\SalesHead $sales_head
     * @return \Illuminate\Http\Response
     */
    public function show(SalesHead $sales_head)
    {
        return view('SalesHeadManager::sales_head.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \SalesHeadManager\Models\SalesHead $sales_head
     * @return \Illuminate\Http\Response
     */
    public function edit(SalesHead $sales_head)
    {
        return view('SalesHeadManager::sales_head.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \SalesHeadManager\Models\SalesHead $sales_head
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SalesHead $sales_head)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \SalesHeadManager\Models\SalesHead $sales_head
     * @return \Illuminate\Http\Response
     */
    public function destroy(SalesHead $sales_head)
    {
        $sales_head->delete();
        if ($sales_head->trashed()) {
            return 'true';
        } else {
            return 'false';
        }

    }
}
