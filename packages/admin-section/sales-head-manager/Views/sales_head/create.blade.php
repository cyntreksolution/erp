@extends('layouts.back.master')@section('title','Sales Manager')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }
    </style>
@stop
@section('content')
    <div class="app-wrapper">
        <div class="app-panel" id="app-panel">
            <div class="app-search">
                <input type="search" class="search-field" placeholder="Search" disabled>
                <i class="search-icon fa fa-search"></i>
            </div>
            <div class="app-panel-inner">
                <div class="scroll-container">
                    <div class="p-3">
                        <button class="btn btn-success py-3 btn-block btn-lg" data-toggle="modal"
                                data-target="#projects-task-modal">ADD NEW SALES MANAGER
                        </button>
                    </div>
                </div>
            </div>
            <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show"><i
                        class="fa fa-chevron-right"></i>
                <i class="fa fa-chevron-left"></i>
            </a>
        </div>
        <!-- /.app-panel -->
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">SALES MANAGERS</h5>
            </div><!-- /.app-main-header -->
            <div class="app-main-content">
                <div class="scroll-container">
                    <div class="media-list">
                        @foreach($sales_heads as $sales_head)
                            <a href="{{$sales_head->id.'/edit'}}">
                                <div class="media">
                                    @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                    <div class="avatar avatar-circle avatar-md project-icon bg-{{$color[$sales_head->color]}}"
                                         data-plugin="firstLitter"
                                         data-target="#{{$sales_head->id*754}}"></div>
                                    <div class="media-body">
                                        <h6 class="project-name"
                                            id="{{$sales_head->id*754}}">{{$sales_head->first_name.' '.$sales_head->last_name}}</h6>
                                        @foreach($sales_head->roles as $role)
                                            <small class="project-detail">{{$role->name}}</small>
                                        @endforeach
                                    </div>
                                    <div class="section-2">
                                        <button class="delete btn btn-light btn-sm ml-2" value="{{$sales_head->id}}"
                                                data-toggle="tooltip"
                                                data-placement="left" title="DELETE SALES MANAGER">
                                            <i class="zmdi zmdi-close"></i>
                                        </button>
                                    </div>
                                </div>
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="projects-task-modal" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'sales_head')) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="fn" type="text" class="task-name-field" placeholder="first name">
                        </div>
                        <div class="col-md-6 task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="ln" type="text" class="task-name-field" placeholder="last name">
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="email" type="text" class="task-name-field" placeholder="email">
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="nic" type="text" class="task-name-field" placeholder="nic">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $(".js-example-responsive").select2({
            width: 'resolve' // need to override the changed default
        });


        $('.delete').click(function (e) {
            e.preventDefault();
            id = $(this).attr("value");
            confirmAlert(id);

        });

        function confirmAlert(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "delete",
                        url: id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            }
                            else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

    </script>
@stop