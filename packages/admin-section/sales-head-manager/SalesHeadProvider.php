<?php

namespace SalesHeadManager;

use Illuminate\Support\ServiceProvider;

class SalesHeadProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/Views', 'SalesHeadManager');
        require __DIR__ . '/web.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('SalesHeadManager', function($app){
            return new SalesHeadManager;
        });
    }
}
