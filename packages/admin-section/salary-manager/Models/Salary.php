<?php

namespace SalaryManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use UserManager\Models\Users;

class Salary extends Model
{
    use SoftDeletes;

    protected $table='attendance';

    protected $dates = ['deleted_at'];


    public function employee()
    {
        return $this->belongsToMany('EmployeeManager\Models\Employee');

    }
}
