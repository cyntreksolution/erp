<?php

namespace SalaryManager\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use UserManager\Models\Users;

class Attendance extends Model
{
    use SoftDeletes;

    protected $table='attendance';

    protected $dates = ['deleted_at'];

    protected $fillable =[
        'employee_id',
        'date',
        'work_hours',
        'work_rate',
        'work_total',
        'created_by',
    ];

    public function employee()
    {
        return $this->belongsTo('EmployeeManager\Models\Employee');

    }

    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query->orderBy('date', 'asc')
            ->offset($start)
            ->limit($length);
    }

    public function scopefilterData($query, $employee=null,$date_range = null)
    {
        if (!empty($employee)) {
            $query->where('employee_id', '=',$employee) ;
        }

        if (!empty($date_range)) {
            $dates = explode(' - ', $date_range);
            $start = $dates[0];
            $end = $dates[1];
            $query->where('date', '>=', Carbon::parse($start)->format('Y-m-d'))
                ->where('date', '<=', Carbon::parse($end)->format('Y-m-d'));
        }

        return $query;
    }

}
