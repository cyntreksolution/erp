<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web'])->group(function () {
    Route::prefix('attendance')->group(function () {
        Route::get('', 'SalaryManager\Controllers\AttendanceController@index')->name('attendance.index');

        Route::post('create', 'SalaryManager\Controllers\AttendanceController@create')->name('attendance.create');
//        Route::post('create', 'AttendanceManager\Controllers\AttendanceController@create')->name('attendance.create');

        Route::get('{employee}', 'SalaryManager\Controllers\AttendanceController@show')->name('attendance.show');

        Route::get('{employee}/edit', 'SalaryManager\Controllers\AttendanceController@edit')->name('attendance.edit');

        Route::get('get/employee', 'SalaryManager\Controllers\AttendanceController@getemployee')->name('attendance.edit');


//        Route::post('', 'AttendanceManager\Controllers\AttendanceController@store')->name('attendance.store');

        Route::post('add', 'SalaryManager\Controllers\AttendanceController@store')->name('attendance.store');

        Route::get('do/search', 'SalaryManager\Controllers\AttendanceController@search')->name('attendance.store');

        Route::put('{employee}', 'SalaryManager\Controllers\AttendanceController@update')->name('attendance.update');

        Route::delete('{employee}', 'SalaryManager\Controllers\AttendanceController@destroy')->name('attendance.destroy');
    });

    Route::prefix('salary')->group(function () {
        Route::get('salCalc', 'SalaryManager\Controllers\SalaryController@index')->name('salary.index');

        Route::post('create', 'SalaryManager\Controllers\SalaryController@create')->name('salary.create');
//        Route::post('create', 'AttendanceManager\Controllers\SalaryController@create')->name('salary.create');

        Route::get('{employee}', 'SalaryManager\Controllers\SalaryController@show')->name('salary.show');

        Route::get('edit/{salary}', 'SalaryManager\Controllers\SalaryController@edit')->name('salary.edit');

        Route::get('get/employee', 'SalaryManager\Controllers\SalaryController@getemployee')->name('salary.edit');


//        Route::post('', 'AttendanceManager\Controllers\SalaryController@store')->name('salary.store');

        Route::post('add', 'SalaryManager\Controllers\SalaryController@store')->name('salary.store');

        Route::post('{salary}', 'SalaryManager\Controllers\SalaryController@store')->name('salary.store');

        Route::get('json/list', 'SalaryManager\Controllers\SalaryController@jsonList')->name('salary.jsonList');

        Route::get('a/gets', 'SalaryManager\Controllers\SalaryController@change')->name('salary.change');

        Route::put('{employee}', 'SalaryManager\Controllers\SalaryController@update')->name('salary.update');

        Route::delete('{employee}', 'SalaryManager\Controllers\SalaryController@destroy')->name('salary.destroy');
    });

    Route::prefix('allowance')->group(function () {
        Route::get('', 'SalaryManager\Controllers\AllowanceController@index')->name('allowance.index');

        Route::post('create', 'SalaryManager\Controllers\AllowanceController@create')->name('allowance.create');
//        Route::post('create', 'AttendanceManager\Controllers\AllowanceController@create')->name('allowance.create');

        Route::get('{employee}', 'SalaryManager\Controllers\AllowanceController@show')->name('allowance.show');

        Route::get('{employee}/edit', 'SalaryManager\Controllers\AllowanceController@edit')->name('allowance.edit');

        Route::get('get/employee', 'SalaryManager\Controllers\AllowanceController@getemployee')->name('allowance.edit');


//        Route::post('', 'AttendanceManager\Controllers\AllowanceController@store')->name('allowance.store');

        Route::post('add', 'SalaryManager\Controllers\AllowanceController@store')->name('allowance.store');

        Route::get('do/search', 'SalaryManager\Controllers\AllowanceController@search')->name('allowance.store');

        Route::put('{employee}', 'SalaryManager\Controllers\AllowanceController@update')->name('allowance.update');

        Route::delete('{employee}', 'SalaryManager\Controllers\AllowanceController@destroy')->name('allowance.destroy');
    });
});
