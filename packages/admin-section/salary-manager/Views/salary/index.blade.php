@extends('layouts.back.master')
@section('title','Attendance Manager')
@section('css')
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"/>
    {{--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css"/>--}}
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/select/1.2.5/css/select.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="https://editor.datatables.net/extensions/Editor/css/editor.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <style>

    </style>
@stop
@section('content')


    {{ Form::open(array('url' => 'salary/add','enctype'=>'multipart/form-data', 'id'=>'form'))}}
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                <input type="text" name="date" id="dte" class="form-control form-control-1 input-sm dte"
                       placeholder="select month">
            </div>

        </div>
    </div>

    <table id="example" class="display text-center" style="width:100%">
        <thead>
        <tr>
            <th>#</th>
            <th>a</th>
            <th>Name</th>
            <th>Work days</th>
            <th>OT hrs</th>
            <th>OT total</th>
            <th>Basic</th>
            <th>Incentives</th>
            <th>Gross salary</th>
            <th>EPF</th>
            <th>ETF</th>
            <th>Advances</th>
            <th>Net salary</th>
            <th>Change</th>
            <th>Create</th>


        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>#</th>
            <th>a</th>
            <th>Name</th>
            <th>Work days</th>
            <th>OT rate</th>
            <th>OT total</th>
            <th>Basic</th>
            <th>Incentives</th>
            <th>Gross salary</th>
            <th>EPF</th>
            <th>ETF</th>
            <th>Advances</th>
            <th>Net salary</th>
            <th>Change</th>
            <th>Create</th>

        </tr>
        </tfoot>
    </table>
    <input type="button" value="Create" class="create btn btn-success">
    {!!Form::close()!!}

    <div class="modal fade" id="projects-task-modal" tabindex="-1" role="dialog"
         aria-labelledby="image-gallery-modal" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                    {{--<div class="task-pickers">--}}
                    {{--<div class="task-field-picker task_date_picker">--}}
                    {{--<div for="task_due_date">--}}
                    {{--<i class="zmdi zmdi-calendar"></i>--}}
                    {{--</div>--}}
                    {{--<input type="text" name="specification"--}}
                    {{--placeholder="specification">--}}
                    {{--</div>--}}
                    {{--</div>--}}
                </div>
                <div class="modal-body p-5">

                </div><!-- /.modal-body -->
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
@stop
@section('js')

    <script src="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/js/jquery-1.12.4.js')}}"></script>

    <script src="{{asset('assets/vendor/js/dataTables.editor.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>



    {{--<script src="{{asset('https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>--}}

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
    {{--<script src="https://cdn.datatables.net/select/1.2.5/js/dataTables.select.min.js"></script>--}}
    {{--<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>--}}

    {{--<script src="{{asset('https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js')}}"></script>--}}
    {{--<script src="{{asset('https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js')}}"></script>--}}
    {{--<script src="{{asset('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js')}}"></script>--}}
    {{--<script src="{{asset('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js')}}"></script>--}}
    {{--<script src="{{asset('https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js')}}"></script>--}}
    {{--<script src="{{asset('https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js')}}"></script>--}}
    {{--<script src="{{asset('js/editor.js')}}"></script>--}}
    {{--<script src="https://editor.datatables.net/extensions/Editor/js/dataTables.editor.min.jss"></script>--}}

    <script>



        $(document).ready(function () {
            $('.dte').datepicker().datepicker('setDate', 'today');
            var date = $('#dte').val();
            var table = $('#example').DataTable({
                "ajax": {
                    'type': 'GET',
                    'url': 'salary/json/list',
                    'data': {
                        date: date
                    },
                }


            });


            $('#dte').change(function () {
                var date = $('#dte').val();

                if ($.fn.DataTable.isDataTable('#example')) {
                    $('#example').DataTable().destroy();
                }

                $('#example tbody').empty();

                $('#example').DataTable({
                    select: {
                        style: 'multi'
                    },
                    "ajax": {
                        'type': 'GET',
                        'url': 'salary/json/list',
                        'data': {
                            date: date
                        },
                        'dom': 'Bfrtip',
                        'buttons': [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ]
                    }

                })

            })
        });



        $('.dte').datepicker({
            autoclose: true,
            minViewMode: 1,
            format: 'mm/yyyy',
        });



        $('.create').click(function () {

            swal({
                    title: "Are you sure?",
                    text: "You will not be able to undo this payment!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        swal("Created!", "Payment added", "success");
                        setTimeout(location.reload.bind(location), 2000);
                        $("#form").submit();

                        // setTimeout(location.reload.bind(location), 600);
                    } else {
                        swal("Cancelled");
                    }
                });
        })
    </script>
@stop