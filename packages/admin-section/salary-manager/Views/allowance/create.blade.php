@extends('layouts.back.master')@section('title','Allowance Manager')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.wizard.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/messaging.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/inbox.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/clockpicker/dist/bootstrap-clockpicker.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css"/>

    <link rel="stylesheet"
          href="{{asset('assets\vendor\bootstrap-slider-master\dist\css\bootstrap-slider.min.css')}}">



    <style type="text/css">
        #container {
            position: relative;
            padding: 0px;
            width: 100%;
            height: 350px;
            overflow: auto;
        }

        #container .content {
            width: 100%;
            height: 100%;
        }

        .avatar {
            width: 1.5rem;
            height: 1.5rem;
            font-size: 16px;
        }

    </style>
@stop
@section('content')
    <div class="app-wrapper">
        <div class="app-main">

            <div class="app-main-header" style="height: 20px">
                <div class="row ">
                    <div class="col-md-5 ">
                        Work Date: {{$date}}
                    </div>
                    <div class="col-md-3 text-center">
                        Cash Flow Type: {{$cst}}
                    </div>
                    <div class="col-md-2 text-center ">

                    </div>
                    <div class="col-md-2 text-center ">
                       Amount
                    </div>
                </div>
            </div>
            {{ Form::open(array('url' => 'allowance/add','enctype'=>'multipart/form-data',))}}
            <div class="app-main-content bg-white py-4 " id="container">

                <div class=" px-2 content">
                    <div class=" px-2">
                        <div class="mail-box">
                                @foreach($employees as $employee)
                                    <div class="row">

                                        <div class="col-md-10 my-2">
                                            <div class="panel-item media">
                                                <a>
                                                    <div class=" mr-3">
                                                        <input class="check" name="empId[]" type="checkbox"
                                                               data-plugin="switchery" data-size="small"
                                                               data-switchery="true" style="display: none;"
                                                               value="{{$employee->id}}" checked readonly>

                                                        {{--<input type="hidden" name="empId[]" value="{{$employee->id}}">--}}

                                                    </div>
                                                </a>

                                                @if(isset($employee->image))
                                                    <a href="javascript:void(0)" class="avatar avatar-circle avatar-sm">
                                                        <img src="{{asset($employee->image_path.$employee->image)}}"
                                                             alt="Generic placeholder image">
                                                    </a>
                                                    <div class="avatar avatar avatar-sm">
                                                        <img src="{{asset($employee->image_path.$employee->image)}}"
                                                             alt="">
                                                    </div>
                                                @else
                                                    @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                                    <a
                                                            class="avatar avatar-circle avatar-md project-icon text-white text-small bg-{{$color[$employee->color]}} "
                                                            data-plugin="firstLitter"
                                                            data-target="#raw-{{$employee->id*874}}">
                                                    </a>
                                                @endif

                                                <div class="media-body">
                                                    <h5 class="mail-cat-name">
                                                        <h6 id="raw-{{$employee->id*874}}"
                                                            id="">{{$employee->full_name}}</h6>
                                                    </h5>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-2 mt-3">
                                            <input type="hidden" value="{{$employee->id}}" name="emp[]">
                                            <input class="form-control" id="otr{{$employee->id}}" type="text"
                                                   name="amount[]" value="0">
                                        </div>


                                    </div>
                                @endforeach
                        </div>
                    </div>
                </div>
                <input type="hidden" id="main_desc_id" name="main_desc_id" value=1>
                <input type="hidden" value="{{$date}}" name="dte"><input type="hidden" value="{{$cst}}" name="cst">
                <input type="hidden" value="{{$transaction}}" name="transaction"><input type="hidden" value="{{$balance}}" name="balance">
            </div>
            <div class="row">
                <div class="col-md-3 mt-3">
                    <select name="sub_desc" required
                            class="form-control select2 " id="sub_desc">
                        <option value="">Sub Description</option>
                        @foreach($cashinDesc as $cashinDesc)
                           <option value="{{$cashinDesc->id}}">{{$cashinDesc->sub_desc}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3 mt-2">
                    Remark
                </div>
                <div class="col-md-3 mt-3">

                    <input class="form-control"  type="text" name="remark" value="" required>
                </div>

                <div class="col-md-1 mt-2" id="app-messaging-form bg-white" style="padding: 5px; float: right">
                    <input type="submit" id="add" class="btn btn-success btn-md" value="Add">
                </div>
            </div>

            {!!Form::close()!!}
        </div>


    </div>
@stop
@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>

    {{--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--}}
    <script src="{{asset('assets/vendor/bootstrap-slider-master/dist/bootstrap-slider.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>

    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="{{asset('assets/examples/js/demos/form.wizard.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/moment/min/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/clockpicker/dist/bootstrap-clockpicker.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

    <script>

        var ps = new PerfectScrollbar('#container');
        // $('.check').change(function (e) {
        //     if ($('.check').is(":checked")) {
        //         $(this).find('.aw').remove();
        //     }
        //     else {
        //
        //     }
        //     });


        // $(document).ready(function() {
        //     $(".check").checked;
        // });

        $('.check').change(function (e) {
            e.preventDefault();
            var id = $(this).attr("value");

            if ($(this).is(":checked")) {
                $("#oth" + id).prop("disabled", false);
                $("#otr" + id).prop("disabled", false);
                $("#hrs" + id).slider("enable");
                // data-slider-enabled="false
            }
            else {

                $("#oth" + id).prop("disabled", true);
                $("#otr" + id).prop("disabled", true);
                $("#hrs" + id).slider("disable");

            }

        });

        // $('.check').change(function (e) {
        //     e.preventDefault();
        //     var id = $(this).attr("value");
        //     var ot= $(e).closest('.oth').attr("id");
        //     alert(ot);
        //
        //     if ($(this).is(":checked")) {
        //
        //         // $(this).find('.aw').remove();
        //         $(e).closest(".oth").attr("disabled","disabled");
        //         // $(".oth").attr("disabled","disabled");
        //         // $(".otr").attr("disabled","disabled");
        //         // $(".hrs").attr("data-slider-enabled","false");
        //         // data-slider-enabled="false
        //
        //         $(e).parent().addClass('hide');
        //
        //
        //     }
        //     else {
        //         // alert(12);
        //         $(e).closest(".oth").prop("disabled", true);
        //        // var aa= $(".oth").prop("disabled", true);
        //        // alert(aa)
        //        //  $("#target input").prop("disabled", true);
        //     }
        // });

        $('#search').click(function (e) {
            var d = $('#date').val();
            e.preventDefault();
            // alert(d);

            $.ajax({

                type: "get",
                url: 'attendance/do/search',
                data: {date: d},

                success: function (response) {

                    $(".packing").html('');

                    // for(var i=0; i<response.length;i++) {
                    //     var res =response[i];
                    //     $(".packing").append('<div class="card mx-2">' +
                    //         ' <a href="' + res['id'] + '" target="_blank">' +
                    //         '<div class="media mx-4 content2">' +
                    //         '<div class="avatar avatar text-white avatar-md project-icon bg-primary" data-target="#project-1">' +
                    //         '<i class="fa fa-fire" aria-hidden="true"></i>' +
                    //         '</div>' +
                    //         '<div class="media-body">' +
                    //         '<h6 class="project-name" id="project-1">' + res['name'] + '</h6>' +
                    //         '<small class="project-detail">Usage :' + res['qty'] + '</small>' +
                    //         '<div>' +
                    //         '</div>' +
                    //         '</a>' +
                    //         '</div>');
                    // }

                }
            });
        });


    </script>

@stop