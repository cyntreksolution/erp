@extends('layouts.back.master')@section('title','Attendance Manager')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.wizard.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/messaging.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/inbox.css')}}">

    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">

    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">
    {{--<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />--}}



    <style type="text/css">
        #container {
            position: relative;
            padding: 0px;
            width: 100%;
            height: 350px;
            overflow: auto;
        }

        #container .content {
            width: 100%;
            height: 100%;
        }

        .avatar {
            width: 1.5rem;
            height: 1.5rem;
            font-size: 16px;
        }
    </style>
@stop
@section('content')
    <div class="app-wrapper">


        <div class="app-main">
            {{ Form::open(array('url' => 'attendance/create','enctype'=>'multipart/form-data','id'=>'bootstrap-wizard-form'))}}
            <div class="app-main-header">
                <div class="row">


                    @if($role = Auth::user()->hasRole(['Owner']))
                        <div class="col-md-7">
                            <div class="form-group">
                                <input class="form-control" id="date_range" type="text" name="date" placeholder="select date"/>
                            </div>
                        </div>
                    @else
                        <input class="form-control" id="date" value="{{\Carbon\Carbon::today()}}" type="hidden" name="date" placeholder="select date"/>
                    @endif


                </div>
            </div>
            <div class="app-main-content bg-white py-4 " id="container">
                <div class=" px-2 content">
                    <div class=" px-2">
                        <div class="mail-box">
                            {{--inbox-cat-list  for green bacground--}}

                            <div class="row">
                                @foreach($employees as $employee)
                                    <div class="col-md-4 my-2">
                                        <div class="panel-item media">
                                            <a>
                                                <div class=" mr-3">
                                                    <input type="checkbox" class="check" data-plugin="switchery"
                                                           name="att[]" data-size="small"
                                                           data-switchery="true" style="display: none;"
                                                           value="{{$employee->id}}">
                                                    <input type="hidden" name="empId[]" value="{{$employee->id}}">
                                                    <label for="mail-item-3">
                                                    </label>

                                                </div>
                                            </a>

                                            @if(isset($employee->image))
                                                <a href="javascript:void(0)" class="avatar avatar-circle avatar-sm">
                                                    <img src="{{asset($employee->image_path.$employee->image)}}"
                                                         alt="Generic placeholder image">
                                                </a>
                                                <div class="avatar avatar avatar-sm">
                                                    <img src="{{asset($employee->image_path.$employee->image)}}" alt="">
                                                </div>
                                            @else
                                                @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                                <a
                                                        class="avatar avatar-circle avatar-md project-icon text-white text-small bg-{{$color[$employee->color]}} "
                                                        data-plugin="firstLitter"
                                                        data-target="#raw-{{$employee->id*874}}">
                                                </a>

                                            @endif


                                            {{--<a href="javascript:void(0)" class="avatar avatar-circle avatar-sm"><img--}}
                                            {{--src="assets/global/images/203.jpg" alt="Generic placeholder image">--}}
                                            {{--<i--}}
                                            {{--class="status status-online"></i>--}}
                                            {{--</a>--}}

                                            <div class="media-body">
                                                <h5 class="mail-cat-name">
                                                    <h6 id="raw-{{$employee->id*874}}"
                                                        id="">{{$employee->full_name}}</h6>
                                                </h5>
                                            </div>
                                        </div>

                                    </div>
                                @endforeach
                            </div>

                        </div>
                    </div>

                </div>
            </div>
            <div id="app-messaging-form" style="padding: 10px; float: right">
                <input type="submit" class="btn btn-success btn-md" value="Next">
            </div>
            {!!Form::close()!!}
        </div>
    </div>

    <div class="padding">
        <div class="row">

        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
{{--    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--}}
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="{{asset('assets/examples/js/demos/form.wizard.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>

    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
     <script>
        var ps = new PerfectScrollbar('#container');

        // $(document).ready(function() {
        //     // var m = moment().format("MM/DD/YYYY");
        //     // alert(m)
        // });


        $(function () {
            $('#date_range').daterangepicker({
                "singleDatePicker": true,
                "showDropdowns": true,
                "timePicker": true,
                "timePicker24Hour": true,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                },
                "locale": {
                    "format": "YYYY-MM-DD",
                },
                "minDate": "11/01/2019",
                "maxDate":moment()
            });


            $('#date_range').val('')
        });


        $("#bootstrap-wizard-form").validate({
            rules: {
                date: "required",
                aa: "required",
            },
            messages: {
                date: "Please select a date",
                aa: "Please select a date",


            },

        });
    </script>
@stop
