<?php

namespace SalaryManager\Controllers;

use Carbon\Carbon;
use CashLockerManager\Classes\CashTransaction;
use CashLockerManager\Models\CashBookTransaction;
use CashLockerManager\Models\CashLocker;
use CashLockerManager\Models\main_desc_cashlocker;
use SalaryManager\Models\Allowance;
use EmployeeManager\Models\Employee;
use Illuminate\Support\Facades\DB;
use Sentinel;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use OwnerManager\Models\Owner;
use ProductionManagerManager\Models\ProductionManager;
use RecipeManager\Models\Recipe;
use SemiFinishProductManager\Models\SemiFinishProduct;

class AllowanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:allowance-index', ['only' => ['index']]);
        $this->middleware('permission:allowance-list', ['only' => ['list']]);
        $this->middleware('permission:allowance-create', ['only' => ['create','store']]);

    }
    public function index()
    {
//        return redirect(route('attendance.create'));
        $employee = Employee::orderBy('full_name')->get();
//        return $employee;
        return view('SalaryManager::allowance.index')->with([
            'employees' => $employee
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'att' => 'required',
            'date' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect(route('allowance.index'))
                ->with([
                    'error' => true,
                    'error.title' => 'Error !',
                    'error.message' => 'Please select employees'
                ])
                ->withInput();
        }

        $maindesc = 'SalaryAndWages';
        $cat = 'Employees';
        $emp = $request->att;
        $dt = $request->date;
        $cst = $request->cash_type;

        $cashinDesc = main_desc_cashlocker::where('main_desc', '=' , $maindesc)
                                            ->where('category', '=' , $cat)
                                           ->where('type', '=' , $cst)
                                            ->get();
        $balance = CashLocker::where('id', 1)->first();
        $transaction = CashBookTransaction::where('created_at', '>', Carbon::today())->get();

        $employee = null;

        foreach ($emp as $att) {

            $employee[] = Employee::where('id', '=', $att)->first();
        }

        return view('SalaryManager::allowance.create')->with([
            'employees' => $employee,
            'date' => $dt,
            'cst' => $cst,
            'balance' => $balance,
            'transaction' => $transaction,
            'cashinDesc' => $cashinDesc
        ]);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $emp = $request->emp;
        $subdesc = $request->sub_desc;
        $date = strtotime($request->dte);

        foreach ($emp as $key => $id) {
            $subdescName = main_desc_cashlocker::where('id' , '=' , $subdesc)->first();
            $emp_use = Employee::where('id', '=', $id)->first();
            $use_type = $emp_use->full_name;
            $category_reference_id = $id;
            $flow = $request->cst;
            $desc = $subdescName->sub_desc.'-'.$request->remark.' For '.$use_type;
            $description = $desc;
            $main_desc = $subdescName->main_desc;
            $main_desc_id = $subdesc;
            $amount = $request->amount[$key];
            if ($flow == 'CashIn') {
                CashTransaction::updateCashLocker(1, $amount);
                CashTransaction::updateCashTransaction(1, $amount, 0, 1, $description,$main_desc_id,$category_reference_id,$main_desc,$subdescName->category);
            } else {
                CashTransaction::updateCashLocker(2, $amount);
                CashTransaction::updateCashTransaction(2, $amount, 0, 0, $description,$main_desc_id,$category_reference_id,$main_desc,$subdescName->category);
            }
           /* $att = new Allowance();
            $att->employee_id = $id;
            $att->date = date('Y-m-d', $date);
            $att->amount = $amt[$key];
            $att->issued_by = $issued;
            $att->created_by = Sentinel::getUser()->id;
            $att->save();*/


        }

        return redirect(route('allowance.index'))->with([
            'success' => true,
            'success.title' => 'Success !',
            'success.message' => 'allowances has been added.'
        ]);


    }



    /**
     * Display the specified resource.
     *
     * @param  \EmployeeManager\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Salary $employee)
    {
        return view('SalaryManager::allowance.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \EmployeeManager\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Salary $employee)
    {
        return view('SalaryManager::allowance.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \EmployeeManager\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Salary $employee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \EmployeeManager\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Salary $employee)
    {
//        $employee->delete();
//        if ($employee->trashed()) {
//            return 'true';
//        } else {
//            return 'false';
//        }

    }

    public function search(Request $request)
    {
//        return $request;
//        return $raw = RawMaterial::where('raw_material_id', '=', $request->id)->first();

    }
}
