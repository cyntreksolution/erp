<?php

namespace SalaryManager\Controllers;

use Illuminate\Support\Facades\Auth;
use SalaryManager\Models\Attendance;
use EmployeeManager\Models\Employee;
use Illuminate\Support\Facades\DB;
use Sentinel;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use OwnerManager\Models\Owner;
use ProductionManagerManager\Models\ProductionManager;
use RecipeManager\Models\Recipe;
use SemiFinishProductManager\Models\SemiFinishProduct;

class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:attendance-index', ['only' => ['index']]);
        $this->middleware('permission:attendance-create', ['only' => ['create']]);
        $this->middleware('permission:attendance-store', ['only' => ['store']]);
    }
    public function index()
    {
//        return redirect(route('attendance.create'));
        $employee = Employee::orderBy('full_name')->get();
//        return $employee;
        return view('SalaryManager::attendance.index')->with([
            'employees' => $employee
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'att' => 'required',
            'date' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect(route('attendance.index'))
                ->with([
                    'error' => true,
                    'error.title' => 'Error !',
                    'error.message' => 'Please select employees'
                ])
                ->withInput();
        }


        $emp = $request->att;
        $dt = $request->date;
        $dte = strtotime($request->date);


        $employeef = null;
        $employee = null;

        foreach ($emp as $att) {
            $attendance = Attendance::where('employee_id', '=', $att)->where('date', '=', date('Y-m-d', $dte))->count();
            if ($attendance > 0) {
                $employeef[] = Employee::where('id', '=', $att)->first();
            } else {
                $employee[] = Employee::where('id', '=', $att)->first();
            }
        }

        return view('SalaryManager::attendance.create')->with([
            'employees' => $employee,
            'employeef' => $employeef,
            'date' => $dt
        ]);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
// return $request;

        $emp = $request->empId;
        $wrkh = $request->wrk_hrs;
        $oth = $request->ot_hrs;
        $otr = $request->ot_rate;
        $date = strtotime($request->dte);
//        date('Y-m-d',$date);

//   return $wrkh;



        foreach ($emp as $key => $id) {
            $wrkrate = Employee::find($id)->work_rate;

            $att =  Attendance::firstOrCreate(['employee_id'=>$id,'date'=>date('Y-m-d', $date)],[
                'work_hours'=>$wrkh[$key],
                'work_rate'=>$wrkrate,
                'work_total'=>($wrkh[$key] * $wrkrate),
                'created_by'=>Auth::user()->id,
            ]);
        }

        return redirect(route('attendance.index'))->with([
            'success' => true,
            'success.title' => 'Success !',
            'success.message' => 'attendance for ' . $request->dte . ' has been added.'
        ]);


    }

    /**
     * Display the specified resource.
     *
     * @param  \EmployeeManager\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Attendance $employee)
    {
        return view('SalaryManager::attendance.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \EmployeeManager\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Attendance $employee)
    {
        return view('SalaryManager::attendance.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \EmployeeManager\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Attendance $employee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \EmployeeManager\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attendance $employee)
    {
        $employee->delete();
        if ($employee->trashed()) {
            return 'true';
        } else {
            return 'false';
        }

    }

    public function search(Request $request)
    {
        return $request;
        return $raw = RawMaterial::where('raw_material_id', '=', $request->id)->first();

    }
}
