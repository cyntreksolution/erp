<?php

namespace SalaryManager\Controllers;

use ConfigManager\Models\Config;
use DB;
use Illuminate\Support\Facades\Auth;
use Response;
use SalaryManager\Models\Allowance;
use SalaryManager\Models\Attendance;
use SalaryManager\Models\Salary;
use EmployeeManager\Models\Employee;
use SalaryManager\Models\SPayment;
use Sentinel;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SalaryController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:salary-index', ['only' => ['index','jsonList']]);
        $this->middleware('permission:salary-list', ['only' => ['list']]);
        $this->middleware('permission:salary-create', ['only' => ['create','store']]);


    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        return redirect(route('attendance.create'));
        $employee = Employee::orderBy('full_name')->get();
//        return $employee;
        return view('SalaryManager::salary.index')->with([
            'employees' => $employee
        ]);

    }

    public function jsonList(Request $request)
    {
//        return $request;
        $month = substr($request->date, 1, 1);
        $year = substr($request->date, 3, 4);


//        $aa=date('Y-m', $request->date);
//
//        $post = Attendance::whereYear('date', '=', date('YY',$date))
//            ->whereMonth('date', '=',date('mm',$date))
//            ->first();

//return $post[1];
        if ($request->ajax()) {
            $data = Attendance::with('employee.allowance')
                ->where('status', '=', '0')
                ->select('attendance.*', DB::raw("SUM(work_hours) as wrk_hrs"), DB::raw("SUM(ot_total) as ot_tot"), DB::raw("SUM(ot_hours) as ot_hrs"), DB::raw("SUM(work_total) as wrk_tot"))
                ->whereMonth('date', $month)
                ->whereYear('date', $year)
                ->groupBy('employee_id')
                ->get();
//           return $data;
            $hrs = Config::first();

            $jsonList = array();
            $i = 1;

            foreach ($data as $key => $item) {
//                return $item;
//             return   count($item->date);
                $adv = Allowance::where('employee_id', '=', $item->employee_id)
                    ->whereMonth('date', $month)
                    ->whereYear('date', $year)
                    ->get();
                $allowance = $adv->sum('amount');

//              return  $item['employee']['allowance']->sum('amount');
                $dd = array();

                if (($item['employee']->basic) >= $item->wrk_tot) {
                    array_push($dd, $i);
                    array_push($dd, '<input class="check" name="empId[]" type="checkbox"
                                                               data-plugin="switchery" data-size="small"
                                                               data-switchery="true"
                                                               value="' . $item->employee_id . '">');


                    array_push($dd, '<font color="red"> <div class="text-left">' . $item['employee']->full_name . '</div></font><input name="name[]" type="hidden" value="' . $item['employee']->full_name . '">');
                    $days = $item->wrk_hrs / $hrs->hpd;
                    array_push($dd, '<font color="red">' . $days . '</font><input name="days[]" type="hidden" value="' . $days . '">');

                    array_push($dd, '<font color="red">' . $item->ot_hrs . '</font><input name="ot_hrs[]" type="hidden" value="' . $item->ot_hrs . '">');

                    array_push($dd, '<font color="red">' . $item->ot_tot . '</font><input name="ot_total[]" type="hidden" value="' . $item->ot_tot . '">');

                    if (is_null($item['employee']->basic)) {
                        array_push($dd, '<font color="red">' . '-' . '</font><input name="basic[]" type="hidden" value="' . $item['employee']->basic . '">');
                    } else {
                        array_push($dd, '<font color="red">' . $item['employee']->basic . '</font><input name="basic[]" type="hidden" value="' . $item['employee']->basic . '">');
                    }
                    $incentive = 0;
                    if (($item['employee']->basic) >= $item->wrk_tot) {
                        $incentive = 0;
                    } else {
                        $incentive = $item->wrk_tot - $item['employee']->basic;
                    }
                    array_push($dd, '<font color="red">' . $incentive . '</font><input name="incentives[]" type="hidden" value="' . $incentive . '">');

                    $gross = $incentive + $item['employee']->basic + $item->ot_tot;
                    array_push($dd, '<font color="red">' . $gross . '</font><input name="gross_salary[]" type="hidden" value="' . $gross . '">');

                    $epf = ($item['employee']->epf_rate * $item['employee']->basic) / 100;
                    array_push($dd, '<font color="red">' . '-' . $epf . '</font><input name="epf[]" type="hidden" value="' . $epf . '">');

                    $etf = ($item['employee']->etf_rate * $item['employee']->basic) / 100;
                    array_push($dd, '<font color="red">' . '-' . $etf . '</font><input name="etf[]" type="hidden" value="' . $etf . '">');  //etf    - etf rate*basic

                    if (is_null($item['employee']['allowance'])) {
                        array_push($dd, '<font color="red">' . '-' . '</font>');
                    } else {
                        array_push($dd, '<font color="red">' . '-' . $allowance . '</font><input name="allowance[]" type="hidden" value="' . $allowance . '">');
                    }

                    $net = $gross - ($epf + $etf + $allowance);
                    array_push($dd, '<font color="red">' . $net . '</font><input name="net_salary[]" type="hidden" value="' . $net . '">');

                    array_push($dd, '<a href="salary/edit/' . $item->employee_id. '"  class="change btn btn-md btn-success" name="change" value="' . $item->employee_id . '">
                   <i class="material-icons">Change</i></a>');

                    array_push($dd, '<button id="a"  class="create btn btn-md btn-primary" name="create"  value="' . $item->employee_id . '">
                   <i class="material-icons">Create</i></button>');
                    array_push($jsonList, $dd);
                    $i++;
                }


                else

                    {

                    array_push($dd, $i);
                    array_push($dd, '<input class="check" name="empId[]" type="checkbox"
                                                               data-plugin="switchery" data-size="small"
                                                               data-switchery="true"
                                                               value="' . $item->employee_id . '" checked>');
                    array_push($dd, '<div class="text-left">' . $item['employee']->full_name . '</div><input name="name[]" type="hidden" value="' . $item['employee']->full_name . '">');
                    $days = $item->wrk_hrs / $hrs->hpd;
                    array_push($dd, $days . '<input name="days[]" type="hidden" value="' . $days . '">');


                    array_push($dd, $item->ot_hrs . '<input name="ot_hrs[]" type="hidden" value="' . $item->ot_hrs . '">');

                    array_push($dd, $item->ot_tot . '<input name="ot_total[]" type="hidden" value="' . $item->ot_tot . '">');

                    if (is_null($item['employee']->basic)) {
                        array_push($dd, '-');
                    } else {
                        array_push($dd, $item['employee']->basic . '<input name="basic[]" type="hidden" value="' . $item['employee']->basic . '">');
                    }
//                    $incentive = 0;
                    if (($item['employee']->basic) >= $item->wrk_tot) {
                        $incentive = 0;
                    } else {
                        $incentive = $item->wrk_tot - $item['employee']->basic;
                    }
                    array_push($dd, $incentive . '<input name="incentives[]" type="hidden" value="' . $incentive . '">');

                    $gross = $incentive + $item['employee']->basic + $item->ot_tot;
                    array_push($dd, $gross . '<input name="gross_salary[]" type="hidden" value="' . $gross . '">');

                    $epf = ($item['employee']->epf_rate * $item['employee']->basic) / 100;
                    array_push($dd, '-' . $epf . '<input name="epf[]" type="hidden" value="' . $epf . '">');

                    $etf = ($item['employee']->etf_rate * $item['employee']->basic) / 100;
                    array_push($dd, '-' . $etf . '<input name="etf[]" type="hidden" value="' . $etf . '">');  //etf    - etf rate*basic

                    if (is_null($item['employee']['allowance'])) {
                        array_push($dd, '-');
                    } else {
                        array_push($dd, '-' . $allowance . '<input name="allowance[]" type="hidden" value="' . $allowance . '">');
                    }

                    $net = $gross - ($epf + $etf + $allowance);
                    array_push($dd, $net . '<input name="net_salary[]" type="hidden" value="' . $net . '">');

                    array_push($dd, '<a href="salary/add/'. $item->employee_id . '" id="a" onclick="getId('. $item->employee_id . ')" class="change btn btn-md btn-success" name="change" >
                   <i class="material-icons">Change</i></a>');

                    array_push($dd, '<button id="a"  class="create btn btn-md btn-primary" name="create"  value="' . $item->employee_id . '">
                   <i class="material-icons">Create</i></button>');
                    array_push($jsonList, $dd);
                    $i++;
                }

            }
            return Response::json(array('data' => $jsonList));
        } else {
            return Response::json(array('data' => []));
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'att' => 'required',
            'date' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect(route('salary.index'))
                ->with([
                    'error' => true,
                    'error.title' => 'Error !',
                    'error.message' => 'Please select employees'
                ])
                ->withInput();
        }


        $emp = $request->att;
        $dt = $request->date;
        $dte = strtotime($request->date);


        $employeef = null;
        $employee = null;

        foreach ($emp as $att) {
            $attendance = Attendance::where('emp_id', '=', $att)->where('date', '=', date('Y-m-d', $dte))->count();
            if ($attendance > 0) {
                $employeef[] = Employee::where('id', '=', $att)->first();
            } else {
                $employee[] = Employee::where('id', '=', $att)->first();
            }
        }

        return view('SalaryManager::salary.create')->with([
            'employees' => $employee,
            'employeef' => $employeef,
            'date' => $dt
        ]);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $request;

        $month = substr($request->date, 0, 2);
        $year = substr($request->date, 3, 4);

        $date = $request->date;
        $emp = $request->empId;
        $oth = $request->ot_hrs;
        $ot_tot = $request->ot_total;
        $basic = $request->basic;
        $incentive = $request->incentives;
        $gross = $request->gross_salary;
        $epf = $request->epf;
        $etf = $request->etf;
        $allowance = $request->allowance;
        $net = $request->net_salary;
        $date = strtotime($request->date);
//        date('Y-m-d',$date);

//   return $wrkh;

        foreach ($emp as $key => $id) {
            $pay = new SPayment();
            $pay->employee_id = $id;
            $pay->month = $request->date;
            $pay->work_days = $request->days[$key];
            $pay->ot_hours = $oth[$key];
            $pay->ot_total = $ot_tot[$key];
            $pay->basic = $basic[$key];
            $pay->incentives = $incentive[$key];
            $pay->gross_salary = $gross[$key];
            $pay->epf = $epf[$key];
            $pay->etf = $etf[$key];
            $pay->allowances = $allowance[$key];
            $pay->net_salary = $net[$key];
            $pay->created_by = Auth::user()->id;
            $pay->save();

            $att = Attendance::where('status', '=', '0')
                ->where('employee_id', '=', $id)
                ->whereMonth('date', $month)
                ->whereYear('date', $year)
                ->get();

            foreach ($att as $dat) {
                $dat->status = 1;
                $dat->save();
            }
        }

        return redirect(route('salary.index'))->with([
            'success' => true,
            'success.title' => 'Success !',
            'success.message' => 'payment for ' . $request->date . ' has been added.'
        ]);


    }

    /**
     * Display the specified resource.
     *
     * @param  \EmployeeManager\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Salary $employee)
    {
        return view('SalaryManager::salary.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \EmployeeManager\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Salary $employee)
    {
return $employee;
//        return view('SalaryManager::salary.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \EmployeeManager\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Salary $employee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \EmployeeManager\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Salary $employee)
    {
//        $employee->delete();
//        if ($employee->trashed()) {
//            return 'true';
//        } else {
//            return 'false';
//        }

    }

    public function search(Request $request)
    {
//        return $request;
//        return $raw = RawMaterial::where('raw_material_id', '=', $request->id)->first();

    }

    public function change(Request $request)
    {
        return $request;
        return $this->jsonList();
//        return $raw = RawMaterial::where('raw_material_id', '=', $request->id)->first();
//        return $net=Attendance::

    }
}
