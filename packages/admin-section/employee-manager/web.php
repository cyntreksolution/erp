<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web','auth'])->group(function () {
    Route::prefix('employee')->group(function () {
        Route::post('/salarydata', 'EmployeeManager\Controllers\SalaryDataController@salarydata')->name('salary.data');

        Route::get('', 'EmployeeManager\Controllers\EmployeeController@index')->name('employee.index');

        Route::get('create', 'EmployeeManager\Controllers\EmployeeController@create')->name('employee.create');

        Route::get('{employee}', 'EmployeeManager\Controllers\EmployeeController@show')->name('employee.show');

        Route::get('{employee}/edit', 'EmployeeManager\Controllers\EmployeeController@edit')->name('employee.edit');



        Route::post('', 'EmployeeManager\Controllers\EmployeeController@store')->name('employee.store');

        Route::put('{employee}', 'EmployeeManager\Controllers\EmployeeController@update')->name('employee.update');

        Route::delete('{employee}', 'EmployeeManager\Controllers\EmployeeController@destroy')->name('employee.destroy');
    });

    Route::prefix('emp/team')->group(function () {
        Route::get('', 'EmployeeManager\Controllers\EmployeeTeamController@index')->name('team.index');

        Route::get('/activate/{group}', 'EmployeeManager\Controllers\EmployeeTeamController@activate')->name('team.activate');
        Route::get('/deactivate/{group}', 'EmployeeManager\Controllers\EmployeeTeamController@deactivate')->name('team.deactivate');

        Route::get('create', 'EmployeeManager\Controllers\EmployeeTeamController@create')->name('team.create');

        Route::get('{employee}', 'EmployeeManager\Controllers\EmployeeTeamController@show')->name('team.show');

        Route::get('{employee}/edit', 'EmployeeManager\Controllers\EmployeeTeamController@edit')->name('team.edit');



        Route::post('', 'EmployeeManager\Controllers\EmployeeTeamController@store')->name('team.store');

        Route::put('{employee}', 'EmployeeManager\Controllers\EmployeeTeamController@update')->name('team.update');

        Route::delete('{employee}', 'EmployeeManager\Controllers\EmployeeTeamController@destroy')->name('team.destroy');

    });
});
//Route::post('/salarydata', 'EmployeeManager\Controllers\SalaryDataController@salarydata');

Route::get('employee/get/employee', 'EmployeeManager\Controllers\EmployeeController@getemployee')->name('team.edit');
Route::post('employee/get/groupx', 'EmployeeManager\Controllers\EmployeeController@getGroup')->name('team.group');
Route::get('employee/team/table/data', 'EmployeeManager\Controllers\EmployeeTeamController@tableData')->name('employee-team.table');
Route::get('employees/team/data', 'EmployeeManager\Controllers\EmployeeTeamController@employeeTeamData')->name('employee-team.data');
