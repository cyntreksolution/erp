@extends('layouts.back.master')@section('title','Employee')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.wizard.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/css/style.css')}}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>

    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>

    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.cs')}}s">
    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }
    </style>
@stop
@section('content')
    <div class="app-wrapper">
        <div class="app-panel" id="app-panel">
            <div class="app-search">
                <input type="search" class="search-field" placeholder="Search" disabled>
                <i class="search-icon fa fa-search"></i>
            </div>
            <div class="app-panel-inner">
                <div class="scroll-container">
                    <div class="p-3">
                        <button class="btn btn-success py-3 btn-block btn-lg" data-toggle="modal"
                                data-target="#projects-task-modal">NEW EMPLOYEE
                        </button>
                        <hr>
                        <button class="btn btn-warning py-3 btn-block btn-lg" data-toggle="modal"
                                data-target="#salary-data">SALARY DATA
                        </button>
                    </div>
                </div>
            </div>
            <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show"><i
                        class="fa fa-chevron-right"></i>
                <i class="fa fa-chevron-left"></i>
            </a>
        </div>
        <!-- /.app-panel -->
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">EMPLOYEES</h5>
            </div><!-- /.app-main-header -->
            <div class="app-main-content">
                <div class="scroll-container">
                    <div class="media-list ">
                        @foreach($employees as $employee)
                            <!--a href="{{--$employee->id.'/edit'--}}"-->
                                <div class="media">
                                    @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                    <div class="avatar avatar-circle avatar-md project-icon bg-{{$color[$employee->color]}}"
                                         data-plugin="firstLitter"
                                         data-target="#{{$employee->id*754}}"></div>
                                    <div class="media-body">
                                        <h6 class="project-name"
                                            id="{{$employee->id*754}}">{{$employee->full_name}}</h6>
                                        <small class="project-detail">{{$employee['designation']->name}}</small>
                                        {{--@foreach($employee->designation as $des)--}}
                                        {{--<small class="project-detail">{{$des->name}}</small>--}}
                                        {{--@endforeach--}}
                                    </div>
                                    <div class="section-2">



                                        <a class="btn btn-md  btn-info text-center"
                                                 href=" {{route('employee.edit',$employee->id)}}">Show Details</a>

                                        <!--a class="btn btn-md  btn-warning text-center" data-toggle="modal"
                                           data-target="#salary-data">Salary Data</a-->

                                        <!--button class="btn btn-warning py-2 btn-block btn-sm" data-toggle="modal"
                                                data-target="#salary-data">Add Salary Data
                                        </button-->
                                        <!--button class="delete btn btn-light btn-sm ml-2" value="{{$employee->id}}"
                                                data-toggle="tooltip"
                                                data-placement="left" title="DELETE EMPLOYEE">
                                            <i class="zmdi zmdi-close"></i>
                                        </button-->
                                    </div>
                                </div>
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="projects-task-modal" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'employee','enctype'=>'multipart/form-data','id'=>'bootstrap-wizard-form'))}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="wizard p-4" id="bootstrap-wizard-1">
                                <ul class="nav nav-tabs vertices keep-prefix-suffix" role="tablist">
                                    <li class="nav-item">
                                        <a class=" nav-link active" data-toggle="tab" href="#ex5-step-1"
                                           role="tab">
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#ex5-step-2"
                                           role="tab">
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#ex5-step-3"
                                           role="tab">
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="ex5-step-1" role="tabpanel">
                                        <div class="task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="fname"
                                                   placeholder="First Name">
                                        </div>
                                        <hr>
                                        <div class="task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="lname"
                                                   placeholder="Last Name">
                                        </div>
                                        <hr>
                                        <div class="task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="fullname"
                                                   placeholder="Full Name">
                                        </div>
                                        <hr>
                                        <div class="task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="dob"
                                                   placeholder="Date of Birth">
                                        </div>
                                        <hr>
                                        <div class="task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="nic"
                                                   placeholder="NIC">
                                        </div>
                                        <hr>
                                        <div class="task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="file" name="image"
                                                   placeholder="Material Name">
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="ex5-step-2" role="tabpanel">
                                        <div class="task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="mobile"
                                                   placeholder="Mobile Number">
                                        </div>
                                        <hr>
                                        <div class="task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="home"
                                                   placeholder="Home Number">
                                        </div>
                                        <hr>
                                        <div class="task-name-wrap">
                                            <textarea class="task-desc-field" type="text" name="address"
                                                      placeholder="Address"></textarea>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="ex5-step-3" role="tabpanel">
                                        <div class="task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="epf"
                                                   placeholder="EPF No">
                                        </div>
                                        <hr>
                                        <div class="task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="etf"
                                                   placeholder="ETF No">
                                        </div>
                                        <hr>
                                        <div class="task-desc-wrap">
                                            <textarea class="task-desc-field" name="desc"
                                                      placeholder="Description"></textarea>
                                        </div>
                                    </div>

                                </div>

                                <div class="pager d-flex justify-content-center">
                                    <input type="submit" id="finish-btn" class="finish btn btn-success w-50"
                                           value="Finish"/>

                                    <button type="button" id="next-btn" class="next btn btn-success w-50">Next
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {!!Form::close()!!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="salary-data" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'employee/salarydata','enctype'=>'multipart/form-data','id'=>'bootstrap-wizard-form'))}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="wizard p-4" id="bootstrap-wizard-1">
                                <ul class="nav nav-tabs vertices keep-prefix-suffix" role="tablist">
                                    <li class="nav-item">
                                        <a class=" nav-link active" data-toggle="tab" href="#ex5-step-1"
                                           role="tab">
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="ex5-step-1" role="tabpanel">
                                        <div class="task-name-wrap">
                                            <select id="single2" name="eid" class="form-control select2">
                                                <option value=''>Select Employee Name</option>
                                                @foreach($employees as $emp)
                                                    <option value="{{$emp->id}}">{{$emp->full_name}}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                        <div class="task-name-wrap">

                                            <input class="form-control" type="number" name="eregi"
                                                   placeholder="Employee Register Number">
                                        </div>
                                        <div class="task-name-wrap">

                                            <input class="form-control" type="number" name="ebasic"
                                                   placeholder="Basic Salary">
                                        </div>

                                        <div class="task-name-wrap">

                                            <input class="form-control" type="number" name="eact"
                                                   placeholder="Actual Salary Per Day">
                                        </div>

                                        <div class="task-name-wrap">

                                            <input class="form-control" type="number" name="ebudjet"
                                                   placeholder="Budjet Insentive">
                                        </div>

                                        <div class="task-name-wrap">

                                            <input class="form-control" type="number" name="emp_epf"
                                                   placeholder="EPF % - Employee Contribution">
                                        </div>

                                        <div class="task-name-wrap">

                                            <input class="form-control" type="number" name="com_epf"
                                                   placeholder="EPF % - Company Contribution">
                                        </div>

                                        <div class="task-name-wrap">

                                            <input class="form-control" type="number" name="etf"
                                                   placeholder="ETF % - Company Contribution">
                                        </div>


                                        <div class="task-name-wrap">

                                            <input class="form-control" type="number" name="ot"
                                                   placeholder="OT Rate">
                                        </div>

                                        <div class="task-name-wrap">

                                            <input class="form-control" type="number" name="base"
                                                   placeholder="Base Allowance">
                                        </div>

                                        <div class="task-name-wrap">

                                            <input class="form-control" type="number" name="oth"
                                                   placeholder="Other Allowance">
                                        </div>

                                    </div>

                                </div>

                                <div class="pager d-flex justify-content-center">
                                    <input type="submit" id="finish-btn" class="finish btn btn-success w-50"
                                           value="Finish"/>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {!!Form::close()!!}
            </div>
        </div>
    </div>




@stop
@section('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    {{--<script src="{{asset('assets/examples/js/demos/form.wizard.js')}}"></script>--}}
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script>

        function e() {
            return !!$("#bootstrap-wizard-form").valid() || (o.focusInvalid(), !1)
        }

        var o = $("#bootstrap-wizard-form").validate({
            rules: {
                fname: {required: !0},
                lname: {required: !0},
                fullname: {required: !0},
                address: {required: !0},
            },
            messages: {
                fname: "Please enter the first name",
                lname: "Please enter the last name",
                fullname: "Please enter the full name",
                address: "Please enter the address",
            },
            errorElement: "div", errorPlacement: function (e, o) {
                e.addClass("form-control-feedback"), o.closest(".form-group").addClass("has-danger"), "checkbox" === o.prop("type") ? e.insertAfter(o.parent(".checkbox")) : e.insertAfter(o)
            }, highlight: function (e, o, r) {
                $(e).closest(".form-group").addClass("has-danger").removeClass("has-success"), $(e).removeClass("form-control-success").addClass("form-control-danger")
            }, unhighlight: function (e, o, r) {
                $(e).closest(".form-group").addClass("has-success").removeClass("has-danger"), $(e).removeClass("form-control-danger").addClass("form-control-success")
            }
        });
        $("#bootstrap-wizard-1").bootstrapWizard({
            tabClass: "nav-tabs",
            nextSelector: ".pager>.btn.next",
            previousSelector: ".pager>.btn.previous",
            onTabShow: function (e, o, r) {
                $(e).addClass("visited");
                var n = $("#finish-btn"), s = $("#next-btn"), t = o.find("li").length;
                r + 1 == t ? (n.show(), s.hide()) : (n.hide(), s.show())
            },
            onTabClick: function () {
                return e()
            },
            onPrevious: function (e, o, r) {
                $(e).removeClass("visited")
            },
            onNext: function (o, r, n) {
                return e()
            }
        }), $("#finish-btn").on("click", function (o) {
            e() ? swal("Good job!", "Thanks for your time", "success") : swal("Faild", "Please fill in all fields", "error")
        })


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $(".js-example-responsive").select2({
            width: 'resolve' // need to override the changed default
        });


        $('.delete').click(function (e) {
            e.preventDefault();
            id = $(this).attr("value");
            confirmAlert(id);

        });

        function confirmAlert(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "delete",
                        url: id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            }
                            else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

    </script>
@stop