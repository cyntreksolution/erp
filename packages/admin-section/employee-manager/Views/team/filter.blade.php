@extends('layouts.back.master')@section('title','Employee Team')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.wizard.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/css/style.css')}}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>

    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.cs')}}s">
    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .avatar {
            width: 1.5rem;
            height: 1.5rem;
            font-size: 16px;
        }
    </style>
@stop
@section('content')

        {{ Form::open(array('url' => 'employee/team/filter/data','method'=>'POST'))}}
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    {!! Form::text('date',null,['class' => 'form-control','id'=>'date','placeholder'=>'Select Date','autocomplete'=>'off']) !!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <button type="submit" class="btn btn-success">Filter</button>
                </div>
            </div>
        </div>
        {!!Form::close()!!}

    <div class="app-wrapper">
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">EMPLOYEES</h5>
            </div><!-- /.app-main-header -->
            {{ Form::open(array('url' => 'emp/team','enctype'=>'multipart/form-data'))}}
            <div class="app-main-content bg-white py-4 " id="container">
                <div class=" px-2 content">
                    <div class=" px-2">
                        <div class="mail-box">
                            {{--inbox-cat-list  for green bacground--}}

                            <div class="row">
                                @foreach($employees as $employee)
                                    <div class="col-md-4 my-2">
                                        <div class="panel-item media">
                                            @if(isset($employee->image))
                                                <a href="javascript:void(0)" class="avatar avatar-circle avatar-sm">
                                                    <img src="{{asset($employee->image_path.$employee->image)}}"
                                                         alt="Generic placeholder image">
                                                </a>
                                                <div class="avatar avatar avatar-sm">
                                                    <img src="{{asset($employee->image_path.$employee->image)}}" alt="">
                                                </div>
                                            @else
                                                @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                                <a
                                                        class="avatar avatar-circle avatar-md project-icon text-white text-small bg-{{$color[$employee->color]}} "
                                                        data-plugin="firstLitter"
                                                        data-target="#raw-{{$employee->id*874}}">
                                                </a>
                                            @endif

                                            <div class="media-body">
                                                <h5 class="mail-cat-name">
                                                    <h6 id="raw-{{$employee->id*874}}"
                                                        id="">{{$employee->full_name}}</h6>
                                                </h5>
                                            </div>

                                            <a>
                                                <div class=" mr-3">
                                                    <input type="checkbox" class="check" data-plugin="switchery"
                                                           name="employees[]" data-size="small"
                                                           data-switchery="true" style="display: none;"
                                                           value="{{$employee->id}}">
                                                    <label for="mail-item-3">
                                                    </label>

                                                </div>
                                            </a>
                                        </div>

                                    </div>
                                @endforeach
                            </div>

                        </div>
                    </div>

                </div>
            </div>
            <div class="row text-center">
                <div class="col-sm-6">
                    <input type="text" class="form-control float-right" placeholder="Group 01" name="description" required style="text-align: left;" autocomplete="off">
                </div>
                <div class="col-md-6">
                    <input type="submit" class="btn btn-success btn-md" value="Create Group">
                </div>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
@stop
@section('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    {{--<script src="{{asset('assets/examples/js/demos/form.wizard.js')}}"></script>--}}
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>

    <script>
        $(document).ready(function () {
            $('#date').daterangepicker({
                "setDate" : new Date(),
                "showDropdowns": true,
                "timePicker": false,
                "singleDatePicker": true,
                "locale": {
                    "format": "YYYY-MM-DD",
                },
                "minDate": "11/01/2019"
            });
            $('#date').val('')

        });
    </script>
@stop