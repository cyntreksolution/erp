@extends('layouts.back.master')@section('title','Employee Manager')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">

    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/Buttons-1.5.1/css/buttons.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/datatables.net/css/buttons.dataTables.min.css')}}">
    <style type="text/css">
        #floating-button {
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #00C851;
            position: fixed;
            bottom: 80px;
            right: 30px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .plus {
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }

        .pagination {
            display: inline-block;
        }

        .pagination li {
            color: black;
            float: left;
            padding: 8px 16px;
            text-decoration: none;
        }

        .pagination li.active {
            background-color: #4CAF50;
            color: white;
        }

        .pagination li:hover:not(.active) {background-color: #ddd;}
    </style>
@stop
@section('content')

    {{ Form::open(array('url' => 'emp/team','enctype'=>'multipart/form-data'))}}
    <div class="row">
        <div class="col-sm-3">
            <div class="form-group">
                {!! Form::text('date',null,['class' => 'form-control','id'=>'date','placeholder'=>'Select Date','autocomplete'=>'off']) !!}
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <button type="button" class="btn btn-success" onclick="process_form(this)">Filter</button>
            </div>
        </div>
    </div>



    <div class="app-main">
        <div class="app-main-header">
            <h5 class="app-main-heading text-center">Last Employee Active Groups</h5>
        </div>

        <div class="scroll-container" id="scroll-container">
            <div class="app-main-content">
                <div class="container">


                    <div class="">
                        <table id="emp_group_table" class="display text-center">

                            <thead>
                            <tr>
                                <th style="text-align: center;">#</th>
                                <th>Name</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th style="text-align: center;">#</th>
                                <th>Name</th>
                            </tr>
                            </tfoot>

                        </table>

                        <div class="row text-center">
                            <div class="col-sm-6">
                                <input type="text" class="form-control float-right" placeholder="Group 01" name="description" required style="text-align: left;" autocomplete="off">
                            </div>
                            <div class="col-md-6">
                                <input type="submit" class="btn btn-success btn-md" value="Create Group" disabled id="team-add">
                            </div>
                        </div>
                        <div class="row text-center">
                            <div class="col-sm-6">
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>

                </div>
            </div>

        </div>
    </div>

@stop
@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script src="{{asset('assets/vendor/dtable/Buttons-1.5.1/js/dataTables.buttons.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/buttons/1.5.1/js/buttons.jqueryui.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/JSZip-2.5.0/jszip.min.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/pdfmake-0.1.32/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/Buttons-1.5.1/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js')}}"></script>


    <script>
        $(document).ready(function () {

            var counterChecked = 0;

            $('body').on('change', 'input[type="checkbox"]', function() {

                this.checked ? counterChecked++ : counterChecked--;
                counterChecked > 0 ? $('#team-add').prop("disabled", false): $('#team-add').prop("disabled", true);

            });

            $('#date').daterangepicker({
                "showDropdowns": true,
                "timePicker": false,
                "singleDatePicker": true,
                "locale": {
                    "format": "YYYY-MM-DD",
                },
                "minDate": "11/01/2019"
            });
            $('#date').val('')

            table = $('#emp_group_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "scrollY": false,
                "bPaginate": false,
                "ajax": {
                    url: "{{url('/employees/team/data')}}",
                    type: "get",
                    error: function () {

                    }
                },
                columns: [
                    { data: "no" },
                    { data: "full_name" ,"className": "text-left"}
                ],
                pageLength: 100,
                responsive: true
            });
        });
        function process_form(e) {

            let date = $("#date").val();

            table.ajax.url('/employees/team/data?date=' + date).load();
        }
    </script>
@stop