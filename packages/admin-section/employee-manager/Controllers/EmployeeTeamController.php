<?php

namespace EmployeeManager\Controllers;

use Carbon\Carbon;
use EmployeeManager\Models\Employee;
use EmployeeManager\Models\Group;
use Illuminate\Support\Facades\DB;
use Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use OwnerManager\Models\Owner;
use ProductionManagerManager\Models\ProductionManager;
use RecipeManager\Models\Recipe;
use SemiFinishProductManager\Models\SemiFinishProduct;

class EmployeeTeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:employee_team-index', ['only' => ['index','tableData']]);
        $this->middleware('permission:employee_team-create', ['only' => ['create','store']]);
        $this->middleware('permission:employee_team-store', ['only' => ['activate','deactivate']]);
    }
    public function index()
    {
        $date = Carbon::now()->format('Y-m-d');

        $groups = Group::orderBy('status', 'desc')->whereDate('created_at', $date)->paginate(15);
        return view('EmployeeManager::team.index', compact('groups'));
    }


    public function employeeTeamData(Request $request)
    {

        $date = Carbon::now()->format('Y-m-d');
        if(isset($request->date) && !empty($request->date)){
            $date = $request->date;
        }

        $employees = Employee::whereHas('attendance', function ($q)  use($date){
            $q->where('date','like',$date.'%');
        })->orderBy('first_name')->get();
        $i=1;
        $data=array();
        foreach($employees as $e){

            $data[]=[
                'no'=>"<input type='checkbox' class='employee' name='employees[]' value='$e->id'/>",
                'full_name' => $e->full_name
            ];
            $i++;
        }


        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval(sizeof($data)),
            "recordsFiltered" => intval(sizeof($data)),
            "data" => $data
        ];
        return json_encode($json_data);
    }

    public function tableData(Request $request)
    {
        $search = $request->search['value'];
        $date = Carbon::now()->format('Y-m-d');
        if(isset($request->date) && !empty($request->date)){
            $date = $request->date;
        }

        $groups = Group::select('created_at', 'name','description','id')
            ->where('created_at','like', $date. '%')
            ->get();
        $i=1;
        $data=array();
        foreach($groups as $g){

            $employees = DB::table('employee as e')
                ->select('e.full_name')
                ->join("group_employees as ge","ge.employee_id","=","e.id")
                ->where('ge.group_id', $g->id)
                ->get();
            $emp_name='';
            foreach($employees as $e){
                $emp_name.=$e->full_name.' ,';
            }
            $data[]=[
                'no'=>$i,
                'created_at' =>  Carbon::parse($g->created_at)->format('Y-m-d'),
                'name' => $g->name,
                'description' => $g->description,
                'members' =>rtrim($emp_name,',')
            ];
            $i++;
        }


        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval(sizeof($data)),
            "recordsFiltered" => intval(sizeof($data)),
            "data" => $data
        ];
        return json_encode($json_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $date=Carbon::today()->format('Y-m-d');

        $employees = Employee::whereHas('attendance', function ($q)  use($date){
            $q->where('date','like',$date.'%');
        })->orderBy('first_name')->get();
        return view('EmployeeManager::team.create', compact('employees'));
    }

    public function filter(Request $request){
        $date=Carbon::parse($request->date)->format('Y-m-d');

        $employees = Employee::whereHas('attendance', function ($q)  use($date){
            $q->where('date','like',$date.'%');
        })->orderBy('first_name')->get();

        return view('EmployeeManager::team.filter', compact('employees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $employee_ids = $request->employees;
        $group = new Group();
        $group->name = 'G-' . Carbon::now()->format('ymdhms');
        $group->description = $request->description;
        $group->created_at=$request->date;
        $group->save();

        $employee_id_arr =[];
        foreach ($employee_ids as $emp){
            $employee_id_arr[$emp] = ['created_at' => $request->date];
        }

        $group->employees()->sync($employee_id_arr);

        return redirect(route('team.index'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => 'Employee Group has been added'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \EmployeeManager\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        return view('EmployeeManager::employee.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \EmployeeManager\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        return view('EmployeeManager::employee.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \EmployeeManager\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \EmployeeManager\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();
        if ($employee->trashed()) {
            return 'true';
        } else {
            return 'false';
        }

    }

    public function activate(Request $request, $group)
    {
        $group = Group::whereId($group)->withoutGlobalScope('status')->first();
        $members = $group->employees;

        $activeEmployees = [];
        foreach ($members as $member) {
            $employee = Employee::whereId($member->id)->first();
            $groups = $employee->groups;
            foreach ($groups as $group) {
                $status = ($group->status == 1) ? true : false;
                if ($status) {
                    array_push($activeEmployees, $employee->first_name);
                }
            }
        }

        if (!empty($activeEmployees) && sizeof($activeEmployees) > 0) {
            return redirect(route('team.index'))->with([
                'error' => true,
                'error.title' => 'Sorry !',
                'error.message' => 'Some Employees Already In a Active Group ex: ' . $activeEmployees[0]
            ]);
        }

        $group->status = 1;
        $group->save();
        return redirect(route('team.index'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => 'Employee Group has been Activated'
        ]);
    }

    public function deactivate(Request $request, Group $group)
    {
        $group->status = 0;
        $group->save();
        return redirect(route('team.index'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => 'Employee Group has been Deactivated'
        ]);
    }
}
