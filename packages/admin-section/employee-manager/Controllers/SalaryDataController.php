<?php
namespace EmployeeManager\Controllers;

use EmployeeManager\Models\Employee;
use EmployeeManager\Models\Group;
use EmployeeManager\Models\SalaryData;
use Illuminate\Support\Facades\DB;
use Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use OwnerManager\Models\Owner;
use ProductionManagerManager\Models\ProductionManager;
use RecipeManager\Models\Recipe;
use SemiFinishProductManager\Models\SemiFinishProduct;

class SalaryDataController extends Controller
{

    public function salarydata(Request $request)
    {
        $emp = new SalaryData();
        //dd($request);
        $emp->emp_id = $request->eid;
        $emp->emp_regi_no = $request->eregi;
        $emp->emp_basic = $request->ebasic;
        $emp->emp_actual = $request->eact;
        $emp->emp_budjet = $request->ebudjet;
        $emp->emp_epf = $request->emp_epf;
        $emp->com_epf = $request->com_epf;
        $emp->etf = $request->etf;
        $emp->ot = $request->ot;
        $emp->base = $request->base;
        $emp->oth = $request->oth;

        //dd($emp);
        $emp->save();
        $employee = Employee::all();
        return view('EmployeeManager::employee.create')->with([
            'employees' => $employee
        ]);
    }

}
