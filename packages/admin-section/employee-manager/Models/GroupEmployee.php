<?php

namespace EmployeeManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use UserManager\Models\Users;

class GroupEmployee extends Model
{
    protected $dates = ['deleted_at'];
}
