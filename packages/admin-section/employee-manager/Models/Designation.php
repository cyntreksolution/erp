<?php

namespace EmployeeManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use UserManager\Models\Users;

class Designation extends Model
{
    use SoftDeletes;

    protected $table = 'designation';


    protected $dates = ['deleted_at'];

    public function designation()
    {
        return $this->hasOne('EmployeeManager\Models\Employee');
    }

}
