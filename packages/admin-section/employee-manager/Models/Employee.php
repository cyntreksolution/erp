<?php

namespace EmployeeManager\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use SalaryManager\Models\Attendance;
use UserManager\Models\Users;

class Employee extends Model
{
    use SoftDeletes;

    protected $table = 'employee';

    protected $dates = ['deleted_at'];

    public function designation()
    {
        return $this->belongsTo('EmployeeManager\Models\Designation');
    }

    public function allowance()
    {
        return $this->hasMany('SalaryManager\Models\Allowance');
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'group_employees');
    }

    public function scopeNotInGroup($query)
    {
        return $query->doesntHave('groups');
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('sort', function (Builder $builder) {
            $builder->orderBy('first_name', 'asc');
        });
    }


    public function attendance(){
        return $this->hasMany(Attendance::class);
    }


}
