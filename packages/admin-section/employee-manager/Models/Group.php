<?php

namespace EmployeeManager\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use UserManager\Models\Users;

class Group extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('status', function (Builder $builder) {
            $builder->where('status', '=', 1);
        });
    }

    public function employees()
    {
        return $this->belongsToMany(Employee::class, 'group_employees')->withPivot('created_at');
    }
}
