<?php

namespace AssistantManager\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use UserManager\Models\Users;

class Assistant extends Users
{
    use SoftDeletes;

    protected $table = 'users';

    protected $primaryKey = 'id';

    protected $dates = ['deleted_at'];
}
