<?php

namespace AssistantManager;

use Illuminate\Support\ServiceProvider;

class AssistantProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/Views', 'AssistantManager');
        require __DIR__ . '/web.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('AssistantManager', function($app){
            return new AssistantManager;
        });
    }
}
