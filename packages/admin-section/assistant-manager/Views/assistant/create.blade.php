@extends('layouts.back.master')@section('title','Assistant')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/packages/assistant/assistant.css')}}"/>

@stop
@section('content')
    <div class="app-wrapper">
        <div class="app-panel" id="app-panel">
            <div class="app-search">
                <input type="search" class="search-field" placeholder="Search" disabled>
                <i class="search-icon fa fa-search"></i>
            </div>
            <div class="app-panel-inner">
                <div class="scroll-container">
                    <div class="p-3">
                        <button class="btn btn-success py-3 btn-block btn-lg" data-toggle="modal"
                                data-target="#projects-task-modal">ADD NEW ASSISTANT
                        </button>
                    </div>
                </div>
            </div>
            <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show"><i
                        class="fa fa-chevron-right"></i>
                <i class="fa fa-chevron-left"></i>
            </a>
        </div>
        <!-- /.app-panel -->
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">ASSISTANTS</h5>
            </div><!-- /.app-main-header -->
            <div class="app-main-content">
                <div class="scroll-container">
                    <div class="media-list">
                        @foreach($assistants as $assistant)
                            <a href="{{$assistant->id}}">
                                <div class="media">
                                    @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                    <div class="avatar avatar-circle avatar-md project-icon bg-{{$color[$assistant->color]}}"
                                         data-plugin="firstLitter"
                                         data-target="#{{$assistant->id*754}}"></div>
                                    <div class="media-body">
                                        <h6 class="project-name"
                                            id="{{$assistant->id*754}}">{{$assistant->first_name.' '.$assistant->last_name}}</h6>
                                        @foreach($assistant->roles as $role)
                                            <small class="project-detail">{{$role->name}}</small>
                                        @endforeach
                                    </div>
                                    <div class="section-2">
                                        <button class="delete btn btn-light btn-sm ml-2" value="{{$assistant->id}}"
                                                data-toggle="tooltip"
                                                data-placement="left" title="DELETE ASSISTANT">
                                            <i class="zmdi zmdi-close"></i>
                                        </button>
                                    </div>
                                </div>
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

 <div class="modal fade" id="projects-task-modal" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'assistant','id'=>'jq-validation-example-1')) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="fn" type="text" class="task-name-field" placeholder="first name">
                        </div>
                        <hr>
                        <div class="row task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="ln" type="text" class="task-name-field" placeholder="last name">
                        </div>

                    <hr>
                    <div class="row">
                        <div class="task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="email" type="text" class="task-name-field" placeholder="email">
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="nic" type="text" class="task-name-field" placeholder="nic">
                        </div>
                    </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/packages/assistant/assistant.js')}}"></script>
@stop