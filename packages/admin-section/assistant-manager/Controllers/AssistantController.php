<?php

namespace AssistantManager\Controllers;

use App\User;
use AssistantManager\Models\Assistant;
use Illuminate\Support\Facades\Auth;
use Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use StockManager\Models\Stock;

class AssistantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect(route('assistant.create'));
        return view('AssistantManager::assistant.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $assistant = Assistant::whereHas('roles', function ($query) {
            $query->where('slug', 'like', 'assistant');
        })->get();
        return view('AssistantManager::assistant.create')->with(['assistants' => $assistant]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $credentials = [
            'first_name' => $request->fn,
            'last_name' => $request->ln,
            'email' => $request->email,
            'password' => $request->nic,
            'created_by' => Auth::user()->id,
            'color' => rand() % 7,
        ];
        $user = Sentinel::registerAndActivate($credentials);
        $role = Sentinel::findRoleBySlug('assistant');
        $role->users()->attach($user);

        return redirect(route('assistant.create'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => $request->fn.' '.$request->ln.' has been added as an Buntalk Assistant.'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \AssistantManager\Models\Assistant $assistant
     * @return \Illuminate\Http\Response
     */
    public function show(Assistant $assistant)
    {

        return view('AssistantManager::assistant.show')->with([
            'assistant' => $assistant,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \AssistantManager\Models\Assistant $assistant
     * @return \Illuminate\Http\Response
     */
    public function edit(Assistant $assistant)
    {
        return view('AssistantManager::assistant.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \AssistantManager\Models\Assistant $assistant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Assistant $assistant)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \AssistantManager\Models\Assistant $assistant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Assistant $assistant)
    {
        $assistant->delete();
        if ($assistant->trashed()) {
            return 'true';
        } else {
            return 'false';
        }

    }
}
