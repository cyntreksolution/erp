<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web','log'])->group(function () {
    Route::prefix('assistant')->group(function () {
        Route::get('', 'AssistantManager\Controllers\AssistantController@index')->name('assistant.index');

        Route::get('create', 'AssistantManager\Controllers\AssistantController@create')->name('assistant.create');

        Route::get('{assistant}', 'AssistantManager\Controllers\AssistantController@show')->name('assistant.show');

        Route::get('{assistant}/edit', 'AssistantManager\Controllers\AssistantController@edit')->name('assistant.edit');


        Route::post('', 'AssistantManager\Controllers\AssistantController@store')->name('assistant.store');

        Route::put('{assistant}', 'AssistantManager\Controllers\AssistantController@update')->name('assistant.update');

        Route::delete('{assistant}', 'AssistantManager\Controllers\AssistantController@destroy')->name('assistant.destroy');
    });
});
