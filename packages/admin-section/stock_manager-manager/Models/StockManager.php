<?php

namespace StockManagerManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use UserManager\Models\Users;

class StockManager extends Users
{
    use SoftDeletes;

    protected $table = 'users';

    protected $primaryKey = 'id';

    protected $dates = ['deleted_at'];
}
