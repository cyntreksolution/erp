<?php

namespace StockManagerManager\Controllers;

use Illuminate\Support\Facades\Auth;
use Sentinel;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use StockManager\Models\Stock;
use StockManagerManager\Models\StockManager;
use UserManager\Models\Users;

class StockManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect(route('stock_manager.create'));
        return view('StockManagerManager::stock_manager.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $stock_manager = StockManager::whereHas('roles', function ($query) {
            $query->where('slug', 'like', 'stock-manager');
        })->get();
        return view('StockManagerManager::stock_manager.create')->with(['sms'=>$stock_manager]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::transaction(function () use ($request) {
            $credentials = [
                'first_name' => $request->fn,
                'last_name' => $request->ln,
                'email' => $request->email,
                'password' => $request->nic,
                'created_by' => Auth::user()->id,
                'color' => rand() % 7,
            ];
            $user = Sentinel::registerAndActivate($credentials);
            $role = Sentinel::findRoleBySlug('stock-manager');
            $role->users()->attach($user);

        });

        return redirect(route('stock_manager.create'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => $request->fn . ' ' . $request->ln . ' has been added as an Buntalk Stock Manager.'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \StockManagerManager\Models\StockManager $stock_manager
     * @return \Illuminate\Http\Response
     */
    public function show(StockManager $stock_manager)
    {
        return view('StockManagerManager::stock_manager.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \StockManagerManager\Models\StockManager $stock_manager
     * @return \Illuminate\Http\Response
     */
    public function edit(StockManager $stock_manager)
    {
        return view('StockManagerManager::stock_manager.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \StockManagerManager\Models\StockManager $stock_manager
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StockManager $stock_manager)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \StockManagerManager\Models\StockManager $stock_manager
     * @return \Illuminate\Http\Response
     */
    public function destroy(StockManager $stock_manager)
    {
        $stock_manager->delete();
        if ($stock_manager->trashed()) {
            return 'true';
        }else{
            return 'false';
        }

    }
}
