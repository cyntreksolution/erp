<?php

namespace StockManagerManager;

use Illuminate\Support\ServiceProvider;

class StockManagerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/Views', 'StockManagerManager');
        require __DIR__ . '/web.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('StockManagerManager', function($app){
            return new StockManagerManager;
        });
    }
}
