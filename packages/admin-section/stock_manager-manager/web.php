<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web','log'])->group(function () {
    Route::prefix('stock_manager')->group(function () {
        Route::get('', 'StockManagerManager\Controllers\StockManagerController@index')->name('stock_manager.index');

        Route::get('create', 'StockManagerManager\Controllers\StockManagerController@create')->name('stock_manager.create');

        Route::get('{stock_manager}', 'StockManagerManager\Controllers\StockManagerController@show')->name('stock_manager.show');

        Route::get('{stock_manager}/edit', 'StockManagerManager\Controllers\StockManagerController@edit')->name('stock_manager.edit');


        Route::post('', 'StockManagerManager\Controllers\StockManagerController@store')->name('stock_manager.store');

        Route::put('{stock_manager}', 'StockManagerManager\Controllers\StockManagerController@update')->name('stock_manager.update');

        Route::delete('{stock_manager}', 'StockManagerManager\Controllers\StockManagerController@destroy')->name('stock_manager.destroy');
    });
});
