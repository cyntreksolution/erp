<?php

namespace ConfigManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use UserManager\Models\Users;

class Config extends Model
{
    use SoftDeletes;

    protected $table='config';

    protected $dates = ['deleted_at'];


}
