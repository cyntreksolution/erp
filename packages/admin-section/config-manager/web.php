<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web','log'])->group(function () {
    Route::prefix('config')->group(function () {
        Route::get('/', 'ConfigManager\Controllers\ConfigController@index')->name('config.index');

        Route::get('create', 'ConfigManager\Controllers\ConfigController@create')->name('config.create');

        Route::get('{employee}', 'ConfigManager\Controllers\ConfigController@show')->name('config.show');

        Route::get('{employee}/edit', 'ConfigManager\Controllers\ConfigController@edit')->name('config.edit');

        Route::get('get/employee', 'ConfigManager\Controllers\ConfigController@getemployee')->name('config.edit');


        Route::post('', 'ConfigManager\Controllers\ConfigController@store')->name('employee.store');

        Route::put('{employee}', 'ConfigManager\Controllers\ConfigController@update')->name('config.update');

        Route::delete('{employee}', 'ConfigManager\Controllers\ConfigController@destroy')->name('config.destroy');
    });
});
