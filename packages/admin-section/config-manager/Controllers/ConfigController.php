<?php

namespace ConfigManager\Controllers;

use EmployeeManager\Models\Config;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('ConfigManager::config.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('ConfigManager::config.create')->with([

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if (isset($request->image)) {
            $file = $request->file('image');
            $path = '/uploads/images/raw_materials/';
            $destinationPath = storage_path($path);
            $file->move($destinationPath, $file->getClientOriginalName());

            $emp = new Employee();
            $emp->first_name = $request->fname;
            $emp->last_name = $request->lname;
            $emp->full_name = $request->fullname;
            $emp->desc = $request->desc;
            $emp->mobile = $request->mobile;
            $emp->home = $request->home;
            $emp->address = $request->address;
            $emp->birthday = $request->dob;
            $emp->epf_no = $request->epf;
            $emp->etf_no = $request->etf;
            $emp->image = $file->getClientOriginalName();
            $emp->image_path = 'storage' . $path;
            $emp->color = rand() % 7;
            $emp->created_by = Auth::user()->id;
            $emp->save();
            return redirect(route('employee.create'))->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => $request->fname . ' ' . $request->lname . ' has been added as an Buntalk Employee.'
            ]);
        } else {
            $emp = new Employee();
            $emp->designation_id = 1;
            $emp->first_name = $request->fname;
            $emp->last_name = $request->lname;
            $emp->full_name = $request->fullname;
            $emp->desc = $request->desc;
            $emp->mobile = $request->mobile;
            $emp->home = $request->home;
            $emp->address = $request->address;
            $emp->birthday = $request->dob;
            $emp->epf_no = $request->epf;
            $emp->etf_no = $request->etf;
            $emp->color = rand() % 7;
            $emp->created_by = Auth::user()->id;
            $emp->save();
            return redirect(route('employee.create'))->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => $request->fname . ' ' . $request->lname. ' has been added as an Buntalk Employee.'
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \EmployeeManager\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        return view('EmployeeManager::employee.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \EmployeeManager\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        return view('EmployeeManager::employee.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \EmployeeManager\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \EmployeeManager\Models\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();
        if ($employee->trashed()) {
            return 'true';
        } else {
            return 'false';
        }

    }

    public function getemployee(Request $request)
    {

        return $emp=Employee::find($request->id);


    }
}
