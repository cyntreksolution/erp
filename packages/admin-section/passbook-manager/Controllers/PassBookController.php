<?php

namespace PassBookManager\Controllers;

use Carbon\Carbon;
use PassBookManager\Classes\BankTransaction;
use PassBookManager\Classes\ChequeData;
use PassBookManager\Models\Cheque;
use PassBookManager\Models\PassBook;
use PurchasingOrderManager\Models\PurchasingOrder;
use RawMaterialManager\Models\RawAdjust;
use RecipeManager\Models\RecipeContent;
use Response;
use Sentinel;
use RawMaterialManager\Models\RawMaterial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use StockManager\Classes\StockTransaction;
use StockManager\Models\Stock;
use StockManager\Models\StockRawMaterial;
use SupplierManager\Models\Supplier;

class PassBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:pass_book-index', ['only' => ['index']]);
        $this->middleware('permission:passBook-list', ['only' => ['list']]);
        $this->middleware('permission:pass_book-cheque-deposit-create', ['only' => ['create','store']]);


    }
    public function index()
    {
        return redirect(route('pass_book.create'));
        return view('PassBookManager::pass_book.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        ChequeData::issueChequeClearOnDate();
        ChequeData::receivedChequeClearOnDate();
        $balance=PassBook::where('id',1)->first();
        $issued=Cheque::where('type',1)->where('status',1)->get();
        $received='';
        $transaction=Cheque::where('created_at', '>', Carbon::now()->subDays(7))->get();
        $chequeToDeposit=Cheque::where('type',1)->where('status',1)->get();
        return view('PassBookManager::pass_book.create')->with([
            'balance'=>$balance,
            'transaction'=>$transaction,
            'chequesToDeposit'=>$chequeToDeposit,
        ]);
    }


    public function store(Request $request)
    {
        $flow=$request->flow;
        $account=1;
        $amount=$request->amount;
        $description=$request->description;
        $cheque_no=$request->chequeNo;
        $bank=$request->bank;
        $branch=$request->branch;
        $cdate=$request->date;
        if($flow=='issueCheque'){
            BankTransaction::issuCheque(5,$amount,1,$cheque_no,$cdate,Carbon::now(),0,$description);
        }
        elseif ($flow=='cashDeposit'){
            BankTransaction::updatePassBook(3,$amount,$account);
            BankTransaction::cashDeposit(1,$amount,$description);
        }
        elseif ($flow=='chequeDeposit'){
            BankTransaction::receivedCheque(4,$amount,$account,$cheque_no,$cdate,Carbon::now(),0,$description,$bank,$branch);
        }
        return redirect(route('pass_book.index'));
    }


    public function show(RawMaterial $raw_material)
    {
//        return view('RawMaterialManager::pass_book.show')->with([
//            'raw_material' => $raw_material
//        ]);
//    }
//
//    public function details(RawMaterial $raw_material)
//    {
//        $transactions = StockRawMaterial::with('rawMaterials')
//            ->where('raw_material_id', '=', $raw_material->raw_material_id)->get();
//        return view('RawMaterialManager::pass_book.details')->with([
//            'raw_material' => $raw_material,
//            'transactions' => $transactions,
//        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \RawMaterialManager\Models\RawMaterial $raw_material
     * @return \Illuminate\Http\Response
     */
    public function edit(RawMaterial $raw_material)
    {
        return view('PassBookManager::raw_material.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \RawMaterialManager\Models\RawMaterial $raw_material
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PassBook $passBook)
    {
//        if (isset($request->Name)) {
//            $raw_material->name = $request->Name;
//            $raw_material->save();
//            return redirect('raw_material/' . $raw_material->raw_material_id)->with([
//                'success' => true,
//                'success.title' => 'Congratulations !',
//                'success.message' => 'name has been updated'
//            ]);
//        }
//
//        if (isset($request->desc)) {
//            $raw_material->desc = $request->desc;
//            $raw_material->save();
//            return redirect('raw_material/' . $raw_material->raw_material_id)->with([
//                'success' => true,
//                'success.title' => 'Congratulations !',
//                'success.message' => 'Description has been updated'
//            ]);
//        }
//
//        if (isset($request->buffer_stock)) {
//            $raw_material->buffer_stock = $request->buffer_stock;
//            $raw_material->save();
//            return redirect('raw_material/' . $raw_material->raw_material_id)->with([
//                'success' => true,
//                'success.title' => 'Congratulations !',
//                'success.message' => 'Buffer stock has been updated'
//            ]);
//        }
//
//        if (isset($request->stock)) {
//            $currentStock = $raw_material->available_qty;
//            if ($raw_material->measurement == 'gram') {
//                $diffrence = $currentStock - ($request->stock * 1000);
//                $ajustedQty = ($request->stock * 1000);
//            } elseif ($raw_material->measurement == 'milliliter') {
//                $diffrence = $currentStock - ($request->stock * 1000);
//                $ajustedQty = ($request->stock * 1000);
//            } else {
//                $diffrence = $currentStock - $request->stock;
//                $ajustedQty = $request->stock;
//            }
//            $raw_material->available_qty = $ajustedQty;
//            $raw_material->save();
//
//            $adjusted = new RawAdjust();
//            $adjusted->raw_material_raw_material_id = $raw_material->raw_material_id;
//            $adjusted->current_stock = $currentStock;
//            $adjusted->difference = $diffrence;
//            $adjusted->created_by = Sentinel::getUser()->id;
//            $adjusted->save();
//
//            StockTransaction::rawMaterialStockTransaction(3, $raw_material->raw_material_id,$ajustedQty,$adjusted->id,1);
//            return redirect('raw_material/stock/status/' . $raw_material->raw_material_id)->with([
//                'info' => true,
//                'info.title' => 'Congratulations !',
//                'info.message' => 'Current Stock Adjusted'
//            ]);
//        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \RawMaterialManager\Models\RawMaterial $raw_material
     * @return \Illuminate\Http\Response
     */
    public function destroy(PassBook $passBook)
    {
//        $rc = RecipeContent::where('raw_material_id', '=', $passBook->raw_material_id)->count();
//        if ($rc == 0) {
//            $raw_material->delete();
//            if ($raw_material->trashed()) {
//                return 'true';
//            } else {
//                return 'false';
//            }
//        } else {
//            return 'false';
//        }

    }


//    public function stock(Request $request)
//    {
//        $data = RawMaterial::all();
//        $jsonList = array();
//        $i = 1;
//
//        foreach ($data as $key => $item) {
//            $dd = array();
//            array_push($dd, $i);
//            array_push($dd, $item->name);
//            $qty = $item->available_qty / 1000;
//            array_push($dd, $item->available_qty);
//            array_push($dd, $item->available_qty);
//            array_push($dd, $item->available_qty);
//
//
////            array_push($dd, '<a href="edit/' . $item->stockpile_id . '" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons">dvr</i></a>');
////            array_push($dd, ' <a href="#" class="btn btn-simple btn-danger btn-icon remove" onclick="del(' . $item->stockpile_id . ')"><i class="material-icons">close</i></a>');
//            array_push($jsonList, $dd);
//            $i++;
//        }
//        return Response::json(array('data' => $jsonList));
//
//
//    }
}
