@extends('layouts.back.master')@section('title','Bank Account')
@section('css')

    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/modal.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.wizard.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.validation.css')}}">
    <link rel="stylesheet" href="{{asset('assets/packages/raw_materials/raw_material.css')}}">

    <style>


    </style>
@stop
{{--@section('current','Cash Book')--}}
{{--@section('current_url',route('raw_materials.create'))--}}

@section('content')
    <div class="app-wrapper">
        <div class="app-panel" id="app-panel">
            <div class="app-panel-inner">
                <div class="scroll-container">
                    @can('pass_book-cheque-deposit-create')
                    <div class="p-3">
                        <button class="btn btn-success py-3 btn-block btn-lg" data-toggle="modal"
                                data-target="#inflow">Cash Deposit
                        </button>
                    </div>
                    <hr class="m-0">
                    <div class="p-3">
                        <button class="btn btn-success py-3 btn-block btn-lg" data-toggle="modal"
                                data-target="#chDeposit">Cheque Deposit
                        </button>
                    </div>
                    <hr class="m-0">
                    <div class="p-3">
                        <button class="btn btn-danger py-3 btn-block btn-lg" data-toggle="modal"
                                data-target="#outflow">Issue Cheque
                        </button>
                    </div>
                    @endcan
                    <hr class="m-0">
                    <div class="media-list">
                        <a>
                            <div class="media content2">
                                @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                <div class="avatar avatar-sm bg-{{$color[2]}}">$$
                                </div>
                                <div class="media-body">
                                    <h6 class="media-heading my-1">
                                        <h6>{{$balance->balance}} LKR</h6>
                                    </h6>
                                </div>
                            </div>
                        </a>

                        <a>
                            <div class="media content2">
                                @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                <div class="avatar avatar-sm bg-{{$color[0]}}">+
                                </div>
                                <div class="media-body">
                                    <h6 class="media-heading my-1">
                                        <h6>{{$balance->balance}} LKR</h6>
                                    </h6>
                                </div>
                            </div>
                        </a>

                        <a>
                            <div class="media content2">
                                @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                <div class="avatar avatar-sm bg-{{$color[3]}}">-
                                </div>
                                <div class="media-body">
                                    <h6 class="media-heading my-1">
                                        <h6>{{$balance->balance}} LKR</h6>
                                    </h6>
                                </div>
                            </div>
                        </a>


                    </div>
                </div>
                <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show">
                    <i class="fa fa-chevron-right"></i> <i class="fa fa-chevron-left"></i></a></div>
        </div>
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">Bank Account Cash Flow</h5>
            </div>

            <div class="app-main-content px-3 py-4" id="container">
                <div class="content ">
                    <div class="contacts-list">
                        <div class="media-list">
                            @foreach($transaction as $t)
                                @if($t->type==1)
                                    @if($t->status==1)
                                        @php
                                            $customer=\RouteManager\Models\Shop::where('id',$t->supplier)->first();
                                        @endphp
                                        <div class="media">
                                            <div class="avatar avatar-sm bg-success">+
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading" id="media-list-item-3">recieved
                                                    from{{$customer->name}}</h6>
                                                <small>{{$t->amount}} lkr</small>
                                            </div>
                                            <h6 class="media-heading">Cheque Date {{$t->cheque_date}}</h6>
                                        </div>
                                    @elseif($t->status==2)
                                        <div class="media">
                                            <div class="avatar avatar-sm bg-success">+
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading" id="media-list-item-3">recieved
                                                    from{{$customer->name}}</h6>
                                                <small>{{$t->amount}} lkr</small>
                                            </div>
                                            <h6 class="media-heading">Cheque Date {{$t->cheque_date}}</h6>
                                        </div>
                                    @endif

                                @elseif($t->type==2)
                                    @php
                                        $supplier=\SupplierManager\Models\Supplier::where('id',$t->supplier)->first();
                                    @endphp
                                    @if($t->status==1)
                                        <div class="media">
                                            <div class="avatar avatar-sm bg-warning">-
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading"
                                                    id="media-list-item-3">issued to {{$supplier->supplier_name}}</h6>
                                                <small>{{$t->amount}} lkr</small>
                                            </div>
                                            <h6 class="media-heading">Cheque Date {{$t->cheque_date}}</h6>
                                        </div>
                                    @elseif($t->status==2)
                                        <div class="media">
                                            <div class="avatar avatar-sm bg-danger">-
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading"
                                                    id="media-list-item-3">issued to {{$supplier->supplier_name}}</h6>
                                                <small>{{$t->amount}} lkr</small>
                                            </div>
                                            <h6 class="media-heading">Banked On {{$t->cheque_date}}</h6>
                                        </div>
                                    @endif
                                @elseif($t->type==3)
                                    <div class="media">
                                        <div class="avatar avatar-sm bg-success">+
                                        </div>
                                        <div class="media-body">
                                            <h6 class="media-heading"
                                                id="media-list-item-3">Cash Deposit {{$t->description}}</h6>
                                            <small>{{$t->amount}} lkr</small>
                                        </div>
                                        <h6 class="media-heading">on {{$t->cheque_date}}</h6>
                                    </div>
                                @elseif($t->type==4)
                                    @if($t->status==1)
                                        @php
                                            $customer=\RouteManager\Models\Shop::where('id',$t->supplier)->first();
                                        @endphp
                                        <div class="media">
                                            <div class="avatar avatar-sm bg-info">+
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading" id="media-list-item-3">recieved
                                                    {{$t->description}}</h6>
                                                <small>{{$t->amount}} lkr</small>
                                            </div>
                                            <h6 class="media-heading">Cheque Date {{$t->cheque_date}}</h6>
                                        </div>
                                    @elseif($t->status==2)
                                        <div class="media">
                                            <div class="avatar avatar-sm bg-success">+
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading" id="media-list-item-3">{{$t->description}}</h6>
                                                <small>{{$t->amount}} lkr</small>
                                            </div>
                                            <h6 class="media-heading">Cheque Date {{$t->cheque_date}}</h6>
                                        </div>
                                    @endif
                                @elseif($t->type==5)
                                    @php
                                        $supplier=\SupplierManager\Models\Supplier::where('id',$t->supplier)->first();
                                    @endphp
                                    @if($t->status==1)
                                        <div class="media">
                                            <div class="avatar avatar-sm bg-warning">-
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading"
                                                    id="media-list-item-3">{{$t->description}}</h6>
                                                <small>{{$t->amount}} lkr</small>
                                            </div>
                                            <h6 class="media-heading">Cheque Date {{$t->cheque_date}}</h6>
                                        </div>
                                    @elseif($t->status==2)
                                        <div class="media">
                                            <div class="avatar avatar-sm bg-danger">-
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading"
                                                    id="media-list-item-3">{{$t->description}}</h6>
                                                <small>{{$t->amount}} lkr</small>
                                            </div>
                                            <h6 class="media-heading">Banked On {{$t->cheque_date}}</h6>
                                        </div>
                                    @endif
                                @endif
                            @endforeach
                        </div>
                    </div>

                </div>
                {{--</div>--}}
            </div>
        </div>
    </div>


    <div class="modal fade" id="outflow" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'pass_book','enctype'=>'multipart/form-data'))}}
                <input type="hidden" name="flow" value="issueCheque">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="task-name-wrap">
                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                        <input class="task-name-field" type="number" name="amount"
                               placeholder="amount">
                    </div>
                    <hr>
                    <div class="task-name-wrap">
                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                        <input class="task-name-field" type="number" name="$cheque_no"
                               placeholder="cheque number">
                    </div>
                    <hr>
                    <div class="task-name-wrap">
                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                        <input class="task-name-field" type="date" name="$cheque_date"
                               placeholder="cheque date">
                    </div>
                    <hr>
                    <div class="task-name-wrap">
                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                        <input class="task-name-field" type="text" name="description"
                               placeholder="Description">
                    </div>

                </div>
                <div class="modal-footer">
                    <input type="submit" id="finish-btn" class="finish btn btn-block btn-danger "
                           value="Issue Cheque"/>

                </div>
                {!!Form::close()!!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="inflow" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'pass_book','enctype'=>'multipart/form-data'))}}
                <input type="hidden" name="flow" value="cashDeposit">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="task-name-wrap">
                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                        <input class="task-name-field" type="number" name="amount"
                               placeholder="amount">
                    </div>
                    <hr>
                    <div class="task-name-wrap">
                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                        <input class="task-name-field" type="text" name="description"
                               placeholder="description">
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" id="finish-btn" class="finish btn btn-block btn-success "
                           value="Cash Deposit"/>
                </div>
                {!!Form::close()!!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="chDeposit" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'pass_book','enctype'=>'multipart/form-data'))}}
                <input type="hidden" name="flow" value="chequeDeposit">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="task-name-wrap">
                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                        <input class="task-name-field" type="number" name="amount"
                               placeholder="amount">
                    </div>
                    <hr>
                    <div class="task-name-wrap">
                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                        <input class="task-name-field" type="date" name="date"
                               placeholder="date">
                    </div>
                    <hr>
                    <div class="task-name-wrap">
                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                        <input class="task-name-field" type="text" name="chequeNo"
                               placeholder="cheque number">
                    </div>
                    <hr>
                    <div class="task-name-wrap">
                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                        <input class="task-name-field" type="text" name="bank"
                               placeholder="Bank">
                    </div>
                    <hr>
                    <div class="task-name-wrap">
                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                        <input class="task-name-field" type="text" name="branch"
                               placeholder="Branch">
                    </div>
                    <hr>
                    <div class="task-name-wrap">
                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                        <input class="task-name-field" type="text" name="description"
                               placeholder="description">
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" id="finish-btn" class="finish btn btn-block btn-success "
                           value="Cheque Deposit"/>
                </div>
                {!!Form::close()!!}
            </div>
        </div>
    </div>

@stop
@section('js')
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="{{asset('assets/examples/js/demos/form.wizard.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/validate.min.js')}}"></script>
    <script src="{{asset('assets/packages/raw_materials/raw_material.js')}}"></script>

@stop