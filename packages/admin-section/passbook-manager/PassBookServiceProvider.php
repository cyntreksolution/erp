<?php

namespace PassBookManager;

use Illuminate\Support\ServiceProvider;

class PassBookServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/Views', 'PassBookManager');
        require __DIR__ . '/web.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('PassBookManager', function($app){
            return new PassBookManager;
        });
    }
}
