<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web'])->group(function () {
    Route::prefix('pass_book')->group(function () {
        Route::get('', 'PassBookManager\Controllers\PassBookController@index')->name('pass_book.index');

        Route::get('create', 'PassBookManager\Controllers\PassBookController@create')->name('pass_book.create');

        Route::get('{pass_book}', 'PassBookManager\Controllers\PassBookController@show')->name('pass_book.show');

        Route::get('{pass_book}/edit', 'PassBookManager\Controllers\PassBookController@edit')->name('pass_book.edit');



        Route::post('', 'PassBookManager\Controllers\PassBookController@store')->name('pass_book.store');

        Route::post('{pass_book}', 'PassBookManager\Controllers\PassBookController@update')->name('pass_book.update');

        Route::delete('{pass_book}', 'PassBookManager\Controllers\PassBookController@destroy')->name('pass_book.destroy');
    });
});
