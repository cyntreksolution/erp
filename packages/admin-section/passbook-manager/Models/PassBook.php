<?php

namespace PassBookManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PassBook extends Model
{
    use SoftDeletes;

    protected $table = 'bank_account';

    protected $primaryKey = 'id';

    protected $dates = ['deleted_at'];


}
