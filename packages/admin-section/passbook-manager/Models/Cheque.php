<?php

namespace PassBookManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cheque extends Model
{
    use SoftDeletes;

    protected $table = 'cheque';

    protected $primaryKey = 'id';

    protected $dates = ['deleted_at'];


}
