<?php
/**
 * Created by PhpStorm.
 * User: Isuru Sithmal
 * Date: 3/1/2018
 * Time: 4:33 PM
 */

namespace PassBookManager\Classes;


use Carbon\Carbon;
use PassBookManager\Models\Cheque;

class ChequeData
{
    public static function getUnClearedIssueCheques(){
        $uncleared=Cheque::where('type',2)->where('status',1)->get();
        return $uncleared;
    }

    public static function getUnClearedRecievedCheques(){
        $uncleared=Cheque::where('type',1)->where('status',1)->get();
        return $uncleared;
    }

    public static function issueChequeClearOnDate(){
        $date=Carbon::now();
        $uncleared=Cheque::where('type',2)->where('status',1)->where('cheque_date','<',$date)->get();
        foreach ($uncleared as $item){
            $item->status=2;
            $item->save();
            BankTransaction::updatePassBook(1,$item->amount,1);
        }

        $uncleared=Cheque::where('type',5)->where('status',1)->where('cheque_date','<',$date)->get();
        foreach ($uncleared as $item){
            $item->status=2;
            $item->save();
            BankTransaction::updatePassBook(1,$item->amount,1);
        }
    }

    public static function receivedChequeClearOnDate(){
        $date=Carbon::now();
        $uncleared=Cheque::where('type',1)->where('status',1)->where('cheque_date','<',$date)->get();
        foreach ($uncleared as $item){
            $item->status=2;
            $item->save();
            BankTransaction::updatePassBook(1,$item->amount,1);
        }
        $uncleared=Cheque::where('type',4)->where('status',1)->where('cheque_date','<',$date)->get();
        foreach ($uncleared as $item){
            $item->status=2;
            $item->save();
            BankTransaction::updatePassBook(1,$item->amount,1);
        }
    }

}