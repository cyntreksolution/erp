<?php
/**
 * Created by PhpStorm.
 * User: Isuru Sithmal
 * Date: 2/28/2018
 * Time: 3:41 PM
 */

namespace PassBookManager\Classes;


use Carbon\Carbon;
use PassBookManager\Models\Cheque;
use PassBookManager\Models\PassBook;

class BankTransaction
{
    public static function updatePassBook($type,$amount,$account)
    {
        if($type==2){
            $amount=-$amount;
        }
        $bankAccount=PassBook::where('id',$account)->first();
        $bankAccount->balance=$bankAccount->balance+$amount;
        $bankAccount->save();
    }

    public static function receivedCheque($type,$amount,$account,$cheque_no,$cheque_date,$receive_date,$customer,$description,$bank,$branch){
        $cheque=new Cheque();
        $cheque->type=$type;
        $cheque->bank_account_id=$account;
        $cheque->cheque_date=$cheque_date;
        $cheque->cheque_no=$cheque_no;
        $cheque->amount=$amount;
        $cheque->issue_date=$receive_date;
        $cheque->supplier=$customer;
        $cheque->bank=$bank;
        $cheque->branch=$branch;
        $cheque->description=$description;
        $cheque->save();
    }

    public static function issuCheque($type,$amount,$account,$cheque_no,$cheque_date,$issue_date,$supplier,$description){
        $cheque=new Cheque();
        $cheque->type=$type;
        $cheque->bank_account_id=$account;
        $cheque->cheque_date=$cheque_date;
        $cheque->cheque_no=$cheque_no;
        $cheque->amount=$amount;
        $cheque->issue_date=$issue_date;
        $cheque->supplier=$supplier;
        $cheque->description=$description;
        $cheque->save();
        return $cheque;
    }

    public static function cashDeposit($account,$amount,$description){
        $cheque=new Cheque();
        $cheque->type=3;
        $cheque->bank_account_id=$account;
        $cheque->cheque_date=Carbon::now();
        $cheque->cheque_no=0;
        $cheque->amount=$amount;
        $cheque->issue_date=Carbon::now();
        $cheque->supplier=0;
        $cheque->description=$description;
        $cheque->save();
        return $cheque;
    }
}