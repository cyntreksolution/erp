<?php

namespace SupplierManager\Controllers;

use Illuminate\Support\Facades\Auth;
use Sentinel;
use RawMaterialManager\Models\RawMaterial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use StockManager\Models\Stock;
use SupplierManager\Models\Supplier;
use UserManager\Models\Users;

class SupplierController extends Controller

{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect(route('supplier.create'));
        return view('SupplierManager::supplier.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $raw=RawMaterial::all();
        $supplier = Supplier::all();
        return view('SupplierManager::supplier.create')->with([
            'suppliers'=>$supplier,
            'rmaterials'=>$raw,
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($request->image)) {
            $file = $request->file('image');
            $path = '/uploads/images/suppliers/';
            $destinationPath = storage_path($path);
            $file->move($destinationPath, $file->getClientOriginalName());

            $supplier=new Supplier();
            $supplier->supplier_name=$request->name;
            $supplier->address=$request->address;
            $supplier->mobile = $request->tp;
            $supplier->mobile1 = $request->tp1;
            $supplier->mobile2 = $request->tp2;
            $supplier->image = $file->getClientOriginalName();
            $supplier->image_path = 'storage' . $path;
            $supplier->colour = rand() % 7;
            $supplier->created_by = Auth::user()->id;
            $supplier->save();
            return redirect(route('supplier.create'))->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => $request->name .' has been added as an Buntalk Supplier.'
            ]);
        } else {
            $supplier=new Supplier();
            $supplier->supplier_name=$request->name;
            $supplier->address=$request->address;
            $supplier->mobile = $request->tp;
            $supplier->mobile1 = $request->tp1;
            $supplier->mobile2 = $request->tp2;
            $supplier->colour = rand() % 7;
            //$supplier->created_by = Sentinel::getUser()->id;
            $supplier->save();
            return redirect(route('supplier.create'))->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => $request->name .' has been added as an Buntalk Supplier.'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \StockManager\Models\Stock $stock
     * @return \Illuminate\Http\Response
     */
    public function show(Supplier $supplier)
    {
        return view('SupplierManager::supplier.show')->with(
            ['supplier'=>$supplier]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \StockManager\Models\Stock $stock
     * @return \Illuminate\Http\Response
     */
    public function edit(Stock $stock)
    {
        return view('SupplierManager::supplier.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \StockManager\Models\Stock $stock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stock $stock)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \StockManager\Models\Stock $stock
     * @return \Illuminate\Http\Response
     */
    public function destroy(Supplier $supplier)
    {
        $supplier->delete();
        if ($supplier->trashed()) {
            return 'true';
        }else{
            return 'false';
        }

    }

    public function getsupplier(Request $request){
        return $sup=Supplier::find($request->id);
    }
}
