<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web','log'])->group(function () {
    Route::prefix('supplier')->group(function () {
        Route::get('', 'SupplierManager\Controllers\SupplierController@index')->name('supplier.index');

        Route::get('create', 'SupplierManager\Controllers\SupplierController@create')->name('supplier.create');

        Route::get('{supplier}', 'SupplierManager\Controllers\SupplierController@show')->name('supplier.show');

        Route::get('{supplier}/edit', 'SupplierManager\Controllers\SupplierController@edit')->name('supplier.edit');

        Route::post('getsupplier', 'SupplierManager\Controllers\SupplierController@getsupplier')->name('supplier.getsupplier');


        Route::post('', 'SupplierManager\Controllers\SupplierController@store')->name('supplier.store');

        Route::put('{supplier}', 'SupplierManager\Controllers\SupplierController@update')->name('supplier.update');

        Route::delete('{supplier}', 'SupplierManager\Controllers\SupplierController@destroy')->name('supplier.destroy');
    });
});
