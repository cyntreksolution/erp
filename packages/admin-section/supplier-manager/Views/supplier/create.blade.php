@extends('layouts.back.master')@section('title','Suppliers')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.wizard.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/css/style.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/css/site.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/demos.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.fileupload.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.validation.css')}}">
    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }
    </style>
@stop
@section('content')
    <div class="app-wrapper">
        <div class="app-panel" id="app-panel">
            <div class="app-search"><input type="search" class="search-field" placeholder="Search">
                <i class="search-icon fa fa-search"></i>
            </div>
            <div class="app-panel-inner">
                <div class="scroll-container">
                    <div class="p-3">
                        <button class="btn btn-success py-3 btn-block btn-lg" data-toggle="modal"
                                data-target="#projects-task-modal">ADD NEW SUPPLIER
                        </button>
                    </div>
                </div>
            </div>
            <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show"><i
                        class="fa fa-chevron-right"></i>
                <i class="fa fa-chevron-left"></i>
            </a>
        </div>
        <!-- /.app-panel -->
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">SUPPLIER LIST</h5>
            </div><!-- /.app-main-header -->
            <div class="app-main-content">
                <div class="container">
                    <div class="row">
                        @foreach($suppliers as $supplier)

                            <div class="list-group-item col-md-6 justify-content-between">
                                <div class="media">
                                    @if(isset($supplier->image))
                                        <a href="{{$supplier->id}}" class="avatar avatar avatar-sm">
                                            <img src="{{asset($supplier->image_path.$supplier->image)}}" alt="">
                                        </a>
                                    @else
                                        @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                        <a href="{{$supplier->id}}"
                                           class="avatar avatar-circle avatar-sm bg-{{$color[$supplier->colour]}} "
                                           data-plugin="firstLitter"
                                           data-target="#raw-{{$supplier->id*874}}">
                                        </a>
                                    @endif
                                    <div class="media-body">
                                        <h6 class="media-heading my-1">
                                            <a id="raw-{{$supplier->id*874}}" id="">{{$supplier->supplier_name}}</a>
                                        </h6>
                                        <small>{{$supplier->address}}</small>
                                    </div>
                                </div>
                                <div class="section-2">
                                    <button class="delete btn btn-light btn-sm ml-2" value="{{$supplier->id}}"
                                            data-toggle="tooltip"
                                            data-placement="left" title="DELETE SUPPLIER">
                                        <i class="zmdi zmdi-close"></i>
                                    </button>
                                </div>
                            </div>

                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="projects-task-modal" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'supplier','enctype'=>'multipart/form-data', 'id'=>'bootstrap-wizard-form'))}}
                {{--<form id="bootstrap-wizard-form">--}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="wizard p-4" id="bootstrap-wizard-1">
                                <ul class="nav nav-tabs vertices keep-prefix-suffix" role="tablist">
                                    <li class="nav-item">
                                        <a class=" nav-link active" data-toggle="tab" href="#ex5-step-1"
                                           role="tab">
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#ex5-step-2"
                                           role="tab">
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="ex5-step-1" role="tabpanel">
                                        <div class="task-name-wrap">
                                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text"  name="name"
                                                   placeholder="Supplier Name">
                                        </div>
                                        <hr>
                                        <div class="task-name-wrap">
                                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field"  type="text" name="address"
                                                   placeholder="Supplier Address">
                                        </div>
                                        <hr>
                                        <div class="task-name-wrap">
                                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="tp"
                                                   placeholder="Telephone Number">
                                        </div>
                                        <hr>
                                        <div class="task-name-wrap">
                                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="tp1"
                                                   placeholder="Telephone Number 1">
                                        </div>
                                        <hr>
                                        <div class="task-name-wrap">
                                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="tp2"
                                                   placeholder="Telephone Number 2">
                                        </div>
                                        <hr>
                                        {{--<div class="task-name-wrap">--}}
                                            {{--<span><i class="zmdi zmdi-check"></i></span>--}}
                                            {{--<input class="task-name-field" type="text" name="dob"--}}
                                                   {{--placeholder="Raw Materials">--}}
                                        {{--</div>--}}

                                        {{--<select class="js-example-responsive" name="permissions[]" multiple="multiple" style="width: 100%">--}}
                                            {{--@foreach($rmaterials as $raw)--}}
                                                {{--<option value=" {{$raw->raw_material_id}}">{{$raw->name}}</option>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}
                                        {{--<hr>--}}

                                    </div>
                                    <div class="tab-pane" id="ex5-step-2" role="tabpanel">
                                        <div class="task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="file" name="image">
                                        </div>
                                        <hr>
                                    </div>
                                </div>

                                <div class="pager d-flex justify-content-center">
                                    <input type="submit" id="finish-btn" class="finish btn btn-success w-50"
                                           value="Finish"/>

                                    <button type="button" id="next-btn" class="next btn btn-success w-50">Next
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--</form>--}}
                {!!Form::close()!!}
            </div>
        </div>
    </div>


@stop
@section('js')

    {{--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--}}
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>--}}
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/examples/js/demos/form.wizard.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    {{--<script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>--}}

    {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>--}}
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>--}}
    {{--<script src="{{asset('assets/vendor/blueimp-file-upload/js/vendor/jquery.ui.widget.js')}}"></script>--}}
    {{--<script src="{{asset('assets/vendor/blueimp-file-upload/js/tmpl.min.js')}}"></script>--}}
    {{--<script src="{{asset('assets/vendor/blueimp-file-upload/js/load-image.all.min.js')}}"></script>--}}
    {{--<script src="{{asset('assets/vendor/blueimp-file-upload/js/canvas-to-blob.min.js')}}"></script>--}}
    {{--<script src="{{asset('assets/vendor/blueimp-file-upload/js/jquery.iframe-transport.js')}}"></script>--}}
    {{--<script src="{{asset('assets/vendor/blueimp-file-upload/js/jquery.fileupload.js')}}"></script>--}}
    {{--<script src="{{asset('assets/vendor/blueimp-file-upload/js/jquery.fileupload-process.js')}}"></script>--}}
    {{--<script src="{{asset('assets/vendor/blueimp-file-upload/js/jquery.fileupload-image.js')}}"></script>--}}
    {{--<script src="{{asset('assets/vendor/blueimp-file-upload/js/jquery.fileupload-audio.js')}}"></script>--}}
    {{--<script src="{{asset('assets/vendor/blueimp-file-upload/js/jquery.fileupload-video.js')}}"></script>--}}
    {{--<script src="{{asset('assets/vendor/blueimp-file-upload/js/jquery.fileupload-validate.js')}}"></script>--}}
    {{--<script src="{{asset('assets/vendor/blueimp-file-upload/js/jquery.fileupload-ui.js')}}"></script>--}}
    {{--<script src="{{asset('assets/examples/js/demos/form.fileupload.js')}}"></script>--}}
    {{--<script src="{{asset('assets/js/site.js')}}"></script>--}}


    <script>




        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

//        $(".js-example-responsive").select2({
//            width: 'resolve' // need to override the changed default
//        });


        $('.delete').click(function (e) {
            e.preventDefault();
            id = $(this).attr("value");
            confirmAlert(id);

        });

        function confirmAlert(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "delete",
                        url: id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            }
                            else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

        $("#bootstrap-wizard-form").validate({
            rules: {
                name: {required: !0, },
                address: "required",
                tp: {required: !0, number: !0, maxlength: 10},

                // email: {required: !0, email: !0},
                // name: {required: !0, minlength: 3},
                // country: "required",
                // city: "required",
                // payment_method: "required",
                // spending: {required: !0, number: !0},
                // services: "required",
                // message: {required: !0, minlength: 6, maxlength: 140}

            },
            messages: {
                name: "Please enter the supplier name",
                address: "Please enter the address",
            },
            errorElement: "div", errorPlacement: function (e, o) {
                e.addClass("form-control-feedback"), o.closest(".form-group").addClass("has-danger"), "checkbox" === o.prop("type") ? e.insertAfter(o.parent(".checkbox")) : e.insertAfter(o)
            }, highlight: function (e, o, r) {
                $(e).closest(".form-group").addClass("has-danger").removeClass("has-success"), $(e).removeClass("form-control-success").addClass("form-control-danger")
            }, unhighlight: function (e, o, r) {
                $(e).closest(".form-group").addClass("has-success").removeClass("has-danger"), $(e).removeClass("form-control-danger").addClass("form-control-success")
            }
        });


    </script>
@stop