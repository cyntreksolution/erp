@extends('layouts.back.master')@section('title','Supplier Details')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/lightbox2/dist/css/lightbox.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}"/>

@stop
@section('content')


    <div class="profile-wrapper">
        <div class="profile-section-user">
            <div class="profile-cover-img"><img src="../assets/global/images/profile-cover-1.jpg" alt=""></div>
            <div class="profile-info-brief p-3"><img class="img-fluid user-profile-avatar"
                                                     src="../assets/global/images/profile-avatar-2.jpg" alt="">
                <div class="text-center"><h5 class="text-uppercase mb-4">{{$supplier->supplier_name}}</h5>

                </div><!-- /.profile-info-brief -->


                <hr class="m-0">

                <div class="hidden-sm-down">
                    <hr class="m-0">
                    {{$supplier->address}} <br>
                    {{$supplier->mobile}} <br>
                    {{$supplier->desc}} <br>
                </div>
            </div>
        </div>
        <div class="profile-section-main"><!-- Nav tabs -->
            <ul class="nav nav-tabs profile-tabs" role="tablist">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#profile-overview"
                                        role="tab">Timeline</a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#profile-photos" role="tab">Photos</a>
                </li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#profile-settings"
                                        role="tab">Settings</a>
                </li>
            </ul><!-- /.nav-tabs --><!-- Tab panes -->
            <div class="tab-content profile-tabs-content">
                <div class="tab-pane active" id="profile-overview" role="tabpanel">
                    <div class="stream-posts">
                        <div class="stream-post">
                            <div class="sp-author"> Supplied <br> Products</div>
                            <div class="sp-content">
                                <div class="sp-paragraph mb-0">
                                    <div class="app-main">
                                        <div class="app-main-content">
                                            <div class="container">
                                                <div class="row">


                                                    <div class="media">
                                                        <a href="#" class="avatar avatar avatar-sm">
                                                            <img src="http://localhost/erp/storage/uploads/images/raw_materials/white sugar.jpg" alt="">
                                                        </a>


                                                        <div class="media-body">
                                                            <h6 class="media-heading my-1">
                                                                <a id="raw-2622">White Sugar</a>
                                                            </h6>
                                                            <small>for all products</small>
                                                        </div>
                                                    </div>
                                                    <div class="section-2">
                                                        <a href="#" class="btn btn-light btn-sm">
                                                            <i class="zmdi zmdi-close"></i>
                                                        </a>
                                                    </div>

                                                                                      <br> <br>
                                                    <div class="media">
                                                        <a href="#" class="avatar avatar avatar-sm">
                                                            <img src="http://localhost/erp/storage/uploads/images/raw_materials/white sugar.jpg" alt="">
                                                        </a>


                                                        <div class="media-body">
                                                            <h6 class="media-heading my-1">
                                                                <a id="raw-2622">White Sugar</a>
                                                            </h6>
                                                            <small>for all products</small>
                                                        </div>
                                                    </div>
                                                    <div class="section-2">
                                                        <a href="#" class="btn btn-light btn-sm">
                                                            <i class="zmdi zmdi-close"></i>
                                                        </a>
                                                    </div>

                                                    <br> <br>   <br> <br>
                                                    <div class="app-panel-inner">
                                                        <div class="scroll-container">
                                                            <div class="p-1" >
                                                                <button class="btn btn-success py-1 btn-block" style="float: right" data-toggle="modal"
                                                                        data-target="#projects-task-modal">ADD NEW PRODUCT
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.sp-content -->
                        </div><!-- /.stream-post -->
                        <div class="stream-post">
                            <div class="sp-author"> Recent <br> Products</div>
                            <div class="sp-content">
                                <div class="sp-info">posted 2h ago</div>
                                <p class="sp-paragraph mb-0">Auk Soldanella plainscraft acetonylidene
                                    wolfishness irrecognizant Candolleaceae provision Marsipobranchii arpen
                                    Paleoanthropus supersecular inidoneous autoplagiarism palmcrist occamy
                                    equestrianism periodontoclasia mucedin overchannel goelism decapsulation
                                    pourer zira</p></div><!-- /.sp-content --></div><!-- /.stream-post -->
                    </div><!-- /.stream-posts -->
                </div><!-- /#profile-overview -->
                <div class="tab-pane" id="profile-photos" role="tabpanel">
                    <div class="img-gallery img-gallery-zoom-effect">
                        <div class="row">
                            <div class="col-6 col-md-4"><a href="../assets/global/images/blog-img-11.jpg"
                                                           class="g__item rounded"
                                                           data-lightbox="profile-gallery" data-title="image">
                                    <figure class="g__figure"><img src="../assets/global/images/blog-img-11.jpg"
                                                                   alt="">
                                        <figcaption class="g__overlay"><i class="g__zoom fa fa-search fa-2x"></i>
                                        </figcaption>
                                    </figure>
                                </a><!-- /.g__item --></div><!-- /.col- -->
                            <div class="col-6 col-md-4"><a href="../assets/global/images/blog-img-12.jpg"
                                                           class="g__item rounded"
                                                           data-lightbox="profile-gallery" data-title="image">
                                    <figure class="g__figure"><img src="../assets/global/images/blog-img-12.jpg"
                                                                   alt="">
                                        <figcaption class="g__overlay"><i class="g__zoom fa fa-search fa-2x"></i>
                                        </figcaption>
                                    </figure>
                                </a><!-- /.g__item --></div><!-- /.col- -->
                            <div class="col-6 col-md-4"><a href="../assets/global/images/blog-img-13.jpg"
                                                           class="g__item rounded"
                                                           data-lightbox="profile-gallery" data-title="image">
                                    <figure class="g__figure"><img src="../assets/global/images/blog-img-13.jpg"
                                                                   alt="">
                                        <figcaption class="g__overlay"><i class="g__zoom fa fa-search fa-2x"></i>
                                        </figcaption>
                                    </figure>
                                </a><!-- /.g__item --></div><!-- /.col- -->
                            <div class="col-6 col-md-4"><a href="../assets/global/images/blog-img-14.jpg"
                                                           class="g__item rounded"
                                                           data-lightbox="profile-gallery" data-title="image">
                                    <figure class="g__figure"><img src="../assets/global/images/blog-img-14.jpg"
                                                                   alt="">
                                        <figcaption class="g__overlay"><i class="g__zoom fa fa-search fa-2x"></i>
                                        </figcaption>
                                    </figure>
                                </a><!-- /.g__item --></div><!-- /.col- -->
                            <div class="col-6 col-md-4"><a href="../assets/global/images/blog-img-15.jpg"
                                                           class="g__item rounded"
                                                           data-lightbox="profile-gallery" data-title="image">
                                    <figure class="g__figure"><img src="../assets/global/images/blog-img-15.jpg"
                                                                   alt="">
                                        <figcaption class="g__overlay"><i class="g__zoom fa fa-search fa-2x"></i>
                                        </figcaption>
                                    </figure>
                                </a><!-- /.g__item --></div><!-- /.col- -->
                            <div class="col-6 col-md-4"><a href="../assets/global/images/blog-img-16.jpg"
                                                           class="g__item rounded"
                                                           data-lightbox="profile-gallery" data-title="image">
                                    <figure class="g__figure"><img src="../assets/global/images/blog-img-16.jpg"
                                                                   alt="">
                                        <figcaption class="g__overlay"><i class="g__zoom fa fa-search fa-2x"></i>
                                        </figcaption>
                                    </figure>
                                </a><!-- /.g__item --></div><!-- /.col- -->
                            <div class="col-6 col-md-4"><a href="../assets/global/images/blog-img-17.jpg"
                                                           class="g__item rounded"
                                                           data-lightbox="profile-gallery" data-title="image">
                                    <figure class="g__figure"><img src="../assets/global/images/blog-img-17.jpg"
                                                                   alt="">
                                        <figcaption class="g__overlay"><i class="g__zoom fa fa-search fa-2x"></i>
                                        </figcaption>
                                    </figure>
                                </a><!-- /.g__item --></div><!-- /.col- -->
                            <div class="col-6 col-md-4"><a href="../assets/global/images/blog-img-18.jpg"
                                                           class="g__item rounded"
                                                           data-lightbox="profile-gallery" data-title="image">
                                    <figure class="g__figure"><img src="../assets/global/images/blog-img-18.jpg"
                                                                   alt="">
                                        <figcaption class="g__overlay"><i class="g__zoom fa fa-search fa-2x"></i>
                                        </figcaption>
                                    </figure>
                                </a><!-- /.g__item --></div><!-- /.col- -->
                            <div class="col-6 col-md-4"><a href="../assets/global/images/blog-img-19.jpg"
                                                           class="g__item rounded"
                                                           data-lightbox="profile-gallery" data-title="image">
                                    <figure class="g__figure"><img src="../assets/global/images/blog-img-19.jpg"
                                                                   alt="">
                                        <figcaption class="g__overlay"><i class="g__zoom fa fa-search fa-2x"></i>
                                        </figcaption>
                                    </figure>
                                </a><!-- /.g__item --></div><!-- /.col- --></div><!-- /.row --></div>
                    <!-- /.img-gallery --></div><!-- /#profile-photos -->
                <div class="tab-pane" id="profile-settings" role="tabpanel">
                    <div class="edit-cover mb-4"><img class="img-fluid"
                                                      src="../assets/global/images/profile-cover-2.jpg" alt="">
                        <button class="btn btn-sm btn-success px-4">Edit</button>
                    </div><!-- /.edit-cover -->
                    <div class="row">
                        <div class="col-xl-2 col-md-3">
                            <div class="d-flex flex-wrap align-content-start justify-content-between">
                                <div><a href="#"><img src="../assets/global/images/profile-avatar-2.jpg" alt=""
                                                      class="img-thumbnail mb-3"></a>
                                    <div>
                                        <button class="btn btn-primary btn-sm px-3 mr-2">Edit</button>
                                        <button class="btn btn-success btn-sm">X</button>
                                    </div>
                                </div>
                                <div class="mt-5">
                                    <div class="mb-3"><strong>Your Gender:</strong></div>
                                    <label class="d-block custom-control custom-radio"><input id="radio1"
                                                                                              name="radio"
                                                                                              type="radio"
                                                                                              class="custom-control-input">
                                        <span class="custom-control-indicator"></span> <span
                                                class="custom-control-description">Male</span></label><label
                                            class="d-block custom-control custom-radio"><input id="radio2"
                                                                                               name="radio"
                                                                                               checked="checked"
                                                                                               type="radio"
                                                                                               class="custom-control-input">
                                        <span class="custom-control-indicator"></span> <span
                                                class="custom-control-description">Female</span></label></div>
                            </div><!-- /.d-flex --></div><!-- /.col- -->
                        <div class="col-xl-10 col-md-9">
                            <form action="#"><h5 class="my-4">Basic Information</h5>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group"><label for="edit__name">YOUR NAME</label><input
                                                    type="text" id="edit__name" class="form-control"
                                                    value="DWIGHT GULLY"></div>
                                    </div><!-- /.col- -->
                                    <div class="col-md-6">
                                        <div class="form-group"><label for="edit__password">YOUR
                                                PASSWORD</label><input type="password" id="edit__password"
                                                                       class="form-control" value="fake-password">
                                        </div>
                                    </div><!-- /.col- -->
                                    <div class="col-md-12"><label for="edit__bio">WHO AM I</label><textarea
                                                name="edit__bio" id="edit__bio" class="form-control" rows="6">vichyssoise aetheogamous care callosal prothetically Iberism stratospherical eozoon gentianose spermotoxin bibitory pterotheca unportraited trimodal benzol</textarea>
                                    </div>
                                </div><!-- /.row --><h5 class="my-4 profile-edit-section-heading">Contact
                                    Information</h5>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group"><label for="edit__email">EMAIL</label><input
                                                    type="email" id="edit__email" class="form-control"
                                                    value="someone@example.com"></div>
                                    </div><!-- /.col- -->
                                    <div class="col-md-6">
                                        <div class="form-group"><label for="edit__website">WEBSITE</label><input
                                                    type="url" id="edit__website" class="form-control"
                                                    value="http://www.example.com/"></div>
                                    </div><!-- /.col- -->
                                    <div class="col-md-6">
                                        <div class="form-group"><label for="edit__phone">PHONE</label><input
                                                    type="text" id="edit__phone" class="form-control"
                                                    value="000-5421-524"></div>
                                    </div><!-- /.col- -->
                                    <div class="col-md-6">
                                        <div class="form-group"><label for="edit__skype">SKYPE</label><input
                                                    type="text" id="edit__skype" class="form-control"
                                                    value="myskype"></div>
                                    </div><!-- /.col- --></div><!-- /.row --><h5
                                        class="my-4 profile-edit-section-heading">General Information</h5>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group"><label for="edit__job">JOB</label><input
                                                    type="text" id="edit__job" class="form-control"
                                                    value="Web Developer"></div>
                                    </div><!-- /.col- -->
                                    <div class="col-md-6">
                                        <div class="form-group"><label
                                                    for="edit__position">POSITION</label><input type="text"
                                                                                                id="edit__position"
                                                                                                class="form-control"
                                                                                                value="Team Manager">
                                        </div>
                                    </div><!-- /.col- -->
                                    <div class="col-md-6">
                                        <div class="form-group"><label for="edit__major">STUDIED</label><input
                                                    type="text" id="edit__major" class="form-control"
                                                    value="computer science"></div>
                                    </div><!-- /.col- -->
                                    <div class="col-md-6">
                                        <div class="form-group"><label for="edit__school">HIGH
                                                SCHOOL</label><input type="text" id="edit__school"
                                                                     class="form-control" value="Fake High school">
                                        </div>
                                    </div><!-- /.col- --></div><!-- /.row -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <button class="btn btn-block btn-success mt-5">Save</button>
                                    </div><!-- /.col- --></div><!-- /.row --></form>
                        </div><!-- /.col- --></div><!-- /.row --></div><!-- /#profile-settings -->
            </div>
            <!-- /.tab-content -->
        </div><!-- /.profile-section-main -->
    </div><!-- /.profile-wrapper -->


    <!-- /.stream-post -->

@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/waypoints/lib/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/waypoints/lib/shortcuts/sticky.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/counterup/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/js/jquery.sparkline.min.js')}}"></script>
    <script src="{{asset('js/site.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
@stop