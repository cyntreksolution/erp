<?php

namespace SupplierManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
    use SoftDeletes;

    protected $table = 'supplier';

    protected $primaryKey = 'id';

    protected $dates = ['deleted_at'];

    public function rawMaterials(){
        return $this->belongsTo('RawMaterialManager\Models\RawMaterial','raw_material_id','raw_material_id');
    }
}
