<?php

namespace ProductionManagerManager\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use OwnerManager\Models\Owner;
use ProductionManagerManager\Models\ProductionManager;
use RecipeManager\Models\Recipe;
use SemiFinishProductManager\Models\SemiFinishProduct;

class ProductionManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect(route('production_manager.create'));
        return view('ProductionManagerManager::production_manager.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $production = ProductionManager::whereHas('roles', function ($query) {
            $query->where('slug', 'like', 'production-manager');
        })->get();
        return view('ProductionManagerManager::production_manager.create')->with([
            'pms' => $production
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        DB::transaction(function () use ($request) {
            $credentials = [
                'first_name' => $request->fn,
                'last_name' => $request->ln,
                'email' => $request->email,
                'password' => $request->nic,
                'created_by' =>Auth::user()->id,
                'color' => rand() % 7,
            ];
            $user = Sentinel::registerAndActivate($credentials);
            $role = Sentinel::findRoleBySlug('production-manager');
            $role->users()->attach($user);

        });

        return redirect(route('production_manager.create'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => $request->fn . ' ' . $request->ln . ' has been added as an Buntalk Production Manager.'
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \ProductionManagerManager\Models\ProductionManager $production_manager
     * @return \Illuminate\Http\Response
     */
    public function show(ProductionManager $production_manager)
    {
        return view('ProductionManagerManager::production_manager.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \ProductionManagerManager\Models\ProductionManager $production_manager
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductionManager $production_manager)
    {
        return view('ProductionManagerManager::production_manager.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \ProductionManagerManager\Models\ProductionManager $production_manager
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductionManager $production_manager)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \ProductionManagerManager\Models\ProductionManager $production_manager
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductionManager $production_manager)
    {
        $production_manager->delete();
        if ($production_manager->trashed()) {
            return 'true';
        } else {
            return 'false';
        }

    }
}
