<?php

namespace ProductionManagerManager;

use Illuminate\Support\ServiceProvider;

class ProductionManagerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/Views', 'ProductionManagerManager');
        require __DIR__ . '/web.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('ProductionManagerManager', function($app){
            return new ProductionManagerManager;
        });
    }
}
