<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web','log'])->group(function () {
    Route::prefix('production_manager')->group(function () {
        Route::get('', 'ProductionManagerManager\Controllers\ProductionManagerController@index')->name('production_manager.index');

        Route::get('create', 'ProductionManagerManager\Controllers\ProductionManagerController@create')->name('production_manager.create');

        Route::get('{production_manager}', 'ProductionManagerManager\Controllers\ProductionManagerController@show')->name('production_manager.show');

        Route::get('{production_manager}/edit', 'ProductionManagerManager\Controllers\ProductionManagerController@edit')->name('production_manager.edit');


        Route::post('', 'ProductionManagerManager\Controllers\ProductionManagerController@store')->name('production_manager.store');

        Route::put('{production_manager}', 'ProductionManagerManager\Controllers\ProductionManagerController@update')->name('production_manager.update');

        Route::delete('{production_manager}', 'ProductionManagerManager\Controllers\ProductionManagerController@destroy')->name('production_manager.destroy');
    });
});
