<?php

namespace ProductionManagerManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use UserManager\Models\Users;

class ProductionManager extends Users
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
}
