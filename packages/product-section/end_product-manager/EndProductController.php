<?php

namespace EndProductManager\Controllers;


use App\AgentBuffer;
use App\SalesEndProduct;
use App\Variant;
use BurdenManager\Models\BurdenPayment;
use EndProductManager\Models\Category;
use EndProductManager\Models\EndProduct;
use EndProductRecipeManager\Models\EndProductRecipe;
use Illuminate\Support\Facades\Response;
use MenuManager\Classes\AdminMenu;
use RawMaterialManager\Models\RawMaterial;
use Sentinel;
use StockManager\Models\StockEndProduct;
use Validator;
use EndProductManager\Classes\ProductContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PackingItemManager\Models\PackingItem;
use SemiFinishProductManager\Models\SemiFinishProduct;
use GuzzleHttp;

class EndProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        return redirect(route('end_products.create'));
        $categories = Category::all()->pluck('name', 'id');
        return view('EndProductManager::end_product.index')->with([
            'categories' => $categories
        ]);
    }
    public function enable_frac($id)
    {


        $data = EndProduct::find($id);
        $data->is_fractional = 1;
        $data->save();
        return redirect()->back();
    }

    public function disable_frac($id)
    {
        $data = EndProduct::find($id);
        $data->is_fractional = 0;
        $data->save();
        return redirect()->back();
    }

    public function act($id)
    {


        $data = EndProduct::find($id);
        $data->status = 1;
        $data->save();
        return redirect()->back();
    }

    public function inact($id)
    {
        $data = EndProduct::find($id);
        $data->status = 0;
        $data->save();
        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $endproduct = EndProduct::all();
        $semiproduct = SemiFinishProduct::all();
        $packitem = PackingItem::all();
        $categories = Category::all();
        $products = RawMaterial::withoutGlobalScope('type')->get();
        return view('EndProductManager::end_product.create')->with([
            'endproducts' => $endproduct,
            'semiproducts' => $semiproduct,
            'packitems' => $packitem,
            'categories' => $categories,
            'products' => $products,

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'semi_produts' => 'required',
            'packing_items' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect(route('end_products.create'))
                ->with([
                    'error' => true,
                    'error.title' => 'Error !',
                    'error.message' => 'Packing Item or Semi Finish Product Not Selected'
                ])
                ->withInput();
        } else {
            $name = $request->end_product_name;
            $barcode = $request->barcode;
            $weight = $request->weight;
            $semi_products = $request->semi_produts;
            $semi_products_qty = $request->semi_products_qty;
            $raw_materials = $request->raw_material;
            $raw_materials_qty = $request->raw_material_qty;
            $packing_items = $request->packing_items;
            $packing_items_qty = $request->packing_items_qty;
            ($request->buffer_stock == NULL) ? $buffer_strock = 0 : $buffer_strock = $request->buffer_stock;
            ($request->d_price == NULL) ? $d_price = 0 : $d_price = $request->d_price;
            ($request->s_price == NULL) ? $s_price = 0 : $s_price = $request->s_price;
            ($request->p_price == NULL) ? $p_price = 0 : $p_price = $request->p_price;
            ($request->commission == NULL) ? $commission = 0 : $commission = $request->commission;
            ($request->unit_discount == NULL) ? $unit_discount = 0 : $unit_discount = $request->unit_discount;
            ($request->transport_allowance == NULL) ? $transport_allowance = 0 : $transport_allowance = $request->transport_allowance;
            ($request->vat == NULL) ? $vat = 0 : $vat = $request->vat;
            ($request->nbt == NULL) ? $nbt = 0 : $nbt = $request->nbt;
            ($request->other_taxes == NULL) ? $other_taxes = 0 : $other_taxes = $request->other_taxes;
            ($request->mr == NULL) ? $mr = 0 : $mr = $request->mr;
            ($request->damage == NULL) ? $damage = 0 : $damage = $request->damage;
            $desc = $request->desc;
            $image = null;
            $path = null;
            if (isset($request->image)) {
                $file = $request->file('image');
                $path = '/uploads/images/end_products/';
                $destinationPath = storage_path($path);
                $file->move($destinationPath, $file->getClientOriginalName());
                $image = $file->getClientOriginalName();
            }
            $endProduct = new EndProduct();
            $endProduct->stock_id = 1;
            $endProduct->category_id = $request->category_id;
            $endProduct->barcode = $barcode;
            $endProduct->name = $name;
            $endProduct->weight = $weight;
            $endProduct->buffer_stock = $buffer_strock;
            $endProduct->distributor_price = $d_price;
            $endProduct->selling_price = $s_price;
            $endProduct->printed_price = $p_price;
            $endProduct->commission = $commission;
            $endProduct->unit_discount = $unit_discount;
            $endProduct->transport_allowance = $transport_allowance;
            $endProduct->vat = $vat;
            $endProduct->nbt = $nbt;
            $endProduct->other_taxes = $other_taxes;
            $endProduct->mr = $mr;
            $endProduct->damage = $damage;
            $endProduct->colour = rand() % 7;
            $endProduct->created_by = Sentinel::getUser()->id;
            $endProduct->desc = $desc;
            $endProduct->image = $image;
            $endProduct->image_path = 'storage' . $path;
            $endProduct->save();


            //sales db end product
            $sales_end_product = new SalesEndProduct();
            $sales_end_product->id = $endProduct->id;
            $sales_end_product->stock_id = 1;
            $sales_end_product->category_id = $request->category_id;
            $sales_end_product->barcode = $barcode;
            $sales_end_product->name = $name;
            $sales_end_product->weight = $weight;
            $sales_end_product->buffer_stock = $buffer_strock;
            $sales_end_product->distributor_price = $d_price;
            $sales_end_product->selling_price = $s_price;
            $sales_end_product->printed_price = $p_price;
            $sales_end_product->commission = $commission;
            $sales_end_product->unit_discount = $unit_discount;
            $sales_end_product->transport_allowance = $transport_allowance;
            $sales_end_product->vat = $vat;
            $sales_end_product->nbt = $nbt;
            $sales_end_product->other_taxes = $other_taxes;
            $sales_end_product->mr = $mr;
            $sales_end_product->damage = $damage;
            $sales_end_product->colour = rand() % 7;
            $sales_end_product->created_by = Sentinel::getUser()->id;
            $sales_end_product->desc = $desc;
            $sales_end_product->image = $image;
            $sales_end_product->image_path = 'storage' . $path;
            $sales_end_product->save();


            foreach ($semi_products as $key => $product) {
                $endProduct->semiProducts()->attach($product, ['qty' => $semi_products_qty[$key]]);
            }

            if (!empty($raw_materials) && sizeof($raw_materials) > 0) {
                foreach ($raw_materials as $key => $material) {
                    $endProduct->rawMaterials()->attach($material, ['qty' => $raw_materials_qty[$key]]);
                }
            }

            foreach ($packing_items as $key => $item) {
                $endProduct->packingItems()->attach($item, ['qty' => $packing_items_qty[$key]]);
            }


            return redirect(route('end_products.create'));

        }
    }

    /**
     * Display the specified resource.
     *
     * @param \EndProductManager\Models\EndProduct $end_product
     * @return \Illuminate\Http\Response
     */
    public function show(EndProduct $end_product)
    {

        $recipes = EndProductRecipe::whereEndProductId($end_product->id)->get();

        $semi = EndProduct::with('semiProducts')
            ->where('id', '=', $end_product->id)
            ->first();
        $packing = EndProduct::with('packingItems')
            ->where('id', '=', $end_product->id)
            ->first();

        $burdenPayments=BurdenPayment::where('pro_id', '=', $end_product->id)
            ->where('pro_type', '=', 2)
            ->get();
//        return $semi;
//        return $packing;
        return view('EndProductManager::end_product.show')->with([
            'end_product' => $end_product,
            'semi' => $semi,
            'packing' => $packing,
            'recipes' => $recipes,
            'burdenPayments'=>$burdenPayments
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \EndProductManager\Models\EndProduct $end_product
     * @return \Illuminate\Http\Response
     */
    public function edit(EndProduct $end_product)
    {
        return view('EndProductManager::end_product.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \EndProductManager\Models\EndProduct $end_product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EndProduct $end_product)
    {
        $salesEndProduct = SalesEndProduct::find($end_product->id);

        if (isset($request->name)) {
            $end_product->name = $request->name;
            $end_product->save();
            $salesEndProduct->name = $end_product->name;
            $salesEndProduct->save();
            return redirect('end_product/' . $end_product->id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'name has been updated'
            ]);
        }


        if (isset($request->available_qty)) {
            $end_product->available_qty = $request->available_qty;
            $end_product->save();
            return redirect('end_product/' . $end_product->id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'available quantity has been updated'
            ]);
        }


        if (isset($request->barcode)) {
            $end_product->barcode = $request->barcode;
            $end_product->save();

            $salesEndProduct->barcode = $end_product->barcode;
            $salesEndProduct->save();
            return redirect('end_product/' . $end_product->id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Barcode has been updated'
            ]);
        }

        if (isset($request->weight)) {
            $end_product->weight = $request->weight;
            $end_product->save();
            return redirect('end_product/' . $end_product->id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Weight has been updated'
            ]);
        }

        if (isset($request->bstock)) {
            $end_product->buffer_stock = $request->bstock;
            $end_product->save();
            return redirect('end_product/' . $end_product->id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Buffer stock has been updated'
            ]);
        }

        if (isset($request->mr)) {
            $end_product->mr = $request->mr;
            $end_product->save();
            return redirect('end_product/' . $end_product->id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Mr has been updated'
            ]);
        }

        if (isset($request->damage)) {
            $end_product->damage = $request->damage;
            $end_product->save();
            return redirect('end_product/' . $end_product->id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Damages has been updated'
            ]);
        }

        if (isset($request->desc)) {
            $end_product->desc = $request->desc;
            $end_product->save();

            $salesEndProduct->desc = $end_product->desc;
            $salesEndProduct->save();

            return redirect('end_product/' . $end_product->id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Description has been updated'
            ]);
        }


        if (isset($request->distributor_price)) {
            $end_product->distributor_price = $request->distributor_price;
            $end_product->save();

            $salesEndProduct->distributor_price = $end_product->distributor_price;
            $salesEndProduct->save();
            return redirect('end_product/' . $end_product->id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Distributer price has been updated'
            ]);
        }

        if (isset($request->selling_price)) {
            $end_product->selling_price = $request->selling_price;
            $end_product->save();

            $salesEndProduct->selling_price = $end_product->selling_price;
            $salesEndProduct->save();
            return redirect('end_product/' . $end_product->id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Selling Price has been updated'
            ]);
        }

        if (isset($request->printed_price)) {
            $end_product->printed_price = $request->printed_price;
            $end_product->save();

            $salesEndProduct->printed_price = $end_product->printed_price;
            $salesEndProduct->save();
            return redirect('end_product/' . $end_product->id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Printed Price has been updated'
            ]);
        }

        if (isset($request->commission)) {
            $end_product->commission = $request->commission;
            $end_product->save();

            $salesEndProduct->commission = $end_product->commission;
            $salesEndProduct->save();
            return redirect('end_product/' . $end_product->id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'commission has been updated'
            ]);
        }

        if (isset($request->unit_discount)) {
            $end_product->unit_discount = $request->unit_discount;
            $end_product->save();

            $salesEndProduct->unit_discount = $end_product->unit_discount;
            $salesEndProduct->save();
            return redirect('end_product/' . $end_product->id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Unit discount has been updated'
            ]);
        }

        if (isset($request->transport_allowance)) {
            $end_product->transport_allowance = $request->transport_allowance;
            $end_product->save();
            return redirect('end_product/' . $end_product->id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => '	Transport allowance has been updated'
            ]);
        }

        if (isset($request->vat)) {
            $end_product->vat = $request->vat;
            $end_product->save();

            $salesEndProduct->vat = $end_product->vat;
            $salesEndProduct->save();
            return redirect('end_product/' . $end_product->id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'VAT has been updated'
            ]);
        }

        if (isset($request->nbt)) {
            $end_product->nbt = $request->nbt;
            $end_product->save();

            $salesEndProduct->nbt = $end_product->nbt;
            $salesEndProduct->save();
            return redirect('end_product/' . $end_product->id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'NBT has been updated'
            ]);
        }

        if (isset($request->other_taxes)) {
            $end_product->other_taxes = $request->other_taxes;
            $end_product->save();

            $salesEndProduct->other_taxes = $end_product->other_taxes;
            $salesEndProduct->save();
            return redirect('end_product/' . $end_product->id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Other taxes has been updated'
            ]);
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \EndProductManager\Models\EndProduct $end_product
     * @return \Illuminate\Http\Response
     */

    public function stock(Request $request)
    {
        if (isset($request->filter)) {
            $category = $request->category;
            $data = EndProduct::with('stockEndProduct')
                ->orderBy('end_product.name')->where('category_id', '=', $category)->get();
        } else {
            $data = EndProduct::with('stockEndProduct')
                ->orderBy('end_product.name')->get();
        }


        $jsonList = array();
        $i = 1;

        $stock_value = 0;
        $stock_weight = 0;
        foreach ($data as $key => $item) {
            $dd = array();
            array_push($dd, $i);
            array_push($dd, $item->name);
            array_push($dd, number_format($item->buffer_stock, 3));
            array_push($dd, $item->available_qty);
            array_push($dd, '');
            array_push($dd, number_format($item->selling_price, 2));
            array_push($dd, number_format($item->printed_price, 2));
            array_push($dd, number_format($item->weight, 3));
            array_push($dd, number_format($item->available_qty * $item->selling_price, 2));
            array_push($dd, number_format($item->available_qty * $item->weight, 3));
            array_push($jsonList, $dd);
            $stock_value = $stock_value + ($item->available_qty * $item->selling_price);
            $stock_weight = $stock_weight + ($item->available_qty * $item->weight);
            $i++;
        }

        $ddx = [];
        array_push($ddx, 'Total');
        array_push($ddx, '');
        array_push($ddx, '');
        array_push($ddx, '');
        array_push($ddx, '');
        array_push($ddx, '');
        array_push($ddx, '');
        array_push($ddx, '');
        array_push($ddx, number_format($stock_value, 2));
        array_push($ddx, number_format($stock_weight, 3));
        array_push($jsonList, $ddx);


        return Response::json(array('data' => $jsonList));
    }


    public function destroy(EndProduct $end_product)
    {

        $end_product->delete();
        if ($end_product->trashed()) {
            return 'true';
        } else {
            return 'false';
        }
    }

    public function getendpro(Request $request)
    {
        return $endpro = EndProduct::where('id', '=', $request->id)->first();
    }

    public function getReturn(Request $request)
    {
        $endpro = EndProduct::where('id', '=', $request->id)->first();
        $variant = Variant::whereEndProductId($request->id)->whereReturnAccepted(1)->first();
        if (!empty($variant)) {
            $selling_price = $endpro->selling_price * $variant->size;
            $id = $endpro->id;
            $name = $endpro->name;
            $array = [];
            $array['selling_price'] = $selling_price;
            $array['id'] = $id;
            $array['name'] = $name;
            return $array;
        } else {
            return $endpro;
        }
    }

    public function createSubCategory(Request $request)
    {
        $subCategory = $request->cat_name;
        $category = 1;
        $cat = new Category();
        $cat->name = $subCategory;
        $cat->loading_type = $request->loading_type;
        $cat->save();

//        $client = new GuzzleHttp\Client();
//        $res = $client->post(API::baseURL().'add_subcategory',  [GuzzleHttp\RequestOptions::JSON => ['name' =>  $subCategory,'cat_id' =>  '1']]);
////        echo $res->getStatusCode(); // 200
////        echo $res->getBody();

        return redirect(route('end_products.create'));

    }

    public function delpay($id)
    {

        $data = BurdenPayment::find($id);

        $end_product = EndProduct::where('id','=',$data->pro_id)->first();
        $data->delete();

        $recipes = EndProductRecipe::whereEndProductId($end_product->id)->get();

        $semi = EndProduct::with('semiProducts')
            ->where('id', '=', $end_product->id)
            ->first();
        $packing = EndProduct::with('packingItems')
            ->where('id', '=', $end_product->id)
            ->first();

        $burdenPayments=BurdenPayment::where('pro_id', '=', $end_product->id)
            ->where('pro_type', '=', 2)
            ->get();
//        return $semi;
//        return $packing;
        return view('EndProductManager::end_product.show')->with([
            'end_product' => $end_product,
            'semi' => $semi,
            'packing' => $packing,
            'recipes' => $recipes,
            'burdenPayments'=>$burdenPayments
        ]);

    }

    public function products(Request $request)
    {

        $agent = $request->agent;
        $day = $request->day;
        $time = $request->time;
        $category = $request->category;

        $data = AgentBuffer::whereAgentId($agent)->where('day', '=', AgentBuffer::day[$day])->where('time', '=', AgentBuffer::time[$time])->whereCategoryId($category)->first();
        if (!empty($data)) {
            return ['success' => false, 'message' => 'Duplicate Data'];
        } else {
            $category = Category::find($category);
            $items = collect();
            $variants = collect();
            foreach ($category->products as $product) {
                if (!empty($product->variants) && sizeof($product->variants) > 0) {
                    foreach ($product->variants as $variant) {
                        $variants->push(['id' => $variant->id, 'name' => $product->name . ' ' . $variant->variant]);
                    }
                } else {
                    $items->push(['id' => $product->id, 'name' => $product->name]);
                }
            }
//            return ['success' => true, 'message' => $category->products->pluck('name','id')];
            return ['success' => true, 'message' => ['items' => $items->pluck('name', 'id'), 'variants' => $variants->pluck('name', 'id')]];
        }

    }

    public function updateReusable(Request $request, $end_product)
    {
        $endpro = EndProduct::where('id', '=', $end_product)->first();
        $endpro->reusable_type = $request->reusableType;
        $endpro->save();
        return redirect(route('end_products.show', $end_product));
    }


    public function customize(Request $request)
    {
        $id = $request->id;
        $end = EndProduct::find($id);
        $end->customize = !$end->customize;
        $end->save();
        return 'true';
    }

    public function recipeChange(Request $request)
    {
        $id = $request->id;
        $status = $request->status;
        $end_p_recipe = EndProductRecipe::find($id);
        $end_p = $end_p_recipe->end_product_id;
        if ($status == 0) {
            $recipes = EndProductRecipe::where('end_product_id', '=', $end_p)->whereStatus(1)->get();
            if ($recipes->count() >= 1) {
                $end_p_recipe->status = 0;
                $end_p_recipe->save();
            } else {
                return 'false';
            }
        } else {
            $recipes = EndProductRecipe::where('end_product_id', '=', $end_p)->whereStatus(1)->get();
            foreach ($recipes as $r) {
                $r->status = 0;
                $r->save();
            }
            $end_p_recipe->status = 1;
            $end_p_recipe->save();
        }
        return 'true';
    }


    public function burdendetail(Request $request)
    {

        $tp = 2;//2=End
        $data = new BurdenPayment();
        $data->pro_id = $request->end_id;
        $data->pro_type = $tp;
        $data->default_size = $request->default_size;
        $data->stage_id = $request->stage_id;
        $data->grade_a = $request->A;
        $data->grade_b = $request->B;
        $data->grade_c = $request->C;
        $data->grade_d = $request->D;

        $isin = BurdenPayment::where('pro_id','=',$request->end_id)
                            ->where('pro_type','=',$tp)
                            ->where('stage_id','=',$request->stage_id)
                            ->first();
        if(empty($isin)) {
            $data->save();
        }


        $endproduct = EndProduct::all();
        $semiproduct = SemiFinishProduct::all();
        $packitem = PackingItem::all();
        $categories = Category::all();
        $products = RawMaterial::withoutGlobalScope('type')->get();
        return view('EndProductManager::end_product.create')->with([
            'endproducts' => $endproduct,
            'semiproducts' => $semiproduct,
            'packitems' => $packitem,
            'categories' => $categories,
            'products' => $products,

        ]);
    }

    public function updateBurdenDetails(Request $request)
    {

        // dd($request);
        $end_product = $request->ep_id;
        $recipe_id = $request->recipe_id;
        $endpro = EndProductRecipe::where('id', '=', $recipe_id)->first();

        if (!empty($request->day_expire)) {
            $endpro->days_expire = $request->day_expire;
        }

        if (!empty($request->over_head)) {
            $endpro->over_head = $request->over_head;
        }

        if (!empty($request->dist_cost)) {
            $endpro->distribute_cost = $request->dist_cost;
        }

        if (!empty($request->b_weight)) {
            $endpro->burden_weight = $request->b_weight;
        }

        if (!empty($request->no_item)) {
            $endpro->burden_items = $request->no_item;
        }

        if (!empty($request->first_stage)) {
            $endpro->stage_1 = $request->first_stage;
        }

        if (!empty($request->second_stage)) {
            $endpro->stage_2 = $request->second_stage;
        }


        $endpro->save();

        return redirect(route('end_product.show', $end_product));
    }

    public function updateBurdenPayments(Request $request)
    {

        $id = $request->id;
        $data = BurdenPayment::find($id);
        $end_product = $request->pro_id;
        $data->grade_a = $request->A;
        $data->grade_b = $request->B;
        $data->grade_c = $request->C;
        $data->grade_d = $request->D;
        $data->save();


//        $client = new GuzzleHttp\Client();
//        $res = $client->post(API::baseURL().'add_subcategory',  [GuzzleHttp\RequestOptions::JSON => ['name' =>  $subCategory,'cat_id' =>  '1']]);
////        echo $res->getStatusCode(); // 200
////        echo $res->getBody();

        return redirect(route('end_product.show', $end_product));
    }
}
