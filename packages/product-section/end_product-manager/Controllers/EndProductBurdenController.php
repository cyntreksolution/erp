<?php

namespace EndProductBurdenManager\Controllers;

use App\EndProductBurdenGrade;
use App\Exports\BurdenExport;
use App\SemiIssueDataList;
use App\Traits\SemiFinishLog;
use Barryvdh\DomPDF\Facade as PDF;
use BurdenManager\Models\Burden;
use BurdenManager\Models\BurdenGrade;
use Carbon\Carbon;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use EmployeeManager\Models\Group;
use EmployeeManager\Models\GroupEmployee;
use EndProductBurdenManager\Models\BurdenPacking;
use EndProductBurdenManager\Models\BurdenRaw;
use EndProductBurdenManager\Models\BurdenSemi;
use EndProductBurdenManager\Models\TempBurdenPacking;
use EndProductBurdenManager\Models\TempBurdenRaw;
use EndProductBurdenManager\Models\TempBurdenSemi;
use EndProductBurdenManager\Models\EndProductBurden;
use EndProductManager\Models\EndProduct;
use EndProductRecipeManager\Models\EndProductRecipe;
use Exception;
use GradeManager\Models\Grade;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use EmployeeManager\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use RawMaterialManager\Models\RawMaterial;
use RawMaterialManager\Models\VirtualMaterial;
use RecipeManager\Models\RecipeSemiProduct;
use SemiFinishProductManager\Models\SemiFinishProduct;
use EndProductBurdenManager\Models\EndProductBurdenHeader;
use ShortEatsManager\Models\CookingRequest;
use StockManager\Classes\StockTransaction;
use BurdenManager\Models\ReusableProducts;

//newModel
use BurdenManager\Models\AllocateSemiFinish;

class EndProductBurdenController extends Controller
{
    use SemiFinishLog;

    public function index()
    {
        return redirect(route('eburden.create'));
    }

    public function create()
    {
        $burdens = EndProductBurden::where('status', '<', '5')->orderBy('status')->get();
        $endProducts = EndProduct::whereHas('category', function ($q) {
            return $q->whereLoadingType('to_loading');
        })->orderBy('category_id')->get()->pluck('name', 'id');
        // var_dump($endProducts);
        return view('EndProductBurdenManager::burden.create', compact('endProducts', 'burdens'));
    }

    public function store(Request $request)
    {


        $endProduct = $request->end_product_id;
        $burden_size = $request->burden_size;

        $active_recipe = EndProductRecipe::whereEndProductId($endProduct)->whereStatus(1)->first();

        if (empty($active_recipe)) {
            return redirect(route('eburden.create'))->with([
                'error' => true,
                'error.title' => 'Sorry !',
                'error.message' => 'No Active Recipe Found'
            ]);
        }

        $burdenCount = EndProductBurden::whereDate('created_at', '=', Carbon::today()->toDateString())->get()->count();
        $last_digit = $burdenCount + 1;
        $serial = date('ymd') . '-' . $last_digit;

        $burden_end = new EndProductBurden();
        $burden_end->end_product_id = $endProduct;
        $burden_end->serial = $serial;
        $burden_end->burden_size = $burden_size;
        $burden_end->expected_end_product_qty = $burden_size;
        $burden_end->recipe_id = $active_recipe->id;
        $burden_end->created_by = Sentinel::getUser()->id;
        $burden_end->save();;


        $burden_id = $request->burden_id;
        $burden_qty = $request->burden_qty;

        $cooking_request_id = $request->cooking_request_id;
        $cooking_request_qty = $request->cooking_request_qty;


        if (!empty($burden_id)) {
            foreach ($burden_id as $key => $burden) {
                $burden_data = BurdenGrade::find($key);

                $semi_id = Burden::find($burden_data->burden_id);
                //dd($semi_id);
                $allocation = new AllocateSemiFinish();
                $allocation->end_product_burden_id = $burden_end->id;
                $allocation->semi_finish_burden_id = $burden_data->burden_id;
                $allocation->allocate_qty = $burden_qty[$key];
                $allocation->burden_type = 'burden';
              // dd($semi_id);
                $allocation->save();




                $burden_data->used_qty = $burden_data->used_qty - $burden_qty[$key];
                $burden_data->save();

               // StockTransaction::semiFinishStockTransaction(2, $semi->semi_finish_product_id, '', $semi->qty, $bd->serial, 1);
               // StockTransaction::updateSemiFinishAvailableQty(2, $semi->semi_finish_product_id,  $semi->qty);
                $semi_product = SemiFinishProduct::find($semi_id->semi_finish_product_id);
                $this->recordSemiFinishProductOutstanding($semi_product,1,$burden_qty[$key], 'Allocate Semi Finish For Make End Product', $burden_data->id, 'EndProductBurdenManager\\Models\\EndProductBurdenController',$burden_data->burden_id,'AllocateEnd',2,'',$burden_end->id);
            }
        }
        if (!empty($cooking_request_id)) {
            foreach ($cooking_request_id as $key => $burden) {
                $allocation = new AllocateSemiFinish();
                $allocation->end_product_burden_id = $burden_end->id;
                $allocation->semi_finish_burden_id = $key;
                $allocation->allocate_qty = $cooking_request_qty[$key];
                $allocation->burden_type = 'cooking_request';
                $allocation->save();

                $burden_data = CookingRequest::find($key);
                $burden_data->used_qty = $burden_data->used_qty - $cooking_request_qty[$key];
                $burden_data->save();

               // StockTransaction::semiFinishStockTransaction(2, $semi->semi_finish_product_id, '', $semi->qty, $bd->serial, 1);
              //  StockTransaction::updateSemiFinishAvailableQty(2, $semi->semi_finish_product_id,  $semi->qty);
                $semi_product = SemiFinishProduct::find($burden_data->semi_finish_product_id);
                $this->recordSemiFinishProductOutstanding($semi_product,1,$cooking_request_qty[$key], 'Allocate Semi Finish For Make End Product', $key, 'EndProductBurdenManager\\Models\\EndProductBurdenController',$key,'AllocateEnd',2,'',$burden_end->id);

            }
        }

      $dummy = new Request();
      $this->mergeBurdensAllocation($dummy, [$burden_end->id]);

        //bypass as new CR
        return redirect(route('eburden.create'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => 'new burden created successfully'
        ]);
    }

    public function show(EndProductBurden $burden)
    {
        $burden = EndProductBurden::where('id', '=', $burden->id)
            ->with('semiProdcuts', 'recipe.recipeContents.materials')
            ->first();
        $date = Carbon::now();
        $teams = GroupEmployee::whereDate('created_at', $date)->get();

        return view('BurdenManager::burden.show', compact('teams'))->with([
            'burden' => $burden
        ]);
    }

    public function edit(EndProductBurden $burden)
    {
        $date = Carbon::now();
        $teams = Group::whereStatus(1)->whereDate('created_at', '=', Carbon::today()->toDateString())->get();
        return view('BurdenManager::burden.edit', compact('teams'))->with([
            'burden' => $burden,
        ]);
    }

    public function update(Request $request, EndProductBurden $burden)
    {
        $employee = $request->emp_group;
        try {
            DB::transaction(function () use ($employee, $burden) {
                $burdenEmp = new EndProductBurdenGroupEmployee();
                $burdenEmp->burden_id = $burden->id;
                $burdenEmp->group_id = $employee;
                $burdenEmp->save();

                $burden->status = 3;
                $burden->save();
            });

            return redirect(route('burden.index'))->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'dasfs'
            ]);
        } catch (Exception $e) {
            return $e;
            return redirect(route('burden.index'))->with([
                'error' => true,
                'error.title' => 'Sorry !',
                'error.message' => 'Something Went Wrong '
            ]);
        }
    }

    public function tableData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'serial', 'semi_finish_product_id', 'status', 'burden_size', 'recipe_id', 'created_at'];
        $order_column = $columns[$order_by[0]['column']];

        $burdens = EndProductBurden::tableData($order_column, $order_by_str, $start, $length);
        if ($request->filter == true) {
            $semi = $request->semi;
            $status = $request->status;
            $date_range = $request->date_range;
            $burdens = $burdens->filterData($semi, $status, $date_range)->get();
            $burdenCount = $burdens->count();
        } elseif (is_null($search) || empty($search)) {
            $burdens = $burdens->get();
            $burdenCount = EndProductBurden::all()->count();
        } else {
            $burdens = $burdens->searchData($search)->get();
            $burdenCount = $burdens->count();
        }

        $data[][] = array();
        $i = 0;


        foreach ($burdens as $key => $burden) {
            $btnView = '<a class="btn btn-outline-info btn-sm mr-1" target="_blank" href="' . route('burden.show', $burden->id) . '"> <i class="fa fa-eye"></i> </a>';
            $btnDelete = ($burden->status == 1) ? '<button class="btn btn-outline-danger btn-sm mr-1" onclick="deleteBurden(' . $burden->id . ')"> <i class="fa fa-trash"></i> </button>' : null;

            switch ($burden->status) {
                case 1:
                    $statusBtn = '<button class="btn btn-outline-warning btn-sm mr-1">pending</button>';
                    break;

                case 2:
                    $statusBtn = '<button class="btn btn-outline-info btn-sm mr-1">received</button>';
                    break;

                case 3:
                    $statusBtn = '<button class="btn btn-outline-primary btn-sm mr-1">creating</button>';
                    break;
                case 4:
                    $statusBtn = '<button class="btn btn-outline-danger btn-sm mr-1">burning</button>';
                    break;
                case 5:
                    $statusBtn = '<button class="btn btn-outline-success btn-sm mr-1">grade</button>';
                    break;
                case 6:
                    $statusBtn = '<button class="btn btn-outline-dark btn-sm mr-1">finished</button>';
                    break;
            }
            $data[$i] = array(
                ++$key,
                $burden->serial,
                $burden->semiProdcuts->name,
                $statusBtn,
                $burden->burden_size . ' KG',
                $burden->recipe->name,
                Carbon::parse($burden->created_at)->format('Y-m-d H:m:s'),
                $btnView . $btnDelete,
            );
            $i++;
        }

        if ($burdenCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($burdenCount),
            "recordsFiltered" => intval($burdenCount),
            "data" => $data
        ];

        return json_encode($json_data);
    }

    public function exportFilter(Request $request)
    {
        $semi = $request->semi;
        $status = $request->status;
        $date_range = $request->date_range;
        $records = EndProductBurden::filterData($semi, $status, $date_range)->get();
        return Excel::download(new BurdenExport($records), Carbon::now() . '_burden_report.xlsx');
    }

    public function mergeList(Request $request)
    {
        $burdens = EndProductBurden::whereStatus(1)->get();
        return view('EndProductBurdenManager::burden.merge-list', compact('burdens'));
    }

    public function mergedList(Request $request)
    {
        $merged = EndProductBurdenHeader::orderBy('created_at', 'asc')->get();
        return view('EndProductBurdenManager::burden.merged-list', compact('merged'));
    }


    public function mergeBurdens(Request $request, $burden_id = null)
    {
        $burdens = (empty($burden_id)) ? $request->burden_id : $burden_id;
        $endProductIdAll = [];
        $showRawTypes = [];
        $showSemiTypes = [];
        $showPackingTypes = [];
        $serial = 'EM-' . date('ymd') . '-' . random_int(10000, 99999);

        if (isset($request->saveButton) || !empty($burden_id)) {
            $serialCodeRegister = new EndProductBurdenHeader();
            $serialCodeRegister->serial = $serial;
            $serialCodeRegister->created_by = Sentinel::getUser()->id;
            $serialCodeRegister->burdens = json_encode($burdens);
            if (!empty($burden_id)) {
                $serialCodeRegister->issue_semi =1;
            }

            $serialCodeRegister->save();
            $serialNo = $serialCodeRegister->id;


        }

        foreach ($burdens as $burden) {
            $bd = EndProductBurden::find($burden);
            if ($bd->status == 2) {
                return "Burden Already Merged";
            }
            $unitsCount = $bd->burden_size;
            $endProductId = $bd->end_product_id;
            $endProductIdAll[] = $endProductId;
            $raws = $bd->recipe->rawContent()->get();
            $semis = $bd->recipe->semiContent()->get();
            $packing = $bd->recipe->packingContent()->get();
            foreach ($raws as $item) {
                $showRawTypes[] = $item->rawMaterial->name;
            }
            foreach ($semis as $item) {
                $showSemiTypes[] = $item->semiFinishProduct->name;
            }
            foreach ($packing as $item) {
                $showPackingTypes[] = $item->packingItem->name;
            }

            $category =0;
            if (isset($request->saveButton) || !empty($burden_id)) {
//                DB::transaction(function () use ($raws, $semis, $packing, $unitsCount, $serialNo, $endProductId, $bd) {
                $bd = EndProductBurden::find($bd->id);
                $bd->status = 2;
                $bd->save();

                $category = $bd->endProduct->category_id;
                foreach ($raws as $item) {
                    BurdenRaw::create([
                        'end_product_id' => $endProductId,
                        'merge_id' => $serialNo,
                        'raw_material_id' => $item->raw_material_id,
                        'raw_material_name' => $item->rawMaterial->name,
                        'qty' => ($item->units * $unitsCount
                        )]);

                }
                foreach ($semis as $item) {
                    $semi = BurdenSemi::create([
                        'end_product_id' => $endProductId,
                        'merge_id' => $serialNo,
                        'semi_finish_product_id' => $item->semi_finish_product_id,
                        'semi_finish_product_name' => $item->semiFinishProduct->name,
                        'qty' => ($item->units * $unitsCount)
                    ]);

                    

                }
                foreach ($packing as $item) {
                    BurdenPacking::create([
                        'end_product_id' => $endProductId,
                        'merge_id' => $serialNo,
                        'raw_material_id' => $item->packing_id,
                        'raw_material_name' => $item->packingItem->name,
                        'qty' => ($item->units * $unitsCount
                        )]);
                }
//                });
            }

            if (isset($request->saveButton) || !empty($burden_id)) {
                $serialCodeRegister = EndProductBurdenHeader::find($serialNo);
                $serialCodeRegister->category = $category;
                $serialCodeRegister->save();
            }
            if (isset($request->showButton)) {
                DB::transaction(function () use ($raws, $semis, $packing, $unitsCount, $serial, $endProductId) {
                    foreach ($raws as $item) {
                        TempBurdenRaw::create([
                            'end_product_id' => $endProductId,
                            'merge_id' => $serial,
                            'raw_material_id' => $item->raw_material_id,
                            'raw_material_name' => $item->rawMaterial->name,
                            'qty' => ($item->units * $unitsCount
                            )]);

                    }
                    foreach ($semis as $item) {
                        TempBurdenSemi::create([
                            'end_product_id' => $endProductId,
                            'merge_id' => $serial,
                            'semi_finish_product_id' => $item->semi_finish_product_id,
                            'semi_finish_product_name' => $item->semiFinishProduct->name,
                            'qty' => ($item->units * $unitsCount
                            )]);
                    }
                    foreach ($packing as $item) {
                        TempBurdenPacking::create([
                            'end_product_id' => $endProductId,
                            'merge_id' => $serial,
                            'packing_id' => $item->packing_id,
                            'packing_name' => $item->packingItem->name,
                            'qty' => ($item->units * $unitsCount
                            )]);
                    }
                });
            }

        }
        $showRawTypeUnique[] = array_unique($showRawTypes);
        $showSemiTypeUnique[] = array_unique($showSemiTypes);
        $showPackingTypeUnique[] = array_unique($showPackingTypes);
        // $serial = 'EM-200204-11921';
        $endProductIdAll = array_unique($endProductIdAll);
        foreach ($endProductIdAll as $endProductId) {
            $endProduct = EndProduct::whereId($endProductId)->first();
            $endProductName[] = $endProduct->name;
        }
        if (isset($request->showButton)) {
            $raw_materials_total = TempBurdenRaw::whereMergeId($serial)
                ->selectRaw(' COALESCE(sum(qty), 0) as qty, raw_material_id,raw_material_name')
                ->groupBy('raw_material_id')
                ->get();
            $count = 0;
            $dataRawProductPoints = array();

            foreach ($endProductIdAll as $endProductId) {
                $dataPoints = array();
                foreach ($showRawTypeUnique[0] as $showRawType) {
                    $raw_material = TempBurdenRaw::selectRaw(' COALESCE(sum(qty), 0) as qty, raw_material_id,raw_material_name')
                        ->whereEndProductId($endProductId)
                        ->whereRawMaterialName($showRawType)
                        ->whereMergeId($serial)
                        ->first();
                    if ($raw_material == Null) {
                        $dataPoints[] = array("type" => $showRawType, "qty" => 0);
                    } else {
                        $dataPoints[] = array("type" => $showRawType, "qty" => $raw_material->qty);
                    }
                }

                $dataRawProductPoints[] = array("end_product_name" => $endProductName[$count], "data" => $dataPoints);
                $count++;
            }

            $semi_finish_products_total = TempBurdenSemi::whereMergeId($serial)
                ->selectRaw(' COALESCE(sum(qty), 0) as qty, semi_finish_product_id,semi_finish_product_name')
                ->groupBy('semi_finish_product_id')
                ->get();
            $count = 0;
            $dataSemiProductPoints = array();

            foreach ($endProductIdAll as $endProductId) {
                $dataPoints = array();
                foreach ($showSemiTypeUnique[0] as $showSemiType) {
                    $semi_finish_product = TempBurdenSemi::selectRaw(' COALESCE(sum(qty), 0) as qty, semi_finish_product_id,semi_finish_product_name')
                        ->whereEndProductId($endProductId)
                        ->whereSemiFinishProductName($showSemiType)
                        ->whereMergeId($serial)
                        ->first();
                    if ($semi_finish_product == Null) {
                        $dataPoints[] = array("type" => $showSemiType, "qty" => 0);
                    } else {
                        $dataPoints[] = array("type" => $showSemiType, "qty" => $semi_finish_product->qty);
                    }
                }

                $dataSemiProductPoints[] = array("end_product_name" => $endProductName[$count], "data" => $dataPoints);
                $count++;
            }


            $packing_total = TempBurdenPacking::whereMergeId($serial)
                ->selectRaw(' COALESCE(sum(qty), 0) as qty, packing_id,packing_name')
                ->groupBy('packing_id')
                ->get();

            $count = 0;
            $dataPackingProductPoints = array();
            foreach ($endProductIdAll as $endProductId) {
                $dataPoints = array();
                foreach ($showPackingTypeUnique[0] as $showPackingType) {
                    $packing_product = TempBurdenPacking::selectRaw(' COALESCE(sum(qty), 0) as qty, packing_id,packing_name')
                        ->whereEndProductId($endProductId)
                        ->wherePackingName($showPackingType)
                        ->whereMergeId($serial)
                        ->first();
                    if ($packing_product == Null) {
                        $dataPoints[] = array("type" => $showPackingType, "qty" => 0);
                    } else {
                        $dataPoints[] = array("type" => $showPackingType, "qty" => $packing_product->qty);
                    }
                }

                $dataPackingProductPoints[] = array("end_product_name" => $endProductName[$count], "data" => $dataPoints);
                $count++;
            }
        }

        if (isset($request->saveButton) || !empty($burden_id)) {
            $raw_materials_total = BurdenRaw::whereMergeId($serialNo)
                ->selectRaw(' COALESCE(sum(qty), 0) as qty, raw_material_id,raw_material_name')
                ->groupBy('raw_material_id')
                ->get();
            $count = 0;
            $dataRawProductPoints = array();
            foreach ($endProductIdAll as $endProductId) {
                $dataPoints = array();
                foreach ($showRawTypeUnique[0] as $showRawType) {
                    $raw_material = BurdenRaw::selectRaw(' COALESCE(sum(qty), 0) as qty, raw_material_id,raw_material_name')
                        ->whereEndProductId($endProductId)
                        ->whereRawMaterialName($showRawType)
                        ->whereMergeId($serialNo)
                        ->first();
                    if ($raw_material == Null) {
                        $dataPoints[] = array("type" => $showRawType, "qty" => 0);
                    } else {
                        $dataPoints[] = array("type" => $showRawType, "qty" => $raw_material->qty);
                    }
                }

                $dataRawProductPoints[] = array("end_product_name" => $endProductName[$count], "data" => $dataPoints);
                $count++;
            }

            $semi_finish_products_total = BurdenSemi::whereMergeId($serialNo)
                ->selectRaw(' COALESCE(sum(qty), 0) as qty, semi_finish_product_id,semi_finish_product_name')
                ->groupBy('semi_finish_product_id')
                ->get();

            $count = 0;
            $dataSemiProductPoints = array();

            foreach ($endProductIdAll as $endProductId) {
                $dataPoints = array();
                foreach ($showSemiTypeUnique[0] as $showSemiType) {
                    $semi_finish_product = BurdenSemi::selectRaw(' COALESCE(sum(qty), 0) as qty, semi_finish_product_id,semi_finish_product_name')
                        ->whereEndProductId($endProductId)
                        ->whereSemiFinishProductName($showSemiType)
                        ->whereMergeId($serialNo)
                        ->first();
                    if ($semi_finish_product == Null) {
                        $dataPoints[] = array("type" => $showSemiType, "qty" => 0);
                    } else {
                        $dataPoints[] = array("type" => $showSemiType, "qty" => $semi_finish_product->qty);
                    }
                }

                $dataSemiProductPoints[] = array("end_product_name" => $endProductName[$count], "data" => $dataPoints);
                $count++;
            }

            $packing_total = BurdenPacking::whereMergeId($serialNo)
                ->selectRaw(' COALESCE(sum(qty), 0) as qty, raw_material_id,raw_material_name')
                ->groupBy('raw_material_id')
                ->get();

            $count = 0;
            $dataPackingProductPoints = array();
            foreach ($endProductIdAll as $endProductId) {
                $dataPoints = array();
                foreach ($showPackingTypeUnique[0] as $showPackingType) {
                    $packing_product = BurdenPacking::selectRaw(' COALESCE( COALESCE(sum(qty), 0), 0) as qty, raw_material_id,raw_material_name')
                        ->whereEndProductId($endProductId)
                        ->whereRawMaterialName($showPackingType)
                        ->whereMergeId($serialNo)
                        ->first();
                    if ($packing_product == Null) {
                        $dataPoints[] = array("type" => $showPackingType, "qty" => 0);
                    } else {
                        $dataPoints[] = array("type" => $showPackingType, "qty" => $packing_product->qty);
                    }
                }

                $dataPackingProductPoints[] = array("end_product_name" => $endProductName[$count], "data" => $dataPoints);
                $count++;
            }
        }

        return view('EndProductBurdenManager::burden.merge-print', compact('dataRawProductPoints', 'dataSemiProductPoints', 'dataPackingProductPoints', 'endProductIdAll', 'showRawTypeUnique', 'showSemiTypeUnique', 'showPackingTypeUnique', 'raw_materials_total', 'semi_finish_products_total', 'packing_total'));

    }

    public function mergeBurdensAllocation(Request $request, $burden_id = null)
    {
        $burdens = (empty($burden_id)) ? $request->burden_id : $burden_id;
        $endProductIdAll = [];
        $showRawTypes = [];
        $showSemiTypes = [];
        $showPackingTypes = [];
        $serial = 'EM-' . date('ymd') . '-' . random_int(10000, 99999);

        if (isset($request->saveButton) || !empty($burden_id)) {
            $serialCodeRegister = new EndProductBurdenHeader();
            $serialCodeRegister->serial = $serial;
            $serialCodeRegister->created_by = Sentinel::getUser()->id;
            $serialCodeRegister->burdens = json_encode($burdens);
            if (!empty($burden_id)) {
                $serialCodeRegister->issue_semi =1;
            }

            $serialCodeRegister->save();
            $serialNo = $serialCodeRegister->id;


        }

        foreach ($burdens as $burden) {
            $bd = EndProductBurden::find($burden);
            if ($bd->status == 2) {
                return "Burden Already Merged";
            }
            $unitsCount = $bd->burden_size;
            $endProductId = $bd->end_product_id;
            $endProductIdAll[] = $endProductId;
            $raws = $bd->recipe->rawContent()->get();
            $semis = $bd->recipe->semiContent()->get();
            $packing = $bd->recipe->packingContent()->get();
            foreach ($raws as $item) {
                $showRawTypes[] = $item->rawMaterial->name;
            }
            foreach ($semis as $item) {
                $showSemiTypes[] = $item->semiFinishProduct->name;
            }
            foreach ($packing as $item) {
                $showPackingTypes[] = $item->packingItem->name;
            }

            $category =0;
            if (isset($request->saveButton) || !empty($burden_id)) {
//                DB::transaction(function () use ($raws, $semis, $packing, $unitsCount, $serialNo, $endProductId, $bd) {
                $bd = EndProductBurden::find($bd->id);
                $bd->status = 2;
                $bd->save();

                $category = $bd->endProduct->category_id;
                foreach ($raws as $item) {
                    BurdenRaw::create([
                        'end_product_id' => $endProductId,
                        'merge_id' => $serialNo,
                        'raw_material_id' => $item->raw_material_id,
                        'raw_material_name' => $item->rawMaterial->name,
                        'qty' => ($item->units * $unitsCount
                        )]);

                }
                foreach ($semis as $item) {
                    $semi = BurdenSemi::create([
                        'end_product_id' => $endProductId,
                        'merge_id' => $serialNo,
                        'semi_finish_product_id' => $item->semi_finish_product_id,
                        'semi_finish_product_name' => $item->semiFinishProduct->name,
                        'qty' => ($item->units * $unitsCount)
                    ]);

                    if (!empty($burden_id)) {
                        $sm = BurdenSemi::find($semi->id);
                        $sm->send_qty = $semi->qty;
                        $sm->save();

                        $bd->issue_semi = 1;
                        $bd->save();


                        $json_data = json_encode($sm);
                       // StockTransaction::semiFinishStockTransaction(2, $semi->semi_finish_product_id, '', $semi->qty, $bd->serial, 1);
                       // StockTransaction::updateSemiFinishAvailableQty(2, $semi->semi_finish_product_id,  $semi->qty);


                        $record = new SemiIssueDataList();
                        $record->type=0;
                        $record->semi_id=$semi->semi_finish_product_id;
                        $record->qty=$semi->qty;
                        $record->description='Semi issue while merge process - SYSTEM GENERATED';
                        $record->data=json_encode($json_data);
                        $record->created_by= Sentinel::getUser()->id;
                        $record->save();
                      //  $semi_product = SemiFinishProduct::find($semi->semi_finish_product_id);
                       // $this->recordSemiFinishProductOutstanding($semi_product,1,-$semi->qty, 'For Burden Request', $record->id, 'App\\SemiIssueDataList');


                    }


                }
                foreach ($packing as $item) {
                    BurdenPacking::create([
                        'end_product_id' => $endProductId,
                        'merge_id' => $serialNo,
                        'raw_material_id' => $item->packing_id,
                        'raw_material_name' => $item->packingItem->name,
                        'qty' => ($item->units * $unitsCount
                        )]);
                }
//                });
            }

            if (isset($request->saveButton) || !empty($burden_id)) {
                $serialCodeRegister = EndProductBurdenHeader::find($serialNo);
                $serialCodeRegister->category = $category;
                $serialCodeRegister->save();
            }
            if (isset($request->showButton)) {
                DB::transaction(function () use ($raws, $semis, $packing, $unitsCount, $serial, $endProductId) {
                    foreach ($raws as $item) {
                        TempBurdenRaw::create([
                            'end_product_id' => $endProductId,
                            'merge_id' => $serial,
                            'raw_material_id' => $item->raw_material_id,
                            'raw_material_name' => $item->rawMaterial->name,
                            'qty' => ($item->units * $unitsCount
                            )]);

                    }
                    foreach ($semis as $item) {
                        TempBurdenSemi::create([
                            'end_product_id' => $endProductId,
                            'merge_id' => $serial,
                            'semi_finish_product_id' => $item->semi_finish_product_id,
                            'semi_finish_product_name' => $item->semiFinishProduct->name,
                            'qty' => ($item->units * $unitsCount
                            )]);
                    }
                    foreach ($packing as $item) {
                        TempBurdenPacking::create([
                            'end_product_id' => $endProductId,
                            'merge_id' => $serial,
                            'packing_id' => $item->packing_id,
                            'packing_name' => $item->packingItem->name,
                            'qty' => ($item->units * $unitsCount
                            )]);
                    }
                });
            }

        }
        $showRawTypeUnique[] = array_unique($showRawTypes);
        $showSemiTypeUnique[] = array_unique($showSemiTypes);
        $showPackingTypeUnique[] = array_unique($showPackingTypes);
        // $serial = 'EM-200204-11921';
        $endProductIdAll = array_unique($endProductIdAll);
        foreach ($endProductIdAll as $endProductId) {
            $endProduct = EndProduct::whereId($endProductId)->first();
            $endProductName[] = $endProduct->name;
        }
        if (isset($request->showButton)) {
            $raw_materials_total = TempBurdenRaw::whereMergeId($serial)
                ->selectRaw(' COALESCE(sum(qty), 0) as qty, raw_material_id,raw_material_name')
                ->groupBy('raw_material_id')
                ->get();
            $count = 0;
            $dataRawProductPoints = array();

            foreach ($endProductIdAll as $endProductId) {
                $dataPoints = array();
                foreach ($showRawTypeUnique[0] as $showRawType) {
                    $raw_material = TempBurdenRaw::selectRaw(' COALESCE(sum(qty), 0) as qty, raw_material_id,raw_material_name')
                        ->whereEndProductId($endProductId)
                        ->whereRawMaterialName($showRawType)
                        ->whereMergeId($serial)
                        ->first();
                    if ($raw_material == Null) {
                        $dataPoints[] = array("type" => $showRawType, "qty" => 0);
                    } else {
                        $dataPoints[] = array("type" => $showRawType, "qty" => $raw_material->qty);
                    }
                }

                $dataRawProductPoints[] = array("end_product_name" => $endProductName[$count], "data" => $dataPoints);
                $count++;
            }

            $semi_finish_products_total = TempBurdenSemi::whereMergeId($serial)
                ->selectRaw(' COALESCE(sum(qty), 0) as qty, semi_finish_product_id,semi_finish_product_name')
                ->groupBy('semi_finish_product_id')
                ->get();
            $count = 0;
            $dataSemiProductPoints = array();

            foreach ($endProductIdAll as $endProductId) {
                $dataPoints = array();
                foreach ($showSemiTypeUnique[0] as $showSemiType) {
                    $semi_finish_product = TempBurdenSemi::selectRaw(' COALESCE(sum(qty), 0) as qty, semi_finish_product_id,semi_finish_product_name')
                        ->whereEndProductId($endProductId)
                        ->whereSemiFinishProductName($showSemiType)
                        ->whereMergeId($serial)
                        ->first();
                    if ($semi_finish_product == Null) {
                        $dataPoints[] = array("type" => $showSemiType, "qty" => 0);
                    } else {
                        $dataPoints[] = array("type" => $showSemiType, "qty" => $semi_finish_product->qty);
                    }
                }

                $dataSemiProductPoints[] = array("end_product_name" => $endProductName[$count], "data" => $dataPoints);
                $count++;
            }


            $packing_total = TempBurdenPacking::whereMergeId($serial)
                ->selectRaw(' COALESCE(sum(qty), 0) as qty, packing_id,packing_name')
                ->groupBy('packing_id')
                ->get();

            $count = 0;
            $dataPackingProductPoints = array();
            foreach ($endProductIdAll as $endProductId) {
                $dataPoints = array();
                foreach ($showPackingTypeUnique[0] as $showPackingType) {
                    $packing_product = TempBurdenPacking::selectRaw(' COALESCE(sum(qty), 0) as qty, packing_id,packing_name')
                        ->whereEndProductId($endProductId)
                        ->wherePackingName($showPackingType)
                        ->whereMergeId($serial)
                        ->first();
                    if ($packing_product == Null) {
                        $dataPoints[] = array("type" => $showPackingType, "qty" => 0);
                    } else {
                        $dataPoints[] = array("type" => $showPackingType, "qty" => $packing_product->qty);
                    }
                }

                $dataPackingProductPoints[] = array("end_product_name" => $endProductName[$count], "data" => $dataPoints);
                $count++;
            }
        }

        if (isset($request->saveButton) || !empty($burden_id)) {
            $raw_materials_total = BurdenRaw::whereMergeId($serialNo)
                ->selectRaw(' COALESCE(sum(qty), 0) as qty, raw_material_id,raw_material_name')
                ->groupBy('raw_material_id')
                ->get();
            $count = 0;
            $dataRawProductPoints = array();
            foreach ($endProductIdAll as $endProductId) {
                $dataPoints = array();
                foreach ($showRawTypeUnique[0] as $showRawType) {
                    $raw_material = BurdenRaw::selectRaw(' COALESCE(sum(qty), 0) as qty, raw_material_id,raw_material_name')
                        ->whereEndProductId($endProductId)
                        ->whereRawMaterialName($showRawType)
                        ->whereMergeId($serialNo)
                        ->first();
                    if ($raw_material == Null) {
                        $dataPoints[] = array("type" => $showRawType, "qty" => 0);
                    } else {
                        $dataPoints[] = array("type" => $showRawType, "qty" => $raw_material->qty);
                    }
                }

                $dataRawProductPoints[] = array("end_product_name" => $endProductName[$count], "data" => $dataPoints);
                $count++;
            }

            $semi_finish_products_total = BurdenSemi::whereMergeId($serialNo)
                ->selectRaw(' COALESCE(sum(qty), 0) as qty, semi_finish_product_id,semi_finish_product_name')
                ->groupBy('semi_finish_product_id')
                ->get();

            $count = 0;
            $dataSemiProductPoints = array();

            foreach ($endProductIdAll as $endProductId) {
                $dataPoints = array();
                foreach ($showSemiTypeUnique[0] as $showSemiType) {
                    $semi_finish_product = BurdenSemi::selectRaw(' COALESCE(sum(qty), 0) as qty, semi_finish_product_id,semi_finish_product_name')
                        ->whereEndProductId($endProductId)
                        ->whereSemiFinishProductName($showSemiType)
                        ->whereMergeId($serialNo)
                        ->first();
                    if ($semi_finish_product == Null) {
                        $dataPoints[] = array("type" => $showSemiType, "qty" => 0);
                    } else {
                        $dataPoints[] = array("type" => $showSemiType, "qty" => $semi_finish_product->qty);
                    }
                }

                $dataSemiProductPoints[] = array("end_product_name" => $endProductName[$count], "data" => $dataPoints);
                $count++;
            }

            $packing_total = BurdenPacking::whereMergeId($serialNo)
                ->selectRaw(' COALESCE(sum(qty), 0) as qty, raw_material_id,raw_material_name')
                ->groupBy('raw_material_id')
                ->get();

            $count = 0;
            $dataPackingProductPoints = array();
            foreach ($endProductIdAll as $endProductId) {
                $dataPoints = array();
                foreach ($showPackingTypeUnique[0] as $showPackingType) {
                    $packing_product = BurdenPacking::selectRaw(' COALESCE( COALESCE(sum(qty), 0), 0) as qty, raw_material_id,raw_material_name')
                        ->whereEndProductId($endProductId)
                        ->whereRawMaterialName($showPackingType)
                        ->whereMergeId($serialNo)
                        ->first();
                    if ($packing_product == Null) {
                        $dataPoints[] = array("type" => $showPackingType, "qty" => 0);
                    } else {
                        $dataPoints[] = array("type" => $showPackingType, "qty" => $packing_product->qty);
                    }
                }

                $dataPackingProductPoints[] = array("end_product_name" => $endProductName[$count], "data" => $dataPoints);
                $count++;
            }
        }

        return view('EndProductBurdenManager::burden.merge-print', compact('dataRawProductPoints', 'dataSemiProductPoints', 'dataPackingProductPoints', 'endProductIdAll', 'showRawTypeUnique', 'showSemiTypeUnique', 'showPackingTypeUnique', 'raw_materials_total', 'semi_finish_products_total', 'packing_total'));

    }



    public function endProductMake(Request $request)
    {
        $burden = $request->burden;
        $date = Carbon::now()->format('y-m-d');
        $employers = Employee::all()->pluck('full_name', 'id');
        $teams = Group::whereDate('created_at', $date)->get()->pluck('name', 'id');
        return view('EndProductBurdenManager::burden.makeProduct', compact('teams', 'employers', 'burden'));
    }


    public function endProductBake(Request $request, EndProductBurden $burden)
    {
        $burden->employee_group = $request->employee_group;
        $burden->baker = $request->baker;
        $burden->status = 4;
        $burden->save();

        return redirect(route('eburden.create'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => 'Burden in progress'
        ]);
    }

    public function gradeView(Request $request, EndProductBurden $burden)
    {
        $weight = 0;
        $grades = Grade::all();
        $reusable_type = $burden->endProduct->reusable_type;
        if ($burden->endProduct->reusable_type > 1) {
            $weight = $burden->endProduct->weight;
        }
        return view('EndProductBurdenManager::burden.grade', compact('burden', 'grades', 'weight', 'reusable_type'));
    }

    public function gradeNow(Request $request, EndProductBurden $burden)
    {
        $qty = $request->quantity;
        $grades = $request->grade;
        $weight = $request->weight;


        try {
            DB::transaction(function () use ($burden, $qty, $grades) {
                $burden_qty = $burden->expected_end_product_qty;
                foreach ($grades as $key => $grade) {
                    if ($qty[$key] != 0) {
                        $bg = new EndProductBurdenGrade();
                        $bg->burden_id = $burden->id;
                        $bg->grade_id = $grade;
                        $bg->burden_qty = $qty[$key];
                     //   $bg->selling_price = $burden->endProduct->selling_price;
                     //   $bg->total_value = $burden->endProduct->selling_price * $burden_qty;

                        $bg->save();

                        if ($grade == 4 || $grade == 3) {
                            $burden_qty = $burden_qty - $qty[$key];
                        }
//                        StockTransaction::endProductStockTransaction(1, $burden->end_product_id, 0, 0, 1, $qty[$key], 0, 1);
                    }
                }
//                StockTransaction::updateEndProductAvailableQty(1, $burden->end_product_id, $burden_qty);

                $burden->status = 5;
                $burden->save();
            });
            $endProduct = EndProduct::find($burden->end_product_id);
            $reusableProduct = new ReusableProducts();
            $reusableProduct->reusable_type = $endProduct->reusable_type;
            $reusableProduct->product_id = $endProduct->id;
            $reusableProduct->note_id = $burden->id;
            $reusableProduct->qty = $qty[2];
            $reusableProduct->expected_weight = $weight;
            $reusableProduct->accepted_weight = 0;
            //collect type (1->semi,2->endproduct,3->salesReturn)
            $reusableProduct->collect_type = 2;
            $reusableProduct->status = 1;
            $reusableProduct->save();
            return redirect(route('burden_grade.index'))->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Graded Successfully'
            ]);
        } catch (Exception $e) {
            return $e;
            return redirect(route('burden_grade.index'))->with([
                'error' => true,
                'error.title' => 'Sorry !',
                'error.message' => 'Something Went Wrong '
            ]);
        }

        return redirect(route('burden_grade.index'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => 'Burden in progress'
        ]);
    }

    public function deletePendingBurden(Request $request)
    {
        $id = $request->burden_id;
        $burden = EndProductBurden::whereId($id)->whereStatus(1)->first();
        $burden->delete();
        return 'true';
    }

    public function destroy(EndProductBurden $burden)
    {
        $burden->delete();
        if ($burden->trashed()) {
            return true;
        } else {
            return false;
        }
    }

    public function updateQty(Request $request, $burden)
    {
        $bur = EndProductBurden::whereId($burden)->first();


        $endProduct_fraction = $bur->endProduct->is_fractional;


        if (!is_int($request->qty) && !$endProduct_fraction) {
            return redirect()->back()->with([
                'error'=>true,
                'error.title'=>'Sorry',
                'error.message'=>'Do not use decimal places for this end product',
            ]);
        }

        $bur->burden_size = $request->qty;
        $bur->expected_end_product_qty = $request->qty;
        $bur->save();

        return redirect()->back();
        return redirect(route('end-product-merge-list.index'));
    }

    public function checklistSemi(Request $request)
    {
        $burden_qty = $request->burden_qty;
        $end_product_id = $request->end_product_select;
        $endProduct = EndProduct::find($end_product_id);

        $endProductRecipes = EndProductRecipe::where('end_product_id', "=", $end_product_id)
            ->where('status', "=", '1')
            ->first();

        return view('EndProductBurdenManager::burden.create-checklist', compact('endProductRecipes', 'endProduct', 'burden_qty'));
    }

    public function createChecklistShow(Request $request)
    {
//        dd($request->all());
        $endProductId = $request->end_product_id;
        $burdenQty = $request->burden_qty;
        $semi_finish_product_id = $request->semi_finish_product_id;
        $semi_finish_product_burden_qty = $request->semi_finish_product_burden_qty;
        $semi_order = $request->semi_order;
        $totalBurdenqty = 0;
        $burdenIds = $request->burden_id;
        $burdenDefQtys = $request->burdenDefQty;
        $burdenQtys = $request->burdenqty;
        $CRIds = $request->cookReq;
        $CRDefQtys = $request->cookReqDefQty;
        $CRQtys = $request->cookReqQty;
        foreach ($request->burdenqty as $burdenqty) {
            $totalBurdenqty = $totalBurdenqty + $burdenqty;
        }
        $total_cooking_request_qty = 0;
        foreach ($request->cookReqQty as $cookingRequestQtyqty) {
            $total_cooking_request_qty = $total_cooking_request_qty + $cookingRequestQtyqty;
        }
        return view('EndProductBurdenManager::burden.create-checklistshow', compact('burdenIds', 'burdenDefQtys', 'burdenQtys', 'CRIds', 'CRDefQtys', 'CRQtys', 'semi_order', 'endProductId', 'burdenQty', 'semi_finish_product_id', 'semi_finish_product_burden_qty', 'totalBurdenqty', 'total_cooking_request_qty'));
    }


    public function printBurden(Request $request,EndProductBurden $burden){
        $recipe = EndProductRecipe::find($burden->recipe_id);


        $pdf = PDF::loadView('EndProductBurdenManager::burden.print', compact('burden','recipe'));
        $pdf->setPaper([0, 0, 220, 9999], 'portrait');
        return $pdf->stream();
        return view('EndProductBurdenManager::burden.print', compact('burden','recipe'));
    }
}
