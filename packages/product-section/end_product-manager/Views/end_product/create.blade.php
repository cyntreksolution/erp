@extends('layouts.back.master')@section('title','End Products')
@section('css')

    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.wizard.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/css/style.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/messaging.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/demos.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.fileupload.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.validation.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/modal.css')}}">
    <link rel="stylesheet" href="{{asset('assets/packages/end_product/end_product.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">


    <style>
        .hide {
            display: none;
        }

        .floating-button {
            width: 35px;
            height: 35px;
            border-radius: 50%;
            background: #60c84c;
            position: fixed;

            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .plus {
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 35px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }


    </style>
@stop
@section('content')
@section('current','End Products')
@section('current_url',route('end_products.create'))
<div class="app-wrapper">
    <div class="app-panel" id="app-panel">
        <div class="app-search"><input type="search" class="search-field" placeholder="Search"> <i
                    class="search-icon fa fa-search"></i></div><!-- /.app-search -->
            <div class="app-panel-inner">
                <div class="scroll-container">
                    <div class="p-3">
                    <button class="btn btn-success py-3 btn-block btn-lg" data-toggle="modal"
                            data-target="#projects-task-modal">NEW END PRODUCT
                    </button>
                    </div>
                    <hr class="m-0">

                    <div class="p-3">
                    <button class="btn btn-warning py-3 btn-block btn-lg" data-toggle="modal"
                            data-target="#cat">NEW Category
                    </button>
                    </div>

                    <hr class="m-0">

                    <div class="p-3">
                        <button class="btn btn-secondary py-3 btn-block btn-lg" data-toggle="modal"
                                data-target="#endburden">Add Burden Details
                        </button>
                    </div>
                    <hr class="m-0">
                    <div class="p-3">
                        <button class="btn btn-danger py-3 btn-block btn-lg" data-toggle="modal"
                                data-target="#newrecipe">NEW RECIPE
                        </button>
                    </div>

                    <hr class="m-0">
                    <div class="projects-list">
                    {{--@foreach($stocks as $raw)--}}
                    {{--<a href="{{$stock->stock_id}}">--}}
                    {{--<div class="media">--}}
                    {{--@php $color=array("blue","danger","purple","yellow","success","info","red") @endphp--}}
                    {{--<div class="avatar avatar-circle avatar-md project-icon" data-plugin="firstLitter"--}}
                    {{--data-target="#{{$raw->stock_id*754}}"></div>--}}
                    {{--<div class="media-body">--}}
                    {{--<h6 class="project-name" id="{{$raw->stock_id*754}}">{{$raw->name}}</h6>--}}
                    {{--<small class="project-detail">{{$raw->address}}</small>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</a>--}}
                    {{--@endforeach--}}
                    </div>
                </div>
            </div>
            <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show"><i
                    class="fa fa-chevron-right"></i>
            <i class="fa fa-chevron-left"></i>
            </a>
            </div>



            <div class="app-main">
                <div class="app-main-header">
                <h5 class="app-main-heading text-center">END PRODUCTS</h5>
                </div>
                <div class="app-main-content px-3 py-4" style="background-color: white" id="container">
                    <div class="content ">
                        <div class="contacts-list">
                            @foreach($endproducts as $endproduct)

                            <div class="panel-item media">

                                <div class="col-sm-6">
                                    @if(isset($endproduct->image))
                                        <div class="avatar avatar avatar-sm">
                                            <img src="{{asset($endproduct->image_path.$endproduct->image)}}" alt="">
                                        </div>
                                    @else
                                        @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                        <div
                                                class="avatar avatar-circle avatar-sm project-icon bg-{{$color[$endproduct->colour]}} "
                                                data-plugin="firstLitter"
                                                data-target="#raw-{{$endproduct->id*874}}">
                                        </div>
                                    @endif
                                    <div class="media-body">
                                        <h6 class="media-heading my-1">
                                            <h6 id="raw-{{$endproduct->id*874}}"
                                                id="">{{$endproduct->name}}</h6>
                                        </h6>
                                        <i class="qty-icon fa fa-archive"></i>
                                        <small>{{'Current stock : '.$endproduct->available_qty}} units</small>
                                        <br>
                                        {{--<small>{{$endproduct->desc}}</small>--}}
                                    </div>
                                </div>
                                <div class="section-2">

                                        <a class="btn btn-md  btn-danger text-center"
                                           href="{{$endproduct->id}}">Product Details</a>



                                        <a class="btn btn-md  btn-info text-center"
                                           href=" {{route('ep.burdenshow',$endproduct->id)}}">Burden Details</a>



                                    <!--button class="delete btn btn-light btn-sm ml-2" value="{{$endproduct->id}}"
                                            data-toggle="tooltip"
                                            data-placement="left" title="DELETE ASSISTANT">
                                        <i class="zmdi zmdi-close"></i>
                                    </button-->

                                </div>


                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="projects-task-modal" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                    <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12">
                        <div class="card">
                            {{ Form::open(array('route' => 'end_products.store','enctype'=>'multipart/form-data' ,'id'=>'bootstrap-wizard-form'))}}
                            <div class="wizard p-4" id="bootstrap-wizard-1">
                                <ul class="nav nav-tabs vertices keep-prefix-suffix" role="tablist">
                                <li class="nav-item">
                                    <a class=" nav-link active" data-toggle="tab" href="#ex5-step-1"
                                       role="tab">
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#ex5-step-2"
                                       role="tab">
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#ex5-step-3"
                                       role="tab">
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#ex5-step-4"
                                       role="tab">
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#ex5-step-5"
                                       role="tab">
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#ex5-step-6"
                                       role="tab">
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#ex5-step-7"
                                       role="tab">
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="ex5-step-1" role="tabpanel">

                                    <div class="task-name-wrap">
                                        <select name="category_id" required
                                                class="form-control select2 ">
                                            <option value="">Select Category</option>
                                            @foreach($categories as $category)
                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <hr>

                                    <div class="task-name-wrap">
                                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                        <input class="task-name-field" type="text" name="end_product_name"
                                               placeholder="Product name">
                                    </div>
                                    <hr>

                                    <div class="task-name-wrap">
                                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                        <input class="task-name-field" type="text" name="desc"
                                               placeholder="Description">
                                    </div>
                                    <hr>

                                    <div class="task-name-wrap">
                                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                        <input class="task-name-field" type="text" name="barcode"
                                               placeholder="Barcode">
                                    </div>
                                    <hr>

                                    <div class="task-name-wrap">
                                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                        <input class="task-name-field" type="text" name="weight"
                                               placeholder="Weight[g]">
                                    </div>
                                    <hr>

                                    <div class="task-name-wrap">
                                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                        <input class="task-name-field" type="text" name="buffer_stock"
                                               placeholder="Average Buffer Stock">
                                    </div>
                                    <hr>

                                </div>
                                <div class="tab-pane" id="ex5-step-2" role="tabpanel">

                                    <div class="task-name-wrap">
                                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                        <input class="task-name-field" type="text" name="d_price"
                                               placeholder="Distributor Price">
                                    </div>
                                    <hr>
                                    <div class="task-name-wrap">
                                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                        <input class="task-name-field" type="text" name="s_price"
                                               placeholder="Selling Price">
                                    </div>
                                    <hr>
                                    <div class="task-name-wrap">
                                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                        <input class="task-name-field" type="text" name="p_price"
                                               placeholder="Printed Price">
                                    </div>
                                    <hr>
                                    <div class="task-name-wrap">
                                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                        <input class="task-name-field" type="file" name="image">
                                    </div>
                                    <hr>
                                </div>
                                <div class="tab-pane" id="ex5-step-3" role="tabpanel">

                                    <div class="task-name-wrap">
                                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                        <input class="task-name-field" type="text" name="commission"
                                               placeholder="Commission[%]">
                                    </div>
                                    <hr>
                                    <div class="task-name-wrap">
                                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                        <input class="task-name-field" type="text" name="unit_discount"
                                               placeholder="Unit Discount">
                                    </div>
                                    <hr>
                                    <div class="task-name-wrap">
                                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                        <input class="task-name-field" type="text" name="transport_allowance"
                                               placeholder="Transport Allowance">
                                    </div>
                                    <hr>
                                    <div class="task-name-wrap">
                                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                        <input class="task-name-field" type="text" name="vat" placeholder="VAT">
                                    </div>
                                    <hr>
                                    <div class="task-name-wrap">
                                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                        <input class="task-name-field" type="text" name="nbt" placeholder="NBT">
                                    </div>
                                    <hr>

                                </div>
                                <div class="tab-pane" id="ex5-step-4" role="tabpanel">

                                    <div class="task-name-wrap">
                                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                        <input class="task-name-field" type="text" name="other_taxes"
                                               placeholder="Other Taxes">
                                    </div>
                                    <hr>
                                    <div class="task-name-wrap">
                                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                        <input class="task-name-field" type="text" name="mr" placeholder="M/R[%]">
                                    </div>
                                    <hr>
                                    <div class="task-name-wrap">
                                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                        <input class="task-name-field" type="text" name="damage"
                                               placeholder="Damage[%]">
                                    </div>
                                    <hr>

                                </div>
                                <div class="tab-pane" id="ex5-step-5" role="tabpanel">
                                    <div class="card">
                                        {{--<form action="#" id="bootstrap-wizard-form">--}}
                                        <div class="projects-list">
                                            <div class=" row">
                                                <form name="semi" id="semi">
                                                    <div class="col-sm-6 ">
                                                        <div class="d-flex mr-auto">
                                                            <div class="form-group">
                                                                <label for="single">Semi Finished Products</label>
                                                                <select id="single" name="semi_produts"
                                                                        class="form-control select2 " ui-jp="select2"
                                                                        style="width: 10rem"
                                                                        ui-options="{theme: 'bootstrap'}">
                                                                    @foreach($semiproducts as $semi)
                                                                        <option value=" {{$semi->semi_finish_product_id}}">{{$semi->name}}</option>
                                                                    @endforeach

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 mt-1 ">
                                                        <div class="d-flex activity-counters justify-content-between">

                                                            <div class="text-center px-2">

                                                                <label for="qty">Quantity</label>
                                                                <input class="form-control" id="qty"
                                                                       name="semi_products_qty"
                                                                       placeholder="" type="number" style="width:90px">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 py-4 px-4 mt-1">
                                                        <div class="d-flex activity-counters justify-content-between">

                                                            <div class="text-center px-2">

                                                                <div class="form-group">
                                                                    <div class="floating-button" id="btnsemi">
                                                                        <p class="plus">+</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="abc row px-1">

                                            </div>
                                        </div>

                                        {{--</form>--}}

                                    </div>
                                </div>

                                <div class="tab-pane" id="ex5-step-6" role="tabpanel">
                                    <div class="card">
                                        <div class="projects-list">
                                            <div class=" row">
{{--                                                <form name="semi" id="semi">--}}
                                                    <div class="col-sm-6 ">
                                                        <div class="d-flex mr-auto">
                                                            <div class="form-group">
                                                                <label for="single">Raw Materials</label>
                                                                <select id="raw" name="raw_materials"
                                                                        class="form-control select2 " ui-jp="select2"
                                                                        style="width: 10rem"
                                                                        ui-options="{theme: 'bootstrap'}">
                                                                    @foreach($products as $product)
                                                                        <option value="{{$product->raw_material_id}}">{{$product->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 mt-1 ">
                                                        <div class="d-flex activity-counters justify-content-between">

                                                            <div class="text-center px-2">

                                                                <label for="qty">Quantity</label>
                                                                <input class="form-control" id="raw_qty"
                                                                       name="raw_materials_qty"
                                                                       placeholder="" type="number" style="width:90px">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 py-4 px-4 mt-1">
                                                        <div class="d-flex activity-counters justify-content-between">

                                                            <div class="text-center px-2">

                                                                <div class="form-group">
                                                                    <div class="floating-button" id="btnEnd">
                                                                        <p class="plus">+</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
{{--                                                </form>--}}
                                            </div>
                                            <div class="raw_material_div row px-1">

                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="tab-pane" id="ex5-step-7" role="tabpanel">
                                    <div class="card">

                                        <div class=" projects-list">
                                            <form name="pack" id="packing">
                                                <div class=" row">
                                                    <div class="col-sm-6 ">
                                                        <div class="d-flex mr-auto">
                                                            <div class="form-group">
                                                                <label for="single">Packing Items</label>
                                                                <select id="packingsel" name="packing_items"
                                                                        class="form-control select2 " ui-jp="select2"
                                                                        style="width: 10rem"
                                                                        ui-options="{theme: 'bootstrap'}">
                                                                    <option>Select Packing Items</option>
                                                                    @foreach($packitems as $pack)
                                                                        <option value=" {{$pack->id}}">{{$pack->name}}</option>
                                                                    @endforeach

                                                                </select>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 mt-1 ">
                                                        <div class="d-flex activity-counters justify-content-between">

                                                            <div class="text-center px-2">

                                                                <label for="single">Quantity</label>
                                                                <input class="form-control" id="pqty"
                                                                       name="packing_items_qty"
                                                                       placeholder="" type="text" style="width:90px">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 py-4 px-4 mt-1">
                                                        <div class="d-flex activity-counters justify-content-between">

                                                            <div class="text-center px-2">

                                                                <div class="form-group">
                                                                    <div class="floating-button" id="btnpack">
                                                                        <p class="plus">+</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <div class="packing row p-2">

                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="pager d-flex justify-content-center">
                                <input type="submit" id="finish-btn" class="finish btn btn-success w-50"
                                       value="Finish">


                                <button type="button" id="next-btn" class="next btn btn-success w-50">Next
                                </button>
                            </div>
                        </div>
                        {!!Form::close()!!}
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="cat" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                    <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-lg-12">
                    <div class="card">
                        {{ Form::open(array('route' => 'end_products.sub','enctype'=>'multipart/form-data' ,'id'=>'bootstrap-wizard-form'))}}
                        <div class="wizard p-4" id="bootstrap-wizard-1">
                            <ul class="nav nav-tabs vertices keep-prefix-suffix" role="tablist">
                                <li class="nav-item">
                                    <a class=" nav-link active" data-toggle="tab" href="#ex5-step-1"
                                       role="tab">
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="ex5-step-1" role="tabpanel">


                                    <div class="task-name-wrap">
                                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                        <input class="task-name-field" type="text" name="cat_name"
                                               placeholder="category name">
                                    </div>
                                    <hr>

                                    <div class="task-name-wrap">
                                        <input class="" type="radio" name="loading_type" value="to_burden"> Post-Made Loading
                                    </div>
                                    <hr>
                                    <div class="task-name-wrap">
                                        <input class="" type="radio" name="loading_type" value="to_loading"> Pre-Made Loading
                                    </div>
                                    <hr>

                                    {{--<div class="task-name-wrap">--}}
                                    {{--<span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>--}}
                                    {{--<input class="task-name-field" type="text" name="desc"--}}
                                    {{--placeholder="Description">--}}
                                    {{--</div>--}}
                                    {{--<hr>--}}

                                </div>
                            </div>
                            <div class="pager d-flex justify-content-center">
                                <input type="submit" id="finish-btn" class="finish btn btn-success btn-block"
                                       value="Finish">
                            </div>
                        </div>
                        {!!Form::close()!!}
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="endburden" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {{ Form::open(array('route' => 'end_products.bd','enctype'=>'multipart/form-data' ,'id'=>'bootstrap-wizard-form'))}}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                    <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                </button>
            </div>
            <div class="modal-body">


                <div class="task-name-wrap">

                    <select name="end_id"
                            class="form-control select2" >
                        <option value="" >Select End Product</option>
                        @foreach($endproducts as $endproduct)
                            <option value="{{$endproduct->id}}" >{{$endproduct->name}}</option>
                        @endforeach
                    </select>

                </div>
                <hr>



                <div class="task-name-wrap">
                    <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                    <input class="task-name-field" type="number" name="default_size"
                           value=1 readonly>
                </div>
                <hr>

                <div class="task-name-wrap">

                    <select name="stage_id"
                            class="form-control select2" >
                        <option value="" >Select Stage</option>

                        <option value="1" >First Stage</option>
                        <option value="2" >Second Stage</option>

                    </select>

                </div>
                <hr>
                <div class="task-name-wrap">
                    <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                    <input class="task-name-field" type="number" step="0.01" name="A"
                           placeholder="Pay Amount for Grade A">
                </div>
                <hr>

                <div class="task-name-wrap">
                    <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                    <input class="task-name-field" type="number" step="0.01" name="B"
                           placeholder="Pay Amount for Grade B">
                </div>
                <hr>
                <div class="task-name-wrap">
                    <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                    <input class="task-name-field" type="number" step="0.01" name="C"
                           placeholder="Pay Amount for Grade C">
                </div>
                <hr>

                <div class="task-name-wrap">
                    <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                    <input class="task-name-field" type="number" step="0.01" name="D"
                           placeholder="Pay Amount for Grade D">
                </div>
                <hr>


                {{--<div class="task-name-wrap">--}}
                {{--<span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>--}}
                {{--<input class="task-name-field" type="text" name="desc"--}}
                {{--placeholder="Description">--}}
                {{--</div>--}}
                {{--<hr>--}}

            </div>

            <div class="pager d-flex justify-content-center">
                <input type="submit" id="finish-btn" class="finish btn btn-warning btn-block"
                       value="Finish">
            </div>



            {!!Form::close()!!}
        </div>
    </div>
</div>



@stop
@section('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    {{--    <script src="{{asset('assets/examples/js/apps/projects.js')}}"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/examples/js/demos/form.wizard.js')}}"></script>

    <script src="{{asset('assets/js/site.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('assets/packages/end_product/end_product.js')}}"></script>

    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $('.delete').click(function (e) {
            e.preventDefault();
            id = $(this).attr("value");
            confirmAlert(id);

        });

        function confirmAlert(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "delete",
                        url: id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            } else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

        $("#bootstrap-wizard-form").validate({
            rules: {
                end_product_name: "required",
                weight: {required: !0, number: !0},
                buffer_stock: {required: !0, number: !0},
                barcode: {required: !0, number: !0},
                d_price: {required: !0, number: !0},
                s_price: {required: !0, number: !0},
                p_price: {required: !0, number: !0},
                commission: {required: !0, number: !0},
                unit_discount: {required: !0, number: !0},
                transport_allowance: {required: !0, number: !0},
                vat: {required: !0, number: !0},
                nbt: {required: !0, number: !0},
                other_taxes: {required: !0, number: !0},
                mr: {required: !0, number: !0},
                damage: {required: !0, number: !0},
                semi_product_qty: {required: !0, number: !0},
                packing_item_qty: {required: !0, number: !0},

            },
            messages: {
                end_product_name: "Please enter the end product name",
                barcode: "Please enter the barcode",
                weight: "Please enter the weight",
                buffer_stock: "Please enter the buffer stock",
                d_price: "Please enter the distributor price",
                s_price: "Please enter the selling price",
                p_price: "Please enter the printed price",
                commission: "Please enter the commission",
                unit_discount: "Please enter the unit discount",
                transport_allowance: "Please enter the transport allowance",
                vat: "Please enter the VAT",
                nbt: "Please enter the NBT",
                other_taxes: "Please enter the other taxes",
                mr: "Please enter the M/R",
                damage: "Please enter the damage",
                semi_product_qty: "Please enter the semi product quantity",
                packing_item_qty: "Please enter the packing item quantity",
                username: {
                    required: "Please enter a username",
                    minlength: "Your username must consist of at least 2 characters"
                },
                password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                },
                confirm_password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long",
                    equalTo: "Please enter the same password as above"
                },
                email: "Please enter a valid email address",
                agree: "Please accept our policy"
            },
            errorElement: "div", errorPlacement: function (e, o) {
                e.addClass("form-control-feedback"), o.closest(".form-group").addClass("has-danger"), "checkbox" === o.prop("type") ? e.insertAfter(o.parent(".checkbox")) : e.insertAfter(o)
            }, highlight: function (e, o, r) {
                $(e).closest(".form-group").addClass("has-danger").removeClass("has-success"), $(e).removeClass("form-control-success").addClass("form-control-danger")
            }, unhighlight: function (e, o, r) {
                $(e).closest(".form-group").addClass("has-success").removeClass("has-danger"), $(e).removeClass("form-control-danger").addClass("form-control-success")
            }
        });


        $("#btnsemi").click(function (e) {
            e.preventDefault();
            var a = $("#single option:selected").val();
            var qty = $('#qty').val();

            var o = validateSemi();
            if (o) {
                $.ajax({
                    type: "get",
                    url: '../semi_finish_product/get/semipro',
                    data: {id: a},


                    success: function (response) {

                        $(".abc").append('<div class="semi style="width: 100%" > ' +
                            '<div class=" card-body d-flex  align-items-center p-2" >' +
                            '<div class="d-flex mr-auto">' +
                            '<div>' +
                            '<div class="avatar avatar text-white avatar-md project-icon bg-primary">' +
                            '<img class="card-img-top" src="" alt="">' +
                            '</div>' +
                            '</div>' +
                            '<h5 class="mt-3 mx-3" >' + response['name'] + '</h5>' +
                            '<input type="hidden" name="semi_produts[]" value=" ' + response['semi_finish_product_id'] + ' "/>' +
                            '<input type="hidden" name="semi_products_qty[]" value=" ' + qty + ' "/>' +
                            '</div>' +
                            '<div class="d-flex activity-counters justify-content-between">' +
                            '<div class="text-center px-5">' +
                            '<div class="text-primary"><h6 >' + qty + '</h6>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<div class="section-2">' +
                            '<div onclick="hidediv(this)" class="delete btn btn-light btn-sm ml-2" value=" ' + response['semi_finish_product_id'] + ' " data-toggle="tooltip" data-placement="left" title="DELETE"> ' +
                            '<i class="zmdi zmdi-close"></i>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>');
                        reset();
                    }


                });
            }


        });

        $("#btnEnd").click(function (e) {
            e.preventDefault();
            var a = $("#raw option:selected").val();
            var qty = $('#raw_qty').val();

            var o = validateRaw();
            if (o) {
                $.ajax({
                    type: "post",
                    url: '/raw_material/get/raw',
                    data: {id: a},
                    success: function (response) {
                        console.log(response)
                        $(".raw_material_div").append('<div class="semi style="width: 100%" > ' +
                            '<div class=" card-body d-flex  align-items-center p-2" >' +
                            '<div class="d-flex mr-auto">' +
                            '<div>' +
                            '<div class="avatar avatar text-white avatar-md project-icon bg-primary">' +
                            '<img class="card-img-top" src="" alt="">' +
                            '</div>' +
                            '</div>' +
                            '<h5 class="mt-3 mx-3" >' + response['name'] + '</h5>' +
                            '<input type="hidden" name="raw_material[]" value=" ' + response['raw_material_id'] + ' "/>' +
                            '<input type="hidden" name="raw_material_qty[]" value=" ' + qty + ' "/>' +
                            '</div>' +
                            '<div class="d-flex activity-counters justify-content-between">' +
                            '<div class="text-center px-5">' +
                            '<div class="text-primary"><h6 >' + qty + '</h6>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<div class="section-2">' +
                            '<div onclick="hidediv(this)" class="delete btn btn-light btn-sm ml-2" value=" ' + response['semi_finish_product_id'] + ' " data-toggle="tooltip" data-placement="left" title="DELETE"> ' +
                            '<i class="zmdi zmdi-close"></i>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>');
                        reset();
                    }


                });
            }


        });

        function validateSemi() {
            var x = document.getElementById('qty').value;
            var y = document.getElementById('single').value;
            if (y == "") {
                swal('Select semi product')
                return false;
            }
            if (x == "") {
                swal('enter quantity')
                return false;
            } else {
                return true;
            }
        }

        function validateRaw() {
            var x = document.getElementById('raw_qty').value;
            var y = document.getElementById('raw').value;
            if (y == "") {
                swal('Select Raw Material')
                return false;
            }
            if (x == "") {
                swal('Enter Quantity')
                return false;
            } else {
                return true;
            }
        }

        function hidediv(e) {
            $(e).closest(".semi").addClass("hide");

//            var div = this.document.getElementById("aa");
//            div.style.display = "none";

        }

        function reset() {

            document.getElementById("single").value = "";
            document.getElementById("qty").value = "";

        }


        $("#btnpack").click(function (e) {
            e.preventDefault();
            var packingitem = $("#packingsel option:selected").val();
            var pqty = $('#pqty').val();

            var o = validatePacking();

            if (o) {
                $.ajax({
                    type: "get",
                    url: '../packing/get/packingitem',
                    data: {id: packingitem},

                    success: function (response) {

                        $(".packing").append('<div class="pack" style="width: 100%">' +
                            ' <div class=" card-body d-flex  align-items-center p-2" >' +
                            '<div class="d-flex mr-auto">' +
                            '<div>' +
                            '<div class="avatar avatar text-white avatar-md project-icon bg-primary">' +
                            '<img class="card-img-top" src="" alt="">' +
                            '</div>' +
                            '</div>' +
                            '<h5 class="mt-3 mx-3" >' + response['name'] + '</h5>' +
                            '<input type="hidden" name="packing_items[]" value=" ' + response['id'] + ' "/>' +
                            '<input type="hidden" name="packing_items_qty[]" value=" ' + pqty + ' "/>' +
                            '</div>' +
                            '<div class="d-flex activity-counters justify-content-between">' +
                            '<div class="text-center px-5">' +
                            '<div class="text-primary"><h6 >' + pqty + '</h6>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<div class="section-2">' +
                            '<div onclick="hidepack(this)" class="delete btn btn-light btn-sm ml-2" value=" ' + response['id'] + ' " data-toggle="tooltip" data-placement="left" title="DELETE"> ' +
                            '<i class="zmdi zmdi-close"></i>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>');
                    }
                });
                clear();
            }

        });

        function validatePacking() {
            var x = document.getElementById('pqty').value;
            var y = document.getElementById('packingsel').value;
            if (y == "") {
                swal('Select Packing Item')
                return false;
            }
            if (x == "") {
                swal('enter quantity')
                return false;
            } else {
                return true;
            }
        }

        function hidepack(e) {
            $(e).closest(".pack").addClass("hide");
        }


        function clear() {
            document.getElementById("pqty").value = "";
            document.getElementById("packingsel").value = "";

        }
    </script>
@stop
