@extends('layouts.back.master')@section('title','End Product')
@section('css')

    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/modal.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/lightbox2/dist/css/lightbox.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/packages/end_product/recipe.css')}}">
    <link rel="stylesheet" href="{{asset('end_product.css')}}">
    <style>
        .profile-pic {
            position: relative;
            /*display: inline-block;*/
        }

        .profile-pic:hover .edit {
            display: block;
        }

        .edit {
            padding-top: 7px;
            padding-right: 7px;
            position: absolute;
            right: 0;
            top: 0;
            display: none;
        }

        .edit a {
            color: #000;
        }

        div.out {
            width: 40%;
            height: 120px;
            margin: 0 15px;
            background-color: #d6edfc;
            float: left;
        }

        div.in {
            width: 60%;
            height: 60%;
            background-color: #fc0;
            margin: 10px auto;
        }

        p {
            line-height: 1em;
            margin: 0;
            padding: 0;
        }

        .hide {
            display: none;
        }

    </style>

    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }
    </style>
@stop
@section('current','End Product Recipe')
@section('current_url',route('end_products.create'))
@section('{end_product}',route('end_product.show',[$end_product->id]))
@section('current','Semi Finish Products')
@section('current_url',route('semi_finish_products.create'))
@section('current2',$end_product->name)
@section('current_url2','')
@section('content')
    <div class="app-wrapper ">

        <div class="profile-section-user" id="app-panel">
            <div class="profile-cover-img profile-pic"><img
                        src="{{asset('assets\img\end product.jpg')}}" alt="">
            </div>
            <div class="profile-info-brief p-3">
                @if(isset($raw_material->image))
                    <div class="user-profile-avatar avatar-circle avatar avatar-sm">
                        <img src="{{asset($end_product->image_path.$end_product->image)}}" alt="">
                    </div>
                @else
                    @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                    <div class="user-profile-avatar avatar avatar-circle avatar-md project-iconr bg-{{$color[$end_product->colour]}}"
                         data-plugin="firstLitter"
                         data-target="#{{$end_product->id*754}}"></div>
                @endif

                <div class="text-center">
                    <div class="enterleave">
                        <h5 class="text-uppercase mb-1" id="{{$end_product->id*754}}">{{$end_product->name}}
                            <a href="#" class="hide btn btn-light btn-sm" data-toggle="modal"
                               data-target="#EditName">
                                <i class="zmdi zmdi-edit"></i>
                            </a>
                        </h5>
                    </div>
                    <div class="enterleave">
                        <h7 class="text mb-lg-1">{{'Available Quantity : '.$end_product->available_qty}}</h7>
                        <a href="#" class="hide btn btn-light btn-sm" data-toggle="modal"
                           data-target="#EditavailableQty">
                            <i class="zmdi zmdi-edit"></i>
                        </a>
                    </div>

                    <div class="enterleave">
                        <h7 class="text mb-lg-1">{{'Barcode : '.$end_product->barcode}}</h7>
                        <a href="#" class="hide btn btn-light btn-sm" data-toggle="modal" data-target="#EditBarcode">
                            <i class="zmdi zmdi-edit"></i>
                        </a>
                    </div>
                    <div class="enterleave">
                        <h7 class="enterleave text mb-lg-1">{{'Weight : '.$end_product->weight}}</h7>
                        <a href="#" class="hide btn btn-light btn-sm" data-target="#EditWeight" data-toggle="modal">
                            <i class="zmdi zmdi-edit"></i>
                        </a>
                    </div>
                    <div class="enterleave">
                        <h7 class="text mb-lg-1">{{'Buffer Stock : '.$end_product->buffer_stock}}</h7>
                        <a href="#" class="hide btn btn-light btn-sm" data-target="#EditBstock" data-toggle="modal">
                            <i class="zmdi zmdi-edit"></i>
                        </a>
                    </div>

                    <div class="enterleave">
                        <h7 class="text mb-lg-1">{{'MR : '.$end_product->mr}}</h7>
                        <a href="#" class="hide btn btn-light btn-sm" data-target="#EditMr" data-toggle="modal">
                            <i class="zmdi zmdi-edit"></i>
                        </a>
                    </div>

                    <div class="enterleave">
                        <h7 class="text mb-lg-1">{{'Damages : '.$end_product->damage}}</h7>
                        <a href="#" class="hide btn btn-light btn-sm" data-target="#EditDamage" data-toggle="modal">
                            <i class="zmdi zmdi-edit"></i>
                        </a>
                    </div>
                </div><!-- /.profile-info-brief -->


                <hr class="m-0">
                <div class="enterleave">
                    <div class="text-center">
                        {{$end_product->desc}}
                        <div class="section-1">
                            <a href="#" class="hide btn btn-light btn-sm" data-toggle="modal"
                               data-target="#EditDescription">
                                <i class="zmdi zmdi-edit"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="enterleave">
                    <div class="text-center">
                        <h7 class="text mb-lg-1">Reusable Type : @if($end_product->reusable_type == 1)
                                None
                            @endif
                            @if($end_product->reusable_type == 2)
                                Reusable Bun
                            @endif
                            @if($end_product->reusable_type == 3)
                                Reusable Bread
                            @endif
                            @if($end_product->reusable_type == 4)
                                Reusable Cake
                            @endif
                        </h7>
                        <a href="#" class="hide btn btn-light btn-sm" data-toggle="modal"
                           data-target="#EditReusableType">
                            <i class="zmdi zmdi-edit"></i>
                        </a>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-6">
                            Customizable
                        </div>
                        <div class="col-md-6">
                            <input type="checkbox" class="customize" data-plugin="switchery"
                                   value="{{$end_product->id}}"
                                   {{$end_product->customize?'checked':null}} data-size="medium">
                        </div>
                    </div>

                    <hr class="mt-1">

                    <!--div class="text-center mt-1">
                        <div class="row">
                            <div class="col-md-6 mt-1">Fraction</div>
                            {{ Form::open(array('url' => 'end_product/'.$end_product->id)) }}
                            <button type="submit" name="fraction" value="{{$end_product->is_fractional}}"
                                    class="btn btn-sm {{$end_product->is_fractional==0?'btn-danger':'btn-success'}}">{{$end_product->is_fractional==0?'disabled':'enabled'}}</button>
                            {{ Form::close() }}
                        </div>
                    </div>


                    <div class="text-center mt-1">
                        <div class="row">
                            <div class="col-md-6 mt-1">Status</div>
                            {{ Form::open(array('url' => 'end_product/'.$end_product->id)) }}
                            <button type="submit" name="status" value="{{$end_product->status}}"
                                    class="btn btn-sm {{$end_product->status==0?'btn-danger':'btn-success'}}">{{$end_product->status==0?'disabled':'enabled'}}</button>
                            {{ Form::close() }}
                        </div>
                    </div-->

                    <hr>
                    <div>
                        <td>
                            @if($end_product->is_fractional)

                                <a href="frac/dis/able/{{$end_product->id}}" class="btn btn-danger">Fraction Disable</a>

                            @else

                                <a href="frac/en/able/{{$end_product->id}}" class="btn btn-warning">Fraction Enable</a>

                            @endif


                            @if($end_product->status)

                                <a href="status/inact/{{$end_product->id}}" class="btn btn-danger">Inactive</a>

                            @else

                                <a href="status/act/{{$end_product->id}}" class="btn btn-warning">Active</a>

                            @endif

                        </td>
                    </div>

                    <hr>


                </div>
            </div>
        </div>
        <div class="app-main bg-white">


            {{--<div class="app-main-header">--}}
            {{--<h5 class="app-main-heading text-center"> Recipes</h5>--}}
            {{--</div>--}}
            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content px-3 py-4 ps ps--active-y" id="container">
                    <div class="content ">
                        <div class="row">
                            <div class="col-lg-4 col-sm-6">
                                <div class="card">
                                    <header class="card-header bg-danger"><h6 class="card-heading">Prices</h6>
                                    </header>
                                    <div class=" enterleave card-body d-flex p-2">

                                        <div class=" mr-2 text-primary">
                                            Distributor price
                                        </div>
                                        <div class=" mx-2 text-primary">
                                            {{$end_product->distributor_price}}</div>
                                        <div class="  mx-3">
                                            <a href="#" class="hide btn btn-light btn-sm" data-toggle="modal"
                                               data-target="#EditDisPrice">
                                                <i class="zmdi zmdi-edit"></i>
                                            </a>
                                        </div>

                                    </div>
                                    <div class=" enterleave d-flex p-2">

                                        <div class="mr-2 text-primary">
                                            Selling price
                                        </div>
                                        <div class=" mx-2 text-primary">
                                            {{$end_product->selling_price}}
                                        </div>
                                        <div class="mx-3">
                                            <a href="#" class="hide btn btn-light btn-sm" data-toggle="modal"
                                               data-target="#EditSellingPrice"> <i class="zmdi zmdi-edit"></i>
                                            </a>
                                        </div>

                                    </div>

                                    <div class="enterleave d-flex p-2">

                                        <div class="mr-2 text-primary">
                                            Printed price
                                        </div>
                                        <div class=" mx-2 text-primary">
                                            {{$end_product->printed_price}}</div>
                                        <div class="mx-3">
                                            <a href="#" class="hide btn btn-light btn-sm" data-toggle="modal"
                                               data-target="#EditPrintedPrice">
                                                <i class="zmdi zmdi-edit"></i>
                                            </a>
                                        </div>

                                    </div>
                                    <div class="progress progress-xs">
                                        <div class="progress-bar bg-danger" style="width:100%" role="progressbar"
                                             aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">

                                        </div>
                                    </div>
                                </div><!-- /.card -->
                            </div>
                            <div class="col-lg-4 col-sm-6">
                                <div class="card">
                                    <header class="card-header bg-success"><h6 class="card-heading">Allowance</h6>
                                    </header>
                                    <div class=" enterleave card-body d-flex p-2">

                                        <div class=" mr-2 text-primary">
                                            Commission
                                        </div>
                                        <div class=" mx-2 text-primary">
                                            {{$end_product->commission}}</div>
                                        <div class="  mx-3">
                                            <a href="#" class="hide btn btn-light btn-sm" data-toggle="modal"
                                               data-target="#EditCommission"> <i class="zmdi zmdi-edit"></i>
                                            </a>
                                        </div>

                                    </div>
                                    <div class="enterleave d-flex p-2">

                                        <div class="mr-2 text-primary">
                                            Unit Discount
                                        </div>
                                        <div class=" mx-2 text-primary">
                                            {{$end_product->unit_discount}}</div>
                                        <div class="mx-3">
                                            <a href="#" class="hide btn btn-light btn-sm" data-toggle="modal"
                                               data-target="#EditUnitDiscount">
                                                <i class="zmdi zmdi-edit"></i>
                                            </a>
                                        </div>

                                    </div>

                                    <div class="enterleave d-flex p-2">

                                        <div class="mr-2 text-primary">
                                            Transport
                                        </div>
                                        <div class=" mx-2 text-primary">
                                            {{$end_product->transport_allowance}}</div>
                                        <div class="mx-3">
                                            <a href="#" class="hide btn btn-light btn-sm" data-toggle="modal"
                                               data-target="#EditTransAllowlance"> <i class="zmdi zmdi-edit"></i>
                                            </a>
                                        </div>

                                    </div>
                                    <div class="progress progress-xs">
                                        <div class="progress-bar bg-success" style="width: 100%" role="progressbar"
                                             aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div><!-- /.card -->
                            </div>
                            <div class="col-lg-4 col-sm-6">
                                <div class="card">
                                    <header class="card-header bg-primary"><h6 class="card-heading text-danger">
                                            Taxes</h6>
                                    </header>
                                    <div class=" enterleave card-body d-flex p-2">

                                        <div class="mr-2 text-primary">
                                            VAT
                                        </div>
                                        <div class=" mx-2 text-primary">
                                            {{$end_product->vat}}</div>
                                        <div class="  mx-3">
                                            <a href="#" class="hide btn btn-light btn-sm" data-toggle="modal"
                                               data-target="#EditVat"> <i class="zmdi zmdi-edit"></i>
                                            </a>
                                        </div>

                                    </div>
                                    <div class="enterleave d-flex p-2">

                                        <div class="mr-2 text-primary">
                                            NBT
                                        </div>
                                        <div class=" mx-2 text-primary">
                                            {{$end_product->nbt}}</div>
                                        <div class="mx-3">
                                            <a href="#" class="hide btn btn-light btn-sm" data-toggle="modal"
                                               data-target="#EditNbt"> <i class="zmdi zmdi-edit"></i>
                                            </a>
                                        </div>

                                    </div>

                                    <div class="enterleave d-flex p-2">

                                        <div class="mr-2 text-primary">
                                            Other taxes
                                        </div>
                                        <div class=" mx-2 text-primary">
                                            {{$end_product->other_taxes}}</div>
                                        <div class="mx-3">
                                            <a href="#" class="hide btn btn-light btn-sm" data-toggle="modal"
                                               data-target="#EditOtherTaxes"> <i class="zmdi zmdi-edit"></i>
                                            </a>
                                        </div>

                                    </div>
                                    <div class="progress progress-xs">
                                        <div class="progress-bar bg-primary" style="width: 100%" role="progressbar"
                                             aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="card">

                                <header class="card-header bg-warning"><h6 class="card-heading">Variants</h6>
                                </header>

                                <div class="enterleave card-body d-flex p-2">
                                    {!! Form::open(['route' => ['variants.store',$end_product->id], 'method' => 'post','style'=>'width:100%']) !!}
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" name="variant" class="form-control"
                                                   placeholder="Variant Name">
                                        </div>
                                        <div class="col-md-3">
                                            <input type="number" name="size" class="form-control"
                                                   placeholder="Variant Weight/Size " step=".001">
                                        </div>
                                        <div class="col-md-3">
                                            <input type="submit" class="btn btn-primary" value="save">
                                        </div>

                                        {!! Form::close() !!}


                                        <div class="col-md-12 mt-3">
                                            @if (!empty(\EndProductManager\Models\EndProduct::find($end_product->id)->variants))
                                                <div class="">
                                                    <table id="invoice_table"
                                                           class="table table-full-width text-center">
                                                        <thead>
                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Size</th>
                                                            <th>Return Accept</th>
                                                            <th>Status</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach(\EndProductManager\Models\EndProduct::find($end_product->id)->variants as $key => $variant)
                                                            <tr>
                                                                <td>{{$variant->variant}}</td>
                                                                <td>{{$variant->size}}</td>
                                                                <td>
                                                                    <input type="checkbox" class="one"
                                                                           data-plugin="switchery"
                                                                           value="{{$variant->id}}" data-size="medium"
                                                                            {{$variant->return_accepted}} {{($variant->return_accepted ==1)?'checked':null}}>
                                                                </td>
                                                                <td>
                                                                    @if($variant->status == 1)
                                                                        <a href="{{route('variants.destroy',$variant->id)}}"
                                                                           class="btn btn-danger text-white btn-xs">deactivate</a>
                                                                    @else
                                                                        <a href="{{route('variants.destroy',$variant->id)}}"
                                                                           class="btn btn-success text-white btn-xs">activate</a>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="progress-bar bg-warning" style="width:100%" role="progressbar"
                                     aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">

                                </div>
                            </div>
                        </div>
                    </div>


                    <!--- Recipy-->
                    <div class="col-md-12">
                        <div class="card">
                            <table class="bg-dribbble">
                                <tr class="bg-dribbble">
                                    <td class="bg-dribbble col-md-6 bg-dribbble">
                                        <header class="card-header bg-dribbble" style="border-color:palevioletred">
                                            Recipy
                                        </header>
                                    </td>


                                    <td class="bg-dribbble col-md-3">
                                        <button class="btn btn-primary btn-block btn-sm" data-toggle="modal"
                                                data-target="#">ADD NEW RECIPY
                                        </button>
                                    </td>
                                </tr>
                            </table>
                            <!--input type="submit" class="btn btn-primary" data-toggle="modal" data-target="#projects-task-modal" value="Add New Recipy"-->
                            </header>

                            <div class="enterleave card-body d-flex p-2">

                                <div class="col-md-12">
                                    <table class="col-md-12">
                                        <tbody>
                                        @foreach($recipes as $recipe)
                                            <tr colspan="3">

                                                <td class="col-3">
                                                    <h6 class="project-name">{{$recipe->name}}</h6>

                                                    @if($recipe->status==1)
                                                        <i class="status status-online"></i><span
                                                                class="text-success">Active Recipe</span>
                                                    @else
                                                        <i class="status status-busy"></i><span
                                                                class="text-danger">Inactive Recipe</span>
                                                    @endif
                                                </td>

                                                <td class="col-3">
                                                    <button class="btn btn-danger btn-block btn-sm" data-toggle="modal"
                                                            data-target="#UpdateBurdenDetails{{$recipe->id}}">UPDATE BURDEN DETAILS
                                                    </button>
                                                </td>

                                                <div class="modal fade" id="UpdateBurdenDetails{{$recipe->id}}" tabindex="-1" role="dialog"
                                                     aria-divledby="myModaldiv"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" style="margin-top: 200px" role="document">
                                                        <div class="modal-content">
                                                            {{ Form::open(array('route' => 'end_products.updatebd','enctype'=>'multipart/form-data' ,'id'=>'bootstrap-wizard-form'))}}
                                                            <input name="ep_id" type="hidden" value="{{$end_product->id}}">
                                                            <input name="recipe_id" type="hidden" value="{{$recipe->id}}">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                                                                    <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                                                                    <div class="row task-name-wrap">
                                                                        <span><i class="zmdi zmdi-check"></i></span>
                                                                        <input name="ep_name" type="text" class="task-name-field"
                                                                               value="{{$end_product->name}}">
                                                                    </div>
                                                                    <hr>
                                                                    <div class="row task-name-wrap">
                                                                        <span><i class="zmdi zmdi-check"></i></span>
                                                                        <input readonly name="ep_recipe_id" type="text" class="task-name-field"
                                                                               value="{{$recipe->name}}">
                                                                    </div>

                                                                    <hr>
                                                                    <div class="row task-name-wrap">
                                                                        <label class="col-8 col-form-label">Days For
                                                                            Expiry-{{$recipe->days_expire}}</label>

                                                                        <span><i class="zmdi zmdi-check"></i></span>
                                                                        <input name="day_expire" type="text" class="task-name-field"
                                                                               placeholder="Days For Expiry">
                                                                    </div>

                                                                    <hr>
                                                                    <div class="row task-name-wrap">
                                                                        <label class="col-8 col-form-label">Over Heard
                                                                            %-{{$recipe->over_head}}</label>

                                                                        <span><i class="zmdi zmdi-check"></i></span>
                                                                        <input name="over_head" type="text" class="task-name-field"
                                                                               placeholder="Over Head %">
                                                                    </div>

                                                                    <hr>
                                                                    <div class="row task-name-wrap">
                                                                        <label class="col-8 col-form-label">Cost Of Distribution
                                                                            %-{{$recipe->distribute_cost}}</label>

                                                                        <span><i class="zmdi zmdi-check"></i></span>
                                                                        <input name="dist_cost" type="text" class="task-name-field"
                                                                               placeholder="Cost Of Distribution %">
                                                                    </div>

                                                                    <hr>
                                                                    <div class="row task-name-wrap">
                                                                        <label class="col-8 col-form-label">Total Burden
                                                                            Weight(Kg)-{{$recipe->burden_weight}}</label>

                                                                        <span><i class="zmdi zmdi-check"></i></span>
                                                                        <input name="b_weight" type="text" class="task-name-field"
                                                                               placeholder="Total Burden Weight(Kg)">
                                                                    </div>

                                                                    <hr>
                                                                    <div class="row task-name-wrap">
                                                                        <label class="col-8 col-form-label">Number Of Items For
                                                                            Burden-{{$recipe->burden_items}}</label>

                                                                        <span><i class="zmdi zmdi-check"></i></span>
                                                                        <input name="no_item" type="text" class="task-name-field"
                                                                               placeholder="Number Of Items For Burden">
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-8 col-form-label">First Stage-{{$recipe->stage_1}}</label>
                                                                        <div class="col-12">

                                                                            <select name="first_stage" class="form-control select2">
                                                                                <option value=''>Select First Stage</option>
                                                                                <option value="NoFirst">No First Stage</option>
                                                                                <option value="Remove">Remove</option>
                                                                                <option value="Unload">Unload</option>
                                                                                <option value="Grind">Grind</option>
                                                                                <option value="Make">Make</option>
                                                                                <option value="Mill">Mill</option>
                                                                                <option value="Mix">Mix</option>
                                                                                <option value="Prepair">Prepair</option>
                                                                                <option value="Pack">Pack</option>
                                                                                <option value="Burn">Burn</option>
                                                                                <option value="Cook">Cook</option>
                                                                                <option value="Bake">Bake</option>
                                                                                <option value="Fry">Fry</option>
                                                                                <option value="DeepFry">Deep Fry</option>
                                                                                <option value="Boil">Boil</option>
                                                                                <option value="Wrap">Wrap</option>
                                                                                <option value="Cut">Cut</option>
                                                                                <option value="Cream">Cream</option>
                                                                                <option value="Paste">Paste</option>
                                                                                <option value="Seal">Seal</option>
                                                                                <option value="Sort">Sort</option>


                                                                            </select>
                                                                        </div>
                                                                    </div>


                                                                    <div class="form-group">
                                                                        <label class="col-8 col-form-label">Second
                                                                            Stage-{{$recipe->stage_2}}</label>
                                                                        <div class="col-12">
                                                                            <select name="second_stage" class="form-control select2">
                                                                                <option value=''>Select Second Stage</option>
                                                                                <option value="NoSecond">No Second Stage</option>
                                                                                <option value="Remove">Remove</option>
                                                                                <option value="Unload">Unload</option>
                                                                                <option value="Grind">Grind</option>
                                                                                <option value="Make">Make</option>
                                                                                <option value="Mill">Mill</option>
                                                                                <option value="Mix">Mix</option>
                                                                                <option value="Prepair">Prepair</option>
                                                                                <option value="Pack">Pack</option>
                                                                                <option value="Burn">Burn</option>
                                                                                <option value="Cook">Cook</option>
                                                                                <option value="Bake">Bake</option>
                                                                                <option value="Fry">Fry</option>
                                                                                <option value="DeepFry">Deep Fry</option>
                                                                                <option value="Boil">Boil</option>
                                                                                <option value="Wrap">Wrap</option>
                                                                                <option value="Cut">Cut</option>
                                                                                <option value="Cream">Cream</option>
                                                                                <option value="Paste">Paste</option>
                                                                                <option value="Seal">Seal</option>
                                                                                <option value="Sort">Sort</option>


                                                                            </select>
                                                                        </div>
                                                                    </div>


                                                                </div>
                                                            </div>

                                                            <div class="modal-footer">
                                                                <input type="submit" class="btn btn-success btn-block" value="UPDATE">
                                                            </div>
                                                            {{ Form::close() }}
                                                        </div>
                                                    </div>
                                                </div>


                                                <td class="col-3">

                                                    <button class="btn btn-outline-primary" type="button"
                                                            data-toggle="collapse"
                                                            data-target="#multiCollapseExample{{$recipe->id}}"
                                                            aria-expanded="false"
                                                            aria-controls="multiCollapseExample{{$recipe->id}}"
                                                            style="width: 100%; border-color: transparent">
                                                        <span>
                                                        <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                        </span> Ingredients
                                                    </button>
                                                </td>
                                                <td class="col-3">
                                                    @if((!empty($recipe->status)) && $recipe->status==1)
                                                        <input type="checkbox" class="recipe" data-plugin="switchery"
                                                               data-size="medium" value="{{$recipe->id}}"
                                                               checked="true">
                                                    @else
                                                        <input type="checkbox" class="recipe" data-plugin="switchery"
                                                               data-size="medium" value="{{$recipe->id}}">
                                                    @endif

                                                </td>

                                                <div class="col-md-12">
                                                    <div class="collapse" id="multiCollapseExample{{$recipe['id']}}"
                                                         style="">

                                                        <div class="card-body">

                                                            <div class="row burden">

                                                                <div class="col-md-2" class="bg-danger">
                                                                    <h6><b>#</b></h6>
                                                                </div>
                                                                <div class="col-md-4" class="bg-danger">
                                                                    <h6><b>Description</b></h6>
                                                                </div>

                                                                <div class="col-md-3" class="bg-danger"
                                                                     style="text-align: center;">
                                                                    <h6><b>Units</b></h6>
                                                                </div>
                                                                <div class="col-md-3" class="bg-danger"
                                                                     style="text-align: right;">
                                                                    <h6><b>Price</b></h6>
                                                                </div>

                                                            </div>
                                                            <hr>
                                                            @php $cost = 0; @endphp
                                                            @foreach($recipe->semiContent as $r)
                                                                <div class="row burden">
                                                                    <div class="col-md-2">
                                                                        <a href="{{route('semi_finish_products.show',[$r->semiFinishProduct->semi_finish_product_id])}}"><i
                                                                                    class="fa fa-eye"></i> </a>
                                                                    </div>

                                                                    <div class="col-md-4">
                                                                        {{$r->semiFinishProduct->name}}
                                                                    </div>
                                                                    <div class="col-md-3" style="text-align: center;">
                                                                        {{$r->units}}
                                                                    </div>
                                                                    <div class="col-md-3" style="text-align: right;">
                                                                        <h6>{{number_format($r->semiFinishProduct->price($r->semiFinishProduct)*$r->units,2)}}</h6>
                                                                    </div>
                                                                    @php $cost = $cost + $r->semiFinishProduct->price($r->semiFinishProduct)*$r->units; @endphp
                                                                </div>
                                                            @endforeach
                                                            <hr>
                                                            @foreach($recipe->rawContent as $r)
                                                                <div class="row burden">
                                                                    <div class="col-md-2">
                                                                        <span><i class="fa fa-shopping-bag"></i> </span>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        {{$r->rawMaterial->name}}
                                                                    </div>
                                                                    <div class="col-md-3" style="text-align: center;">
                                                                        {{$r->units}}
                                                                    </div>
                                                                    <div class="col-md-3" style="text-align: right;">
                                                                        <h6>{{number_format(($r->rawMaterial->price/$r->rawMaterial->units_per_packs)*$r->units,2)}}</h6>
                                                                    </div>
                                                                </div>
                                                                @php $cost = $cost + ($r->rawMaterial->price/$r->rawMaterial->units_per_packs)*$r->units; @endphp
                                                            @endforeach

                                                            <hr>
                                                            @foreach($recipe->packingContent as $r)

                                                                <div class="row burden">
                                                                    <div class="col-md-2">
                                                                        <span><i class="fa fa-shopping-bag"></i> </span>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        {{$r->packingItem->name}}
                                                                    </div>
                                                                    <div class="col-md-3" style="text-align: center;">
                                                                        {{$r->units}}
                                                                    </div>
                                                                    <div class="col-md-3" style="text-align: right;">
                                                                        <h6>{{number_format(($r->packingItem->price/$r->packingItem->units_per_packs )*$r->units,2)}}</h6>
                                                                    </div>
                                                                </div>
                                                                @php $cost = $cost + ($r->packingItem->price/$r->packingItem->units_per_packs )*$r->units; @endphp
                                                            @endforeach
                                                            <div class="row" class="row burden">

                                                                <div class="col-md-2" class="bg-danger">
                                                                    <h6><b></b></h6>
                                                                </div>
                                                                <div class="col-md-4" class="bg-danger">
                                                                    <h6><b>Cost Of End Product</b></h6>
                                                                </div>

                                                                <div class="col-md-3" class="bg-danger"
                                                                     style="text-align: center;">
                                                                    <h6><b></b></h6>
                                                                </div>
                                                                <div class="col-md-3" class="bg-danger"
                                                                     style="text-align: right;">
                                                                    <h6><b>{{number_format($cost,2)}}</b></h6>
                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>

                                            </tr>
                                        @endforeach


                                        </tbody>
                                    </table>


                                </div>
                            </div>


                            <div class="progress-bar bg-dribbble" style="width:100%" role="progressbar"
                                 aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">

                            </div>
                        </div>
                    </div>


                    <!--End Of Recipy-->

                    <br>
                    <div class="col-md-12">
                        <table class="table table-dark">
                            <thead class="thead-light">
                            <th>Stage</th>
                            <th>Grade A</th>
                            <th>Grade B</th>
                            <th>Grade C</th>
                            <th>Grade D</th>
                            <th>Action</th>
                            </thead>
                            <tbody>

                            @foreach($burdenPayments as $b)
                                <tr>

                                    @if($b->stage_id ==1)
                                        <td>Stage One</td>
                                    @else
                                        <td>Stage Two</td>
                                    @endif

                                    <td>{{$b->grade_a}}</td>
                                    <td>{{$b->grade_b}}</td>
                                    <td>{{$b->grade_c}}</td>
                                    <td>{{$b->grade_d}}</td>
                                    @php
                                        /*$count = \BurdenManager\Models\BurdenPayment::where('pro_id','=',$b->pro_id)
                                                                                    ->where('stage_id','=',$b->stage_id)
                                                                                    ->where('pro_id','=',$b->pro_id)
                                                                                    ->count();*/

                                        $count = \BurdenManager\Models\BurdenPayment::where('pro_id','=',$b->pro_id)
                                                                            ->where('pro_type','=',$b->pro_type)
                                                                            ->where('stage_id','=',$b->stage_id )
                                                                            ->count();


                                    @endphp
                                    @if($count == 1)
                                    <td><button data-toggle="modal"  data-target="#endburden{{$b->id}}" class="btn btn-sm btn-success">Edit</button></td>
                                    @else

                                        <td><button data-toggle="modal"  data-target="#endburden{{$b->id}}" class="btn btn-sm btn-success">Edit</button>
                                        <a href="{{route('del.pay',['id'=>$b->id])}}" class="btn btn-sm btn-danger mr-2"><i class="fa fa-recycle"></i> </a></td>
                                    @endif
                                </tr>


                                <div class="modal fade" id="endburden{{$b->id}}" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
                                     aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            {!! Form::open(['route' => ['end_products.payments',$b->id], 'method' => 'post','style'=>'width:100%']) !!}
                                            <input name="pro_id" type="hidden" value="{{$b->pro_id}}">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                                                    <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <!--?php
                                                $proType='';
                                                $stageType='';
                                                dd($burdenPayments);
                                                if($burdenPayments->stage_id ==1){
                                                    $stageType='First Stage';
                                                }else if($burdenPayments->stage_id ==2)
                                                    {
                                                        $stageType='Second Stage';
                                                    }
                                            want to check protype & stage type
                                                    ?-->

                                                <div class="row task-name-wrap">
                                                    <label class="col-8 col-form-label">Burden Units </label>



                                                    <input class="form-control" type="number" name="default_size"
                                                           value=1 readonly>
                                                </div>

                                                <!--div class="task-name-wrap">

                                                    <input class="form-control" type="number" name="default_size"
                                                           value=1 readonly>
                                                </div-->


                                                <div class="task-name-wrap">
                                                    <label class="col-8 col-form-label">Product Type </label>
                                                    <input class="form-control" type="text" name="ptype"
                                                           value="{{$b->pro_type}}" readonly>
                                                </div>

                                                <div class="task-name-wrap">
                                                    <label class="col-8 col-form-label">Stage </label>
                                                    <input class="form-control" type="text" name="st"
                                                           value="{{$b->stage_id}}" readonly>
                                                </div>




                                                <div class="task-name-wrap">
                                                    <label class="col-8 col-form-label">Pay Value of Grade A </label>
                                                    <input class="form-control" type="number" name="A" step="0.01"
                                                           value="{{$b->grade_a}}" >
                                                </div>


                                                <div class="task-name-wrap">
                                                    <label class="col-8 col-form-label">Pay Value of Grade B </label>
                                                    <input class="form-control" type="number" name="B" step="0.01"
                                                           value="{{$b->grade_b}}" >
                                                </div>


                                                <div class="task-name-wrap">
                                                    <label class="col-8 col-form-label">Pay Value of Grade C </label>
                                                    <input class="form-control" type="number" name="C" step="0.01"
                                                           value="{{$b->grade_c}}" >
                                                </div>


                                                <div class="task-name-wrap">
                                                    <label class="col-8 col-form-label">Pay Value of Grade D </label>
                                                    <input class="form-control" type="number" name="D" step="0.01"
                                                           value="{{$b->grade_d}}" >
                                                </div>




                                                {{--<div class="task-name-wrap">--}}
                                                {{--<span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>--}}
                                                {{--<input class="task-name-field" type="text" name="desc"--}}
                                                {{--placeholder="Description">--}}
                                                {{--</div>--}}
                                                {{--<hr>--}}

                                            </div>

                                            <div class="pager d-flex justify-content-center">
                                                <input type="submit" id="finish-btn" class="finish btn btn-success btn-block"
                                                       value="Update">
                                            </div>



                                            {!!Form::close()!!}
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                            </tbody>


                        </table>
                    </div>
                    <!--project-task-model: END-->



                    <!--div class="modal fade" id="UpdateBurdenDetails" tabindex="-1" role="dialog"
                         aria-divledby="myModaldiv"
                         aria-hidden="true">
                        <div class="modal-dialog" style="margin-top: 200px" role="document">
                            <div class="modal-content">
                                {{-- Form::open(array('route' => 'end_products.updatebd','enctype'=>'multipart/form-data' ,'id'=>'bootstrap-wizard-form'))--}}
                                <input name="ep_id" type="hidden" value="{{$end_product->id}}">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                                        <div class="row task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input name="ep_name" type="text" class="task-name-field"
                                                   value="{{$end_product->name}}">
                                        </div>
                                        <hr>
                                        <div class="row task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input name="ep_recipe_id" type="text" class="task-name-field"
                                                   value="{{$recipe->name}}">
                                        </div>

                                        <hr>
                                        <div class="row task-name-wrap">
                                            <label class="col-8 col-form-label">Days For
                                                Expiry-{{$recipe->days_expire}}</label>

                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input name="day_expire" type="text" class="task-name-field"
                                                   placeholder="Days For Expiry">
                                        </div>

                                        <hr>
                                        <div class="row task-name-wrap">
                                            <label class="col-8 col-form-label">Over Heard
                                                %-{{$recipe->over_head}}</label>

                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input name="over_head" type="text" class="task-name-field"
                                                   placeholder="Over Head %">
                                        </div>

                                        <hr>
                                        <div class="row task-name-wrap">
                                            <label class="col-8 col-form-label">Cost Of Distribution
                                                %-{{$recipe->distribute_cost}}</label>

                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input name="dist_cost" type="text" class="task-name-field"
                                                   placeholder="Cost Of Distribution %">
                                        </div>

                                        <hr>
                                        <div class="row task-name-wrap">
                                            <label class="col-8 col-form-label">Total Burden
                                                Weight(Kg)-{{$recipe->burden_weight}}</label>

                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input name="b_weight" type="text" class="task-name-field"
                                                   placeholder="Total Burden Weight(Kg)">
                                        </div>

                                        <hr>
                                        <div class="row task-name-wrap">
                                            <label class="col-8 col-form-label">Number Of Items For
                                                Burden-{{$recipe->burden_items}}</label>

                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input name="no_item" type="text" class="task-name-field"
                                                   placeholder="Number Of Items For Burden">
                                        </div>

                                        <div class="form-group">
                                            <label class="col-8 col-form-label">First Stage-{{$recipe->stage_1}}</label>
                                            <div class="col-12">

                                                <select name="first_stage" class="form-control select2">
                                                    <option value=''>Select First Stage</option>
                                                    <option value="NoFirst">No First Stage</option>
                                                    <option value="Remove">Remove</option>
                                                    <option value="Unload">Unload</option>
                                                    <option value="Grind">Grind</option>
                                                    <option value="Make">Make</option>
                                                    <option value="Mill">Mill</option>
                                                    <option value="Mix">Mix</option>
                                                    <option value="Sheet">Sheet</option>
                                                    <option value="Prepair">Prepair</option>
                                                    <option value="Pack">Pack</option>
                                                    <option value="Burn">Burn</option>
                                                    <option value="Cook">Cook</option>
                                                    <option value="Bake">Bake</option>
                                                    <option value="Fry">Fry</option>
                                                    <option value="DeepFry">Deep Fry</option>
                                                    <option value="Boil">Boil</option>
                                                    <option value="Wrap">Wrap</option>
                                                    <option value="Cut">Cut</option>
                                                    <option value="Cream">Cream</option>
                                                    <option value="Paste">Paste</option>
                                                    <option value="Seal">Seal</option>
                                                    <option value="Sort">Sort</option>


                                                </select>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-8 col-form-label">Second
                                                Stage-{{$recipe->stage_2}}</label>
                                            <div class="col-12">
                                                <select name="second_stage" class="form-control select2">
                                                    <option value=''>Select Second Stage</option>
                                                    <option value="NoSecond">No Second Stage</option>
                                                    <option value="Remove">Remove</option>
                                                    <option value="Unload">Unload</option>
                                                    <option value="Grind">Grind</option>
                                                    <option value="Make">Make</option>
                                                    <option value="Mill">Mill</option>
                                                    <option value="Mix">Mix</option>
                                                    <option value="Sheet">Sheet</option>
                                                    <option value="Prepair">Prepair</option>
                                                    <option value="Pack">Pack</option>
                                                    <option value="Burn">Burn</option>
                                                    <option value="Cook">Cook</option>
                                                    <option value="Bake">Bake</option>
                                                    <option value="Fry">Fry</option>
                                                    <option value="DeepFry">Deep Fry</option>
                                                    <option value="Boil">Boil</option>
                                                    <option value="Wrap">Wrap</option>
                                                    <option value="Cut">Cut</option>
                                                    <option value="Cream">Cream</option>
                                                    <option value="Paste">Paste</option>
                                                    <option value="Seal">Seal</option>
                                                    <option value="Sort">Sort</option>


                                                </select>
                                            </div>
                                        </div>


                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-success btn-block" value="UPDATE">
                                </div>
                                {{-- Form::close() --}}
                            </div>
                        </div>
                    </div-->

                    <div class="modal fade" id="EditName" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
                         aria-hidden="true">
                        <div class="modal-dialog" style="margin-top: 200px" role="document">
                            <div class="modal-content">
                                {{ Form::open(array('url' => 'end_product/'.$end_product->id, 'id'=>'name' )) }}
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                                        <div class="row task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input name="name" type="text" class="task-name-field"
                                                   value="{{$end_product->name}}">
                                        </div>

                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="EditavailableQty" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
                         aria-hidden="true">
                        <div class="modal-dialog" style="margin-top: 200px" role="document">
                            <div class="modal-content">
                                {{ Form::open(array('url' => 'end_product/'.$end_product->id, 'id'=>'avqty')) }}
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                                        <div class="row task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input name="available_qty" type="text" class="task-name-field"
                                                   value="{{$end_product->available_qty}}">
                                        </div>

                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="EditBarcode" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
                         aria-hidden="true">
                        <div class="modal-dialog" style="margin-top: 200px" role="document">
                            <div class="modal-content">
                                {{ Form::open(array('url' => 'end_product/'.$end_product->id, 'id'=>'barcode')) }}
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                                        <div class="row task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input name="barcode" type="text" class="task-name-field"
                                                   value="{{$end_product->barcode}}">
                                        </div>

                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="EditWeight" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
                         aria-hidden="true">
                        <div class="modal-dialog" style="margin-top: 200px" role="document">
                            <div class="modal-content">
                                {{ Form::open(array('url' => 'end_product/'.$end_product->id, 'id'=>'bootstrap-wizard-form')) }}
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                                        <div class="row task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input name="weight" type="text" class="task-name-field"
                                                   value="{{$end_product->weight}}">
                                        </div>

                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="EditBstock" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
                         aria-hidden="true">
                        <div class="modal-dialog" style="margin-top: 200px" role="document">
                            <div class="modal-content">
                                {{ Form::open(array('url' => 'end_product/'.$end_product->id, 'id'=>'bstock')) }}
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                                        <div class="row task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input name="bstock" type="text" class="task-name-field"
                                                   value="{{$end_product->buffer_stock}}">
                                        </div>

                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="EditMr" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
                         aria-hidden="true">
                        <div class="modal-dialog" style="margin-top: 200px" role="document">
                            <div class="modal-content">
                                {{ Form::open(array('url' => 'end_product/'.$end_product->id, 'id'=>'mr')) }}
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                                        <div class="row task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input name="mr" type="text" class="task-name-field"
                                                   value="{{$end_product->mr}}">
                                        </div>

                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="EditDamage" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
                         aria-hidden="true">
                        <div class="modal-dialog" style="margin-top: 200px" role="document">
                            <div class="modal-content">
                                {{ Form::open(array('url' => 'end_product/'.$end_product->id, 'id'=>'damage')) }}
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                                        <div class="row task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input name="damage" type="text" class="task-name-field"
                                                   value="{{$end_product->damage}}">
                                        </div>

                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="EditDescription" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
                         aria-hidden="true">
                        <div class="modal-dialog" style="margin-top: 200px" role="document">
                            <div class="modal-content">
                                {{ Form::open(array('url' => 'end_product/'.$end_product->id, 'id'=>'dece')) }}
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                                        <div class="row task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input name="desc" type="text" class="task-name-field"
                                                   value="{{$end_product->desc}}">
                                        </div>

                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="EditDisPrice" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
                         aria-hidden="true">
                        <div class="modal-dialog" style="margin-top: 200px" role="document">
                            <div class="modal-content">
                                {{ Form::open(array('url' => 'end_product/'.$end_product->id, 'id'=>'dprice' )) }}
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                                        <div class="row task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input name="distributor_price" type="text" class="task-name-field"
                                                   value="{{$end_product->distributor_price}}">
                                        </div>

                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="EditSellingPrice" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
                         aria-hidden="true">
                        <div class="modal-dialog" style="margin-top: 200px" role="document">
                            <div class="modal-content">
                                {{ Form::open(array('url' => 'end_product/'.$end_product->id, 'id'=>'selprice')) }}
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                                        <div class="row task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input name="selling_price" type="text" class="task-name-field"
                                                   value="{{$end_product->	selling_price}}">
                                        </div>

                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="EditPrintedPrice" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
                         aria-hidden="true">
                        <div class="modal-dialog" style="margin-top: 200px" role="document">
                            <div class="modal-content">
                                {{ Form::open(array('url' => 'end_product/'.$end_product->id, 'id'=>'printPrice')) }}
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                                        <div class="row task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input name="printed_price" type="text" class="task-name-field"
                                                   value="{{$end_product->	printed_price}}">
                                        </div>

                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="EditCommission" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
                         aria-hidden="true">
                        <div class="modal-dialog" style="margin-top: 200px" role="document">
                            <div class="modal-content">
                                {{ Form::open(array('url' => 'end_product/'.$end_product->id, 'id'=>'commission')) }}
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                                        <div class="row task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input name="commission" type="text" class="task-name-field"
                                                   value="{{$end_product->	commission}}">
                                        </div>

                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="EditUnitDiscount" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
                         aria-hidden="true">
                        <div class="modal-dialog" style="margin-top: 200px" role="document">
                            <div class="modal-content">
                                {{ Form::open(array('url' => 'end_product/'.$end_product->id, 'id'=>'unitprice')) }}
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                                        <div class="row task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input name="unit_discount" type="text" class="task-name-field"
                                                   value="{{$end_product->unit_discount}}">
                                        </div>

                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="EditTransAllowlance" tabindex="-1" role="dialog"
                         aria-divledby="myModaldiv"
                         aria-hidden="true">
                        <div class="modal-dialog" style="margin-top: 200px" role="document">
                            <div class="modal-content">
                                {{ Form::open(array('url' => 'end_product/'.$end_product->id, 'id'=>'transport')) }}
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                                        <div class="row task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input name="transport_allowance" type="text" class="task-name-field"
                                                   value="{{$end_product->transport_allowance}}">
                                        </div>

                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="EditVat" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
                         aria-hidden="true">
                        <div class="modal-dialog" style="margin-top: 200px" role="document">
                            <div class="modal-content">
                                {{ Form::open(array('url' => 'end_product/'.$end_product->id, 'id'=>'vat')) }}
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                                        <div class="row task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input name="vat" type="text" class="task-name-field"
                                                   value="{{$end_product->vat}}">
                                        </div>

                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="EditNbt" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
                         aria-hidden="true">
                        <div class="modal-dialog" style="margin-top: 200px" role="document">
                            <div class="modal-content">
                                {{ Form::open(array('url' => 'end_product/'.$end_product->id, 'id'=>'nbt')) }}
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                                        <div class="row task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input name="nbt" type="text" class="task-name-field"
                                                   value="{{$end_product->nbt}}">
                                        </div>

                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="EditOtherTaxes" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
                         aria-hidden="true">
                        <div class="modal-dialog" style="margin-top: 200px" role="document">
                            <div class="modal-content">
                                {{ Form::open(array('url' => 'end_product/'.$end_product->id, 'id'=>'othertax')) }}
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                                        <div class="row task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input name="other_taxes" type="text" class="task-name-field"
                                                   value="{{$end_product->other_taxes}}">
                                        </div>

                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="EditReusableType" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
                         aria-hidden="true">
                        <div class="modal-dialog" style="margin-top: 200px" role="document">
                            <div class="modal-content">
                                {{ Form::open(array('url' => 'end_product/updateReusable/'.$end_product->id, 'id'=>'bstock')) }}
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                                        <div class="row task-name-wrap">
                                            <div class="col-lg-2">
                                                <span><i class="zmdi zmdi-check"></i></span>
                                            </div>
                                            <div class="col-lg-10">
                                                <select class="form-control" name="reusableType">
                                                    <option value="1" <?=$end_product->reusable_type == '1' ? ' selected="selected"' : '';?>>
                                                        None
                                                    </option>
                                                    <option value="2" <?=$end_product->reusable_type == '2' ? ' selected="selected"' : '';?>>
                                                        Reusable Bun
                                                    </option>
                                                    <option value="3" <?=$end_product->reusable_type == '3' ? ' selected="selected"' : '';?>>
                                                        Reusable Bread
                                                    </option>
                                                    <option value="4" <?=$end_product->reusable_type == '2' ? ' selected="selected"' : '';?>>
                                                        Reusable Cake
                                                    </option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-success btn-block" value="UPDATE">
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>

                    @stop

                    @section('js')
                        <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
                        <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
                        <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>


                        <script>

                            $('#UpdateBurdenDetails').on('shown.bs.modal', function (e) {

                            })






                            $("div.enterleave")
                                .on("mouseenter", function () {
                                    $("a:first", this).removeClass('hide');
                                })
                                .on("mouseleave", function () {
                                    $("a:first", this).addClass('hide');

                                });


                            var o = $("#bootstrap-wizard-form").validate({
                                rules: {
                                    weight: {required: !0, number: !0},
                                },
                                messages: {
                                    weight: {
                                        required: "Please enter new weight",
                                        number: "Please enter a valid weight",
                                    }
                                },
                                errorElement: "div", errorPlacement: function (e, o) {
                                    e.addClass("form-control-feedback"), o.closest(".form-group").addClass("has-danger"), "checkbox" === o.prop("type") ? e.insertAfter(o.parent(".checkbox")) : e.insertAfter(o)
                                }, highlight: function (e, o, r) {
                                    $(e).closest(".form-group").addClass("has-danger").removeClass("has-success"), $(e).removeClass("form-control-success").addClass("form-control-danger")
                                }, unhighlight: function (e, o, r) {
                                    $(e).closest(".form-group").addClass("has-success").removeClass("has-danger"), $(e).removeClass("form-control-danger").addClass("form-control-success")
                                }
                            });

                            var o = $("#name").validate({
                                rules: {
                                    name: {required: !0}
                                },

                                messages: {


                                    name: {required: "Please enter new name"},
                                },


                            });

                            var o = $("#avqty").validate({
                                rules: {
                                    available_qty: {required: !0, number: !0}
                                },
                                messages: {
                                    available_qty: {
                                        required: "Please enter new Available Quantity",
                                        number: "Please enter a valid Available Quantity"
                                    }
                                }
                            });
                            var o = $("#dprice").validate({
                                rules: {
                                    distributor_price: {required: !0, number: !0}
                                },
                                messages: {
                                    distributor_price: {
                                        required: "Please enter new distributor price",
                                        number: "Please enter a valid distributor price"
                                    }
                                }
                            });
                            var o = $("#barcode").validate({
                                rules: {
                                    barcode: {required: !0, number: !0}
                                },
                                messages: {
                                    barcode: {
                                        required: "Please enter new barcode",
                                        number: "Please enter a valid Abarcode"
                                    }
                                }
                            });
                            var o = $("#mr").validate({
                                rules: {
                                    mr: {required: !0, number: !0}
                                },
                                messages: {
                                    mr: {
                                        required: "Please enter new mr",
                                        number: "Please enter a valid mr"
                                    }
                                }
                            });
                            var o = $("#damage").validate({
                                rules: {
                                    damage: {required: !0, number: !0}
                                },
                                messages: {
                                    damage: {
                                        required: "Please enter new damage",
                                        number: "Please enter a valid damage"
                                    }
                                }
                            });
                            var o = $("#dece").validate({
                                rules: {
                                    desc: {required: !0}
                                },
                                messages: {
                                    desc: {
                                        required: "Please enter new Description",

                                    }
                                }
                            });
                            var o = $("#selprice").validate({
                                rules: {
                                    selling_price: {required: !0, number: !0}
                                },
                                messages: {
                                    selling_price: {
                                        required: "Please enter new selling price",
                                        number: "Please enter a valid selling price"
                                    }
                                }
                            });
                            var o = $("#printPrice").validate({
                                rules: {
                                    printed_price: {required: !0, number: !0}
                                },
                                messages: {
                                    printed_price: {
                                        required: "Please enter new printed price",
                                        number: "Please enter a valid printed price"
                                    }
                                }
                            });

                            var o = $("#commission").validate({
                                rules: {
                                    commission: {required: !0, number: !0}
                                },
                                messages: {
                                    commission: {
                                        required: "Please enter new commission",
                                        number: "Please enter a valid commission"
                                    }
                                }
                            });
                            var o = $("#unitprice").validate({
                                rules: {
                                    unit_discount: {required: !0}
                                },
                                messages: {
                                    unit_discount: {
                                        required: "Please enter new unit discount",
                                        number: "Enter a valid unit discount"

                                    }
                                }
                            });
                            var o = $("#transport").validate({
                                rules: {
                                    transport_allowance: {required: !0, number: !0}
                                },
                                messages: {
                                    transport_allowance: {
                                        required: "Please enter new transport allowance",
                                        number: "Please enter a valid transport allowance"
                                    }
                                }
                            });
                            var o = $("#bstock").validate({
                                rules: {
                                    bstock: {required: !0, number: !0}
                                },
                                messages: {
                                    bstock: {
                                        required: "Please enter new buffer stock",
                                        number: "Please enter a valid buffer stock"
                                    }
                                }
                            });

                            var o = $("#vat").validate({
                                rules: {
                                    vat: {required: !0, number: !0}
                                },
                                messages: {
                                    vat: {
                                        required: "Please enter new vat",
                                        number: "Please enter a valid vat"
                                    }
                                }
                            });
                            var o = $("#nbt").validate({
                                rules: {
                                    nbt: {required: !0}
                                },
                                messages: {
                                    nbt: {
                                        required: "Please enter new NBT",
                                        number: "Enter a valid NBT"

                                    }
                                }
                            });
                            var o = $("#othertax").validate({
                                rules: {
                                    other_taxes: {required: !0, number: !0}
                                },
                                messages: {
                                    other_taxes: {
                                        required: "Please enter other taxes",
                                        number: "Please enter a valid other taxes"
                                    }
                                }
                            });

                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                            });


                            $('.one').change(function (e) {
                                var id = $(this).attr("value");
                                if ($(this).is(":checked")) {
                                    swal({
                                            title: "Are you sure?",
                                            text: "Change Active Return Accepted Variant?",
                                            type: "warning",
                                            showCancelButton: true,
                                            confirmButtonColor: '#3085d6',
                                            confirmButtonText: "Yes, Change it!",
                                            closeOnConfirm: false
                                        },
                                        function () {
                                            $.ajax({
                                                type: "post",
                                                url: '/variant/change/' + id + '/return',
                                                success: function (response) {
                                                    if (response == 'true') {
                                                        swal("changed!", "Variant Status Change Successfully", "success");
                                                        setTimeout(location.reload.bind(location), 90);
                                                    } else {
                                                        swal("Error!", "Something Went Wrong", "error");
                                                    }
                                                }
                                            });
                                        }
                                    );
                                } else {
                                    swal({
                                            type: 'error',
                                            title: 'Cannot De Activate',
                                            showConfirmButton: false,
                                            timer: 1500
                                        },
                                        function () {
                                            setTimeout(location.reload.bind(location), 00);
                                        }
                                    );
                                }
                            });


                            $('.recipe').change(function (e) {
                                var id = $(this).attr("value");
                                var status = $(this).is(":checked") ? 1 : 0;
                                swal({
                                        title: "Are you sure?",
                                        text: "Change Active Recipe?",
                                        type: "warning",
                                        showCancelButton: true,
                                        confirmButtonColor: '#3085d6',
                                        confirmButtonText: "Yes, Change it!",
                                        closeOnConfirm: false
                                    },
                                    function () {
                                        $.ajax({
                                            type: "get",
                                            url: '/end/recipe/change/' + id + '/' + status + '',
                                            success: function (response) {
                                                if (response == 'true') {
                                                    swal("changed!", "Active Recipe Change Successfully", "success");
                                                    setTimeout(location.reload.bind(location), 90);
                                                } else {
                                                    swal("Error!", "Something Went Wrong", "error");
                                                }
                                            }
                                        });
                                    }
                                );

                            });

                            $('.customize').change(function (e) {
                                var id = $(this).attr("value");

                                swal({
                                        title: "Are you sure?",
                                        text: "Enable Customizable ?",
                                        type: "warning",
                                        showCancelButton: true,
                                        confirmButtonColor: '#3085d6',
                                        confirmButtonText: "Yes, Change it!",
                                        closeOnConfirm: false
                                    },
                                    function () {
                                        $.ajax({
                                            type: "get",
                                            url: '/end_product/item/customize/' + id,
                                            success: function (response) {
                                                if (response == 'true') {
                                                    swal("changed!", "End Product Status Change Successfully", "success");
                                                    setTimeout(location.reload.bind(location), 90);
                                                } else {
                                                    swal("Error!", "Something Went Wrong", "error");
                                                }
                                            }
                                        });
                                    }
                                );

                            });


                        </script>



                    @stop
                    @section('js')
                        {{--    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--}}
                        <script src="{{asset('assets/examples/js/apps/projects.js')}}"></script>
                        {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>--}}
                        <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
                        <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
                        <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
                        {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>--}}
                        <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
                        <script src="{{asset('assets/packages/semi_finish/semi_finish.js')}}"></script>
                        <script src="{{asset('assets/packages/recipe/end-recipe.js')}}"></script>
@stop
