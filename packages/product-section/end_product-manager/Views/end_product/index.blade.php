@extends('layouts.back.master')@section('title','End Product')
@section('css')
    <style type="text/css">

        th, td {
            text-align: right !important;
        }
    </style>

    <style type="text/css">
        #floating-button{
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #00C851;
            position: fixed;
            bottom: 80px;
            right: 30px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index:2
        }

        .plus{
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }
    </style>

      <link rel="stylesheet" href="{{asset('assets/vendor/dtable/Buttons-1.5.1/css/buttons.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.min.css')}}">
   <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/datatables.net/css/buttons.dataTables.min.css')}}">
@stop
@section('content')

    <div class="row">
        <div class="app-main-content mb-2 col-md-12">
            <div class="row mx-8">
                <div class="col-md-6">
                    {!! Form::select('category',$categories , null , ['class' => 'form-control','placeholder'=>'Select Category','id'=>'category']) !!}
                </div>
                <div class="col-md-6">
                    <button class="btn btn-info " onclick="process_form(this)">Filter</button>
                </div>

                <!-- loading view -->
            </div>
        </div>
    </div>

    @if($role = Sentinel::check()->roles[0]->slug=='owner')
        <div id="floating-button" data-toggle="tooltip" data-placement="left" data-original-title="NEW END PRODUCT"
             onclick="location.href = '{{url('end_product/create')}}';">
            <p class="plus">+</p>
        </div>
    @endif

    <table id="example" class="display text-center" style="width:100%">
        <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th class="text-center">Re-order level</th>
            <th class="text-center">System stock</th>
            <th class="text-center">Physical stock</th>
            <th class="text-center">Selling price</th>
            <th class="text-center">Printed price</th>
            <th class="text-center">Unit weight</th>
            <th class="text-center">Stock value</th>
            <th class="text-center">Stock weight</th>
            <!-- <th class="text-center">Action</th> -->

        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th class="text-center">Re-order level</th>
            <th class="text-center">System stock</th>
            <th class="text-center">Physical stock</th>
            <th class="text-center">Selling price</th>
            <th class="text-center">Printed price</th>
            <th class="text-center">Unit weight</th>
            <th class="text-center">Stock value</th>
            <th class="text-center">Stock weight</th>
            <!-- <th class="text-center">Action</th> -->

        </tr>
        </tfoot>
    </table>

@stop
@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/Buttons-1.5.1/js/dataTables.buttons.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/buttons/1.5.1/js/buttons.jqueryui.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/JSZip-2.5.0/jszip.min.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/pdfmake-0.1.32/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/Buttons-1.5.1/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js')}}"></script>
    <script>
        var table;
        $(document).ready(function () {
            table = $('#example').DataTable({
                responsive: true,
                "ajax": '{{url('end_product/raw/list')}}',
                dom: 'lfBrtip',
                "columnDefs": [
                    {
                        "targets": [ 4 ],
                        "visible": false,
                        "searchable": false

                    },

                ],
                "order": [],
                buttons: [
                    {
                        extend: 'copy',
                        className: 'btn btn-xs btn-info ml-2 sml assets-export-btn export-copy ttip',
                    },

                    {
                        extend: 'excel',
                        className: 'btn btn-xs btn-success ml-2 sml assets-export-btn export-copy ttip',
                    },
                    {
                        extend: 'pdfHtml5',
                        download: 'open',
                        className: 'btn btn-xs btn-danger ml-2 sml assets-export-btn export-copy ttip',
                        titleAttr: 'Pdf',
                        filename: 'End_product_summary_report',
                        extension: '.pdf',
                        orientation: 'landscape',
                        title: "Buntalk End Product Stores Summary Report",
                        footer: true,
                        exportOptions: {
                            orthogonal: "Export-pdf",
                        },
                        customize: function (doc) {
                            var rowCount = doc.content[1].table.body.length;
                            for (i = 1; i < rowCount; i++) {
                                doc.content[1].table.body[i][0].alignment = 'left';
                                doc.content[1].table.body[i][1].alignment = 'left';
                                doc.content[1].table.body[i][2].alignment = 'right';
                                doc.content[1].table.body[i][3].alignment = 'right';
                                doc.content[1].table.body[i][4].alignment = 'right';
                                doc.content[1].table.body[i][5].alignment = 'right';
                                doc.content[1].table.body[i][6].alignment = 'right';
                                doc.content[1].table.body[i][7].alignment = 'right';
                                doc.content[1].table.body[i][8].alignment = 'right';
                            }
                            doc.content[1].table.widths = Array(doc.content[1].table.body[0].length + 1).join('*').split('');
                            doc.content.splice(0, 1);
                            var logo = '';
                            doc.pageMargins = [10, 50, 10, 40];
                            doc.defaultStyle.fontSize = 9;
                            var now = new Date();
                            var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear() +' '+now.getHours()+':'+now.getMinutes()+':'+now.getSeconds();
                            doc.defaultStyle.alignment = 'left';
                            doc.styles.tableHeader.alignment = 'center';
                            doc.styles.tableHeader.fontSize = 10;
                            doc.styles.tableFooter.fontSize = 10;
                            doc['header'] = (function () {
                                return {
                                    columns: [
                                        {
                                            //image: logo,
                                            //width: 24
                                        },
                                        {
                                            alignment: 'left',
                                            italics: true,
                                            text: 'Buntalk End Product Stores Summary Report',
                                            fontSize: 18,
                                            margin: [10, 0]
                                        },
                                        {
                                            alignment: 'right',
                                            fontSize: 12,
                                            text: jsDate
                                        }
                                    ],
                                    margin: 20
                                };
                            });
                            doc['footer'] = (function (page, pages) {
                                return {
                                    columns: [
                                        {
                                            alignment: 'left',
                                            text: ['Created At: ', {text: jsDate.toString()}]
                                        },

                                        {
                                            alignment: 'right',
                                            text: ['Page No ', {text: page.toString()}, ' of ', {text: pages.toString()}]
                                        }
                                    ],
                                    margin: 20
                                };
                            });
                            var objLayout = {};
                            objLayout['hLineWidth'] = function (i) {
                                return .5;
                            };
                            objLayout['vLineWidth'] = function (i) {
                                return .5;
                            };
                            objLayout['hLineColor'] = function (i) {
                                return '#aaa';
                            };
                            objLayout['vLineColor'] = function (i) {
                                return '#aaa';
                            };
                            objLayout['paddingLeft'] = function (i) {
                                return 4;
                            };
                            objLayout['paddingRight'] = function (i) {
                                return 4;
                            };
                            doc.content[0].layout = objLayout;
                        },
                    },
                    {
                        extend: 'print',
                        orientation: 'landscape',
                        className: 'btn btn-xs btn-primary ml-2 sml assets-export-btn export-copy ttip',
                    },

                ]

            });

            $('#example tbody').on('click', 'td button', function (e) {
                // var data = table.row( $(this).parents('tr') ).data();
                e.preventDefault();
                var id = $(this).attr("value");
                confirmAlert(id);

            });
        });


        function confirmAlert(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "delete",
                        url: id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            } else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

        function process_form(e) {
            let category = $("#category").val();
            table.ajax.url('/end_product/list/table/data?category=' + category + '&filter=' + true).load();
        }

    </script>
@stop
