@section('content')
@section('current','End Products')
@section('current_url',route('ep.test',$endProduct->id))

<br>
<!--div class="app-main" -->

<div class="app-main-header" >
    <h3 class="app-main-heading text-center text-danger" >END PRODUCT {{$endProduct->name}}</h3>
    <!--div class="form-group" id=data-toggle="class" data-target="#app-panel" data-class="show"-->
    <div class="row" >
        <div class="col-6" >$endProduct
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#projects-task-modal">New End Product</button>
        </div>
        <div class="col-6" >
            <button type="button" class="btn btn-warning btn-block" data-toggle="modal" data-target="#cat">New End Product Category</button>
        </div>
    </div>
    <!--/div-->
</div>