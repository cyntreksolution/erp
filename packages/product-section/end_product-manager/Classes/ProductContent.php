<?php

namespace EndProductManager\Classes;

use EndProductManager\Models\EndProduct;
use ProductionOrderManager\Models\ProductionOrderData;
use ProductionOrderManager\Models\ProductionOrderHeader;
use RawMaterialManager\Models\RawMaterial;
use RecipeManager\Models\Recipe;
use RecipeManager\Models\RecipeContent;
use RecipeManager\Models\RecipeSemiProduct;
use SemiFinishProductManager\Models\SemiFinishProduct;

/**
 * User: Chinthaka Dilan F
 * Date: 11/30/2017
 * Project : erp
 */
class ProductContent
{

    public static function getSemiProductsOfEndProduct(EndProduct $endProduct)
    {
        $semiProducts = $endProduct->semiProducts()->get();
        return $semiProducts;
    }

    public static function getPackingItemsOfEndProduct(EndProduct $endProduct){
        $packingItems=$endProduct->packingItems()->get();
        return $packingItems;
    }












//    public static function getIngredientsOfProduct(EndProduct $end_product)
//    {
//        $semiProducts = $end_product->semiProducts()->get();
//        $raw_materials = array();
//        foreach ($semiProducts as $semiProduct) {
//            foreach (self::getRecipesOfSemiProduct($semiProduct) as $recipe) {
//                foreach (self::getContentsOfRecips($recipe) as $contentsOfRecip) {
//                    array_push($raw_materials, self::getMaterialsOfRecipeContent($contentsOfRecip));
//                }
//            }
//        }
//        return $raw_materials;
//    }
//
//    public static function getSemiProductsOfEndProduct(EndProduct $endProduct)
//    {
//        $semiProducts = $endProduct->semiProducts()->get();
//        return $semiProducts;
//    }
//
//    public static function getRecipsOfProduct(EndProduct $end_product)
//    {
//        $semiProducts = $end_product->semiProducts()->get();
//        $recipes = array();
//        foreach ($semiProducts as $semiProduct) {
//            foreach (self::getRecipesOfSemiProduct($semiProduct) as $recipe) {
//                array_push($recipes, $recipe);
//            }
//        }
//        return $recipes;
//
//    }
//
//    public static function getRecipesOfSemiProduct(SemiFinishProduct $semiFinishProduct)
//    {
//        return $semiFinishProduct->recipes()
//            ->where('recipe_semi_finish_product.status', '=', 2)
////            ->pluck('recipe.id');
//            ->get();
//    }
//
//    public static function getContentsOfRecips(Recipe $recipe)
//    {
//        return $recipe->recipeContents()->get();
//    }
//
//    public static function getMaterialsOfRecipeContent(RecipeContent $recipeContent)
//    {
//        return $recipeContent->materials()->get();
//    }
//
//    public static function getMaterialsForActiveProductionOrder($active_production_order)
//    {
//        $array = array();
//        foreach ($active_production_order as $item) {
//            $po = $item->id;
//            $order_qty = $item->qty;
//            $order_burden_kg = $item->burden_kg;
//            $order_recipe_id = $item->recipe_id;
//            $semi_finish_product = ProductContent::getSemiProductsOfEndProduct(EndProduct::find($item->end_product_end_product_id))->first();
//            $semi_product_count=$semi_finish_product->pivot->qty;
//            $order_semi_finish_product_id = $semi_finish_product->semi_finish_product_id;
//            $units_per_recipe = RecipeSemiProduct::where('recipe_id', '=', $order_recipe_id)
//                ->where('semi_finish_product_semi_finish_product_id', '=', $order_semi_finish_product_id)
//                ->where('status', '=', 2)
//                ->first()->units;
//
//            $recips_per_order = round(($order_qty * $semi_product_count ) / $units_per_recipe,'2');
//            $burdens = round($recips_per_order / $order_burden_kg,2);
////            $burdens = $recips_per_order / $order_burden_kg;
//            $recipe_contents = ProductContent::getContentsOfRecips(Recipe::find($order_recipe_id));
//            $mat = array();
//            foreach ($recipe_contents as $key => $recipe_content) {
//                $needed_material_id = $recipe_content->raw_material_id;
//                $needed_material_qty = round(($recipe_content->qty * $recips_per_order) / $burdens,'2');
////                array_push($mat,RawMaterial::find($needed_material_id));
//                $raw = RawMaterial::find($needed_material_id);
//                $mat[$key] = [
//                    'raw_material_id' => $raw->raw_material_id,
//                    'name' => $raw->name,
//                    'image' => $raw->image,
//                    'image_path' => $raw->image_path,
//                    'measurement' => $raw->measurement,
//                    'gram_per_unit' => round($raw->gram_per_unit,'2'),
//                    'color' => $raw->color,
//                    'qty_per_burden' => $needed_material_qty,
//                ];
//            }
//
//            $req = [
//                'semi' => $semi_finish_product,
//                'materials' => $mat,
//                'total_burdens' => $burdens,
//                'kg_per_burden' => $order_burden_kg,
//                'total_qty' => $order_qty * $semi_product_count ,
//                'po' => $po,
//            ];
//            array_push($array, $req);
//        }
//        return $array;
//    }

}