<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web'])->group(function () {
    Route::prefix('end_product')->group(function () {
        Route::get('', 'EndProductManager\Controllers\EndProductController@index')->name('end_products.index');
        Route::get('create', 'EndProductManager\Controllers\EndProductController@create')->name('end_products.create');
        Route::get('{end_product}', 'EndProductManager\Controllers\EndProductController@show')->name('end_product.show');
        Route::get('semi/pay/delete/{id}', 'EndProductManager\Controllers\EndProductController@delpay')->name('del.pay');

        Route::get('{end_product}/edit', 'EndProductManager\Controllers\EndProductController@edit')->name('end_products.edit');
        Route::get('raw/list', 'EndProductManager\Controllers\EndProductController@stock')->name('end_products.stock');
        Route::post('{end_product}', 'EndProductManager\Controllers\EndProductController@update')->name('end_products.update');
        Route::delete('{end_product}', 'EndProductManager\Controllers\EndProductController@destroy')->name('end_products.destroy');
        Route::post('updateReusable/{end_product}', 'EndProductManager\Controllers\EndProductController@updateReusable')->name('end_products.updateReusable');

        Route::post('save/end', 'EndProductManager\Controllers\EndProductController@store')->name('end_products.store');
        Route::post('sub/cat', 'EndProductManager\Controllers\EndProductController@createSubCategory')->name('end_products.sub');
        Route::post('burden/detail', 'EndProductManager\Controllers\EndProductController@burdendetail')->name('end_products.bd');
        Route::post('update/bd', 'EndProductManager\Controllers\EndProductController@updateBurdenDetails')->name('end_products.updatebd');

        Route::get('status/inact/{id}', 'EndProductManager\Controllers\EndProductController@inact');
        Route::get('status/act/{id}', 'EndProductManager\Controllers\EndProductController@act');
        Route::get('frac/dis/able/{id}', 'EndProductManager\Controllers\EndProductController@disable_frac');
        Route::get('frac/en/able/{id}', 'EndProductManager\Controllers\EndProductController@enable_frac');



    });
        Route::get('agent/{agent}/day/{day}/time/{time}/category/{category}/products', 'EndProductManager\Controllers\EndProductController@products')->name('category.products');

});


Route::get('/burdendetail/{end_product}','EndProductManager\Controllers\EndProductController@burdenshow')->name('ep.burdenshow');

Route::post('end_product/get/endpro', 'EndProductManager\Controllers\EndProductController@getendpro')->name('end_products.info');
Route::post('end_product/get/endpro/return', 'EndProductManager\Controllers\EndProductController@getReturn')->name('end_products.info');
Route::get('end_product/list/table/data/', 'EndProductManager\Controllers\EndProductController@stock');
Route::get('end_product/item/customize/{id}', 'EndProductManager\Controllers\EndProductController@customize');
Route::get('/end/recipe/change/{id}/{status}', 'EndProductManager\Controllers\EndProductController@recipeChange');
Route::post('/end_product/payment/{id}', 'EndProductManager\Controllers\EndProductController@updateBurdenPayments')->name('end_products.payments');

