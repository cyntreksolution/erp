<?php

namespace EndProductManager;

use Illuminate\Support\ServiceProvider;

class EndProductProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/Views', 'EndProductManager');
        require __DIR__ . '/web.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('EndProductManager', function($app){
            return new EndProductManager;
        });
    }
}
