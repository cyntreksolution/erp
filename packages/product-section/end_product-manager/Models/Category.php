<?php

namespace EndProductManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use SemiFinishProductManager\Models\SemiFinishProduct;

class Category extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];


    public function products(){
        return $this->hasMany(EndProduct::class);
    }

//    public function packingItems()
//    {
//        return $this->belongsToMany('PackingItemManager\Models\PackingItem')
//            ->withPivot('qty');
//    }
//
//    public function semiProducts()
//    {
//        return $this->belongsToMany('SemiFinishProductManager\Models\SemiFinishProduct')
//            ->withPivot('qty');
//    }
//
//    public function xxx()
//    {
//
//
//    }
}