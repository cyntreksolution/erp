<?php

namespace EndProductManager\Models;

use App\Variant;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RawMaterialManager\Models\RawMaterial;
use SemiFinishProductManager\Models\SemiFinishProduct;

class EndProduct extends Model
{
    use SoftDeletes;

    protected $table = 'end_product';

    protected $dates = ['deleted_at'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('sort', function (Builder $builder) {
            $builder->orderBy('name', 'asc');
        });
    }

    public function packingItems()
    {
        return $this->belongsToMany('PackingItemManager\Models\PackingItem')
            ->withPivot('qty');
    }

    public function semiProducts()
    {
        return $this->belongsToMany('SemiFinishProductManager\Models\SemiFinishProduct')
            ->withPivot('qty');
    }
    public function rawMaterials()
    {
        return $this->belongsToMany(RawMaterial::class)
            ->withPivot('qty');
    }

    public function variants()
    {
        return $this->hasMany(Variant::class);
    }

    public function stockEndProduct()
    {
        return $this->belongsTo('StockManager\Models\StockEndProduct','id','end_product_id');

    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }


}