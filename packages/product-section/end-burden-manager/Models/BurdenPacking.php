<?php

namespace EndProductBurdenManager\Models;

use EmployeeManager\Models\Employee;
use EmployeeManager\Models\Group;
use EndProductManager\Models\EndProduct;
use EndProductRecipeManager\Models\EndProductRecipe;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use PackingItemManager\Models\PackingItem;
use RawMaterialManager\Models\RawMaterial;
use SemiFinishProductManager\Models\SemiFinishProduct;

class BurdenPacking extends Model
{
    protected $table = 'merged_burden_packing';
    protected $fillable =['packing_id','end_product_id','qty','merge_id','raw_material_id','raw_material_name'];

    public function packingItem(){
        return $this->belongsTo(PackingItem::class);
    }

    public function endProduct(){
        return $this->belongsTo(EndProduct::class,'end_product_id');
    }
}
