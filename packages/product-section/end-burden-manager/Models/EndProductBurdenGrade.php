<?php

namespace EndProductBurdenManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use SemiFinishProductManager\Models\SemiFinishProduct;

class EndProductBurdenGrade extends Model
{
    use SoftDeletes;

    protected $table = 'burden_grade';

    protected $dates = ['deleted_at'];

    public function grade()
    {
        return $this->belongsTo('GradeManager\Models\Grade');
    }

}
