<?php

namespace EndProductBurdenManager\Models;

use EmployeeManager\Models\Employee;
use EmployeeManager\Models\Group;
use EndProductManager\Models\Category;
use EndProductManager\Models\EndProduct;
use EndProductRecipeManager\Models\EndProductRecipe;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use PackingItemManager\Models\PackingItem;
use RawMaterialManager\Models\RawMaterial;
use SalesRepManager\Models\SalesRep;
use SemiFinishProductManager\Models\SemiFinishProduct;

class EndProductBurdenHeader extends Model
{
    protected $table = 'end_product_burden_header';
    protected $fillable =['id','serial','status','created_by'];

    public function packingItem(){
        return $this->belongsTo(PackingItem::class);
    }

    public function rawMaterialList(){
       return $this->belongsToMany(BurdenRaw::class,'merged_burden_raw','merge_id','id');
    }

    public function category(){
        return $this->belongsTo(Category::class,'category');
    }

    public function agent()
    {
        return $this->hasOne(SalesRep::class, 'id', 'created_by');
    }
}
