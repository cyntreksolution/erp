<?php

namespace EndProductBurdenManager\Models;

use EmployeeManager\Models\Employee;
use EmployeeManager\Models\Group;
use EndProductManager\Models\EndProduct;
use EndProductRecipeManager\Models\EndProductRecipe;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RawMaterialManager\Models\RawMaterial;
use SemiFinishProductManager\Models\SemiFinishProduct;

class BurdenRaw extends Model
{
    protected $table = 'merged_burden_raw';
    protected $fillable =['raw_material_id','end_product_burden_id','end_product_id','raw_material_name','qty','merge_id'];

    public function rawMaterial(){
        return $this->belongsTo(RawMaterial::class);
    }

    public function endProduct(){
        return $this->belongsTo(EndProduct::class,'end_product_id');
    }
}
