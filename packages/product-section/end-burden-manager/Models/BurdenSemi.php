<?php

namespace EndProductBurdenManager\Models;

use EmployeeManager\Models\Employee;
use EmployeeManager\Models\Group;
use EndProductManager\Models\EndProduct;
use EndProductRecipeManager\Models\EndProductRecipe;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RawMaterialManager\Models\RawMaterial;
use SemiFinishProductManager\Models\SemiFinishProduct;

class BurdenSemi extends Model
{
    protected $table = 'merged_burden_semi';
    protected $fillable =['semi_finish_product_id','end_product_burden_id','end_product_id','semi_finish_product_name','qty','merge_id','send_qty'];

    public function semiFinishProduct(){
        return $this->belongsTo(SemiFinishProduct::class);
    }

    public function endProduct(){
        return $this->belongsTo(EndProduct::class,'end_product_id');
    }

    public function endProductBurden(){
        return $this->belongsTo(EndProductBurden::class,'end_product_burden_id');
    }
}
