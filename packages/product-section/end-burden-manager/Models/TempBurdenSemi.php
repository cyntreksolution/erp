<?php

namespace EndProductBurdenManager\Models;

use EmployeeManager\Models\Employee;
use EmployeeManager\Models\Group;
use EndProductManager\Models\EndProduct;
use EndProductRecipeManager\Models\EndProductRecipe;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RawMaterialManager\Models\RawMaterial;
use SemiFinishProductManager\Models\SemiFinishProduct;

class TempBurdenSemi extends Model
{
    protected $table = 'tmp_merged_burden_semi';
    protected $fillable =['semi_finish_product_id','end_product_id','semi_finish_product_name','qty','merge_id'];

    public function semiFinishProduct(){
        return $this->belongsTo(SemiFinishProduct::class);
    }
    public function endProduct(){
        return $this->belongsTo(EndProduct::class);
    }
}
