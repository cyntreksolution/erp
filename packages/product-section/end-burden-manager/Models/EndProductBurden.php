<?php

namespace EndProductBurdenManager\Models;

use Carbon\Carbon;
use EmployeeManager\Models\Employee;
use EmployeeManager\Models\Group;
use EndProductManager\Models\EndProduct;
use EndProductRecipeManager\Models\EndProductRecipe;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use SemiFinishProductManager\Models\SemiFinishProduct;

class EndProductBurden extends Model
{
    use SoftDeletes;

    protected $table = 'end_product_burden';

    protected $dates = ['deleted_at'];

    public function endProduct()
    {
        return $this->belongsTo(EndProduct::class);
    }

    public function endProdcuts()
    {
        return $this->belongsTo('EndProductManager\Models\EndProduct', 'end_product_id', 'id');
    }

    public function recipe()
    {
        return $this->belongsTo(EndProductRecipe::class);
    }

    public function employeeGroup()
    {
        return $this->belongsToMany(Group::class, 'group_employees','group_id');
    }


    public function baker()
    {
        return $this->hasOne(Employee::class, 'id', 'baker');
    }

    public function grade()
    {
        return $this->hasMany(\App\EndProductBurdenGrade::class,'burden_id');
    }

    public function gradeA()
    {
        return $this->hasMany(\App\EndProductBurdenGrade::class,'burden_id')->whereGradeId(1);
    }


    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }

    public function scopeSearchData($query, $term)
    {
        return $query
            ->Where('id', 'like', "%" . $term . "%")
            ->orWhere('serial', 'like', "%" . $term . "%")
            ->orWhere('status', 'like', "%" . $term . "%")
            ->orWhere('burden_size', 'like', "%" . $term . "%")
            ->orWhere('created_at', 'like', "%" . $term . "%")
            ->orWhereHas('semiProdcuts', function ($query) use ($term) {
                $query->where('name', 'like', "%" . $term . "%");
            })
            ->orWhereHas('recipe', function ($query) use ($term) {
                $query->where('name', 'like', "%" . $term . "%");
            });
    }

    public function scopefilterData($query, $category, $end_product,$status=null,$date_range=null,$date_type=null)
    {


        if (!empty($category)) {
            $query = $query->whereHas('endProduct.category', function ($q) use ($category){
                return $q->whereId($category);
            });
        }

        if (!empty($end_product)) {
            $query = $query->whereEndProductId($end_product);
        }


        if (!empty($status)) {
            $query = $query->whereStatus($status);
        }

        if (!empty($date_range)) {
            $dates = explode(' - ', $date_range);
            $start = $dates[0];
            $end = $dates[1];
            if ($date_type == 2){
                $query->where('created_start_time', '>=', Carbon::parse($start)->format('Y-m-d'))
                    ->where('created_start_time', '<=', Carbon::parse($end)->addDay()->format('Y-m-d'));
            }elseif ($date_type == 3){
                $query->where('bake_start_time', '>=', Carbon::parse($start)->format('Y-m-d'))
                    ->where('bake_start_time', '<=', Carbon::parse($end)->addDay()->format('Y-m-d'));
            }elseif ($date_type == 4){

            $query->where('updated_at', '>=', Carbon::parse($start)->format('Y-m-d'))
                ->where('updated_at', '<=', Carbon::parse($end)->addDay()->format('Y-m-d'))
                ->where('status', '>=', 5);

        }else{
                $query->where('created_at', '>=', Carbon::parse($start)->format('Y-m-d'))
                        ->where('created_at', '<=', Carbon::parse($end)->addDay()->format('Y-m-d'));
            }
        }

        return $query;
    }
}
