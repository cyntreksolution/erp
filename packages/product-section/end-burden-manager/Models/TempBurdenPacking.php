<?php

namespace EndProductBurdenManager\Models;

use EmployeeManager\Models\Employee;
use EmployeeManager\Models\Group;
use EndProductManager\Models\EndProduct;
use EndProductRecipeManager\Models\EndProductRecipe;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use PackingItemManager\Models\PackingItem;
use RawMaterialManager\Models\RawMaterial;
use SemiFinishProductManager\Models\SemiFinishProduct;

class TempBurdenPacking extends Model
{
    protected $table = 'tmp_merged_burden_packing';
    protected $fillable =['packing_id','end_product_id','packing_name','qty','merge_id'];

    public function packingItem(){
        return $this->belongsTo(PackingItem::class);
    }
    public function endProduct(){
        return $this->belongsTo(EndProduct::class,'end_product_id');
    }
}
