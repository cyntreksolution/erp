<?php

namespace EndProductBurdenManager;

use Illuminate\Support\ServiceProvider;

class EndProductBurdenServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/Views', 'EndProductBurdenManager');
        $this->loadRoutesFrom(__DIR__.'/web.php');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('EndProductBurdenManager', function($app){
            return new EndProductBurdenManager;
        });
    }
}
