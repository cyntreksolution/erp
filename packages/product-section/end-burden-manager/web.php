<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

use EndProductBurdenManager\Models\EndProductBurden;

Route::middleware(['web'])->group(function () {
    Route::prefix('end-burden')->group(function () {
        Route::post('/edit/qty/{burden}', 'EndProductBurdenManager\Controllers\EndProductBurdenController@updateQty')->name('eburden.merge');


        Route::get('list/merge', 'EndProductBurdenManager\Controllers\EndProductBurdenController@mergeList')->name('eburden.mergelist');
        Route::get('print/{burden}', 'EndProductBurdenManager\Controllers\EndProductBurdenController@printBurden')->name('eburden.print');
        Route::get('list/merged', 'EndProductBurdenManager\Controllers\EndProductBurdenController@mergedList')->name('eburden.mergedlist');
        Route::post('list/merge', 'EndProductBurdenManager\Controllers\EndProductBurdenController@mergeBurdens')->name('eburden.merge');

        Route::get('end-product/make/bake/{burden}', 'EndProductBurdenManager\Controllers\EndProductBurdenController@endProductMake')->name('eburden.make');
        Route::post('end-product/make/bake/{burden}', 'EndProductBurdenManager\Controllers\EndProductBurdenController@endProductBake')->name('eburden.make');

        Route::get('end-product/grade/bake/{burden}', 'EndProductBurdenManager\Controllers\EndProductBurdenController@gradeView')->name('eburden.grade');
        Route::post('end-product/grade/bake/{burden}', 'EndProductBurdenManager\Controllers\EndProductBurdenController@gradeNow')->name('eburden.grade');


        Route::get('list', 'EndProductBurdenManager\Controllers\EndProductBurdenController@burdenListShow')->name('eburden.list');
        Route::get('table/data', 'EndProductBurdenManager\Controllers\EndProductBurdenController@tableData')->name('eburden.table');
        Route::post('excel/data', 'EndProductBurdenManager\Controllers\EndProductBurdenController@exportFilter')->name('eburden.excel');


        Route::get('', 'EndProductBurdenManager\Controllers\EndProductBurdenController@index')->name('eburden.index');
        Route::get('bake/ready/list', 'EndProductBurdenManager\Controllers\EndProductBurdenController@bakeList')->name('bake.list');

        Route::get('create', 'EndProductBurdenManager\Controllers\EndProductBurdenController@create')->name('eburden.create');

        Route::get('date', 'EndProductBurdenManager\Controllers\EndProductBurdenController@dateReport')->name('eburden.date');
        Route::post('date', 'EndProductBurdenManager\Controllers\EndProductBurdenController@getReport')->name('eburden.date');
        Route::post('analyse', 'EndProductBurdenManager\Controllers\EndProductBurdenController@analyse')->name('eburden.analyse');

        Route::get('view/history', 'EndProductBurdenManager\Controllers\EndProductBurdenController@history')->name('eburden.history');

        Route::get('view/history/{burden}', 'EndProductBurdenManager\Controllers\EndProductBurdenController@historyView')->name('eburden.historyView');

        Route::get('{burden}', 'EndProductBurdenManager\Controllers\EndProductBurdenController@show')->name('eburden.show');

        Route::get('{burden}/edit', 'EndProductBurdenManager\Controllers\EndProductBurdenController@edit')->name('eburden.edit');

        Route::get('{burden}/bake', 'EndProductBurdenManager\Controllers\EndProductBurdenController@bake')->name('eburden.bake');

        Route::post('', 'EndProductBurdenManager\Controllers\EndProductBurdenController@store')->name('eburden.store');


        Route::post('{burden}', 'EndProductBurdenManager\Controllers\EndProductBurdenController@update')->name('eburden.update');
        Route::post('bake/{burden}', 'EndProductBurdenManager\Controllers\EndProductBurdenController@baekNow')->name('eburden.bakenow');

        Route::get('get/recipe', 'EndProductBurdenManager\Controllers\EndProductBurdenController@semiFinishProductRecipeAndCount')->name('eburden.updatex');

        Route::delete('{burden}', 'EndProductBurdenManager\Controllers\EndProductBurdenController@destroy')->name('eburden.destroy');
        Route::post('delete/burden', 'EndProductBurdenManager\Controllers\EndProductBurdenController@deletePendingBurden')->name('eburden.destroy');
        Route::post('create/checklist', 'EndProductBurdenManager\Controllers\EndProductBurdenController@checklistSemi')->name('eburden.createchecklist');
        Route::post('create/createchecklistShow', 'EndProductBurdenManager\Controllers\EndProductBurdenController@createChecklistShow')->name('eburden.createchecklistShow');
    });
});
