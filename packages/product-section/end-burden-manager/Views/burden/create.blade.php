@extends('layouts.back.master')@section('title','Make End Product')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>

    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">

    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        #projects-task-modal2 .close {
            border: none;
            outline: none;
            box-shadow: none;
            position: absolute;
            right: 1rem;
            top: 1rem;
            width: 25px;
            height: 25px;
            line-height: 25px;
            text-align: center;
            font-size: 1.13rem;
            border-radius: 500px;
            border: 1px solid #868e96
        }

        #floating-button {
            width: 45px;
            height: 45px;
            border-radius: 50%;
            background: #db4437;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2;
            text-align: center;
            padding: 2px;
        }

        #floating-buttona {
            width: 45px;
            height: 45px;
            border-radius: 50%;
            background: #db4437;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2;
            text-align: center;
            padding: 2px;
        }

        .plus {
            color: white;

            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 38px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }


        #container {
            position: relative;
            padding: 0px;
            width: 100%;
            height: 200px;
            overflow: auto;
        }

        #container .content {
            width: 100%;
            height: 50%;
        }

        .hide {
            display: none;
        }


        #container2 {
            position: relative;
            /*padding: 0px;*/
            width: 98%;
            height: 420px;
            overflow: auto;
        }

        #container2 .content {
            width: 100%;
            height: 20%;
        }

    </style>
@stop
@section('current','Burden')
@section('current_url',route('burden.create'))
@section('content')
    <div class="app-wrapper">
        <div class="app-panel" id="app-panel">
            {{--            <div class="app-search"><input type="search" class="search-field" placeholder="Search">--}}
            {{--                <i class="search-icon fa fa-search"></i>--}}
            {{--            </div>--}}
            <div class="app-panel-inner">
                <div class="scroll-container">
                    <div class="p-3">
                        <button class="btn btn-success py-3 btn-block btn-lg" data-toggle="modal"
                                data-target="#projects-task-modal">NEW BURDEN
                        </button>
                    </div>
                </div>

                <hr class="m-0">
                <div class="people-list d-flex justify-content-start flex-wrap p-3">

                    {{--                    <a href="view/history">--}}
                    {{--                        <button class="avatar-sm avatar-circle fz-base font-weight-bold add-people-btn mx-1">--}}
                    {{--                            <i data-toggle="tooltip" data-placement="bottom" title="Burden History"--}}
                    {{--                               class="zmdi zmdi-trending-up"></i>--}}
                    {{--                        </button>--}}
                    {{--                    </a>--}}

                    {{--                    <a href="date">--}}
                    {{--                        <button class="avatar-sm avatar-circle fz-base font-weight-bold add-people-btn mx-1">--}}
                    {{--                            <i data-toggle="tooltip" data-placement="bottom" title="Check Date"--}}
                    {{--                               class="zmdi zmdi-time-interval"></i>--}}
                    {{--                        </button>--}}
                    {{--                    </a>--}}

                </div>
                <hr class="m-0">
                <div class="media-list" id="container">
                    {{--                    @foreach($semi as $product)--}}
                    {{--                        <a href="..\semi_finish_product\{{$product->semi_finish_product_id}}">--}}
                    {{--                            <div class="media content">--}}
                    {{--                                @if(isset($product->image))--}}
                    {{--                                    <div href="{{$product->semi_finish_product_id}}"--}}
                    {{--                                         class="avatar avatar avatar-sm">--}}
                    {{--                                        <img src="{{asset($product->image_path.$product->image)}}" alt="">--}}
                    {{--                                    </div>--}}
                    {{--                                @else--}}
                    {{--                                    @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp--}}
                    {{--                                    <div href="{{$product->semi_finish_product_id}}"--}}
                    {{--                                         class="avatar avatar-circle avatar-sm bg-{{$color[$product->colour]}} "--}}
                    {{--                                         data-plugin="firstLitter"--}}
                    {{--                                         data-target="#raw-{{$product->semi_finish_product_id*874}}">--}}
                    {{--                                    </div>--}}
                    {{--                                @endif--}}
                    {{--                                <div class="media-body">--}}
                    {{--                                    <h6 class="media-heading my-1">--}}
                    {{--                                        <h6 href="{{$product->semi_finish_product_id}}"id="raw-{{$product->semi_finish_product_id*874}}">{{$product->name.' '.$product->specification}}</h6>--}}
                    {{--                                    </h6>--}}
                    {{--                                    <small>{{$product->desc}}</small>--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                        </a>--}}
                    {{--                        <hr class="m-0">--}}
                    {{--                    @endforeach--}}


                </div>
                {{--                <hr class="m-0">--}}
                {{--                <div class="scroll-container">--}}
                {{--                    <div class="p-3">--}}
                {{--                        <a href="view/history" class="btn btn-danger py-3 btn-block btn-lg text-white">BURDEN HISTORY--}}
                {{--                        </a>--}}
                {{--                    </div>--}}
                {{--                </div>--}}

            </div>
            <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show"><i
                        class="fa fa-chevron-right"></i>
                <i class="fa fa-chevron-left"></i>
            </a>
        </div>
        <!-- /.app-panel -->
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">TODAY BURDENS</h5>
            </div><!-- /.app-main-header -->
            <div class="app-main-content">
                <div class="projects-list" id="container2">
                    @foreach($burdens as $brdn)
                        @if ($brdn->status==3)
                            <a href="{{route('eburden.make',$brdn->id)}}">
                                @elseif ($brdn->status==4)
                                    <a href="{{route('eburden.grade',$brdn->id)}}">
                                        @else
                                            <a>
                                                @endif
                                                <div class="media content2">
                                                    <div class="avatar avatar text-white avatar-md project-icon bg-primary"
                                                         data-target="#project-1">
                                                        <i class="fa fa-pause" aria-hidden="true"></i>
                                                    </div>
                                                    <div class="media-body">
                                                        <small class="project-name"
                                                               id="project-1">{{$brdn->serial}}</small>
                                                        <h6 class="project-name"
                                                            id="project-1">{{$brdn['endProduct']->name}}</h6>
                                                        <div>
                                                            <small class="project-detail">Quantity
                                                                : {{$brdn->burden_size}}</small>
                                                        </div>

                                                    </div>


                                                    @if ($brdn->status==1)

                                                        <button onclick="printBurden({{$brdn->id}})" class="btn btn-outline-primary m-2" style="width:6rem">
                                                            <i class="fa fa-print" aria-hidden="true"></i>Print
                                                        </button>

                                                        <button class="btn btn-outline-primary" style="width:6rem">
                                                            <i class="fa fa-fire" aria-hidden="true"></i>pending
                                                        </button>
                                                    @endif
                                                    @if ($brdn->status==2)
                                                        <button onclick="printBurden({{$brdn->id}})" class="btn btn-outline-primary m-2" style="width:6rem">
                                                            <i class="fa fa-print" aria-hidden="true"></i>Print
                                                        </button>

                                                        <button class="btn btn-outline-dark" style="width:6rem">
                                                            <i class="fa fa-fire" aria-hidden="true"></i>merged
                                                        </button>
                                                    @endif
                                                    @if ($brdn->status==3)
                                                        <button class="btn btn-outline-dark"
                                                                style="width:6rem">
                                                            <i class="fa fa-fire" aria-hidden="true"></i>received
                                                        </button>
                                                    @endif
                                                    @if ($brdn->status==4)
                                                        <button disabled class="btn btn-outline-success"
                                                                style="width:6rem">
                                                            <i class="fa fa-fire" aria-hidden="true"></i>grade
                                                        </button>
                                                    @endif
                                                    @if ($brdn->status==1)
                                                        <!--button {{($brdn->endProduct->category->loading_type == 'to_loading') ?'disabled':null}}  onclick="deleteBurden({{$brdn->id}})"
                                                                class="btn btn-outline-danger ml-3 delete-burden"
                                                                style="width:6rem">
                                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                                            Delete
                                                        </button-->
                                                    @endif
                                                </div>
                                                <hr class="m-0">
                                            </a>
                            @endforeach



                </div>


            </div>
        </div>
    </div>

    <div class="modal fade" id="projects-task-modal" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="card">
                        <div class="projects-list">
                            {{ Form::open(array('route' => 'eburden.createchecklist','enctype'=>'multipart/form-data' ,'id'=>'bootstrap-wizard-form'))}}
                            <div class=" row">

                                <div class="col-sm-9">
                                    <label for="single">End Product</label>
                                    {!! Form::select('end_product', $endProducts , null , ['class' => 'form-control w-100','id'=>'end_product_select','name'=>'end_product_select']) !!}
                                </div>


                                <div class="col-sm-3">
                                    <label for="single">Expected Quantity</label>
                                    <input class="form-control" id="brdncont" name="burden_qty" placeholder=""
                                           type="text" style="width:100px">
                                </div>
                            </div>

                            <button type="submit" class="btn mt-3 btn-success btn-lg btn-block">
                                Add
                            </button>

                        </div>

                    </div>
                    {!!Form::close()!!}
                </div>

            </div>
        </div>
    </div>
    <script>
        function printBurden(burden_id){
            window.open('print/'+burden_id, '_blank');
        }
    </script>

@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/js/jquery.sortable.js')}}"></script>
    <script src="{{asset('assets/examples/js/demos/form.wizard.js')}}"></script>

    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>

    <script>
        $("#bootstrap-wizard-form").validate({
            rules: {
                end_product: "required",
                burden_qty: {required: !0, number: !0},
            },
            messages: {
                end_product: "Please Select End Product",
                burden_qty: "Please enter the Quantity",
            },
            errorElement: "div", errorPlacement: function (e, o) {
                e.addClass("form-control-feedback"), o.closest(".form-group").addClass("has-danger"), "checkbox" === o.prop("type") ? e.insertAfter(o.parent(".checkbox")) : e.insertAfter(o)
            }, highlight: function (e, o, r) {
                $(e).closest(".form-group").addClass("has-danger").removeClass("has-success"), $(e).removeClass("form-control-success").addClass("form-control-danger")
            }, unhighlight: function (e, o, r) {
                $(e).closest(".form-group").addClass("has-success").removeClass("has-danger"), $(e).removeClass("form-control-danger").addClass("form-control-success")
            }
        });

        $('#end_product_select').select2();

        function deleteBurden(burden) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "post",
                        url: '/end-burden/delete/burden',
                        data: {
                            burden_id: burden,
                        },
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            } else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                });
        }
    </script>
@stop
