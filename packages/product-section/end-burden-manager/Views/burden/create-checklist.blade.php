@extends('layouts.back.master')@section('title','Make End Product')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>

    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">

    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        #projects-task-modal2 .close {
            border: none;
            outline: none;
            box-shadow: none;
            position: absolute;
            right: 1rem;
            top: 1rem;
            width: 25px;
            height: 25px;
            line-height: 25px;
            text-align: center;
            font-size: 1.13rem;
            border-radius: 500px;
            border: 1px solid #868e96
        }

        #floating-button {
            width: 45px;
            height: 45px;
            border-radius: 50%;
            background: #db4437;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2;
            text-align: center;
            padding: 2px;
        }

        #floating-buttona {
            width: 45px;
            height: 45px;
            border-radius: 50%;
            background: #db4437;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2;
            text-align: center;
            padding: 2px;
        }

        .plus {
            color: white;

            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 38px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }


        #container {
            position: relative;
            padding: 0px;
            width: 100%;
            height: 200px;
            overflow: auto;
        }

        #container .content {
            width: 100%;
            height: 50%;
        }

        .hide {
            display: none;
        }


        #container2 {
            position: relative;
            /*padding: 0px;*/
            width: 98%;
            height: 420px;
            overflow: auto;
        }

        #container2 .content {
            width: 100%;
            height: 20%;
        }

        input[type=checkbox], input[type=radio] {
            height: 25px !important;
        }
    </style>
@stop
@section('content')
    {{ Form::open(array('route' => 'eburden.store','method'=>'post'))}}
    <input type="hidden" name="burden_size" value="{{$burden_qty}}">
    <input type="hidden" name="end_product_id" value="{{$endProduct->id}}">
    <input type="hidden" name="recipe_id" value="{{$endProductRecipes->id}}">


    @foreach($endProductRecipes->semiContent as $semiFinish)
        <a>
            <div class="media content">
                <div class="media-body">
                    <h6 class="project-name" id="{{$semiFinish->id}}">{{$semiFinish->semiFinishProduct->name}}</h6>
                </div>
                <div class="col-md-3">
                    Expected QTY : <label
                            id="label_{{$semiFinish->semiFinishProduct->semi_finish_product_id}}"> {{number_format((float)($semiFinish->units * $burden_qty), 3, '.', '')}}</label>
                    <input type="hidden" id="semi_loaded_qty_{{$semiFinish->semiFinishProduct->semi_finish_product_id}}"
                           value="{{$semiFinish->units * $burden_qty}}" class="semi-loaded-class">
                </div>
                <div class="col-md-3">
                    Remain QTY : <label
                            id="label_remain_{{$semiFinish->semiFinishProduct->semi_finish_product_id}}"> {{number_format((float)($semiFinish->units * $burden_qty), 3, '.', '')}}</label>
                </div>
            </div>
            <div class="col-md-12">
                <div class="" style="">
                    <div class="card-body">

                        @foreach($semiFinish->semiFinishProduct->GradeABurdenList($semiFinish->semiFinishProduct->semi_finish_product_id)->get() as $grade)

                            @if($semiFinish->semiFinishProduct->is_bake ==1)
                                @php
                                    $availble_bake =($grade->used_qty == $grade->burden_qty)?number_format((float)(round($grade->burden_qty,3)), 3, '.', ''):number_format((float)(round($grade->used_qty,3)), 3, '.', '')
                                @endphp
                                @if ($availble_bake>0.000)
                                    <div class="row burden mt-2">
                                        <div class="col-md-2 text-left">
                                            <input data-typex="burden_request" data-grade="{{$grade->id}}"
                                                   data-semi="{{$semiFinish->semiFinishProduct->semi_finish_product_id}}"
                                                   type="checkbox" onclick="checkBoxClick(this)" class="form-control"
                                                   name="burden_id[{{$grade->id}}]">
                                        </div>
                                        <div class="col-md-4">
                                            {{$grade->burden->serial}}
                                        </div>
                                        <div class="col-md-3">
                                            <label id="burden_expected_qty_{{$grade->id}}"> {{($grade->used_qty == $grade->burden_qty)?number_format((float)($grade->burden_qty), 3, '.', ''):number_format((float)($grade->used_qty), 3, '.', '') }}</label>
                                        </div>
                                        <div class="col-md-3">
                                            <input type="number" value="0" step="0.01" name="burden_qty[{{$grade->id}}]"
                                                   id="burden_request_qty_{{$grade->id}}"
                                                   class="form-control burden_request_qty_{{$grade->id}}">
                                        </div>
                                    </div>
                                @endif

                            @else
                                @php
                                    $available_qty = ($grade->used_qty == $grade->actual_semi_product_qty)?number_format((float)(round($grade->actual_semi_product_qty,3)), 3, '.', ''): number_format((float)(round($grade->used_qty,3)), 3, '.', '');
                                @endphp
                                @if ($available_qty > 0.000)
                                    <div class="row burden mt-2">
                                        <div class="col-md-2 text-left">
                                            <input data-typex="cooking_request" data-grade="{{$grade->id}}"
                                                   data-semi="{{$semiFinish->semiFinishProduct->semi_finish_product_id}}"
                                                   type="checkbox" class="form-control" onclick="checkBoxClick(this)"
                                                   name="cooking_request_id[{{$grade->id}}]">
                                        </div>
                                        <div class="col-md-4">
                                            {{$grade->serial}}
                                        </div>
                                        <div class="col-md-3">
                                            <label id="cooking_expected_qty_{{$grade->id}}"> {{($grade->used_qty == $grade->actual_semi_product_qty)?number_format((float)($grade->actual_semi_product_qty), 3, '.', ''): number_format((float)($grade->used_qty), 3, '.', '')}}</label>
                                        </div>
                                        <div class="col-md-3">
                                            <input type="number" value="0" step="0.01"
                                                   id="cooking_request_qty_{{$grade->id}}"
                                                   name="cooking_request_qty[{{$grade->id}}]"
                                                   class="form-control cooking_request_qty_{{$grade->id}}">
                                        </div>
                                    </div>
                                @endif
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </a>
    @endforeach

    <button onclick="this.disabled=true;this.form.submit();" type="submit" id="btnNext" class="btn d-none mt-3 btn-success btn-lg btn-block">
        Next
    </button>

    {!!Form::close()!!}


@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/js/jquery.sortable.js')}}"></script>
    <script src="{{asset('assets/examples/js/demos/form.wizard.js')}}"></script>

    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>

    <script>
        function checkBoxClick(box) {
            let semi = box.dataset["semi"];
            let grade = box.dataset["grade"];
            let type = box.dataset['typex'];

            let inputBoxBurdenName = '#burden_request_qty_' + grade
            let inputBoxCookingName = '#cooking_request_qty_' + grade

            let burdenQTYName = '#burden_expected_qty_' + grade;
            let cookingQTYName = '#cooking_expected_qty_' + grade;

            let burdenQTY = parseFloat($(burdenQTYName).text());
            let cookingQTY = parseFloat($(cookingQTYName).text());


            let neededQtyName = '#semi_requested_qty_' + semi;
            let neededQty = $(neededQtyName).val();


            let semiLoadedName = '#semi_loaded_qty_' + semi;
            let semiLoaded = parseFloat($(semiLoadedName).val());

            let LblRemainName = '#label_remain_' + semi;
            let LblRemain = $(LblRemainName).text();

            if (box.checked) {
                if (type == 'burden_request') {
                    if (semiLoaded > 0) {
                        $(inputBoxBurdenName).attr('readonly', true)
                        if (semiLoaded >= burdenQTY) {
                            burdenQTY = parseFloat(burdenQTY).toFixed(3);
                            $(inputBoxBurdenName).val(burdenQTY)
                            $(semiLoadedName).val(semiLoaded - burdenQTY);
                        } else {
                            $(inputBoxBurdenName).val(semiLoaded)
                            $(semiLoadedName).val(semiLoaded - semiLoaded);
                        }
                    } else {
                        swal("Sorry!", "Quantity is Zero", "warning");
                        box.checked = false;
                    }
                } else {
                    if (semiLoaded > 0) {
                        $(inputBoxCookingName).attr('readonly', true)
                        if (semiLoaded >= cookingQTY) {
                            cookingQTY = parseFloat(cookingQTY).toFixed(3);
                            $(inputBoxCookingName).val(cookingQTY)
                            $(semiLoadedName).val(semiLoaded - cookingQTY);
                        } else {
                            $(inputBoxCookingName).val(semiLoaded)
                            $(semiLoadedName).val(semiLoaded - semiLoaded);
                        }
                    } else {
                        swal("Sorry!", "Quantity is Zero", "warning");
                        box.checked = false;
                    }
                }
            } else {
                if (type == 'burden_request') {
                    $(inputBoxBurdenName).attr('readonly', false)
                    let textVal = parseFloat($(inputBoxBurdenName).val());
                    $(inputBoxBurdenName).val(0)
                    $(semiLoadedName).val(semiLoaded + textVal);
                } else {
                    $(inputBoxCookingName).attr('readonly', false)
                    let textVal = parseFloat($(inputBoxCookingName).val());
                    $(inputBoxCookingName).val(0)
                    $(semiLoadedName).val(semiLoaded + textVal);
                }
            }

            semiLoaded = parseFloat($(semiLoadedName).val()).toFixed(3);
            $(LblRemainName).empty();
            $(LblRemainName).append(semiLoaded);


            let inputs = $('.semi-loaded-class');
            let arr = [];
            for (var i = 0; i < inputs.length; i++) {
                arr[i] = $(inputs[i]).val();
            }

            var new_arr = arr.filter(function (x) {
                return x > 0;
            });

            console.log(new_arr)
            if (new_arr.length == 0) {
                $('#btnNext').removeClass('d-none');
            } else {
                $('#btnNext').addClass('d-none');
            }

        }


    </script>
@stop
