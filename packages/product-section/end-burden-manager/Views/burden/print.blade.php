<html>
<head>
    <link rel="stylesheet" href="{{asset('assets/vendor/css/bootstrap.css')}}">
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/invoice.css')}}" rel="stylesheet">
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <style media="all">
        @page {
            margin: 30px 5px 30px 15px;
            size: 75mm 999mm;
        }
    </style>
</head>

<body style="margin-bottom: 100px">

<div class="row">

    <div class="col-xs-5">
        <img src="{{asset('assets/img/buntak_grace.jpeg')}}" style="width: 100px">
    </div>

    <div class="col-xs-7 text-left">
        <h6>
            <b>Ranjanas Holdings [Pvt] Ltd</b><br>
            Katukurunda<br>
            Habaraduwa<br>
            Hotline : +9477 330 4678<br>
            E-mail : buntalksl@icloud.com<br>
        </h6>
    </div>
</div>

<div class="row" style="margin-top: 15px;">

    <div class="col-xl-12 text-center">
        <h3><b>

                {{$burden->endProduct->name}}
            </b></h3>
    </div>
@php
    $merge = \EndProductBurdenManager\Models\EndProductBurdenHeader::whereId($burden->merge_request_id)->first();

@endphp


</div>

<div>
    <div class="row">
        <div class="col-xs-6 text-nowrap"><b>End Burden ID :</b></div>
        <div class="col-xs-6">{{$burden->serial}}</div>
    </div>
    <div class="row">
        <div class="col-xs-6 text-nowrap"><b>Serial :</b></div>
        <div class="col-xs-6">{{$merge->serial}}</div>
    </div>

    <div class="row">
        <div class="col-xs-6 text-nowrap"><b>Burden Size (Kg/Unit) :</b></div>
        <div class="col-xs-6"><h4><b>{{$burden->burden_size}}</b></h4></div>
    </div>
    <div class="row">
        <div class="col-xs-6 text-nowrap"><b>Expected Quantity :</b></div>
        <div class="col-xs-6">{{$burden->expected_end_product_qty}}</div>
    </div>
    <div class="row">
        <div class="col-xs-6 text-nowrap"><b>Active Recipe :</b></div>
        <div class="col-xs-6">{{$burden['recipe']->name}}</div>
    </div>
    <div class="row">
        <div class="col-xs-6 text-nowrap"><b>Date with Time :</b></div>
        <div class="col-xs-6">{{\Carbon\Carbon::now()->format('Y-m-d H:i:s')}}</div>
    </div>

</div>

<br>


<div class="row">
    <div class="col-xs-12">
        <table class="table">
            <thead>
            <tr>
                <th>Item Name</th>
                <th class="text-center">Volume</th>
                <th class="text-right">Checked</th>

            </tr>
            </thead>
            <tbody>
            @php $cost = 0; @endphp
            @foreach($recipe->rawContent as $r)
                <tr>
                    <td> <h4> {{$r->rawMaterial->name}}/<font face="FMMalithi x"><b> {{$r->rawMaterial->sin_name}}</b></font> -[{{$r->rawMaterial->scale_key}}]</h4></td>


                    <td class="text-center"><h4> {{$r->units * $burden->burden_size}}
                        </h4></td>
                    <td align="right">
                        <div style="width:20px;height:20px;border:1px solid #000;"></div>
                    </td>
                    @php  @endphp
                </tr>
            @endforeach

            <hr>
            @foreach($recipe->packingContent as $r)

                <tr>
                    <td> <h4>{{$r->packingItem->name}}</h4></td>


                    <td class="text-center"><h4> {{$r->units * $burden->burden_size}}
                        </h4></td>
                    <td align="right">
                        <div style="width:20px;height:20px;border:1px solid #000;"></div>
                    </td>

                </tr>
            @endforeach


            </tbody>
        </table>
    </div>

</div>
<div class="row">
    <div class="col-xs-12 pull-right text-right">
        <div class="col-xs-6 text-nowrap"><b>Total Recipe Items:</b></div>
        <div class="col-xs-6"><h4><b>{{$recipe->burden_items}}</b></h4></div>

    </div>

    <div class="col-xs-12 pull-right text-right">
        <div class="col-xs-6 text-nowrap"><b>Total Recipe Weight(Kg):</b></div>
        <div class="col-xs-6"><h4><b>{{number_format((($recipe->burden_weight * $burden->burden_size)/1000),3)}}</b></h4></div>

    </div>
    <div class="col-xs-12 pull-right text-right">
        <div class="col-xs-6 text-nowrap"><b>Scale Weight:</b></div>
        <div class="col-xs-6 -align-right" style="width:100px;height:30px;border:1px solid #000;"></div>

    </div>
</div>



@if($recipe->packingContent->count() != 0)
<hr>
<div class="row text-center">
    <div class="col-xs-12"><h4><b><u>
                    Stickers Printing Data
                </u></b></h4>

    </div>
</div>
<div class="row">
    <div class="col-md-6"><b>
        Date Of Manufacture :
        </b></div>
    <div class="col-md-6" style="text-align: right;"><h4><b>
        {{\Carbon\Carbon::today()->addDay()->format('d-m-Y')}}
            </b></h4></div>
</div>


<div class="row">
    <div class="col-md-6"><b>
        Date Of Expiry :
        </b></div>
    <div class="col-md-6" style="text-align: right;"><h4><b>
        {{\Carbon\Carbon::today()->addDays($recipe->days_expire + 1)->format('d-m-Y')}}
            </b></h4></div>
</div>

<div class="row">
    <div class="col-md-6"><b>
        Batch Number :
        </b></div>
    <div class="col-md-6" style="text-align: right;"><h4><b>
        {{$burden->serial}}
            </b></h4></div>
</div>

<div class="row">
    <div class="col-md-6"><b>
        Printed Price :
        </b></div>
    <div class="col-md-6" style="text-align: right;"><h4><b>
        {{$recipe->endProduct->printed_price}}
            </b></h4></div>
</div>

<div class="row text-center">
    <div class="col-xs-12"><h4><b><u>
        Stickers Issue Order
                </u></b></h4>

    </div>
</div>

<div class="row" style="margin-top: 15px;">
    <div class="col-xs-4"><b>First Lot:</b></div>
    <div class="col-xs-4" style="width:60px;height:30px;border:1px solid #000;"></div>
    <div class="col-xs-4"><b>.............</b></div>

</div>

<div class="row" style="margin-top: 15px;">
    <div class="col-xs-4"><b>Second Lot:</b></div>
    <div class="col-xs-4" style="width:60px;height:30px;border:1px solid #000;"></div>
    <div class="col-xs-4"><b>.............</b></div>

</div>

<div class="row" style="margin-top: 15px;">
    <div class="col-xs-4"><b>Third Lot:</b></div>
    <div class="col-xs-4" style="width:60px;height:30px;border:1px solid #000;"></div>
    <div class="col-xs-4"><b>.............</b></div>

</div>

@endif


<br>

<footer>


    <div class="row col-xs-12" style="margin-top: 60px;">

        <div class="text-center col-xs-12">
            <h5>
                .........................................<br>
                Signature-Stores Superviser
            </h5>
        </div>
    </div>
    <div class="row col-xs-12" style="margin-top: 60px;">
        <div class="text-center col-xs-12">
            <h5>
                .........................................<br>
                Signature-Manager
            </h5>
        </div>

    </div>
    <div class="row col-xs-12" style="margin-top: 60px;">
        <div class="text-center col-xs-12">
            <h5>
                .........................................<br>
                Name with Signature-Accepted By
            </h5>
        </div>

    </div>


</footer>

</body>

</html>

<!--link rel="stylesheet" href="{{asset('assets/vendor/css/bootstrap.css')}}">
<script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

<style media="all">
    @page {
        margin: 30px 5px 30px 5px;
        size: 75mm 999mm;
    }
</style>


<div class="text-center">
    <h6 class="text-uppercase mb-1">Burden Serial : {{$burden->serial}}</h6>
    <h6 class="text-uppercase mb-1" id="{{$burden->id*754}}">{{$burden->endProduct->name}}</h6>
    <h6 class="text mb-lg-1">{{'Burden Size : '.$burden->burden_size}} </h6>
    <br>
</div>

<hr class="m-0">

<div class="text-center py-3">
    {{'Active Recipe : '.$burden->recipe->name}}<br>
    <hr class="mt-1">
    <h6 class="text mb-lg-1">{{\Carbon\Carbon::now()->format('Y-m-d H:i:s')}}</h6>
</div>


<div class="app-main-header">
    <h5 class="app-main-heading text-center">{{$burden->endProduct->name}} Burden Requested
        Ingredients</h5>
</div>
<div class="scroll-container" id="scroll-container">
    <div class="app-main-content">
        <div class="">
            <div class="media-list">
                <hr class="mt-1 mb-1">
                @php $cost = 0; @endphp
                @foreach($recipe->rawContent as $r)
                    <div class="row burden">
                        <div class="col-md-4">
                            {{$r->rawMaterial->name}}
                        </div>
                        <div class="col-md-4" style="text-align: center;">

                        </div>
                        <div class="col-md-4" style="text-align: right;">
                            {{$r->units * $burden->burden_size}}
                        </div>
                    </div>
                @endforeach
                <hr>
                @foreach($recipe->packingContent as $r)

                    <div class="row burden">
                        <div class="col-md-4">
                            {{$r->packingItem->name}}
                        </div>
                        <div class="col-md-4" style="text-align: center;">

                        </div>
                        <div class="col-md-4" style="text-align: right;">
                            {{$r->units * $burden->burden_size}}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>



        <hr style="border: black dashed 3px">
        <div class="" style="padding-top: 25px">
            <div class="media-list">
                <div class="row">
                    <div class="col-md-6">
                        Number Of Units
                    </div>
                    <div class="col-md-6" style="text-align: right;">
                        {{$recipe->burden_items}}
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        Recipe weight
                    </div>
                    <div class="col-md-6" style="text-align: right;">
                        {{$recipe->burden_weight * $burden->burden_size}}
                    </div>
                </div>
                <hr>

                <div class="row">
                    <div class="col-md-6">
                        mfd
                    </div>
                    <div class="col-md-6" style="text-align: right;">
                        {{\Carbon\Carbon::today()->addDay()->format('Y-m-d')}}
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        exd
                    </div>
                    <div class="col-md-6" style="text-align: right;">
                        {{\Carbon\Carbon::today()->addDays($recipe->days_expire + 1)->format('Y-m-d')}}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        Batch Number
                    </div>
                    <div class="col-md-6" style="text-align: right;">
                        {{$burden->serial}}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        Price
                    </div>
                    <div class="col-md-6" style="text-align: right;">
                        {{$recipe->endProduct->printed_price}}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        Issued Stickers
                    </div>
                    <div class="col-md-6" style="text-align: right;">

                    </div>
                </div>

            </div>
        </div>

        <hr>
        <hr>
        <div class="">
            <div class="media-list">



                <div class="row">
                    <div class="col-md-6">
                        mfd
                    </div>
                    <div class="col-md-6">

                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        exd
                    </div>
                    <div class="col-md-6">

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        Batch Number
                    </div>
                    <div class="col-md-6" style="text-align: right;">
                        {{$burden->serial}}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        Price
                    </div>
                    <div class="col-md-6" style="text-align: right;">
                        {{$recipe->endProduct->printed_price}}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        Issued Stickers
                    </div>
                    <div class="col-md-6">

                    </div>
                </div>

            </div>
        </div>


        <hr>
        <hr>
        <div class="text-center">
            <h6 class="text-uppercase mb-1">Burden Serial : {{$burden->serial}}</h6>
            <h6 class="text-uppercase mb-1" id="{{$burden->id*754}}">{{$burden->endProduct->name}}</h6>
            <h6 class="text mb-lg-1">{{'Burden Size : '.$burden->burden_size}} </h6>
            <br>
        </div>

        <hr class="m-0">

        <div class="text-center py-3">
            {{'Active Recipe : '.$burden->recipe->name}}<br>
            <hr class="mt-1">
            <h6 class="text mb-lg-1">{{\Carbon\Carbon::now()->format('Y-m-d H:i:s')}}</h6>
        </div>


        <div class="app-main-header">
            <h5 class="app-main-heading text-center">{{$burden->endProduct->name}} Burden Requested
                Ingredients</h5>
        </div>

    </div>

    <div class="scroll-container" id="scroll-container">
        <div class="app-main-content">
            <div class="">
                <div class="media-list">
                    <hr class="mt-1 mb-1">
                    @php $cost = 0; @endphp
                    @php $allo = \BurdenManager\Models\AllocateSemiFinish::whereendProductBurdenId($burden->id)->get(); @endphp

                    @foreach($allo as $r)
                        @if($r->burden_type=='burden')
                            <div class="row burden">
                                <div class="col-md-4">
                                    {{$r->burden->burden->semiProdcuts->name}}
                                </div>
                                <div class="col-md-4" style="text-align: center;">
                                    {{$r->burden->burden->serial}}
                                </div>
                                <div class="col-md-4" style="text-align: right;">
                                    {{$r->allocate_qty}}
                                </div>
                            </div>
                        @else
                            <div class="row burden">
                                <div class="col-md-4">
                                    {{$r->burden->semiProdcuts->name}}
                                </div>
                                <div class="col-md-4" style="text-align: center;">
                                    {{$r->burden->serial}}
                                </div>
                                <div class="col-md-4" style="text-align: right;">
                                    {{$r->allocate_qty}}
                                </div>
                            </div>
                        @endif

                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div-->


