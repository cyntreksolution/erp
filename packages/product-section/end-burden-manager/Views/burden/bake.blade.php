@extends('layouts.back.master')@section('title','Burden | Creating')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.wizard.css')}}">

    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        #floating-buttona {
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #db4437;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .plus {
            color: white;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }

        .hide {
            display: none;
        }

        #container {
            position: relative;
            /*margin: 0px auto;*/
            padding: 0px;
            width: 100%;
            height: 300px;
            overflow: auto;
        }

        #container .content {
            width: 100%;
            height: 30%;
        }

    </style>
@stop
@section('current','Burden')
@section('current_url',route('burden.create'))
@section('current2','Creating Burden')
@section('current_url2','')
@section('content')
    <div class="app-wrapper">
        <div class="profile-section-user">
            <div class="profile-cover-img profile-pic">
                <img src="{{asset('assets\img\semi2.jpg')}}" alt="">
            </div>
            <div class="profile-info-brief p-3">
                @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                <div class="user-profile-avatar avatar avatar-circle avatar-md project-iconr bg-{{$color[$burden->colour]}}"
                     data-plugin="firstLitter"
                     data-target="#{{$burden->id*754}}"></div>

                <div class="text-center">
                    <h5 class="text-uppercase mb-1" id="{{$burden->id*754}}">{{$burden['semiProdcuts']->name}}</h5>
                    <h6 class="text mb-lg-1">{{'Burden Size : '.$burden->burden_size}} KG</h6>
                    <br>
                </div>

                <hr class="m-0">

                <div class="text-center py-3">
                    {{'Expected Quantity : '.$burden->expected_semi_product_qty}} units<br>
                    {{'Active Recipe : '.$burden['recipe']->name}}<br>
                    <hr class="m-0">
                    {{$burden['recipe']->desc}}
                </div>
            </div>
            <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show"><i
                        class="fa fa-chevron-right"></i>
                <i class="fa fa-chevron-left"></i>
            </a>
        </div>


        <div class="app-main">

            <div class="app-main-header">
                <h5 class="app-main-heading text-center">CREATED BURDEN</h5>
            </div>


            <div class="app-main-content" >
                <div class="">
                    {{ Form::open(array('url' => 'burden/bake/'.$burden->id))}}
                    <input type="hidden" name="supplier_id" id="supplier_id">
                    <div class="projects-list">
                        <div class="row px-5">
                            <div class="col-md-9 py-4">
                                <div class="d-flex mr-auto">
                                    <div class="form-group">
                                        <select id="single" name="Single"
                                                class="form-control select2 " ui-jp="select2"
                                                style="width: 15rem"
                                                ui-options="{theme: 'bootstrap'}">
                                            <option>Select Employees</option>
                                            @foreach($employers as $emp)
                                                <option value=" {{$emp->id}}">{{$emp->full_name}}</option>
                                            @endforeach

                                        </select>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 py-4 ">
                                <div class="btn btn-outline-primary" id="a">Add Employees</div>

                            </div>


                        </div>
                        <hr>

                        <div class="abc px-5" id="container">

                        </div>

                    </div>
                 </div>
            </div>
            <button type="submit" class="btn btn-danger btn-lg btn-block"> Burn Now</button>
            {!!Form::close()!!}

        </div>
    </div>





@stop
@section('js')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    {{--<script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>--}}
    <script src="{{asset('assets/examples/js/demos/form.wizard.js')}}"></script>
    <script src="{{asset('assets/examples/js/apps/projects.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>



    <script>


        var ps = new PerfectScrollbar('#container');


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $('.delete').click(function (e) {
            e.preventDefault();
            id = $(this).attr("value");
            confirmAlert(id);

        });

        function confirmAlert(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "delete",
                        url: id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            }
                            else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

        $("#a").click(function () {

            var a = $("#single option:selected").val();

            $.ajax({
                type: "get",
                url: '/../employee/get/employee',
                data: {id: a},

                success: function (response) {
//'+ response['name']+'
                    $(".abc").append('<div class="worker " style="width: 100%" > ' +
                        '<div class="card-body d-flex border-b-1 flex-wrap align-items-center p-2">' +
                        '<div class="d-flex mr-auto"><div>' +
                        '<div class="avatar avatar text-white avatar-md project-icon bg-primary"><img class="card-img-top"src="" alt="">' +
                        '</div></div>' +
                        '<h5 class="mt-4 mx-4">' + response['first_name'] + ' ' + response['last_name'] + '</h5>' +
                        '<input type="hidden" name="employee" value=" ' + response['id'] + ' "/>' +
                        '<input type="hidden"/>' +
                        '<h6 class="float-left mt-4" style=""> </h6>' +
                        '</div>' +
                        '<div class="d-flex activity-counters justify-content-between">' +
                        '<div class="text-center px-2">' +
                        //                        '<input class="form-control" id="formGroupExampleInput" placeholder="" type="text" style="width:8.9rem" disabled>' +
                        //                        '<small class="text-primary">Ordered</small>' +
                        '</div>' +
                        //                        '<div class="text-center px-2">' +
                        //                        '<small class="text-danger">Price</small>' +
                        '</div>' +
                        '<div class="section-2">' +
                        '<div onclick="hidediv(this)" class="delete btn btn-light btn-sm ml-2" value=" ' + response['id'] + ' " data-toggle="tooltip" data-placement="left" title="DELETE"> ' +
                        '<i class="zmdi zmdi-close"></i>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>');
                }
            });

            reset();
        });

        function hidediv(e) {
            $(e).closest(".worker").addClass("hide");

//            var div = this.document.getElementById("aa");
//            div.style.display = "none";

        }

        function reset() {

            document.getElementById("single").value = "";

        }

    </script>
@stop