<html>
<head>
    <title>Merge Preview</title>
    <link rel="stylesheet" href="{{asset('assets/vendor/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }

        .lead {
            font-size: 18px;
            font-weight: 800;
            margin: 10px;
        }

        caption {
            caption-side: top;
            margin: 10px 0 0 0;
            padding: 0;
        }

        table.dataTable tbody td {
            padding: 3px !important;
        }

        table.dataTable thead td {
            padding: 3px !important;;
        }

        /*table > tbody > tr:last-child {*/
        /*    color: white;*/
        /*    background: grey;*/
        /*}*/
    </style>
</head>

<body>

<div class="col-sm-12">
    <div class="col-md-12">
        @if(!empty($dataRawProductPoints[0]['data']))
            <table class="table display text-center" id="raw_table">
                <caption class="lead">Raw Materials</caption>
                <thead>
                <tr>
                    <th data-orderable="false" style="text-align: left">Item</th>
                    @foreach($showRawTypeUnique[0] as $showRawType)
                        <th style="text-align: right">{{$showRawType}}</th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                @foreach($dataRawProductPoints as $dataRawProductPoints)
                    <tr>
                        <td style="text-align: left">{{$dataRawProductPoints['end_product_name']}}</td>
                        @for($no = 0; $no < sizeof($dataRawProductPoints['data']); $no++)
                            <td style="text-align: right">{{$dataRawProductPoints['data'][$no]['qty']}}</td>
                        @endfor
                    </tr>
                @endforeach
                <tr>
                    <td style="text-align: left">Total</td>
                    @foreach($showRawTypeUnique[0] as $showRawType)
                        @php $key = array_search($showRawType, array_column(json_decode($raw_materials_total), 'raw_material_name')); @endphp
                        <td style="text-align: right">{{$raw_materials_total[$key]->qty}}</td>
                    @endforeach
                </tr>
                </tbody>
            </table>
        @endif
    </div>
    <div class="col-md-12">
        @if(!empty($dataSemiProductPoints[0]['data']))
            <table class="table display text-center mt-5" id="semi_table">
                <caption class="lead">Semi Finish Products</caption>
                <thead>
                <tr>
                    <th style="text-align: left">Item</th>
                    @foreach($showSemiTypeUnique[0] as $showSemiType)
                        <th style="text-align: right">{{$showSemiType}}</th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                @foreach($dataSemiProductPoints as $dataSemiProductPoint)
                    <tr>
                        <td style="text-align: left">{{$dataSemiProductPoint['end_product_name']}}</td>
                        @for($no = 0; $no < sizeof($dataSemiProductPoint['data']); $no++)
                            <td style="text-align: right">{{$dataSemiProductPoint['data'][$no]['qty']}}</td>
                        @endfor
                    </tr>
                @endforeach
                <tr>
                    <td style="text-align: left">Total</td>
                    @foreach($showSemiTypeUnique[0] as $showSemiType)
                        @php $key = array_search($showSemiType, array_column(json_decode($semi_finish_products_total), 'semi_finish_product_name')); @endphp
                        <td style="text-align: right">{{$semi_finish_products_total[$key]->qty}} </td>
                    @endforeach
                </tr>
                <tr>
                    <td style="text-align: left">Burden QTY</td>
                    @foreach($showSemiTypeUnique[0] as $showSemiType)
                        @php $key = array_search($showSemiType, array_column(json_decode($semi_finish_products_total), 'semi_finish_product_name')); @endphp
                        @php
                            $endProduct = \RecipeManager\Models\RecipeSemiProduct::where('semi_finish_product_semi_finish_product_id','=',$semi_finish_products_total[$key]->semi_finish_product_id)->whereStatus(2)->first();
                            $units =!empty($endProduct)?$endProduct->units:0;
                        @endphp
                        @if ($units >0)
                            <td style="text-align: right"> {{ number_format((float)$semi_finish_products_total[$key]->qty / $units, 3, '.', '')}}  </td>
                        @endif

                    @endforeach
                </tr>
                <tr>
                    <td style="text-align: left">Units Per Recipe</td>
                    @foreach($showSemiTypeUnique[0] as $showSemiType)
                        @php $key = array_search($showSemiType, array_column(json_decode($semi_finish_products_total), 'semi_finish_product_name')); @endphp
                        @php
                            $endProduct = \RecipeManager\Models\RecipeSemiProduct::where('semi_finish_product_semi_finish_product_id','=',$semi_finish_products_total[$key]->semi_finish_product_id)->whereStatus(2)->first();
                            $units =!empty($endProduct)?$endProduct->units:0;
                        @endphp
                        @if ($units >0)
                            <td style="text-align: right"> {{$units}}  </td>
                        @endif

                    @endforeach
                </tr>
                </tbody>
            </table>
        @endif
    </div>
    <div class="col-md-12">
        @if(!empty($dataPackingProductPoints[0]['data']))
            <table class="table display text-center mt-5" id="packing_table">
                <caption class="lead">Packing Items</caption>
                <thead>
                <tr>
                    <th style="text-align: left">Item</th>
                    @foreach($showPackingTypeUnique[0] as $showPackingType)
                        <th style="text-align: right">{{$showPackingType}}</th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                @foreach($dataPackingProductPoints as $dataPackingProductPoint)
                    <tr>
                        <td style="text-align: left">{{$dataPackingProductPoint['end_product_name']}}</td>
                        @for($no = 0; $no < sizeof($dataPackingProductPoint['data']); $no++)
                            <td style="text-align: right">{{$dataPackingProductPoint['data'][$no]['qty']}}</td>
                        @endfor
                    </tr>
                @endforeach
                <tr>
                    <td style="text-align: left">Total</td>
                    @foreach($showPackingTypeUnique[0] as $showPackingType)
                        @php $key = array_search($showPackingType, array_column(json_decode($packing_total), 'packing_name')); @endphp
                        <td style="text-align: right">{{$packing_total[$key]->qty}}</td>
                    @endforeach
                </tr>
                </tbody>
            </table>
        @endif
    </div>
    <div class="col-md-12 text-center mt-5">
        <h6 class="text-muted">Powered by Cyntrek Solutions<br>+9471 122 5489 / +9471 532 2262 </h6>
    </div>
</div>


<script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/popper.js/dist/umd/popper.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>

<script>
    $(document).ready(function () {
        table = $('.table').DataTable({
            searching: false,
            paging: false,
            info: false,
            responsive: true,
            ordering: false,
            order: [1, 'asc'],
        });
    });
</script>


</body>

</html>
