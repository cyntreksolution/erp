@extends('layouts.back.master')@section('title','End Product Burden Merge | List')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }
    </style>
@stop

@section('content')
{{--    <div class="app-main-content mb-2">--}}
{{--        <div class="row mx-2">--}}
{{--            <div class="col-md-5">--}}
{{--                {!! Form::select('category',$categories , null , ['class' => 'form-control','placeholder'=>'Select Category','id'=>'category']) !!}--}}
{{--            </div>--}}
{{--            <div class="col-md-5">--}}
{{--                {!! Form::select('end_product',$endProducts, null , ['class' => 'form-control','placeholder'=>'Select Product','id'=>'end_product']) !!}--}}
{{--            </div>--}}

{{--            <div class="col-md-2">--}}
{{--                <button class="btn btn-info" onclick="process_form(this)">Filter</button>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

{!! Form::open(array('route' => 'eburden.merge','id'=>'mergerForm','target'=>"_blank"))!!}
    <div class="">
        <table id="invoice_table" class="display text-center">
            <thead>
            <tr>
                <th></th>
                <th>Serial</th>
                <th>End Product</th>
                <th>Quantity</th>
                <th>Recipe</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
{{--                        @foreach($burdens as $burden)--}}
{{--                            <tr>--}}
{{--                                <td><input type="checkbox" name="burden_id[]" value="{{$burden->id}}"></td>--}}
{{--                                <td>{{$burden->serial}}</td>--}}
{{--                                <td>{{$burden->endProduct->name}}</td>--}}
{{--                                <td>{{$burden->burden_size}}</td>--}}
{{--                                <td>{{$burden->expected_end_product_qty	}}</td>--}}
{{--                                <td>{{$burden->recipe->name}}</td>--}}
{{--                                <td>--}}
{{--                                    <button class="btn btn-sm btn-primary"><i class="fa fa-truck"></i></button>--}}
{{--                                    <button value="{{$burden->id}}" class="addItem btn btn-sm btn-warning ml-1">--}}
{{--                                        <i class="fa fa-hand-grab-o"></i></button>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                        @endforeach--}}
            </tbody>
            <tfoot>
            <tr>
                <th></th>
                <th>Serial</th>
                <th>End Product</th>
                <th>Quantity</th>
                <th>Recipe</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
        <div class="col-md-12 mt-5 float-right">
            <div class="row">
                <div class="col-md-6">
                    <button class="btn btn-block  d-none btn-primary" id="btn_sumbit1" name="showButton" value="showSubmit">Merge Selected Show
                    </button>
                </div>
                <div class="col-md-6">
                    <button class="btn btn-block d-none  btn-success" id="btn_sumbit2" name="saveButton" value="saveSubmit">Merge Save</button>
                </div>
            </div>

        </div>
        {{ Form::close() }}
    </div>

    <div class="modal fade" id="ChangeQty" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(['url'=>'','id'=>'qtyChangeForm']) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            <div class="col-md-6">
                                <span>Quantity</span>
                            </div>
                            <div class="col-md-6">
                                <input name="qty" type="text" class="task-name-field" value="">
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="Change">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
    <script type="text/javascript"
            src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js"></script>

    <script>

        // $(".addItem").click(function (event) {
        //     alert(2)
        //     event.preventDefault();
        //     $("#qtyChangeForm").attr('action', '/end-burden/edit/qty/' + this.value);
        //     $('#ChangeQty').modal('show')
        // });

        function showModal(id){
            event.preventDefault();
            $("#qtyChangeForm").attr('action', '/end-burden/edit/qty/' + id);
            $('#ChangeQty').modal('show')
        }


        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": false,
            "singleDatePicker": true,
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019"
        });
        $('#date_range').val('')

        $(document).ready(function () {
            table = $('#invoice_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "ajax": {
                    url: "{{url('/end-product-merge-list/table/data')}}",
                    type: "get",
                    error: function () {
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    }
                },
                pageLength: 250,
                responsive: true
            });
        });

        function process_form(e) {

            let category = $("#category").val();
            let end_product = $("#end_product").val();
            let table = $('#invoice_table').DataTable();

            if (category != null && category != '') {
                $('#btn_sumbit1').removeClass('d-none');
                $('#btn_sumbit2').removeClass('d-none');
            } else {
                $('#btn_sumbit1').addClass('d-none');
                $('#btn_sumbit2').addClass('d-none');
            }

            table.ajax.url('/end-product-merge-list/table/data?category=' + category + '&end_product=' + end_product + '&filter=' + true).load();

        }


        function process_form_reset() {
            $("#category").val('');
            $("#day").val('');
            $("#time").val('');
            $("#status").val('');
            $("#agent").val('');
            let table = $('#esr_table').DataTable();
            table.ajax.url('order/list/table/data').load();
        }

    </script>

@stop
