@extends('layouts.back.master')@section('title','Make End Product')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>

    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">

    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        #projects-task-modal2 .close {
            border: none;
            outline: none;
            box-shadow: none;
            position: absolute;
            right: 1rem;
            top: 1rem;
            width: 25px;
            height: 25px;
            line-height: 25px;
            text-align: center;
            font-size: 1.13rem;
            border-radius: 500px;
            border: 1px solid #868e96
        }

        #floating-button {
            width: 45px;
            height: 45px;
            border-radius: 50%;
            background: #db4437;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2;
            text-align: center;
            padding: 2px;
        }

        #floating-buttona {
            width: 45px;
            height: 45px;
            border-radius: 50%;
            background: #db4437;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2;
            text-align: center;
            padding: 2px;
        }

        .plus {
            color: white;

            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 38px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }


        #container {
            position: relative;
            padding: 0px;
            width: 100%;
            height: 200px;
            overflow: auto;
        }

        #container .content {
            width: 100%;
            height: 50%;
        }

        .hide {
            display: none;
        }


        #container2 {
            position: relative;
            /*padding: 0px;*/
            width: 98%;
            height: 420px;
            overflow: auto;
        }

        #container2 .content {
            width: 100%;
            height: 20%;
        }

    </style>
@stop
@section('current','Burden')
@section('current_url',route('burden.create'))
@section('content')
    <div class="app-wrapper">
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">TODAY BURDENS MAKE</h5>
            </div><!-- /.app-main-header -->
            <div class="app-main-content">
                <div class="projects-list" id="container2">
                    {{ Form::open(array('route' => ['eburden.make',$burden],'enctype'=>'multipart/form-data' ,'id'=>'bootstrap-wizard-form'))}}
                    <div class=" row">
                        <div class="col-sm-3">
                            <label for="single">Create Team</label>
                        </div>
                        <div class="col-sm-9">
                            {!! Form::select('employee_group', $teams , null , ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class=" row mt-3">
                        <div class="col-sm-3">
                            <label for="single">Baker</label>
                        </div>
                        <div class="col-sm-9">
                            {!! Form::select('baker', $employers , null , ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn mt-3 btn-success btn-lg btn-block">
                    Make Now
                </button>

                {!!Form::close()!!}
            </div>
        </div>
    </div>


@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/js/jquery.sortable.js')}}"></script>
    <script src="{{asset('assets/examples/js/demos/form.wizard.js')}}"></script>

    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>


@stop