@extends('layouts.back.master')@section('title','Make End Product')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>

    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">

    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        #projects-task-modal2 .close {
            border: none;
            outline: none;
            box-shadow: none;
            position: absolute;
            right: 1rem;
            top: 1rem;
            width: 25px;
            height: 25px;
            line-height: 25px;
            text-align: center;
            font-size: 1.13rem;
            border-radius: 500px;
            border: 1px solid #868e96
        }

        #floating-button {
            width: 45px;
            height: 45px;
            border-radius: 50%;
            background: #db4437;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2;
            text-align: center;
            padding: 2px;
        }

        #floating-buttona {
            width: 45px;
            height: 45px;
            border-radius: 50%;
            background: #db4437;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2;
            text-align: center;
            padding: 2px;
        }

        .plus {
            color: white;

            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 38px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }


        #container {
            position: relative;
            padding: 0px;
            width: 100%;
            height: 200px;
            overflow: auto;
        }

        #container .content {
            width: 100%;
            height: 50%;
        }

        .hide {
            display: none;
        }


        #container2 {
            position: relative;
            /*padding: 0px;*/
            width: 98%;
            height: 420px;
            overflow: auto;
        }

        #container2 .content {
            width: 100%;
            height: 20%;
        }

    </style>
@stop
@section('current','Burden')
@section('current_url',route('burden.create'))
@section('content')
    <div class="app-wrapper">
        <!-- /.app-panel -->
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">Allocate Semi Finish</h5>
            </div><!-- /.app-main-header -->
            <div class="app-main-content">
                <div class="projects-list" id="container2">
                    {{ Form::open(array('route' => 'eburden.store','enctype'=>'multipart/form-data' ,'id'=>'bootstrap-wizard-form'))}}
                    <table class="table table-borderless">
                        <thead>
                        @php $endProductDetails = EndProductManager\Models\EndProduct::where('id',"=",$endProductId)->first(); @endphp
                        <tr>
                            <th>End Product : {{$endProductDetails->name}} </th>
                            <input type="hidden" name="end_product_id" value="{{$endProductId}}">
                            <th> Burden Qty : {{$burdenQty}}</th>
                            <input type="hidden" name="burden_qty" value="{{$burdenQty}}">
                        </tr>
                        @foreach($burdenIds as $key=>$value)
                            <input type="hidden" name="burdenIds[]" value="{{$value}}">
                            <input type="hidden" name="burdenDefQtys[]" value="{{$burdenDefQtys[$key]}}">
                            <input type="hidden" name="burdenQtys[]" value="{{$burdenQtys[$key]}}">
                        @endforeach
                        @foreach($CRIds as $key=>$value)
                            <input type="hidden" name="CRIds[]" value="{{$value}}">
                            <input type="hidden" name="CRDefIdQtys[]" value="{{$CRDefQtys[$key]}}">
                            <input type="hidden" name="CRQtys[]" value="{{$CRQtys[$key]}}">
                        @endforeach

                        </thead>
                        <tbody>
                        <tr>
                            <th>Semi Finish Product</th>
                            <th>Expected Qty</th>
                            <th>Allocate Qty</th>
                            <th>Difference</th>
                        </tr>
                        @php $index= 0;
                       $totalSum =0;
                        @endphp
                        @foreach($semi_finish_product_id as $semi_finish_product)
                            @php $semiDetail = SemiFinishProductManager\Models\SemiFinishProduct::where("semi_finish_product_id","=",$semi_finish_product)->first();
                    ; @endphp
                            <input type="hidden" name="semi_finish_product_id[]" value="{{$semi_finish_product}}">
                            <input type="hidden" name="semi_finish_product_burden_qty[]"
                                   value="{{$semi_finish_product_burden_qty[$index]}}">
                            <input type="hidden" name="semi_order[]" value="{{$semi_order[$index]}}">
                            <tr>
                                <td>{{$semiDetail->name}} :</td>
                                <td>{{$semi_finish_product_burden_qty[$index]}} :</td>
                                @if($semi_order[$index] == "Burden")
                                    <td>({{$totalBurdenqty}})</td>
                                    @php
                                        $totalSum = $totalSum+ ($semi_finish_product_burden_qty[$index]-$totalBurdenqty);
                                    @endphp
                                    <td>{{$semi_finish_product_burden_qty[$index]-$totalBurdenqty}}</td>
                                @endif
                                @if($semi_order[$index] == "CookReq")
                                    <td>({{$total_cooking_request_qty}})</td>
                                    @php
                                        $totalSum = $totalSum+ ($semi_finish_product_burden_qty[$index]-$total_cooking_request_qty);
                                    @endphp
                                    <td>{{$semi_finish_product_burden_qty[$index]-$total_cooking_request_qty}}</td>
                                @endif
                            </tr>
                            @php $index++; @endphp
                        @endforeach
                        </tbody>
                    </table>
                    @if($totalSum == 0)
                        <button type="submit" class="btn mt-3 btn-success btn-lg btn-block">
                            Next
                        </button>
                    @endif
                    @if($totalSum != 0)
                        <a href="/end-burden/create" class="btn mt-3 btn-warning btn-lg btn-block">
                            Back
                        </a>
                    @endif
                </div>
            </div>
            {!!Form::close()!!}
        </div>
    </div>

    <div class="modal fade" id="projects-task-modal" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="card">
                        <div class="projects-list">

                        </div>

                    </div>
                    {!!Form::close()!!}
                </div>

            </div>
        </div>
    </div>

@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/js/jquery.sortable.js')}}"></script>
    <script src="{{asset('assets/examples/js/demos/form.wizard.js')}}"></script>

    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>

    <script>
        $("#bootstrap-wizard-form").validate({
            rules: {
                end_product: "required",
                burden_qty: {required: !0, number: !0},
            },
            messages: {
                end_product: "Please Select End Product",
                burden_qty: "Please enter the Quantity",
            },
            errorElement: "div", errorPlacement: function (e, o) {
                e.addClass("form-control-feedback"), o.closest(".form-group").addClass("has-danger"), "checkbox" === o.prop("type") ? e.insertAfter(o.parent(".checkbox")) : e.insertAfter(o)
            }, highlight: function (e, o, r) {
                $(e).closest(".form-group").addClass("has-danger").removeClass("has-success"), $(e).removeClass("form-control-success").addClass("form-control-danger")
            }, unhighlight: function (e, o, r) {
                $(e).closest(".form-group").addClass("has-success").removeClass("has-danger"), $(e).removeClass("form-control-danger").addClass("form-control-success")
            }
        });

        $('#end_product_select').select2();

        function deleteBurden(burden) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "post",
                        url: '/end-burden/delete/burden',
                        data: {
                            burden_id: burden,
                        },
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            } else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                });
        }
    </script>
@stop
