@extends('layouts.back.master')@section('title','Grade End Product')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>

    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">

    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        #projects-task-modal2 .close {
            border: none;
            outline: none;
            box-shadow: none;
            position: absolute;
            right: 1rem;
            top: 1rem;
            width: 25px;
            height: 25px;
            line-height: 25px;
            text-align: center;
            font-size: 1.13rem;
            border-radius: 500px;
            border: 1px solid #868e96
        }

        #floating-button {
            width: 45px;
            height: 45px;
            border-radius: 50%;
            background: #db4437;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2;
            text-align: center;
            padding: 2px;
        }

        #floating-buttona {
            width: 45px;
            height: 45px;
            border-radius: 50%;
            background: #db4437;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2;
            text-align: center;
            padding: 2px;
        }

        .plus {
            color: white;

            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 38px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }


        #container {
            position: relative;
            padding: 0px;
            width: 100%;
            height: 200px;
            overflow: auto;
        }

        #container .content {
            width: 100%;
            height: 50%;
        }

        .hide {
            display: none;
        }


        #container2 {
            position: relative;
            /*padding: 0px;*/
            width: 98%;
            height: 420px;
            overflow: auto;
        }

        #container2 .content {
            width: 100%;
            height: 20%;
        }

    </style>
@stop
@section('current','Burden')
@section('current_url',route('burden.create'))
@section('content')
    <div class="app-wrapper">
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">TODAY BURDENS GRADE</h5>
            </div><!-- /.app-main-header -->
            <div class="app-main-content">
                <div class="projects-list" id="container2">
                    {{ Form::open(array('route' => ['eburden.grade',$burden->id],'enctype'=>'multipart/form-data' ,'id'=>'bootstrap-wizard-form'))}}
                    <div class="media-list">
                    <input type="hidden" name="weightEndProduct" id="weightEndProduct" value="{{$weight}}">
                        @foreach($grades as $grade)
                            <div class="media">
                                <div class="avatar avatar-circle avatar-md project-icon bg-success"
                                     data-plugin="firstLitter"
                                     data-target="12">{{$grade->name}}</div>
                                <div class="media-body">

                                    <h6 class="project-name" id="12">{{$grade->name}}</h6>
                                    <input type="hidden" name="grade[]" value="{{$grade->id}}">
                                </div>
                                {{--<div class="col-md-3">--}}
                                {{--<input type="checkbox" name="ck[]" class="one" data-plugin="switchery"--}}
                                {{--data-size="medium" value="{{$grade->id}}">--}}
                                {{--</div>--}}
                                <div class="col-sm-3">
                                  @if($grade->id == 3)
                                    <div class=" d-flex activity-counters justify-content-between">

                                        <div class="text-center px-2">
                                            <label for="single">Weight</label>
                                            <input class="form-control updateD input-grade" id="weight-{{$grade->id}}"
                                                   name="weight"
                                                   value="0" placeholder="" type="text" style="width:100px" readonly>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                <div class="col-sm-3" id="aa{{$grade->id}}">
                                    <div class=" d-flex activity-counters justify-content-between">

                                        <div class="text-center px-2">
                                            <label for="single">Quantity</label>
                                            <input class="form-control updateD input-grade" id="qty-{{$grade->id}}"
                                                   name="quantity[]"
                                                   value="0" required type="text" style="width:100px"
                                                   @if($reusable_type == 1 && $grade->id == 3)
                                                   readonly
                                                   @endif
                                                   >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <input type="hidden" id="expectedQty" value="{{$burden->expected_end_product_qty}}">
                    </div>
                </div>
                <button type="submit" class="btn btn-success btn-lg btn-block submit"> Grade Now</button>
                <!--button type="submit" class="btn mt-3 btn-success btn-lg btn-block">
                    Grade Now
                </button-->

                {!!Form::close()!!}
            </div>
        </div>
    </div>


@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/js/jquery.sortable.js')}}"></script>
    <script src="{{asset('assets/examples/js/demos/form.wizard.js')}}"></script>

    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>

    <script>
        $(document).ready(function () {
            var expected = jQuery("#expectedQty").val();
            $('#qty-4').val(expected);
        });
        $(".updateD").keyup(function () {
            var expected = jQuery("#expectedQty").val();

            var gradeA = jQuery("#qty-1").val();
            var gradeB = jQuery("#qty-2").val();
            var gradeC = jQuery("#qty-3").val();
            var gradeD = jQuery("#qty-4").val();

            gradeD = parseFloat(expected) - (parseFloat(gradeA) + parseFloat(gradeB) + parseFloat(gradeC));

            $('#qty-4').val(gradeD);

        });
        $("#qty-3").keyup(function(){
            var weightEndProduct = jQuery("#weightEndProduct").val();
            var qty = jQuery("#qty-3").val();
            if(qty == '')
            {
              $('#weight-3').val(0);
            }else{
              var multipleValue = parseFloat(weightEndProduct)*parseFloat(qty);
              $('#weight-3').val(multipleValue);
            }

        });


        $(".submit").click(function (e) {
            e.preventDefault();
            var gradeA = jQuery("#qty-1").val();
            var gradeB = jQuery("#qty-2").val();
            var gradeC = jQuery("#qty-3").val();

            if(gradeA == 0 && gradeB == 0 && gradeC == 0) {
                alert("Contact Admin");
            }else{
                $("#bootstrap-wizard-form").submit();
            }
        });
    </script>
@stop
