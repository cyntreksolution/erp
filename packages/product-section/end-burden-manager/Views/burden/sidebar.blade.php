<div class="profile-section-user">
    <div class="profile-cover-img profile-pic">
        <img src="{{asset('assets\img\semi2.jpg')}}" alt="">
    </div>
    <div class="profile-info-brief p-3">
        @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
        <div class="user-profile-avatar avatar avatar-circle avatar-md project-iconr bg-{{$color[$burden->colour]}}"
             data-plugin="firstLitter"
             data-target="#{{$burden->id*754}}"></div>

        <div class="text-center">
            <h5 class="text-uppercase ">{{$burden->serial}}</h5>
            <h5 class="text-uppercase mb-1" id="{{$burden->id*754}}">{{$burden['semiProdcuts']->name}}</h5>
            <h6 class="text mb-lg-1">{{'Burden Size : '.$burden->burden_size}} KG</h6>
            <br>
        </div>

        <hr class="m-0">

        <div class="text-center py-3">
            {{'Active Recipe : '.$burden['recipe']->name}}<br>
            <hr class="m-1">
            {{'Expected Quantity : '.$burden->expected_semi_product_qty}} units<br>
            {{'Actual Quantity : '.$burden->actual_semi_product_qty}} units<br>
            <hr class="m-1">

            @if (!empty($burden->baker()->get()) && sizeof($burden->baker()->get())>0)
                @foreach($burden->baker()->get() as $baker)
                    {{'Baker : '.$baker->first_name.' '.$baker->lastt_name}}<br>
                @endforeach
            @endif
        </div>
    </div>
</div>