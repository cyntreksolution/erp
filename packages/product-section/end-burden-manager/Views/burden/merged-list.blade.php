@extends('layouts.back.master')@section('title','Sales Request | List')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/css/dataTables.checkboxes.css"
          rel="stylesheet"/>
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }
    </style>
@stop

@section('content')
    <div class="app-main-content mb-2">
        {{--        <div class="row mx-2">--}}
        {{--            <div class="col-md-2">--}}
        {{--                {!! Form::select('agent',$agents , null , ['class' => 'form-control','placeholder'=>'Select Agent','id'=>'agent']) !!}--}}
        {{--            </div>--}}
        {{--            <div class="col-md-2">--}}
        {{--                {!! Form::select('category',$categories , null , ['class' => 'form-control','placeholder'=>'Select Category','id'=>'category']) !!}--}}
        {{--            </div>--}}
        {{--            <div class="col-md-2">--}}
        {{--                {!! Form::select('day', ['Monday'=>'Monday','Tuesday'=>'Tuesday','Wednesday'=>'Wednesday','Thursday'=>'Thursday','Friday'=>'Friday','Saturday'=>'Saturday','Sunday'=>'Sunday','Long Weekend'=>'Long Weekend'] , null , ['class' => 'form-control','placeholder'=>'Select Day','id'=>'day']) !!}--}}
        {{--            </div>--}}
        {{--            <div class="col-md-2">--}}
        {{--                {!! Form::select('time', ['Morning'=>'Morning','Noon'=>'Noon','Evening'=>'Evening'] , null , ['class' => 'form-control','placeholder'=>'Select Time','id'=>'time']) !!}--}}
        {{--            </div>--}}
        {{--            <div class="col-md-2">--}}
        {{--                <input type="text" name="date" id="date" class="form-control" placeholder="Select Date">--}}
        {{--            </div>--}}
        {{--            <div class="col-md-2">--}}
        {{--                <button class="btn btn-info " onclick="process_form(this)">Filter</button>--}}
        {{--            </div>--}}
        {{--        </div>--}}
    </div>


    <div class="">
        <table id="invoice_table" class="display text-center">
            <thead>
            <tr>
                <th>Serial</th>
                <th>Date</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($merged as $merge)
                <tr>
                    <td>{{$merge->serial}}</td>
                    <td>{{$merge->created_at}}</td>
                    <td>
                        <button class="btn btn-sm btn-primary"><i class="fa fa-eye"></i></button>
                        <button class="btn btn-sm btn-dark"><i class="fa fa-file-pdf-o"></i></button>
                    </td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th>Serial</th>
                <th>Date</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>

    </div>
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
    <script type="text/javascript"
            src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js"></script>



    <script>
        $(document).ready(function () {
            table = $('#invoice_table').DataTable({
                searching: true,
                dom: 'Bfrtip',
                buttons: [
                    {
                        text: '',
                        className: 'btn btn-default icon-md icon-trash datatable-btn',
                        action: function (e, dt, node, config) {

                        }
                    }
                ],
                pageLength: 25,
                responsive: true
            });
        });

    </script>

@stop
