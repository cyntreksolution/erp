<?php

namespace PackingItemManager;

use Illuminate\Support\ServiceProvider;

class PackingItemServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/Views', 'PackingItemManager');
        require __DIR__ . '/web.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('PackingItemManager', function($app){
            return new PackingItemManager;
        });
    }
}
