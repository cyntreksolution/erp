<?php

namespace PackingItemManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PackingItem extends Model
{
    use SoftDeletes;

    protected $table = 'packing_item';

    protected $dates = ['deleted_at'];

    public function endProduct(){
      return $this->belongsToMany('EndProductManager\Models\EndProduct');
    }
}
