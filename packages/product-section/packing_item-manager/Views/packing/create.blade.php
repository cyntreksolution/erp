@extends('layouts.back.master')@section('title','Packing')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/demos.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.validation.css')}}">
    <link rel="stylesheet" href="{{asset('assets/packages/packing_item/packing_item.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">


@stop
@section('content')
    <div class="app-wrapper">
        <div class="app-panel" id="app-panel">
            <div class="app-search"><input type="search" class="search-field" placeholder="Search"> <i
                        class="search-icon fa fa-search"></i>
            </div><!-- /.app-search -->
            <div class="app-panel-inner">
                <div class="">
                    <div class="p-3">
                        <button class="btn btn-success py-3 btn-block btn-lg" data-toggle="modal"
                                data-target="#projects-task-modal">NEW PACKING ITEM
                        </button>
                    </div>
                    <hr class="m-0">
                    <div class="projects-list">
                        {{--@foreach($stocks as $stock)--}}
                        {{--<a href="{{$stock->stock_id}}">--}}
                        {{--<div class="media">--}}
                        {{--@php $color=array("blue","danger","purple","yellow","success","info","red") @endphp--}}
                        {{--<div class="avatar avatar-circle avatar-md project-icon" data-plugin="firstLitter"--}}
                        {{--data-target="#{{$stock->stock_id*754}}"></div>--}}
                        {{--<div class="media-body">--}}
                        {{--<h6 class="project-name" id="{{$stock->stock_id*754}}">{{$stock->name}}</h6>--}}
                        {{--<small class="project-detail">{{$stock->address}}</small>--}}
                        {{--</div>--}}

                        {{--</div>--}}
                        {{--</a>--}}
                        {{--@endforeach--}}
                    </div>
                </div>
            </div>
            <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show"><i
                        class="fa fa-chevron-right"></i>
                <i class="fa fa-chevron-left"></i>
            </a>
        </div>

        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">PACKING ITEMS</h5>
            </div>
            <div class="app-main-content" id="container">
                <div class="content ">
                    <div class="row">
                    @foreach($packs as $pack)
                        <div class=" media-list col-md-12">
                            <div class="media">
                                 @if(isset($pack->image))
                                    <a href="{{$pack->id}}" class="avatar avatar avatar-sm">
                                        <img src="{{asset($pack->image_path.$pack->image)}}" alt="">
                                    </a>
                                @else
                                    @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                    <a href="{{$pack->id}}" class="avatar avatar-circle avatar-sm bg-{{$color[$pack->colour]}} "
                                       data-plugin="firstLitter"
                                       data-target="#raw-{{$pack->id*874}}">
                                    </a>
                                @endif
                                <div class="media-body">
                                    <h6 class="media-heading my-1">
                                        <a href="{{$pack->id}}" id="raw-{{$pack->id*874}}" >{{$pack->name}}</a>
                                    </h6>
                                    <small>{{$pack->desc}}</small>
                                </div>
                                <div class="section-2">
                                    <button class="delete btn btn-light btn-sm ml-2" value="{{$pack->id}}"
                                            data-toggle="tooltip"
                                            data-placement="left" title="DELETE SPACKING ITEM">
                                        <i class="zmdi zmdi-close"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="projects-task-modal" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'packing','enctype'=>'multipart/form-data', 'id'=>'jq-validation-example-1'))}}
                {!!Form::token()!!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div><!-- /.modal-header -->
                <div class="modal-body">
                    <div class="task-name-wrap">
                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                        <input class=" task-name-field" type="text" name="name" placeholder="Name">
                    </div>
                    <hr>
                    <div class="task-name-wrap">
                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                        <input class="task-name-field" type="text" name="buffer_stock" placeholder="Buffer Stock">
                    </div>
                    <hr>
                    <div class="task-desc-wrap">
                        <textarea class="task-desc-field" name="desc"
                                  placeholder="Description"></textarea>
                    </div>
                    <hr>
                    <div class="task-name-wrap">
                        <span><i class="zmdi zmdi-check"></i></span>
                        <input class="task-name-field" type="file" name="image" placeholder="Material Name">
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {!!Form::close()!!}
            </div>
        </div>
    </div>
@stop
@section('js')

    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/packages/packing_item/packing_item.js')}}"></script>

@stop