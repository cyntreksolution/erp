<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web','log'])->group(function () {
    Route::prefix('packing')->group(function () {
        Route::get('', 'PackingItemManager\Controllers\PackingItemController@index')->name('packing.index');

        Route::get('create', 'PackingItemManager\Controllers\PackingItemController@create')->name('packing.create');

        Route::get('{packing}', 'PackingItemManager\Controllers\PackingItemController@show')->name('packing.show');

        Route::get('{packing}/edit', 'PackingItemManager\Controllers\PackingItemController@edit')->name('packing.edit');

        Route::post('', 'PackingItemManager\Controllers\PackingItemController@store')->name('packing.store');

        Route::get('get/packingitem', 'PackingItemManager\Controllers\PackingItemController@getpackingitem')->name('packing.store');

        Route::put('{packing}', 'PackingItemManager\Controllers\PackingItemController@update')->name('packing.update');

        Route::delete('{packing}', 'PackingItemManager\Controllers\PackingItemController@destroy')->name('packing.destroy');
    });
});
