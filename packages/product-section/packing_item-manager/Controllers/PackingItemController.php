<?php

namespace PackingItemManager\Controllers;

use Illuminate\Support\Facades\Auth;
use Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use PackingItemManager\Models\PackingItem;
use RawMaterialManager\Models\RawMaterial;
use RecipeManager\Models\Recipe;
use RecipeManager\Models\RecipeContent;
use SemiFinishProductManager\Models\SemiFinishProduct;

class PackingItemController extends Controller

{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect(route('packing.create'));
        return view('PackingItemManager::packing.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $packs=PackingItem::all();
        return view('PackingItemManager::packing.create')->with([
            'packs'=>$packs
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(isset($request->image)){
            $file = $request->file('image');
            $path = '/uploads/images/packing_items/';
            $destinationPath = storage_path($path);
            $file->move($destinationPath, $file->getClientOriginalName());

            $pack = new PackingItem();
            $pack->name = $request->name;
            $pack->desc = $request->desc;
            $pack->buffer_stock = $request->buffer_stock;
            $pack->image = $file->getClientOriginalName();
            $pack->image_path ='storage'.$path;
            $pack->colour = rand() % 7;
            $pack->created_by = Auth::user()->id;
            $pack->save();
            return redirect(route('packing.create'));
        } else {
            $pack = new PackingItem();
            $pack->name = $request->name;
            $pack->desc = $request->desc;
            $pack->buffer_stock = $request->buffer_stock;
            $pack->colour = rand() % 7;
            $pack->created_by = Auth::user()->id;
            $pack->save();
            return redirect(route('packing.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \PackingItemManager\Models\PackingItem $packing
     * @return \Illuminate\Http\Response
     */
    public function show(PackingItem $packing)
    {
        return view('PackingItemManager::packing.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \PackingItemManager\Models\PackingItem $packing
     * @return \Illuminate\Http\Response
     */
    public function edit(PackingItem $packing)
    {
        return view('PackingItemManager::packing.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \PackingItemManager\Models\PackingItem $packing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PackingItem $packing)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \PackingItemManager\Models\PackingItem $packing
     * @return \Illuminate\Http\Response
     */
    public function destroy(PackingItem $packing)
    {
        $packing->delete();
        if ($packing->trashed()) {
            return 'true';
        }else{
            return 'false';
        }
    }

    public function getpackingitem(Request $request){
        return $pack=PackingItem::find($request->id);

    }
}
