<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web'])->group(function () {
    Route::prefix('burden')->group(function () {
        Route::get('list', 'BurdenManager\Controllers\BurdenController@burdenListShow')->name('burden.list');
        Route::get('table/data', 'BurdenManager\Controllers\BurdenController@tableData')->name('burden.table');
        Route::post('excel/data', 'BurdenManager\Controllers\BurdenController@exportFilter')->name('burden.excel');


        Route::get('', 'BurdenManager\Controllers\BurdenController@index')->name('burden.index');
        Route::get('bake/ready/list', 'BurdenManager\Controllers\BurdenController@bakeList')->name('bake.list');

        Route::get('create', 'BurdenManager\Controllers\BurdenController@create')->name('burden.create');

        Route::get('date', 'BurdenManager\Controllers\BurdenController@dateReport')->name('burden.date');
        Route::post('date', 'BurdenManager\Controllers\BurdenController@getReport')->name('burden.date');
        Route::post('analyse', 'BurdenManager\Controllers\BurdenController@analyse')->name('burden.analyse');

        Route::get('view/history', 'BurdenManager\Controllers\BurdenController@history')->name('burden.history');

        Route::get('view/history/{burden}', 'BurdenManager\Controllers\BurdenController@historyView')->name('burden.historyView');

        Route::get('{burden}', 'BurdenManager\Controllers\BurdenController@show')->name('burden.show');
        Route::get('/print/{burden}', 'BurdenManager\Controllers\BurdenController@printList')->name('burden.print');

        Route::get('{burden}/edit', 'BurdenManager\Controllers\BurdenController@edit')->name('burden.edit');

        Route::get('{burden}/bake', 'BurdenManager\Controllers\BurdenController@bake')->name('burden.bake');

        Route::post('update_stage1','BurdenManager\Controllers\BurdenController@sstage')->name('upToSecond.burden');
        Route::post('update_stage2','BurdenManager\Controllers\BurdenController@fstage')->name('upToFirst.burden');


        Route::post('', 'BurdenManager\Controllers\BurdenController@store')->name('burden.store');

        Route::post('grade/save', 'BurdenManager\Controllers\BurdenController@grade')->name('burden.grade');

        Route::get('save/qty', 'BurdenManager\Controllers\BurdenController@saveqty')->name('burden.grade');

        Route::post('{burden}', 'BurdenManager\Controllers\BurdenController@update')->name('burden.update');
        Route::post('bake/{burden}', 'BurdenManager\Controllers\BurdenController@baekNow')->name('burden.bakenow');

        Route::post('bake/{burden}/change/baker', 'BurdenManager\Controllers\BurdenController@changeBaker')->name('burden.changeb');
        Route::post('bake/{burden}/change/group', 'BurdenManager\Controllers\BurdenController@changeGroup')->name('burden.changeEG');

        Route::get('get/recipe', 'BurdenManager\Controllers\BurdenController@semiFinishProductRecipeAndCount')->name('burden.updatex');

        Route::delete('{burden}', 'BurdenManager\Controllers\BurdenController@destroy')->name('burden.destroy');
        Route::post('delete/burden', 'BurdenManager\Controllers\BurdenController@deletePendingBurden')->name('burden.destroy');
    });

    Route::prefix('burden/grade')->group(function () {
        Route::get('index', 'BurdenManager\Controllers\GradeController@index')->name('burden_grade.index');

        Route::get('create', 'BurdenManager\Controllers\GradeController@create')->name('burden_grade.create');

        Route::get('{burden/grade}', 'BurdenManager\Controllers\GradeController@show')->name('burden_grade.show');

        Route::get('{burden}/edit', 'BurdenManager\Controllers\GradeController@edit')->name('burden_grade.edit');

        Route::get('getqty', 'BurdenManager\Controllers\GradeController@getqty')->name('burden_grade.edit');


        Route::post('', 'BurdenManager\Controllers\GradeController@store')->name('burden_grade.store');

        Route::post('{burdengrade}', 'BurdenManager\Controllers\GradeController@update')->name('burden_grade.update');

        Route::delete('{burden/grade}', 'BurdenManager\Controllers\GradeController@destroy')->name('burden_grade.destroy');
    });
});
