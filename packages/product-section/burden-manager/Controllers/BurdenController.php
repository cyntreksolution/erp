<?php

namespace BurdenManager\Controllers;

use App\Exports\BurdenExport;
use Barryvdh\DomPDF\Facade as PDF;
use BurdenManager\Classes\BurdenContent;
use BurdenManager\Models\AllocateSemiFinish;
use BurdenManager\Models\Burden;
use BurdenManager\Models\BurdenEmployee;
use BurdenManager\Models\BurdenGrade;
use BurdenManager\Models\BurdenGroupEmployee;
use Carbon\Carbon;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use EmployeeManager\Models\Group;
use EmployeeManager\Models\GroupEmployee;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use RawMaterialManager\Models\RawMaterial;
use RecipeManager\Classes\RecipeContent;
use RecipeManager\Models\Recipe;
use EmployeeManager\Models\Employee;
use GradeManager\Models\Grade;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use RecipeManager\Models\RecipeSemiProduct;
use SemiFinishProductManager\Models\SemiFinishProduct;

class BurdenController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:burden-index', ['only' => ['burdenListShow','index','create','tableData']]);
        $this->middleware('permission:BurdenGrade-list', ['only' => ['list']]);
        $this->middleware('permission:burden-history', ['only' => ['history']]);
        $this->middleware('permission:burden-deletePendingBurden', ['only' => ['deletePendingBurden']]);
        $this->middleware('permission:burden-printList', ['only' => ['printList']]);

    }
    public function index()
    {
        return redirect(route('burden.create'));
    }

    public function create()
    {
       /* $semiAll = SemiFinishProduct::whereIsBake(2)->get();
        $products = RawMaterial::all();
        $recipes = Recipe::take(4)->get();
        $semi = DB::table('recipe_semi_finish_product')
            ->join('semi_finish_product', 'recipe_semi_finish_product.semi_finish_product_semi_finish_product_id', '=', 'semi_finish_product.semi_finish_product_id')
            ->where('recipe_semi_finish_product.status', '=', 2)
            ->where('semi_finish_product.is_bake', '=', 2)
            ->get();
        $grade = Grade::all();
        $burden = CookingRequest::where('status', '=', 1)
            ->with('semiProdcuts')
            ->get();

        $burdenStockRecieved = CookingRequest::where('status', '=', 2)
            ->with('semiProdcuts')
            ->get();

        $creatingBurdens = CookingRequest::where('status', '=', 3)
            ->with('semiProdcuts')
            ->get();


        $bakingBurdens = CookingRequest::where('status', '=', 4)
            ->with('semiProdcuts')
            ->get();

        $finishBurdens = CookingRequest::where('status', '=', 5)
            ->with('semiProdcuts')
            ->get();

        return view('ShortEatsManager::burden.create')->with([
            'semiProducts' => $semi,
            'recievedBurdens' => $burdenStockRecieved,
            'bakingBurdens' => $bakingBurdens,
            'finishBurdens' => $finishBurdens,
            'creatingBurdens' => $creatingBurdens,
            'burdens' => $burden,
            'grades' => $grade,
            'semi' => $semiAll,
            'recipes' => $recipes,
            'products' => $products,
        ]);*/

        //********************************
        $semiAll = SemiFinishProduct::all();
        $products = RawMaterial::all();
        $recipes = Recipe::take(4)->get();
        $semi = DB::table('recipe_semi_finish_product')
            ->join('semi_finish_product', 'recipe_semi_finish_product.semi_finish_product_semi_finish_product_id', '=', 'semi_finish_product.semi_finish_product_id')
            ->where('recipe_semi_finish_product.status', '=', 2)
            ->where('semi_finish_product.is_bake', '=', 1)
            ->get();
        $grade = Grade::all();



            $burden = Burden::where('status', '=', 1)
                ->with('semiProdcuts')
                ->get();

            $burdenStockRecieved = Burden::where('status', '=', 2)
                ->with('semiProdcuts')
                ->get();

            $creatingBurdens = Burden::where('status', '=', 3)
                ->with('semiProdcuts')
                ->get();


            $bakingBurdens = Burden::where('status', '=', 4)
                ->with('semiProdcuts')
                ->get();

            $finishBurdens = Burden::where('status', '=', 5)
                ->with('semiProdcuts')
                ->get();



        return view('BurdenManager::burden.create')->with([
            'semiProducts' => $semi,
            'recievedBurdens' => $burdenStockRecieved,
            'bakingBurdens' => $bakingBurdens,
            'finishBurdens' => $finishBurdens,
            'creatingBurdens' => $creatingBurdens,
            'burdens' => $burden,
            'grades' => $grade,
            'semi' => $semiAll,
            'recipes' => $recipes,
            'products' => $products,
        ]);

    }

    public function history()
    {

        $burden = Burden::whereStatus(6)->whereDate('updated_at', '=', Carbon::today())
            ->with('semiProdcuts')
            ->orderBy('created_at', 'desc')
            ->get();

        $burden_yes = Burden::whereStatus(6)->whereDate('updated_at', '=', Carbon::yesterday())
            ->with('semiProdcuts')
            ->orderBy('created_at', 'desc')
            ->get();

        return view('BurdenManager::burden.history')->with([
            'burdens' => $burden,
            'burdens_yes' => $burden_yes
        ]);


    }

    public function getBakers($baker)
    {
        return Group::where('id', '=', $baker)->get();
    }

    public function fstage(Request $request)
    {
        $id = $request->burden_id;
        $data = Burden::find($id);
        $data->responce = 0;
        $data->save();
        return redirect()->back();
    }

    public function sstage(Request $request)
    {

        $id = $request->burden_id;
        $data = Burden::find($id);
        $data->responce = 1;
        $data->save();
        return redirect()->back();
    }


    public function historyView(Burden $burden)
    {
        $burden = Burden::where('id', '=', $burden->id)
            ->with('semiProdcuts', 'recipe.recipeContents.materials', 'employeeGroup', 'employees.employee', 'grade.grade')
            ->first();


        if (!empty($burden->employee_group)) {
            $employee_group = GroupEmployee::where('group_id', $burden->employee_group)->first();

            $date_string = Carbon::parse($employee_group->created_at)->format('Y-m-d') . '%';
            $date_string_2 = Carbon::parse($burden->created_at)->format('Y-m-d') . '%';
            $employee_groups = Group::where('created_at', 'like', $date_string)->orWhere('created_at', 'like', $date_string_2)->get();
            $bakers = Group::where('created_at', 'like', $date_string)->orWhere('created_at', 'like', $date_string_2)->get();
        } else {
            $date_string = Carbon::parse($burden->created_at)->format('Y-m-d') . '%';
            $employee_groups = Group::where('created_at', 'like', $date_string)->get();
            $bakers = Group::where('created_at', 'like', $date_string)->get();
        }
        // $bakers = Employee::pluck('full_name','id');


        return view('BurdenManager::burden.historyView', compact('bakers', 'employee_groups'))->with([
            'burden' => $burden,
            'burdenClass' => new BurdenController
        ]);
    }

    public function store(Request $request)
    {

        $semi_finish_product = $request->semi_finish_product;
        $active_recipe = RecipeContent::getActiveRecipe($semi_finish_product)->recipe_id;
        $burden_size = $request->burden_size;
        $burden_count = $request->burden_count;
        $expected_semi_qty = RecipeContent::getExpectedQty($semi_finish_product) * $burden_size;

        for ($i = 0; $i < $burden_count; $i++) {
            $in = DB::select("show table status like 'burden'");


            $burdenCount = Burden::whereDate('created_at', '=', Carbon::today()->toDateString())->get()->count();
            $last_digit = $burdenCount + 1;
            $serial = date('ymd') . '-' . $last_digit;
            $burden = new Burden();
            $burden->semi_finish_product_id = $semi_finish_product;

            $ft = SemiFinishProduct::where('semi_finish_product_id', '=', $burden->semi_finish_product_id)->first();

            $nft = $ft->is_fractional;

            $burden->serial = $serial;
            $burden->burden_size = $burden_size;

            if ($nft == 1) {

                $burden->expected_semi_product_qty = round($expected_semi_qty, 3);
            } else {
                $burden->expected_semi_product_qty = round($expected_semi_qty);
            }

            $burden->recipe_id = $active_recipe;
            $burden->created_by = Auth::user()->id;
            $burden->save();
        }
        // return redirect(route('burden.index'))->with([
        return redirect(route('proOrder.index'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => 'new burden created successfully'
        ]);
    }

    public function show(Burden $burden)
    {
        $burden = Burden::where('id', '=', $burden->id)
            ->with('semiProdcuts', 'recipe.recipeContents.materials')
            ->first();
        $date = Carbon::now();
        $teams = GroupEmployee::whereDate('created_at', $date)->get();

        return view('BurdenManager::burden.show', compact('teams'))->with([
            'burden' => $burden
        ]);
    }

    public function edit(Burden $burden)
    {
        $date = Carbon::now();
        $teams = Group::whereStatus(1)->whereDate('created_at', '=', Carbon::today()->toDateString())->get();
        return view('BurdenManager::burden.edit', compact('teams'))->with([
            'burden' => $burden,
        ]);
    }

    public function bake(Burden $burden)
    {
        $employers = Employee::all();
        return view('BurdenManager::burden.bake')->with([
            'burden' => $burden,
            'employers' => $employers,
        ]);
    }

    public function update(Request $request, Burden $burden)
    {
        $employee = $request->emp_group;
        try {
            DB::transaction(function () use ($employee, $burden) {
                $burdenEmp = new BurdenGroupEmployee();
                $burdenEmp->burden_id = $burden->id;
                $burdenEmp->group_id = $employee;
                $burdenEmp->save();

                $burden->status = 3;
                $burden->save();
            });

            return redirect(route('burden.index'))->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'dasfs'
            ]);
        } catch (Exception $e) {
            return $e;
            return redirect(route('burden.index'))->with([
                'error' => true,
                'error.title' => 'Sorry !',
                'error.message' => 'Something Went Wrong '
            ]);
        }
    }

    public function baekNow(Request $request, Burden $burden)
    {
        $employee = $request->employee;
        try {
            DB::transaction(function () use ($employee, $burden) {
                $burden->baker = $employee;
                $burden->status = 4;
                $burden->save();
            });

            return redirect(route('burden.index'))->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'baking started'
            ]);
        } catch (Exception $e) {
            return $e;
            return redirect(route('burden.index'))->with([
                'error' => true,
                'error.title' => 'Sorry !',
                'error.message' => 'Something Went Wrong '
            ]);
        }
    }



    public function grade(Request $request)
    {
//        $burden_id = $request->burden_id;
//        $available_qty = $request->avalableQTY;
//        foreach ($request->grade as $key => $gr) {
//            $grade = $gr;
//            $gradeQty = $request->gradeQty[$key];
//            $burdebnGrade = new BurdenGrade();
//            $burdebnGrade->burden_id = $burden_id;
//            $burdebnGrade->grade_id = $grade;
//            $burdebnGrade->burden_qty = $gradeQty;
//            $burdebnGrade->save();
//        }
//        $bd = Burden::find($burden_id);
//        $bd->status = 4;
////        $bd->actual_qty = $available_qty;
//        $bd->save();
//
//        foreach ($request->grade as $key => $gr) {
//            $grade = $gr;
//            $gradeQty = $request->gradeQty[$key];
//            $stockTransaction = new StockSemiFinish();
//            $stockTransaction->stock_id = 2;
//            $stockTransaction->semi_finish_product_id = $bd->semi_finish_product_id;
//            $stockTransaction->grade_id = $grade;
//            $stockTransaction->qty = $gradeQty;
//            $stockTransaction->save();
//        }
//
//
//        return redirect(route('burden.index'))->with([
//            'success' => true,
//            'success.title' => 'Congratulations !',
//            'success.message' => 'Garde Added',
//        ]);
    }

    public function semiFinishProductRecipeAndCount(Request $request)
    {
        $semi_finish_product = $request->id;
        $res = RecipeSemiProduct::where('semi_finish_product_semi_finish_product_id', '=', $semi_finish_product)->
        where('status', '=', 2)
            ->first();
        $semi = SemiFinishProduct::find($semi_finish_product);

        $req = [
            'recipe' => $res->recipe_id,
            'units_per_recipe' => $res->units,
            'name' => $semi->name,
        ];
        return $req;
    }

    public function saveqty(Request $request)
    {
        $burden = Burden::find($request->burden_id);
        $burden->actual_semi_product_qty = $request->qty;
        $burden->status = 5;
        $burden->save();
        return 'true';
    }

    public function dateReport()
    {

        $semi = SemiFinishProduct::all();
        return view('BurdenManager::burden.date')->with([
            'semiproducts' => $semi,
        ]);


    }

    public function getReport(Request $request)
    {

        $start_date = substr($request->date, 0, 10);
        $start_date = date_create($start_date);
        $start_date = date_format($start_date, "Y-m-d");

        $end_date = substr($request->date, 12, 23);
        $end_date = date_create($end_date);
        $end_date = date_format($end_date, "Y-m-d");

        $product = $request->product;
        if ($product == 0) {
            $burdens = Burden::whereBetween('created_at', array($start_date, $end_date))->with('semiProdcuts')->get();
            $array = [];
            foreach ($burdens as $burden) {
                $burdenx = [
                    'id' => $burden['id'],
                    'burden_size' => $burden['burden_size'],
                    'expected_semi_product_qty' => $burden['expected_semi_product_qty'],
                    'name' => $burden['semiProdcuts']->name,
                ];
                array_push($array, $burdenx);
            }
            return $array;

        } else {
            $burdens = Burden::whereBetween('created_at', array($start_date, $end_date))->with('semiProdcuts')
                ->where('semi_finish_product_id', '=', $product)->get();
            $array = [];
            foreach ($burdens as $burden) {
                $burdenx = [
                    'id' => $burden['id'],
                    'burden_size' => $burden['burden_size'],
                    'expected_semi_product_qty' => $burden['expected_semi_product_qty'],
                    'name' => $burden['semiProdcuts']->name,
                ];
                array_push($array, $burdenx);
            }
            return $array;
        }
    }

    public function analyse(Request $request)
    {
//        return $request;
        $start_date = substr($request->date, 0, 10);
        $start_date = date_create($start_date);
        $start_date = date_format($start_date, "Y-m-d");

        $end_date = substr($request->date, 12, 23);
        $end_date = date_create($end_date);
        $end_date = date_format($end_date, "Y-m-d");

        $product = $request->Single;

        $dates = $this->generateDateRange(Carbon::parse($start_date), Carbon::parse($end_date));
        $burdensProducts = Burden::whereBetween('created_at', array($start_date, $end_date))->with('semiProdcuts')->groupBy('semi_finish_product_id')->get();
        $burdensProducts = Burden::whereBetween('created_at', array($start_date, $end_date))->selectRaw('*, sum(burden_size) as sum')->with('semiProdcuts')->groupBy('semi_finish_product_id')->get();

        $productNames = [];
        foreach ($burdensProducts as $key => $burdensProduct) {
            foreach ($dates as $key => $date) {
                $burdenCount = Burden::where('semi_finish_product_id', $burdensProduct->semi_finish_product_id)->whereDate('created_at', $date)->get()->count();
                $dte[$key] = $burdenCount;
            }
            $name = [
                'name' => $burdensProduct['semiProdcuts']->name,
                'date' => $dte,
                'sum' => $burdensProduct->sum
            ];
            array_push($productNames, $name);
        }

        if ($product == 0) {
            return view('BurdenManager::burden.analyse')->with([
                'dates' => $dates,
                'productNames' => $productNames,
            ]);
        } else {
            return view('BurdenManager::burden.analyseProduct');
        }

    }

    private function generateDateRange(Carbon $start_date, Carbon $end_date)
    {
        $dates = [];

        for ($date = $start_date; $date->lte($end_date); $date->addDay()) {
            $dates[] = $date->format('Y-m-d');
        }

        return $dates;
    }

    public function bakeList(Request $request)
    {
        $burdens = Burden::whereStatus(3)->get();
        return view('BurdenManager::burden.toBake', compact('burdens'));
    }

    public function deletePendingBurden(Request $request)
    {
        $id = $request->burden_id;
        $burden = Burden::whereId($id)->whereStatus(1)->first();
        $burden->delete();
        return 'true';
    }

    public function destroy(Burden $burden)
    {
        $burden->delete();
        if ($burden->trashed()) {
            return true;
        } else {
            return false;
        }
    }

    public function burdenListShow(Request $request)
    {
        $semiFinish = SemiFinishProduct::whereIsBake(1)->get()->pluck('name', 'semi_finish_product_id');
        return view('BurdenManager::burden.list', compact('semiFinish'));
    }

    public function tableData(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'serial', 'semi_finish_product_id', 'status', 'burden_size', 'recipe_id', 'created_at'];
        $order_column = $columns[$order_by[0]['column']];

        $stat_data = Burden::query();
        $burdens = Burden::tableData($order_column, $order_by_str, $start, $length);

        $burdenCount = $burdens->count();
        $filterCount = 0;
        $stat_a = 0;
        $stat_b = 0;
        $stat_c = 0;
        $stat_d = 0;

        $teambn = null;

        if ($request->filter == true) {
            $semi = $request->semi;
            $status = $request->status;
            $date_range = $request->date_range;
            $date_type = $request->date_type;

            $burdens = Burden::tableData($order_column, $order_by_str, $start, $length)->filterData($semi, $status, $date_range,$date_type)->get();
            $filterCount = Burden::filterData($semi, $status, $date_range,$date_type)->count();


            foreach ($stat_data->filterData($semi, $status, $date_range)->get() as $stat) {
                if ($stat->status == 6) {
                    $grades = $stat->grade;
                    foreach ($grades as $key => $grade) {
                        switch ($grade['grade_id']) {
                            case  1:
                                $stat_a = $stat_a + $grade['burden_qty'];
                                break;
                            case 2:
                                $stat_b = $stat_b + $grade['burden_qty'];
                                break;
                            case  3:
                                $stat_c = $stat_c + $grade['burden_qty'];
                                break;
                            case  4:
                                $stat_d = $stat_d + $grade['burden_qty'];
                                break;
                        }
                    }
                }
            }
            $stat_data = $stat_data->filterData($semi, $status, $date_range)->select(DB::raw('SUM(burden_size) as burden_size'))->first();

        } elseif (is_null($search) || empty($search)) {

            foreach ($stat_data->where('created_at', '=', Carbon::today())->get() as $stat) {
                if ($stat->status == 6) {
                    $grades = $stat->grade;
                    foreach ($grades as $key => $grade) {
                        switch ($grade['grade_id']) {
                            case  1:
                                $stat_a = $stat_a + $grade['burden_qty'];
                                break;
                            case 2:
                                $stat_b = $stat_b + $grade['burden_qty'];
                                break;
                            case  3:
                                $stat_c = $stat_c + $grade['burden_qty'];
                                break;
                            case  4:
                                $stat_d = $stat_d + $grade['burden_qty'];
                                break;
                        }
                    }
                }
            }

            $stat_data = $stat_data->where('created_at', '=', Carbon::today())->select(DB::raw('SUM(burden_size) as burden_size'))->first();


            $burdens = $burdens->where('created_at', '=', Carbon::today())->get();
            $burdenCount = Burden::where('created_at', '=', Carbon::today())->count();
        } else {
            $burdens = $burdens->searchData($search)->get();
            $burdenCount = $burdens->count();
        }

        $data = [];
        $i = 0;

        $stat_burden_size = 0;


        foreach ($burdens as $key => $burden) {

            if($date_type == 2) {

                    $groups = Group::find($burden->employee_group);
                $teambn = $serial = "<a target='_blank' href='#'>$groups->name</a>";
            }elseif ($date_type == 3){

                    $groups = Group::find($burden->baker);
                $teambn = $serial = "<a target='_blank' href='#'>$groups->name</a>";

            }
            $btnView = null;
            $btnDelete = null;
            $grades = null;
            $u = "view/history/$burden->id";
            $serial = "<a target='_blank' href='$u'>$burden->serial</a>";

            if (Sentinel::check()->roles[0]->slug == 'owner' ) {


                $btnView = '<a class="btn btn-outline-info btn-sm mr-1" target="_blank" href="' . route('burden.show', $burden->id) . '"> <i class="fa fa-eye"></i> </a>';
                if ($status == 1) {

                    $btnDelete = ($burden->status == 1) ? '<button class="btn btn-outline-danger btn-sm mr-1" onclick="deleteBurden(' . $burden->id . ')"> <i class="fa fa-trash"></i> </button>' : null;

                }
            }

            switch ($burden->status) {
                case 1:
                    $statusBtn = '<button class="btn btn-outline-warning btn-sm mr-1">pending</button>';
                    break;

                case 2:
                    $statusBtn = '<button class="btn btn-outline-info btn-sm mr-1">received</button>';
                    break;

                case 3:
                    $statusBtn = '<button class="btn btn-outline-primary btn-sm mr-1">creating</button>';
                    break;
                case 4:
                    $statusBtn = '<button class="btn btn-outline-danger btn-sm mr-1">burning</button>';
                    break;
                case 5:
                    $statusBtn = '<button class="btn btn-outline-success btn-sm mr-1">grade</button>';
                    break;
                case 6:
                    $statusBtn = '<button class="btn btn-outline-dark btn-sm mr-1">finished</button>';
                    break;
            }

            $gradeA = 0;
            $gradeB = 0;
            $gradeC = 0;
            $gradeD = 0;

            if ($burden->status == 6) {
                $grades = $burden->grade;
                foreach ($grades as $key => $grade) {
                    switch ($grade['grade_id']) {
                        case  1:
                            $gradeA = $grade['burden_qty'];
                            break;
                        case 2:
                            $gradeB = $grade['burden_qty'];
                            break;
                        case  3:
                            $gradeC = $grade['burden_qty'];
                            break;
                        case  4:
                            $gradeD = $grade['burden_qty'];
                            break;
                    }
                }
            }


            $stat_burden_size = $stat_data->burden_size;

            $data[$i] = array(
                $serial,
                $burden->semiProdcuts->name,
                $burden->burden_size,
                Carbon::parse($burden->created_at)->format('Y-m-d H:m:s'),
                $gradeA,
                $gradeB,
                $gradeC,
                $gradeC * $burden->semiProdcuts->weight,
                $gradeD,
                $teambn . $btnView . $btnDelete
            );
            $i++;
        }


        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($burdenCount),
            "recordsFiltered" => intval($filterCount),
            "data" => $data,
            "stat_burden_size" => number_format($stat_burden_size, 3),
            "stat_a" => number_format($stat_a, 3),
            "stat_b" => number_format($stat_b, 3),
            "stat_c" => number_format($stat_c, 3),
            "stat_d" => number_format($stat_d, 3),
        ];

        return json_encode($json_data);
    }

    public function exportFilter(Request $request)
    {
        $semi = $request->semi;
        $status = $request->status;
        $date_range = $request->date_range;
        $records = Burden::filterData($semi, $status, $date_range)->get();
        return Excel::download(new BurdenExport($records), Carbon::now() . '_burden_report.xlsx');
    }


    public function changeBaker(Request $request, Burden $burden)
    {

        $burden->baker = $request->baker;
        $burden->save();
        return redirect()->back()->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => "Baker Updated !",
        ]);
    }

    public function changeGroup(Request $request, Burden $burden)
    {

        $old_data = BurdenGroupEmployee::where('burden_id', '=', $burden->id)->delete();

        $burden->employee_group = $request->employee_group;
        $burden->save();

        $burdenEmp = new BurdenGroupEmployee();
        $burdenEmp->burden_id = $burden->id;
        $burdenEmp->group_id = $request->employee_group;
        $burdenEmp->save();

        return redirect()->back()->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => "Employee Group Updated",
        ]);
    }


    public function printList(Request $request, Burden $burden)
    {
        $burden = Burden::where('id', '=', $burden->id)
            ->with('semiProdcuts', 'recipe.recipeContents.materials')
            ->first();
        $date = Carbon::now();
        $teams = GroupEmployee::whereDate('created_at', $date)->get();

        //$pdf = PDF::loadView('BurdenManager::burden.print', compact('teams', 'burden'));
        //$pdf->setPaper([0, 0, 220, 9999], 'portrait');
        //return $pdf->stream();

        return view('BurdenManager::burden.print', compact('teams'))->with([
            'burden' => $burden
        ]);
    }
}
