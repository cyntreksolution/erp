<?php

namespace BurdenManager\Controllers;

use App\ReusableStock;
use App\Traits\SemiFinishLog;
use BurdenManager\Models\Burden;
use BurdenManager\Models\BurdenGrade;
use GradeManager\Models\Grade;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use RawMaterialManager\Models\VirtualMaterial;
use SemiFinishProductManager\Models\SemiFinishProduct;
use StockManager\Classes\StockTransaction;
use BurdenManager\Models\ReusableProducts;


class GradeController extends Controller
{
    use SemiFinishLog;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $burdens = BurdenContent::getBurdenContent();
//        $employeee = Employee::with('designation')->get();
        return view('BurdenManager::grade.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        $production_order_details_id = 2;
//        $burden = new Burden();
//        return 'k';
        return view('BurdenManager::grade.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return 'd';
    }

    /**
     * Display the specified resource.
     *
     * @param \BurdenManager\Models\Grade $grade
     * @return \Illuminate\Http\Response
     */
    public function show(Grade $grade)
    {
        return view('BurdenManager::grade.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \BurdenManager\Models\Grade $grade
     * @return \Illuminate\Http\Response
     */
    public function edit(Burden $burden)
    {

        $weight = 0;
        $grade = Grade::all();
        if($burden->semiProdcuts->reusable_type > 1)
        {
            $weight = $burden->semiProdcuts->weight;
        }
        return view('BurdenManager::grade.edit')->with([
            'burden' => $burden,
            'grades' => $grade,
            'weight' => $weight,
            'reusable_type' => $burden->semiProdcuts->reusable_type,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \BurdenManager\Models\Grade $grade
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $burden = $request->burden_id;
        $qty = $request->quantity;
        $weight = $request->weight;
        $grades = $request->grade;
        $semi = $request->semi_finish_product;
        $burden_qty = $request->burden_qty;
        $isin = BurdenGrade::where('burden_id', '=', $burden)->first();
        if (empty($isin)) {
            try {
                DB::transaction(function () use ($burden, $qty, $grades, $semi, $burden_qty) {
                    $used_qty = 0;

                    foreach ($grades as $key => $grade) {
                        if ($grade <= 2) {
                            $used_qty = $used_qty + $qty[$key];
                        }
                    }


                    foreach ($grades as $key => $grade) {
                        //if ($qty[$key] != 0) {
                        $bg = new BurdenGrade();
                        $bg->burden_id = $burden;
                        $bg->grade_id = $grade;
                        $bg->burden_qty = $qty[$key];
                        if ($bg->grade_id == 1) {
                            $bg->used_qty = $used_qty;
                        } else {
                            $bg->used_qty = 0;
                        }
                        if ($bg->grade_id == 1) {
                            $bg->save();
                        } else {
                            if ($qty[$key] != 0) {
                                $bg->save();
                            }
                        }

                        if ($grade == 4 || $grade == 3) {
                            $burden_qty = $burden_qty - $qty[$key];
                        }
                        if ($grade == 3) {
                            $vir = new VirtualMaterial();
                            $vir->burden_id = $burden;
                            $vir->semi_product_id = $semi;
                            $vir->qty = $qty[$key];
                            $vir->save();

                            $semiFinish = SemiFinishProduct::find($semi);
                            $trans = new ReusableStock();
                            $trans->causer_id = $semiFinish->semi_finish_product_id;
                            $trans->causer_type = 'SemiFinishProductManager\\Models\\SemiFinishProduct';
                            $trans->qty = $qty[$key];
                            $trans->expected_weight = $qty[$key] * $semiFinish->weight;
                            $trans->sales_return_header_id = null;
                            $trans->reusable_product_id = $semiFinish->semi_finish_product_id;
                            $trans->reusable_type = $semiFinish->reusable_type;
                            $trans->received_type = 'semi_grade';
                            $trans->save();
                        }
                        $gradeRef = $bg->id;

                        if ($bg->grade_id == 1) {
                            //StockTransaction::semiFinishStockTransaction(1, $semi, $grade, $qty[$key], 1, 1);
                            $semifini = SemiFinishProduct::find($semi);
                            $this->recordSemiFinishProductOutstanding($semifini, 1, $qty[$key], "Burden Grade $grade", $key, 'BurdenManager\\Models\\BurdenGrade', $burden, 'BurdenRequestGrade', 1, $gradeRef, '');

                        } else {
                            if ($qty[$key] != 0) {
                                // StockTransaction::semiFinishStockTransaction(1, $semi, $grade, $qty[$key], 1, 1);
                                $semifini = SemiFinishProduct::find($semi);
                                $this->recordSemiFinishProductOutstanding($semifini, $grade, $qty[$key], "Burden Grade $grade", $key, 'BurdenManager\\Models\\BurdenGrade', $burden, 'BurdenRequestGrade', 1, $gradeRef, '');

                            }
                        }
                        //}
                    }
                    //  StockTransaction::updateSemiFinishAvailableQty(1, $semi, $burden_qty);

                    $burden = Burden::find($burden);
                    $burden->status = 6;
                    $burden->save();
                });
                $semiFinish = SemiFinishProduct::find($semi);
                $reusableProduct = new ReusableProducts();
                $reusableProduct->reusable_type = $semiFinish->reusable_type;
                $reusableProduct->product_id = $semiFinish->semi_finish_product_id;
                $reusableProduct->note_id = $burden;
                $reusableProduct->qty = $qty[2];
                $reusableProduct->expected_weight = $weight;
                $reusableProduct->accepted_weight = 0;
                //collect type (1->semi,2->endproduct,3->salesReturn)
                $reusableProduct->collect_type = 1;
                $reusableProduct->status = 1;
                $reusableProduct->save();

                return  'true';
                return redirect(route('burden.index'))->with([
                    'success' => true,
                    'success.title' => 'Congratulations !',
                    'success.message' => 'Graded Successfully'
                ]);
            } catch (Exception $e) {
                return $e;
                return redirect(route('burden.index'))->with([
                    'error' => true,
                    'error.title' => 'Sorry !',
                    'error.message' => 'Something Went Wrong '
                ]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \BurdenManager\Models\Grade $grade
     * @return \Illuminate\Http\Response
     */
    public function destroy(Grade $grade)
    {
        $grade->delete();
        if ($grade->trashed()) {
            return true;
        } else {
            return false;
        }
    }

    public function getqty(Request $request)
    {
        return 'a';
        return $grade = Grade::find($request->id);
//        return $raw=RawMaterial::where('raw_material_id','=',$request->id)->first();

    }
}
