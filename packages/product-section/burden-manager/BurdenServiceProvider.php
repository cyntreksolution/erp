<?php

namespace BurdenManager;

use Illuminate\Support\ServiceProvider;

class BurdenServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/Views', 'BurdenManager');
        require __DIR__ . '/web.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('BurdenManager', function($app){
            return new BurdenManager;
        });
    }
}
