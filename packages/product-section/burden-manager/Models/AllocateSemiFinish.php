<?php

namespace BurdenManager\Models;

use EmployeeManager\Models\Group;
use EndProductBurdenManager\Models\EndProductBurden;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use SemiFinishProductManager\Models\SemiFinishProduct;
use ShortEatsManager\Models\CookingRequest;

class AllocateSemiFinish extends Model
{
    use SoftDeletes;

    protected $table = 'allocate_semi_finish';

    protected $dates = ['deleted_at'];

    public function burden(){
        if ($this->burden_type=='burden'){
            return $this->belongsTo(BurdenGrade::class,'semi_finish_burden_id');
        }else{
            return $this->belongsTo(CookingRequest::class,'semi_finish_burden_id');
        }
    }

}
