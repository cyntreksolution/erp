<?php

namespace BurdenManager\Models;

use App\SalesReturnHeader;
use EmployeeManager\Models\Group;
use EndProductManager\Models\EndProduct;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use SemiFinishProductManager\Models\SemiFinishProduct;

class ReusableProducts extends Model
{
    use SoftDeletes;

    protected $table = 'reusable_products';

    protected $dates = ['deleted_at'];

    const COLORS =['red','yellow','blue'];

    public function debitNoteHeader(){
        return $this->belongsTo(SalesReturnHeader::class,'note_id','debit_note_number');
    }
    public function endProduct(){
        return $this->belongsTo(EndProduct::class,'product_id');
    }
}
