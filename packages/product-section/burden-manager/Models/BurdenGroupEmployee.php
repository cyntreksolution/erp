<?php

namespace BurdenManager\Models;

use EmployeeManager\Models\Group;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use SemiFinishProductManager\Models\SemiFinishProduct;

class BurdenGroupEmployee extends Model
{
    use SoftDeletes;

    protected $table = 'burden_group_employees';

    protected $dates = ['deleted_at'];


    public function employee(){
        return $this->belongsTo('EmployeeManager\Models\Employee');
    }

    public function employeeGroup(){
        return $this->belongsTo(Group::class);
    }
}
