<?php

namespace BurdenManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use SemiFinishProductManager\Models\SemiFinishProduct;

class BurdenEmployee extends Model
{
    use SoftDeletes;

    protected $table = 'burden_employee';

    protected $dates = ['deleted_at'];

    public function employee(){
        return $this->belongsTo('EmployeeManager\Models\Employee');
    }
}
