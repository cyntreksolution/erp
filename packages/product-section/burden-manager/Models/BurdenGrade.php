<?php

namespace BurdenManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use SemiFinishProductManager\Models\SemiFinishProduct;

class BurdenGrade extends Model
{
    use SoftDeletes;

    protected $table = 'burden_grade';

    protected $dates = ['deleted_at'];

//    public static function boot()
//    {
//        parent::boot();
//
//        static::created(function ($item) {
//            $burden_id = $item->burden_id;
//            $burdens = BurdenGrade::whereBurdenId($burden_id)->all();
//            $used_qty = 0;
//            if (!empty($burdens)) {
//                foreach ($burdens as $burden) {
//                    if ($burden->grade_id <= 2) {
//                        $used_qty = $used_qty + $burden->burden_qty;
//                    }
//                }
//            }
//
//            if (!empty($burdens)) {
//                foreach ($burdens as $burden) {
//                    $b = BurdenGrade::find($burden->id);
//                    $b->used_qty = $used_qty;
//                    $b->Save();
//                }
//            }
//
//        });
//    }

    public function grade()
    {
        return $this->belongsTo('GradeManager\Models\Grade');
    }

    public function burden()
    {
        return $this->belongsTo(Burden::class);
    }

}
