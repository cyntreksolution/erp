@extends('layouts.back.master')@section('title','Burden | Pending')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/lightbox2/dist/css/lightbox.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}"/>

    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">

    <style>
        .profile-pic {
            position: relative;
            /*display: inline-block;*/
        }

        .profile-pic:hover .edit {
            display: block;
        }

        .edit {
            padding-top: 7px;
            padding-right: 7px;
            position: absolute;
            right: 0;
            top: 0;
            display: none;
        }

        .edit a {
            color: #000;
        }

        #container {
            position: relative;
            padding: 0px;
            width: 100%;
            height: 330px;
            overflow: auto;
        }

        #container .content {
            width: 100%;
            height: 25%;
        }
    </style>

@stop
@section('current','Burden')
@section('current_url',route('burden.create'))
@section('current2','Pending Burden')
@section('current_url2','')
@section('content')
    <div class="app-wrapper">
        <div class="profile-section-user">
            <div class="profile-cover-img profile-pic">
                <img src="{{asset('assets\img\semi2.jpg')}}" alt="">
            </div>
            <div class="profile-info-brief p-3">
                @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                <div class="user-profile-avatar avatar avatar-circle avatar-md project-iconr bg-{{$color[$burden->colour]}}"
                     data-plugin="firstLitter"
                     data-target="#{{$burden->id*754}}"></div>

                <div class="text-center">
                    <h5 class="text-uppercase mb-1">Burden Serial : {{$burden->serial}}</h5>
                    <h5 class="text-uppercase mb-1" id="{{$burden->id*754}}">{{$burden['semiProdcuts']->name}}</h5>
                    <h6 class="text mb-lg-1">{{'Burden Size : '.$burden->burden_size}} KG</h6>
                    <br>
                </div>

                <hr class="m-0">

                <div class="text-center py-3">
                    {{'Expected Quantity : '.$burden->expected_semi_product_qty}} units<br>
                    {{'Active Recipe : '.$burden['recipe']->name}}<br>
                    <hr class="m-0">
                    {{$burden['recipe']->desc}}
                </div>
            </div>
        </div>
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">{{$burden['semiProdcuts']->name}} Burden Requested
                    Ingredients</h5>
            </div>
            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">
                    <div class="container">
                        <div class="media-list">
                            @foreach($burden['recipe']->recipeContents as $item)
                                <div class="" style="padding-bottom: 1px"></div>
                                <div class="card p-1 bg-faded" style="width: 100%; margin-bottom:5px">
                                    <div class="media bg-white p-3">
                                        @if(isset($item['materials']->image))
                                            <img class="avatar avatar-circle avatar-md"
                                                 src="{{asset($item['materials']->image_path.$item['materials']->image)}}"
                                                 alt="">
                                        @else
                                            @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                            <div class="avatar avatar-circle avatar-md project-iconr bg-{{$color[$item['materials']->colour]}}"
                                                 data-plugin="firstLitter"
                                                 data-target="#{{$item['materials']->raw_material_id}}"></div>
                                        @endif
                                        <div class="media-body">
                                            <h6 id="{{$item['materials']->raw_material_id}}">{{$item['materials']->name}}</h6>
                                            {{--<div>Android Developer</div>--}}
                                        </div>
                                        <div class="d-flex activity-counters justify-content-between">
                                            <div class="text-center px-2">
                                                <h6 class="text-warning">QTY per Burden</h6>
                                                <h6>
                                                    @if($item['materials']->measurement =='gram')
                                                        @if($item['qty']*$burden['burden_size'] > 999)
                                                            {{($item['qty']*$burden['burden_size']/1000)}}
                                                        @else
                                                            {{($item['qty']*$burden['burden_size'])}}
                                                        @endif

                                                    @elseif($item['materials']->measurement =='milliliter')
                                                        @if($item['qty']*$burden['burden_size'] > 999)
                                                            {{($item['qty']*$burden['burden_size']/1000)}}
                                                        @else
                                                            {{($item['qty']*$burden['burden_size'])}}
                                                        @endif

                                                    @elseif($item['materials']->measurement =='unit')
                                                        {{($item['qty']*$burden['burden_size'])}}

                                                    @endif
                                                </h6>
                                                <small>
                                                    @if($item['materials']->measurement =='gram')
                                                        @if($item['qty']*$burden['burden_size'] > 999)
                                                            kilo gram
                                                        @else
                                                            grams
                                                        @endif

                                                    @elseif($item['materials']->measurement =='milliliter')
                                                        @if($item['qty']*$burden['burden_size'] > 999)
                                                            litre
                                                        @else
                                                            milliliter
                                                        @endif

                                                    @elseif($item['materials']->measurement =='unit')
                                                        units
                                                    @endif
                                                </small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/waypoints/lib/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/waypoints/lib/shortcuts/sticky.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/counterup/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/js/jquery.sparkline.min.js')}}"></script>
    <script src="{{asset('js/site.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>


    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>

    <script>
        var ps = new PerfectScrollbar('#container');

    </script>
@stop