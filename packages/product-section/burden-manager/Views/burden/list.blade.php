@extends('layouts.back.master')@section('title','Burden | List')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
            white-space: nowrap;
        !important;
        }
    </style>
@stop

@section('content')
    {{ Form::open(array('url' => '/burden/excel/data','id'=>'filter_form'))}}
    <div class="row">

        <div class="col-sm-2">
            <div class="form-group">
                {!! Form::select('semi', $semiFinish , null , ['class' => 'form-control','placeholder'=>'Semi Finish Product','id'=>'semi']) !!}
            </div>
        </div>

        <div class="col-sm-2">
            <div class="form-group">
                {!! Form::select('status', [1=>'pending',2=>'received',3=>'creating',4=>'burning',5=>'grade',6=>'finished',] , null , ['class' => 'form-control','placeholder'=>'Status','id'=>'status']) !!}
            </div>
        </div>

        <div class="col-sm-2">
            <div class="form-group">
                {!! Form::select('date_type', [1=>'Date of Created',2=>'Date of Stage One',3=>'Date Of Stage Two',4=>'Date Of Grated or Finished',] , null , ['class' => 'form-control','id'=>'date_type']) !!}
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <input type="text" name="date_range" id="date_range" class="form-control">
            </div>
        </div>

        <div class="col-sm-3">
            <div class="form-group">
                <button type="button" class="btn btn-success" onclick="process_form()">Filter Now</button>
                <button type="button" class="btn btn-warning" onclick="process_form_reset()">Reset Filter</button>
                <button type="submit" name="action" value="download_data" class="btn btn-info"><i
                            class="fa fa-download"></i></button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
    <div class="">
        <table id="burden_table" class="display text-center">
            <thead>
            <tr>
                <th>Burden Serial</th>
                <th>Semi Finish Product Name</th>
{{--                <th>Current Status</th>--}}
                <th>Burden Size</th>
                {{--                <th>Recipe Name</th>--}}
                <th>Date</th>
                <th>A</th>
                <th>B</th>
                <th>C</th>
                <th>C weight</th>
                <th>D</th>
                <th>Action</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Burden Serial</th>
                <th>Semi Finish Product Name</th>
{{--                <th>Current Status</th>--}}
                <th>Burden Size</th>
                {{--                <th>Recipe Name</th>--}}
                <th>Date</th>
                <th>A</th>
                <th>B</th>
                <th>C</th>
                <th>C weight</th>
                <th>D</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
    </div>
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script>
        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019"
        });
        $('#date_range').val('')


        var grade_a;
        var grade_b;
        var grade_c;
        var grade_d;
        var grade_c_weight;
        var weight;
        $(document).ready(function () {
            table = $('#burden_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "ajax": {
                    url: "{{url('/burden/table/data')}}", // json datasource
                    type: "get",
                    error: function () {  // error handling code
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    },
                    dataSrc: function (data) {
                        grade_a = data.stat_a;
                        grade_b = data.stat_b;
                        grade_c = data.stat_c;
                        grade_d = data.stat_d;
                        weight = data.stat_burden_size;
                        return data.data;
                    }
                },
                pageLength: 25,
                responsive: true,
                drawCallback: function (settings) {
                    var api = this.api();

                    $(api.column(2).footer()).html(
                        'Burden Weight : ' + weight
                    );
                    $(api.column(4).footer()).html(
                        'Grade A : ' + grade_a
                    );
                    $(api.column(5).footer()).html(
                        'Grade B : ' + grade_b
                    );
                    $(api.column(6).footer()).html(
                        'Grade C : ' + grade_c
                    );
                    $(api.column(8).footer()).html(
                        'Grade D : ' + grade_d
                    );
                }
            });
        });

        function process_form() {
            let date_range = $("#date_range").val();
            let status = $("#status").val();
            let semi = $("#semi").val();
            let date_type = $("#date_type").val();
            let table = $('#burden_table').DataTable();
            table.ajax.url('/burden/table/data?semi=' + semi + '&status=' + status + '&date_range=' + date_range + '&date_type=' + date_type + '&filter=' + true).load();

        }

        function process_form_reset() {
            $("#date_range").val('');
            $("#status").val('');
            $("#semi").val('');
            let table = $('#burden_table').DataTable();
            table.ajax.url('/burden/table/data').load();

        }


    </script>
    <script>
        function deleteBurden(id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            swal({
                    title: "Are you sure ?",
                    text: "Do you want to Delete? This action cannot be recover !",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, Delete it!",
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: "post",
                            url: '../burden/delete/burden',
                            data: {burden_id: id},

                            success: function (response) {
                                if (response == 'true') {
                                    swal("Nice!", "You successfully Deleted the burning", "success");
                                    setTimeout(location.reload.bind(location), 900);
                                } else {
                                    swal("Error!", "Something went wrong", "error");
                                }
                            }
                        });


                        // });

                    } else {
                        swal("Cancelled", "You cancelled the finishing :)", "error");
                    }
                });


        }
    </script>
@stop
