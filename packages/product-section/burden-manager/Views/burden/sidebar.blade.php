<div class="profile-section-user">
    <div class="profile-cover-img profile-pic">
        <img src="{{asset('assets\img\semi2.jpg')}}" alt="">
    </div>
    <div class="profile-info-brief p-3">
        @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
        <div class="user-profile-avatar avatar avatar-circle avatar-md project-iconr bg-{{$color[$burden->colour]}}"
             data-plugin="firstLitter"
             data-target="#{{$burden->id*754}}"></div>

        <div class="text-center">
            <h5 class="text-uppercase ">{{$burden->serial}}</h5>
            <h5 class="text-uppercase mb-1" id="{{$burden->id*754}}">{{$burden['semiProdcuts']->name}}</h5>
            <h6 class="text mb-lg-1">{{'Burden Size : '.$burden->burden_size}} </h6>
            <br>
        </div>

        <hr class="m-0">

        <div class="py-3">
            {{'Active Recipe : '.$burden['recipe']->name}}<br>
            <hr class="m-1">
            {{'Expected Quantity : '.$burden->expected_semi_product_qty}} units<br>
        </div>
        <div class="py-3">
            @php
                $g_count = \BurdenManager\Models\BurdenGrade::where('burden_id','=',$burden->id)->where('grade_id','<=',2)->sum('burden_qty');
                $g_availble_count = \BurdenManager\Models\BurdenGrade::where('burden_id','=',$burden->id)->where('grade_id','=',1)->sum('used_qty');
                $g_used_count = $g_count-$g_availble_count;
                $gg = \Carbon\Carbon::today();
                $tom = \BurdenManager\Models\Burden::where('id','=',$burden->id)->where('created_start_time', '>', $gg->subDays(2)->format('Y-m-d') . '%')->first();
                $tob = \BurdenManager\Models\Burden::where('id','=',$burden->id)->where('bake_start_time', '>', $gg->subDays(2)->format('Y-m-d') . '%')->first();
 @endphp
            {{--'Graded Quantity : '.$g_count}} <br>
            {{'Used Quantity : '.$g_availble_count}} <br>
            {{'Available Quantity : '.$g_used_count--}} <br>
        </div>
        <div class="py-3">
            <hr class="m-1">
            <?php $bakers=$burdenClass->getBakers($burden->baker);
            $makers=$burdenClass->getBakers($burden->employee_group);

            ?>


            <b class="font-weight-bold">First Stage </b><br>
                -Start At : {{(!empty($burden->created_start_time)) ?\Carbon\Carbon::parse($burden->created_start_time)->format('d-m-Y H:m:s'):'-'}} <br>
                -Work Group {{(!empty($burden->employee_group)) && $makers->count()>0 ?$makers[0]->name:'-'}} </br>
            @if($role = (Auth::user()->hasRole(['Super Admin'])) || (!empty($tom)))
                <i data-toggle="modal" data-target="#exampleModal2"
                   class="fa fa-pencil float-right text-right"></i>
            @endif
            <br>

            @if (!empty($burden->employee_group))
                @foreach ($makers as $gr)
                    @foreach ($gr->employees as $user)
                        <small class="text-left">{{$user->full_name}}</small><br>
                    @endforeach
                @endforeach
            @endif
        </div>
        <div class="py-3">
            <hr class="m-1">
            <b class="font-weight-bold">Second Stage </b><br>
                -Start At : {{(!empty($burden->bake_start_time)) ?\Carbon\Carbon::parse($burden->bake_start_time)->format('d-m-Y H:m:s'):'-'}} <br>
                -Work Group : {{!empty($bakers) && $bakers->count()>0 ?$bakers[0]->name:'-'}} <br>
            @if($role = (Auth::user()->hasRole(['Super Admin'])) || (!empty($tob)))
                <i data-toggle="modal" data-target="#exampleModal"
                   class="fa fa-pencil float-right text-right"></i>
            @endif
            <br>

            @if (!empty($burden->baker))
                @foreach ($bakers as $gr)
                    @foreach ($gr->employees as $user)
                        <small class="text-left">{{$user->full_name}}</small><br>
                    @endforeach
                @endforeach
            @endif

        </div>
        <hr class="m-0">

        <div class="py-3">

        </div>
        <div class="py-3">
            <b class="font-weight-bold">

                @if ($burden->responce === 0)
        {{'Current Responsibility : '}}-First Stage <br>
                <b class="font-weight-bold">Change Responsible Stage
                    {!! Form::open(['route' => ['upToSecond.burden'], 'method' => 'post']) !!}
                    <input type="hidden" name="burden_id" value="{{$burden->id}}">
                    <button type="submit" class="btn btn-warning">To Second Stage</button>
                    {!! Form::close() !!}
                    @else
        {{'Current Responsibility : '}}-Second Stage <br>
                        <b class="font-weight-bold">Change Responsible Stage
                    {!! Form::open(['route' => ['upToFirst.burden'], 'method' => 'post']) !!}
                    <input type="hidden" name="burden_id" value="{{$burden->id}}">
                    <button type="submit" class="btn btn-danger">To First Stage</button>
                    {!! Form::close() !!}
                @endif
        </div>
    </div>
</div>
