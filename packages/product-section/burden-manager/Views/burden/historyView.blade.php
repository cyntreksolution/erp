@extends('layouts.back.master')@section('title','Burden | Details')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/lightbox2/dist/css/lightbox.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}"/>

    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">

    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }


        .profile-pic {
            position: relative;
            /*display: inline-block;*/
        }

        .profile-pic:hover .edit {
            display: block;
        }

        .edit {
            padding-top: 7px;
            padding-right: 7px;
            position: absolute;
            right: 0;
            top: 0;
            display: none;
        }

        .edit a {
            color: #000;
        }

        #container {
            position: relative;
            padding: 0px;
            width: 100%;
            height: 330px;
            overflow: auto;
        }

        #container .content {
            width: 100%;
            height: 25%;
        }
    </style>

@stop
@section('current','Burden Request List')
@section('current_url',route('burden.list'))
@section('current2',$burden['semiProdcuts']->name.' Burden')
@section('current_url2','')
@section('content')

    <div class="app-wrapper">

        <div class="col-md-3">
        @include('BurdenManager::burden.sidebar')
        </div>
        <div class="app-main">

            <div class="app-main-header" class="col-md-12">
                <h5 class="app-main-heading text-center">------------{{$burden->created_at->format('d l F Y').' - '.$burden['semiProdcuts']->name}}
                    Burden--------------</h5>
            </div>
            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content" class="col-md-12">
                    <div class="media-list" id="rcontainer">

                        <div class="media content">
                            @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                            <div class="avatar avatar avatar-md project-icon bg-{{$color[2]}}"
                                 data-plugin="firstLitter"
                                 data-target="#90"></div>
                            <div class="media-body">
                                <div class="col-md-9">
                                    <h6 class="project-name"
                                        id="90">Grade</h6>
                                </div>
                                <br>
                            </div>
                            <div class="col-md-3">
                                <button class="btn btn-outline-primary" type="button" data-toggle="collapse"
                                        data-target="#multiCollapseExample1"
                                        aria-expanded="false"
                                        aria-controls="multiCollapseExample1"
                                        style="width: 100%; border-color: transparent">
                                                <span>
                                                <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                </span> Quantity
                                </button>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="" id="multiCollapseExample1" style="">
                                <div class="card-body">
                                    @foreach($burden['grade'] as $r)
                                        <div class="row burden">
                                            <div class="col-md-2">
                                                <span><i class="fa fa-shopping-bag"></i> </span>
                                            </div>
                                            <div class="col-md-7">
                                                {{$r['grade']->name}}

                                            </div>
                                            <div class="col-md-3">
                                                {{$r->burden_qty}}
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <hr>


                        @php
                        $ref = 'App/Burden';
                            $alc = \BurdenManager\Models\AllocateSemiFinish::where('semi_finish_burden_id','=',$burden->id)->where('burden_type','=','burden')->get();
                            $allocations = \App\SemiBurdenIssueData::whereReference($burden->id)->whereReferenceType($ref)->get();
                            $g_count = \BurdenManager\Models\BurdenGrade::where('burden_id','=',$burden->id)->where('grade_id','<=',2)->sum('burden_qty');

                        @endphp

                        <div class="media content" >

                            <div class="col-md-6" align="center">
                                Date & Description
                            </div>

                            <div class="col-md-4">
                                End Product Serial
                            </div>

                            <div class="col-md-2">
                                <p class="text-nowrap text-right"> Qty</p>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="" id="multiCollapseExample1" style="">
                                <div class="card-body">
                                    @php
                                        $total =0;
                                    @endphp
                                    @foreach($allocations as $allocation)
                                        @php
                                            $data = \EndProductBurdenManager\Models\BurdenSemi::find($allocation->merege_burden_semi_id);
                                        @endphp
                                        <div class="row ">
                                            <div class="col-md-6">
                                                <p class="text-nowrap">{{$allocation->created_at}} - {{$allocation->description}}</p>
                                            </div>

                                            <div class="col-md-4">
                                                <p class="text-nowrap"> {{!empty($data)?$data->endProduct->name:null}} - {{!empty($data)?$data->endProductBurden->serial:null}} </p>
                                            </div>

                                            <div class="col-md-2 text-right">
                                                <p class="text-nowrap text-right"> {{number_format($allocation->used_qty,3)}}</p>
                                            </div>
                                        </div>
                                        @php $total =$total + $allocation->used_qty @endphp
                                    @endforeach
                                    @foreach($alc as $alc)
                                        @php

                                            $endSerial =\EndProductBurdenManager\Models\EndProductBurden::find($alc->end_product_burden_id);

                                            $endName =\EndProductManager\Models\EndProduct::find($endSerial->end_product_id);
                                        @endphp
                                        <div class="row ">
                                            <div class="col-md-6">
                                                <p class="text-nowrap -align-left">{{$alc->created_at->format('Y-M-d H:i:s')}} - Make End Product</p>
                                            </div>


                                            <div class="col-md-4">
                                                <p class="text-nowrap"> {{$endName->name}} - {{$endSerial->serial}}</p>
                                            </div>

                                            <div class="col-md-2 text-right">
                                                <p class="text-nowrap text-right"> {{number_format($alc->allocate_qty,3)}}</p>
                                            </div>
                                        </div>


                                        @php $total =$total + $alc->allocate_qty @endphp
                                    @endforeach
                                    @php //dd($g_count); @endphp
                                    <hr>
                                    <div class="row ">
                                        <div class="col-md-6">
                                            <p class="text-nowrap font-weight-bold">Total</p>
                                        </div>
                                        <div class="col-md-4">
                                        </div>
                                        <div class="col-md-2 text-right">
                                            <p class="text-nowrap font-weight-bold text-right"> {{number_format($total,3)}}</p>
                                        </div>
                                    </div>

                                        <hr>
                                        <div class="row ">
                                            <div class="col-md-6">
                                                <p class="text-nowrap font-weight-bold">Balance Qty In Hand</p>
                                            </div>
                                            <div class="col-md-4">
                                            </div>
                                                <div class="col-md-2 text-right">
                                                    <p class="text-nowrap font-weight-bold text-right"> {{number_format((($g_count)-($total)),3)}}</p>
                                                </div>
                                            </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Second Stage</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => ['burden.changeb',$burden->id], 'method' => 'post']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            Baker
                        </div>
                        <div class="col-md-6">
                            <select required name="baker" class="form-control">
                                @foreach($bakers as $group)
                                    <option value="{{$group->id}}">{{$group->employees->pluck('first_name')}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary">Save changes</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit First Stage</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => ['burden.changeEG',$burden->id], 'method' => 'post']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            Employee Groups
                        </div>
                        <div class="col-md-6">
                            <select required name="employee_group" class="form-control">
                                @foreach($employee_groups as $group)
                                    <option value="{{$group->id}}">{{$group->employees->pluck('first_name')}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary">Save changes</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/waypoints/lib/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/waypoints/lib/shortcuts/sticky.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/counterup/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/js/jquery.sparkline.min.js')}}"></script>
    <script src="{{asset('js/site.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>


    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>

    <script>
        var ps = new PerfectScrollbar('#container');

    </script>
@stop
