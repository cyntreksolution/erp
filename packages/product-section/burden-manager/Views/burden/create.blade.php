@extends('layouts.back.master')@section('title','Make Semi Finish Product')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>

    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">

    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        #projects-task-modal2 .close {
            border: none;
            outline: none;
            box-shadow: none;
            position: absolute;
            right: 1rem;
            top: 1rem;
            width: 25px;
            height: 25px;
            line-height: 25px;
            text-align: center;
            font-size: 1.13rem;
            border-radius: 500px;
            border: 1px solid #868e96
        }

        #floating-button {
            width: 45px;
            height: 45px;
            border-radius: 50%;
            background: #db4437;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2;
            text-align: center;
            padding: 2px;
        }

        #floating-buttona {
            width: 45px;
            height: 45px;
            border-radius: 50%;
            background: #db4437;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2;
            text-align: center;
            padding: 2px;
        }

        .plus {
            color: white;

            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 38px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }


        #container {
            position: relative;
            padding: 0px;
            width: 100%;
            height: 200px;
            overflow: auto;
        }

        #container .content {
            width: 100%;
            height: 50%;
        }

        .hide {
            display: none;
        }


        #container2 {
            position: relative;
            /*padding: 0px;*/
            width: 98%;
            height: 420px;
            overflow: auto;
        }

        #container2 .content {
            width: 100%;
            height: 20%;
        }

    </style>
@stop
@section('current','Burden')
@section('current_url',route('burden.create'))
@section('content')
    <div class="app-wrapper">
        <div class="app-panel" id="app-panel">
            <div class="app-search"><input type="search" class="search-field" placeholder="Search">
                <i class="search-icon fa fa-search"></i>
            </div>
            <div class="app-panel-inner">
                <div class="scroll-container">
                    @if(Auth::user()->hasRole(['Super Admin', 'Owner']))
                    <div class="p-3">
                        <button class="btn btn-success py-3 btn-block btn-lg" data-toggle="modal"
                                data-target="#projects-task-modal">NEW BURDEN
                        </button>
                    </div>
                    @endif
                    <div class="p-3">
                        <a href="view/history" class="btn btn-danger py-3 btn-block btn-lg text-white">BURDEN HISTORY
                        </a>
                        <!--button class="avatar-sm avatar-circle fz-base font-weight-bold add-people-btn mx-1">
                            <i data-toggle="tooltip" data-placement="bottom" title="Burden History"
                               class="zmdi zmdi-trending-up"></i>
                        </button-->
                    </div>
                </div>

                <hr class="m-0">
                <div class="people-list d-flex justify-content-start flex-wrap p-3">

                    <!--a href="view/history">
                        <button class="avatar-sm avatar-circle fz-base font-weight-bold add-people-btn mx-1">
                            <i data-toggle="tooltip" data-placement="bottom" title="Burden History"
                               class="zmdi zmdi-trending-up"></i>
                        </button>
                    </a-->

                    {{--                    <a href="date">--}}
                    {{--                        <button class="avatar-sm avatar-circle fz-base font-weight-bold add-people-btn mx-1">--}}
                    {{--                            <i data-toggle="tooltip" data-placement="bottom" title="Check Date"--}}
                    {{--                               class="zmdi zmdi-time-interval"></i>--}}
                    {{--                        </button>--}}
                    {{--                    </a>--}}

                </div>
                <hr class="m-0">


            </div>
            <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show"><i
                        class="fa fa-chevron-right"></i>
                <i class="fa fa-chevron-left"></i>
            </a>
        </div>
        <!-- /.app-panel -->
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">TODAY BURDENS</h5>
            </div><!-- /.app-main-header -->
            <div class="app-main-content">
                <div class="projects-list" id="container2">
                    @foreach($burdens as $brdn)
                        <div>
                            <div class="media content2">
                                <div class="avatar avatar text-white avatar-md project-icon bg-primary"
                                     data-target="#project-1">
                                    <i class="fa fa-pause" aria-hidden="true"></i>
                                </div>
                                <div class="media-body">
                                    <small class="project-name" id="project-1">{{$brdn->serial}}</small>
                                    <h6 class="project-name" id="project-1">{{$brdn['semiProdcuts']->name}}</h6>
                                    <small class="project-detail">Size : {{$brdn->burden_size}}</small>
                                    <div>
                                        <small class="project-detail">Quantity
                                            : {{$brdn->expected_semi_product_qty}}</small>
                                    </div>

                                </div>

                                <a href="/burden/print/{{$brdn->id}}" target="_blank" class="btn btn-outline-info"
                                   style="width:6rem">
                                    <i class="fa fa-print" aria-hidden="true"></i>
                                    Print
                                </a>

                                <button class="btn btn-outline-primary m-2" style="width:6rem">
                                    <i class="fa fa-fire" aria-hidden="true"></i>
                                    pending
                                </button>

                                <!--button value="{{$brdn->id}}" class="btn btn-outline-danger ml-3 delete-burden"
                                        style="width:6rem">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                    Delete
                                </button-->
                            </div>
                            <hr class="m-0">
                        </div>
                    @endforeach

                    @foreach($recievedBurdens as $brdn)
                    <!--a href="{{$brdn->id.'/edit'}}"-->
                        <div class="media content2">
                            <div class="avatar avatar text-white avatar-md project-icon bg-info"
                                 data-target="#project-1">
                                <i class="fa fa-pause" aria-hidden="true"></i>
                            </div>
                            <div class="media-body">
                                <small class="project-name" id="project-1">{{$brdn->serial}}</small>
                                <h6 class="project-name" id="project-1">{{$brdn['semiProdcuts']->name}}</h6>
                                <small class="project-detail">Size : {{$brdn->burden_size}}</small>
                                <div>
                                    <small class="project-detail">Quantity
                                        : {{$brdn->expected_semi_product_qty}}</small>
                                </div>

                            </div>
                            <button class="btn btn-outline-info" style="width:6rem">
                                <i class="fa fa-fire" aria-hidden="true"></i>
                                recived
                            </button>
                        </div>
                        <hr class="m-0">
                        </a>
                    @endforeach

                    @foreach($creatingBurdens as $brdn)
                    <!--a href="{{$brdn->id.'/bake'}}"-->
                        <div class="media content2">
                            <div class="avatar avatar text-white avatar-md project-icon bg-warning"
                                 data-target="#project-1">
                                <i class="fa fa-pause" aria-hidden="true"></i>
                            </div>
                            <div class="media-body">
                                <small class="project-name" id="project-1">{{$brdn->serial}}</small>
                                <h6 class="project-name" id="project-1">{{$brdn['semiProdcuts']->name}}</h6>
                                <small class="project-detail">Size : {{$brdn->burden_size}}</small>
                                <div>
                                    <small class="project-detail">Quantity
                                        : {{$brdn->expected_semi_product_qty}}</small>
                                </div>

                            </div>
                            <button class="btn btn-outline-warning" style="width:6rem">
                                <i class="fa fa-fire" aria-hidden="true"></i>
                                creating
                            </button>
                        </div>
                        <hr class="m-0">
                        </a>
                    @endforeach

                    @foreach($bakingBurdens as $brdn)
                        <a class="burning" value="{{$brdn->id}}">
                            <div class="media content2">
                                <div class="avatar avatar text-white avatar-md project-icon bg-danger"
                                     data-target="#project-1">
                                    <i class="fa fa-pause" aria-hidden="true"></i>
                                </div>
                                <div class="media-body">
                                    <small class="project-name" id="project-1">{{$brdn->serial}}</small>
                                    <h6 class="project-name" id="project-1">{{$brdn['semiProdcuts']->name}}</h6>
                                    <small class="project-detail">Size : {{$brdn->burden_size}}</small>
                                    <div>
                                        <small class="project-detail">Quantity
                                            : {{$brdn->expected_semi_product_qty}}</small>
                                    </div>

                                </div>
                                <button class="burning btn btn-outline-danger" style="width:6rem" value="{{$brdn->id}}">
                                    <i class="fa fa-fire" aria-hidden="true"></i>
                                    burning
                                </button>
                            </div>
                            <hr class="m-0">
                        </a>
                    @endforeach

                    @foreach($finishBurdens as $brdn)
                        <a href="{{'../burden/grade/'.$brdn->id.'/edit'}}">
                            <div class="media content2">
                                <div class="avatar avatar text-white avatar-md project-icon bg-success"
                                     data-target="#project-1">
                                    <i class="fa fa-pause" aria-hidden="true"></i>
                                </div>
                                <div class="media-body">
                                    <small class="project-name" id="project-1">{{$brdn->serial}}</small>
                                    <h6 class="project-name" id="project-1">{{$brdn['semiProdcuts']->name}}</h6>
                                    <small class="project-detail">Size : {{$brdn->burden_size}}</small>
                                    <div>
                                        <small class="project-detail">Quantity
                                            : {{$brdn->expected_semi_product_qty}}</small>
                                    </div>

                                </div>
                                <button class="btn btn-outline-success" style="width:6rem">
                                    <i class="fa fa-fire" aria-hidden="true"></i>
                                    grade
                                </button>
                            </div>
                            <hr class="m-0">
                        </a>
                    @endforeach

                </div>


            </div>
        </div>
    </div>

    <div class="modal fade" id="projects-task-modal" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="card">
                        <div class="projects-list">
                            {{ Form::open(array('url' => 'burden','enctype'=>'multipart/form-data' ,'id'=>'bootstrap-wizard-form'))}}
                            <div class=" row">

                                <div class="col-sm-6">
                                    <div class="d-flex mr-auto">
                                        <div class="form-group">
                                            <label for="single">Semi-Finished Product</label>
                                            <select onchange="getSizes()"
                                                    id="single" name="semi_finish_product"
                                                    class="form-control select2 " ui-jp="select2"
                                                    style="width: 15rem"
                                                    ui-options="{theme: 'bootstrap'}">
                                                <option>Select Semi Products</option>

                                                @foreach($semiProducts as $product)
                                                    <option value=" {{$product->semi_finish_product_id}}">{{$product->name}}</option>
                                                @endforeach

                                            </select>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="d-flex activity-counters justify-content-between">
                                        <div class="text-center px-2">
                                            <label for="single"> Burden Size</label>
                                            @if(Auth::user()->hasRole(['Super Admin', 'Owner']))
                                                   <input class="form-control" id="size" name="burden_size" type="text" style="width:100px"/>
                                            @else
                                            <select class="form-control" id="size" name="burden_size"></select>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="d-flex activity-counters justify-content-between">

                                        <div class="text-center px-2">

                                            <label for="single">Burden Count</label>
                                            <input class="form-control" id="brdncont" name="burden_count"
                                                   placeholder="" type="text" style="width:100px">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-success btn-lg btn-block">
                                Add
                            </button>

                        </div>

                    </div>
                    {!!Form::close()!!}
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="recipe" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12">
                        <div class="card">

                            {{ Form::open(array('url' => 'recipe','id'=>'bootstrap-wizard-form1')) }}
                            <div class="wizard p-4" id="bootstrap-wizard-1">
                                <ul class="nav nav-tabs vertices keep-prefix-suffix" role="tablist">
                                    <li class="nav-item">
                                        <a class=" nav-link active" data-toggle="tab" href="#ex5-step-1"
                                           role="tab">
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#ex5-step-2"
                                           role="tab">
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    {{--<div class="tab-pane active" id="ex5-step-1" role="tabpanel">--}}
                                    <div class="tab-pane active" id="ex5-step-1" role="tabpanel">
                                        <div class="task-name-wrap">
                                            <select id="single2" name="semi_product" class="form-control select2">
                                                <option value=''>Select Semi Product</option>
                                                @foreach($semi as $sp)
                                                    <option value="{{$sp->semi_finish_product_id}}">{{$sp->name}}</option>
                                                @endforeach

                                            </select>

                                        </div>
                                        <hr>

                                        <div class="task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="recipe_name"
                                                   placeholder="Recipe Name">
                                        </div>
                                        <hr>

                                        <div class="task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="desc"
                                                   placeholder="Description">
                                        </div>
                                        <hr>

                                        <div class="task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="units_per_recipe"
                                                   placeholder="Units per Recipe">
                                        </div>
                                        <hr>


                                    </div>
                                    <div class="tab-pane" id="ex5-step-2" role="tabpanel">
                                        <div class="row">
                                            <form name="ingredient" id="ingredient">
                                                <div class="col-sm-6 mt-1">
                                                    <div class="form-group">
                                                        <label for="single">Ingrediants</label>
                                                        <select id="single" name="kk"
                                                                style="width:100%;"
                                                                onChange="selectIngrediants(this);"
                                                                class="form-control select2" ui-jp="select2"
                                                                ui-options="{theme: 'bootstrap'}">
                                                            <option value=''>Select Ingrediants</option>
                                                            @foreach($products as $product)
                                                                <option value="{{$product->raw_material_id}}">{{$product->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-sm-3 mt-1 ">
                                                    <label for="single">Quantity</label>
                                                    <input type="text" placeholder="" class="abc info form-control w-20"
                                                           id="qty" name="qty"
                                                           style="width: 100%;height: calc(2.25rem + 2px);"/>
                                                </div>

                                                <div class="col-sm-3 py-4 px-4 mt-2">
                                                    <div class="form-group">
                                                        <div id="floating-button">
                                                            <p class="plus">+</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>


                                        </div>
                                        {{--<div class="col-lg-12 col-md-6">--}}
                                        {{--<div class="card p-1 bg-faded">--}}
                                        {{--<div  id="selecting">--}}
                                        {{--<!-- /.media-body -->--}}
                                        {{--</div><!-- /.media -->--}}
                                        {{--</div><!-- /.card -->--}}
                                        {{--</div>--}}
                                        <div class="scroll-container">
                                            <div class="col-lg-12 col-md-6">
                                                <div class="projects-list" id="selecting">

                                                </div>

                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <div class="pager d-flex justify-content-center">
                                    <input type="submit" id="finish-btn" class="finish btn btn-success w-50"
                                           value="Finish"/>

                                    <button type="button" id="next-btn" class="next btn btn-success w-50">Next
                                    </button>
                                </div>
                            </div>
                            {{ Form::close() }}

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        function printItems(e) {
            alert(1)
        }
    </script>

@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/js/jquery.sortable.js')}}"></script>
    <script src="{{asset('assets/examples/js/demos/form.wizard.js')}}"></script>

    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>



    <script>
        var ps = new PerfectScrollbar('#container');
        var psr = new PerfectScrollbar('#container2');

        function getSizes() {
            var semi = $("#single option:selected").val();
            $.ajax({
                url: "/get/semi/burden/size/" + semi,
                type: "GET",
                success: function (data) {
                    var $dropdown = $("#size");
                    $dropdown.empty();
                    $.each(data, function(i,k) {
                        $dropdown.append($("<option />").val(i).text(k));
                    });
                },
                error: function (xhr, status) {

                }
            });
        }

        $("#floating-button").click(function () {

            var a = $("#single option:selected").val();
            var size = $('#size').val();
            var brdncont = $('#brdncont').val();

            reset();
        });

        function reset() {

            document.getElementById("single").value = "";
            document.getElementById("size").value = "";
            document.getElementById("brdncont").value = "";
        }


        $("#floating-buttona").click(function () {

            var a = $("#grade option:selected").val();
            var bqty = $('#bqty').val();

            $.ajax({
                type: "get",
                url: '../burden/getqty',
                data: {id: a},

                success: function (response) {
//
                    $(".abc").append(' <div class=" card-body d-flex  align-items-center p-0" >' +
                        '<div class="d-flex mr-auto">' +
                        '<div>' +
                        '<div class="avatar avatar text-white avatar-md project-icon bg-primary">' +
                        '<img class="card-img-top" src="" alt="">' +
                        '</div>' +
                        '</div>' +
                        '<h5 class="mt-3 mx-3" >' + response['name'] + '</h5>' +
                        '<input type="hidden" name="burden[]" value=" ' + response['id'] + ' "/>' +
                        '<input type="hidden" name="bqty[]" value=" ' + bqty + ' "/>' +
                        '</div>' +
                        '<div class="d-flex activity-counters justify-content-between">' +
                        '<div class="text-center px-5">' +
                        '<div class="text-primary"><h6 >' + bqty + '</h6>' +
                        '</div>' +
                        '</div>' +
                        '</div> ' +
                        '</div>');
                }
            });

            reset();
        });

        function reset() {

            document.getElementById("grade").value = "";
            document.getElementById("bqty").value = "";
        }

        $('.burning').click(function (e) {
            e.preventDefault();
            id = $(this).attr("value");
            confirmAlert(id);

        });

        $('.delete-burden').click(function (e) {
            e.preventDefault();
            id = $(this).attr("value");
            deleteBurden(id);

        });

        function confirmAlert(id) {
            swal({
                    title: "Are you sure to finish burning?",
                    text: "You will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, Finish it!",
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        // swal({
                        //         title: "Add Quantity!",
                        //         text: "Add actual quantity of the burden:",
                        //         type: "input",
                        //         showCancelButton: true,
                        //         closeOnConfirm: false,
                        //     },
                        //     function (inputValue) {
                        //         if (inputValue === false) return false;
                        //         if (inputValue === "") {
                        //             swal.showInputError("You need to add quantity!");
                        //             return false
                        //         }
                        //         var qty = inputValue;

                        $.ajax({
                            type: "get",
                            url: '../burden/save/qty',
                            data: {burden_id: id, qty: 0},

                            success: function (response) {
                                if (response == 'true') {
                                    swal("Nice!", "You successfully finished the burning", "success");
                                    setTimeout(location.reload.bind(location), 900);
                                } else {
                                    swal("Error!", "Something went wrong", "error");
                                }
                            }
                        });


                        // });

                    } else {
                        swal("Cancelled", "You cancelled the finishing :)", "error");
                    }
                });


        }

        function deleteBurden(id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            swal({
                    title: "Are you sure ?",
                    text: "Do you want to Delete? This action cannot be recover !",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, Delete it!",
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: "post",
                            url: '../burden/delete/burden',
                            data: {burden_id: id},

                            success: function (response) {
                                if (response == 'true') {
                                    swal("Nice!", "You successfully Deleted the burning", "success");
                                    setTimeout(location.reload.bind(location), 900);
                                } else {
                                    swal("Error!", "Something went wrong", "error");
                                }
                            }
                        });


                        // });

                    } else {
                        swal("Cancelled", "You cancelled the finishing :)", "error");
                    }
                });


        }


    </script>
@stop
