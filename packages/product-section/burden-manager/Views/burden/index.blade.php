@extends('layouts.back.master')@section('title','Burden')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/inbox.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .hide {
            display: none;
        }

        .ml-3 {
            margin-left: 1rem !important;
        }

        .mr-3 {
            margin-right: 10px !important;
        }

        .burden {
            /*border:2px solid;*/
            border-color: white;
            border-radius: 5px;
            padding: 5px;
        }
    </style>
@section('content')
    <div class="app-wrapper">
        <div class="app-panel" id="app-panel">
            <div class="app-search">
                <input type="search" class="search-field" placeholder="Search" disabled>
                <i class="search-icon fa fa-search"></i>
            </div>
            <div class="scroll-container">
                <div class="app-panel-inner px-2 py-2">
                    <div class="inbox-cat-list">

                        @foreach($burdens as $burden)
                            <a class=" panel-item item" value="{{$burden['burden_id']}}" data-target="12"
                               id="dash1-widget-activities"
                               data-toggle="tooltip" data-placement="top"
                               data-original-title="{{$burden['semi']->name}}">
                                <div class="d-flex mr-auto">
                                    <div id="dash1-easypiechart-1" class="dash1-easypiechart-1 chart easypiechart"
                                         data-percent="0}}">
                                        <div class="svg-chart-icon center-icon avatar avatar-circle avatar-sm bg-danger"
                                             data-plugin="firstLitter"
                                             data-target="#100">
                                        </div>
                                    </div>
                                    <p class="display-4 ml-3"
                                       data-plugin="counterUp">{{$burden['kg_per_burden']}}</p>
                                    <p class="mr-3" style="font-size: 1rem;padding-top: 6%">KG</p>
                                    <p id="200}}"
                                       class="float-left mt-2"
                                       style="line-height: 1.25">{{ str_limit($burden['semi']->name, $limit = 10, $end = '...') }}
                                        {{--<br>{{$burden['total_qty']}} units--}}
                                    </p>
                                </div>
                                <div class="row  py-4">
                                    <div class="col-md-6">
                                        <button class="btn btn-outline-primary" type="button" data-toggle="collapse"
                                                data-target="#multiCollapseExample{{$burden['burden_id']}}"
                                                aria-expanded="false"
                                                aria-controls="multiCollapseExample{{$burden['burden_id']}}"
                                                style="width: 100%; border-color: transparent">
                                                <span>
                                                <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                </span> Ingredients
                                        </button>
                                    </div>

                                    <div class="col-md-6">
                                        <button value="Bake" class="btn btn-outline-danger bake"
                                                style="width: 100%; border-color: transparent" name="btnBake"
                                                id="btnbake">
                                            <span>
                                                <i class="fa fa-free-code-camp" aria-hidden="true"></i>
                                            </span>
                                            Bake
                                        </button>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="collapse" id="multiCollapseExample{{$burden['burden_id']}}">
                                            <div class="card-body">
                                                @foreach($burden['materials'] as $item)

                                                    <div class="row burden">
                                                        <div class="col-md-2">
                                                            <span><i class="fa fa-shopping-bag"></i> </span>
                                                        </div>
                                                        <div class="col-md-7">
                                                            {{$item['meterial_name']}}

                                                        </div>
                                                        <div class="col-md-3">
                                                            @if($item['measurement']!='unit')
                                                                @if($item['qty_per_burden']>999)
                                                                    {{$item['qty_per_burden']/1000}} kg
                                                                @else
                                                                    {{$item['qty_per_burden']}} g
                                                                @endif
                                                            @else
                                                                {{$item['qty_per_burden']}} unit
                                                            @endif
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        @endforeach

                        @foreach($baking as $burden)
                            <a class=" panel-item item" value="{{$burden['burden_id']}}" data-target="12"
                               id="dash1-widget-activities"
                               data-toggle="tooltip" data-placement="top"
                               data-original-title="{{$burden['semi']->name}}">
                                <div class="d-flex mr-auto">
                                    <div id="dash1-easypiechart-1" class="dash1-easypiechart-1 chart easypiechart"
                                         data-percent="0}}">
                                        <div class="svg-chart-icon center-icon avatar avatar-circle avatar-sm bg-warning"
                                             data-plugin="firstLitter"
                                             data-target="#100">
                                        </div>
                                    </div>
                                    <p class="display-4 ml-3"
                                       data-plugin="counterUp">{{$burden['kg_per_burden']}}</p>
                                    <p class="mr-3" style="font-size: 1rem;padding-top: 6%">KG</p>
                                    <p id="200}}"
                                       class="float-left mt-2"
                                       style="line-height: 1.25">{{ str_limit($burden['semi']->name, $limit = 10, $end = '...') }}
                                        {{--<br>{{$burden['total_qty']}} units--}}
                                    </p>
                                </div>
                                <div class="row  py-4">
                                    <div class="col-md-12 ">
                                        <button value="{{$burden['burden_id']}}" class="btn btn-outline-danger finish"
                                                type="button" data-toggle="collapse"
                                                style="width: 100%; border-color: transparent">
                                                <span>
                                                <i class="fa fa-free-code-camp" aria-hidden="true"></i>
                                                </span> Finish Bake
                                        </button>
                                    </div>
                                </div>
                            </a>
                        @endforeach

                        @foreach($finished as $burden)
                            <a class=" btn panel-item item" value="{{$burden['burden_id']}}" data-target="123"
                               id="dash1-widget-activities"
                               data-toggle="tooltip" data-placement="top"
                               data-original-title="{{$burden['semi']->name}}">
                                <div class="d-flex mr-auto">
                                    <div id="dash1-easypiechart-1" class="dash1-easypiechart-1 chart easypiechart"
                                         data-percent="0}}">
                                        <div class="svg-chart-icon center-icon avatar avatar-circle avatar-sm bg-info"
                                             data-plugin="firstLitter"
                                             data-target="#100">
                                        </div>
                                    </div>
                                    <p class="display-4 ml-3"
                                       data-plugin="counterUp">{{$burden['kg_per_burden']}}</p>
                                    <p class="mr-3" style="font-size: 1rem;padding-top: 6%">KG</p>
                                    <p id="200}}"
                                       class="float-left mt-2"
                                       style="line-height: 1.25">{{ str_limit($burden['semi']->name, $limit = 10, $end = '...') }}
                                        {{--<br>{{$burden['total_qty']}} units--}}
                                    </p>
                                </div>
                                <div class="row  py-4">
                                    <div class="col-md-12 ">
                                        <button value="{{$burden['burden_id']}}" class="btn btn-outline-danger grade" type="button"
                                                data-toggle="modal" data-target="#wizard-modal"
                                                style="width: 100%; border-color: transparent" id="btnGrade">
                                                <span>
                                                <i class="fa fa-free-code-camp" aria-hidden="true"></i>
                                                </span> Add Grades
                                        </button>
                                    </div>
                                </div>
                            </a>
                        @endforeach

                    </div>
                </div>
            </div>
            <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show">
                <i class="fa fa-chevron-right"></i>
                <i class="fa fa-chevron-left"></i>
            </a>
        </div>

        <div class="row">
            <div class="col-xs-6 col-md-6" style="width:500px">
                <div class="app-main">
                    <header class="app-main-header" style="width: 500px; padding-right:180px">
                        <h5 id="item_name" class="app-main-heading text-center">SELECT EMPLOYEES </h5>
                    </header>
                    <div class="scroll-container">
                        <div class="app-main-content p-3" id="connected">
                            <div id="content scroll-container">
                                {{ Form::open(array('url' => 'burden','enctype'=>'multipart/form-data','name'=>'myForm','id'=>'myForm'))}}
                                <input type="hidden" name="burden_id" id="semi_product" value="">
                                <div class="connected list no2 mail-list " id="2" style="height: 400px; ">
                                </div>
                                {!!Form::close()!!}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-xs-6 col-md-6" style="width: 500px">
                <div class="app-main">
                    <header class="app-main-header">
                        <h5 class="app-main-heading text-center">EMPLOYEES </h5>
                    </header>
                    <div class="scroll-container">
                        <div class="app-main-content p-3" id="connected">
                            <div id="content ">
                                <div class="connected list no2 mail-list " id="2">

                                    @foreach($employees as $employee)
                                        <div class="card p-1 bg-faded">
                                            <div class="media bg-white p-3">
                                                @if(isset($employee->image))
                                                    <a href="#" class="avatar avatar-circle avatar-md">
                                                        <img src="{{asset($employee->image_path.$employee->image)}}"
                                                             alt="">
                                                    </a>
                                                @else
                                                    @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                                    <a href="#"
                                                       class="avatar avatar-circle avatar-md bg-{{$color[$employee->color]}} "
                                                       data-plugin="firstLitter"
                                                       data-target="#raw-{{$employee->id*874}}">
                                                    </a>
                                                @endif
                                                <div class="media-body">
                                                    <h6 id="raw-{{$employee->id*874}}">{{$employee->first_name.' '.$employee->last_name}}</h6>
                                                    <div>{{$employee['designation']->name}}</div>
                                                </div>
                                            </div>
                                            <input type="hidden" name="emp_id[]" value="{{$employee->id}}">
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade wizard-modal-lg" id="wizard-modal" tabindex="-1" role="dialog"
         aria-labelledby="image-gallery-modal" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-body p-5">
                    <div class="text-center" style="margin-bottom: 4.5rem"><h4>Choose the Grades for Bun Burden</h4>
                    </div>
                    {{ Form::open(array('url' => 'burden/grade/save','enctype'=>'multipart/form-data'))}}
                    <input type="hidden" name="burden_id" id="bureden_id">
                        <div class="row p4">

                            <div class="col-md-6 px-3" style="width: 50%">
                                Expected Units:12
                            </div>
                            <div class="col-md-3 px-4" style="width: 30%">
                                Recieved Units:
                            </div>

                            <div class="col-md-3 px-4" style="width: 20%">
                                <input type="text" class="abc info form-control w-20" id="avalableQTY"
                                       name="avalableQTY"/>
                            </div>

                        </div>


                        <div class="project-tasks">
                            <div class="project-task ">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="checkbox checkbox-circle checkbox-lg ">
                                            <input disabled id="pj-task-1" name="ungradede"  type="checkbox" checked>
                                            <label for="pj-task-1">UnGraded</label>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <input type="text" disabled name="ungraded" class="info form-control" id="ungraded"/>
                                    </div>

                                    {{--<div class="col-md-4">--}}
                                        {{--<label type="text" disabled style="float: right">100%</label>--}}
                                    {{--</div>--}}

                                </div>
                            </div>
                            @foreach($grades as $key=> $grade)
                                <div class="project-task ">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="checkbox checkbox-circle checkbox-lg ">
                                                <input id="pj-task-1" name="grade[]" type="checkbox" value="{{$grade->id}}" >
                                                <label for="pj-task-1">{{$grade->name}}</label>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <input type="text" name="gradeQty[]" class="info form-control" id="gradeQty"/>
                                        </div>

                                        {{--<div class="col-md-4">--}}
                                            {{--<label type="text" style="float: right">100%</label>--}}
                                        {{--</div>--}}

                                    </div>
                                </div>
                            @endforeach
                        </div>


                        <input type="submit" id="next-btn" class="next btn btn-success w-50" value="save">


                    {!!Form::close()!!}
                </div><!-- /.modal-body -->
            </div><!-- /.modal-content -->
        </div>
    </div>

@stop
@section('js')
    <script src="{{asset('assets/vendor/bower_components/sortable/jquery.sortable.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>



    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $(document).ready(function () {
            $(".dash1-easypiechart-1").easyPieChart({
                barColor: $.colors.success,
                scaleLength: !1,
                trackColor: "#f7f7f7",
                size: 80
            })

            $('#avalableQTY').keyup(function (e) {
                var qty = $(this).val();
                $('#ungraded').val(qty);
            });

        });

        $('#btnGrade').click(function (e) {
            var id = $(this).val();
            $('#bureden_id').val(id);
        });

        $('.btn').click(function () {
            jQuery('.abc').addClass('hide')
            var target = '#' + $(this).data('target');
            $(target).removeClass('hide');

        });

        $(".bake").click(function (e) {
            e.preventDefault();
            confirmAlert();
        });

        $(".item").click(function (e) {
            e.preventDefault();
            id = $(this).attr("value");
            $("#semi_product").val(id);
        });

        $(".finish").click(function (e) {
            e.preventDefault();
            id = $(this).attr("value");
//            $("#semi_product").val(id);
            finishBake(id)
        });

        function finishBake(id) {
            swal({
                    title: "Are You Sure ?",
                    text: "Smell is good enough to grade A ?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#60c84c',
                    confirmButtonText: "Yes, Finish !",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "put",
                        url: 'burden/' + id,
                        data: $("#myForm").serialize(),
                        success: function (response) {
                            if (response == 'true') {
                                swal("Nice Job !", "Bake finished Success", "success");
                                setTimeout(location.reload.bind(location), 900);
                            }
                            else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );
        }

        function confirmAlert() {
            swal({
                    title: "All Right ?",
                    text: "Your will not be able to undo this proccess!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#ff7473',
                    confirmButtonText: "Yes, Bake !",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "post",
                        url: '',
                        data: $("#myForm").serialize(),
                        success: function (response) {
                            if (response == 'true') {
                                swal("WoW !", "Bake Started ", "success");
                                setTimeout(location.reload.bind(location), 900);
                            }
                            else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

        $(function () {
            $('.sortable').sortable();
            $('.handles').sortable({
                handle: 'span'
            });
            $('.connected').sortable({
                connectWith: '.connected'
            });
            $('.exclude').sortable({
                items: ':not(.disabled)'
            });
        });
    </script>
@stop



