@extends('layouts.back.master')@section('title','Burden History')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/clockpicker/dist/bootstrap-clockpicker.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}"/>


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .hide {
            display: none;
        }

        .app-main-content {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .portlet-placeholder {
            border: 1px dotted black;
            margin: 0 1em 1em 0;
            height: 100%;
        }

        #projects-task-modal .modal-bg {
            padding: 50px;
        }

        #projects-task-modal .modal-content {
            box-shadow: none;
            border: none
        }

        #projects-task-modal .close {
            border: none;
            outline: none;
            box-shadow: none;
            position: absolute;
            right: 1rem;
            top: 1rem;
            width: 25px;
            height: 25px;
            line-height: 25px;
            text-align: center;
            font-size: 1.13rem;
            border-radius: 500px;
            border: 1px solid #868e96
        }

        #floating-button {
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #db4437;
            position: fixed;
            top: 13rem;
            right: 90px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .plus {
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }

        #container {
            position: relative;
            padding: 0px;
            width: 100%;
            height: 300px;
            overflow: auto;
        }

        #container .content {
            width: 100%;
            height: 50%;
        }


    </style>
@stop
@section('content')
    <div class="app-wrapper">
        <div class="app-main">
            <div class="scroll-container" id="scroll-container">
                {{ Form::open(array('url' => 'burden/analyse'))}}
                {{--<div class="container">--}}
                    <div class="card " id="dash1-widget-activities">
                        <div class="card-header align-items-center p-3">
                            <div class="col-md-5">
                            <div class="form-group"><label for="daterange-ex-4">Date Range</label>
                                <input name="date" id="daterange-ex-4" class="float-right"
                                     style="background: #fff; cursor: pointer;     height: calc(2.25rem + 2px); padding: 5px 10px; border: 1px solid #ccc; width: 100%; border-radius:.25rem; ">
                                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                    <span id="date"></span>
                                </input>
                            </div>
                        </div>
                            <div class="col-md-4">
                            <div class="form-group my-2">
                                <label for="single">Semi products</label>
                                <select id="single" name="Single"
                                        {{--onChange="selectIngredients(this);"--}}
                                        class="form-control select2 " ui-jp="select2"
                                        style="width: 100%"
                                        ui-options="{theme: 'bootstrap'}">
                                    {{--<option>Select Semi finished product</option>--}}
                                    <option value="0">Select Product</option>
                                    @foreach($semiproducts as $semi)
                                        <option value=" {{$semi->semi_finish_product_id}}">{{$semi->name}}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>
                            <div class="col-md-3">
                                <div class="text-center" style="margin-top: 28px; cursor: pointer; margin-left:50px; margin-right: 50px">
                                    <div id="history" class="btn btn-primary btn-md btn-block"> View </div>
                                    <button class="btn btn-success btn-md btn-block"> Advanced </button>
                                </div>
                            </div>
                        </div>
                    {{--</div>--}}
                    {{--<input type="date" name="start_date">--}}

                    {{--<input type="date" name="end_date">--}}
                    {{--<button type="submit" class="btn btn-primary btn-lg btn-block">History</button>--}}
                </div>
                {!!Form::close()!!}

                <div class=" projects-list " id="container">
                    <div class="packing content col-md-12">

                    </div>

                </div>


            </div>

        </div>

    </div>

@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/moment/min/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/clockpicker/dist/bootstrap-clockpicker.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>    <script src="{{asset('assets/vendor/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    {{--<script src="{{asset('assets/examples/js/demos/forms.datetime.js')}}"></script>--}}


    <script>
        var ps = new PerfectScrollbar('#container');

        function t(t, e) {
            $("#daterange-ex-4 span").html(t.format("YYYY-M-D ") + " - " + e.format("YYYY-M-D"))
        }

        var e = moment().subtract(29, "days"), n = moment();
        $("#daterange-ex-4").daterangepicker({
            startDate: e,
            endDate: n,
            ranges: {
                Today: [moment(), moment()],
                Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
                "Last 7 Days": [moment().subtract(6, "days"), moment()],
                "Last 30 Days": [moment().subtract(29, "days"), moment()],
                "This Month": [moment().startOf("month"), moment().endOf("month")],
                "Last Month": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
            }
        }, t), t(e, n)

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $('.delete').click(function (e) {
            e.preventDefault();
            id = $(this).attr("value");
            confirmAlert(id);

        });

        // function confirmAlert(id) {
        //     swal({
        //             title: "Are you sure?",
        //             text: "Your will not be able to recover this imaginary file!",
        //             type: "warning",
        //             showCancelButton: true,
        //             confirmButtonColor: '#3085d6',
        //             confirmButtonText: "Yes, delete it!",
        //             closeOnConfirm: false
        //         },
        //         function () {
        //             $.ajax({
        //                 type: "delete",
        //                 url: id,
        //                 success: function (response) {
        //                     if (response == 'true') {
        //                         swal("Deleted!", "Deleted successfully", "success");
        //                         setTimeout(location.reload.bind(location), 900);
        //                     }
        //                     else {
        //                         swal("Error!", "Something went wrong", "error");
        //                     }
        //                 }
        //             });
        //
        //         }
        //     );
        //
        // }

        // $(document).ready(function () {
        //     $("#unit").hide();
        //     $('#pj-task-1').change(function () {
        //         if (this.checked)
        //             $("#unit").show();
        //         else
        //             $("#unit").hide();
        //
        //     });
        // });

        $("#history").click(function () {

            var semi = $("#single option:selected").val();
            var name = $("#single option:selected").text();
            var date = $('#daterange-ex-4').val();
            // var data = 'semi='+ semi  & 'date='+ date;

            $.ajax({
                type: "post",
                url: 'date',
                data: {product: semi, date:date },

                success: function (response) {

                    $(".packing").html('');
                     for(var i=0; i<response.length-1;i++) {
                         var res =response[i];
                         $(".packing").append('<div class="card mx-2">' +
                             ' <a href="' + res['id'] + '" target="_blank">' +
                             '<div class="media mx-4 content2">' +
                             '<div class="avatar avatar text-white avatar-md project-icon bg-primary" data-target="#project-1">' +
                             '<i class="fa fa-fire" aria-hidden="true"></i>' +
                             '</div>' +
                             '<div class="media-body">' +
                             '<h6 class="project-name" id="project-1">' + res['name'] + '</h6>' +
                             '<small class="project-detail">Size :' + res['burden_size'] + '</small>' +
                             '<div>' +
                             '<small class="project-detail">Quantity:' + res['expected_semi_product_qty'] + '</small>' +
                             '</div>' +
                             '</div>' +
                             '<button class="btn btn-outline-primary" style="width:6rem">' +
                             '<i class="fa fa-fire" aria-hidden="true"></i>Show </button>' +
                             '</div>' +
                             '<hr class="m-0">' +
                             '</a>' +
                             '</div>');
                     }

                }
            });

            reset();
        });

        function reset() {
            $(".packing").append('');

        }

        // $("#single").change(function () {
        //
        //     var a = $("#single option:selected").val();
        //
        //
        //     $.ajax({
        //         type: "post",
        //         url: '../po/getlastprice',
        //         data: {id: a},
        //
        //         success: function (response) {
        //             $(".lastprice").empty();
        //             $(".lastprice").append(response['receieved_price'])
        //         }
        //     });
        //
        // });

    </script>
@stop
