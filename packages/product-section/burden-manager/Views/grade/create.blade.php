@extends('layouts.back.master')@section('title','Grade')
@section('css')
    {{--<link rel="stylesheet" href="{{asset('assets/examples/css/demos/demos.css')}}">--}}
    {{--<link rel="stylesheet" href="{{asset('assets/css/site.css')}}">--}}

    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.wizard.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/css/style.css')}}">

    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <style>
        .t-name-wrap span {
            display: inline-block;
            width: 32px;
            height: 32px;
            line-height: 32px;
            text-align: center;
            border-radius: 500px;
            position: absolute;
            left: 0;
            top: calc(50% - 15px);
            border: 1px solid #ccc;
            font-size: 1.5rem;
            color: #ccc
        }

        .t-name-field {
            padding-left: 42px;
            height: 60px;
            font-size: 1.75rem;
            font-weight: 500;
            border: none;
            outline: none;
            box-shadow: none;
            width: 100%;
            color: #868e96
        }
    </style>
@stop
@section('content')

vk

@stop
@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/js/jquery.sortable.js')}}"></script>
    <script src="{{asset('assets/examples/js/demos/form.wizard.js')}}"></script>
    <script>
        $(function () {
            $('.sortable').sortable();
            $('.handles').sortable({
                handle: 'span'
            });
            $('.connected').sortable({
                connectWith: '.connected'
            });
            $('.exclude').sortable({
                items: ':not(.disabled)'
            });
        });
    </script>


@stop