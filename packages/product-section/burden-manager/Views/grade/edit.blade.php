@extends('layouts.back.master')@section('title','Burden | Grade Now')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}">


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .app-wrapper {
            font-family: Raleway, sans-serif;
            width: 100%;
            border: 1px solid #eee;
        }

        div.hidden {
            display: none
        }
    </style>
@stop
@section('current','Burden')
@section('current_url',route('burden.create'))
@section('current2','Grade Burden')
@section('current_url2','')
@section('content')
    <div class="app-wrapper">
        <div class="profile-section-user">
            <div class="profile-cover-img profile-pic">
                <img src="{{asset('assets\img\semi2.jpg')}}" alt="">
            </div>
            <div class="profile-info-brief p-3">
                @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                <div class="user-profile-avatar avatar avatar-circle avatar-md project-iconr bg-{{$color[$burden->colour]}}"
                     data-plugin="firstLitter"
                     data-target="#{{$burden->id*754}}"></div>

                <div class="text-center">
                    <h5 class="text-uppercase mb-1" id="{{$burden->id*754}}">{{$burden['semiProdcuts']->name}}</h5>
                    <h6 class="text mb-lg-1">{{'Burden Size : '.$burden->burden_size}} KG</h6>
                    <br>
                </div>

                <hr class="m-0">

                <div class="text-center py-3">
                    {{'Expected Quantity : '.$burden->expected_semi_product_qty}} units<br>
                    <input type="hidden" id="expectedQty" value="{{$burden->expected_semi_product_qty}}">
                    {{'Active Recipe : '.$burden['recipe']->name}}<br>
                    <hr class="m-0">
                    {{$burden['recipe']->desc}}
                </div>
            </div>
            <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show"><i
                        class="fa fa-chevron-right"></i>
                <i class="fa fa-chevron-left"></i>
            </a>
        </div>
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">Add Grades - {{$burden['serial']}}</h5>
            </div>
            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">
                    <div class="container">
                        {{ Form::open(array('url' => 'burden/grade/'.$burden['id'],'id'=>'frmGrade','enctype'=>'multipart/form-data'))}}
                        <input type="hidden" name="burden_id" value="{{$burden['id']}}">
                        <input type="hidden" name="burden_qty" value="{{$burden['expected_semi_product_qty']}}">
                        {{--                        <input type="hidden" name="burden_qty" value="{{$burden['actual_semi_product_qty']}}">--}}
                        <input type="hidden" name="semi_finish_product"
                               value="{{$burden['semiProdcuts']->semi_finish_product_id}}">
                        <input type="hidden" name="weightSemi" id="weightSemi" value="{{$weight}}">
                        <div class="media-list">
                            @foreach($grades as $grade)
                                <div class="media">
                                    <div class="avatar avatar-circle avatar-md project-icon bg-success"
                                         data-plugin="firstLitter"
                                         data-target="12">{{$grade->name}}</div>
                                    <div class="media-body">

                                        <h6 class="project-name" id="12">{{$grade->name}}</h6>
                                        <input type="hidden" name="grade[]" class="g-c" value="{{$grade->id}}">
                                    </div>
                                    {{--<div class="col-md-3">--}}
                                    {{--<input type="checkbox" name="ck[]" class="one" data-plugin="switchery"--}}
                                    {{--data-size="medium" value="{{$grade->id}}">--}}
                                    {{--</div>--}}
                                    <div class="col-sm-3">
                                        @if($grade->id == 3)
                                            <div class=" d-flex activity-counters justify-content-between">

                                                <div class="text-center px-2">
                                                    <label for="single">Weight</label>
                                                    <input class="form-control updateD input-grade" id="weight-{{$grade->id}}"
                                                           name="weight"
                                                           value="0" placeholder="" type="text" style="width:100px" readonly>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-sm-3" id="aa{{$grade->id}}">
                                        <div class=" d-flex activity-counters justify-content-between">

                                            <div class="text-center px-2">
                                                <label for="single">Quantity</label>
                                                <input  class="form-control updateD input-grade" id="qty-{{$grade->id}}"
                                                        name="quantity[]"
                                                        value="0" required type="text" style="width:100px"
                                                        @if($reusable_type == 1 && $grade->id == 3)
                                                        readonly
                                                        @endif
                                                >
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            @endforeach
                        </div>
                        <button type="submit" id="submit" value=true class="btn btn-success btn-lg btn-block submit"> Add Grades</button>
                        {!!Form::close()!!}
                    </div>
                </div>

            </div>
        </div>
    </div>

@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>

    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            var expected = jQuery("#expectedQty").val();
            $('#qty-4').val(expected);

        });


        $(".updateD").keyup(function () {
            var expected = jQuery("#expectedQty").val();

            var gradeA = jQuery("#qty-1").val();
            var gradeB = jQuery("#qty-2").val();
            var gradeC = jQuery("#qty-3").val();
            var gradeD = jQuery("#qty-4").val();

            gradeD = parseFloat(expected) - (parseFloat(gradeA) + parseFloat(gradeB) + parseFloat(gradeC));

            $('#qty-4').val(gradeD);

        });

        /*   $('.one').change(function (e) {
               var id = $(this).attr("value");
               if ($(this).is(":checked")) {
                   $("#aa" + id).removeClass("hidden");
               } else {
                   $("#aa" + id).addClass("hidden");

               }
           });*/


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });



        $(".submit").click(function (e) {
            e.preventDefault();
            let expected = jQuery("#expectedQty").val();
            let inputs = $(".input-grade");
            let total = 0;
            for (let i = 0; i < inputs.length; i++) {
                total = parseFloat(total) + parseFloat($(inputs[i]).val())
            }

            let dif = parseFloat(expected).toFixed(2) - total.toFixed(2);

            let A = jQuery("#qty-1").val();
            let B = jQuery("#qty-2").val();
            let C = jQuery("#qty-3").val();
            let D = jQuery("#qty-4").val();

            var CC=Number(document.getElementById("qty-3").value);
            var DD=Number(document.getElementById("qty-4").value);
            var EX=Number(document.getElementById("expectedQty").value);
            var tot = CC + DD ;

            var dam = (tot)/(EX);


            if (dif < 1 || !(dif < 0)) {
                e.preventDefault();
                id = $(this).attr("value");
                if(dam > 0.1) {
                    console.log(id)
                    confirmAlert1(A, B, C, D, expected,dam);
                    console.log('confirmAlert1')
                }else{
                    console.log(id)
                    confirmAlert(A, B, C, D, expected );
                    console.log('confirmAlert')
                }
            }else {
                alert("Grade amount not match to expected");
            }
        });



        function confirmAlert1( A , B , C , D , expected,dam) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            swal({
                    title: "Are you sure?",
                    text: "Grage A = " + ((A*100)/(expected)).toFixed(2)  + " %\n" +
                        "Grage B = " + ((B*100)/(expected)).toFixed(2) + " %\n" +
                        "Grage C = " + ((C*100)/(expected)).toFixed(2) + " %\n" +
                        "Grage D = " + ((D*100)/(expected)).toFixed(2) + " %\n" +
                        " \n" +
                       // "Grade C & D have " + (dam*100).toFixed(2) + " % Wastage",
                        "Grade C & D have " + (dam*100).toFixed(2) + " % Wastage",


                    icon: "danger",
                    showCancelButton: true,
                    confirmButtonColor: '#bf3232',
                    confirmButtonText: "Oh my god ..! Is This OK...?",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "post",
                        icon: "warning",
                        url: '/burden/grade/{{$burden->id}}',
                        // data:{
                        {{--burden_id:{{$burden->id}},--}}
                        {{--quantity:$('.input-grade').serializeArray(),--}}
                        {{--weight:{{$weight}},--}}
                        {{--grade:$('.g-c').val(),--}}
                        {{--semi_finish_product:{{$burden['semiProdcuts']->semi_finish_product_id}},--}}
                        {{--burden_qty:{{$burden['expected_semi_product_qty']}},--}}
                        // },
                        data:$('#frmGrade').serializeArray(),
                        success: function (response) {
                            if (response == 'true') {
                                swal("Graded!", "Graded successfully", "success");
                                setTimeout(window.location.href = "/create", 900);
                            } else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

        function confirmAlert( A , B , C , D , expected) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            swal({
                    title: "Are you sure?",
                    text: "Grage A = " + ((A*100)/(expected)).toFixed(2)  + " %\n" +
                        "Grage B = " + ((B*100)/(expected)).toFixed(2) + " %\n" +
                        "Grage C = " + ((C*100)/(expected)).toFixed(2) + " %\n" +
                        "Grage D = " + ((D*100)/(expected)).toFixed(2) + " %\n" ,

                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, It's OK...",
                    closeOnConfirm: false

                },
                function () {
                    $.ajax({
                        type: "post",
                        icon: "warning",
                        url: '/burden/grade/{{$burden->id}}',
                        // data:{
                        {{--burden_id:{{$burden->id}},--}}
                        {{--quantity:$('.input-grade').serializeArray(),--}}
                        {{--weight:{{$weight}},--}}
                        {{--grade:$('.g-c').val(),--}}
                        {{--semi_finish_product:{{$burden['semiProdcuts']->semi_finish_product_id}},--}}
                        {{--burden_qty:{{$burden['expected_semi_product_qty']}},--}}
                        // },
                        data:$('#frmGrade').serializeArray(),
                        success: function (response) {
                            if (response == 'true') {
                                swal("Graded!", "Graded successfully", "success");
                                setTimeout(window.location.href = "/create", 900);
                            } else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }



        $("#qty-3").keyup(function(){
            var weightSemi = jQuery("#weightSemi").val();
            var qty = jQuery("#qty-3").val();
            if(qty == '')
            {
                $('#weight-3').val(0);
            }else{
                var multipleValue = parseFloat(weightSemi)*parseFloat(qty);
                $('#weight-3').val(multipleValue);
            }

        });

    </script>
@stop
