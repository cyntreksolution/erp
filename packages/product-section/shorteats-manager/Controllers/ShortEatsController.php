<?php

namespace ShortEatsManager\Controllers;

use App\Exports\BurdenExport;
use Barryvdh\DomPDF\Facade as PDF;
use BurdenManager\Models\BurdenGroupEmployee;
use EmployeeManager\Models\Group;
use EmployeeManager\Models\GroupEmployee;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use ShortEatsManager\Classes\BurdenContent;
use ShortEatsManager\Models\Burden;
use ShortEatsManager\Models\BurdenEmployee;
use ShortEatsManager\Models\BurdenGrade;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use RawMaterialManager\Models\RawMaterial;
use RecipeManager\Classes\RecipeContent;
use RecipeManager\Models\Recipe;
use Sentinel;
use EmployeeManager\Models\Employee;
use GradeManager\Models\Grade;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use RecipeManager\Models\RecipeSemiProduct;
use SemiFinishProductManager\Models\SemiFinishProduct;
use ShortEatsManager\Models\CookingRequest;
use ShortEatsManager\Models\CookingRequestEmployee;
use ShortEatsManager\Models\CookingRequestGroupEmployee;
use StockManager\Classes\StockTransaction;
use StockManager\Models\StockSemiFinish;

class ShortEatsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:short_eats-index', ['only' => ['index','create']]);
        $this->middleware('permission:NewFillingRequest-list', ['only' => ['list']]);
        $this->middleware('permission:short_eats-newIngrediensStore', ['only' => ['store']]);
        $this->middleware('permission:short_eats-reUsableWestage', ['only' => ['create']]);
        $this->middleware('permission:short_eats-printList', ['only' => ['printList']]);
        $this->middleware('permission:short_eats-delete', ['only' => ['destroy']]);
        $this->middleware('permission:short_eats-create', ['only' => ['bake']]);
        $this->middleware('permission:short_eats-received', ['only' => ['edit']]);
        $this->middleware('permission:short_eats-report', ['only' => ['burdenListShow','tableData']]);
    }
    public function index()
    {

        return redirect(route('shorteats.create'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function fstage(Request $request)
    {
        $id=$request->burden_id;
        $data = CookingRequest::find($id);
        $data->responce = 0;
        $data->save();
        return redirect()->back();
    }

    public function sstage(Request $request)
    {

        $id=$request->burden_id;
        $data = CookingRequest::find($id);
        $data->responce = 1;
        $data->save();
        return redirect()->back();
    }

    public function create()
    {
        $semiAll = SemiFinishProduct::whereIsBake(2)->get();
        $products = RawMaterial::all();
        $recipes = Recipe::take(4)->get();
        $semi = DB::table('recipe_semi_finish_product')
            ->join('semi_finish_product', 'recipe_semi_finish_product.semi_finish_product_semi_finish_product_id', '=', 'semi_finish_product.semi_finish_product_id')
            ->where('recipe_semi_finish_product.status', '=', 2)
            ->where('semi_finish_product.is_bake', '=', 2)
            ->get();
        $grade = Grade::all();
        $burden = CookingRequest::where('status', '=', 1)
            ->with('semiProdcuts')
            ->get();

        $burdenStockRecieved = CookingRequest::where('status', '=', 2)
            ->with('semiProdcuts')
            ->get();

        $creatingBurdens = CookingRequest::where('status', '=', 3)
            ->with('semiProdcuts')
            ->get();


        $bakingBurdens = CookingRequest::where('status', '=', 4)
            ->with('semiProdcuts')
            ->get();

        $finishBurdens = CookingRequest::where('status', '=', 5)
            ->with('semiProdcuts')
            ->get();

        return view('ShortEatsManager::burden.create')->with([
            'semiProducts' => $semi,
            'recievedBurdens' => $burdenStockRecieved,
            'bakingBurdens' => $bakingBurdens,
            'finishBurdens' => $finishBurdens,
            'creatingBurdens' => $creatingBurdens,
            'burdens' => $burden,
            'grades' => $grade,
            'semi' => $semiAll,
            'recipes' => $recipes,
            'products' => $products,
        ]);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $semi_finish_product = $request->semi_finish_product;
        $active_recipe = RecipeContent::getActiveRecipe($semi_finish_product)->recipe_id;
        $burden_size = $request->burden_size;
        $burden_count = 1;
        $expected_semi_qty = RecipeContent::getExpectedQty($semi_finish_product) * $burden_size;


        $in = DB::select("show table status like 'cooking_requests'");

        $burdenCount = CookingRequest::whereDate('created_at', '=', Carbon::today()->toDateString())->get()->count();
        $last_digit = $burdenCount + 1;
        $serial = 'BR-'.date('ymd') . '-' . $last_digit;
        $burden = new CookingRequest();
        $burden->semi_finish_product_id = $semi_finish_product;
        $burden->serial = $serial;
        $burden->burden_size = $burden_size;
        $burden->used_qty = $expected_semi_qty;
        $burden->expected_semi_product_qty = $expected_semi_qty;
        $burden->recipe_id = $active_recipe;
        $burden->created_by = Auth::user()->id;
        $burden->save();
        //return redirect(route('shorteats.index'))->with([
        return redirect(route('proOrder.index'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => 'new request created successfully'
        ]);
    }


    /**
     * Display the specified resource.
     *
     * @param \ShortEatsManager\Models\Burden $burden
     * @return \Illuminate\Http\Response
     */
    public function show(CookingRequest $cookingRequest)
    {
        $burden = CookingRequest::where('id', '=', $cookingRequest->id)
            ->with('semiProdcuts', 'recipe.recipeContents.materials')
            ->first();
        return view('ShortEatsManager::burden.show')->with([
            'burden' => $burden
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \ShortEatsManager\Models\Burden $burden
     * @return \Illuminate\Http\Response
     */
    public function edit(CookingRequest $cookingRequest)
    {
        $employers = Group::whereDate('created_at', '=', Carbon::today()->toDateString())->get();
        return view('ShortEatsManager::burden.edit')->with([
            'burden' => $cookingRequest,
            'employers' => $employers,
        ]);
    }

    public function bake(CookingRequest $cookingRequest)
    {
        $employers = Employee::all();
        return view('ShortEatsManager::burden.bake')->with([
            'burden' => $cookingRequest,
            'employers' => $employers,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \ShortEatsManager\Models\Burden $burden
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CookingRequest $burden)
    {
        $team = $request->team;
        try {
            DB::transaction(function () use ($team, $burden) {

                $t = new CookingRequestGroupEmployee();
                $t->cooking_request_id = $burden->id;
                $t->group_id = $team;
                $t->save();

                $burden->status = 3;
                $burden->save();
            });

            return redirect(route('shorteats.index'))->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => ''
            ]);
        } catch (Exception $e) {
            return $e;
            return redirect(route('burden.index'))->with([
                'error' => true,
                'error.title' => 'Sorry !',
                'error.message' => 'Something Went Wrong '
            ]);
        }
    }

    public function baekNow(Request $request, CookingRequest $cookingRequest)
    {
        $burden = $cookingRequest;
        $employee = $request->employee;
        try {
            DB::transaction(function () use ($employee, $burden) {
                $burden->baker = $employee;
                $burden->status = 6;
                $burden->actual_semi_product_qty = $burden->expected_semi_product_qty;
                $burden->save();

                StockTransaction::semiFinishStockTransaction(1,$burden->semi_finish_product_id,'',$burden->actual_semi_product_qty,1,1);
                StockTransaction::updateSemiFinishAvailableQty(1,$burden->semi_finish_product_id,$burden->actual_semi_product_qty);
            });

            if ($this->saveqty($cookingRequest)){
                return redirect(route('shorteats.index'))->with([
                    'success' => true,
                    'success.title' => 'Congratulations !',
                    'success.message' => 'baking started'
                ]);
            }
        } catch (Exception $e) {
            return $e;
            return redirect(route('burden.index'))->with([
                'error' => true,
                'error.title' => 'Sorry !',
                'error.message' => 'Something Went Wrong '
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param CookingRequest $cookingRequest
     * @return bool
     * @throws \Exception
     */
    public function destroy(Request $request)
    {
        $cooking_request_id = $request->burden_id;
        $cookingRequest = CookingRequest::whereId($cooking_request_id)->first();
        $cookingRequest->delete();
        if ($cookingRequest->trashed()) {
            return 'true';
        } else {
            return 'false';
        }
    }

    public function grade(Request $request)
    {
//        $burden_id = $request->burden_id;
//        $available_qty = $request->avalableQTY;
//        foreach ($request->grade as $key => $gr) {
//            $grade = $gr;
//            $gradeQty = $request->gradeQty[$key];
//            $burdebnGrade = new BurdenGrade();
//            $burdebnGrade->burden_id = $burden_id;
//            $burdebnGrade->grade_id = $grade;
//            $burdebnGrade->burden_qty = $gradeQty;
//            $burdebnGrade->save();
//        }
//        $bd = Burden::find($burden_id);
//        $bd->status = 4;
////        $bd->actual_qty = $available_qty;
//        $bd->save();
//
//        foreach ($request->grade as $key => $gr) {
//            $grade = $gr;
//            $gradeQty = $request->gradeQty[$key];
//            $stockTransaction = new StockSemiFinish();
//            $stockTransaction->stock_id = 2;
//            $stockTransaction->semi_finish_product_id = $bd->semi_finish_product_id;
//            $stockTransaction->grade_id = $grade;
//            $stockTransaction->qty = $gradeQty;
//            $stockTransaction->save();
//        }
//
//
//        return redirect(route('burden.index'))->with([
//            'success' => true,
//            'success.title' => 'Congratulations !',
//            'success.message' => 'Garde Added',
//        ]);
    }

    public function semiFinishProductRecipeAndCount(Request $request)
    {
        $semi_finish_product = $request->id;
        $res = RecipeSemiProduct::where('semi_finish_product_semi_finish_product_id', '=', $semi_finish_product)->
        where('status', '=', 2)
            ->first();
        $semi = SemiFinishProduct::find($semi_finish_product);

        $req = [
            'recipe' => $res->recipe_id,
            'units_per_recipe' => $res->units,
            'name' => $semi->name,
        ];
        return $req;
    }

    public function saveqty(CookingRequest $cookingRequest)
    {
        $burden = $cookingRequest;
        $burden->actual_semi_product_qty = $burden->expected_semi_product_qty;
        $burden->status = 6;
        $burden->save();

        $stockTransaction = new StockSemiFinish();
        $stockTransaction->stock_id = 1;
        $stockTransaction->semi_finish_product_id = $burden->semi_finish_product_id;
        $stockTransaction->qty = $burden->actual_semi_product_qty;
        $stockTransaction->save();

        $semi = SemiFinishProduct::where('semi_finish_product_id', '=', $stockTransaction->semi_finish_product_id)->first();
        $semi->available_qty = $stockTransaction->qty;
        $semi->save();


        return 'true';
    }

    public function dateReport()
    {

        $semi = SemiFinishProduct::all();
        return view('ShortEatsManager::burden.date')->with([
            'semiproducts' => $semi,
        ]);


    }

    public function getReport(Request $request)
    {

        $start_date = substr($request->date, 0, 10);
        $start_date = date_create($start_date);
        $start_date = date_format($start_date, "Y-m-d");

        $end_date = substr($request->date, 12, 23);
        $end_date = date_create($end_date);
        $end_date = date_format($end_date, "Y-m-d");

        $product = $request->product;
        if ($product == 0) {
            $burdens = Burden::whereBetween('created_at', array($start_date, $end_date))->with('semiProdcuts')->get();
            $array = [];
            foreach ($burdens as $burden) {
                $burdenx = [
                    'id' => $burden['id'],
                    'burden_size' => $burden['burden_size'],
                    'expected_semi_product_qty' => $burden['expected_semi_product_qty'],
                    'name' => $burden['semiProdcuts']->name,
                ];
                array_push($array, $burdenx);
            }
            return $array;

        } else {
            $burdens = Burden::whereBetween('created_at', array($start_date, $end_date))->with('semiProdcuts')
                ->where('semi_finish_product_id', '=', $product)->get();
            $array = [];
            foreach ($burdens as $burden) {
                $burdenx = [
                    'id' => $burden['id'],
                    'burden_size' => $burden['burden_size'],
                    'expected_semi_product_qty' => $burden['expected_semi_product_qty'],
                    'name' => $burden['semiProdcuts']->name,
                ];
                array_push($array, $burdenx);
            }
            return $array;
        }
    }

    public function analyse(Request $request)
    {
//        return $request;
        $start_date = substr($request->date, 0, 10);
        $start_date = date_create($start_date);
        $start_date = date_format($start_date, "Y-m-d");

        $end_date = substr($request->date, 12, 23);
        $end_date = date_create($end_date);
        $end_date = date_format($end_date, "Y-m-d");

        $product = $request->Single;

        $dates = $this->generateDateRange(Carbon::parse($start_date), Carbon::parse($end_date));
        $burdensProducts = Burden::whereBetween('created_at', array($start_date, $end_date))->with('semiProdcuts')->groupBy('semi_finish_product_id')->get();
        $burdensProducts = Burden::whereBetween('created_at', array($start_date, $end_date))->selectRaw('*, sum(burden_size) as sum')->with('semiProdcuts')->groupBy('semi_finish_product_id')->get();

        $productNames = [];
        foreach ($burdensProducts as $key => $burdensProduct) {
            foreach ($dates as $key => $date) {
                $burdenCount = Burden::where('semi_finish_product_id', $burdensProduct->semi_finish_product_id)->whereDate('created_at', $date)->get()->count();
                $dte[$key] = $burdenCount;
            }
            $name = [
                'name' => $burdensProduct['semiProdcuts']->name,
                'date' => $dte,
                'sum' => $burdensProduct->sum
            ];
            array_push($productNames, $name);
        }

        if ($product == 0) {
            return view('ShortEatsManager::burden.analyse')->with([
                'dates' => $dates,
                'productNames' => $productNames,
            ]);
        } else {
            return view('ShortEatsManager::burden.analyseProduct');
        }

    }

    private function generateDateRange(Carbon $start_date, Carbon $end_date)
    {
        $dates = [];

        for ($date = $start_date; $date->lte($end_date); $date->addDay()) {
            $dates[] = $date->format('Y-m-d');
        }

        return $dates;
    }


    public function bakeList(Request $request)
    {
        $burdens = Burden::whereStatus(3)->get();
        return view('ShortEatsManager::burden.toBake', compact('burdens'));
    }

    public function burdenListShow(Request $request)
    {
        $semiFinish = SemiFinishProduct::whereIsBake(2)->get()->pluck('name', 'semi_finish_product_id');
        return view('ShortEatsManager::burden.list', compact('semiFinish'));
    }

    public function tableData(Request $request)
    {
        $user = Auth::user();
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id','serial','semi_finish_product_id','status','burden_size','recipe_id','created_at'];
        $order_column = $columns[$order_by[0]['column']];

        $filterCount =0;

        $burdens = CookingRequest::tableData($order_column, $order_by_str, $start, $length);

        $stat_data = CookingRequest::query();



        if ($request->filter == true) {
            $semi = $request->semi;
            $status = $request->status;
            $date_range = $request->date_range;
            $date_type = $request->date_type;
            $burdens = $burdens->filterData($semi, $status, $date_range,$date_type)->get();
            $filterCount = CookingRequest::filterData($semi, $status, $date_range,$date_type)->count();
            $burdenCount = CookingRequest::all()->count();

            $stat_data= $stat_data->filterData($semi, $status, $date_range)->select(DB::raw('SUM(burden_size) as  burden_size'))
                ->first();

        } elseif (is_null($search) || empty($search)) {
            $burdens = $burdens->get();
            $stat_data= $stat_data->select(DB::raw('SUM(burden_size) as  burden_size'))
                ->first();
            $burdenCount = CookingRequest::all()->count();
        } else {
            $burdens = $burdens->searchData($search)->get();
            $burdenCount = $burdens->count();
        }

        $data=[];
        $i = 0;
        $burden_size=0;

        foreach ($burdens as $key => $burden) {
            $teamck = null;
            if($date_type == 2) {
                $groups = Group::find($burden->employee_group);
                $teamck = $serial = "<a target='_blank' href='#'>$groups->name</a>";
            }elseif ($date_type == 3){
                $groups = Group::find($burden->baker);
                $teamck = $serial = "<a target='_blank' href='#'>$groups->name</a>";
            }

            $btnView = null;
            $btnDelete = null;
            if (\Cartalyst\Sentinel\Laravel\Facades\Sentinel::check()->roles[0]->slug == 'owner' ) {
                $btnView = '<a class="btn btn-outline-info btn-sm mr-1" target="_blank" href="' . route('burden.show', $burden->id) . '"> <i class="fa fa-eye"></i> </a>';
                if ($status == 1) {
                    $btnDelete = ($burden->status == 1) ? '<button class="btn btn-outline-danger btn-sm mr-1" onclick="deleteBurden(' . $burden->id . ')"> <i class="fa fa-trash"></i> </button>' : null;
                }
            }
            switch ($burden->status) {
                case 1:
                    $statusBtn = '<button class="btn btn-outline-warning btn-sm mr-1">pending</button>';
                    break;

                case 2:
                    $statusBtn = '<button class="btn btn-outline-info btn-sm mr-1">received</button>';
                    break;

                case 3:
                    $statusBtn = '<button class="btn btn-outline-primary btn-sm mr-1">creating</button>';
                    break;
                case 4:
                    $statusBtn = '<button class="btn btn-outline-danger btn-sm mr-1">burning</button>';
                    break;
                case 5:
                    $statusBtn = '<button class="btn btn-outline-success btn-sm mr-1">grade</button>';
                    break;
                case 6:
                    $statusBtn = '<button class="btn btn-outline-dark btn-sm mr-1">finished</button>';
                    break;
            }


            $url = route('shorteats.historyView',[$burden->id]);
            $serial = "<a target='_blank' href='$url'> $burden->serial</a>";
            $data[$i] = array(
                ++$key,
                $serial,
                $burden->semiProdcuts->name,
                $statusBtn,
                $burden->burden_size . ' KG',
                $burden->recipe->name,
                Carbon::parse($burden->created_at)->format('Y-m-d H:m:s'),
                $btnView .''.$teamck. $btnDelete,
            );
            $i++;
        }

        $burden_size = $stat_data->burden_size;

        if ($burdenCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($burdenCount),
            "recordsFiltered" => intval($filterCount),
            "data" => $data,
            "burden_size" => number_format($burden_size,3),
        ];

        return json_encode($json_data);
    }

    public function exportFilter(Request $request)
    {
        $semi = $request->semi;
        $status = $request->status;
        $date_range = $request->date_range;
        $records = CookingRequest::filterData($semi, $status, $date_range)->get();
        return Excel::download(new BurdenExport($records), Carbon::now().'_cooking_request_report.xlsx');
    }

    public function getBakers($baker){
        return Group::where('id', '=', $baker)->get();
    }

    public function historyView(CookingRequest $burden)
    {


        if (!empty($burden->employee_group)) {
            $employee_group=GroupEmployee::where('group_id', $burden->employee_group)->first();
            $date_string = Carbon::parse($employee_group->created_at)->format('Y-m-d') . '%';
            $date_string_2 = Carbon::parse($burden->created_at)->format('Y-m-d') . '%';
            $bakers =Group::where('created_at', 'like', $date_string)->orWhere('created_at', 'like', $date_string_2)->get();
            $employee_groups = Group::where('created_at', 'like', $date_string)->orWhere('created_at', 'like', $date_string_2)->get();
        }else{

            $date_string = Carbon::parse($burden->created_at)->format('Y-m-d') . '%';
            $bakers =Group::where('created_at', 'like', $date_string)->get();
            $employee_groups = Group::where('created_at', 'like', $date_string)->get();
        }

        //$bakers = Employee::pluck('full_name','id');



        return view('ShortEatsManager::burden.ChistoryView',compact('bakers','employee_groups'))->with([
            'burden' => $burden,
            'burdenClass' => new ShortEatsController
        ]);
    }

    public function changeBaker(Request $request, CookingRequest $burden){
        $burden->baker = $request->baker;
        $burden->save();

        return redirect()->back()->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => "Baker Updated !",
        ]);
    }

    public function changeGroup(Request $request,CookingRequest $burden){

        $burden->employee_group = $request->employee_group;
        $burden->save();

        $old_data = CookingRequestGroupEmployee::where('cooking_request_id','=',$burden->id)->delete();


        $burdenEmp = new CookingRequestGroupEmployee();
        $burdenEmp->cooking_request_id = $burden->id;
        $burdenEmp->group_id = $request->employee_group;
        $burdenEmp->save();

        return redirect()->back()->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => "Employee Group Updated",
        ]);
    }

    public function printList(Request $request,CookingRequest $burden){

        $burden = CookingRequest::where('id', '=', $burden->id)
            ->with('semiProdcuts', 'recipe.recipeContents.materials')
            ->first();
        $date = Carbon::now();
        $teams = GroupEmployee::whereDate('created_at', $date)->get();

  //      $pdf = PDF::loadView('ShortEatsManager::burden.print', compact('teams','burden'));
  //      $pdf->setPaper([0, 0, 220, 9999], 'portrait');
   //     return $pdf->stream();


    /*    $invoice = PurchasingOrder::whereId($purchasingOrder)->first();
        $items = Content::where('purchasing_order_id', '=', $invoice->id)->get();
        //  return view('po.show-grn', compact('items', 'invoice'));

        return view('PurchasingOrderManager::po.show-grn')->with([
            'invoice' => $invoice,
            'items' => $items,

        ]);*/

        return view('ShortEatsManager::burden.print', compact('teams'))->with([
            'burden' => $burden
        ]);
    }


}
