@extends('layouts.back.master')@section('title','Burden | Details')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/lightbox2/dist/css/lightbox.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}"/>

    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">

    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }


        .profile-pic {
            position: relative;
            /*display: inline-block;*/
        }

        .profile-pic:hover .edit {
            display: block;
        }

        .edit {
            padding-top: 7px;
            padding-right: 7px;
            position: absolute;
            right: 0;
            top: 0;
            display: none;
        }

        .edit a {
            color: #000;
        }

        #container {
            position: relative;
            padding: 0px;
            width: 100%;
            height: 330px;
            overflow: auto;
        }

        #container .content {
            width: 100%;
            height: 25%;
        }
    </style>

@stop
@section('current','Burden History')
@section('current_url',route('burden.history'))
@section('current2',$burden['semiProdcuts']->name.' Burden')
@section('current_url2','')
@section('content')
    <div class="app-wrapper">
        <div class="profile-section-user">
            <div class="profile-cover-img profile-pic">
                <img src="{{asset('assets\img\semi2.jpg')}}" alt="">
            </div>
            <div class="profile-info-brief p-3">
                @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                <div class="user-profile-avatar avatar avatar-circle avatar-md project-iconr bg-{{$color[$burden->colour]}}"
                     data-plugin="firstLitter"
                     data-target="#{{$burden->id*754}}"></div>

                <div class="text-center">
                    <h5 class="text-uppercase mb-1" id="{{$burden->id*754}}">{{$burden['semiProdcuts']->name}}</h5>
                    <h6 class="text mb-lg-1">{{'Burden Size : '.$burden->burden_size}} KG</h6>
                    <br>
                </div>

                <hr class="m-0">

                <div class="text-center py-3">
                    {{'Active Recipe : '.$burden['recipe']->name}}<br>
                    <hr class="m-1">
                    {{'Expected Quantity : '.$burden->expected_semi_product_qty}} units<br>
                    {{'Actual Quantity : '.$burden->actual_semi_product_qty}} units<br>
                    <hr class="m-1">
                    {{'Baker : '.$burden['bakers']->first_name.' '.$burden['bakers']->last_name}}<br>
                </div>
            </div>
        </div>
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">{{$burden->created_at->format('d l F Y').' - '.$burden['semiProdcuts']->name}} Burden</h5>
            </div>
            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">
                    <div class="media-list" id="rcontainer">
                        <div class="media content">
                            @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                            <div class="avatar avatar avatar-md project-icon bg-{{$color[$burden['recipe']->colour]}}"
                                 data-plugin="firstLitter"
                                 data-target="#{{$burden['recipe']->id*754}}"></div>
                            <div class="media-body">
                                <h6 class="project-name"
                                    id="{{$burden['recipe']->id*754}}">{{$burden['recipe']->name}}</h6>
                                <small class="project-detail">{{$burden['recipe']->desc}}</small>
                                <br>
                            </div>
                            <div class="col-md-3">

                                <button class="btn btn-outline-primary" type="button" data-toggle="collapse"
                                        data-target="#multiCollapseExample{{$burden['recipe']->id}}"
                                        aria-expanded="false"
                                        aria-controls="multiCollapseExample{{$burden['recipe']->id}}"
                                        style="width: 100%; border-color: transparent">
                                                <span>
                                                <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                </span> Ingredients
                                </button>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="" id="multiCollapseExample{{$burden['recipe']->id}}" style="">
                                <div class="card-body">
                                    @foreach($burden['recipe']->recipeContents as $r)
                                        <div class="row burden">
                                            <div class="col-md-2">
                                                <span><i class="fa fa-shopping-bag"></i> </span>
                                            </div>
                                            <div class="col-md-7">
                                                {{$r['materials']->name}}

                                            </div>
                                            <div class="col-md-3">
                                                {{$r['qty']}} g
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="media content">
                            @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                            <div class="avatar avatar avatar-md project-icon bg-{{$color[5]}}"
                                 data-plugin="firstLitter"
                                 data-target="#89"></div>
                            <div class="media-body">
                                <h6 class="project-name"
                                    id="89">Employees</h6>
                                <br>
                            </div>
                            <div class="col-md-3">
                                <button class="btn btn-outline-primary" type="button" data-toggle="collapse"
                                        data-target="#multiCollapseExample{{$burden->id}}"
                                        aria-expanded="false"
                                        aria-controls="multiCollapseExample{{$burden->id}}"
                                        style="width: 100%; border-color: transparent">
                                                <span>
                                                <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                </span> Employees
                                </button>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="" id="multiCollapseExample{{$burden->id}}" style="">
                                <div class="card-body">
                                    @foreach($burden['employees'] as $r)
                                        <div class="row burden">
                                            <div class="col-md-2">
                                                <span><i class="fa fa-shopping-bag"></i> </span>
                                            </div>
                                            <div class="col-md-7">
                                                {{$r['employee']->first_name.' '.$r['employee']->last_name}}

                                            </div>
                                            <div class="col-md-3">
                                                {{--{{$r['qty']}} g--}}
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>


                        <div class="media content">
                            @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                            <div class="avatar avatar avatar-md project-icon bg-{{$color[2]}}"
                                 data-plugin="firstLitter"
                                 data-target="#90"></div>
                            <div class="media-body">
                                <h6 class="project-name"
                                    id="90">Grade</h6>
                                <br>
                            </div>
                            <div class="col-md-3">
                                <button class="btn btn-outline-primary" type="button" data-toggle="collapse"
                                        data-target="#multiCollapseExample1"
                                        aria-expanded="false"
                                        aria-controls="multiCollapseExample1"
                                        style="width: 100%; border-color: transparent">
                                                <span>
                                                <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                                </span> Quantity
                                </button>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="" id="multiCollapseExample1" style="">
                                <div class="card-body">
                                    @foreach($burden['grade'] as $r)
                                        <div class="row burden">
                                            <div class="col-md-2">
                                                <span><i class="fa fa-shopping-bag"></i> </span>
                                            </div>
                                            <div class="col-md-7">
                                                {{$r['grade']->name}}

                                            </div>
                                            <div class="col-md-3">
                                                {{$r->burden_qty}} units
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/waypoints/lib/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/waypoints/lib/shortcuts/sticky.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/counterup/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/js/jquery.sparkline.min.js')}}"></script>
    <script src="{{asset('js/site.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>


    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>

    <script>
        var ps = new PerfectScrollbar('#container');

    </script>
@stop