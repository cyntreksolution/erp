<html>
<head>
<link rel="stylesheet" href="{{asset('assets/vendor/css/bootstrap.css')}}">
<link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/css/invoice.css')}}" rel="stylesheet">
<script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<style media="all">
        @page {
            margin: 30px 5px 30px 15px;
            size: 75mm 999mm;
        }
    </style>
</head>

<body style="margin-bottom: 100px">

<div class="row">

    <div class="col-xs-5">
        <img src="{{asset('assets/img/buntak_grace.jpeg')}}" style="width: 100px">
    </div>

    <div class="col-xs-7 text-left">
        <h6>
            <b>Ranjanas Holdings [Pvt] Ltd</b><br>
            Katukurunda<br>
            Habaraduwa<br>
            Hotline : +9477 330 4678<br>
            E-mail : buntalksl@icloud.com<br>
        </h6>
    </div>
</div>

<div class="row" style="margin-top: 15px;">

    <div class="col-xl-12 text-center">
        <h3><b>

                    {{$burden['semiProdcuts']->name}}
                </b></h3>
    </div>



</div>

    <div>
        <div class="row">
            <div class="col-xs-6 text-nowrap"><b>Burden Serial :</b></div>
            <div class="col-xs-6">{{$burden->serial}}</div>
        </div>
       <!--div class="row">
            <div class="col-xs-6 text-nowrap" id="{{$burden->id*754}}"><b>Burden Product Name :</b></div>
            <div class="col-xs-6">{{$burden['semiProdcuts']->name}}</div>
        </div-->
        <div class="row">
            <div class="col-xs-6 text-nowrap"><b>Burden Size (Kg/Unit) :</b></div>
            <div class="col-xs-6"><h4><b>{{$burden->burden_size}}</b></h4></div>
        </div>
        <div class="row">
            <div class="col-xs-6 text-nowrap"><b>Expected Quantity :</b></div>
            <div class="col-xs-6">{{$burden->expected_semi_product_qty}}</div>
        </div>
        <div class="row">
            <div class="col-xs-6 text-nowrap"><b>Active Recipe :</b></div>
            <div class="col-xs-6">{{$burden['recipe']->name}}</div>
        </div>
        <div class="row">
            <div class="col-xs-6 text-nowrap"><b>Date with Time :</b></div>
            <div class="col-xs-6">{{\Carbon\Carbon::now()->format('Y-m-d H:i:s')}}</div>
        </div>

    </div>

<br>


<div class="row">
    <div class="col-xs-12">
        <table class="table">
            <thead>
            <tr>
                <th>Item Name</th>
                <th class="text-center">Volume</th>
                <th class="text-right">Checked</th>

            </tr>
            </thead>
            <tbody>
            @php  @endphp
            @foreach($burden['recipe']->recipeContents as $item)

                <tr>
                    <td> <h4 id="{{$item['materials']->raw_material_id}}">{{$item['materials']->name}}/<font face="FMMalithi x"><b> {{$item['materials']->sin_name}}</b></font> -[{{$item['materials']->scale_key}}]</h4></td>


                        <td class="text-center"><h4>
                            @if($item['materials']->measurement =='gram')
                                @if($item['qty']*$burden['burden_size'] > 999)
                                    {{($item['qty']*$burden['burden_size']/1000)}}
                                @else
                                    {{($item['qty']*$burden['burden_size'])}}
                                @endif

                            @elseif($item['materials']->measurement =='milliliter')
                                @if($item['qty']*$burden['burden_size'] > 999)
                                    {{($item['qty']*$burden['burden_size']/1000)}}
                                @else
                                    {{($item['qty']*$burden['burden_size'])}}
                                @endif

                            @elseif($item['materials']->measurement =='unit')
                                {{($item['qty']*$burden['burden_size'])}}

                            @endif

                            @if($item['materials']->measurement =='gram')
                                @if($item['qty']*$burden['burden_size'] > 999)
                                    kg
                                @else
                                    g
                                @endif

                            @elseif($item['materials']->measurement =='milliliter')
                                @if($item['qty']*$burden['burden_size'] > 999)
                                    litre
                                @else
                                    milliliter
                                @endif

                            @elseif($item['materials']->measurement =='unit')
                                units
                                @endif
                            </h4></td>
                        <td align="right">
                            <div style="width:20px;height:20px;border:1px solid #000;"></div>
                        </td>




                    @php  @endphp
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>

</div>
<div class="row">
    <div class="col-xs-12 pull-right text-right">
        <div class="col-xs-6 text-nowrap"><b>Total Recipe Items:</b></div>
        <div class="col-xs-6"><h4><b>{{$burden['recipe']->recipe_item}}</b></h4></div>

    </div>

    <div class="col-xs-12 pull-right text-right">
        <div class="col-xs-6 text-nowrap"><b>Total Recipe Weight(Kg):</b></div>
        <div class="col-xs-6"><h4><b>{{($burden['recipe']->recipe_weight * $burden->burden_size)/1000}}</b></h4></div>

    </div>
    <div class="col-xs-12 pull-right text-right">
        <div class="col-xs-6 text-nowrap"><b>Scale Weight:</b></div>
        <div class="col-xs-6 -align-right" style="width:100px;height:30px;border:1px solid #000;"></div>

    </div>
</div>
<br>

<footer>


    <div class="row col-xs-12" style="margin-top: 60px;">

        <div class="text-center col-xs-12">
            <h5>
                .........................................<br>
                Signature-Stores Superviser
            </h5>
        </div>
    </div>
    <div class="row col-xs-12" style="margin-top: 60px;">
        <div class="text-center col-xs-12">
            <h5>
                .........................................<br>
                Signature-Manager
            </h5>
        </div>

    </div>
    <div class="row col-xs-12" style="margin-top: 60px;">
        <div class="text-center col-xs-12">
            <h5>
                .........................................<br>
                Name with Signature-Accepted By
            </h5>
        </div>

    </div>


</footer>

</body>

</html>














