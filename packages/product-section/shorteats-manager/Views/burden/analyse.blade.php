@extends('layouts.back.master')@section('title','Burden Analyse')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/clockpicker/dist/bootstrap-clockpicker.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}"/>


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }


    </style>
@stop
@section('content')
    <div class="app-main">
        <div class="col-md-12">
            <canvas id="lineChart"></canvas>
        </div>
        <div class="row my-5">
            <div class="col-md-4">
                <canvas id="chart-area"/>
            </div>
            <div class="col-md-7  ml-3">
                <div class=" card">
                    <header class="card-header"><h6 class="card-heading">Open Projects</h6>
                        <ul class="card-toolbar">
                            <li><a href="javascript:void(0)"><i class="zmdi zmdi-refresh"></i></a></li>
                            <li class="dropdown"><a href="javascript:void(0)" data-toggle="dropdown"
                                                    aria-expanded="false"><i class="zmdi zmdi-more-vert"></i></a>
                                <div class="dropdown-menu float-right"><a class="dropdown-item" href="#">Option 1</a> <a
                                            class="dropdown-item" href="#">Option 2</a> <a class="dropdown-item"
                                                                                           href="#">Option
                                        3</a></div>
                            </li>
                        </ul><!-- /.card-toolbar --></header>
                    <div class="media-list">
                        <div class="media">
                            <div class="avatar avatar-sm bg-success" data-plugin="firstLitter"
                                 data-target="#media-list-item-3">B
                            </div>
                            <div class="media-body"><h6 class="media-heading" id="media-list-item-3">Bootstrap 4 alpha
                                    3</h6>
                                <small>5 tasks, 30 issues</small>
                            </div>
                        </div><!-- /.media -->
                        <div class="media">
                            <div class="avatar avatar-sm bg-warning" data-plugin="firstLitter"
                                 data-target="#media-list-item-4">J
                            </div>
                            <div class="media-body"><h6 class="media-heading" id="media-list-item-4">Jquery 3 master
                                    branch</h6>
                                <small>30 pulls, 43 issues</small>
                            </div>
                        </div><!-- /.media -->
                        <div class="media">
                            <div class="avatar avatar-sm bg-primary" data-plugin="firstLitter"
                                 data-target="#media-list-item-5">D
                            </div>
                            <div class="media-body"><h6 class="media-heading" id="media-list-item-5">D3 charting
                                    Libraray</h6>
                                <small>3 merges, 31 fixes</small>
                            </div>
                        </div><!-- /.media -->
                        <div class="media">
                            <div class="avatar avatar-sm bg-danger" data-plugin="firstLitter"
                                 data-target="#media-list-item-6">L
                            </div>
                            <div class="media-body"><h6 class="media-heading" id="media-list-item-6">Luxury admin
                                    panel</h6>
                                <small>415 tasks, 0 issues</small>
                            </div>
                        </div><!-- /.media --></div><!-- /.media-list --></div>
            </div>
        </div>


            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                    <div class="table-responsive">
                        <table class="table text-nowrap">
                            <tbody>
                            <tr class="bg-faded">
                                <th>Burden</th>
                                <th>Actual quantity from the espected quantity</th>
                                <th>Burden size</th>
                                <th>Serial number</th>
                                <th>Time</th>
                            </tr>
                            <tr>
                                <td>Cream Bun</td>
                                <td style="min-width: 200px" class="align-middle">
                                    <div class="progress progress-xs">
                                        <div class="progress-bar" role="progressbar" style="width: 80%"
                                             aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>20</td>
                                <td>18965987</td>
                                <td>9.30-11.00</td>
                            </tr>
                            <tr>
                                <td>Raisin Bun</td>
                                <td style="min-width: 200px" class="align-middle">
                                    <div class="progress progress-xs">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: 90%"
                                             aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                                <td>30</td>
                                <td>18695398</td>
                                <td>13.00-15.30</td>
                            </tr>


                            </tbody>
                        </table>
                    </div>
                           </div><!-- /.col --></div>
            </div>


@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/moment/min/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/chart.js/dist/Chart.min.js')}}"></script>


    <script>
        var ctx = document.getElementById("lineChart");
        var myLineChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [@forEach($dates as $date) "{{$date}}", @endforeach],
                datasets: [
                        @foreach($productNames as $productName)
                    {
                        label: "{{$productName['name']}}",
                        fill: false,
                        backgroundColor: x = getRandomColor(),
                        borderColor: x,
                        data: [
                            @foreach($productName['date'] as $date)
                            {{$date}},
                            @endforeach
                        ],
                    },
                    @endforeach
                ]
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: 'Summery on Burden Count'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Month'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Value'
                        }
                    }]
                }
            }
        })
        var randomScalingFactor = function () {
            return Math.round(Math.random() * 100);
        };
        var config = {
            type: 'pie',
            data: {
                datasets: [{
                    data: [
                        @foreach($productNames as $productName)
                        {{$productName['sum']}},
                        @endforeach
                    ],
                    backgroundColor: [
                        getRandomColor(),
                        getRandomColor(),
                        getRandomColor(),
                        getRandomColor(),
                        getRandomColor(),
                    ],
                    label: 'Dataset 1'
                }],
                labels: [
                    @foreach($productNames as $productName)
                        "{{$productName['name']}}",
                    @endforeach
                ]
            },
            options: {
                responsive: true
            }
        };
        var ctxx = document.getElementById("chart-area").getContext("2d");
        window.myPie = new Chart(ctxx, config);


        function getRandomColor() {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }

    </script>
@stop
