@extends('layouts.back.master')@section('title','Cooking Request | Details')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/lightbox2/dist/css/lightbox.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}"/>

    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">

    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }


        .profile-pic {
            position: relative;
            /*display: inline-block;*/
        }

        .profile-pic:hover .edit {
            display: block;
        }

        .edit {
            padding-top: 7px;
            padding-right: 7px;
            position: absolute;
            right: 0;
            top: 0;
            display: none;
        }

        .edit a {
            color: #000;
        }

        #container {
            position: relative;
            padding: 0px;
            width: 100%;
            height: 330px;
            overflow: auto;
        }

        #container .content {
            width: 100%;
            height: 25%;
        }
    </style>

@stop
@section('current','Cooking Request List')
@section('current_url',route('shorteats.list'))
@section('current2',$burden['semiProdcuts']->name.' Cooking Request')
@section('current_url2','')
@section('content')
    <div class="app-wrapper">

        <div class="profile-section-user">
            <div class="profile-cover-img profile-pic">
                <img src="{{asset('assets\img\semi2.jpg')}}" alt="">
            </div>
            <div class="profile-info-brief p-3">
                @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                <div class="user-profile-avatar avatar avatar-circle avatar-md project-iconr bg-{{$color[$burden->colour]}}"
                     data-plugin="firstLitter"
                     data-target="#{{$burden->id*754}}"></div>

                <div class="text-center">
                    <h5 class="text-uppercase ">{{$burden->serial}}</h5>
                    <h5 class="text-uppercase mb-1" id="{{$burden->id*754}}">{{$burden['semiProdcuts']->name}}</h5>
                    <h6 class="text mb-lg-1">{{'Burden Size : '.$burden->burden_size}} </h6>
                    <br>
                </div>

                <hr class="m-0">

                <div class="py-3">
                    {{'Active Recipe : '.$burden['recipe']->name}}<br>
                    <hr class="m-1">

                    {{--'Expected Quantity : '.$burden->expected_semi_product_qty}} units<br>
                    {{'Used Quantity : '. ($burden->expected_semi_product_qty - $burden->used_qty)}} units<br>
                    {{'Available Quantity : '.$burden->used_qty--}}

                    @php
                        $exQty = $burden->expected_semi_product_qty;
                        $makers = $burdenClass->getBakers($burden->employee_group);
                        $bakers = $burdenClass->getBakers($burden->baker);

                    @endphp


                    <hr class="m-1">
                    <b class="font-weight-bold">First Stage
                        - {{!empty($burden->employee_group) && $makers->count()>0 ?$makers[0]->name:'-'}} </b>
                    @if($role = Sentinel::check()->roles[0]->slug=='owner')
                        <i data-toggle="modal" data-target="#exampleModal2"
                           class="fa fa-pencil float-right text-right"></i>
                    @endif
                    <br>

                    @if (!empty($burden->employee_group))
                        @foreach ($makers as $gr)
                            @foreach ($gr->employees as $user)
                                <small class="text-left">{{$user->full_name}}</small><br>
                            @endforeach
                        @endforeach
                    @endif


                    <hr class="m-1">
                    <b class="font-weight-bold">Second Stage
                        - {{!empty($bakers) && $bakers->count()>0 ?$bakers[0]->name:'-'}} </b>
                    @if($role = Sentinel::check()->roles[0]->slug=='owner')
                        <i data-toggle="modal" data-target="#exampleModal"
                           class="fa fa-pencil float-right text-right"></i>
                    @endif
                    <br>

                    @if (!empty($bakers))
                        @foreach ($bakers as $gr)
                            @foreach ($gr->employees as $user)
                                <small class="text-left">{{$user->full_name}}</small><br>
                            @endforeach
                        @endforeach
                    @endif
                    <hr class="m-1">




                    <div class="py-3">
                        <b class="font-weight-bold">

                            @if ($burden->responce === 0)
                                {{'Current Responsibility : '}}-First Stage <br>
                                <b class="font-weight-bold">Change Responsible Stage
                                    {!! Form::open(['route' => ['update-first-stage-cooking-req'], 'method' => 'post']) !!}
                                    <input type="hidden" name="burden_id" value="{{$burden->id}}">
                                    <button type="submit" class="btn btn-warning">To Second Stage</button>
                                    {!! Form::close() !!}
                                    @else
                                        {{'Current Responsibility : '}}-Second Stage <br>
                                        <b class="font-weight-bold">Change Responsible Stage
                                            {!! Form::open(['route' => ['update-second-stage-cooking-req'], 'method' => 'post']) !!}
                                            <input type="hidden" name="burden_id" value="{{$burden->id}}">
                                            <button type="submit" class="btn btn-danger">To First Stage</button>
                        {!! Form::close() !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>




        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">{{$burden->created_at->format('d M Y').' - '.$burden['semiProdcuts']->name}}
                    Cooking Request</h5>
            </div>
            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">
                    <div class="media-list" id="rcontainer">

                        @php
                            $ref = 'App/CookingRequest';
                                $alc = \BurdenManager\Models\AllocateSemiFinish::where('semi_finish_burden_id','=',$burden->id)->where('burden_type','=','cooking_request')->get();
                                $allocations = \App\SemiBurdenIssueData::whereReference($burden->id)->whereReferenceType($ref)->get();
                        @endphp

                        <div class="media content" >

                            <div class="col-md-6" align="center">
                                Date & Description
                            </div>



                            <div class="col-md-6">
                                <p class="text-nowrap text-right"> Qty</p>
                            </div>
                        </div>

                        <!--div class="col-md-12">
                            <div class="col-md-12" id="multiCollapseExample1" style="" >
                                <div class="card-body col-md-12" -->
                        @php
                            $total =0;
                        @endphp
                        @foreach($allocations as $allocation)
                            @php
                                $data = \EndProductBurdenManager\Models\BurdenSemi::find($allocation->merege_burden_semi_id);
                            @endphp
                            <div class="row col-md-12">
                                <div class="col-md-6 text-left">
                                    < class="text-nowrap">{{$allocation->created_at->format('Y-M-d H:i:s')}} </p>
                                </div>
                                <div class="col-md-6 text-left">
                                    < class="text-nowrap">   -{{$allocation->description}}</p>
                                </div>
                            </div>
                            <div class="row col-md-12">
                                <div class="col-md-6 text-right">
                                    <p class="text-nowrap"> {{!empty($data)?$data->endProduct->name:null}} - {{!empty($data)?$data->endProductBurden->serial:null}} </p>
                                </div>

                                <div class="col-md-6 text-right">
                                    <p class="text-nowrap text-right"> {{number_format($allocation->used_qty,3)}}</p>
                                </div>
                            </div>
                            <hr>
                            @php $total =$total + $allocation->used_qty @endphp
                        @endforeach
                        @foreach($alc as $alc)
                            @php

                                $endSerial =\EndProductBurdenManager\Models\EndProductBurden::find($alc->end_product_burden_id);

                                $endName =\EndProductManager\Models\EndProduct::find($endSerial->end_product_id);
                            @endphp
                            <div class="row col-md-12">
                                <div class="col-md-6 text-left">
                                    <p class="text-nowrap -align-left">{{$alc->created_at->format('Y-M-d H:i:s')}}</p>
                                </div>
                                <div class="col-md-6 text-left">
                                    <p class="text-nowrap -align-left"> - Make End Product</p>
                                </div>
                            </div>
                            <div class="row col-md-12">

                                <div class="col-md-6 text-right">
                                    <p class="text-nowrap"> {{$endName->name}} - {{$endSerial->serial}}</p>
                                </div>

                                <div class="col-md-6 text-right">
                                    <p class="text-nowrap text-right"> {{number_format($alc->allocate_qty,3)}}</p>
                                </div>
                            </div>
                            <hr>

                            @php $total =$total + $alc->allocate_qty @endphp
                        @endforeach


                        <div class="row col-md-12 ">
                            <div class="col-md-6 text-right">
                                <p class="text-nowrap font-weight-bold">Total</p>
                            </div>



                            <div class="col-md-6 text-right">
                                <p class="text-nowrap font-weight-bold text-right"> {{number_format($total,3)}}</p>
                            </div>
                        </div>

                        <hr>
                        <div class="row col-md-12">
                            <div class="col-md-6 text-right">
                                <p class="text-nowrap font-weight-bold">Balance Volum In Hand</p>
                            </div>

                            <div class="col-md-6 text-right">
                                <p class="text-nowrap font-weight-bold text-right"> {{number_format((($exQty)-($total)),3)}}</p>
                            </div>
                        </div>
                        <!--/div>
                    </div>
                </div-->




                    </div>
                </div>

            </div>
        </div>
    </div>



    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit First Stage</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => ['shorteats.changeEG',$burden->id], 'method' => 'post']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            Employee Groups
                        </div>
                        <div class="col-md-6">
                            <select required name="employee_group" class="form-control">
                                @foreach($employee_groups as $group)
                                    <option value="{{$group->id}}">{{$group->employees->pluck('first_name')}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary">Save changes</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>


    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Second Stage</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => ['shorteats.changeb',$burden->id], 'method' => 'post']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            Baker
                        </div>
                        <div class="col-md-6">
                            {{--!! Form::select('baker', $bakers , null , ['class' => 'form-control','required']) !!--}}
                            <select required name="baker" class="form-control">
                                @foreach($employee_groups as $group)
                                    <option value="{{$group->id}}">{{$group->employees->pluck('first_name')}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary">Save changes</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/waypoints/lib/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/waypoints/lib/shortcuts/sticky.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/counterup/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/js/jquery.sparkline.min.js')}}"></script>
    <script src="{{asset('js/site.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>


    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>

    <script>
        var ps = new PerfectScrollbar('#container');

    </script>
@stop
