<?php

namespace BurdenManager\Classes;

use BurdenManager\Models\Burden;
use EndProductManager\Classes\ProductContent;
use EndProductManager\Models\EndProduct;
use ProductionOrderManager\Models\ProductionOrderData;
use ProductionOrderManager\Models\ProductionOrderHeader;
use RawMaterialManager\Models\RawMaterial;
use RecipeManager\Models\Recipe;
use RecipeManager\Models\RecipeContent;
use RecipeManager\Models\RecipeSemiProduct;
use SemiFinishProductManager\Models\SemiFinishProduct;

/**
 * User: Chinthaka Dilan F
 * Date: 11/30/2017
 * Project : erp
 */
class BurdenContent
{
    public static function getBurdenContent($staus)
    {
        $array = array();
        $burdenSets = Burden::where('status', '=', $staus)->get();
        foreach ($burdenSets as $burdenSet) {
              $orderBurden = ProductionOrderData::find($burdenSet->production_order_data_id);
            $order_qty = $orderBurden->qty;
            $order_burden_kg = $orderBurden->burden_kg;
            $order_recipe_id = $orderBurden->recipe_id;
            $semi_finish_product = ProductContent::getSemiProductsOfEndProduct(EndProduct::find($orderBurden->end_product_end_product_id))->first();
            $order_semi_finish_product_id = $semi_finish_product->semi_finish_product_id;
            $units_per_recipe = RecipeSemiProduct::where('recipe_id', '=', $order_recipe_id)->where('semi_finish_product_semi_finish_product_id', '=', $order_semi_finish_product_id)->where('status', '=', 2)->first()->units;

            $recips_per_order = $order_qty / $units_per_recipe;
            $burdens = $recips_per_order / $order_burden_kg;
            $recipe_contents = ProductContent::getContentsOfRecips(Recipe::find($order_recipe_id));
            $mat = array();
            foreach ($recipe_contents as $key => $recipe_content) {
                $needed_material_id = $recipe_content->raw_material_id;
                $needed_material_qty = ($recipe_content->qty * $recips_per_order) / $burdens;
//                array_push($mat,RawMaterial::find($needed_material_id));
                $mat[$key] = [
                    'meterial_name' => RawMaterial::find($needed_material_id)->name,
                    'measurement' => RawMaterial::find($needed_material_id)->measurement,
                    'qty_per_burden' => $needed_material_qty,
                    ];
            }

            $req = [
                'semi' => $semi_finish_product,
                'materials' => $mat,
                'total_burdens' => $burdens,
                'kg_per_burden' => $order_burden_kg,
                'burden_id' => $burdenSet->id,];
            array_push($array, $req);

        }


        return $array;

    }

}
