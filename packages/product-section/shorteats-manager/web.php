<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web'])->group(function () {
    Route::prefix('short-eats')->group(function () {
        Route::get('list', 'ShortEatsManager\Controllers\ShortEatsController@burdenListShow')->name('shorteats.list');
        Route::get('table/data', 'ShortEatsManager\Controllers\ShortEatsController@tableData')->name('shorteats.table');
        Route::post('excel/data', 'ShortEatsManager\Controllers\ShortEatsController@exportFilter')->name('shorteats.excel');

        Route::get('/print/{burden}', 'ShortEatsManager\Controllers\ShortEatsController@printList')->name('shorteats.print');

        Route::post('delete/short-eat', 'ShortEatsManager\Controllers\ShortEatsController@destroy')->name('shorteats.delete');
        Route::get('', 'ShortEatsManager\Controllers\ShortEatsController@index')->name('shorteats.index');
        Route::get('bake/ready/list', 'ShortEatsManager\Controllers\ShortEatsController@bakeList')->name('bake.list');

        Route::get('create', 'ShortEatsManager\Controllers\ShortEatsController@create')->name('shorteats.create');

        Route::get('date', 'ShortEatsManager\Controllers\ShortEatsController@dateReport')->name('shorteats.date');
        Route::post('date', 'ShortEatsManager\Controllers\ShortEatsController@getReport')->name('shorteats.date');
        Route::post('analyse', 'ShortEatsManager\Controllers\ShortEatsController@analyse')->name('shorteats.analyse');


        Route::post('update_stage1_cooking_req','ShortEatsManager\Controllers\ShortEatsController@sstage')->name('update-first-stage-cooking-req');
        Route::post('update_stage2_cooking_req','ShortEatsManager\Controllers\ShortEatsController@fstage')->name('update-second-stage-cooking-req');

        Route::get('view/history', 'ShortEatsManager\Controllers\ShortEatsController@history')->name('shorteats.history');

        Route::get('view/history/{burden}', 'ShortEatsManager\Controllers\ShortEatsController@historyView')->name('shorteats.historyView');

        Route::get('{cookingRequest}', 'ShortEatsManager\Controllers\ShortEatsController@show')->name('shorteats.show');

        Route::get('{cookingRequest}/edit', 'ShortEatsManager\Controllers\ShortEatsController@edit')->name('shorteats.edit');

        Route::get('{cookingRequest}/bake', 'ShortEatsManager\Controllers\ShortEatsController@bake')->name('shorteats.bake');

        Route::post('', 'ShortEatsManager\Controllers\ShortEatsController@store')->name('shorteats.store');

        Route::post('grade/save', 'ShortEatsManager\Controllers\ShortEatsController@grade')->name('shorteats.grade');

        Route::get('save/qty', 'ShortEatsManager\Controllers\ShortEatsController@saveqty')->name('shorteats.grade');

        Route::post('{burden}', 'ShortEatsManager\Controllers\ShortEatsController@update')->name('shorteats.update');
        Route::post('bake/{cookingRequest}', 'ShortEatsManager\Controllers\ShortEatsController@baekNow')->name('shorteats.bakenow');

        Route::get('get/recipe', 'ShortEatsManager\Controllers\ShortEatsController@semiFinishProductRecipeAndCount')->name('shorteats.updatex');

        Route::delete('{shorteats}', 'ShortEatsManager\Controllers\ShortEatsController@destroy')->name('shorteats.destroy');

        Route::post('bake/{burden}/change/baker', 'ShortEatsManager\Controllers\ShortEatsController@changeBaker')->name('shorteats.changeb');
        Route::post('bake/{burden}/change/group', 'ShortEatsManager\Controllers\ShortEatsController@changeGroup')->name('shorteats.changeEG');

    });
});