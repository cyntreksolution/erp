<?php

namespace ShortEatsManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use SemiFinishProductManager\Models\SemiFinishProduct;

class CookingRequestEmployee extends Model
{
    use SoftDeletes;

    protected $table = 'cooking_request_employee';

    protected $dates = ['deleted_at'];

    public function employee(){
        return $this->belongsTo('EmployeeManager\Models\Employee');
    }
}
