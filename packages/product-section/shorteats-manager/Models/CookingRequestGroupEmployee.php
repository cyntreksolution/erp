<?php

namespace ShortEatsManager\Models;

use EmployeeManager\Models\Group;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use SemiFinishProductManager\Models\SemiFinishProduct;

class CookingRequestGroupEmployee extends Model
{
//    use SoftDeletes;

    protected $table = 'cooking_request_group_employees';

    protected $dates = ['deleted_at'];

    public function employee(){
        return $this->belongsTo('EmployeeManager\Models\Employee');
    }

    public function employeeGroup()
    {
        return $this->belongsToMany(Group::class, 'burden_group_employees');
    }
}
