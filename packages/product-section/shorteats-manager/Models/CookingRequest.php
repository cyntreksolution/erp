<?php

namespace ShortEatsManager\Models;

use Carbon\Carbon;
use EmployeeManager\Models\Group;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CookingRequest extends Model
{
    use SoftDeletes;
    public function employees(){
        return $this->hasMany('BurdenManager\Models\BurdenEmployee');
    }

    public function employeeGroup()
    {
        return $this->belongsToMany(Group::class,'cooking_request_group_employees');
    }


    public function semiProdcuts(){
        return $this->belongsTo('SemiFinishProductManager\Models\SemiFinishProduct','semi_finish_product_id','semi_finish_product_id');
    }

    public function recipe(){
        return $this->belongsTo('RecipeManager\Models\Recipe');
    }

    public function baker(){
        return $this->hasOne('EmployeeManager\Models\Employee','id','baker');
    }

    public function grade(){
        return $this->hasMany('BurdenManager\Models\BurdenGrade');
    }

    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }

    public function scopeSearchData($query, $term)
    {
        return $query
            ->Where('id', 'like', "%" . $term . "%")
            ->orWhere('serial', 'like', "%" . $term . "%")
            ->orWhere('status', 'like', "%" . $term . "%")
            ->orWhere('burden_size', 'like', "%" . $term . "%")
            ->orWhere('created_at', 'like', "%" . $term . "%")
            ->orWhereHas('semiProdcuts', function ($query) use ($term) {
                $query->where('name', 'like', "%" . $term . "%");
            })
            ->orWhereHas('recipe', function ($query) use ($term) {
                $query->where('name', 'like', "%" . $term . "%");
            });
    }

    public function scopefilterData($query, $semi, $status, $date_range, $date_type=null)
    {
        if (!empty($semi)) {
            $query->whereSemiFinishProductId($semi);
        }
        if (!empty($status)) {
            $query->whereStatus($status);
        }


        if (!empty($date_range)) {
            $dates = explode(' - ', $date_range);
            $start = $dates[0];
            $end = $dates[1];
            if ($date_type == 2){
                $query->where('created_start_time', '>=', Carbon::parse($start)->format('Y-m-d'))
                    ->where('created_start_time', '<=', Carbon::parse($end)->addDay()->format('Y-m-d'));
            }elseif ($date_type == 3){
                $query->where('bake_start_time', '>=', Carbon::parse($start)->format('Y-m-d'))
                    ->where('bake_start_time', '<=', Carbon::parse($end)->addDay()->format('Y-m-d'));
            }elseif ($date_type == 4){
                $query->where('updated_at', '>=', Carbon::parse($start)->format('Y-m-d'))
                    ->where('updated_at', '<=', Carbon::parse($end)->addDay()->format('Y-m-d'))
                    ->where('status', '>=', 5);


            }else{
                $query->where('created_at', '>=', Carbon::parse($start)->format('Y-m-d'))
                    ->where('created_at', '<=', Carbon::parse($end)->addDay()->format('Y-m-d'));
            }
            //$query= $query->where('created_at','>=',Carbon::parse($start)->format('Y-m-d'))
            //    ->where('created_at','<=',Carbon::parse($end)->addDay()->format('Y-m-d'));
        }
    }
}
