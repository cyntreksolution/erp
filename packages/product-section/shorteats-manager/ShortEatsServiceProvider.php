<?php

namespace ShortEatsManager;

use Illuminate\Support\ServiceProvider;

class ShortEatsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/Views', 'ShortEatsManager');
        require __DIR__ . '/web.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('ShortEatsManager', function($app){
            return new ShortEatsManager;
        });
    }
}
