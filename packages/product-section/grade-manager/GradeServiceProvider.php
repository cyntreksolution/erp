<?php

namespace GradeManager;

use Illuminate\Support\ServiceProvider;

class GradeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/Views', 'GradeManager');
        require __DIR__ . '/web.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('GradeManager', function($app){
            return new GradeManager;
        });
    }
}
