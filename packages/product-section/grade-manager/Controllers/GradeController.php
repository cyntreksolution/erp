<?php

namespace GradeManager\Controllers;

use GradeManager\Models\Grade;
use Illuminate\Support\Facades\Auth;
use Sentinel;
use RawMaterialManager\Models\RawMaterial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use StockManager\Models\Stock;

class GradeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect(route('grade.create'));
        return view('GradeManager::grade.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $grade = Grade::all();
        return view('GradeManager::grade.create')->with([
            'grades' => $grade,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $grade = new Grade();
        $grade->name = $request->name;
        $grade->desc = $request->desc;
        $grade->colour = rand() % 7;
        $grade->created_by = Auth::user()->id;
        $grade->save();
        return redirect(route('grade.create'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \GradeManager\Models\Grade $grade
     * @return \Illuminate\Http\Response
     */
    public function show(Grade $grade)
    {
        return view('GradeManager::grade.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \GradeManager\Models\Grade $grade
     * @return \Illuminate\Http\Response
     */
    public function edit(Grade $grade)
    {
        return view('GradeManager::grade.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \GradeManager\Models\Grade $grade
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Grade $grade)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \GradeManager\Models\Grade $grade
     * @return \Illuminate\Http\Response
     */
    public function destroy(Grade $grade)
    {
        $grade->delete();
        if ($grade->trashed()) {
            return 'true';
        } else {
            return 'false';
        }
    }
}
