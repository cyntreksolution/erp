<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web','log'])->group(function () {
    Route::prefix('grade')->group(function () {
        Route::get('', 'GradeManager\Controllers\GradeController@index')->name('grade.index');

        Route::get('create', 'GradeManager\Controllers\GradeController@create')->name('grade.create');

        Route::get('{grade}', 'GradeManager\Controllers\GradeController@show')->name('grade.show');

        Route::get('{grade}/edit', 'GradeManager\Controllers\GradeController@edit')->name('grade.edit');


        Route::post('', 'GradeManager\Controllers\GradeController@store')->name('grade.store');

        Route::put('{grade}', 'GradeManager\Controllers\GradeController@update')->name('grade.update');

        Route::delete('{grade}', 'GradeManager\Controllers\GradeController@destroy')->name('grade.destroy');
    });
});
