<?php

namespace GradeManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Grade extends Model
{
    use SoftDeletes;

    protected $table = 'grade';

    protected $primaryKey = 'id';

    protected $dates = ['deleted_at'];
}
