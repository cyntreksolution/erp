@extends('layouts.back.master')@section('title','New Grade')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.wizard.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/css/style.css')}}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>

    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>

    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.cs')}}s">
    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }
    </style>
@stop
@section('content')
    <div class="app-wrapper">
        <div class="app-panel" id="app-panel">
            <div class="app-search">
                <input type="search" class="search-field" placeholder="Search" disabled>
                <i class="search-icon fa fa-search"></i>
            </div>
            <div class="app-panel-inner">
                <div class="scroll-container">
                    <div class="p-3">
                        <button class="btn btn-success py-3 btn-block btn-lg" data-toggle="modal"
                                data-target="#projects-task-modal">NEW GRADE
                        </button>
                    </div>
                </div>
            </div>
            <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show"><i
                        class="fa fa-chevron-right"></i>
                <i class="fa fa-chevron-left"></i>
            </a>
        </div>
        <!-- /.app-panel -->
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">GRADES</h5>
            </div><!-- /.app-main-header -->
            <div class="app-main-content">
                <div class="scroll-container">
                    <div class="media-list ">
                        @foreach($grades as $grade)
                            <a href="{{$grade->id.'/edit'}}">
                                <div class="media">
                                    @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                    <div class="avatar avatar-circle avatar-md project-icon bg-{{$color[$grade->colour]}}"
                                         data-plugin="firstLitter"
                                         data-target="#{{$grade->id*754}}"></div>
                                    <div class="media-body">
                                        <h6 class="project-name"
                                            id="{{$grade->id*754}}">{{$grade->name}}</h6>
                                        <small class="project-detail">{{$grade->desc}}</small>
                                    </div>
                                    <div class="section-2">
                                        <button class="delete btn btn-light btn-sm ml-2" value="{{$grade->id}}"
                                                data-toggle="tooltip"
                                                data-placement="left" title="DELETE GRADE">
                                            <i class="zmdi zmdi-close"></i>
                                        </button>
                                    </div>
                                </div>
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="projects-task-modal" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'grade','enctype'=>'multipart/form-data'))}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="wizard p-4" id="wizard-keep-prefix-suffix-demo">
                                <ul class="nav nav-tabs vertices keep-prefix-suffix" role="tablist">
                                    <li class="nav-item">
                                        <a class=" nav-link active" data-toggle="tab" href="#ex5-step-1"
                                           role="tab">
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="ex5-step-1" role="tabpanel">
                                        <div class="task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="name"
                                                   placeholder="First Name">
                                        </div>
                                        <hr>
                                        <div class="task-desc-wrap">
                                            <textarea class="task-desc-field" name="desc"
                                                      placeholder="Description"></textarea>
                                        </div>
                                    </div>

                                </div>

                                <div class="pager d-flex justify-content-center">
                                    <input type="submit" id="finish-btn" class="finish btn btn-success w-50"
                                           value="Finish"/>
                                    <button type="button" id="next-btn" class="next btn btn-success w-50">Next
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {!!Form::close()!!}
            </div>
        </div>
    </div>

@stop
@section('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/examples/js/demos/form.wizard.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $(".js-example-responsive").select2({
            width: 'resolve' // need to override the changed default
        });


        $('.delete').click(function (e) {
            e.preventDefault();
            id = $(this).attr("value");
            confirmAlert(id);

        });

        function confirmAlert(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "delete",
                        url: id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            }
                            else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

    </script>
@stop