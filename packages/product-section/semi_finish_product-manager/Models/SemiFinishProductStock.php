<?php

namespace SemiFinishProductManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SemiFinishProductStock extends Model
{
    use SoftDeletes;

    protected $table = 'semi_finish_product_stock';

    protected $primaryKey = 'id';

    protected $dates = ['deleted_at'];

//    public function recipes(){
//       return $this->belongsToMany('RecipeManager\Models\Recipe');
//    }
//
//    public function endProducts(){
//        return $this->belongsToMany('EndProductManager\Models\EndProduct');
//    }

}
