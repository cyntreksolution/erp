<?php

namespace SemiFinishProductManager\Models;

use App\RecipeSemiFinish;
use BurdenManager\Models\BurdenGrade;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use RecipeManager\Models\Recipe;
use ShortEatsManager\Models\CookingRequest;

class SemiFinishProduct extends Model
{
    use SoftDeletes;

    protected $table = 'semi_finish_product';

    protected $primaryKey = 'semi_finish_product_id';

    protected $dates = ['deleted_at'];

    public function recipes()
    {
        return $this->belongsToMany('RecipeManager\Models\Recipe');
    }

    public function endProducts()
    {
        return $this->belongsToMany('EndProductManager\Models\EndProduct');
    }

    public function burden()
    {
        return $this->belongsToMany('BurdenManager\Models\Burden', 'semi_finish_product_id', 'semi_finish_product_id');
    }

    public function stockSemiFinishProduct()
    {
        return $this->belongsTo('StockManager\Models\StockSemiFinish', 'semi_finish_product_id', 'semi_finish_product_id');

    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('sort', function (Builder $builder) {
            $builder->orderBy('name', 'asc');
        });
    }

    public function scopeGradeABurdenList($query, $semiFinishProduct)
    {
        $semi = SemiFinishProduct::find($semiFinishProduct);
        if ($semi->is_bake == 1) {
            $burden = BurdenGrade::select(DB::raw('sum(burden_qty) as burden_qty'), DB::raw('sum(used_qty) as used_qty'), 'burden_grade.burden_id', 'created_at', 'id')->with('burden')
                ->whereHas('burden.semiProdcuts', function ($query) use ($semiFinishProduct) {
                    return $query->where('semi_finish_product_id', '=', $semiFinishProduct);
                })
                ->where('used_qty', '>=', 0.001)
                ->where('grade_id', '=', 1)
                ->groupBy('burden_id')
                ->orderBY('created_at');
        } else {
            $burden = CookingRequest::whereHas('semiProdcuts', function ($query) use ($semiFinishProduct) {
                return $query->where('semi_finish_product_id', '=', $semiFinishProduct);
            })->where('status', '=', 6)
                ->where('used_qty', '>=', 0.001);
        }

        return $burden;
    }


    public function price(SemiFinishProduct $semiFinishProduct)
    {
        $active_recipe = RecipeSemiFinish::where('semi_finish_product_semi_finish_product_id', '=', $semiFinishProduct->semi_finish_product_id)->where('status', '=', 2)->first();
        $recipe_id = $active_recipe->recipe_id;
        $units = $active_recipe->units;
        $price = 0;
        if (!empty($recipe_id)) {
            $recipe = Recipe::find($recipe_id);
            foreach ($recipe->recipeContents as $item) {
                $qty = $item->qty;
                $item_price = $item->materials->price;
                $units_per_pack = $item->materials->units_per_packs;
                $qt = $item_price / $units_per_pack;
                $price += $qt * $qty;
            }
        }
        return $price / $units;
    }
}
