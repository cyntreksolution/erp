<?php

namespace SemiFinishProductManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SemiFinishStock extends Model
{
    use SoftDeletes;

    protected $table = 'stock_semi_finish_product';

    protected $dates = ['deleted_at'];

}
