<?php

namespace SemiFinishProductManager\Controllers;

use App\SemiInquiry;
use App\Traits\SemiFinishLog;
use BurdenManager\Models\BurdenGrade;
use Carbon\Carbon;
use EndProductManager\Models\Category;
use EndProductManager\Models\EndProduct;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use SemiFinishProductManager\Models\RecipeSize;
use SemiFinishProductManager\Models\SemiCheck;
use Sentinel;
use Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use RecipeManager\Models\Recipe;
use SemiFinishProductManager\Models\SemiFinishProduct;
use BurdenManager\Models\Burden;
use BurdenManager\Models\BurdenPayment;
use ShortEatsManager\Models\CookingRequest;

class SemiFinishProductController extends Controller

{
    function __construct()
    {
        $this->middleware('permission:burden_payment_details-index', ['only' => ['burdenPayDetails']]);
        $this->middleware('permission:burden_payment_details-list', ['only' => ['list']]);
        $this->middleware('permission:burden_payment_details-stock', ['only' => ['index','stock']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    use SemiFinishLog;

    public function index()
    {
        $category = Category::all()->pluck('name', 'id');
        $chklist = SemiCheck::whereDate('created_at', Carbon::today())->groupBy('ref')->get()->pluck('ref', 'ref');
        return view('SemiFinishProductManager::semi_finish_product.index', compact('category','chklist'));//->with(['ref' => $chklist,        ]);
    }

    public function burdenPayDetails()
    {
        $semiproduct = SemiFinishProduct::orderBy('name')->get();
        $endproduct = EndProduct::orderBy('name')->get();


        return view('SemiFinishProductManager::semi_finish_product.payburden')->with([
            'products' => $semiproduct,
            'endproducts' => $endproduct
        ]);

    }

    public function burdenPayCreate()
    {

        $semiproduct = SemiFinishProduct::orderBy('name')->first();


        return view('SemiFinishProductManager::semi_finish_product.payburden-create',$semiproduct);
            //->with([
           // 'products' => $product
      //  ]);

    }



    public function usage()
    {
        return view('SemiFinishProductManager::semi_finish_product.usage');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product = SemiFinishProduct::orderBy('name')->get();
        return view('SemiFinishProductManager::semi_finish_product.create')->with([
            'products' => $product
        ]);
    }

    public function delpay($id)
    {
        $data = BurdenPayment::find($id);
        $semi_finish_product = SemiFinishProduct::where('semi_finish_product_id','=',$data->pro_id)->first();
        $data->delete();
        return redirect(route('semi_finish_products.show',$semi_finish_product));
    }

   public function enable_frac($semi_finish_product_id)
    {
        $data = SemiFinishProduct::find($semi_finish_product_id);
        $data->is_fractional = 1;
        $data->save();
        return redirect()->back();
    }

    public function disable_frac($semi_finish_product_id)
    {
        $data = SemiFinishProduct::find($semi_finish_product_id);
        $data->is_fractional = 0;
        $data->save();
        return redirect()->back();
    }

    public function act($semi_finish_product_id)
    {
        $data = SemiFinishProduct::find($semi_finish_product_id);
        $data->status = 1;
        $data->save();
        return redirect()->back();
    }

    public function inact($semi_finish_product_id)
    {
        $data = SemiFinishProduct::find($semi_finish_product_id);
        $data->status = 0;
        $data->save();
        return redirect()->back();
    }

    public function semiburden(Request $request)
    {
        $tp=1;//1=Semi
        $data = new BurdenPayment();
        $data->pro_id = $request->semi_id;
        $data->pro_type = $tp;
        $data->default_size = $request->default_size;
        $data->stage_id = $request->stage;

        $data->grade_a = $request->A;
        $data->grade_b = $request->B;
        $data->grade_c = $request->C;
        $data->grade_d = $request->D;

        $isin = BurdenPayment::where('pro_id','=',$request->semi_id)
                            ->where('pro_type','=',$tp)
                            ->where('stage_id','=',$request->stage)
                            ->first();
        if(empty($isin)) {

            $data->save();
        }

        $product = SemiFinishProduct::orderBy('name')->get();
        return view('SemiFinishProductManager::semi_finish_product.create')->with([
            'products' => $product
        ]);
    }

    public function stock(Request $request)
    {

        $data = SemiFinishProduct::get();
        $jsonList = array();
        $i = 1;
//        $date = \Carbon\Carbon::today()->subDays(30);


        foreach ($data as $key => $item) {

            $ldate = "No Checked";
            $lastcheck = SemiCheck::where('semi_id','=',$item->semi_finish_product_id)->where('status','=',2)->orderByDesc('id')->first();
            if(!empty($lastcheck)){
                $ldate1 = $lastcheck->created_at->format('Y-m-d H:m:s');
                $ldate = $ldate1.' ['.$lastcheck->ref.']';
                    //{{$ldate->format('Y-m-d H:m:s')}}
            }


            $dd = array();

            array_push($dd, $i);

            array_push($dd, $item->name);
            array_push($dd, $ldate);
            array_push($dd, number_format($item->available_qty,3));


            $burden_balance =$item->GradeABurdenList($item->semi_finish_product_id)->get()->sum('used_qty');
            array_push($dd, number_format($burden_balance,3));



            $color_class = (number_format($burden_balance,3) != number_format($item->available_qty,3)) ?'btn-danger' :'btn-success';

//            array_push($dd, '<a href="edit/' . $item->stockpile_id . '" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons">dvr</i></a>');
            // array_push($dd, ' <button value="' . $item->raw_material_id . '" class="delete btn btn-simple btn-danger btn-icon remove" data-toggle="tooltip"
            //                                     data-placement="left" title="DELETE END PRODUCT"><i class="fa fa-close"></i></button>');
            array_push($dd, " <a href='semi_finish_product/burden/view/$item->semi_finish_product_id'  class='btn btn-simple $color_class btn-icon search'><i class='fa fa-eye'></i></a>");
            array_push($jsonList, $dd);
            $i++;
        }
        return Response::json(array('data' => $jsonList));


    }


    public function updateAvailableStock(Request $request,$semi_finish_product){

        $data = SemiFinishProduct::where('semi_finish_product_id','=',$semi_finish_product)->with('stockSemiFinishProduct')->get();

       /* foreach ($data as $key => $item) {

            $burden_balance =$item->GradeABurdenList($item->semi_finish_product_id)->get()->sum('used_qty');

        }*/

        $sf = SemiFinishProduct::where('semi_finish_product_id','=',$semi_finish_product)->first();

        $dif = ($request->available_stock)-($sf->available_qty);

        $tp = 1;

        if($dif < 0){
            $tp = 2;
            $dif = $dif * (-1);
        }



        $semi_product = SemiFinishProduct::find($semi_finish_product);
        $this->recordSemiFinishProductOutstanding($semi_product,0,$dif, 'Admin Adjustment', 0, 'Admin\\Adjustment',0,'AdminAdjustments',$tp,0,0);

    //    $sf->save();
        return redirect(route('semi_finish_products.show',$semi_finish_product));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($request->image)) {
            $file = $request->file('image');
            $path = '/uploads/images/semi_finmished_products/';
            $destinationPath = storage_path($path);
            $file->move($destinationPath, $file->getClientOriginalName());

            $rw = new SemiFinishProduct();
            $rw->stock_id = 1;
            $rw->name = $request->name;
            $rw->is_bake = $request->is_bake;
            $rw->desc = $request->desc;
            $rw->buffer_stock = $request->buffer_stock;
            $rw->image = $file->getClientOriginalName();
            $rw->image_path = 'storage' . $path;
            $rw->colour = rand() % 7;
            $rw->created_by = Auth::user()->id;
            $rw->save();
            return redirect(route('semi_finish_products.create'));
        } else {
            $rw = new SemiFinishProduct();
            $rw->stock_id = 1;
            $rw->name = $request->name;
            $rw->is_bake = $request->is_bake;
            $rw->desc = $request->desc;
            $rw->buffer_stock = $request->buffer_stock;
            $rw->colour = rand() % 7;
            $rw->created_by = Auth::user()->id;
            $rw->save();
            return redirect(route('semi_finish_products.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \SemiFinishProductManager\Models\SemiFinishProduct $semi_finish_product
     * @return \Illuminate\Http\Response
     */
    public function show(SemiFinishProduct $semi_finish_product)
    {

        $semi_finish_product_id=$semi_finish_product->semi_finish_product_id;

        $data = SemiFinishProduct::where('semi_finish_product_id','=',$semi_finish_product_id)->with('stockSemiFinishProduct')
            ->get();

        foreach ($data as $key => $item) {



            $burden_balance =$item->GradeABurdenList($item->semi_finish_product_id)->get()->sum('used_qty');

        }


        $recipes = SemiFinishProduct::with('recipes.recipeContents')
            ->where('semi_finish_product_id', '=', $semi_finish_product->semi_finish_product_id)
            ->first();
        $endProducts = SemiFinishProduct::with('endProducts')
            ->where('semi_finish_product_id', '=', $semi_finish_product->semi_finish_product_id)
            ->first();

        $burdenPayments=BurdenPayment::where('pro_id', '=', $semi_finish_product_id)
                        ->where('pro_type', '=', 1)
                        ->get();


        return view('SemiFinishProductManager::semi_finish_product.show',compact('burden_balance'))->with([
            'recipes' => $recipes,
            'endProducts' => $endProducts,
            'burdenPayments'=>$burdenPayments
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \SemiFinishProductManager\Models\SemiFinishProduct $semi_finish_product
     * @return \Illuminate\Http\Response
     */
    public function edit(SemiFinishProduct $semi_finish_product)
    {
        return view('SemiFinishProductManager::semi_finish_product.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \SemiFinishProductManager\Models\SemiFinishProduct $semi_finish_product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SemiFinishProduct $semi_finish_product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \SemiFinishProductManager\Models\SemiFinishProduct $semi_finish_product
     * @return \Illuminate\Http\Response
     */
    public function destroy(SemiFinishProduct $semi_finish_product)
    {
        $semi_finish_product->delete();
        if ($semi_finish_product->trashed()) {
            return 'true';
        }else{
            return 'false';
        }    }

    public function getsemipro(Request $request){
        return $raw=SemiFinishProduct::find($request->id);

    }
    public function updateBS(Request $request,$semi_finish_product){
      $sf = SemiFinishProduct::where('semi_finish_product_id','=',$semi_finish_product)->first();
      $sf->buffer_stock = $request->bstock;
      $sf->save();
      return redirect(route('semi_finish_products.show',$semi_finish_product));
    }


    public function viewBurdens($semi_finish_product)
    {
      $burdens = Burden::where('semi_finish_product_id','=',$semi_finish_product)->get();
      $semiFinish = SemiFinishProduct::where('semi_finish_product_id','=',$semi_finish_product)->first();
      return view('SemiFinishProductManager::semi_finish_product.view_burden',compact('semiFinish'))->with([
          'burdens' => $burdens,
          'semiFinishProductName' => $semiFinish->name
      ]);
    }

    public function semiList()
    {
        return view('SemiFinishProductManager::semi_finish_product.semi_burden_balance');

    }

    public function semiBalList(Request $request)
    {


        $cat = Category::find($request->category);

        $ref1 = Carbon::now()->format('ymd');
        $ref = $cat->name.'-'.$ref1;


        return view('SemiFinishProductManager::semi_finish_product.semi_check_balance')->with([
            'ref' => $ref
       ]);

    }

    public function semiPrintList(Request $request)
    {


        $ref = $request->ref;

        return view('SemiFinishProductManager::semi_finish_product.semi_check_print')->with([
            'ref' => $ref
        ]);

    }


    public function semiBalCheckList(Request $request)
    {
        if($request->com == 0) {

            $semip = SemiFinishProduct::find($request->sid);
            if($semip->is_bake == 1) {
                $data = BurdenGrade::where('burden_id', '=', $request->bid)->where('grade_id', '=', 1)->first();
            }else{
                $data = CookingRequest::where('id', '=', $request->bid)->first();
            }
            $chk = SemiCheck::where('semi_id', '=', $request->sid)->where('burden_id', '=', $request->bid)->where('ref', '=', $request->ref)->first();
            if (empty($chk)) {
                $sm = new SemiCheck();
                $sm->ref = $request->ref;
                $sm->semi_id = $request->sid;
                $sm->burden_id = $request->bid;
                $sm->bal_qty = $data->used_qty;
                $sm->checked_by = Sentinel::getUser()->id;
                $sm->status = 1;
                $sm->save();
            }
        }else{

            $data = SemiCheck::where('burden_id', '=' , $request->bid)->where('semi_id', '=' , $request->sid)->where('ref', '=' , $request->ref)->where('status', '=', 1)->first();
            $data->delete();

        }
        $ref = $request->ref;

        return view('SemiFinishProductManager::semi_finish_product.semi_check_balance')->with([
            'ref' => $ref
        ]);
    }

  /*  public function semiBalUncheckList(Request $request)
    {

        $data = SemiCheck::where('burden_id', '=' , $request->bid)->where('semi_id', '=' , $request->sid)->where('ref', '=' , $request->ref)->where('status', '=', 1)->first();
        $data->delete();

        $ref = $request->ref;

        return view('SemiFinishProductManager::semi_finish_product.semi_check_balance')->with([
            'ref' => $ref
        ]);

    }*/

    public function semiConfirm(Request $request)
    {

        $chk = SemiCheck::where('semi_id','=',$request->sid)->where('ref','=',$request->ref)->get();
        if($chk->count() == 0){
            $sm = new SemiCheck();
            $sm->ref = $request->ref;
            $sm->semi_id = $request->sid;
            $sm->burden_id = 0;
            $sm->bal_qty = 0;
            $sm->checked_by = Sentinel::getUser()->id;
            $sm->status = 2;
            $sm->save();
        }else{
            foreach ($chk as $key => $item) {
                $sm = SemiCheck::find($item->id);
                $sm->status = 2;
                $sm->save();
            }
        }


        $inq = SemiInquiry::where('semi_id','=',$request->sid)->orderByDesc('id')->first();
        $inq->is_checked = 1;
        $inq->check_ref = $request->ref;
        $inq->save();

        $ref = $request->ref;

        return view('SemiFinishProductManager::semi_finish_product.semi_check_balance')->with([
            'ref' => $ref
        ]);
    }


    public function updateReusable(Request $request,$semi_finish_product)
    {
      $sf = SemiFinishProduct::where('semi_finish_product_id','=',$semi_finish_product)->first();
      $sf->reusable_type = $request->reusableType;
      $sf->save();
      return redirect(route('semi_finish_products.show',$semi_finish_product));
    }

    public function updateWeight(Request $request,$semi_finish_product)
    {
      $sf = SemiFinishProduct::where('semi_finish_product_id','=',$semi_finish_product)->first();
      $sf->weight = $request->weight;
      $sf->save();
      return redirect(route('semi_finish_products.show',$semi_finish_product));
    }


    public function availableSemiBurdenList(Request $request){
        $semi_id = $request->semi;
        $semiFinish = SemiFinishProduct::where('semi_finish_product_id','=',$semi_id)->first();
//        $burden_lists = $semi_product->GradeABurdenList($semi_id)->get();
        return View::make('SemiFinishProductManager::semi_finish_product.burden_list', compact('semiFinish'));
    }

    public function SemiSize(Request $request)
    {
        $gp = new RecipeSize();
        $gp->semi_id = $request->semi_id;
        $gp->burden_size = $request->burden_size;
        $gp->save();

        $product = SemiFinishProduct::orderBy('name')->get();
        return view('SemiFinishProductManager::semi_finish_product.create')->with([
            'products' => $product
        ]);


    }

    public function updateBurdenPayments(Request $request)
    {

        $id = $request->id;
        $data = BurdenPayment::find($id);
        $semi_finish_product = $request->pro_id;
        $data->grade_a = $request->A;
        $data->grade_b = $request->B;
        $data->grade_c = $request->C;
        $data->grade_d = $request->D;
        $data->save();


        return redirect(route('semi_finish_products.show',$semi_finish_product));
    }

    public function editSemiPaySt1(Request $request)
    {

        $pid = $request->pid;
        $data = BurdenPayment::find($pid);
        $data->grade_a = $request->A;
        $data->grade_b = $request->B;
        $data->grade_c = $request->C;
        $data->grade_d = $request->D;
        $data->save();


        return redirect(route('semi_finish_product.payburden'));
    }




}
