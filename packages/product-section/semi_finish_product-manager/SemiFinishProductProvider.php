<?php

namespace SemiFinishProductManager;

use Illuminate\Support\ServiceProvider;

class SemiFinishProductProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/Views', 'SemiFinishProductManager');
        require __DIR__ . '/web.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('SemiFinishProductManager', function($app){
            return new SemiFinishProductManager;
        });
    }
}
