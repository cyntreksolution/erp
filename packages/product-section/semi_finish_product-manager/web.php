<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web'])->group(function () {
    Route::prefix('semi_finish_product')->group(function () {
        Route::get('', 'SemiFinishProductManager\Controllers\SemiFinishProductController@index')->name('semi_finish_products.index');

        Route::get('semi/pay/delete/{id}', 'SemiFinishProductManager\Controllers\SemiFinishProductController@delpay')->name('dele.pay');

        Route::get('semi/burden/list', 'SemiFinishProductManager\Controllers\SemiFinishProductController@semiList')->name('semi-burden.list');

        Route::post('semi/burden/list/edit', 'SemiFinishProductManager\Controllers\SemiFinishProductController@editSemiPaySt1')->name('semi_burdenst1.edit');

        Route::post('end/burden/list/edit', 'SemiFinishProductManager\Controllers\SemiFinishProductController@editEndPaySt1')->name('end_burdenst1.edit');

        Route::get('semi/burden/print/list', 'SemiFinishProductManager\Controllers\SemiFinishProductController@semiPrintList')->name('semi-burden.print-list');

        Route::get('semi/bal/list', 'SemiFinishProductManager\Controllers\SemiFinishProductController@semiBalCheckList')->name('semi-balance.store');

        Route::get('semi/bal/confirm', 'SemiFinishProductManager\Controllers\SemiFinishProductController@semiConfirm')->name('semi-balance.confirm');

        // Route::get('semi/bal/unlist', 'SemiFinishProductManager\Controllers\SemiFinishProductController@semiBalUncheckList')->name('semi-balance.unstore');


        Route::get('/semi/balance/list/', 'SemiFinishProductManager\Controllers\SemiFinishProductController@semiBalList')->name('semi-balance.list');

        Route::get('create', 'SemiFinishProductManager\Controllers\SemiFinishProductController@create')->name('semi_finish_products.create');

       // Route::get('{semi_finish_product}', 'SemiFinishProductManager\Controllers\SemiFinishProductController@show')->name('semi_finish_products.show');

        Route::get('{semi_finish_product}/edit', 'SemiFinishProductManager\Controllers\SemiFinishProductController@edit')->name('semi_finish_products.edit');


        Route::post('', 'SemiFinishProductManager\Controllers\SemiFinishProductController@store')->name('semi_finish_products.store');

        Route::post('semiburden', 'SemiFinishProductManager\Controllers\SemiFinishProductController@semiburden')->name('semi_finish_products.semiburden');


        Route::get('raw/list', 'SemiFinishProductManager\Controllers\SemiFinishProductController@stock')->name('semi_finish_products.stock');

        Route::get('get/semipro', 'SemiFinishProductManager\Controllers\SemiFinishProductController@getsemipro')->name('semi_finish_products.store');

        Route::put('{semi_finish_product}', 'SemiFinishProductManager\Controllers\SemiFinishProductController@update')->name('semi_finish_products.update');

        Route::delete('{semi_finish_product}', 'SemiFinishProductManager\Controllers\SemiFinishProductController@destroy')->name('semi_finish_products.destroy');

        Route::post('updateBS/{semi_finish_product}', 'SemiFinishProductManager\Controllers\SemiFinishProductController@updateBS')->name('semi_finish_products.updateBS');
        Route::post('updateAC/{semi_finish_product}', 'SemiFinishProductManager\Controllers\SemiFinishProductController@updateAvailableStock')->name('semi_finish_products.updateBS');

        Route::get('burden/view/{semi_finish_product}', 'SemiFinishProductManager\Controllers\SemiFinishProductController@viewBurdens')->name('semi_finish_products.burdens');

        Route::post('updateReusable/{semi_finish_product}', 'SemiFinishProductManager\Controllers\SemiFinishProductController@updateReusable')->name('semi_finish_products.updateReusable');

        Route::post('updateWeight/{semi_finish_product}', 'SemiFinishProductManager\Controllers\SemiFinishProductController@updateWeight')->name('semi_finish_products.updateWeight');

        Route::get('semi/burden/available/list/{semi}', 'SemiFinishProductManager\Controllers\SemiFinishProductController@availableSemiBurdenList')->name('semi_finish_products.availableList');

        Route::post('semi/size', 'SemiFinishProductManager\Controllers\SemiFinishProductController@SemiSize')->name('semi.size');

        Route::get('{semi_finish_product}', 'SemiFinishProductManager\Controllers\SemiFinishProductController@show')->name('semi_finish_products.show');

        Route::get('frac/dis/able/{semi_finish_product_id}', 'SemiFinishProductManager\Controllers\SemiFinishProductController@disable_frac');

        Route::get('frac/en/able/{semi_finish_product_id}', 'SemiFinishProductManager\Controllers\SemiFinishProductController@enable_frac');

        Route::get('status/inact/{semi_finish_product_id}', 'SemiFinishProductManager\Controllers\SemiFinishProductController@inact');

        Route::get('status/act/{semi_finish_product_id}', 'SemiFinishProductManager\Controllers\SemiFinishProductController@act');

        Route::post('/semi_finish_product/payment/{id}', 'SemiFinishProductManager\Controllers\SemiFinishProductController@updateBurdenPayments')->name('semi_finish_products.payments');

    });
});




