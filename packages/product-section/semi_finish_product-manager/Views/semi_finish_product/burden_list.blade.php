<div class="media content">
    <div class="media-body">
        <h6 class="project-name" id="{{$semiFinish->id}}">{{$semiFinish->name}}</h6>
    </div>
    <div class="col-md-3">
        Expected QTY : <label
                id="label_{{$semiFinish->semi_finish_product_id}}"> </label>
        <input type="hidden" id="semi_loaded_qty_{{$semiFinish->semi_finish_product_id}}"
               value="" class="semi-loaded-class">
    </div>
    <div class="col-md-3">
        Remain QTY : <label
                id="label_remain_{{$semiFinish->semi_finish_product_id}}"> </label>
    </div>
</div>

@foreach($semiFinish->GradeABurdenList($semiFinish->semi_finish_product_id)->get() as $grade)
    @if($semiFinish->is_bake ==1)
        @php
            $availble_bake =($grade->used_qty == $grade->burden_qty)?number_format((float)(round($grade->burden_qty,3)), 3, '.', ''):number_format((float)(round($grade->used_qty,3)), 3, '.', '')
        @endphp
        @if ($availble_bake>0.000)
            <div class="row burden mt-2">
                <div class="col-md-2 text-left">
                    <input data-typex="burden_request" data-grade="{{$grade->id}}"
                           data-semi="{{$semiFinish->semi_finish_product_id}}"
                           type="checkbox" onclick="checkBoxClick(this)" class="form-control"
                           name="burden_id[{{$grade->id}}]">
                </div>
                <div class="col-md-4">
                    {{$grade->burden->serial}}
                </div>
                <div class="col-md-3">
                    <label id="burden_expected_qty_{{$grade->id}}"> {{($grade->used_qty == $grade->burden_qty)?number_format((float)($grade->burden_qty), 3, '.', ''):number_format((float)($grade->used_qty), 3, '.', '') }}</label>
                </div>
                <div class="col-md-3">
                    <input type="number" value="0" step="0.001" name="burden_qty[{{$grade->id}}]"
                           id="burden_request_qty_{{$grade->id}}"
                           class="form-control burden_request_qty_{{$grade->id}}">
                </div>
            </div>
        @endif

    @else
        @php
            $available_qty = ($grade->used_qty == $grade->actual_semi_product_qty)?number_format((float)(round($grade->actual_semi_product_qty,3)), 3, '.', ''): number_format((float)(round($grade->used_qty,3)), 3, '.', '');
        @endphp
        @if ($available_qty > 0.000)
            <div class="row burden mt-2">
                <div class="col-md-2 text-left">
                    <input data-typex="cooking_request" data-grade="{{$grade->id}}"
                           data-semi="{{$semiFinish->semi_finish_product_id}}"
                           type="checkbox" class="form-control" onclick="checkBoxClick(this)"
                           name="cooking_request_id[{{$grade->id}}]">
                </div>
                <div class="col-md-4">
                    {{$grade->serial}}
                </div>
                <div class="col-md-3">
                    <label id="cooking_expected_qty_{{$grade->id}}"> {{($grade->used_qty == $grade->actual_semi_product_qty)?number_format((float)($grade->actual_semi_product_qty), 3, '.', ''): number_format((float)($grade->used_qty), 3, '.', '')}}</label>
                </div>
                <div class="col-md-3">
                    <input type="number" value="0" step="0.001"
                           id="cooking_request_qty_{{$grade->id}}"
                           name="cooking_request_qty[{{$grade->id}}]"
                           class="form-control cooking_request_qty_{{$grade->id}}">
                </div>
            </div>
        @endif
    @endif
@endforeach
