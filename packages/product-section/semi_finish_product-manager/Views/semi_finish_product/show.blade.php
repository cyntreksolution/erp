@extends('layouts.back.master')@section('title','Semi Finish Product')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}">


        <link rel="stylesheet" href="{{asset('assets/examples/css/apps/modal.css')}}"/>
        <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
        <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/lightbox2/dist/css/lightbox.min.css')}}"/>


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .app-wrapper {
            font-family: Raleway, sans-serif;
            width: 100%;
            border: 1px solid #eee;
        }
    </style>
@stop
@section('current','Semi Finish Products')
@section('current_url',route('semi_finish_products.create'))
@section('current2',$recipes['name'])
@section('current_url2','')
@section('content')
    <div class="app-wrapper">
        <div class="profile-section-user" id="app-panel">
            <div class="profile-cover-img">
                <img src="{{asset('assets\img\semi2.jpg')}}" alt="">
            </div>


            <div class="profile-info-brief p-3">
                @if(isset($recipes->image))
                    <div class="user-profile-avatar avatar avatar-circle avatar-sm">
                        <img src="{{asset($recipes->image_path.$recipes->image)}}" alt="">
                    </div>
                @else
                    @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                    <div class="user-profile-avatar avatar avatar-circle avatar-md project-iconr bg-{{$color[$recipes->colour]}}"
                         data-plugin="firstLitter"
                         data-target="#{{$recipes->id*754}}"></div>
                @endif
                <div class="text-center">
                    @if(isset($material->image))
                        <button class=" btn btn-outline-primary btn-sm btn-rounded my-3">
                            <i class="fa fa-pencil px-1"></i>
                            CHANGE IMAGE
                        </button>
                    @else
                        <button class=" btn btn-outline-primary btn-sm btn-rounded my-3">
                            <i class="fa fa-pencil px-1"></i>
                            NEW IMAGE
                        </button>
                    @endif
                    <h5 class="text-uppercase mb-4" id="{{$recipes->id*754}}">{{$recipes['name']}}</h5>
                    <div class="hidden-sm-down ">
                        <div class="my-1">
                            <i data-toggle="tooltip" data-placement="left" title="Buffer Stock"
                               class="fa fa-briefcase"></i> Buffer Stock {{ $recipes['buffer_stock']}} units
                               <a href="#" class="btn btn-light btn-sm" data-target="#EditBstock" data-toggle="modal">
                                   <i class="zmdi zmdi-edit"></i>
                               </a>
                        </div><br>
                        <div class="info">
                            <i data-toggle="tooltip" data-placement="left" title="Availble Quantity"
                               class="fa fa-archive"></i> Available Qty {{ number_format($recipes['available_qty'],3)}} units
                            <a href="#" class="btn btn-light btn-sm" data-target="#EditAvailableStock" data-toggle="modal">
                                <i class="zmdi zmdi-edit"></i>
                            </a>
                        </div><br>
                        <div class="info">
                            <i data-toggle="tooltip" data-placement="left" title="Burden Quantity"
                               class="fa fa-archive"></i> Burden Qty {{number_format($burden_balance,3)}} units


                        </div><br>
                        <div class="info">
                            <i data-toggle="tooltip" data-placement="left" title="Reusable Type"
                               class="fa fa-archive"></i> Reusable Type :
                                 @if($recipes['reusable_type'] == 1)
                                    None
                                 @endif
                                 @if($recipes['reusable_type'] == 2)
                                    Reusable Bun
                                 @endif
                                 @if($recipes['reusable_type'] == 3)
                                    Reusable Bread
                                 @endif
                                 @if($recipes['reusable_type'] == 4)
                                    Reusable Cake
                                 @endif
                            <a href="#" class="btn btn-light btn-sm" data-target="#EditReusableType" data-toggle="modal">
                                <i class="zmdi zmdi-edit"></i>
                            </a>
                        </div><br>
                        <div class="info">
                            <i data-toggle="tooltip" data-placement="left" title="Weight"
                               class="fa fa-archive"></i> Weight {{ number_format($recipes['weight'],3)}} g
                            <a href="#" class="btn btn-light btn-sm" data-target="#EditWeight" data-toggle="modal">
                                <i class="zmdi zmdi-edit"></i>
                            </a>
                        </div>
                        <hr>
                        <div>
                        <td>
                            @if($recipes->is_fractional)

                              <a href="frac/dis/able/{{$recipes->semi_finish_product_id}}" class="btn btn-danger">Fraction Disable</a>

                            @else

                               <a href="frac/en/able/{{$recipes->semi_finish_product_id}}" class="btn btn-warning">Fraction Enable</a>

                            @endif


                                @if($recipes->status)

                                    <a href="status/inact/{{$recipes->semi_finish_product_id}}" class="btn btn-danger">Inactive</a>

                                @else

                                    <a href="status/act/{{$recipes->semi_finish_product_id}}" class="btn btn-warning">Active</a>

                                @endif

                          </td>
                          </div>
                      </div>
                  </div>
              </div>
              <!-- Models -->
              <div class="modal fade" id="EditBstock" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
                   aria-hidden="true">
                  <div class="modal-dialog" style="margin-top: 200px" role="document">
                      <div class="modal-content">
                          {{ Form::open(array('url' => 'semi_finish_product/updateBS/'.$recipes['semi_finish_product_id'], 'id'=>'bstock')) }}
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                                  <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                              </button>
                          </div>
                          <div class="modal-body">
                              <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                                  <div class="row task-name-wrap">
                                      <span><i class="zmdi zmdi-check"></i></span>
                                      <input name="bstock" type="text" class="task-name-field"
                                             value="{{$recipes['buffer_stock']}}">
                                  </div>

                              </div>
                          </div>

                          <div class="modal-footer">
                              <input type="submit" class="btn btn-success btn-block" value="UPDATE">
                          </div>
                          {{ Form::close() }}
                      </div>
                  </div>
              </div>
              <!-- CloseModel -->
              <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show"><i
                          class="fa fa-chevron-right"></i>
                  <i class="fa fa-chevron-left"></i>
              </a>
          </div>
          <div class="app-main">
              <div class="app-main-header">
                  <h5 class="app-main-heading text-center">{{$recipes['name']}} Recipes</h5>
              </div>
              <div class="scroll-container" id="scroll-container">
                  <div class="app-main-content">
                      <div class="container">
                          <div class="media-list">
                              @if(sizeof($recipes['recipes'])>0)
                                  @foreach($recipes['recipes'] as $recipe)
                                      {{--<a href="{{$recipe->id.'/edit'}}">--}}
                                    <div class="media">
                                        @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                        <div class="avatar avatar avatar-md project-icon bg-{{$color[$recipe->colour]}}"
                                             data-plugin="firstLitter"
                                             data-target="#{{$recipe->id*754}}"></div>
                                        <div class="media-body">
                                            <h6 class="project-name"
                                                id="{{$recipe->id*754}}">{{$recipe->name}}</h6>
                                            {{--<small class="project-detail">{{$recipe->desc}}</small><br>--}}
                                            @php
                                                $act= RecipeManager\Models\RecipeSemiProduct::where('recipe_id','=',$recipe->id)->first();
                                            @endphp
                                            @if($act->status==2)
                                                <i class="status status-online"></i><span
                                                        class="text-success">Active Recipe</span>
                                            @else
                                                <i class="status status-busy"></i><span
                                                        class="text-danger">Inactive Recipe</span>
                                            @endif
                                        </div>
                                        <div class="media-body">
                                            <h6 class="text mb-lg-1">Units per Recipe : {{$act->units}} units</h6>
                                        </div>
                                        <div class="col-md-3">
                                            <button class="btn btn-outline-primary" type="button" data-toggle="collapse"
                                                    data-target="#multiCollapseExample{{$recipe['id']}}"
                                                    aria-expanded="false"
                                                    aria-controls="multiCollapseExample{{$recipe['id']}}"
                                                    style="width: 100%; border-color: transparent">
                                        <span>
                                        <i class="fa fa-shopping-bag" aria-hidden="true"></i>
                                        </span> Ingredients
                                            </button>
                                            {{--@if($act->status==2)--}}
                                            {{--<input type="checkbox" class="one" data-plugin="switchery"--}}
                                            {{--data-size="medium" value="{{$act->id}}" checked>--}}
                                            {{--@else--}}
                                            {{--<input type="checkbox" class="one" data-plugin="switchery"--}}
                                            {{--data-size="medium" value="{{$act->id}}">--}}
                                            {{--@endif--}}

                                        </div>
                                        <div class="col-md-1">
                                            @if((!empty($act->status)) && $act->status==2)
                                                <input type="checkbox" class="one" data-plugin="switchery"
                                                       data-size="medium" value="{{$act->id}}" checked="true">
                                            @else
                                                <input type="checkbox" class="one" data-plugin="switchery"
                                                       data-size="medium" value="{{$act->id}}">
                                            @endif
                                        </div>
                                        {{--<div class="section-2">--}}
                                        {{--<button class="delete btn btn-light btn-sm ml-2" value="{{$recipe->id}}"--}}
                                        {{--data-toggle="tooltip"--}}
                                        {{--data-placement="left" title="DELETE ASSISTANT">--}}
                                        {{--<i class="zmdi zmdi-close"></i>--}}
                                        {{--</button>--}}
                                        {{--</div>--}}
                                    </div>
                                    <div class="col-md-12">
                                        <div class="collapse" id="multiCollapseExample{{$recipe['id']}}" style="">
                                            <div class="card-body">
                                                <div class="row burden">
                                                        <div class="col-md-2">
                                                        </div>
                                                        <div class="col-md-4">
                                                        </div>
                                                        <div class="col-md-2">
                                                            <center><b>Unit type</b></center>

                                                        </div>
                                                        <div class="col-md-2">
                                                            <center><b>Units</b></center>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <center><b>Price</b></center>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    @php $totalUnitPrice = 0;@endphp
                                                @foreach($recipe->recipeContents as $r)

                                                    <div class="row burden">
                                                        <div class="col-md-2">
                                                            <span><i class="fa fa-shopping-bag"></i> </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                        {{$r['materials']->name}}
                                                            @php
                                                                $unitprice = 0;
                                                                if($r['materials']->units_per_packs != 0 || !empty($r['materials']->units_per_packs)){
                                                                $unitprice=$r['materials']->price/$r['materials']->units_per_packs;
                                                                }
                                                            @endphp

                                                        </div>
                                                        <div class="col-md-2">
                                                            @if($r['materials']->measurement=='gram')
                                                                @if($r['qty'] >= 1000)
                                                                    <h6><center>kg</center></h6>
                                                                @else
                                                                    <h6><center>g</center></h6>
                                                                @endif

                                                            @elseif($r['materials']->measurement=='milliliter')
                                                                @if($r['qty'] >= 1000)
                                                                    <h6><center>l</center></h6>
                                                                @else
                                                                    <h6><center>ml</center></h6>
                                                                @endif
                                                            @elseif($r['materials']->measurement=='unit')
                                                                <h6><center>units</center></h6>
                                                            @endif

                                                        </div>
                                                        <div class="col-md-2">
                                                            @if($r['materials']->measurement=='gram')
                                                                @if($r['qty'] >= 1000)
                                                                    <h6><center>{{$r['qty']/1000}}</center></h6>
                                                                @else
                                                                    <h6><center>{{$r['qty']}}</center></h6>
                                                                @endif

                                                            @elseif($r['materials']->measurement=='milliliter')
                                                                @if($r['qty'] >= 1000)
                                                                    <h6><center>{{$r['qty']/1000}}</center></h6>
                                                                @else
                                                                    <h6><center>{{$r['qty']}}</center></h6>
                                                                @endif
                                                            @elseif($r['materials']->measurement=='unit')
                                                                <h6><center>{{$r['qty']}}</center></h6>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-2" style="text-align: right;">
                                                            <h6>{{number_format((float)$unitprice*$r['qty'], 2, '.', '')}}</h6>
                                                            @php $totalUnitPrice = $totalUnitPrice + ($unitprice*$r['qty']);@endphp
                                                        </div>
                                                    </div>
                                                @endforeach
                                                <hr>
                                                <div class="row burden">
                                                        <div class="col-md-10" style="text-align: right;">
                                                        <h6>Total   :</h6>
                                                        <br>
                                                        <h6>Unit Price   :</h6>
                                                        </div>
                                                        <div class="col-md-2" style="text-align: right;">
                                                            <h6>{{number_format((float)$totalUnitPrice, 3, '.', '')}}</h6>
                                                            <br>
                                                            <h6>{{number_format((float)$totalUnitPrice/$act->units, 3, '.', '')}}</h6>
                                                        </div>
                                                    </div>

                                            </div>

                                        </div>
                                    </div>
                                    {{--</a>--}}

                                @endforeach
                            @else
                                <div class="media">

                                    <div class="media-body">
                                        <h6 class="project-name">No Recipes</h6>
                                    </div>
                                </div>
                            @endif

                                  <br>
                                  <div class="col-md-12">
                                      <table class="table table-dark">
                                          <thead class="thead-light">
                                          <th>Stage</th>
                                          <th>Grade A</th>
                                          <th>Grade B</th>
                                          <th>Grade C</th>
                                          <th>Grade D</th>
                                          <th>Action</th>
                                          </thead>
                                          <tbody>

                                            @foreach($burdenPayments as $b)
                                                <tr>
                                                    @if($b->stage_id ==1)
                                                        <td>Stage One</td>
                                                    @else
                                                        <td>Stage Two</td>
                                                    @endif

                                                <td>{{$b->grade_a}}</td>
                                                <td>{{$b->grade_b}}</td>
                                                <td>{{$b->grade_c}}</td>
                                                <td>{{$b->grade_d}}</td>
                                                    @php


                                                    $count = \BurdenManager\Models\BurdenPayment::where('pro_id','=',$b->pro_id)
                                                                            ->where('pro_type','=',$b->pro_type)
                                                                            ->where('stage_id','=',$b->stage_id )
                                                                            ->count();

                                                    @endphp
                                                    @if($count == 1)
                                                        <td><button data-toggle="modal"  data-target="#semiburden{{$b->id}}" class="btn btn-sm btn-success">Edit</button>

                                                    @else
                                                        <td><button data-toggle="modal"  data-target="#semiburden{{$b->id}}" class="btn btn-sm btn-success">Edit</button>
                                                        <a href="{{route('dele.pay',['id'=>$b->id])}}" class="btn btn-sm btn-danger mr-2"><i class="fa fa-recycle"></i> </a>
                                                    @endif



                                                </td>
                                                </tr>

                                                <div class="modal fade" id="semiburden{{$b->id}}" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            {!! Form::open(['route' => ['semi_finish_products.payments',$b->id], 'method' => 'post','style'=>'width:100%']) !!}
                                                            <input name="pro_id" type="hidden" value="{{$b->pro_id}}">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                                                                    <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">



                                                                <div class="row task-name-wrap">
                                                                    <label class="col-8 col-form-label">Burden Units </label>
                                                                    <input class="form-control" type="number" name="default_size"
                                                                           value=1 readonly>
                                                                </div>

                                                                <!--div class="task-name-wrap">

                                                                    <input class="form-control" type="number" name="default_size"
                                                                           value=1 readonly>
                                                                </div-->


                                                                <div class="task-name-wrap">
                                                                    <label class="col-8 col-form-label">Product Type </label>
                                                                    <input class="form-control" type="text" name="ptype"
                                                                           value="{{$b->pro_type}}" readonly>
                                                                </div>

                                                                <div class="task-name-wrap">
                                                                    <label class="col-8 col-form-label">Stage </label>
                                                                    <input class="form-control" type="text" name="st"
                                                                           value="{{$b->stage_id}}" readonly>
                                                                </div>




                                                                <div class="task-name-wrap">
                                                                    <label class="col-8 col-form-label">Pay Value of Grade A </label>
                                                                    <input class="form-control" type="number" name="A" step="0.01"
                                                                           value="{{$b->grade_a}}" >
                                                                </div>


                                                                <div class="task-name-wrap">
                                                                    <label class="col-8 col-form-label">Pay Value of Grade B </label>
                                                                    <input class="form-control" type="number" name="B" step="0.01"
                                                                           value="{{$b->grade_b}}" >
                                                                </div>


                                                                <div class="task-name-wrap">
                                                                    <label class="col-8 col-form-label">Pay Value of Grade C </label>
                                                                    <input class="form-control" type="number" name="C" step="0.01"
                                                                           value="{{$b->grade_c}}" >
                                                                </div>


                                                                <div class="task-name-wrap">
                                                                    <label class="col-8 col-form-label">Pay Value of Grade D </label>
                                                                    <input class="form-control" type="number" name="D" step="0.01"
                                                                           value="{{$b->grade_d}}" >
                                                                </div>


                                                                {{--<div class="task-name-wrap">--}}
                                                                {{--<span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>--}}
                                                                {{--<input class="task-name-field" type="text" name="desc"--}}
                                                                {{--placeholder="Description">--}}
                                                                {{--</div>--}}
                                                                {{--<hr>--}}

                                                            </div>

                                                            <div class="pager d-flex justify-content-center">
                                                                <input type="submit" id="finish-btn" class="finish btn btn-success btn-block"
                                                                       value="Update">

                                                            </div>



                                                            {!!Form::close()!!}
                                                        </div>
                                                    </div>
                                                </div>

                                            @endforeach

                                          </tbody>


                                      </table>
                                  </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>




    <div class="modal fade" id="EditAvailableStock" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'semi_finish_product/updateAC/'.$recipes['semi_finish_product_id'], 'id'=>'bstock')) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="available_stock" type="number" step="0.001" class="task-name-field"
                                   value="{{$burden_balance}}"

                            >
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="UPDATE">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <div class="modal fade" id="EditReusableType" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'semi_finish_product/updateReusable/'.$recipes['semi_finish_product_id'], 'id'=>'bstock')) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                        <div class="col-lg-2">
                            <span><i class="zmdi zmdi-check"></i></span>
                          </div>
                            <div class="col-lg-10">
                            <select class="form-control" name="reusableType">
                              <option value="1" <?=$recipes['reusable_type'] == '1' ? ' selected="selected"' : '';?>>None</option>
                              <option value="2" <?=$recipes['reusable_type'] == '2' ? ' selected="selected"' : '';?>>Reusable Bun</option>
                              <option value="3" <?=$recipes['reusable_type'] == '3' ? ' selected="selected"' : '';?>>Reusable Bread</option>
                              <option value="4" <?=$recipes['reusable_type'] == '4' ? ' selected="selected"' : '';?>>Reusable Cake</option>
                            </select>
                          </div>
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="UPDATE">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <div class="modal fade" id="EditWeight" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'semi_finish_product/updateWeight/'.$recipes['semi_finish_product_id'], 'id'=>'bstock')) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                        <div class="col-lg-2">
                            <span><i class="zmdi zmdi-check"></i></span>
                          </div>
                            <div class="col-lg-10">
                              <input name="weight" type="number" step="0.001" class="task-name-field"
                                     value="{{number_format($recipes['weight'],3)}}">
                          </div>
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="UPDATE">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script>

        $('.one').change(function (e) {
            var id = $(this).attr("value");
            if ($(this).is(":checked")) {
                swal({
                        title: "Are you sure?",
                        text: "Change Active recipe ?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: "Yes, change it!",
                        closeOnConfirm: false
                    },
                    function () {
                        $.ajax({
                            type: "post",
                            url: '/../recipe/active/new',
                            data: {id: id},
                            success: function (response) {
                                if (response == 'true') {
                                    swal("changed!", "Recipe chnage successfully", "success");
                                    setTimeout(location.reload.bind(location), 90);
                                }
                                else {
                                    swal("Error!", "Something went wrong", "error");
                                }
                            }
                        });
                    }
                );
            }
            else {
                swal({
                        type: 'error',
                        title: 'No Active Recipes',
                        showConfirmButton: false,
                        timer: 1500
                    },
                    function () {
                        setTimeout(location.reload.bind(location), 00);
                    }
                );
            }
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $('.delete').click(function (e) {
            e.preventDefault();
            id = $(this).attr("value");
            confirmAlert(id);

        });

        function confirmAlert(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "delete",
                        url: id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            }
                            else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

    </script>
@stop
