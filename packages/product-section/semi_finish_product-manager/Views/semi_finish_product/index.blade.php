@extends('layouts.back.master')@section('title','Check Semi Finish Product')
@section('css')
    <style type="text/css">
        #floating-button{
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #00C851;
            position: fixed;
            bottom: 80px;
            right: 30px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index:2
        }

        .plus{
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }
        .sml{
            height: 30px;
            width:50px;
            text-align: center;
            font-size: 0.75rem;
        }
    </style>

    {{--<link rel="stylesheet" href="{{asset('assets/vendor/bower_components/datatables.net/css/jquery.dataTables.min.css')}}">--}}
    {{--<link rel="stylesheet" href="{{asset('assets/vendor/dtable/Buttons-1.5.1/css/buttons.dataTables.css')}}">--}}
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/Buttons-1.5.1/css/buttons.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    {{--<link rel="stylesheet" href="{{asset('assets/vendor/dtable/DataTables-1.10.16/css/dataTables.bootstrap4.css')}}">--}}
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/datatables.net/css/buttons.dataTables.min.css')}}">
@stop
@section('content')


    <div class="app-main-content mb-2">
        <div class="row text-center mx-12">

            <div class="col-md-2">
                {!! Form::select('category',$category , null , ['class' => 'form-control','placeholder'=>'Select Category','id'=>'category']) !!}
            </div>

            <div class="col-md-2">
                {!! Form::select('ref',$chklist , null , ['class' => 'form-control','placeholder'=>'Select Reference','id'=>'ref']) !!}
            </div>


            <div class="col-md-8">
                 <a href="{{route('semi-burden.list')}}" class="btn btn-warning" target="_blank">Burden Wise View</a>
                <a onclick="chkPrint()" class="btn btn-info text-white" id="pntBTN" target="_blank">Print Checked List</a> {{--href="{{route('semi-burden.print-list')}}" --}}
                <a onclick="chkBal()" class="btn btn-danger text-white" id="bidBTN" target="_blank">Check</a>


            </div>

        </div>
    </div>

    @if (Sentinel::check()->roles[0]->slug == 'owner')
    <div id="floating-button" data-toggle="tooltip" data-placement="left" data-original-title="NEW SEMI FINISH PRODUCT" onclick="location.href = '{{url('semi_finish_product/create')}}';">
        <p class="plus">+</p>
    </div>
    @endif

            <table id="example" class="display text-center" >
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th class="text-center">Last Checked Date & Reference</th>
                    <th class="text-center">Current stock</th>
                    <th class="text-center">Current burden stock</th>
                    <th class="text-center">Action</th>

                </tr>
                </thead><tfoot>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th class="text-center">Last Checked Date & Reference</th>
                    <th class="text-center">Current stock</th>
                    <th class="text-center">Current burden stock</th>
                    <th class="text-center">Action</th>

                </tr>
                </tfoot>
            </table>


@stop
@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/Buttons-1.5.1/js/dataTables.buttons.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/JSZip-2.5.0/jszip.min.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/pdfmake-0.1.32/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/Buttons-1.5.1/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js')}}"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        var table;
        $(document).ready(function () {
            table = $('#example').DataTable({
                responsive: true,
                "ajax": '{{url('/semi_finish_product/raw/list')}}',
                dom: 'lfBrtip',

                buttons: [
                    {
                        extend: 'copy',
                        className: 'btn btn-xs btn-info ml-3 sml assets-export-btn export-copy ttip',
                    },

                    { extend: 'excel',
                        className: 'btn btn-xs btn-success ml-3 sml assets-export-btn export-copy ttip',
                    },
                    { extend: 'pdf',
                        className: 'btn btn-xs btn-danger ml-3 sml assets-export-btn export-copy ttip',
                    },

                    { extend: 'print',
                        className: 'btn btn-xs btn-primary ml-3 sml assets-export-btn export-copy ttip',
                    },
                ]

            });

            $('#example tbody').on( 'click', 'td button', function (e) {
                // var data = table.row( $(this).parents('tr') ).data();
                e.preventDefault();
                var id = $(this).attr("value");
                confirmAlert(id);

            } );
        });


        function chkBal()
        {
            let category = $("#category").val();
            window.location.href = "/semi_finish_product/semi/balance/list?category="+ category;
        }

        function chkPrint()
        {
            let ref = $("#ref").val();
            window.location.href = "/semi_finish_product/semi/burden/print/list?ref="+ ref;
        }


        /* function chkBal(e) {

             var optionSelected = $("option:selected", this);
             var valueSelected = this.value;

             $.ajax({
                 type: "get",
                 url : '/semi_finish_product/semi/balance/list' ,
                 data: {category: valueSelected},

                 error:function(error){
                     console.log(error)
                 }
             });
         }

           function chkBal() {
            let category = $("#category").val();
            table.ajax.url('/semi_finish_product/semi/balance/list?category=' +category ).load();
        }*/


        $(document).ready(function () {
            $("#bidBTN").hide();
            $("#pntBTN").hide();

        });

        $('#category').on('change', function (e) {
            let category = $("#category").val();
            if (category == '') {

                $("#bidBTN").hide();

            }else {
                $("#bidBTN").show();

            }

        }).trigger("change");

        $('#ref').on('change', function (e) {
            let ref = $("#ref").val();
            if (ref == '') {

                $("#pntBTN").hide();

            }else {
                $("#pntBTN").show();

            }

        }).trigger("change");



        function confirmAlert(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "delete",
                        url: id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            }
                            else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

    </script>
@stop
