
@extends('layouts.back.master')@section('title','Burden Payment Details')
@section('css')

    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/modal.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.wizard.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.validation.css')}}">
    <link rel="stylesheet" href="{{asset('assets/packages/raw_materials/raw_material.css')}}">

    <style>


    </style>
@stop
{{--@section('current','Cash Book')--}}
{{--@section('current_url',route('raw_materials.create'))--}}

@section('content')
    <html>

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">


        <title> Payment Type </title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
        <script src="main.js"></script>
    </head>
    <body>
    <div class="container">
        <div class="text-center">
            <br>
            <h5>Payments For Semi Finish Products</h5>


            <div class="row" >

                <div class="col-md-12">
                    <table class="table table-dark">
                        <thead class="thead-light">
                        <th>Semi Finish Product</th>
                        <th colspan="4" class="text-center bg-dribbble">First Stage</th>
                        <th>Action</th>
                        <th colspan="4" class="text-center bg-yellow">Second Stage</th>
                        <th>Action</th>
                        </thead>

                        <thead class="thead-light">
                        <th style=""></th>

                        <th class="text-center  bg-dribbble">A</th>
                        <th class="text-center bg-dribbble">B</th>
                        <th class="text-center bg-dribbble">C</th>
                        <th class="text-center bg-dribbble">D</th>
                        <th ></th>
                        <th class="text-center bg-yellow">A</th>
                        <th class="text-center bg-yellow">B</th>
                        <th class="text-center bg-yellow">C</th>
                        <th class="text-center bg-yellow">D</th>
                        <th ></th>
                        </thead>

                        <tr>
                        @php $ck=0;  @endphp
                        @foreach($products as $key => $product)
                            @php
                            $sbid1 = null;
                            $payitem1 = \BurdenManager\Models\BurdenPayment::where('pro_id','=',$products[$key]->semi_finish_product_id)
                                                                        ->where('pro_type','=',1)
                                                                        ->where('stage_id','=',1 )
                                                                        ->first();


                            $payitem2 = \BurdenManager\Models\BurdenPayment::where('pro_id','=',$products[$key]->semi_finish_product_id)
                                                                        ->where('pro_type','=',1)
                                                                        ->where('stage_id','=',2 )
                                                                        ->first();

                            $fc_semi = \BurdenManager\Models\BurdenPayment::where('pro_id','=',$products[$key]->semi_finish_product_id)
                                                                        ->where('pro_type','=',1)
                                                                        ->where('stage_id','=',1 )
                                                                        ->count();

                             $sc_semi = \BurdenManager\Models\BurdenPayment::where('pro_id','=',$products[$key]->semi_finish_product_id)
                                                                        ->where('pro_type','=',1)
                                                                        ->where('stage_id','=',2 )
                                                                        ->count();




                           @endphp


                            <tr>
                                @if($fc_semi > 1  || $sc_semi > 1)
                                    <td class="text-center bg-danger">{{$products[$key]->name}}</td>
                                @else
                                    <td>{{$products[$key]->name}}</td>
                                @endif

                                <td class="text-center bg-dribbble">{{!empty($payitem1)?number_format($payitem1->grade_a,2):''}}</td>
                                <td class="text-center bg-dribbble">{{!empty($payitem1)?number_format($payitem1->grade_b,2):''}}</td>
                                <td class="text-center bg-dribbble">{{!empty($payitem1)?number_format($payitem1->grade_c,2):''}}</td>
                                <td class="text-center bg-dribbble">{{!empty($payitem1)?number_format($payitem1->grade_d,2):''}}</td>
                               @if(!empty($payitem1->id))
                                <td><a href="#" class="btn btn-light btn-sm" data-target="#semiburdenst1{{$payitem1->id}}" data-toggle="modal"><i class="zmdi zmdi-edit"></i></a></td>
                                @else
                                        <td></td>
                                @endif
                                <!--a class="btn btn-sm btn-warning" target="_blank" href="{{--'burden/payment/'.$products[$key]->semi_finish_product_id . '/' . $pt1 . '/' . $sid1--}}" >Edit</a-->
                                <td class="text-center  bg-yellow">{{!empty($payitem2)?number_format($payitem2->grade_a,2):''}}</td>
                                <td class="text-center  bg-yellow">{{!empty($payitem2)?number_format($payitem2->grade_b,2):''}}</td>
                                <td class="text-center  bg-yellow">{{!empty($payitem2)?number_format($payitem2->grade_c,2):''}}</td>
                                <td class="text-center  bg-yellow">{{!empty($payitem2)?number_format($payitem2->grade_d,2):''}}</td>
                                    @if(!empty($payitem2->id))
                                        <td><a href="#" class="btn btn-light btn-sm" data-target="#semiburdenst2{{$payitem2->id}}" data-toggle="modal"><i class="zmdi zmdi-edit"></i></a></td>
                                    @else
                                        <td></td>
                                    @endif
                            </tr>



                        @if(!empty($payitem1->id))
                            <div class="modal fade" id="semiburdenst1{{$payitem1->id}}" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
                                 aria-hidden="true">

                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                {{ Form::open(array('route' => ['semi_burdenst1.edit',$payitem1->id],'enctype'=>'multipart/form-data' ,'id'=>'bootstrap-wizard-form'))}}

                                                @php  @endphp
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                                                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="task-name-wrap">
                                                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                                        <input type="hidden" name="pid" value="{{$payitem1->id}}">
                                                        <input name="A" type="number" step="0.01" class="task-name-field" required
                                                               value="{{number_format($payitem1->grade_a,2)}}">

                                                    </div>
                                                    <hr>

                                                    <div class="task-name-wrap">
                                                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                                        <input class="task-name-field" type="number" step="0.01" name="B" required value="{{number_format($payitem1->grade_b,2)}}">

                                                    </div>
                                                    <hr>
                                                    <div class="task-name-wrap">
                                                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                                        <input class="task-name-field" type="number" step="0.01" name="C" required value="{{number_format($payitem1->grade_c,2)}}">

                                                    </div>
                                                    <hr>

                                                    <div class="task-name-wrap">
                                                        <span style="top: 25px;"><i class="zmdi zmdi-check"></i></span>
                                                        <input class="task-name-field" type="number" step="0.01" name="D" required value="{{number_format($payitem1->grade_d,2)}}">

                                                    </div>
                                                    <hr>


                                                    {{--<div class="task-name-wrap">--}}
                                                    {{--<span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>--}}
                                                    {{--<input class="task-name-field" type="text" name="desc"--}}
                                                    {{--placeholder="Description">--}}
                                                    {{--</div>--}}
                                                    {{--<hr>--}}

                                                </div>

                                                <div class="pager d-flex justify-content-center">
                                                    <input type="submit" id="finish-btn" class="finish btn btn-warning btn-block"
                                                           value="Finish">
                                                </div>



                                                {!!Form::close()!!}
                                            </div>
                                        </div>
                                    </div>


                                @endif

                            @if(!empty($payitem2->id))
                                <div class="modal fade" id="semiburdenst2{{$payitem2->id}}" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
                                     aria-hidden="true">

                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            {{ Form::open(array('route' => ['semi_burdenst1.edit',$payitem2->id],'enctype'=>'multipart/form-data' ,'id'=>'bootstrap-wizard-form'))}}

                                            @php  @endphp
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                                                    <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="task-name-wrap">
                                                    <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                                    @php  @endphp
                                                    <input name="A" type="number" step="0.01" class="task-name-field" required
                                                           value="{{number_format($payitem2->grade_a,2)}}">

                                                </div>
                                                <hr>

                                                <div class="task-name-wrap">
                                                    <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                                    <input class="task-name-field" type="number" step="0.01" name="B" required value="{{number_format($payitem2->grade_b,2)}}">

                                                </div>
                                                <hr>
                                                <div class="task-name-wrap">
                                                    <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                                    <input class="task-name-field" type="number" step="0.01" name="C" required value="{{number_format($payitem2->grade_c,2)}}">

                                                </div>
                                                <hr>

                                                <div class="task-name-wrap">
                                                    <span style="top: 25px;"><i class="zmdi zmdi-check"></i></span>
                                                    <input class="task-name-field" type="number" step="0.01" name="D" required value="{{number_format($payitem2->grade_d,2)}}">

                                                </div>
                                                <hr>


                                                {{--<div class="task-name-wrap">--}}
                                                {{--<span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>--}}
                                                {{--<input class="task-name-field" type="text" name="desc"--}}
                                                {{--placeholder="Description">--}}
                                                {{--</div>--}}
                                                {{--<hr>--}}

                                            </div>

                                            <div class="pager d-flex justify-content-center">
                                                <input type="submit" id="finish-btn" class="finish btn btn-warning btn-block"
                                                       value="Finish">
                                            </div>



                                            {!!Form::close()!!}
                                        </div>
                                    </div>
                                </div>


                                @endif




                                @endforeach

                            </tr>

                    </table>
                </div>
            </div>

        </div>


        <div class="text-center">
            <br>
            <h5>Payments For End Products</h5>


            <div class="row" >

                <div class="col-md-12">
                    <table class="table table-dark">
                        <thead class="thead-light">
                        <th>Semi Finish Product</th>
                        <th colspan="4" class="text-center bg-dribbble">First Stage</th>
                        <th>Action</th>
                        <th colspan="4" class="text-center bg-yellow">Second Stage</th>
                        <th>Action</th>
                        </thead>

                        <thead class="thead-light">
                        <th style=""></th>

                        <th class="text-center  bg-dribbble">A</th>
                        <th class="text-center bg-dribbble">B</th>
                        <th class="text-center bg-dribbble">C</th>
                        <th class="text-center bg-dribbble">D</th>
                        <th ></th>
                        <th class="text-center bg-yellow">A</th>
                        <th class="text-center bg-yellow">B</th>
                        <th class="text-center bg-yellow">C</th>
                        <th class="text-center bg-yellow">D</th>
                        <th ></th>
                        </thead>

                        <tr>
                        @foreach($endproducts as $key => $endproduct)
                            @php

                                $endpayitem1 = \BurdenManager\Models\BurdenPayment::where('pro_id','=',$endproducts[$key]->id)
                                                                            ->where('pro_type','=',2)
                                                                            ->where('stage_id','=',1 )
                                                                            ->first();

                                $endpayitem2 = \BurdenManager\Models\BurdenPayment::where('pro_id','=',$endproducts[$key]->id)
                                                                            ->where('pro_type','=',2)
                                                                            ->where('stage_id','=',2 )
                                                                            ->first();

                                 $fc_end = \BurdenManager\Models\BurdenPayment::where('pro_id','=',$endproducts[$key]->id)
                                                                            ->where('pro_type','=',2)
                                                                            ->where('stage_id','=',1 )
                                                                            ->count();

                                $sc_end = \BurdenManager\Models\BurdenPayment::where('pro_id','=',$endproducts[$key]->id)
                                                                            ->where('pro_type','=',2)
                                                                            ->where('stage_id','=',2 )
                                                                            ->count();

                            @endphp


                            <tr>
                                @if($fc_end > 1 || $sc_end > 1)
                                    <td class="text-center bg-danger">{{$endproducts[$key]->name}}</td>
                                @else
                                    <td>{{$endproducts[$key]->name}}</td>
                                @endif
                                <td class="text-center bg-dribbble">{{!empty($endpayitem1)?number_format($endpayitem1->grade_a,2):''}}</td>
                                <td class="text-center bg-dribbble">{{!empty($endpayitem1)?number_format($endpayitem1->grade_b,2):''}}</td>
                                <td class="text-center bg-dribbble">{{!empty($endpayitem1)?number_format($endpayitem1->grade_c,2):''}}</td>
                                <td class="text-center bg-dribbble">{{!empty($endpayitem1)?number_format($endpayitem1->grade_d,2):''}}</td>
                                    @if(!empty($endpayitem1->id))
                                        <td><a href="#" class="btn btn-light btn-sm" data-target="#endburdenst1{{$endpayitem1->id}}" data-toggle="modal"><i class="zmdi zmdi-edit"></i></a></td>
                                    @else
                                        <td></td>
                                    @endif
                                <td class="text-center  bg-yellow">{{!empty($endpayitem2)?number_format($endpayitem2->grade_a,2):''}}</td>
                                <td class="text-center  bg-yellow">{{!empty($endpayitem2)?number_format($endpayitem2->grade_b,2):''}}</td>
                                <td class="text-center  bg-yellow">{{!empty($endpayitem2)?number_format($endpayitem2->grade_c,2):''}}</td>
                                <td class="text-center  bg-yellow">{{!empty($endpayitem2)?number_format($endpayitem2->grade_d,2):''}}</td>
                                    @if(!empty($endpayitem2->id))
                                        <td><a href="#" class="btn btn-light btn-sm" data-target="#endburdenst2{{$endpayitem2->id}}" data-toggle="modal"><i class="zmdi zmdi-edit"></i></a></td>
                                    @else
                                        <td></td>
                                    @endif


                            </tr>

                            @if(!empty($endpayitem1->id))
                                <div class="modal fade" id="endburdenst1{{$endpayitem1->id}}" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
                                     aria-hidden="true">

                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            {{ Form::open(array('route' => ['semi_burdenst1.edit',$endpayitem1->id],'enctype'=>'multipart/form-data' ,'id'=>'bootstrap-wizard-form'))}}


                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                                                    <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="task-name-wrap">
                                                    <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                                    <input type="hidden" name="pid" value="{{$endpayitem1->id}}">
                                                    <input name="A" type="number" step="0.01" class="task-name-field" required
                                                           value="{{number_format($endpayitem1->grade_a,2)}}">

                                                </div>
                                                <hr>

                                                <div class="task-name-wrap">
                                                    <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                                    <input class="task-name-field" type="number" step="0.01" name="B" required value="{{number_format($endpayitem1->grade_b,2)}}">

                                                </div>
                                                <hr>
                                                <div class="task-name-wrap">
                                                    <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                                    <input class="task-name-field" type="number" step="0.01" name="C" required value="{{number_format($endpayitem1->grade_c,2)}}">

                                                </div>
                                                <hr>

                                                <div class="task-name-wrap">
                                                    <span style="top: 25px;"><i class="zmdi zmdi-check"></i></span>
                                                    <input class="task-name-field" type="number" step="0.01" name="D" required value="{{number_format($endpayitem1->grade_d,2)}}">

                                                </div>
                                                <hr>


                                                {{--<div class="task-name-wrap">--}}
                                                {{--<span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>--}}
                                                {{--<input class="task-name-field" type="text" name="desc"--}}
                                                {{--placeholder="Description">--}}
                                                {{--</div>--}}
                                                {{--<hr>--}}

                                            </div>

                                            <div class="pager d-flex justify-content-center">
                                                <input type="submit" id="finish-btn" class="finish btn btn-warning btn-block"
                                                       value="Finish">
                                            </div>



                                            {!!Form::close()!!}
                                        </div>
                                    </div>
                                </div>


                                @endif


                            @if(!empty($endpayitem2->id))
                                <div class="modal fade" id="endburdenst2{{$endpayitem2->id}}" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
                                     aria-hidden="true">

                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            {{ Form::open(array('route' => ['end_burdenst1.edit',$endpayitem2->id],'enctype'=>'multipart/form-data' ,'id'=>'bootstrap-wizard-form'))}}


                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                                                    <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="task-name-wrap">
                                                    <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                                    @php  @endphp
                                                    <input name="A" type="number" step="0.01" class="task-name-field" required
                                                           value="{{number_format($endpayitem2->grade_a,2)}}">

                                                </div>
                                                <hr>

                                                <div class="task-name-wrap">
                                                    <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                                    <input class="task-name-field" type="number" step="0.01" name="B" required value="{{number_format($endpayitem2->grade_b,2)}}">

                                                </div>
                                                <hr>
                                                <div class="task-name-wrap">
                                                    <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                                    <input class="task-name-field" type="number" step="0.01" name="C" required value="{{number_format($endpayitem2->grade_c,2)}}">

                                                </div>
                                                <hr>

                                                <div class="task-name-wrap">
                                                    <span style="top: 25px;"><i class="zmdi zmdi-check"></i></span>
                                                    <input class="task-name-field" type="number" step="0.01" name="D" required value="{{number_format($endpayitem2->grade_d,2)}}">

                                                </div>
                                                <hr>


                                                {{--<div class="task-name-wrap">--}}
                                                {{--<span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>--}}
                                                {{--<input class="task-name-field" type="text" name="desc"--}}
                                                {{--placeholder="Description">--}}
                                                {{--</div>--}}
                                                {{--<hr>--}}

                                            </div>

                                            <div class="pager d-flex justify-content-center">
                                                <input type="submit" id="finish-btn" class="finish btn btn-warning btn-block"
                                                       value="Finish">
                                            </div>



                                            {!!Form::close()!!}
                                        </div>
                                    </div>
                                </div>


                                @endif


                                @endforeach

                            </tr>

                    </table>
                </div>
            </div>

        </div>


    </div>



    </body>
    </html>
@stop
@section('js')
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="{{asset('assets/examples/js/demos/form.wizard.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/validate.min.js')}}"></script>
    <script src="{{asset('assets/packages/raw_materials/raw_material.js')}}"></script>

    <script>
        function confirmAlert(id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "delete",
                        url: '/descDelete/'+id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            }
                            else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

    </script>

@stop

