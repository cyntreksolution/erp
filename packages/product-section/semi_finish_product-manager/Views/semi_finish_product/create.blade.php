@extends('layouts.back.master')@section('title','Semi Finish Product')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.validation.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/packages/semi_finish/semi_finish.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('end_product.css')}}">
@stop
@section('current','Semi Finish Products')
@section('current_url',route('semi_finish_products.create'))
@section('content')
    <div class="app-wrapper">
        <div class="app-panel" id="app-panel">
            <div class="app-search"><input type="search" class="search-field" placeholder="Search"> <i
                        class="search-icon fa fa-search"></i></div><!-- /.app-search -->
            <div class="app-panel-inner">
                <div class="scroll-container">
                    <div class="p-3">
                        <button class="btn btn-success py-3 btn-block btn-lg" data-toggle="modal"
                                data-target="#projects-task-modal">NEW SEMI-FINISHED ITEM
                        </button>
                    </div>
                    <div class="p-3">
                        <button class="btn btn-warning py-3 btn-block btn-lg" data-toggle="modal"
                                data-target="#semiburden">ADD BURDEN DETAILS
                        </button>
                    </div>

                    <div class="p-3">
                        <button class="btn btn-danger py-3 btn-block btn-lg" data-toggle="modal"
                                data-target="#SemiBurdenSize">ADD BURDEN SIZE
                        </button>
                    </div>
                    <hr class="m-0">
                    <div class="projects-list">
                        {{--@foreach($stocks as $stock)--}}
                        {{--<a href="{{$stock->stock_id}}">--}}
                        {{--<div class="media">--}}
                        {{--@php $color=array("blue","danger","purple","yellow","success","info","red") @endphp--}}
                        {{--<div class="avatar avatar-circle avatar-md project-icon" data-plugin="firstLitter"--}}
                        {{--data-target="#{{$stock->stock_id*754}}"></div>--}}
                        {{--<div class="media-body">--}}
                        {{--<h6 class="project-name" id="{{$stock->stock_id*754}}">{{$stock->name}}</h6>--}}
                        {{--<small class="project-detail">{{$stock->address}}</small>--}}
                        {{--</div>--}}

                        {{--</div>--}}
                        {{--</a>--}}
                        {{--@endforeach--}}
                    </div>
                </div>
            </div>
            <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show"><i
                        class="fa fa-chevron-right"></i>
                <i class="fa fa-chevron-left"></i>
            </a>
        </div>

        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">SEMI-FINISHED PRODUCTS</h5>
            </div>
            <div class="app-main-content px-3 py-4" id="container">
                <div class="content ">
                    <div class="contacts-list">
                        @foreach($products as $product)
                            <a href="{{$product->semi_finish_product_id}}">
                                <div class="panel-item media my-2">
                                    @if(isset($product->image))
                                        <div href="{{$product->semi_finish_product_id}}"
                                             class="avatar avatar avatar-sm">
                                            <img src="{{asset($product->image_path.$product->image)}}" alt="">
                                        </div>
                                    @else
                                        @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                        <div href="{{$product->semi_finish_product_id}}"
                                             class="avatar avatar-sm bg-{{$color[$product->colour]}} "
                                             data-plugin="firstLitter"
                                             data-target="#raw-{{$product->semi_finish_product_id*874}}">
                                        </div>
                                    @endif
                                    <div class="media-body">
                                        <h6 class="media-heading my-1">
                                            <h6 href="{{$product->semi_finish_product_id}}"
                                                id="raw-{{$product->semi_finish_product_id*874}}">{{$product->name.' '.$product->specification}}</h6>
                                        </h6>
                                        <i class="qty-icon fa fa-archive"></i>
                                        <small>{{'Current stock : '.$product->available_qty}} units</small><br>
                                        {{--<small>{{$product->desc}}</small>--}}
                                    </div>
{{--                                    <div class="section-2">--}}
{{--                                        <button class="delete btn btn-light btn-sm ml-2"--}}
{{--                                                value="{{$product->semi_finish_product_id}}"--}}
{{--                                                data-toggle="tooltip"--}}
{{--                                                data-placement="left" title="DELETE SEMI-FINISHED PRODUCT">--}}
{{--                                            <i class="zmdi zmdi-close"></i>--}}
{{--                                        </button>--}}
{{--                                    </div>--}}
                                </div>
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="projects-task-modal" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'semi_finish_product','enctype'=>'multipart/form-data','id'=>'jq-validation-example-1'))}}
                {!!Form::token()!!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                    {{--<div class="task-pickers">--}}
                    {{--<div class="task-field-picker task_date_picker">--}}
                    {{--<div for="task_due_date">--}}
                    {{--<i class="zmdi zmdi-calendar"></i>--}}
                    {{--</div>--}}
                    {{--<input type="text" name="specification"--}}
                    {{--placeholder="specification">--}}
                    {{--</div>--}}
                    {{--</div>--}}
                </div>
                <div class="modal-body">
                    <div class="task-name-wrap">
                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                        <input class="task-name-field" type="text" name="name" placeholder="Name">
                    </div>
                    <hr>
                    <div class="task-name-wrap">
                        <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                        <input class="task-name-field" type="text" name="buffer_stock" placeholder="Buffer Stock">
                    </div>
                    <hr>
                    <div class="task-name-wrap">
                        <select name="is_bake"
                                class="form-control select2 ">
                            <option value="1">Bake</option>
                            <option value="2">No Bake</option>
                        </select>
                    </div>
                    <hr>
                    <div class="task-desc-wrap">
                        <textarea class="task-desc-field" name="desc"
                                  placeholder="Description"></textarea>
                    </div>
                    <hr>
                    <div class="task-name-wrap">
                        <span><i class="zmdi zmdi-check"></i></span>
                        <input class="task-name-field" type="file" name="image" placeholder="Material Name">
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {!!Form::close()!!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="semiburden" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'semi_finish_product/semiburden','enctype'=>'multipart/form-data' ,'id'=>'bootstrap-wizard-form'))}}

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">


                                    <div class="task-name-wrap">

                                            <select name="semi_id"
                                                    class="form-control select2" >
                                                <option value="" ></option>
                                                @foreach($products as $product)
                                                    <option value="{{$product->semi_finish_product_id}}" >{{$product->name}}</option>
                                                @endforeach
                                            </select>

                                    </div>


                                    <hr>

                                    <div class="task-name-wrap">

                                        <input class="form-control" type="number" name="default_size"
                                               value=1 readonly>
                                    </div>

                                    <hr>

                                    <div class="task-name-wrap">

                                        <select name="stage"
                                            class="form-control select2" >
                                            <option value='' >Select Stage</option>

                                            <option value="1" >First Stage</option>
                                            <option value="2" >Second Stage</option>

                                        </select>

                                    </div>
                                    <hr>

                                    <div class="task-name-wrap">

                                        <input class="form-control" type="number" name="A" step="0.01"
                                               placeholder="Pay Amount for Grade A">
                                    </div>

                                    <hr>
                                    <div class="task-name-wrap">

                                        <input class="form-control" type="number" name="B" step="0.01"
                                               placeholder="Pay Amount for Grade B">
                                    </div>
                                    <hr>

                                    <div class="task-name-wrap">

                                        <input class="form-control" type="number" name="C" step="0.01"
                                               placeholder="Pay Amount for Grade C">
                                    </div>

                                    <hr>
                                    <div class="task-name-wrap">
                                        <!--span style="top: 15px;"><i class="form-control-check"></i></span-->
                                        <input class="form-control" type="number" name="D" step="0.01"
                                               placeholder="Pay Amount for Grade D">
                                    </div>

                                    <hr>


                                    {{--<div class="task-name-wrap">--}}
                                        {{--<span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>--}}
                                        {{--<input class="task-name-field" type="text" name="desc"--}}
                                        {{--placeholder="Description">--}}
                                        {{--</div>--}}
                                        {{--<hr>--}}

                </div>

                <div class="pager d-flex justify-content-center">
                                    <input type="submit" id="finish-btn" class="finish btn btn-warning btn-block"
                                           value="Finish">
                </div>



                {!!Form::close()!!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="SemiBurdenSize" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route' => ['semi.size',$product->semi_finish_product_id], 'method' => 'post']) !!}
                {{-- Form::open(array('url' => 'recipe/new/si/ze','enctype'=>'multipart/form-data','id'=>'jq-validation-example-1'))--}}
                {!!Form::token()!!}

                <div class="modal-header">
                    <button type="button" class="align-right close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="form-control zmdi zmdi-close"></i></span>
                    </button>
                    {{--<div class="task-pickers">--}}
                    {{--<div class="task-field-picker task_date_picker">--}}
                    {{--<div for="task_due_date">--}}
                    {{--<i class="zmdi zmdi-calendar"></i>--}}
                    {{--</div>--}}
                    {{--<input type="text" name="specification"--}}
                    {{--placeholder="specification">--}}
                    {{--</div>--}}
                    {{--</div>--}}
                </div>
                <div class="modal-body">

                    <div class="task-name-wrap">

                        <select name="semi_id"
                                class="form-control select2" >
                            <option value="" ></option>
                            @foreach($products as $product)
                                <option value="{{$product->semi_finish_product_id}}" >{{$product->name}}</option>
                            @endforeach
                        </select>

                    </div>
                    <hr>
                    <div class="task-name-wrap">

                        <input class="form-control" type="number" step="0.01" name="burden_size" placeholder="Type New Burden Size" required>
                    </div>


                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {!!Form::close()!!}
            </div>
        </div>
    </div>




@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('assets/packages/semi_finish/semi_finish.js')}}"></script>
@stop