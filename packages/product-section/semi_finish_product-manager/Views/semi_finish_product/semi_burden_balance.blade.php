
<html>
<head>
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/invoice.css')}}" rel="stylesheet">
    <style media="all">
        @page {
            margin: 30px 5px 30px 5px;
            size: 75mm 999mm;
        }
    </style>
</head>

<body style="margin-bottom: 100px">

<!--div class="row">
    <div class="row" style="margin-left: 50px">
        <div class="col-xs-5">
            <img src="{{asset('assets/img/buntak_grace.jpeg')}}" style="width: 100px">
        </div>
    </div>


    <div class="col-xs-7 text-right">
        <h6>
            <b>Ranjanas Holdings [Pvt] Ltd</b><br>
            Katukurunda<br>
            Habaraduwa<br>
            Hotline : +9477 330 4678<br>
            E-mail : buntalksl@icloud.com<br>
        </h6>
    </div>
</div-->
<div class="row">
    <div class="col-md-12">


        <div class="row">
            <div class="col-xs-6 text-nowrap"><b>Date With Time :</b></div>
            <div class="col-xs-6"><b>:{{\Carbon\Carbon::now()->format('Y-m-d H:i:s')}}</b></div>
        </div>

    </div>
    <div class="col-xl-12 text-center">
            <h6><b><u>
                        Balance Semi Finish Burdens
            </u></b></h6>
    </div>
</div>
<br>

<div class="row">
    <div class="col-xs-12">
        <table class="table">
            <thead>
            <div class="row">
                <div class="col-md-5 text-left" > <h6><b><u>Item Name</u></b></h6></div>
                <div class="col-md-4"><h6><b><u>Burden Id</u></b></h6></div>
                <div class="col-md-3 text-right" > <h6><b><u>Qty</u></b></h6></div>

            </div>
            </thead>
            <tbody>
            @php $semiFinish = \SemiFinishProductManager\Models\SemiFinishProduct::get(); @endphp
            @foreach($semiFinish as $key => $semi)



                    @php $burdens = \BurdenManager\Models\Burden::where('semi_finish_product_id','=',$semi->semi_finish_product_id)->get();
                    @endphp

                    @php $total = 0; @endphp
                    @if ($semi->GradeABurdenList($semi->semi_finish_product_id)->count()>0)
                        @foreach($semi->GradeABurdenList($semi->semi_finish_product_id)->get() as $grade)
                            @if (number_format($grade->used_qty,3)>0)
                                @if($semi->is_bake ==1)
                                    <div class="row">

                                        <div class="col-md-5 text-left" >{{$semi->name}}</div>

                                        <div class="col-md-4">
                                            {{$grade->burden->serial}}
                                        </div>
                                        <div class="col-md-3 text-right">
                                            {{number_format($grade->used_qty,3)}}
                                            @php $total = $total + $grade->used_qty; @endphp
                                        </div>
                                    </div>
                                @else
                                    <div class="row">
                                        <div class="col-md-5 text-left" >{{$semi->name}}</div>

                                        <div class="col-md-4">
                                            {{$grade->serial}}
                                        </div>
                                        <div class="col-md-3 text-right">
                                            {{number_format($grade->used_qty,3)}}
                                            @php $total = $total +  $grade->used_qty; @endphp
                                        </div>
                                    </div>
                                @endif
                       {{--     @else

                                <div class="row">

                                    <div class="col-md-5 text-left" >{{$semi->name}}</div>

                                    <div class="col-md-4">
                                        No Stocks
                                    </div>
                                    <div class="col-md-3 text-right">
                                        In Hand
                                    </div>
                                </div>--}}
                            @endif

                        @endforeach
                        <div class="row">
                            <div class="col-md-5 font-weight-bold">
                                Total - {{$semi->name}}
                            </div>
                            <div class="col-md-4 font-weight-bold">
                                <!--div class="col-md-1" style="width:20px;height:20px;border:1px solid #000;"></div-->
                            </div>
                            <div class="col-md-3 text-right font-weight-bold">
                                <label class="text-right font-weight-bold"
                                       id="cooking_expected_qty_{{$grade->id}}">{{number_format($total,3)}}</label>
                            </div>
                        </div>

                    @endif
                <hr>
            @endforeach

            </tbody>
        </table>
    </div>

</div>
<br>

<footer>

    <!--div class="row" style="margin-top: 80px; margin-left: 50px">

        <div class="col-xs-9 text-center">
            <h5>
                .........................................<br>
                Signature-Supplier
            </h5>
        </div>
    </div>

    <div class="row" style="margin-top: 80px; margin-left: 50px">

        <div class="col-xs-9 text-center">
            <h5>
                .........................................<br>
                Signature-Authorized By
            </h5>
        </div>

    </div-->


</footer>

</body>

</html>
