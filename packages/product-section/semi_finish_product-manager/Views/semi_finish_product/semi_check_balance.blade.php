@extends('layouts.back.master')@section('title','Semi Finish Products Balance')
@section('css')
    <style type="text/css">
        #floating-button{
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #00C851;
            position: fixed;
            bottom: 80px;
            right: 30px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index:2
        }

        .plus{
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }
        .sml{
            height: 30px;
            width:50px;
            text-align: center;
            font-size: 0.75rem;
        }
    </style>

    {{--<link rel="stylesheet" href="{{asset('assets/vendor/bower_components/datatables.net/css/jquery.dataTables.min.css')}}">--}}
    {{--<link rel="stylesheet" href="{{asset('assets/vendor/dtable/Buttons-1.5.1/css/buttons.dataTables.css')}}">--}}
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/Buttons-1.5.1/css/buttons.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    {{--<link rel="stylesheet" href="{{asset('assets/vendor/dtable/DataTables-1.10.16/css/dataTables.bootstrap4.css')}}">--}}
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/datatables.net/css/buttons.dataTables.min.css')}}">
@stop
@section('content')


    <html>
    <head>
        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/css/invoice.css')}}" rel="stylesheet">
        <style media="all">
            @page {
                margin: 30px 5px 30px 5px;
                size: 75mm 999mm;
            }
        </style>
    </head>

    <body style="margin-bottom: 100px">

    <div class="row">
        <div class="col-md-12">
           <div class="row">
                <div class="col-xs-6 text-nowrap"><b>Check Reference :</b></div>

                <div class="col-xs-6"><b>:{{$ref}}</b></div>
            </div>

        </div>
        <div class="col-xl-12 text-center">
            <h5><b><u>
                        Balance Semi Finish Burdens
                    </u></b></h5>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-xs-12">
            <table id="semi_table" class="table">
                <thead>
                <div class="row">
                    <div class="col-md-4 text-left" > <h6><b><u>Item Name</u></b></h6></div>
                    <div class="col-md-2"><h6><b><u>Burden Id</u></b></h6></div>
                    <div class="col-md-2 text-center" > <h6><b><u>Qty</u></b></h6></div>
                    <div class="col-md-2 text-center" > <h6><b><u>Action</u></b></h6></div>
                    <div class="col-md-2 text-center" > <h6><b><u>Remark</u></b></h6></div>

                </div>
                </thead>
                <tbody>
                @php $semiFinish = \SemiFinishProductManager\Models\SemiFinishProduct::get(); @endphp
                @foreach($semiFinish as $key => $semi)



                    @php $burdens = \BurdenManager\Models\Burden::where('semi_finish_product_id','=',$semi->semi_finish_product_id)->get();
                    $td = date('Y-m-d');
                     $chkConfirm = \SemiFinishProductManager\Models\SemiCheck::where('semi_id','=',$semi->semi_finish_product_id)->where('status','=',2)->where('created_at','like',$td.'%')->first();

                    @endphp
                    @if(empty($chkConfirm))

                    @php $total = 0; $semiCount = 0; $chkCount = 0; @endphp
                    @if ($semi->GradeABurdenList($semi->semi_finish_product_id)->count()>0)
                        @foreach($semi->GradeABurdenList($semi->semi_finish_product_id)->get() as $grade1)
                            @if (number_format($grade1->used_qty,3) != 0)


                            @php  $chk1 = \SemiFinishProductManager\Models\SemiCheck::where('semi_id','=',$semi->semi_finish_product_id)->where('ref','=',$ref)->get();
                            $chkCount = $chk1->count();

                            $semiCount++;
                            @endphp

                            @endif
                        @endforeach
                    @endif

               {{--     @if ($semi->GradeABurdenList($semi->semi_finish_product_id)->count()>0)  --}}
                        @foreach($semi->GradeABurdenList($semi->semi_finish_product_id)->get() as $grade)
                            @php
                                if($semi->is_bake ==1){
                                $chk = \SemiFinishProductManager\Models\SemiCheck::where('semi_id','=',$semi->semi_finish_product_id)->where('burden_id','=',$grade->burden_id)->where('ref','=',$ref)->first();
                                }else{
                                 $chk = \SemiFinishProductManager\Models\SemiCheck::where('semi_id','=',$semi->semi_finish_product_id)->where('burden_id','=',$grade->id)->where('ref','=',$ref)->first();
                                }

                            @endphp


                            @if (number_format($grade->used_qty,3) != 0)

                                @if($semi->is_bake ==1)
                                    <div class="row">

                                        <div class="col-md-4 text-left" >{{$semi->name}}</div>

                                        <div class="col-md-2"><a href="view/history/{{$grade->burden->serial}}" target='_blank' >
                                                {{$grade->burden->serial}}</a>
                                        </div>
                                        <div class="col-md-2 text-right">
                                            {{number_format($grade->used_qty,3)}}
                                            @php $total = $total + $grade->used_qty; @endphp
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <input type="hidden" id="bid" value="{{$grade->burden_id}}">
                                            <input type="hidden" id="sd" value="{{$semi->semi_finish_product_id}}">
                                            <input type="hidden" id="ref" value="{{$ref}}">
                                            @if(!empty($chk))
                                                @if($semiCount != $chkCount)
                                                <input type="hidden" id="sts_{{$grade->burden_id}}" name="status" value="{{$chk->status}}">
                                                <!--input type="hidden" id="" value="Uncheck"-->
                                                <button class="btn btn-danger btn-sm m-2 text-white" id="btn_{{$grade->burden_id}}"
                                                        {{--        onclick="semiCheck('{{$grade->burden_id}}','{{$ref}}','{{$semi->semi_finish_product_id}}',{{$chk->status}},this)">Uncheck  --}}
                                                        onclick="semiCheck(event,'{{$grade->burden_id}}','{{$semi->semi_finish_product_id}}')">Uncheck   </button>
                                                @endif
                                            @else
                                                <input type="hidden" id="sts_{{$grade->burden_id}}" name="status" value="0">
                                                <!--input type="hidden" id="" value="Check"-->

                                                <button class="btn btn-success btn-sm m-2 text-white" id="btn_{{$grade->burden_id}}"
                                                        {{--         onclick="semiCheck('{{$grade->burden_id}}','{{$ref}}','{{$semi->semi_finish_product_id}}',0,this)">Check  --}}
                                                        onclick="semiCheck(event,'{{$grade->burden_id}}','{{$semi->semi_finish_product_id}}')">Check
                                                </button>


                                       {{--     <button class="btn btn-info btn-sm m-2 text-white" onclick="semiCheck('{{$grade->burden_id}}','{{$ref}}','{{$semi->semi_finish_product_id}}')">Check</button>  --}}
                                            @endif
                                          </div>
                                    </div>
                                @else
                                    <div class="row">
                                        <div class="col-md-4 text-left" >{{$semi->name}}</div>

                                        <div class="col-md-2"><a href="view/history/{{$grade->id}}" target='_blank' >
                                                {{$grade->serial}}</a>
                                        </div>
                                        <div class="col-md-2 text-right">
                                            {{number_format($grade->used_qty,3)}}
                                            @php  $total = $total +  $grade->used_qty; @endphp
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <input type="hidden" id="bid" value="{{$grade->id}}">
                                            <input type="hidden" id="sd" value="{{$semi->semi_finish_product_id}}">
                                            <input type="hidden" id="ref" value="{{$ref}}">
                                            @if(!empty($chk))
                                                @if($semiCount != $chkCount)
                                                <input type="hidden" id="sts_{{$grade->id}}" name="status" value="{{$chk->status}}">
                                                <!--input type="hidden" id="" value="Uncheck"-->
                                                <button class="btn btn-danger btn-sm m-2 text-white" id="btn_{{$grade->id}}"
                                               {{--        onclick="semiCheck('{{$grade->burden_id}}','{{$ref}}','{{$semi->semi_finish_product_id}}',{{$chk->status}},this)">Uncheck  --}}
                                               onclick="semiCheck(event,'{{$grade->id}}','{{$semi->semi_finish_product_id}}')">Uncheck
                                                </button>
                                                @endif

                                            @else
                                                <!--input type="hidden" id="" value="Check"-->
                                                <input type="hidden" id="sts_{{$grade->id}}" name="status" value="0">
                                                <button class="btn btn-success btn-sm m-2 text-white" id="btn_{{$grade->id}}"
                                                {{--         onclick="semiCheck('{{$grade->burden_id}}','{{$ref}}','{{$semi->semi_finish_product_id}}',0,this)">Check  --}}
                                                onclick="semiCheck(event,'{{$grade->id}}','{{$semi->semi_finish_product_id}}')">Check
                                                </button>
                                            @endif
                                        </div>
                                    </div>
                                @endif

                            @endif


                        @endforeach
                        <div class="row">
                            <div class="col-md-4 font-weight-bold">
                                Total - {{$semi->name}}
                            </div>
                            <div class="col-md-2 font-weight-bold">

                            </div>
                            <div class="col-md-2 text-right font-weight-bold">
                                <label class="text-right font-weight-bold"
                                       id="totqty" value="{{$total}}">{{number_format($total,3)}}</label>
                            </div>
                            <div class="col-md-2 text-center">

                            @if($semiCount == $chkCount)
                                @if(number_format($total,3) == number_format($semi->available_qty,3))
                                <button class="btn btn-warning btn-sm m-2 text-white" id="btn_{{$semi->semi_finish_product_id}}"
                                        onclick="semiConfirm('{{$ref}}','{{$semi->semi_finish_product_id}}',0,this)">Confirm
                                </button>
                                    @else
                                        <button class="btn btn-danger btn-sm m-2 text-white">Wrong Available Qty-{{number_format($total,3).'-'.$semi->available_qty}}

                                        </button>
                                    @endif
                            @endif
                            </div>
                        </div>

                  {{--  @endif  --}}


                @endif
                @endforeach

                </tbody>
            </table>
        </div>

    </div>
    <br>

    <footer>
</footer>

    </body>

    </html>
@stop
@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        function semiCheck(e,bid,sid) {
            e.preventDefault();

            let button_el = "#btn_" + bid;
            let ref = $('#ref').val();
            let url = '';
            let status = "#sts_" + bid;
            com = $(status).val();


            if(com == '0') {

                url = '/semi_finish_product/semi/bal/list?bid=' + bid + '&ref=' + ref + '&sid=' + sid + '&com=' + com ;
                $(button_el).removeClass('btn-success');
                $(button_el).addClass('btn-danger');
                $(button_el).html("Uncheck");
                $(status).val('1');
                $.ajax({
                    type: "get",
                    url: url,
                    success: function (response) {
                        if (response == 'true') {


                        }
                    }
                });
            }else if(com == '1'){
                url = '/semi_finish_product/semi/bal/list?bid=' + bid + '&ref=' + ref + '&sid=' + sid + '&com=' + com ;

                $(button_el).addClass('btn-success');
                $(button_el).removeClass('btn-danger');
                $(button_el).html("Check");
                $(status).val('0');

                $.ajax({
                    type: "get",
                    url: url,
                    success: function (response) {
                        if (response == 'true') {

                        }
                    }
                });
            }


        }




        function semiConfirm(ref,sid,confirm,element) {
            let button_semi = "#btn_" + sid;
            $(button_semi).removeClass('btn-warning');

             url = '/semi_finish_product/semi/bal/confirm?ref=' + ref + '&sid=' + sid ,
                $.ajax({
                    type: "get",
                    url: url,
                    success: function (response) {
                        if (response == 'true') {

                        }
                    }
                });

        }




    </script>
@stop
