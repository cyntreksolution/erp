<html>
<head>
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/invoice.css')}}" rel="stylesheet">
    <style media="all">

        @page {
            margin: 30px 5px 30px 5px;
            size: 75mm 999mm;
        }
    </style>
</head>

<body style="margin-bottom: 100px">

<div class="row">

    <div class="col-xs-6">
        <img src="{{asset('assets/img/buntak_grace.jpeg')}}" style="width: 100px">
    </div>

    <div class="col-xs-6 text-right">
        <h6>
            <b>Ranjanas Holdings [Pvt] Ltd</b><br>
            Katukurunda<br>
            Habaraduwa<br>
            Hotline : 077 330 4678<br>
        </h6>
    </div>
</div>

    <div class="row">
        <div class="col-md-12">


            <div class="row">
                <div class="col-xs-6 text-nowrap"><b>Date With Time :</b></div>
                <div class="col-xs-6"><b>:{{\Carbon\Carbon::now()->format('Y-m-d H:m')}}</b></div>
            </div>
            <div class="row">
                <div class="col-xs-6 text-nowrap"><b>Reference No :</b></div>
                <div class="col-xs-6"><b>:{{$ref}}</b></div>
            </div>


        </div>
        <div class="col-xl-12 text-center">
            <h6><b><u>
                        Semi Finish Checked List
                    </u></b></h6>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-xs-12">
            <table class="table">
                <thead>
                <div class="row">
                    <div class="col-md-5 text-left" > <h6><b><u>Item Name</u></b></h6></div>
                    <div class="col-md-4"><h6><b><u>Burden Id</u></b></h6></div>
                    <div class="col-md-3 text-right" > <h6><b><u>Qty</u></b></h6></div>

                </div>
                </thead>
                <tbody>

               @php
                $semiFinish = \SemiFinishProductManager\Models\SemiCheck::where('ref','=',$ref)->where('status','=',2)->groupBy('semi_id')->get();
               @endphp
                @foreach($semiFinish as $key => $semi)

                    @php
                    $total = 0;
                    $semiItem = \SemiFinishProductManager\Models\SemiCheck::where('ref','=',$ref)->where('status','=',2)->where('semi_id','=',$semi->semi_id)->get();
                    @endphp

                    @foreach($semiItem as $key => $item)


                    @php

                        $semiName = \SemiFinishProductManager\Models\SemiFinishProduct::where('semi_finish_product_id','=',$item->semi_id)->first();

                        if($semiName->is_bake == 1){
                            $sr = \BurdenManager\Models\Burden::find($item->burden_id);
                        }else{
                            $sr = \ShortEatsManager\Models\CookingRequest::find($item->burden_id);
                        }

                    @endphp

                                    <div class="row">

                                        <div class="col-md-5 text-left" >{{$semiName->name}}</div>
                                        @if(empty($sr->serial))
                                            <div class="col-md-4">
                                                No Balance
                                            </div>
                                        @else
                                        <div class="col-md-4">
                                            {{$sr->serial}}
                                        </div>
                                        @endif
                                        <div class="col-md-3 text-right">
                                            {{number_format($item->bal_qty,3)}}
                                            @php $total = $total + $item->bal_qty; @endphp
                                        </div>
                                    </div>



                    @endforeach


                    <div class="row">
                        <div class="col-md-5 font-weight-bold">
                            Total - {{$semi->name}}
                        </div>
                        <div class="col-md-4 font-weight-bold">
                        </div>
                        <div class="col-md-3 text-right font-weight-bold">
                            <label class="text-right font-weight-bold"
                                   >{{number_format($total,3)}}</label>
                        </div>
                    </div>
                @endforeach




                </tbody>
            </table>
        </div>

    </div>
    <br>

    <footer>

        <div class="row" style="margin-top: 80px; margin-left: 50px">

            <div class="col-xs-9 text-center">
                <h5>
                    .........................................<br>
                    Signature-Supplier
                </h5>
            </div>
        </div>

        <div class="row" style="margin-top: 80px; margin-left: 50px">

            <div class="col-xs-9 text-center">
                <h5>
                    .........................................<br>
                    Signature-Authorized By
                </h5>
            </div>

        </div>


    </footer>

    </body>

    </html>
