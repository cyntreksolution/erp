@extends('layouts.back.master')@section('title','Semi Finish Burden')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .hide {
            display: none;
        }
    </style>
@stop
@section('content')
    <div class="app-wrapper">


        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">{{$semiFinishProductName}}</h5>
            </div>


            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">
                    <div class="container">


                        <div class="projects-list">
                            <div class="col-md-12">
                                <div class="" style="">
                                    <div class="card-body">
                                        @php $total = 0; @endphp
                                        @if ($semiFinish->GradeABurdenList($semiFinish->semi_finish_product_id)->count()>0)
                                            @foreach($semiFinish->GradeABurdenList($semiFinish->semi_finish_product_id)->get() as $grade)
                                                @if (number_format($grade->used_qty,3)>0)
                                                    @if($semiFinish->is_bake ==1)
                                                        <div class="row burden mt-2">
                                                            <div class="col-md-6">
                                                                {{$grade->burden->serial}}-[{{$grade->burden_id}}]
                                                            </div>
                                                            <div class="col-md-6 text-right">
                                                                <label class="text-right"
                                                                       id="burden_expected_qty_{{$grade->id}}">{{number_format($grade->used_qty,3)}}</label>
                                                                @php $total = $total + $grade->used_qty; @endphp
                                                            </div>
                                                        </div>
                                                    @else
                                                        <div class="row burden mt-2">
                                                            <div class="col-md-6">
                                                                {{$grade->serial}}
                                                            </div>
                                                            <div class="col-md-6 text-right">
                                                                <label class="text-right"
                                                                       id="cooking_expected_qty_{{$grade->id}}">{{number_format($grade->used_qty,3)}}</label>
                                                                @php $total = $total +  $grade->used_qty; @endphp
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endif

                                            @endforeach
                                            <div class="row burden mt-2">
                                                <div class="col-md-6 font-weight-bold">
                                                    Total
                                                </div>
                                                <div class="col-md-6 text-right font-weight-bold">
                                                    <label class="text-right font-weight-bold"
                                                           id="cooking_expected_qty_{{$grade->id}}">{{number_format($total,3)}}</label>
                                                </div>
                                            </div>
                                        @else
                                            <div class="row burden mt-2">
                                                <div class="col-md-12 font-weight-bold text-center">
                                                    No Data
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <a href="{{route('semi_finish_products.index')}}"
                                       class="btn mt-3 btn-success btn-lg btn-block"> Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


    </div>

@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });


    </script>
@stop
