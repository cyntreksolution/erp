<?php

namespace RawMaterialManager\Controllers;

use App\RawApproval;
use App\RawInquiry;
use App\RawMaterialPrice;
use App\RawPackType;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Auth;
use PurchasingOrderManager\Models\PurchasingOrder;
use RawMaterialManager\Models\RawAdjust;
use RecipeManager\Models\RecipeContent;
use Response;
use RawMaterialManager\Models\RawMaterial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use StockManager\Classes\CStockValue;
use StockManager\Classes\StockTransaction;
use StockManager\Models\Stock;
use StockManager\Models\StockRawMaterial;
use SupplierManager\Models\Supplier;
use PurchasingOrderManager\Models\Content;
use UserManager\Models\Users;

class RawMaterialController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:raw_material_manager-index', ['only' => ['index','stock']]);
        $this->middleware('permission:raw_material_manager-create', ['only' => ['create','store']]);
        $this->middleware('permission:raw_material_manager-edit', ['only' => ['edit']]);

    }

    public function index()
    {
        $suppliers  = Supplier::pluck('supplier_name','id');
        return view('RawMaterialManager::raw_material.index',compact('suppliers'));
    }


    public function create()
    {
        $supliers = Supplier::orderBy('supplier_name')->get();
        //$supliers2 = Supplier::orderBy('supplier_name')->take(4)->get();
        $supliers2 = Supplier::orderBy('supplier_name')->get();
        $materials = RawMaterial::all();
        $materials = $materials->sortBy('name');
        $pack_raw = RawMaterial::withoutGlobalScopes()->whereType(7)->get()->pluck('name', 'raw_material_id');

        return view('RawMaterialManager::raw_material.create')->with([
            'materials' => $materials,
            'supliers2' => $supliers2,
            'supliers' => $supliers,
            'pack_raw' => $pack_raw
        ]);
    }

    public function status()
    {
        $raws = RawMaterial::orderBy('name', 'asc')->get();
        return view('RawMaterialManager::raw_material.stock')->with([
            'raws' => $raws
        ]);
    }


    public function store(Request $request)
    {

        $image = null;
        $image_path = null;
        if (isset($request->image)) {
            $file = $request->file('image');
            $path = '/uploads/images/raw_materials/';
            $destinationPath = storage_path($path);
            $file->move($destinationPath, $file->getClientOriginalName());

            $image = $file->getClientOriginalName();
            $image_path = 'storage' . $path;
        }

        if (!empty($request->pack_item)) {

            $rw2 = new RawMaterial();
            $rw2->stock_id = 1;
            $rw2->name = $request->pack_item;
            $rw2->type = 7;
            $rw2->desc = $request->desc;
            $rw2->buffer_stock = $request->bstock;
            $rw2->measurement = 'unit';
            $rw2->gram_per_unit = 0;
            $rw2->units_per_packs = $request->unitpack;
            $rw2->buffer_stock = $request->bstock;
            $rw2->colour = rand() % 7;
            $rw2->created_by = Auth::user()->id;
            $rw2->image = $image;
            $rw2->image_path = $image_path;
            $rw2->status = $request->status;
            $rw2->fraction = $request->fraction;
            $rw2->price = $request->price;
            $rw2->main_supplier_id = $request->main_sup;
            $rw2->sub_supplier_id = $request->sub_sup;

            $rw2->save();

            $price = new RawMaterialPrice();
            $price->raw_material_id = $rw2->raw_material_id;
            $price->units_per_packs = $request->unitpack;
            $price->supplier_id = $request->main_sup;
            $price->price = $request->price;
            $price->status = 2;
            $price->save();
        }

        $rw = new RawMaterial();
        $rw->stock_id = 1;
        $rw->name = $request->name;
        $rw->type = $request->type;
        $rw->desc = $request->desc;
        $rw->buffer_stock = $request->bstock;
        $rw->measurement = $request->unit;
        $rw->gram_per_unit = $request->grams;
        $rw->units_per_packs = $request->unitpack;
        $rw->buffer_stock = $request->bstock;
        $rw->colour = rand() % 7;
        $rw->created_by = Auth::user()->id;
        $rw->status = $request->status;
        $rw->fraction = $request->fraction;
        $rw->price = $request->price;
        $rw->main_supplier_id = $request->main_sup;
        $rw->sub_supplier_id = $request->sub_sup;

        if (!empty($request->pack_item)) {
            $rw->pack_item = $rw2->raw_material_id;
            $rw->units_per_packs = $request->units_per_pack;
        }

        $rw->save();

        $price = new RawMaterialPrice();
        $price->raw_material_id = $rw->raw_material_id;
        $price->price = $request->price;
        $price->units_per_packs = $request->unitpack;
        $price->supplier_id = $request->main_sup;
        $price->status = 2;
        $price->save();

        return redirect(route('raw_materials.create'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => 'Raw material has been created'
        ]);

    }


    public function show(RawMaterial $raw_material)
    {


        $pack_raw = RawMaterial::withoutGlobalScopes()->whereType(7)->get()->pluck('name', 'raw_material_id');


        return view('RawMaterialManager::raw_material.show')->with([
            'raw_material' => $raw_material,
            'pack_raw' => $pack_raw,
            'msupliers' => $raw_material->main_supplier_id,
            'ssupliers' => $raw_material->sub_supplier_id
        ]);
    }

    public function details(RawMaterial $raw_material)
    {
        $transactions = StockRawMaterial::with('rawMaterials')
            ->where('raw_material_id', '=', $raw_material->raw_material_id)->get();
        return view('RawMaterialManager::raw_material.details')->with([
            'raw_material' => $raw_material,
            'transactions' => $transactions,

        ]);
    }

    public function edit(RawMaterial $raw_material)
    {
        return view('RawMaterialManager::raw_material.edit');
    }

    public function changeSup(Request $request)
    {

        $rws = RawMaterial::where('raw_material_id', '=', $request->raw_id)->first();

        $rws->main_supplier_id = $request->main_sup;
        $rws->sub_supplier_id = $request->sub_sup;
        $rws->save();

        return redirect(route('raw_materials.create'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => 'Suppliers has been updated'
        ]);
    }


    public function update(Request $request, RawMaterial $raw_material)
    {
        if (isset($request->fraction)) {
            $raw_material->fraction = !$request->fraction;
            $raw_material->save();
            return redirect('raw_material/' . $raw_material->raw_material_id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Fraction has been updated'
            ]);
        }
        if (isset($request->sale_fraction)) {
            $raw_material->sale_fraction = !$request->sale_fraction;
            $raw_material->save();
            return redirect('raw_material/' . $raw_material->raw_material_id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Fraction has been updated'
            ]);
        }

        if (isset($request->status)) {
            $raw_material->status = !$request->status;
            $raw_material->save();
            return redirect('raw_material/' . $raw_material->raw_material_id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Status has been updated'
            ]);
        }

        if (isset($request->Name)) {
            $raw_material->name = $request->Name;
            $raw_material->save();
            return redirect('raw_material/' . $raw_material->raw_material_id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'name has been updated'
            ]);
        }

        if (isset($request->sinhala)) {
            $raw_material->sin_name = $request->sinhala;
            $raw_material->save();
            return redirect('raw_material/' . $raw_material->raw_material_id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Sinhala Name has been updated'
            ]);
        }

        if (isset($request->pack_item)) {
            $raw_material->pack_type = $request->pack_item;
            $raw_material->units_per_packs = (!empty($request->units_per_pack)) ? $request->units_per_pack : 0;
            $raw_material->save();

            $pack = new RawPackType();
            $pack->raw_material_id = $raw_material->raw_material_id;
            $pack->pack_type = $request->pack_item;
            $pack->units_per_pack = (!empty($request->units_per_pack)) ? $request->units_per_pack : 0;
            $pack->status = 1;
            $pack->save();


            return redirect('raw_material/' . $raw_material->raw_material_id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Pack Item has been updated'
            ]);
        }

        if (isset($request->units_per_pack)) {
            $raw_material->units_per_packs = $request->units_per_pack;
            $raw_material->save();

            $pack = new RawPackType();
            $pack->raw_material_id = $raw_material->raw_material_id;
            $pack->pack_type = $raw_material->pack_type;
            $pack->units_per_pack = (!empty($request->units_per_pack)) ? $request->units_per_pack : 0;
            $pack->status = 1;
            $pack->save();

            return redirect('raw_material/' . $raw_material->raw_material_id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Units per Pack Item has been updated'
            ]);
        }

        if (isset($request->desc)) {
            $raw_material->desc = $request->desc;
            $raw_material->save();
            return redirect('raw_material/' . $raw_material->raw_material_id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Description has been updated'
            ]);
        }

        if (isset($request->scale_key)) {
            $raw_material->scale_key = $request->scale_key;
            $raw_material->save();
            return redirect('raw_material/' . $raw_material->raw_material_id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Scale Key has been updated'
            ]);
        }

        if (isset($request->buffer_stock)) {
            $raw_material->buffer_stock = $request->buffer_stock;
            $raw_material->save();
            return redirect('raw_material/' . $raw_material->raw_material_id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Buffer stock has been updated'
            ]);
        }

        if (isset($request->available_qty)) {
            $adjusted = new RawAdjust();
            $adjusted->raw_material_raw_material_id = $raw_material->raw_material_id;
            $adjusted->current_stock = $raw_material->available_qty;
            $adjusted->difference = $raw_material->available_qty - $request->available_qty ;
            $adjusted->created_by = Auth::user()->id;
            $adjusted->save();

            $raw_material->available_qty = $request->available_qty;
            $raw_material->save();


            return redirect('raw_material/' . $raw_material->raw_material_id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Available stock has been updated'
            ]);
        }

        if (isset($request->price)) {
            $raw_material->price = $request->price;
            $raw_material->save();

            $price = new RawMaterialPrice();
            $price->raw_material_id = $raw_material->raw_material_id;
            $price->price = $request->price;
            $price->status = 2;
            $price->save();

            return redirect('raw_material/' . $raw_material->raw_material_id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Price has been updated'
            ]);
        }

        if (isset($request->raw_material_type)) {
            $raw_material->type = $request->raw_material_type;
            $raw_material->save();


            return redirect('raw_material/' . $raw_material->raw_material_id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Type has been updated'
            ]);
        }

        if (isset($request->stock)) {
            $currentStock = $raw_material->available_qty;
            if ($raw_material->measurement == 'gram') {
                $diffrence = $currentStock - ($request->stock * 1000);
                $ajustedQty = ($request->stock * 1000);
            } elseif ($raw_material->measurement == 'milliliter') {
                $diffrence = $currentStock - ($request->stock * 1000);
                $ajustedQty = ($request->stock * 1000);
            } else {
                $diffrence = $currentStock - $request->stock;
                $ajustedQty = $request->stock;
            }
            $raw_material->available_qty = $ajustedQty;
            $raw_material->save();
            $adjusted = new RawAdjust();
            $adjusted->raw_material_raw_material_id = $raw_material->raw_material_id;
            $adjusted->current_stock = $currentStock;
            $adjusted->difference = $diffrence;
            $adjusted->created_by = Auth::user()->id;
            $adjusted->save();

            StockTransaction::rawMaterialStockTransaction(3, 0, $raw_material->raw_material_id, $ajustedQty, $adjusted->id, 1);
            return redirect('raw_material/stock/status/' . $raw_material->raw_material_id)->with([
                'info' => true,
                'info.title' => 'Congratulations !',
                'info.message' => 'Current Stock Adjusted'
            ]);
        }
    }


    public function destroy(RawMaterial $raw_material)
    {
        $raw_material->delete();
        if ($raw_material->trashed()) {
            return 'true';
        } else {
            return 'false';
        }
    }

    public function getraw(Request $request)
    {
        return $raw = RawMaterial::withoutGlobalScopes()->where('raw_material_id', '=', $request->id)->first();

    }

    public function getRawMeals(Request $request)
    {
        return $raw = RawMaterial::withoutGlobalScopes()->where('type', '=', 9)->pluck('name', 'raw_material_id');
    }

    public function stockOld(Request $request)
    {

        $data = StockRawMaterial::groupBy('raw_material_id')->get();
        $jsonList = array();
        $i = 1;

        foreach ($data as $key => $item) {
//            return $item;

            $up = RawMaterial::where('raw_material_id', '=', $item['rawMaterials']->raw_material_id)->first();
            if (!empty($up->units_per_packs)) {

                $unit_pack = $item['rawMaterials']->units_per_packs;

            } else {
                $unit_pack = 1;
            }

            $quantity = StockRawMaterial::where('raw_material_id', '=', $item['rawMaterials']->raw_material_id)
                ->where('type', '=', '2')
                ->where('created_at', '<', date($data))
                ->sum('qty');

            $qty = $quantity;

            /*$tr = StockRawMaterial::where('raw_material_id', '=', $item['rawMaterials']->raw_material_id)
                ->where('type', '=', '1')
                ->get();

            $qtyq = $tr->sum('qty');


            $getlastprice = Content::where('raw_material_id', '=', $item['rawMaterials']->raw_material_id)
                ->where('received_price', '>', 0)
                ->orderBy('created_at', 'desc')
                ->first();
//            $lastprice = $getlastprice->received_price;


            $sup = StockTransaction::getLastRawMaterialSupplier($item['rawMaterials']->raw_material_id);

            $supplier = PurchasingOrder::with('supplier')
                ->where('id', '=', $sup)
                ->first();*/

            $dd = array();

            array_push($dd, $i);

            array_push($dd, $item['rawMaterials']->name);


            $buffer = $item['rawMaterials']->buffer_stock;


            array_push($dd, $buffer . 'Days');


            $avqty = $item['rawMaterials']->available_qty;
            // array_push($dd, ($item['rawMaterials']->units_per_packs)?$avqty/$item['rawMaterials']->units_per_packs:$avqty . ' ' . $item['rawMaterials']->pack_type);
            array_push($dd, ($avqty / $unit_pack) . ' ' . $item['rawMaterials']->pack_type);


            $price = ($avqty / $unit_pack) * $item['rawMaterials']->price;
            array_push($dd, 'Rs ' . number_format($price, 2));

            if (is_null($qty)) {
                array_push($dd, '-');
                array_push($dd, '-');
                array_push($dd, '-');
            } else {
                $day_usage = (($qty / $unit_pack) / 30);
                array_push($dd, number_format(($qty / $unit_pack), 0) . '[' . number_format(($qty % $unit_pack), 3) . ']' . ' ' . $item['rawMaterials']->pack_type . '[Grams/Units]');
                array_push($dd, (($qty / $unit_pack) / 30) . ' ' . $item['rawMaterials']->pack_type);
                if ($day_usage > 0) {
                    array_push($dd, ($avqty / $day_usage));
                    $bd = ($buffer - ($avqty / $day_usage));
                } else {
                    array_push($dd, 1);
                    $bd = ($buffer - $avqty);
                }


                if ($bd < 0) {

                    //   $btn_history = "<a href='$btn_history_url' class='btn btn-success'>Order History</a>";

                }


                /*
                 How to Identify Insufficient stock
                1.first we update buffer stock colum as buffer days (how many days maintain stock- it is depend on the frequency of supplier visits)
                2.Then Calculate -    $bd = ($buffer-($avqty/$day_usage));
                if($bd < 0 )
                    {
                Insufficient stock ,want to purchase (supplier details in action colum using eye mark)
                then click eye mark want to desplay details of last 4 invoices.

                Purchased Date  | Supplier Name  | Qty Purchased | Unit Price | Supplier TP

                }else
                {
                sufficient stock .
                }

                Finally want to calculate total current stock value.

                  */

            }


            array_push($jsonList, $dd);
            $i++;
        }
        return Response::json(array('data' => $jsonList));


    }

    public function stock(Request $request)
    {

        $supplier = $request->supplier;
        $data = RawMaterial::withoutGlobalScopes();
        if (!empty($supplier)) {
            $data = $data->where('main_supplier_id','=',$supplier);
        }
        $data =$data->get();
        $jsonList = [];
        $i = 1;

        $total_value = 0;
        foreach ($data as $key => $item) {

            if(Auth::user()->hasRole(["Super Admin","Assistant"]) ) {

                if($item->is_approved == 1) {


                    $dd = [];
                    array_push($dd, ++$key);
                    array_push($dd, $item->name);

                    $unit_pack = !empty($item->units_per_packs) ? $item->units_per_packs : 1;
                    $available_qty_pack = (int)($item->available_qty / $unit_pack);
                    $available_qty_mod = ($item->available_qty % $unit_pack);
                    $available_qty = $item->available_qty;
                    array_push($dd, $available_qty_pack . "[$available_qty_mod] $item->pack_type");


                    $price = (($available_qty) / ($unit_pack)) * $item->price;

                    array_push($dd, number_format($price, 2) . '[' . number_format($item->price, 2) . ']');

                    $total_value += $price;

                    $last30 = abs(RawInquiry::where('raw_material_id', '=', $item->raw_material_id)->where('created_at', '>', Carbon::now()->subMonth()->format('Y-m-d H:i:s'))->where('type', '=', 2)->sum('qty'));

                    $per_day_usage = $last30 / 30;

                    $per_day_usage = !empty($per_day_usage) ? $per_day_usage : 1;

                    $buffer_day = round($item->available_qty / $per_day_usage);

                    $action = "";
                    if ($item->buffer_stock > $buffer_day) {

                        //  $action = "<a href='' target='_blank' class='btn btn-danger'>Manager Request</a>";
                        if ($item->is_approved == 1) {
                            $raw_app= RawApproval::where('raw_id','=',$item->raw_material_id)->where('status','=',1)->first();

                            if(empty($raw_app->raw_id)){
                                $action = '<a class="btn btn-danger btn-sm mr-1" href="' . route('manager.re-try', ['raw_id' => $item->raw_material_id ]) . '">Retry Please...</a>';

                            }else{
                                $action = '<a class="btn btn-success btn-sm mr-1" href="' . route('manager.approve', ['raw_id' => $raw_app->raw_id ,'id' => $raw_app->id]) . '">Approve</a>';

                            }


                        }
                    }else{

                        $action = '<a class="btn btn-danger btn-sm mr-1" href="' . route('manager.re-try', ['raw_id' => $item->raw_material_id ]) . '">Retry Please...</a>';

                    }

                    array_push($dd, number_format($last30, 3));//last 30
                    array_push($dd, number_format($per_day_usage, 3));//per day
                    array_push($dd, $item->buffer_stock . " Days");//re order days
                    array_push($dd, ceil($buffer_day) . " Days");//buffer days
                    array_push($dd, $action);//action

                    array_push($jsonList, $dd);
                    $i++;
                }
            }else{

                $dd = [];
                array_push($dd, ++$key);
                array_push($dd, $item->name);

                $unit_pack = !empty($item->units_per_packs) ? $item->units_per_packs : 1;
                $available_qty_pack = (int)($item->available_qty / $unit_pack);
                $available_qty_mod = ($item->available_qty % $unit_pack);
                $available_qty = $item->available_qty;
                array_push($dd, $available_qty_pack . "[$available_qty_mod] $item->pack_type");



                $price = (($available_qty)/($unit_pack)) * $item->price;
                array_push($dd,  number_format($price, 2).'['.number_format($item->price, 2).']');
                $total_value += $price;

                $last30 = abs(RawInquiry::where('raw_material_id', '=', $item->raw_material_id)->where('created_at', '>', Carbon::now()->subMonth()->format('Y-m-d H:i:s'))->where('type', '=', 2)->sum('qty'));

                $per_day_usage = $last30 / 30;

                $per_day_usage = !empty($per_day_usage) ? $per_day_usage : 1;

                $buffer_day = round($item->available_qty / $per_day_usage);

                $action = "";
                if ($item->buffer_stock > $buffer_day) {
                    //  $action = "<a href='' target='_blank' class='btn btn-danger'>Manager Request</a>";
                    if($item->is_approved == 1){
                        $action = '<a class="btn btn-warning btn-sm mr-1" href="#">Approving</a>';

                    }elseif($item->is_approved == 2){
                        $action = '<a class="btn btn-dark btn-sm mr-1" href="#">Purchasing</a>';

                    }elseif($item->is_approved == 3){
                        $action = '<a class="btn btn-info btn-sm mr-1" href="#">Delivering</a>';

                    }elseif($item->is_approved == 0){
                        $action = '<a class="btn btn-danger btn-sm mr-1" href="'. route('manager.request', ['raw_id' => $item->raw_material_id]) . '">Manager Request</a>';
                    }else{
                        $action = '<a class="btn btn-outline-info btn-sm mr-1" href="#">Something Went Wrong...!</a>';
                    }

                }

                array_push($dd, number_format($last30,3));//last 30
                array_push($dd, number_format($per_day_usage,3));//per day
                array_push($dd, $item->buffer_stock . " Days");//re order days
                array_push($dd, ceil($buffer_day) . " Days");//buffer days
                array_push($dd, $action);//action

                array_push($jsonList, $dd);
                $i++;
            }


        }
        if(Auth::user()->hasRole(["Super Admin","Assistant"]) ) {
            array_push($jsonList, [$i, '', '', number_format($total_value, 2), '', '', '', '', '']);
           }
        return Response::json(array('data' => $jsonList));

    }

    public function managerReq(Request $request)
    {

        $isit = RawApproval::where('raw_id','=',$request->raw_id)->where('status','=',1)->first();

        if(empty($isit)) {
            $man = new RawApproval();
            $man->raw_id = $request->raw_id;
            $man->status = 1;
            $man->save();

            $raw = RawMaterial::where('raw_material_id', '=', $request->raw_id)->first();
            $raw->is_approved = 1;
            $raw->save();
        }else{
            $raw = RawMaterial::where('raw_material_id', '=', $request->raw_id)->first();
            $raw->is_approved = 1;
            $raw->save();
        }
        $suppliers  = Supplier::pluck('supplier_name','id');
        return view('RawMaterialManager::raw_material.index',compact('suppliers'));

    }

    public function managerRetry(Request $request)
    {


            $raw = RawMaterial::where('raw_material_id', '=', $request->raw_id)->first();
            $raw->is_approved = 0;
            $raw->save();

            $man = RawApproval::where('raw_id','=',$request->raw_id)->where('status','=',1)->first();
            if(!empty($man)) {
                $man->delete();
            }

            $suppliers  = Supplier::pluck('supplier_name','id');
        return view('RawMaterialManager::raw_material.index',compact('suppliers'));

    }


    public function managerApp(Request $request)
    {
        $app = RawInquiry::where('raw_material_id', '=', $request->raw_id)->OrderByDesc('id')->first();
        $raw = RawMaterial::where('raw_material_id', '=', $request->raw_id)->first();
        $man = RawApproval::where('id','=',$request->id)->where('status','=',1)->first();
        $man->bal_units = $raw->available_qty;
        $man->value = round((($raw->price)/($raw->units_per_packs)),2);
        $man->updated_at = Carbon::now();
        $man->approved_by = Auth::id();
        $man->status = 2;
        $man->save();

        $raw->is_approved = 2;
        $raw->save();

        $app->is_approved = 1;
        $app->save();

        $suppliers  = Supplier::pluck('supplier_name','id');
        return view('RawMaterialManager::raw_material.index',compact('suppliers'));

    }


    public function date()
    {
        $raw = RawMaterial::all();
        return view('RawMaterialManager::raw_material.date')->with([
            'raws' => $raw
        ]);
    }

    public function getReport(Request $request)
    {

        $start_date = substr($request->date, 0, 10);
        $start_date = date_create($start_date);
        $start_date = date_format($start_date, "Y-m-d");

        $end_date = substr($request->date, 12, 23);
        $end_date = date_create($end_date);
        $end_date = date_format($end_date, "Y-m-d");

        $product = $request->product;
        if ($product == 0) {
            $rawMaterialUsage = StockRawMaterial::with('rawMaterials')->where('type', '=', '2')->selectRaw('*, sum(qty) as usages')->groupBy('raw_material_id')->get();
            $rawMaterialOrders = StockRawMaterial::with('rawMaterials')->where('type', '=', '1')->selectRaw('*, sum(qty) as ordered')->groupBy('raw_material_id')->get();
            $array = [];
            foreach ($rawMaterialUsage as $item) {
                if ($item['rawMaterials']->measurement == 'unit') {
                    $qty = abs($item['qty']) . ' units';
                } else {
                    $qty = abs($item['qty'] / 1000) . ' kg';
                }
                $rawx = [
                    'id' => $item['raw_material_id'],
                    'qty' => $qty,
                    'name' => $item['rawMaterials']->name,
                    'image' => $item['rawMaterials']->image,
                    'imagePath' => $item['rawMaterials']->image_path,
                ];
                array_push($array, $rawx);
            }
            return $array;

        } else {
            $rawMaterialUsage = StockRawMaterial::with('rawMaterials')->where('type', '=', '2')->where('raw_material_id', '=', $product)->selectRaw('*, sum(qty) as usages')->groupBy('raw_material_id')->get();
            $array = [];
            foreach ($rawMaterialUsage as $item) {
                if ($item['rawMaterials']->measurement == 'unit') {
                    $qty = abs($item['qty']) . ' units';
                } else {
                    $qty = abs($item['qty'] / 1000) . ' kg';
                }
                $rawx = [
                    'id' => $item['raw_material_id'],
                    'qty' => $qty,
                    'name' => $item['rawMaterials']->name,
                    'image' => $item['rawMaterials']->image,
                    'imagePath' => $item['rawMaterials']->image_path,
                ];
                array_push($array, $rawx);
            }
            return $array;
        }
    }

    public function analyse(Request $request)
    {
        $start_date = substr($request->date, 0, 10);
        $start_date = date_create($start_date);
        $start_date = date_format($start_date, "Y-m-d");

        $end_date = substr($request->date, 12, 23);
        $end_date = date_create($end_date);
        $end_date = date_format($end_date, "Y-m-d");

        $product = $request->Single;

        $dates = $this->generateDateRange(Carbon::parse($start_date), Carbon::parse($end_date));

        $rawMaterialUsage = StockRawMaterial::with('rawMaterials')->where('type', '=', '2')->selectRaw('*, sum(qty) as usages')->groupBy('raw_material_id')->get();
        $rawMaterialOrders = StockRawMaterial::with('rawMaterials')->where('type', '=', '1')->selectRaw('*, sum(qty) as ordered')->groupBy('raw_material_id')->get();

        $productNames = [];
//        foreach ($burdensProducts as $key => $burdensProduct) {
//            foreach ($dates as $key => $date) {
//                $burdenCount = Burden::where('semi_finish_product_id', $burdensProduct->semi_finish_product_id)->whereDate('created_at', $date)->get()->count();
//                $dte[$key] = $burdenCount;
//            }
//            $name = [
//                'name' => $burdensProduct['semiProdcuts']->name,
//                'date' => $dte,
//                'sum' => $burdensProduct->sum
//            ];
//            array_push($productNames, $name);
//        }

        if ($product == 0) {
            return view('RawMaterialManager::raw_material.analyse')->with([
//                'dates' => $dates,
//                'productNames' => $productNames,
            ]);
        } else {
            $rawMaterialUsage = StockRawMaterial::with('rawMaterials')->where('type', '=', '2')->where('raw_material_id', '=', $product)->get();
            $rawMaterialOrders = StockRawMaterial::with('rawMaterials')->where('type', '=', '1')->where('raw_material_id', '=', $product)->get();
            foreach ($rawMaterialUsage as $key => $item) {
                foreach ($dates as $key => $date) {

                }
            }
            return view('RawMaterialManager::raw_material.analyseRaw');
        }

    }

    private function generateDateRange(Carbon $start_date, Carbon $end_date)
    {
        $dates = [];

        for ($date = $start_date; $date->lte($end_date); $date->addDay()) {
            $dates[] = $date->format('Y-m-d');
        }

        return $dates;
    }


    public function getRawMaterialData(Request $request)
    {
        return RawMaterial::where('raw_material_id', '=', $request->id)->first();
    }


}
