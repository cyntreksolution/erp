<?php

namespace RawMaterialManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RawAdjust extends Model
{
    use SoftDeletes;

    protected $table = 'raw_adjust';

    protected $dates = ['deleted_at'];

}
