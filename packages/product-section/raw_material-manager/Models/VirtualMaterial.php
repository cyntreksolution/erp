<?php

namespace RawMaterialManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VirtualMaterial extends Model
{
    use SoftDeletes;

    protected $table = 'virtual_material';

    protected $dates = ['deleted_at'];

    public function burden(){
        return $this->belongsTo('BurdenManager\Models\Burden');
    }

    public function semiProduct(){
        return $this->belongsTo('SemiFinishProductManager\Models\SemiFinishProduct');
    }
}
