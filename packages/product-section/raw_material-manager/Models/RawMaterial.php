<?php

namespace RawMaterialManager\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class RawMaterial extends Model
{
    use SoftDeletes;

    const PACK_TYPE = [
        'Bag' => 'Bag',
        'Case' => 'Case',
        'Can' => 'Can',
        'Packet' => 'Packet',
        'KG' => 'KG',
        'G' => 'G',
        'L' => 'L',
        'ML' => 'ML',
        'Bottle' => 'Bottle',
        'Tins' => 'Tins',
        'Cups' => 'Cups',
        'Cards' => 'Cards',
        'Bundle' => 'Bundle',
        'Boxes' => 'Boxes',
        'Dozen' => 'Dozen',
        'Pieces' => 'Pieces',
        'Units' => 'Units',
    ];

    const RAW_MATERIAL_TYPE =[
        1=>'Raw Material',
        2=>'Not Raw Material',
        3=>'Cannot Specific',
        4=>'Outlet Items',
        5=>'Crates',
        6=>'Packing Items',
        7=>'Pack Type Product',
        8=>'Re Usable',
        9=>'Meals',

    ];


    protected $table = 'raw_material';

    protected $primaryKey = 'raw_material_id';

    protected $dates = ['deleted_at'];

    public function suppliers()
    {
        return $this->belongsToMany('SupplierManager\Models\Supplier');
    }

    public function pack()
    {
        return $this->belongsTo(RawMaterial::class, 'pack_item', 'raw_material_id')->withoutGlobalScopes(['type']);
    }

    protected static function boot()
    {
        parent::boot();

//        static::addGlobalScope('type', function (Builder $builder) {
//            $builder->where('type', '=', 1);
//        });

        static::addGlobalScope('sort', function (Builder $builder) {
            $builder->orderBy('name', 'asc');
        });
    }
}
