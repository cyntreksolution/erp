<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web','auth'])->group(function () {
    Route::prefix('raw_material')->group(function () {
        Route::get('', 'RawMaterialManager\Controllers\RawMaterialController@index')->name('raw_materials.index');
        Route::get('manager/request', 'RawMaterialManager\Controllers\RawMaterialController@managerReq')->name('manager.request');
       // Route::get('manager/approval', 'RawMaterialManager\Controllers\RawMaterialController@managerApp')->name('manager.approve');
        Route::get('manager/re-try', 'RawMaterialManager\Controllers\RawMaterialController@managerRetry')->name('manager.re-try');


        Route::get('stock/status', 'RawMaterialManager\Controllers\RawMaterialController@status')->name('raw_materials.stock');

        Route::get('stock/date', 'RawMaterialManager\Controllers\RawMaterialController@date')->name('raw_materials.date');
        Route::post('stock/date', 'RawMaterialManager\Controllers\RawMaterialController@getReport')->name('raw_materials.date');
        Route::post('analyse', 'RawMaterialManager\Controllers\RawMaterialController@analyse')->name('raw_materials.analyse');



        Route::get('stock/status/{raw_material}', 'RawMaterialManager\Controllers\RawMaterialController@details')->name('raw_materials.stock');

        Route::get('create', 'RawMaterialManager\Controllers\RawMaterialController@create')->name('raw_materials.create');

        Route::get('{raw_material}', 'RawMaterialManager\Controllers\RawMaterialController@show')->name('raw_materials.show');

        Route::get('{raw_material}/edit', 'RawMaterialManager\Controllers\RawMaterialController@edit')->name('raw_materials.edit');

        Route::get('raw/list', 'RawMaterialManager\Controllers\RawMaterialController@stock')->name('raw_materials.stock');



        Route::post('changeSup', 'RawMaterialManager\Controllers\RawMaterialController@changeSup')->name('raw.changeSup');


        Route::post('getraw', 'RawMaterialManager\Controllers\RawMaterialController@getraw')->name('raw_materials.store');

        Route::post('store', 'RawMaterialManager\Controllers\RawMaterialController@store')->name('raw.store');


        Route::post('{raw_material}', 'RawMaterialManager\Controllers\RawMaterialController@update')->name('raw_materials.update');

        Route::delete('{raw_material}', 'RawMaterialManager\Controllers\RawMaterialController@destroy')->name('raw_materials.destroy');
    });
});
Route::post('raw_material/get/raw', 'RawMaterialManager\Controllers\RawMaterialController@getRaw')->name('raw_materials.info');
Route::get('get/raw/material/meals/data', 'RawMaterialManager\Controllers\RawMaterialController@getRawMeals')->name('raw_materials.measl');
