<?php

namespace RawMaterialManager;

use Illuminate\Support\ServiceProvider;

class RawMaterialServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/Views', 'RawMaterialManager');
        require __DIR__ . '/web.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('RawMaterialManager', function($app){
            return new RawMaterialManager;
        });
    }
}
