@extends('layouts.back.master')@section('title','Raw Material Details')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/modal.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/lightbox2/dist/css/lightbox.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}"/>

    <style>

    </style>

@stop
@section('current','Raw Materials')
@section('current_url',route('raw_materials.create'))
@section('current2',$raw_material->name)
@section('current_url2','')
@section('content')
    <div class="profile-wrapper">
        <div class="profile-section-user">
            <div class="profile-cover-img profile-pic">
                <img src="{{asset('assets\img\raw material.jpg')}}" alt="">
            </div>
            <div class="profile-info-brief p-3">
                @if(isset($raw_material->image))
                    <div class="user-profile-avatar avatar-circle avatar avatar-sm">
                        <img src="{{asset($raw_material->image_path.$raw_material->image)}}" alt="">
                    </div>
                @else
                    @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                    <div class="user-profile-avatar avatar avatar-circle avatar-md project-iconr bg-{{$color[$raw_material->colour]}}"
                         data-plugin="firstLitter"
                         data-target="#{{$raw_material->id*754}}"></div>
                @endif

                {{--<img class="img-fluid user-profile-avatar"src="{{asset($raw_material->image_path.$raw_material->image)}}">--}}
                <div class="text-center">
                    @if(isset($raw_material->image))
                        <button class=" btn btn-outline-primary btn-sm btn-rounded my-3" data-toggle="modal"
                                data-target="#EditImage">
                            <i class="fa fa-pencil px-1"></i>
                            CHANGE IMAGE
                        </button>
                    @else
                        <button class=" btn btn-outline-primary btn-sm btn-rounded my-3" data-toggle="modal"
                                data-target="#EditImage">
                            <i class="fa fa-pencil px-1"></i>
                            NEW IMAGE
                        </button>
                    @endif

                    <h5 class="text-uppercase mb-1" id="{{$raw_material->id*754}}">{{$raw_material->name}}
                        <a href="#" class="btn btn-light btn-sm" data-toggle="modal"
                           data-target="#EditName">
                            <i class="zmdi zmdi-edit" data-toggle="tooltip"
                               data-placement="right" title="Edit Name"></i>
                        </a>
                    </h5>
                        <h6 class="text mb-lg-1">Name of Sinhala  :<font face="FMMalithi x"><b> {{$raw_material->sin_name}}</b></font>
                            <span class="section-1">
                                        <a href="#" class="btn btn-light btn-sm" data-toggle="modal"
                                           data-target="#sinhala">
                                            <i class="zmdi zmdi-edit" data-toggle="tooltip"
                                               data-placement="right" title="Edit Singhala Name"></i>
                                        </a>
                                    </span>
                            </h6>

                            <h6 class="text mb-lg-1">Available Quantity : {{$raw_material->available_qty}}
                                <span class="section-1">
                                        <a href="#" class="btn btn-light btn-sm" data-toggle="modal"
                                           data-target="#availableQty">
                                            <i class="zmdi zmdi-edit" data-toggle="tooltip"
                                               data-placement="right" title="Edit Available Quantity"></i>
                                        </a>
                                    </span>
                            </h6>

                            <h6 class="text mb-lg-1">Buffer Stock :{{$raw_material->buffer_stock}}
                                <span class="section-1">
                                        <a href="#" class="btn btn-light btn-sm" data-toggle="modal"
                                           data-target="#BufferStock">
                                            <i class="zmdi zmdi-edit" data-toggle="tooltip"
                                               data-placement="right" title="Edit Description"></i>
                                        </a>
                                    </span>
                            </h6>


                    <br>
                    <h6 class="text mb-lg-1">Current Price : {{($raw_material->price)?$raw_material->price:0}} LKR
                        <span class="section-1">
                                        <a href="#" class="btn btn-light btn-sm" data-toggle="modal"
                                           data-target="#PriceChange">
                                            <i class="zmdi zmdi-edit" data-toggle="tooltip"
                                               data-placement="right" title="Edit Price"></i>
                                        </a>
                                    </span>
                    </h6>
                    <br>

                </div>

                <hr class="m-0">

                <div class="text-center">
                    Description : {{$raw_material->desc}}
                    <div class="section-1">
                        <a href="#" class="btn btn-light btn-sm" data-toggle="modal"
                           data-target="#EditDesc">
                            <i class="zmdi zmdi-edit" data-toggle="tooltip"
                               data-placement="right" title="Edit Description"></i>
                        </a>
                    </div>
                </div>

                    <hr class="m-0">

                    <div class="text-center">
                        Scale Key : {{$raw_material->scale_key}}
                        <div class="section-1">
                            <a href="#" class="btn btn-light btn-sm" data-toggle="modal"
                               data-target="#EditScaleKey">
                                <i class="zmdi zmdi-edit" data-toggle="tooltip"
                                   data-placement="right" title="Edit Scale Key"></i>
                            </a>
                        </div>
                    </div>

                <hr class="mt-1">

                <div class="text-center">
                    Type : {{\RawMaterialManager\Models\RawMaterial::RAW_MATERIAL_TYPE[$raw_material->type]}}
                    <div class="section-1">
                        <a href="#" class="btn btn-light btn-sm" data-toggle="modal"
                           data-target="#EditRawType">
                            <i class="zmdi zmdi-edit" data-toggle="tooltip"
                               data-placement="right" title="Edit RAw Type"></i>
                        </a>
                    </div>
                </div>

                <hr class="mt-1">

                <div class="text-center mt-1">


                    {{--                    @if(!empty($raw_material->pack))--}}
                    <div class="row">

                        <div class="col-md-10">Pack Type
                            : {{(!empty($raw_material->pack_type))?\RawMaterialManager\Models\RawMaterial::PACK_TYPE[$raw_material->pack_type]:'-'}}</div>
                        <div class="col-md-2">
                            <div class="section-1">
                                <a href="#" class="btn btn-light btn-sm" data-toggle="modal"
                                   data-target="#EditPack">
                                    <i class="zmdi zmdi-edit" data-toggle="tooltip"
                                       data-placement="right" title="Edit Pack"></i>
                                </a>
                            </div>
                        </div>
                    </div>


                    <div class="row mt-1">
                        <div class="col-md-10">Units Per Pack
                            : {{((!empty($raw_material->units_per_packs)))?$raw_material->units_per_packs.' '.$raw_material->measurement:0}} </div>
                        <div class="col-md-2">
                            <div class="section-1">
                                <a href="#" class="btn btn-light btn-sm" data-toggle="modal"
                                   data-target="#EditUnitsPerPack">
                                    <i class="zmdi zmdi-edit" data-toggle="tooltip"
                                       data-placement="right" title="Edit Units Per Pack"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <hr class="mt-1">



                    <div class="text-center">
                        @if($msupliers != 0)
                            @php
                        $msup = \SupplierManager\Models\Supplier::where('id','=',$msupliers)->first();
                            @endphp
                            Main Supplier : {{$msup->supplier_name}}

                        @else
                            Main Supplier : Not Define Yet
                        @endif
                    </div>
                    <div class="text-center">
                        @if($ssupliers != 0)
                            @php
                                $ssup = \SupplierManager\Models\Supplier::where('id','=',$ssupliers)->first();


                            @endphp
                            Sub Supplier : {{$ssup->supplier_name}}

                        @else
                            Sub Supplier : Not Define Yet
                        @endif
                    </div>
                    {{--                    @endif--}}

                </div>


                <hr class="mt-1">

                <div class="text-center mt-1">
                    <div class="row">
                        <div class="col-md-6 mt-1">Retail Fraction</div>
                        {{ Form::open(array('url' => 'raw_material/'.$raw_material->raw_material_id)) }}
                        <button type="submit" name="fraction" value="{{$raw_material->fraction}}"
                                class="btn btn-sm {{$raw_material->fraction==0?'btn-danger':'btn-success'}}">{{$raw_material->fraction==0?'disabled':'enabled'}}</button>
                        {{ Form::close() }}
                    </div>
                </div>

                <div class="text-center mt-1">
                    <div class="row">
                        <div class="col-md-6 mt-1">Whole Sale Fraction</div>
                        {{ Form::open(array('url' => 'raw_material/'.$raw_material->raw_material_id)) }}
                        <button type="submit" name="sale_fraction" value="{{$raw_material->sale_fraction}}"
                                class="btn btn-sm {{$raw_material->sale_fraction==0?'btn-danger':'btn-success'}}">{{$raw_material->sale_fraction==0?'disabled':'enabled'}}</button>
                        {{ Form::close() }}
                    </div>
                </div>


                <div class="text-center mt-1">
                    <div class="row">
                        <div class="col-md-6 mt-1">Status</div>
                        {{ Form::open(array('url' => 'raw_material/'.$raw_material->raw_material_id)) }}
                        <button type="submit" name="status" value="{{$raw_material->status}}"
                                class="btn btn-sm {{$raw_material->status==0?'btn-danger':'btn-success'}}">{{$raw_material->status==0?'disabled':'enabled'}}</button>
                        {{ Form::close() }}
                    </div>
                </div>

            </div>
        </div>
    </div>


    <div class="modal fade" id="supplier" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'supplier','enctype'=>'multipart/form-data', 'id'=>'bootstrap-wizard-form'))}}
                {{--<form id="bootstrap-wizard-form">--}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="wizard p-4" id="bootstrap-wizard-1">
                                <ul class="nav nav-tabs vertices keep-prefix-suffix" role="tablist">
                                    <li class="nav-item">
                                        <a class=" nav-link active" data-toggle="tab" href="#ex5-step-1"
                                           role="tab">
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#ex5-step-2"
                                           role="tab">
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="ex5-step-1" role="tabpanel">
                                        <div class="task-name-wrap">
                                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="name"
                                                   placeholder="Supplier Name">
                                        </div>
                                        <hr>
                                        <div class="task-name-wrap">
                                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="address"
                                                   placeholder="Supplier Address">
                                        </div>
                                        <hr>
                                        <div class="task-name-wrap">
                                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="tp"
                                                   placeholder="Telephone Number">
                                        </div>
                                        <hr>
                                        {{--<div class="task-name-wrap">--}}
                                        {{--<span><i class="zmdi zmdi-check"></i></span>--}}
                                        {{--<input class="task-name-field" type="text" name="dob"--}}
                                        {{--placeholder="Raw Materials">--}}
                                        {{--</div>--}}

                                        {{--<select class="js-example-responsive" name="permissions[]" multiple="multiple" style="width: 100%">--}}
                                        {{--@foreach($rmaterials as $raw)--}}
                                        {{--<option value=" {{$raw->raw_material_id}}">{{$raw->name}}</option>--}}
                                        {{--@endforeach--}}
                                        {{--</select>--}}
                                        {{--<hr>--}}

                                    </div>
                                    <div class="tab-pane" id="ex5-step-2" role="tabpanel">
                                        <div class="task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="file" name="image">
                                        </div>
                                        <hr>
                                    </div>
                                </div>

                                <div class="pager d-flex justify-content-center">
                                    <input type="submit" id="finish-btn" class="finish btn btn-success w-50"
                                           value="Finish"/>

                                    <button type="button" id="next-btn" class="next btn btn-success w-50">Next
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--</form>--}}
                {!!Form::close()!!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="EditImage" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{--{{ Form::open(array('url' => 'assistant','id'=>'jq-validation-example-1')) }}--}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="img" type="file" class="task-name-field" placeholder="first name">
                        </div>
                        <hr>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {{--{{ Form::close() }}--}}
            </div>
        </div>
    </div>

    <div class="modal fade" id="EditName" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'raw_material/'.$raw_material->raw_material_id)) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="Name" type="text" class="task-name-field" value="{{$raw_material->name}}">
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

    <div class="modal fade" id="BufferStock" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'raw_material/'.$raw_material->raw_material_id)) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="buffer_stock" type="text" class="task-name-field"
                                   value="{{$raw_material->buffer_stock}}">
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

    <div class="modal fade" id="availableQty" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'raw_material/'.$raw_material->raw_material_id)) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="available_qty" type="text" class="task-name-field"
                                   value="{{$raw_material->available_qty}}">
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

    <div class="modal fade" id="sinhala" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'raw_material/'.$raw_material->raw_material_id)) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="sinhala" type="text" class="task-name-field"
                                                 value="{{$raw_material->sin_name}}">
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

    <div class="modal fade" id="EditDesc" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">

        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'raw_material/'.$raw_material->raw_material_id)) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            <span class="px-2"><i class="zmdi zmdi-check"></i></span>
                            <textarea name="desc" cols="10" rows="20"
                                      class="task-name-field"> {{$raw_material->desc}} </textarea>
                        </div>


                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {{ Form::close() }}
                {{--</div>--}}
            </div>
        </div>
    </div>

    <div class="modal fade" id="EditScaleKey" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">

        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'raw_material/'.$raw_material->raw_material_id)) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            <div class="row task-name-wrap">
                                <span><i class="zmdi zmdi-check"></i></span>
                                <input name="scale_key" type="text" class="task-name-field" value="{{$raw_material->scale_key}}">
                            </div>

                        </div>


                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {{ Form::close() }}
                {{--</div>--}}
            </div>
        </div>
    </div>


    <div class="modal fade" id="PriceChange" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">

        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'raw_material/'.$raw_material->raw_material_id)) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            <span class="px-2"><i class="zmdi zmdi-check"></i></span>
                            <input name="price" type="number" class="task-name-field" step="0.01"
                                   value="{{($raw_material->price)?$raw_material->price:0}}">
                        </div>


                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="UPDATE NOW">
                </div>
                {{ Form::close() }}
                {{--</div>--}}
            </div>
        </div>
    </div>

    <div class="modal fade" id="EditPack" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">

        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'raw_material/'.$raw_material->raw_material_id)) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            {!! Form::select('pack_item', \RawMaterialManager\Models\RawMaterial::PACK_TYPE, null , ['class' => 'form-control','placeholder'=>'Select Pack']) !!}
                        </div>
                        <div class="row task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="units_per_pack" type="number" step="0.01" placeholder="units per pack"
                                   class="task-name-field" value="">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="UPDATE NOW">
                </div>
                {{ Form::close() }}
                {{--</div>--}}
            </div>
        </div>
    </div>

    <div class="modal fade" id="EditUnitsPerPack" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">

        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'raw_material/'.$raw_material->raw_material_id)) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="units_per_pack" type="text" class="task-name-field" value="">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="UPDATE NOW">
                </div>
                {{ Form::close() }}
                {{--</div>--}}
            </div>
        </div>
    </div>

    <div class="modal fade" id="EditRawType" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">

        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'raw_material/'.$raw_material->raw_material_id)) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row task-name-wrap">
                        {!! Form::select('raw_material_type', \RawMaterialManager\Models\RawMaterial::RAW_MATERIAL_TYPE, null , ['class' => 'form-control','placeholder'=>'Select Pack']) !!}
                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="UPDATE NOW">
                </div>
                {{ Form::close() }}
                {{--</div>--}}
            </div>
        </div>
    </div>

@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/waypoints/lib/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/waypoints/lib/shortcuts/sticky.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/counterup/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/js/jquery.sparkline.min.js')}}"></script>
    <script src="{{asset('js/site.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
@stop