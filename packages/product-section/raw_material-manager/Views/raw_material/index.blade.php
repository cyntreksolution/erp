@extends('layouts.back.master')@section('title','Raw Materials')
@section('css')
    <style type="text/css">
        #floating-button {
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #00C851;
            position: fixed;
            bottom: 80px;
            right: 30px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .plus {
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }

        .sml {
            height: 30px;
            width: 50px;
            text-align: center;
            font-size: 0.75rem;
        }

        td{
            text-align: right !important;
        }
    </style>
    {{--<link rel="stylesheet" href="{{asset('assets/vendor/bower_components/datatables.net/css/jquery.dataTables.min.css')}}">--}}
    {{--<link rel="stylesheet" href="{{asset('assets/vendor/dtable/Buttons-1.5.1/css/buttons.dataTables.css')}}">--}}
    {{--<link rel="stylesheet" href="{{asset('assets/vendor/dtable/Buttons-1.5.1/css/buttons.bootstrap4.min')}}">--}}
    {{--<link rel="stylesheet" href="{{asset('assets/vendor/dtable/DataTables-1.10.16/css/dataTables.bootstrap4.css')}}">--}}
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    {{--<link rel="stylesheet" href="{{asset('assets/vendor/dtable/DataTables-1.10.16/css/dataTables.bootstrap4.css')}}">--}}
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/datatables.net/css/buttons.dataTables.min.css')}}">
@stop
@section('content')
    @if($role = Auth::user()->hasRole(['Owner','Super Admin']))
        <div id="floating-button" data-toggle="tooltip" data-placement="left" data-original-title="NEW RAW MATERIAL"
             onclick="location.href = '{{url('raw_material/create')}}';">
            <p class="plus">+</p>
        </div>
    @endif
    <div class="row mb-3">
        <div class="col-md-7"></div>
        <div class="col-md-3 float-right  text-right">
            {!! Form::select('supplier',$suppliers,null,['class'=>'form-control','id'=>'supplier','placeholder'=>'Select Supplier']) !!}
        </div>
        <div class="col-md-2 float-right text-right">
            <button type="button" onclick="processForm()" class="btn btn-scondary"> <i class="fa fa-filter mr-2"></i> Filter Now</button>
        </div>
    </div>

    <div class="padding">
        <div class="row">
            <table id="example" class="display text-center">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th class="text-center">Current stock</th>
                    <th class="text-center">Current stock value (Rs.)</th>
                    <th class="text-center">Last 30days usage</th>
                    <th class="text-center">Per day usage</th>
                    <th class="text-center">Re-order Days</th>
                    <th class="text-center">Buffer Days</th>
                    <th class="text-center">Action</th>
                    {{--                    <th class="text-center">Last price</th>--}}
                    {{--                    <th class="text-center">Supplier</th>--}}
{{--                    <th class="text-center">Action</th>--}}

                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th class="text-center">Current stock</th>
                    <th class="text-center">Current stock value (Rs.)</th>
                    <th class="text-center">Last 30days usage</th>
                    <th class="text-center">Per day usage</th>
                    <th class="text-center">Re-order Days</th>
                    <th class="text-center">Buffer Days</th>
                    <th class="text-center">Action</th>
                    {{--                    <th class="text-center">Last price</th>--}}
                    {{--                    <th class="text-center">Supplier</th>--}}
{{--                    <th class="text-center">Action</th>--}}
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <style>
        .dataTables_wrapper {
            width: 100%;
        }
    </style>
@stop
@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/Buttons-1.5.1/js/dataTables.buttons.js')}}"></script>
    <script src="{{asset('/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/JSZip-2.5.0/jszip.min.js')}}"></script>
    <script src="{{asset('js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/pdfmake-0.1.32/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/vendor/dtable/Buttons-1.5.1/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('/js/vfs_fonts.js')}}"></script>
    <script>
        function processForm(){
            let supplier = $('#supplier').val();
            let table = $('#example').DataTable();
            table.ajax.url('raw_material/raw/list?supplier=' + supplier ).load();

        }
        var table;
        $(document).ready(function () {
            table = $('#example').DataTable({
                "ajax": '{{url('raw_material/raw/list')}}',
                dom: 'lfBrtip',
                width: '100%',
                float: 'right',
                "autoWidth": false,
                buttons: [
                    {
                        extend: 'copy',
                        className: 'btn btn-xs btn-info ml-3 sml assets-export-btn export-copy ttip',
                    },

                    {
                        extend: 'excel',
                        className: 'btn btn-xs btn-success ml-3 sml assets-export-btn export-copy ttip',
                    },
                    {
                        extend: 'pdf',
                        className: 'btn btn-xs btn-danger ml-3 sml assets-export-btn export-copy ttip',
                    },

                    {
                        extend: 'print',
                        className: 'btn btn-xs btn-primary ml-3 sml assets-export-btn export-copy ttip',
                    },
                ]

            });

            $('#example tbody').on('click', 'td button', function (e) {
                // var data = table.row( $(this).parents('tr') ).data();
                e.preventDefault();
                var id = $(this).attr("value");
                confirmAlert(id);

            });
        });


        function confirmAlert(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "delete",
                        url: id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            } else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

    </script>
@stop