<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web','log'])->group(function () {
    Route::prefix('recipe')->group(function () {
        Route::get('', 'RecipeManager\Controllers\RecipeController@index')->name('recipe.index');

        Route::get('create', 'RecipeManager\Controllers\RecipeController@create')->name('recipe.create');

        Route::get('{recipe}', 'RecipeManager\Controllers\RecipeController@show')->name('recipe.show');

        Route::get('{recipe}/edit', 'RecipeManager\Controllers\RecipeController@edit')->name('recipe.edit');

        Route::post('', 'RecipeManager\Controllers\RecipeController@store')->name('recipe.store');


        Route::post('{recipe}', 'RecipeManager\Controllers\RecipeController@update')->name('recipe.update');

        Route::post('active/new', 'RecipeManager\Controllers\RecipeController@active')->name('recipe.active');
        Route::post('new/group', 'RecipeManager\Controllers\RecipeController@Group')->name('recipe.group');


        Route::delete('{recipe}', 'RecipeManager\Controllers\RecipeController@destroy')->name('recipe.destroy');
    });
});
