<?php

namespace RecipeManager\Controllers;


use Illuminate\Support\Facades\Auth;
use RecipeManager\Models\RecipeGroup;
use RecipeManager\Models\RecipeSize;
use Validator;
use RecipeManager\Requests\RecipeRequest;
use Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use RawMaterialManager\Models\RawMaterial;
use RecipeManager\Models\Recipe;
use RecipeManager\Models\RecipeContent;
use RecipeManager\Models\RecipeSemiProduct;
use SemiFinishProductManager\Models\SemiFinishProduct;

class RecipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect(route('recipe.create'));
        return view('RecipeManager::recipe.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = RawMaterial::withoutGlobalScope('type')->get();
        $semi = SemiFinishProduct::all();
        $lastSemi = SemiFinishProduct::orderBy('semi_finish_product_id', 'desc')->take(4)->get();
        $rec = Recipe::with('semiProducts', 'recipeContents.materials')->orderBy('name')->get();
        return view('RecipeManager::recipe.create')->with([
            'products' => $products,
            'sps' => $semi,
            'recipes' => $rec,
            'lastSemis' => $lastSemi,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request
        $validator = Validator::make($request->all(), [
            'ingredients' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect(route('recipe.create'))
                ->with([
                    'error' => true,
                    'error.title' => 'Error !',
                    'error.message' => 'No Semi Product Selected'
                ])
                ->withInput();
        } else {
            $name = $request->recipe_name;
            $desc = $request->desc;
            $semiProduct = $request->semi_product;
            $units_per_recipe = $request->units_per_recipe;
            $ingredients = $request->ingredients;
            $qty = $request->qty;
            $recipe_weight = $request->bweight;

            try {
                DB::transaction(function () use ($name, $desc, $semiProduct, $ingredients, $qty, $units_per_recipe,$request) {
                    $recipe = new Recipe();
                    $recipe->name = $name;
                    $recipe->desc = $desc;

                    $recipe->recipe_weight = $request->bweight;
                    $recipe->recipe_item = $request->bitem;
                    $recipe->stage_1 = $request->first_stage;
                    $recipe->stage_2 = $request->second_stage;
                    $recipe->recipe_group = $request->recipegroup;



                    $recipe->colour = rand() % 7;
                    $recipe->created_by = Auth::user()->id;
                    $recipe->save();

                    foreach ($ingredients as $key => $ingredient) {
                        $rcon = new RecipeContent();
                        $rcon->recipe_id = $recipe->id;
                        $rcon->raw_material_id = $ingredient;
                        $rcon->qty = $qty[$key];
                        $rcon->save();
                    }
                    $recipe->semiProducts()->attach($semiProduct, ['units' => $units_per_recipe]);
                });

                return redirect(route('recipe.create'))->with([
                    'success' => true,
                    'success.title' => 'Congratulations !',
                    'success.message' => 'New recipe has been added'
                ]);
            } catch (Exception $e) {
                return $e;
                return redirect(route('recipe.create'))->with([
                    'error' => true,
                    'error.title' => 'Sorry !',
                    'error.message' => 'Something Went Wrong '
                ]);
            }
        }


    }

    /**
     * Display the specified resource.
     *
     * @param \RecipeManager\Models\Recipe $recipe
     * @return \Illuminate\Http\Response
     */
    public function show(Recipe $recipe)
    {
        $id = $recipe->id;
        $group = RecipeGroup::all();
        $recipe = Recipe::with('recipeContents.materials', 'semiProducts')->where('id', '=', $id)->first();
        $material = RecipeContent::where('recipe_id', '=', $recipe->id)->with('materials')->get();
        return view('RecipeManager::recipe.show')->with([
            'recipe' => $recipe,
            'materials' => $material,
            'groups' => $group,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \RecipeManager\Models\Recipe $recipe
     * @return \Illuminate\Http\Response
     */
    public function edit(Recipe $recipe)
    {
        return view('RecipeManager::recipe.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \RecipeManager\Models\Recipe $recipe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $recipe = Recipe::find($request->recipe_id);
        $recipe->recipe_weight = $request->recipe_weight;
        $recipe->unit_price = $request->unit_price;
        $recipe->recipe_item = $request->bitem;
        $recipe->recipe_group = $request->recipegroup;
        $recipe->stage_1 = $request->first_stage;
        $recipe->stage_2 = $request->second_stage;



        //$recipe->is_make = $request->is_make;
        //$recipe->is_burn = $request->is_burn;
        $recipe->save();
        return redirect()->back()->with([
            'info' => true,
            'info.title' => 'Congratulations !',
            'info.message' => 'Recipe Updated'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \RecipeManager\Models\Recipe $recipe
     * @return \Illuminate\Http\Response
     */
    public function destroy(Recipe $recipe)
    {
        $recipe->delete();
        if ($recipe->trashed()) {
            return true;
        } else {
            return false;
        }
    }

    public function active(Request $request)
    {
        $rop = RecipeSemiProduct::find($request->id);
        $other_active = RecipeSemiProduct::where('semi_finish_product_semi_finish_product_id', '=', $rop->semi_finish_product_semi_finish_product_id)
            ->where('status', '=', 2)
            ->first();
        if ($other_active) {
            $other_active->status = 1;
            $other_active->save();
        }
        $rop->status = 2;
        $rop->save();
        return 'true';
    }


    public function Group(Request $request)
    {

        $gp = new RecipeGroup();
        $gp->group_name = $request->groupname;
        $gp->save();

        $id = $request->recipe_id;
        $group = RecipeGroup::where('id', '=',$request->recipe_id)->with('group_name')->get();
        $recipe = Recipe::with('recipeContents.materials', 'semiProducts')->where('id', '=', $id)->first();
        $material = RecipeContent::where('recipe_id', '=', $recipe->id)->with('materials')->get();
        return view('RecipeManager::recipe.show')->with([
            'recipe' => $recipe,
            'materials' => $material,
            'groups' =>$group,
        ]);


    }


}
