<?php

namespace RecipeManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecipeContent extends Model
{
    use SoftDeletes;

    protected $table = 'recipe_content';

    protected $primaryKey = 'recipe_content_id';

    protected $dates = ['deleted_at'];

    public function recipe(){
        return $this->belongsTo('RecipeManager\Models\Recipe');
    }

    public function materials(){
        return $this->hasOne('RawMaterialManager\Models\RawMaterial','raw_material_id','raw_material_id')->withoutGlobalScopes();
    }
}
