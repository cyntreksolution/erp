<?php

namespace RecipeManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecipeSemiProduct extends Model
{
    use SoftDeletes;

    protected $table = 'recipe_semi_finish_product';

    protected $dates = ['deleted_at'];

    public function recipe(){
        return $this->belongsTo('RecipeManager\Models\Recipe');
    }

}
