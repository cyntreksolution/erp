<?php

namespace RecipeManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Recipe extends Model
{
    use SoftDeletes;

    protected $table = 'recipe';

    protected $dates = ['deleted_at'];

    public function semiProducts()
    {
        return $this->belongsToMany('SemiFinishProductManager\Models\SemiFinishProduct');
    }

    public function recipeContents(){
        return $this->hasMany('RecipeManager\Models\RecipeContent');
    }

}
