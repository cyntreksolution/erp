<?php

namespace RecipeManager;

use Illuminate\Support\ServiceProvider;

class RecipeProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/Views', 'RecipeManager');
        require __DIR__ . '/web.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('RecipeManager', function($app){
            return new RecipeManager;
        });
    }
}
