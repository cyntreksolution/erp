<?php
/**
 * Created by PhpStorm.
 * User: ccdil
 * Date: 12/25/2017
 * Time: 8:52 PM
 */

namespace RecipeManager\Classes;


use RecipeManager\Models\RecipeSemiProduct;

class RecipeContent
{
    public static function getActiveRecipe($semi_finish_product)
    {
        $active = RecipeSemiProduct::where('semi_finish_product_semi_finish_product_id', '=', $semi_finish_product)
            ->where('status', '=', 2)
            ->with('recipe.recipeContents.materials')
            ->first();
        return $active;
    }

    public static function getExpectedQty($semi_finish_product)
    {
        return $qty_per_recipe = self::getActiveRecipe($semi_finish_product)->units;
    }
}