@extends('layouts.back.master')@section('title','Recipe Detilas')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/lightbox2/dist/css/lightbox.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}"/>

    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">

    <style>


        #container {
            position: relative;
            padding: 0px;
            width: 100%;
            height: 330px;
            overflow: auto;
        }

        #container .content {
            width: 100%;
            height: 20%;
        }

        .profile-pic {
            position: relative;
            /*display: inline-block;*/
        }

        .profile-pic:hover .edit {
            display: block;
        }

        .edit {
            padding-top: 7px;
            padding-right: 7px;
            position: absolute;
            right: 0;
            top: 0;
            display: none;
        }

        .edit a {
            color: #000;
        }
    </style>

@stop
@section('current','Recipe')
@section('current_url',route('recipe.create'))
@section('current2',$recipe->name)
@section('current_url2','')
@section('content')
    <div class="profile-wrapper">
        <div class="profile-section-user">
            <div class="profile-cover-img profile-pic">
                <img src="{{asset('assets\img\recipe.jpg')}}" alt="">
            </div>

            <div class="profile-info-brief p-3">
                @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                <div class="user-profile-avatar avatar avatar-circle avatar-md project-iconr bg-{{$color[$recipe->colour]}}"
                     data-plugin="firstLitter"
                     data-target="#{{$recipe->id*754}}"></div>

                <div class="text-center">
                    <h5 class="text-uppercase mb-1" id="{{$recipe->id*754}}">{{$recipe->name}}
                        <a href="#" class="btn btn-light btn-sm">
                            <i class="zmdi zmdi-edit"></i>
                        </a>
                    </h5>
                    @php
                        $act= RecipeManager\Models\RecipeSemiProduct::where('recipe_id','=',$recipe->id)->first();
                        $semi=SemiFinishProductManager\Models\SemiFinishProduct::where('semi_finish_product_id','=',$act->semi_finish_product_semi_finish_product_id)->first();
                    @endphp

                    <h6 class="text mb-lg-1">{{$semi->name}}</h6>
                    <h6 class="text mb-lg-1">Units per Recipe : {{$act->units}} units</h6>
                    @if($act->status==2)
                        <i class="status status-online"></i><span
                                class="text-success">Active Recipe</span>
                    @else
                        <i class="status status-busy"></i><span
                                class="text-danger">Inactive Recipe</span>
                    @endif
                    <br>
                </div>

                <hr class="m-0">

                <div class="text-center">
                    {{$recipe->desc}}
                    <div class="section-1">
                        <a href="#" class="btn btn-light btn-sm">
                            <i class="zmdi zmdi-edit"></i>
                        </a>
                    </div>
                </div>
                <div class="form-group">
                <button class="btn btn-danger btn-block mt-3" data-toggle="modal"
                        data-target="#AddNewGroup">Add New Group
                </button>
                </div>



                {!! Form::open(['route' => ['recipe.update',$recipe->id], 'method' => 'post']) !!}
                <input type="hidden" name="recipe_id" value="{{$recipe->id}}">
                <div class="form-group">
                    <label class="col-8 col-form-label">Recipe Weight (g)</label>
                    <div class="col-12">
                        <input type="text" class="form-control" name="recipe_weight"  value="{{$recipe->recipe_weight}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-8 col-form-label">Unit Price Rs.{{$recipe->unit_price}}</label>
                    <div class="col-12">
                        <input type="text" class="form-control" name="unit_price" value="{{$recipe->unit_price}}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-8 col-form-label">Items For Burden = {{$recipe->recipe_item}}</label>
                    <div class="col-12">
                    <input class="form-control" type="text" name="bitem">
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-8 col-form-label">Group-{{$recipe->recipe_group}}</label>
                    <div class="col-12">

                    <select name="recipegroup" class="form-control select2">
                        <option value=''>Select Group</option>
                        <option value="NoGroup">No Group</option>
                        @foreach($groups as $group)
                        <option value="{{$group->id}}">{{$group->group_name}}</option>
                        @endforeach
                    </select>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-8 col-form-label">First Stage-{{$recipe->stage_1}}</label>
                    <div class="col-12">

                    <select name="first_stage" class="form-control select2">
                        <option value=''>Select First Stage</option>
                        <option value="NoFirst">No First Stage</option>
                        <option value="Remove">Remove</option>
                        <option value="Unload">Unload</option>
                        <option value="Grind">Grind</option>
                        <option value="Make">Make</option>
                        <option value="Mill">Mill</option>
                        <option value="Mix">Mix</option>
                        <option value="Prepair">Prepair</option>
                        <option value="Pack">Pack</option>
                        <option value="Burn">Burn</option>
                        <option value="Cook">Cook</option>
                        <option value="Bake">Bake</option>
                        <option value="Fry">Fry</option>
                        <option value="DeepFry">Deep Fry</option>
                        <option value="Boil">Boil</option>
                        <option value="Wrap">Wrap</option>
                        <option value="Cut">Cut</option>
                        <option value="Cream">Cream</option>
                        <option value="Paste">Paste</option>
                        <option value="Seal">Seal</option>
                        <option value="Sort">Sort</option>



                    </select>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-8 col-form-label">Second Stage-{{$recipe->stage_2}}</label>
                    <div class="col-12">
                    <select name="second_stage" class="form-control select2">
                        <option value=''>Select Second Stage</option>
                        <option value="NoSecond">No Second Stage</option>
                        <option value="Remove">Remove</option>
                        <option value="Unload">Unload</option>
                        <option value="Grind">Grind</option>
                        <option value="Make">Make</option>
                        <option value="Mill">Mill</option>
                        <option value="Mix">Mix</option>
                        <option value="Prepair">Prepair</option>
                        <option value="Pack">Pack</option>
                        <option value="Burn">Burn</option>
                        <option value="Cook">Cook</option>
                        <option value="Bake">Bake</option>
                        <option value="Fry">Fry</option>
                        <option value="DeepFry">Deep Fry</option>
                        <option value="Boil">Boil</option>
                        <option value="Wrap">Wrap</option>
                        <option value="Cut">Cut</option>
                        <option value="Cream">Cream</option>
                        <option value="Paste">Paste</option>
                        <option value="Seal">Seal</option>
                        <option value="Sort">Sort</option>



                    </select>
                    </div>
                </div>


                <!--div class="form-group">
                    <label class="col-6 col-form-label">Make</label>
                    <input type="checkbox" name="is_make" id="status" class="one" data-plugin="switchery"
                           data-size="medium" value="{{($recipe->is_make==1)?1:0 }}" {{($recipe->is_make==1) ?'checked':null }} >
                </div>
                <div class="form-group">
                    <label class="col-6 col-form-label">Burn </label>
                    <input type="checkbox" name="is_burn" id="status" class="one" data-plugin="switchery"
                           data-size="medium" value="{{($recipe->is_burn==1)?1:0 }}" {{($recipe->is_burn==1) ?'checked':null }}>
                </div-->

                <div class="form-group">
                    <input type="submit" class="btn btn-primary btn-block mt-3" value="Update">
                </div>

                {!! Form::close() !!}
            </div>
        </div>

        <div class="profile-section-main">
            <ul class="nav nav-tabs profile-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#profile-overview" role="tab">Recipe Content</a>
                </li>
            </ul>

            <div class="tab-content profile-tabs-content">
                <div class="tab-pane active" id="profile-overview" role="tabpanel">
                    <div class="stream-posts">
                        <div class="stream-post">
                            <div class="sp-author"> Ingredients</div>
                            <div class="sp-content">
                                <div class="sp-paragraph mb-0">
                                    <div class="app-main">
                                        <div class="app-main-content">
                                            <div class="media-list" id="container">
                                                @foreach($materials as $material)
                                                    <a href="#">
                                                        <div class="media content my-2">
                                                            @if(isset($material->image))
                                                                <div class="avatar avatar avatar-sm">
                                                                    <img src="{{asset($material->image_path.$material->image)}}"
                                                                         alt="">
                                                                </div>
                                                            @else
                                                                @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                                                <div
                                                                        class="avatar avatar-circle avatar-md project-icon bg-{{$color[$material['materials']->colour]}} "
                                                                        data-plugin="firstLitter"
                                                                        data-target="#raw-{{$material['materials']->raw_material_id*874}}">
                                                                </div>
                                                            @endif


                                                            <div class="media-body">
                                                                <h6 class="media-heading my-1">
                                                                    <h6 id="raw-{{$material['materials']->raw_material_id*874}}">{{$material['materials']->name}}</h6>
                                                                </h6>
                                                                @if($material['materials']->measurement=='gram')
                                                                    @if($material['qty'] >= 1000)
                                                                        <h6>{{$material['qty']/1000}} kg</h6>
                                                                    @else
                                                                        <h6>{{$material['qty']}} g</h6>
                                                                    @endif

                                                                @elseif($material['materials']->measurement=='milliliter')
                                                                    @if($material['qty'] >= 1000)
                                                                        <h6>{{$material['qty']/1000}} l</h6>
                                                                    @else
                                                                        <h6>{{$material['qty']}} ml</h6>
                                                                    @endif
                                                                @elseif($material['materials']->measurement=='unit')
                                                                    <h6>{{$material['qty']}} units</h6>
                                                                @endif

                                                                {{--<small>{{$material->qty}} g</small>--}}
                                                            </div>
                                                            <div class="section-2">
                                                                <!--button class="remove btn btn-light btn-sm ml-2"
                                                                        value="#"
                                                                        data-toggle="tooltip"
                                                                        data-placement="left"
                                                                        title="Remove Frome Recipe">
                                                                    <i class="zmdi zmdi-close"></i>
                                                                </button-->
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <hr class="m-0">
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

        </div>
    </div>


    <div class="modal fade" id="AddNewGroup" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['route' => ['recipe.group',$recipe->id], 'method' => 'post']) !!}
                {{-- Form::open(array('url' => 'recipe/new/group','enctype'=>'multipart/form-data','id'=>'jq-validation-example-1'))--}}
                {!!Form::token()!!}
                <input type="hidden" name="recipe_id" value="{{$recipe->id}}">
                <div class="modal-header">
                    <button type="button" class="align-right close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="form-control zmdi zmdi-close"></i></span>
                    </button>
                    {{--<div class="task-pickers">--}}
                    {{--<div class="task-field-picker task_date_picker">--}}
                    {{--<div for="task_due_date">--}}
                    {{--<i class="zmdi zmdi-calendar"></i>--}}
                    {{--</div>--}}
                    {{--<input type="text" name="specification"--}}
                    {{--placeholder="specification">--}}
                    {{--</div>--}}
                    {{--</div>--}}
                </div>
                <div class="modal-body">
                    <div class="task-name-wrap">

                        <input class="form-control" type="text" name="groupname" placeholder="Type Group Name">
                    </div>


                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {!!Form::close()!!}
            </div>
        </div>
    </div>




@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/waypoints/lib/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/waypoints/lib/shortcuts/sticky.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/counterup/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/js/jquery.sparkline.min.js')}}"></script>
    <script src="{{asset('js/site.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>


    <script>
        var ps = new PerfectScrollbar('#container');
    </script>
@stop