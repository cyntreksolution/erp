<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web'])->group(function () {
    Route::prefix('production/order')->group(function () {
        Route::get('', 'ProductionOrderManager\Controllers\ProductionOrderController@index')->name('proOrder.index');

        Route::get('create', 'ProductionOrderManager\Controllers\ProductionOrderController@create')->name('proOrder.create');

        Route::get('{order}', 'ProductionOrderManager\Controllers\ProductionOrderController@show')->name('proOrder.show');

        Route::get('{order}/edit', 'ProductionOrderManager\Controllers\ProductionOrderController@edit')->name('proOrder.edit');


        Route::post('', 'ProductionOrderManager\Controllers\ProductionOrderController@store')->name('proOrder.store');

        Route::put('{order}', 'ProductionOrderManager\Controllers\ProductionOrderController@update')->name('proOrder.update');

        Route::delete('{order}', 'ProductionOrderManager\Controllers\ProductionOrderController@destroy')->name('proOrder.destroy');
    });
});