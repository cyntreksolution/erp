<?php

namespace ProductionOrderManager;

use Illuminate\Support\ServiceProvider;

class ProductionOrderServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/Views', 'ProductionOrderManager');
        require __DIR__ . '/web.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('ProductionOrderManager', function($app){
            return new ProductionOrderManager;
        });
    }
}
