<?php

namespace ProductionOrderManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RawMaterialManager\Models\RawMaterial;

class ProductionOrder extends Model
{
    use SoftDeletes;

    protected $table = 'production_order';

    protected $dates = ['deleted_at'];

    public function requestedEndProducts(){
        return $this->hasMany('ProductionOrderManager\Models\ProductionOrderEndProduct','production_order_id','id');
    }

    public function requestedSemiProducts(){
        return $this->hasMany('ProductionOrderManager\Models\ProductionOrderSemiProduct','production_order_id','id');
    }
    public function requestedRawMaterials(){
        return $this->hasMany(ProductionRawMaterial::class);
    }

}
