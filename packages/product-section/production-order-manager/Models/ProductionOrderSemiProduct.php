<?php

namespace ProductionOrderManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductionOrderSemiProduct extends Model
{
    use SoftDeletes;

    protected $table = 'po_semi';

    protected $dates = ['deleted_at'];

    public function semiProdcuts(){
        return $this->belongsTo('SemiFinishProductManager\Models\SemiFinishProduct','semi_product_id','semi_finish_product_id');
    }

}
