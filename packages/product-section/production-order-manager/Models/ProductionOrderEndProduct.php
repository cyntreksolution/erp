<?php

namespace ProductionOrderManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductionOrderEndProduct extends Model
{
    use SoftDeletes;

    protected $table = 'po_end_product';

    protected $dates = ['deleted_at'];

    public function endProducts(){
        return $this->belongsTo('EndProductManager\Models\EndProduct','end_product_id','id');
    }

}
