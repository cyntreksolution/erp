<?php

namespace ProductionOrderManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RawMaterialManager\Models\RawMaterial;

class ProductionRawMaterial extends Model
{
    use SoftDeletes;

    protected $table = 'po_raw';

    protected $dates = ['deleted_at'];

    public function rawMaterial(){
        return $this->belongsTo(RawMaterial::class,'raw_material_id','raw_material_id');
    }

}
