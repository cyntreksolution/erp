<?php

namespace ProductionOrderManager\Controllers;

use Illuminate\Support\Facades\Auth;
use App\RawInquiry;
use App\SemiInquiry;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use ProductionOrderManager\Models\ProductionOrder;
use ProductionOrderManager\Models\ProductionOrderEndProduct;
use SalesOrderManager\Models\SalesOrder;
use SemiFinishProductManager\Models\RecipeSize;
use SemiFinishProductManager\Models\SemiFinishProduct;
use EndProductManager\Classes\ProductContent;
use EndProductManager\Models\EndProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductionOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:production_order-index', ['only' => ['index']]);
        $this->middleware('permission:NewBurden-list', ['only' => ['list']]);
        $this->middleware('permission:production_order-create', ['only' => ['create','store']]);
        $this->middleware('permission:production_order-delete', ['only' => ['destroy']]);

    }
    public function index()
    {

        $data=array();
        $burning_total=0;
        $semi_finish_product = DB::table('recipe_semi_finish_product')
            ->join('semi_finish_product', 'recipe_semi_finish_product.semi_finish_product_semi_finish_product_id', '=', 'semi_finish_product.semi_finish_product_id')
            ->where('recipe_semi_finish_product.status', '=', 2)
            ->get();

        foreach ($semi_finish_product as $key => $item) {
            $totalb = DB::table('burden')
                ->select(DB::raw('SUM(burden.expected_semi_product_qty) as totalb'))
                ->where('semi_finish_product_id', '=', $item->semi_finish_product_id)
                ->where('status', '<', 6)
                ->where('deleted_at', '=', null)
                ->first();
            $totalc = DB::table('cooking_requests')
                ->select(DB::raw('SUM(cooking_requests.expected_semi_product_qty) as totalc'))
                ->where('semi_finish_product_id', '=', $item->semi_finish_product_id)
                ->where('status', '<', 6)
                ->where('deleted_at', '=', null)
                ->first();

            $last14 = abs(SemiInquiry::where('semi_id', '=', $item->semi_finish_product_id)->where('created_at', '>', Carbon::now()->subDays(14)->format('Y-m-d H:i:s'))->where('type', '=', 2)->sum('qty'));
            $pday = ($last14/14);

            $total = (($totalb->totalb)+($totalc->totalc));
            if ($total > 0){
                $burning_total = $total;

                }else{
                $burning_total =0;
                }


            $details = [
                'semi_id' => $item->semi_finish_product_id,
                'name' => $item->name,
                'exqty' => $item->units,
                'image' => $item->image,
                'image_path' => $item->image_path,
                'buffer_stock' => $item->buffer_stock,
                'is_bake' => $item->is_bake,
                'available_qty' => $item->available_qty,
                'pday' => $pday,
                'colour' => $item->colour,
                'burning_total'=>$burning_total,

            ];

            //$semiSize = RecipeSize::where('semi_id','=',$item->semi_finish_product_id)->first();

            //array_push($data,$details,$semiSize);
            array_push($data,$details);

        }

        return view('ProductionOrderManager::production_order.index')->with([
            'semi' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $endProducts = $request->end_produts;
        $endProductQty = $request->end_produts_qty;
        $kg_per_burdens = $request->kg_per_burdens;
        $active_recipe = null;

        $poh = new ProductionOrderHeader();
        $poh->created_by = Auth::user()->id;
        $poh->save();

        foreach ($endProducts as $key => $endProduct) {
            $pod = new ProductionOrderData();
            $pod->production_order_header_id = $poh->id;
            $pod->end_product_end_product_id = $endProduct;
            $pod->qty = $endProductQty[$key];
            $pod->burden_kg = $kg_per_burdens[$key];
            $pod->recipe_id = ProductContent::getRecipsOfProduct(EndProduct::find($endProduct))[0]->id;
            $pod->save();
        }
        return redirect(back());

    }

    /**
     * Display the specified resource.
     *
     * @param  \EndProductManager\Models\EndProduct $end_product
     * @return \Illuminate\Http\Response
     */
    public function show(EndProduct $end_product)
    {
        return view('EndProductManager::end_product.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \EndProductManager\Models\EndProduct $end_product
     * @return \Illuminate\Http\Response
     */
    public function edit(EndProduct $end_product)
    {
        return view('EndProductManager::end_product.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \EndProductManager\Models\EndProduct $end_product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EndProduct $end_product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \EndProductManager\Models\EndProduct $end_product
     * @return \Illuminate\Http\Response
     */
    public function destroy(EndProduct $end_product)
    {
        $end_product->delete();
        if ($end_product->trashed()) {
            return true;
        } else {
            return false;
        }
    }
}
