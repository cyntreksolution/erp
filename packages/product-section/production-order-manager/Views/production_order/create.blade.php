@extends('layouts.back.master')@section('title','New End Product')
@section('css')
    {{--<link rel="stylesheet" href="{{asset('assets/examples/css/demos/demos.css')}}">--}}
    {{--<link rel="stylesheet" href="{{asset('assets/css/site.css')}}">--}}

    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.wizard.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/css/style.css')}}">

    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <style>
        .t-name-wrap span {
            display: inline-block;
            width: 32px;
            height: 32px;
            line-height: 32px;
            text-align: center;
            border-radius: 500px;
            position: absolute;
            left: 0;
            top: calc(50% - 15px);
            border: 1px solid #ccc;
            font-size: 1.5rem;
            color: #ccc
        }

        .t-name-field {
            padding-left: 42px;
            height: 60px;
            font-size: 1.75rem;
            font-weight: 500;
            border: none;
            outline: none;
            box-shadow: none;
            width: 100%;
            color: #868e96
        }
    </style>
@stop
@section('content')

    <div class="site-content">
        <div class="demo-wrapper"><!--div closed-->

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="wizard p-4" id="wizard-keep-prefix-suffix-demo">
                            <ul class="nav nav-tabs vertices keep-prefix-suffix" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#ex5-step-1" role="tab">
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#ex5-step-2" role="tab">
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">

                                <div class="tab-pane p-5 active" id="ex5-step-1" role="tabpanel">
                                    <div class="text-center">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="t-name-wrap">
                                                    <span><i class="fa fa-user"></i> </span>
                                                    <input class="t-name-field" type="text" name="name"
                                                           placeholder="Name"
                                                           style=" font-size: 1rem;">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="t-name-wrap">
                                                    <span><i class="fa fa-user"></i> </span>
                                                    <input class="t-name-field" type="text" name="barcode"
                                                           placeholder="Barcode"
                                                           style=" font-size: 1rem;">
                                                </div>
                                            </div>
                                        </div><!--row-->

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="t-name-wrap">
                                                    <span><i class="fa fa-user"></i> </span>
                                                    <input class="t-name-field" type="file" name="img"
                                                           placeholder="Image"
                                                           style=" font-size: 1rem;">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="t-name-wrap">
                                                    <span><i class="fa fa-user"></i> </span>
                                                    <input class="t-name-field" type="text" name="weight"
                                                           placeholder="Weight"
                                                           style=" font-size: 1rem;">
                                                </div>
                                            </div>
                                        </div><!--row-->

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="t-name-wrap">
                                                    <span><i class="fa fa-user"></i> </span>
                                                    <input class="t-name-field" type="text" name="buffer_stock"
                                                           placeholder="Buffer Stock"
                                                           style=" font-size: 1rem;">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="t-name-wrap">
                                                    <span><i class="fa fa-user"></i> </span>
                                                    <input class="t-name-field" type="text" name="price"
                                                           placeholder="Unit Price"
                                                           style=" font-size: 1rem;">
                                                </div>
                                            </div>
                                        </div><!--row-->

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="t-name-wrap">
                                                    <span><i class="fa fa-user"></i> </span>
                                                    <input class="t-name-field" type="text" name="fn"
                                                           placeholder="Description"
                                                           style=" font-size: 1rem;">
                                                </div>
                                            </div>
                                        </div><!--row-->

                                    </div>
                                </div><!-- /.tab-pane -->

                                <div class="tab-pane p-5" id="ex5-step-2" role="tabpanel">
                                    {{--<div class="text-center">--}}
                                    <div class="row">
                                        <div class="col-lg-7">
                                            <div class="app-main">
                                                {{--<div class="app-main-header">--}}
                                                {{--</div>--}}
                                                <div class="app-main-content">
                                                    {{--<div class="row">--}}
                                                    {{--<div class="col-md-6">--}}
                                                    <div class="media-list left-list">

                                                        {{--<div class="card">--}}
                                                        <div class="wizard border-0" id="wizard-basic-demo">
                                                            <ul class="nav nav-tabs keep-prefix-suffix" role="tablist">
                                                                <li class="nav-item"><a class="nav-link active"
                                                                                        data-toggle="tab"
                                                                                        href="#ex6-step-1"
                                                                                        role="tab">Step One</a></li>
                                                                <li class="nav-item"><a class="nav-link"
                                                                                        data-toggle="tab"
                                                                                        href="#ex6-step-2"
                                                                                        role="tab">Step Two</a></li>
                                                                <li class="nav-item"><a class="nav-link"
                                                                                        data-toggle="tab"
                                                                                        href="#ex6-step-3"
                                                                                        role="tab">Step Three</a></li>
                                                            </ul>

                                                            <div class="tab-content">
                                                                <div class="tab-pane p-5 active" id="ex6-step-1"
                                                                     role="tabpanel">
                                                                    <div id="connected">
                                                                        <div class="connected list no2"
                                                                             style="background-color: #b1b7ba; float: left">
                                                                            &nbsp;
                                                                            <div class="card p-1 bg-faded">
                                                                                <div class="media bg-white p-3">
                                                                                    <img class="avatar avatar-circle avatar-md"
                                                                                         src="{{asset('assets/global/images/208.jpg')}}../"
                                                                                         alt="">
                                                                                    <div class="media-body"><h6>
                                                                                            Clementina Blazier</h6>
                                                                                        <div>Android Developer</div>
                                                                                    </div><!-- /.media-body -->
                                                                                </div><!-- /.media -->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- /.tab-pane -->

                                                                <div class="tab-pane p-5" id="ex6-step-2"
                                                                     role="tabpanel">
                                                                    <div id="connected">
                                                                        <div class="connected list no2"
                                                                             style="background-color: #b1b7ba; float: left">
                                                                            &nbsp;
                                                                            <div class="card p-1 bg-faded">
                                                                                <div class="media bg-white p-3">
                                                                                    <img class="avatar avatar-circle avatar-md"
                                                                                         src="{{asset('assets/global/images/208.jpg')}}../"
                                                                                         alt="">
                                                                                    <div class="media-body"><h6>
                                                                                            Clementina Blazier</h6>
                                                                                        <div>Android Developer</div>
                                                                                    </div><!-- /.media-body -->
                                                                                </div><!-- /.media -->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- /.tab-pane -->

                                                                <div class="tab-pane p-5" id="ex6-step-3"
                                                                     role="tabpanel">
                                                                    <div id="connected">
                                                                        <div class="connected list no2"
                                                                             style="background-color: #b1b7ba; float: left">
                                                                            &nbsp;
                                                                            <div class="card p-1 bg-faded">
                                                                                <div class="media bg-white p-3">
                                                                                    <img class="avatar avatar-circle avatar-md"
                                                                                         src="{{asset('assets/global/images/208.jpg')}}../"
                                                                                         alt="">
                                                                                    <div class="media-body"><h6>
                                                                                            Clementina Blazier</h6>
                                                                                        <div>Android Developer</div>
                                                                                    </div><!-- /.media-body -->
                                                                                </div><!-- /.media -->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- /.tab-pane -->
                                                            </div><!-- /.tab-content -->
                                                        </div><!-- /.wizard -->
                                                    </div><!--media-list-->
                                                    {{--</div>--}}


                                                    {{--</div><!--col-->--}}
                                                    {{--</div><!--row px-4-->--}}
                                                </div><!--app-main-content-->
                                            </div> <!--app-main-->


                                        </div><!--col-->
                                        <div class="col-md-5">
                                            <div id="connected">
                                                <div class="app-panel " id="app-panel">
                                                    <div class="app-panel-inner connected list no2">
                                                        <div class="scroll-container">

                                                            <div class="p-3">
                                                                <button class="btn btn-success py-3 btn-block btn-lg"
                                                                        data-toggle="modal"
                                                                        data-target="#projects-task-modal">CREATE
                                                                </button>
                                                            </div>
                                                            <hr class="m-0">
                                                        </div><!--scroll-->

                                                    </div><!--inner-->

                                                    <a href="#" class="app-panel-toggle" data-toggle="class"
                                                       data-target="#app-panel" data-class="show"><i
                                                                class="fa fa-chevron-right"></i>
                                                        <i class="fa fa-chevron-left"></i>
                                                    </a>
                                                </div><!--app-pannel-->
                                            </div>
                                        </div><!--col-->
                                    </div><!--row-->

                                    {{--</div>--}}
                                </div><!-- /.tab-pane -->

                                <div class="pager d-flex justify-content-between">
                                    <button type="button" class="previous btn btn-outline-secondary">Prevoius
                                    </button>
                                    <button type="button" class="next btn btn-outline-success">Next</button>
                                </div><!-- /.pager -->
                            </div><!--tab-content-->


                        </div><!--wizard-->
                    </div><!--card-->
                </div><!--col-->
            </div><!--row-->

        </div><!--wrapper-->
    </div><!--site-content-->

@stop
@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/js/jquery.sortable.js')}}"></script>
    <script src="{{asset('assets/examples/js/demos/form.wizard.js')}}"></script>
    <script>
        $(function () {
            $('.sortable').sortable();
            $('.handles').sortable({
                handle: 'span'
            });
            $('.connected').sortable({
                connectWith: '.connected'
            });
            $('.exclude').sortable({
                items: ':not(.disabled)'
            });
        });
    </script>


@stop