@extends('layouts.back.master')@section('title','Production Order')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/inbox.css')}}"/>

    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }
    </style>
@stop
@section('content')
    <div class="app-wrapper">

        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">Burden & Cooking Request Planer</h5>
            </div>
            <div class="app-main-content">
                <div class="container">
                    <div class="media-list">
                        @foreach($semi as $semi_product_data)
                                <div class="card-body d-flex border-b-1 flex-wrap align-items-center ">
                                    <div class="d-flex mr-auto" id="dash1-widget-activities">
                                        <div id="dash1-easypiechart-1" class="dash1-easypiechart-1 chart easypiechart"
                                             data-percent="{{($semi_product_data['available_qty']/$semi_product_data['buffer_stock'])*100}}">

                                            <div class="avatar avatar-circle avatar-sm bg-red "
                                                 data-plugin="firstLitter"
                                                 data-target="#a-{{$semi_product_data['semi_id']}}"
                                                 style="margin: 15px;">
                                            </div>
                                        </div>
                                        @php

                                            $pdayt = $semi_product_data['pday'];

                                            if($pdayt <= 0){

                                                $pdayt = 1;
                                             }

                                        @endphp
                                        <h6 class="float-right mt-3 mx-3 " id="a-{{$semi_product_data['semi_id']}}" style="line-height: 1.5">{{$semi_product_data['name']}}<br>{{round((($semi_product_data['available_qty']+$semi_product_data['burning_total'])*100/$pdayt))}} % -{{($semi_product_data['available_qty']+$semi_product_data['burning_total'])}}-Units
                                        <br><small>Per day Usage -<b>{{round($semi_product_data['pday'],3)}}</b></small>
                                            <br><small>[According last 14 days]</small></h6>

                                    </div>
                                    <div class="d-flex justify-content-between">
                                        <div class="media mb-2" style="border-bottom: none">
                                            <div class="mr-3">
                                                <span class="circle circle-sm bg-primary">
                                                    <i class="fa fa-archive text-white" aria-hidden="true"></i>
                                                    </i>
                                                </span>
                                            </div>
                                            <div class="media-body"><h6 class="my-1 fz-sm">Current Stock</h6>
                                                <small>{{$semi_product_data['available_qty']}} units</small>
                                                <h6 class="my-1 text-danger fz-sm">Processing Qty</h6>
                                                <small>{{$semi_product_data['burning_total']}} units</small>
                                            </div>
                                        </div><!-- /.media -->
                                        <!--div class="media mb-2" style="border-bottom: none">
                                            <div class="mr-3"><span class="circle circle-sm bg-success">
                                                <i class="fa fa-briefcase text-white" aria-hidden="true"></i></span>
                                            </div>
                                            <div class="media-body"><h6 class="my-1 text-success fz-sm">Buffer
                                                    stock</h6>
                                                <small>{{$semi_product_data['buffer_stock']}} units</small>
                                            </div>
                                        </div--><!-- /.media -->
                                        <div class="media mb-2" style="border-bottom: none">
                                            @php
                                            $req_qty = ((($pdayt)*($semi_product_data['buffer_stock']))-($semi_product_data['available_qty']+$semi_product_data['burning_total']));
                                            $req_size = (($req_qty)/$semi_product_data['exqty']);
                                            @endphp

                                            <div class="mr-3"><span class="circle circle-sm bg-success">
                                                    <i class="fa fa-free-code-camp text-white" aria-hidden="true">
                                             </i>
                                                </span>
                                            </div>

                                            <div class="media-body"><h6 class="my-1 fz-sm">Buffer Days</h6>

                                                <small>{{$semi_product_data['buffer_stock']}} Days</small>
                                                <h6 class="my-1 text-danger fz-sm">Required Burden Size</h6>
                                                @if($req_size > 0)
                                                <h5><b>{{round(($req_qty)/$semi_product_data['exqty'])}}</b></h5>
                                                @else
                                                <small><b>0</b></small>
                                                @endif
                                            </div>
                                        </div><!-- /.media -->
                                        @if(Auth::user()->hasRole(['Owner','Production Manager','Super Admin']))
                                        <div class="media mb-2 mx-4">
                                            @can('production_order-create')
                                            <div class="justify-content-between">
                                                @if((round($req_size) > 0) || ($semi_product_data['pday'] <= 0 ))
                                                    @if($semi_product_data['is_bake'] == 1)
                                                <a href="../burden/1/create" class="create btn btn-danger btn-block"
                                                   data-toggle="modal"
                                                   data-target="#projects-task-modal"
                                                   value="{{$semi_product_data['semi_id']}}"><span
                                                            class="text-white">Create Burden</span></a>
                                                    @else
                                                        <a href="../burden/1/create" class="createC btn btn-danger btn-block"
                                                           data-toggle="modal"
                                                           data-target="#projects-task-modal1"
                                                           value="{{$semi_product_data['semi_id']}}"><span
                                                                    class="text-white">Cooking Request</span></a>


                                                    @endif
                                                @else
                                                    <a href="#" class="create btn btn-warning btn-block"
                                                       value="{{$semi_product_data['semi_id']}}"><span
                                                                class="text-white">Sufficient Stock</span></a>
                                                @endif

                                            </div>
                                            @endcan
                                        </div><!-- /.media -->
                                        @endif
                                    </div>
                                </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="projects-task-modal" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="card">
                        <div class="projects-list">
                            {{ Form::open(array('url' => 'burden','enctype'=>'multipart/form-data' ,'id'=>'bootstrap-wizard-form'))}}
                            <div class=" row">

                                <div class="col-sm-6">
                                    <div class="d-flex mr-auto">
                                        <div class="form-group">
                                            <label for="single">Semi-Finished Product</label>
                                            <lable class="aa btn btn-outline-secondary" style="width: 15rem"></lable>


                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="d-flex activity-counters justify-content-between">
                                        <div class="text-center px-2">
                                        <label for="single"> Burden Size</label>
                                            <select id="size" name="burden_size"
                                                    class="form-control select2" >



                                            </select>
                                            <!--label for="single"> Burden Size</label>
                                            <input class="form-control" id="size" name="burden_size"
                                                   type="text" style="width:100px"/-->
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="d-flex activity-counters justify-content-between">

                                        <div class="text-center px-2">

                                            <label for="single">Burden Count</label>
                                            <input class="form-control" id="brdncont" name="burden_count"
                                                   placeholder="" type="text" style="width:100px">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-success btn-lg btn-block">
                                Add
                            </button>

                        </div>
                        {{Form::close()}}
                    </div>

                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="projects-task-modal1" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="card">
                        <div class="projects-list">
                            {{ Form::open(array('url' => 'short-eats','enctype'=>'multipart/form-data' ,'id'=>'bootstrap-wizard-form'))}}
                            <div class=" row">

                                <div class="col-sm-6">
                                    <div class="d-flex mr-auto">
                                        <div class="form-group">
                                            <label for="single">Semi-Finished Product</label>
                                            <lable class="aa btn btn-outline-secondary" style="width: 15rem"></lable>


                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="d-flex activity-counters justify-content-between">
                                        <div class="text-center px-2">
                                            <label for="single"> Cooking Size</label>
                                            <select id="sizeC" name="burden_size"
                                                    class="form-control select2" >


                                                {{--@foreach($semiSizes as $semiSize)
                                                    <option value="{{$semiSize->id}}" >{{$semiSize->burden_size}}</option>
                                                @endforeach--}}
                                            </select>
                                            <!--label for="single"> Burden Size</label>
                                            <input class="form-control" id="size" name="burden_size"
                                                   type="text" style="width:100px"/-->
                                        </div>
                                    </div>
                                </div>
                                {{--                                <div class="col-sm-3">--}}
                                {{--                                    <div class="d-flex activity-counters justify-content-between">--}}

                                {{--                                        <div class="text-center px-2">--}}

                                {{--                                            <label for="single">Burden Count</label>--}}
                                {{--                                            <input class="form-control" id="brdncont" name="burden_count"--}}
                                {{--                                                   placeholder="" type="text" style="width:100px">--}}
                                {{--                                        </div>--}}
                                {{--                                    </div>--}}
                                {{--                                </div>--}}
                            </div>

                            <button type="submit" class="btn btn-success btn-lg btn-block">
                                Add
                            </button>

                        </div>

                    </div>
                    {!!Form::close()!!}
                </div>

            </div>
        </div>
    </div>


@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js')}}"></script>

    <script>
        $(document).ready(function () {

            $(".dash1-easypiechart-1").easyPieChart({
                barColor: $.colors.primary,
                scaleLength: !1,
                trackColor: "#f7f7f7",
                size: 80,
                color: "#f7f7f7"
            })
//            $("#content").hide();

//            $('#aa').click(function(){
//
//                    $("#unit").show();
//
//            });
        });


        $(".create").click(function () {

            bid = $(this).attr("value");
            //   alert(sid)

            let semi=0;
            $.ajax({
                type: "get",
                url: '../semi_finish_product/get/semipro',
                data: {id: bid},

                success: function (response) {
                    $(".aa").html('');
                    $(".aa").append(response['name']);
                    $(".aa").append(' <input type="hidden" id="semi_id" value="' + response['semi_finish_product_id'] + '" name="semi_finish_product">');
//                    $("#semi_id").value(response['semi_finish_product_id']);
                    semi= response['semi_finish_product_id'];
                    getSizes(semi)

                }
            });



        });

        $(".createC").click(function () {

            bid = $(this).attr("value");
            //   alert(sid)

            let semi=0;
            $.ajax({
                type: "get",
                url: '../semi_finish_product/get/semipro',
                data: {id: bid},

                success: function (response) {
                    $(".aa").html('');
                    $(".aa").append(response['name']);
                    $(".aa").append(' <input type="hidden" id="semi_id" value="' + response['semi_finish_product_id'] + '" name="semi_finish_product">');
//                    $("#semi_id").value(response['semi_finish_product_id']);
                    semi= response['semi_finish_product_id'];
                    getSizesC(semi)
                }
            });



        });

       /* function getSizes() {
            var semi = $("#single option:selected").val();
            $.ajax({
                url: "/get/semi/burden/size/" + semi,
                type: "GET",
                success: function (data) {
                    var $dropdown = $("#size");
                    $dropdown.empty();
                    $.each(data, function(i,k) {
                        $dropdown.append($("<option />").val(i).text(k));
                    });
                },
                error: function (xhr, status) {

                }
            });
        }*/

        function getSizes(semi) {
            $.ajax({
                url: "/get/semi/burden/size/" + semi,
                type: "GET",
                success: function (data) {
                    var $dropdown = $("#size");
                    $dropdown.empty();
                    $.each(data, function(i,k) {
                        $dropdown.append($("<option />").val(i).text(k));
                    });
                },
                error: function (xhr, status) {

                }
            });
        }

        function getSizesC(semi) {
            $.ajax({
                url: "/get/semi/burden/size/" + semi,
                type: "GET",
                success: function (data) {
                    var $dropdown = $("#sizeC");
                    $dropdown.empty();
                    $.each(data, function(i,k) {
                        $dropdown.append($("<option />").val(i).text(k));
                    });
                },
                error: function (xhr, status) {

                }
            });
        }


        // $('#projects-task-modal').on('show.bs.modal', function (event) {
        //
        // });
    </script>
@stop