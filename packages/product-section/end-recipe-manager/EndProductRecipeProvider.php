<?php

namespace EndProductRecipeManager;

use Illuminate\Support\ServiceProvider;

class EndProductRecipeProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/Views', 'EndProductRecipeManager');
        require __DIR__ . '/web.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('EndProductRecipeManager', function($app){
            return new EndProductRecipeManager;
        });
    }
}
