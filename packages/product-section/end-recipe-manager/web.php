<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web'])->group(function () {
    Route::prefix('end-recipe')->group(function () {
        Route::get('', 'EndProductRecipeManager\Controllers\EndProductRecipeController@index')->name('endrecipe.index');

        Route::get('create', 'EndProductRecipeManager\Controllers\EndProductRecipeController@create')->name('endrecipe.create');

        Route::get('{recipe}', 'EndProductRecipeManager\Controllers\EndProductRecipeController@show')->name('en-recipe.show');

        Route::get('{recipe}/edit', 'EndProductRecipeManager\Controllers\EndProductRecipeController@edit')->name('endrecipe.edit');

        Route::post('', 'EndProductRecipeManager\Controllers\EndProductRecipeController@store')->name('endrecipe.store');


        Route::put('{recipe}', 'EndProductRecipeManager\Controllers\EndProductRecipeController@update')->name('endrecipe.update');

        Route::post('active', 'EndProductRecipeManager\Controllers\EndProductRecipeController@active')->name('endrecipe.active');

        Route::delete('{recipe}', 'EndProductRecipeManager\Controllers\EndProductRecipeController@destroy')->name('endrecipe.destroy');
    });
});
