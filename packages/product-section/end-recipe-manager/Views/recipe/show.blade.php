@extends('layouts.back.master')@section('title','Recipe Detilas')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/lightbox2/dist/css/lightbox.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}"/>

    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">

    <style>


        #container {
            position: relative;
            padding: 0px;
            width: 100%;
            height: 330px;
            overflow: auto;
        }

        #container .content {
            width: 100%;
            height: 20%;
        }

        .profile-pic {
            position: relative;
            /*display: inline-block;*/
        }

        .profile-pic:hover .edit {
            display: block;
        }

        .edit {
            padding-top: 7px;
            padding-right: 7px;
            position: absolute;
            right: 0;
            top: 0;
            display: none;
        }

        .edit a {
            color: #000;
        }
    </style>

@stop
@section('current','Recipe')
@section('current_url',route('recipe.create'))
@section('current2',$recipe->name)
@section('current_url2','')
@section('content')
    <div class="profile-wrapper">
        <div class="profile-section-user">
            <div class="profile-cover-img profile-pic">
                <img src="{{asset('assets\img\recipe.jpg')}}" alt="">
            </div>

            <div class="profile-info-brief p-3">
                @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                <div class="user-profile-avatar avatar avatar-circle avatar-md project-iconr bg-{{$color[$recipe->colour]}}"
                     data-plugin="firstLitter"
                     data-target="#{{$recipe->id*754}}"></div>

                <div class="text-center">
                    <h5 class="text-uppercase mb-1" id="{{$recipe->id*754}}">{{$recipe->name}}
                        <a href="#" class="btn btn-light btn-sm">
                            <i class="zmdi zmdi-edit"></i>
                        </a>
                    </h5>
                    @php
                        $act= RecipeManager\Models\RecipeSemiProduct::where('recipe_id','=',$recipe->id)->first();
                        $semi=SemiFinishProductManager\Models\SemiFinishProduct::where('semi_finish_product_id','=',$act->semi_finish_product_semi_finish_product_id)->first();
                    @endphp

                    <h6 class="text mb-lg-1">{{$semi->name}}</h6>
                    <h6 class="text mb-lg-1">Units per Recipe : {{$act->units}} units</h6>
                @if($act->status==2)
                        <i class="status status-online"></i><span
                                class="text-success">Active Recipe</span>
                    @else
                        <i class="status status-busy"></i><span
                                class="text-danger">Innactive Recipe</span>
                    @endif
                    <br>
                </div>

                <hr class="m-0">

                <div class="text-center">
                    {{$recipe->desc}}
                    <div class="section-1">
                        <a href="#" class="btn btn-light btn-sm">
                            <i class="zmdi zmdi-edit"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="profile-section-main">
            <ul class="nav nav-tabs profile-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#profile-overview" role="tab">Recipe Content</a>
                </li>
            </ul>

            <div class="tab-content profile-tabs-content">
                <div class="tab-pane active" id="profile-overview" role="tabpanel">
                    <div class="stream-posts">
                        <div class="stream-post">
                            <div class="sp-author"> Ingredients</div>
                            <div class="sp-content">
                                <div class="sp-paragraph mb-0">
                                    <div class="app-main">
                                        <div class="app-main-content">
                                            <div class="media-list" id="container">
                                                @foreach($materials as $material)
                                                    <a href="#">
                                                        <div class="media content my-2">
                                                            @if(isset($material->image))
                                                                <div class="avatar avatar avatar-sm">
                                                                    <img src="{{asset($material->image_path.$material->image)}}"
                                                                         alt="">
                                                                </div>
                                                            @else
                                                                @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                                                <div
                                                                        class="avatar avatar-circle avatar-md project-icon bg-{{$color[$material['materials']->colour]}} "
                                                                        data-plugin="firstLitter"
                                                                        data-target="#raw-{{$material['materials']->raw_material_id*874}}">
                                                                </div>
                                                            @endif


                                                            <div class="media-body">
                                                                <h6 class="media-heading my-1">
                                                                    <h6 id="raw-{{$material['materials']->raw_material_id*874}}">{{$material['materials']->name}}</h6>
                                                                </h6>
                                                                @if($material['materials']->measurement=='gram')
                                                                    @if($material['qty'] >= 1000)
                                                                        <h6>{{$material['qty']/1000}} kg</h6>
                                                                    @else
                                                                        <h6>{{$material['qty']}} g</h6>
                                                                    @endif

                                                                @elseif($material['materials']->measurement=='milliliter')
                                                                    @if($material['qty'] >= 1000)
                                                                        <h6>{{$material['qty']/1000}} l</h6>
                                                                    @else
                                                                        <h6>{{$material['qty']}} ml</h6>
                                                                    @endif
                                                                @elseif($material['materials']->measurement=='unit')
                                                                    <h6>{{$material['qty']}} units</h6>
                                                                @endif

                                                                {{--<small>{{$material->qty}} g</small>--}}
                                                            </div>
                                                            <div class="section-2">
                                                                <button class="remove btn btn-light btn-sm ml-2"
                                                                        value="#"
                                                                        data-toggle="tooltip"
                                                                        data-placement="left"
                                                                        title="Remove Frome Recipe">
                                                                    <i class="zmdi zmdi-close"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <hr class="m-0">
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

        </div>
    </div>
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/waypoints/lib/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/waypoints/lib/shortcuts/sticky.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/counterup/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/js/jquery.sparkline.min.js')}}"></script>
    <script src="{{asset('js/site.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>


<script>
    var ps = new PerfectScrollbar('#container');
</script>
@stop