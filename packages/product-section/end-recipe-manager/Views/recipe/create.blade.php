@extends('layouts.back.master')@section('title','Recipes')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.wizard.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/modal.css')}}"/>
{{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>--}}
    /*<link rel="stylesheet"*/
{{--          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>--}}
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/demos.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/css/site.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/packages/recipe/recipe.css')}}">
@stop
@section('current','Recipe')
@section('current_url',route('recipe.create'))

@section('content')
    <div class="app-wrapper">
        <div class="app-panel" id="app-panel">
            <div class="app-search">
                <input type="search" class="search-field" placeholder="Search" disabled>
                <i class="search-icon fa fa-search"></i>
            </div>

            <div class="scroll-container">
                <div class="app-panel-inner">

                    <div class="p-3">
                        <button class="btn btn-success py-3 btn-block btn-lg" data-toggle="modal"
                                data-target="#projects-task-modal">NEW RECIPE
                        </button>
                    </div>
                    <hr class="m-0">
                    <div class="people-list d-flex justify-content-start flex-wrap p-3">
                        <button class="avatar-sm avatar-circle fz-base font-weight-bold add-people-btn"
                                data-toggle="modal" data-target="#semiFinish">
                            <i data-toggle="tooltip"
                               data-placement="right" title="New Semi Finish Product" class="zmdi zmdi-plus"></i>
                        </button>
                    </div>
                    <hr class="m-0">
                    <div class="media-list" id="container">
                        @foreach($endProductList as $product)
                            <a href="..\end_product\{{$product->id}}">
                                <div class="media content">
                                    @if(isset($product->image))
                                        <div href="{{$product->id}}"
                                             class="avatar avatar avatar-sm">
                                            <img src="{{asset($product->image_path.$product->image)}}" alt="">
                                        </div>
                                    @else
                                        @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                        <div href="{{$product->id}}"
                                             class="avatar avatar-circle avatar-sm bg-{{$color[$product->colour]}} "
                                             data-plugin="firstLitter"
                                             data-target="#raw-{{$product->id*874}}">
                                        </div>
                                    @endif
                                    <div class="media-body">
                                        <h6 class="media-heading my-1">
                                            <h6 href="{{$product->id}}" id="raw-{{$product->id*874}}">{{$product->name.' '.$product->specification}}</h6>
                                        </h6>
                                        <small>{{$product->desc}}</small>
                                    </div>
                                </div>
                            </a>
                            <hr class="m-0">
                        @endforeach


                    </div>
                </div>
            </div>
            <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show"><i
                        class="fa fa-chevron-right"></i>
                <i class="fa fa-chevron-left"></i>
            </a>
        </div>
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">RECIPES</h5>
            </div>

            <div class="scroll-container">
                <div class="app-main-content">
                    <div class="media-list" id="rcontainer">
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="projects-task-modal" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12">
                        <div class="card">

                            {{ Form::open(array('url' => 'end-recipe','id'=>'end-recipe-form')) }}
                            <div class="wizard p-4" id="bootstrap-wizard-1">
                                <ul class="nav nav-tabs vertices keep-prefix-suffix" role="tablist">
                                    <li class="nav-item">
                                        <a class=" nav-link active" data-toggle="tab" href="#ex5-step-1"
                                           role="tab">
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#ex5-step-2"
                                           role="tab">
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#ex5-step-3"
                                           role="tab">
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#ex5-step-4"
                                           role="tab">
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    {{--<div class="tab-pane active" id="ex5-step-1" role="tabpanel">--}}
                                    <div class="tab-pane active" id="ex5-step-1" role="tabpanel">
                                        <div class="task-name-wrap">
                                            {!! Form::select('end_product', $endProducts , null , ['placeholder'=>'Select End Product','class' => 'form-control select2','id'=>"single2"]) !!}
                                        </div>
                                        <hr>

                                        <div class="task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="recipe_name"
                                                   placeholder="Recipe Name">
                                        </div>
                                        <hr>

                                        <div class="task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="desc"
                                                   placeholder="Description">
                                        </div>
                                        <hr>

                                        <div class="task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="days_expire"
                                                   placeholder="Days For Expiry">
                                        </div>
                                        <hr>

                                        <div class="task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="ohead"
                                                   placeholder="Over Head %">
                                        </div>
                                        <hr>

                                        <div class="task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="dist_cost"
                                                   placeholder="Cost Of Distribution %">
                                        </div>
                                        <hr>

                                        <div class="task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="bweight"
                                                   placeholder="Total Burden Weight">
                                        </div>
                                        <hr>

                                        <div class="task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="bitem"
                                                   placeholder="Number Of Items For Burden">
                                        </div>
                                        <hr>

                                        <div class="task-name-wrap">

                                            <select name="first_stage" class="form-control select2">
                                                <option value=''>Select First Stage</option>
                                                <option value="NoFirst">No First Stage</option>
                                                <option value="Remove">Remove</option>
                                                <option value="Unload">Unload</option>
                                                <option value="Grind">Grind</option>
                                                <option value="Make">Make</option>
                                                <option value="Mill">Mill</option>
                                                <option value="Prepair">Prepair</option>
                                                <option value="Pack">Pack</option>
                                                <option value="Burn">Burn</option>
                                                <option value="Cook">Cook</option>
                                                <option value="Bake">Bake</option>
                                                <option value="Fry">Fry</option>
                                                <option value="DeepFry">Deep Fry</option>
                                                <option value="Boil">Boil</option>
                                                <option value="Wrap">Wrap</option>
                                                <option value="Cut">Cut</option>
                                                <option value="Cream">Cream</option>
                                                <option value="Paste">Paste</option>
                                                <option value="Seal">Seal</option>
                                                <option value="Sort">Sort</option>



                                        </select>
                                        </div>
                                        <hr>

                                        <div class="task-name-wrap">

                                            <select name="second_stage" class="form-control select2">
                                                <option value=''>Select Second Stage</option>
                                                <option value="NoSecond">No Second Stage</option>
                                                <option value="Remove">Remove</option>
                                                <option value="Unload">Unload</option>
                                                <option value="Grind">Grind</option>
                                                <option value="Make">Make</option>
                                                <option value="Mill">Mill</option>
                                                <option value="Prepair">Prepair</option>
                                                <option value="Pack">Pack</option>
                                                <option value="Burn">Burn</option>
                                                <option value="Cook">Cook</option>
                                                <option value="Bake">Bake</option>
                                                <option value="Fry">Fry</option>
                                                <option value="DeepFry">Deep Fry</option>
                                                <option value="Boil">Boil</option>
                                                <option value="Wrap">Wrap</option>
                                                <option value="Cut">Cut</option>
                                                <option value="Cream">Cream</option>
                                                <option value="Paste">Paste</option>
                                                <option value="Seal">Seal</option>
                                                <option value="Sort">Sort</option>



                                            </select>
                                        </div>
                                        <hr>

                                        <div class="task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" value="1" disabled
                                                   name="units_per_recipe"
                                                   placeholder="Units per Recipe">
                                        </div>
                                        <hr>

                                    </div>
                                    <div class="tab-pane" id="ex5-step-2" role="tabpanel">
                                        <div class="row">
                                            <form name="ingredient" id="ingredient">
                                                <div class="col-sm-6 mt-1">
                                                    <div class="form-group">
                                                        <label for="single">Raw Materials</label>
                                                        <select id="single" name="kk"
                                                                style="width:100%;"
                                                                onChange="selectIngrediants(this);"
                                                                class="form-control select2" ui-jp="select2"
                                                                ui-options="{theme: 'bootstrap'}">
                                                            <option value=''>Select Raw Materials</option>
                                                            @foreach($products as $product)
                                                                <option value="{{$product->raw_material_id}}">{{$product->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-sm-3 mt-1 ">
                                                    <label for="single">Quantity</label>
                                                    <input type="text" placeholder="" class="abc info form-control w-20"
                                                           id="qty" name="qty"
                                                           style="width: 100%;height: calc(2.25rem + 2px);"/>
                                                </div>

                                                <div class="col-sm-3 py-4 px-4 mt-2">
                                                    <div class="form-group">
                                                        <div class="floating-button" id="floating-button-raw">
                                                            <p class="plus">+</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>


                                        </div>
                                        {{--<div class="col-lg-12 col-md-6">--}}
                                        {{--<div class="card p-1 bg-faded">--}}
                                        {{--<div  id="selecting">--}}
                                        {{--<!-- /.media-body -->--}}
                                        {{--</div><!-- /.media -->--}}
                                        {{--</div><!-- /.card -->--}}
                                        {{--</div>--}}
                                        <div class="scroll-container">
                                            <div class="col-lg-12 col-md-6">
                                                <div class="projects-list" id="selecting-raw">

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="ex5-step-3" role="tabpanel">
                                        <div class="row">

                                            <div class="col-sm-6 mt-1">
                                                <div class="form-group">
                                                    <label for="single">Semi Finish Products</label>
                                                    <select id="semi" name="se"
                                                            style="width:100%;"
                                                            class="form-control select2" ui-jp="select2">
                                                        <option value=''>Select Semi Finish Products</option>
                                                        @foreach($sps as $product)
                                                            <option value="{{$product->semi_finish_product_id}}">{{$product->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-sm-3 mt-1 ">
                                                <label for="single">Quantity</label>
                                                <input type="text" placeholder="" class="abc info form-control w-20"
                                                       id="semi-qty" name="semi-qty"
                                                       style="width: 100%;height: calc(2.25rem + 2px);"/>
                                            </div>

                                            <div class="col-sm-3 py-4 px-4 mt-2">
                                                <div class="form-group">
                                                    <div class="floating-button" id="floating-button-semi">
                                                        <p class="plus">+</p>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                        {{--<div class="col-lg-12 col-md-6">--}}
                                        {{--<div class="card p-1 bg-faded">--}}
                                        {{--<div  id="selecting">--}}
                                        {{--<!-- /.media-body -->--}}
                                        {{--</div><!-- /.media -->--}}
                                        {{--</div><!-- /.card -->--}}
                                        {{--</div>--}}
                                        <div class="scroll-container">
                                            <div class="col-lg-12 col-md-6">
                                                <div class="projects-list" id="selecting-semi">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="ex5-step-4" role="tabpanel">
                                        <div class="row">
                                            <div class="col-sm-6 mt-1">
                                                <div class="form-group">
                                                    <label for="single">Packing Items</label>
                                                    <select id="packing" name="kk"
                                                            style="width:100%;"
                                                            class="form-control select2" ui-jp="select2"
                                                            ui-options="{theme: 'bootstrap'}">
                                                        <option value=''>Select Packing Item</option>
                                                        @foreach($packingItems as $packingItem)
                                                            <option value="{{$packingItem->raw_material_id}}">{{$packingItem->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-sm-3 mt-1 ">
                                                <label for="single">Quantity</label>
                                                <input type="text" placeholder="" class="abc info form-control w-20"
                                                       id="packing-qty" name="packing-qty"
                                                       style="width: 100%;height: calc(2.25rem + 2px);"/>
                                            </div>

                                            <div class="col-sm-3 py-4 px-4 mt-2">
                                                <div class="form-group">
                                                    <div class="floating-button" id="floating-button-packing">
                                                        <p class="plus">+</p>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        {{--<div class="col-lg-12 col-md-6">--}}
                                        {{--<div class="card p-1 bg-faded">--}}
                                        {{--<div  id="selecting">--}}
                                        {{--<!-- /.media-body -->--}}
                                        {{--</div><!-- /.media -->--}}
                                        {{--</div><!-- /.card -->--}}
                                        {{--</div>--}}
                                        <div class="scroll-container">
                                            <div class="col-lg-12 col-md-6">
                                                <div class="projects-list" id="selecting-packing">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="pager d-flex justify-content-center">
                                    <input type="submit" id="finish-btn" class="finish btn btn-success w-50"
                                           value="Finish"/>

                                    <button type="button" id="next-btn" class="next btn btn-success w-50">Next
                                    </button>
                                </div>
                            </div>
                            {{ Form::close() }}

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js')
{{--    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--}}
    <script src="{{asset('assets/examples/js/apps/projects.js')}}"></script>
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>--}}
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>--}}
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('assets/packages/semi_finish/semi_finish.js')}}"></script>
    <script src="{{asset('assets/packages/recipe/end-recipe.js')}}"></script>
@stop