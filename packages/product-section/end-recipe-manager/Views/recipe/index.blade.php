@extends('layouts.back.master')@section('title','Recipes')
@section('css')
    <style type="text/css">
        #floating-button{
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #00C851;
            position: fixed;
            bottom: 80px;
            right: 30px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index:2
        }

        .plus{
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }
    </style>
@stop
@section('content')
    <div id="floating-button" data-toggle="tooltip" data-placement="left" data-original-title="NEW RECIPE" onclick="location.href = '{{route('end-recipe.create')}}';">
        <p class="plus">+</p>
    </div>
    <div class="padding">
        <div class="row">

        </div>
    </div>
@stop
@section('js')
@stop