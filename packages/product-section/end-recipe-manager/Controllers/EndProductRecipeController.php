<?php

namespace EndProductRecipeManager\Controllers;

use BurdenManager\Models\Burden;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use EndProductManager\Models\EndProduct;
use EndProductRecipeManager\Models\EndProductRecipe;
use EndProductRecipeManager\Models\EndProductRecipePacking;
use EndProductRecipeManager\Models\EndProductRecipeRawMaterial;
use EndProductRecipeManager\Models\EndProductRecipeSemiProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use PackingItemManager\Models\PackingItem;
use RawMaterialManager\Models\RawMaterial;
use SemiFinishProductManager\Models\SemiFinishProduct;

class EndProductRecipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:end_product_recipe-index', ['only' => ['index']]);
        $this->middleware('permission:EndProductPrice-list', ['only' => ['list']]);
        $this->middleware('permission:end_product_recipe-create', ['only' => ['create']]);

    }
    public function index()
    {
        return redirect(route('endrecipe.create'));
        return view('EndProductRecipeManager::recipe.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $endProductList = EndProduct::all();
        $endProducts = EndProduct::all()->pluck('name', 'id');
        $products = RawMaterial::withoutGlobalScope('type')->whereType(1)->orWhere('type', '=', 3)->get();
        $semi = SemiFinishProduct::all();
        $packingItems = RawMaterial::withoutGlobalScope('type')->whereType(6)->get();
//        $recipes = EndProductRecipe::all();
//        $lastSemi = SemiFinishProduct::orderBy('semi_finish_product_id', 'desc')->take(4)->get();
        return view('EndProductRecipeManager::recipe.create', compact('endProducts'))->with([
            'products' => $products,
            'sps' => $semi,
//            'lastSemis' => $lastSemi,
            'packingItems' => $packingItems,
            'endProducts' => $endProducts,
            'endProductList' => $endProductList,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $xx="Testing5";
       // $name = $request->recipe_name;
       // $desc = $request->desc;
        $end_product = $request->end_product;
       // $days_expire = $request->days_expire;
      // $over_head = $request->ohead;
      //  $distribute_cost = $request->dist_cost;
      //  $burden_weight = $request->bweight;
       // $burden_items = $request->bitem;
      //  $stage_1 = $request->first_stage;
      //  $stage_2 = $request->second_stage;


        $units_per_recipe = 1;

        $raw = $request->raw;
        $raw_qty = $request->raw_qty;

        $semi = $request->semi;
        $semi_qty = $request->semi_qty;

        $packing = $request->packing;
        $packing_qty = $request->packing_qty;

        try {
//            DB::transaction(function () use ($end_product, $name, $desc, $units_per_recipe, $raw, $raw_qty, $semi, $semi_qty, $packing, $packing_qty) {
                $burden = null;
                $endRes = EndProductRecipe::whereEndProductId($end_product)->get();
                $burden_list = 0;

                if (!empty($endRes) && $endRes->count() > 0) {
                    foreach ($endRes as $res){
                        $burden = Burden::where('recipe_id', '=', $res->id)->where('status', '=', 1)->first();
                        if (!empty($burden)) {
                            $burden_list =1;
                        }
                    }
                }


                if ($burden_list==0) {
                    if (!empty($endRes)) {
                        foreach ($endRes as $endd){
                            $endd->status = 0;
                            $endd->save();
                        }
                    }

                    $recipe = new EndProductRecipe();

                    $recipe->end_product_id = $end_product;
                    $recipe->name = $request->recipe_name;
                    $recipe->desc = $request->desc;
                    $recipe->qty = $units_per_recipe;
                    $recipe->days_expire = $request->days_expire;
                    $recipe->over_head = $request->ohead;
                    $recipe->distribute_cost = $request->dist_cost;
                    $recipe->burden_weight = $request->bweight;
                    $recipe->burden_items = $request->bitem;
                    $recipe->stage_1 = $request->first_stage;
                    $recipe->stage_2 = $request->second_stage;

                    $recipe->created_by = Auth::user()->id;


                    $recipe->save();

                    if (!empty($raw)) {
                        foreach ($raw as $key => $ingredient) {
                            $rcon = new EndProductRecipeRawMaterial();
                            $rcon->recipe_id = $recipe->id;
                            $rcon->raw_material_id = $ingredient;
                            $rcon->units = $raw_qty[$key];
                            $rcon->save();
                        }
                    }

                    if (!empty($semi)) {
                        foreach ($semi as $key => $ingredient) {
                            $rcon = new EndProductRecipeSemiProduct();
                            $rcon->recipe_id = $recipe->id;
                            $rcon->semi_finish_product_id = $ingredient;
                            $rcon->units = $semi_qty[$key];
                            $rcon->save();
                        }
                    }

                    if (!empty($packing)) {
                        foreach ($packing as $key => $ingredient) {
                            $rcon = new EndProductRecipePacking();
                            $rcon->recipe_id = $recipe->id;
                            $rcon->packing_id = $ingredient;
                            $rcon->units = $packing_qty[$key];
                            $rcon->save();
                        }
                    }
                } else {
                    return redirect(route('endrecipe.create'))->with([
                        'error' => true,
                        'error.title' => 'Sorry !',
                        'error.message' => 'Pending Burden on list'
                    ]);
                }
//            });

            return redirect(route('endrecipe.create'))->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'New recipe has been added'
            ]);

        } catch (Exception $e) {
            return redirect(route('endrecipe.create'))->with([
                'error' => true,
                'error.title' => 'Sorry !',
                'error.message' => 'Something Went Wrong '
            ]);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param \RecipeManager\Models\EndProductRecipe $recipe
     * @return \Illuminate\Http\Response
     */
    public function show(EndProductRecipe $recipe)
    {
        $id = $recipe->id;
        $recipe = EndProductRecipe::with('recipeContents.materials', 'semiProducts')->where('id', '=', $id)->first();
        $material = EndProductRecipeContent::where('recipe_id', '=', $recipe->id)->with('materials')->get();
        return view('EndProductRecipeManager::recipe.show')->with([
            'recipe' => $recipe,
            'materials' => $material,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \RecipeManager\Models\EndProductRecipe $recipe
     * @return \Illuminate\Http\Response
     */
    public function edit(EndProductRecipe $recipe)
    {
        return view('EndProductRecipeManager::recipe.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \RecipeManager\Models\EndProductRecipe $recipe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EndProductRecipe $recipe)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \RecipeManager\Models\EndProductRecipe $recipe
     * @return \Illuminate\Http\Response
     */
    public function destroy(EndProductRecipe $recipe)
    {
        $recipe->delete();
        if ($recipe->trashed()) {
            return true;
        } else {
            return false;
        }
    }

    public function active(Request $request)
    {
        $rop = EndProductRecipeSemiProduct::find($request->id);
        $other_active = EndProductRecipeSemiProduct::where('semi_finish_product_semi_finish_product_id', '=', $rop->semi_finish_product_semi_finish_product_id)
            ->where('status', '=', 2)
            ->first();
        if ($other_active) {
            $other_active->status = 1;
            $other_active->save();
        }
        $rop->status = 2;
        $rop->save();
        return 'true';
    }
}
