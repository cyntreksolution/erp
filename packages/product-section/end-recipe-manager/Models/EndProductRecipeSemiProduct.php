<?php

namespace EndProductRecipeManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RawMaterialManager\Models\RawMaterial;
use SemiFinishProductManager\Models\SemiFinishProduct;

class EndProductRecipeSemiProduct extends Model
{
    use SoftDeletes;

    protected $table = 'end_product_recipe_semi';

    protected $dates = ['deleted_at'];


    public function recipe(){
        return $this->belongsTo(EndProductRecipe::class);
    }
    public function semiFinishProduct(){
        return $this->belongsTo(SemiFinishProduct::class,'semi_finish_product_id');
    }

}
