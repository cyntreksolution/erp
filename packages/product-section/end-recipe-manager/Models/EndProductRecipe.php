<?php

namespace EndProductRecipeManager\Models;

use EndProductManager\Models\EndProduct;
use Illuminate\Database\Eloquent\Model;
use EndProductRecipeManager\Models\EndProductRecipeRawMaterial;
use EndProductRecipeManager\Models\EndProductRecipeSemiProduct;
use Illuminate\Database\Eloquent\SoftDeletes;

class EndProductRecipe extends Model
{
    use SoftDeletes;

    const STAGES = [
        'Millings',
        'Making',
        'Preparing',
        'Packing',
        'Burning',
        'Cooking',
        'Baking',
        'Deep Frying',
        'Frying',
        'Boiling',
        'Warping',
        'Cutting'
    ];

    protected $table = 'end_product_recipe';

    protected $dates = ['deleted_at'];

    public function endProduct()
    {
        return $this->belongsTo(EndProduct::class);
    }

    public function rawContent()
    {
        return $this->hasMany(EndProductRecipeRawMaterial::class, 'recipe_id');
    }

    public function semiContent()
    {
        return $this->hasMany(EndProductRecipeSemiProduct::class, 'recipe_id');
    }


    public function packingContent()
    {
        return $this->hasMany(EndProductRecipePacking::class, 'recipe_id');
    }

}
