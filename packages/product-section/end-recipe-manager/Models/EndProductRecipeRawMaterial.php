<?php

namespace EndProductRecipeManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RawMaterialManager\Models\RawMaterial;

class EndProductRecipeRawMaterial extends Model
{
    use SoftDeletes;

    protected $table = 'end_product_recipe_raw';

    protected $dates = ['deleted_at'];

    public function recipe(){
        return $this->belongsTo(EndProductRecipe::class);
    }
    public function rawMaterial(){
        return $this->belongsTo(RawMaterial::class,'raw_material_id')->withoutGlobalScope("type");
    }

}
