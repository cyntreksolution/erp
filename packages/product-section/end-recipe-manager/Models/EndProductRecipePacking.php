<?php

namespace EndProductRecipeManager\Models;

use EndProductRecipeManager\Models\EndProductRecipe;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use PackingItemManager\Models\PackingItem;
use RawMaterialManager\Models\RawMaterial;

class EndProductRecipePacking extends Model
{
    use SoftDeletes;

    protected $table = 'end_product_recipe_packing';

    protected $dates = ['deleted_at'];

    public function recipe(){
        return $this->belongsTo(EndProductRecipe::class);
    }
    public function packingItem(){
        return $this->belongsTo(RawMaterial::class,'packing_id','raw_material_id')->withoutGlobalScope("type");
    }

}
