<?php

namespace PackingManager;

use Illuminate\Support\ServiceProvider;

class PackingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/Views', 'PackingManager');
        require __DIR__ . '/web.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('PackingManager', function($app){
            return new PackingManager;
        });
    }
}
