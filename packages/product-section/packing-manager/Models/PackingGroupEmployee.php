<?php

namespace PackingManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use SemiFinishProductManager\Models\SemiFinishProduct;

class PackingGroupEmployee  extends Model
{
    use SoftDeletes;

    protected $table = 'packing_group_employees';

    protected $dates = ['deleted_at'];

    public function employee(){
        return $this->belongsTo('EmployeeManager\Models\Employee');
    }
}
