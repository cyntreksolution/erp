<?php

namespace PackingManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use SemiFinishProductManager\Models\SemiFinishProduct;

class PackingEmployee  extends Model
{
    use SoftDeletes;

    protected $table = 'packing_employee';

    protected $dates = ['deleted_at'];

}
