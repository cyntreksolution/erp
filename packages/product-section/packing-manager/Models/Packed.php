<?php

namespace PackingManager\Models;

use EmployeeManager\Models\Group;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use SemiFinishProductManager\Models\SemiFinishProduct;

class Packed  extends Model
{
    use SoftDeletes;

    protected $table = 'packing';

    protected $dates = ['deleted_at'];

    public function employeeGroup()
    {
        return $this->belongsToMany(Group::class, 'packing_group_employees','packing_id');
    }

    public function endProdcuts(){
        return $this->belongsTo('EndProductManager\Models\EndProduct','end_product_id','id');
    }

}
