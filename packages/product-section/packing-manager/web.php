<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web','log'])->group(function () {
    Route::prefix('product/packing')->group(function () {
        Route::get('', 'PackingManager\Controllers\PackingController@index')->name('ppacking.index');

        Route::get('create', 'PackingManager\Controllers\PackingController@create')->name('ppacking.create');

        Route::get('save/qty', 'PackingManager\Controllers\PackingController@saveqty')->name('ppacking.index');

        Route::get('{packing}', 'PackingManager\Controllers\PackingController@show')->name('ppacking.show');

        Route::get('{packing}/edit', 'PackingManager\Controllers\PackingController@edit')->name('ppacking.edit');


        Route::post('', 'PackingManager\Controllers\PackingController@store')->name('ppacking.store');

        Route::post('{packing}', 'PackingManager\Controllers\PackingController@update')->name('ppacking.update');

        Route::delete('{packing}', 'PackingManager\Controllers\PackingController@destroy')->name('ppacking.destroy');
    });
});