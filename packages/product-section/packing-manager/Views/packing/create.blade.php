@extends('layouts.back.master')@section('title','Create Burden')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.wizard.css')}}">

    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        #floating-button {
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #db4437;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .plus {
            color: white;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }
    </style>
@stop
@section('content')
    <div class="app-wrapper">

        <div class="app-main">
            <div class="app-main-header">
                <div class="row">
                    <div class="col-md-6 mb-3 ">
                        <button class="btn btn-success py-3 " data-toggle="modal"
                                data-target="#projects-task-modal">MAKE END PRODUCT
                        </button>
                    </div>
                    <div class="col-md-6">
                        <h5 class="app-main-heading  mt-2">LATEST PACKING</h5>
                    </div>
                </div>
            </div>
            <div class="app-main-content">
                <div class="projects-list">
                    @foreach($packing as $pack)
                        <a class="burning" value="{{$pack->id}}">
                            <div class="media row">
                                <div class="col-md-9 media-body">
                                    <h6 class="project-name" id="project-1">{{$pack['endProdcuts']->name}}</h6>
                                    <div>
                                        <small class="project-detail">Packing Quantity : {{$pack->expected_packing_qty}}
                                            units</small>
                                    </div>
                                    <div>
                                        <small class="project-detail">Available Stock
                                            : {{$pack['endProdcuts']->available_qty}} units
                                        </small>
                                    </div>
                                    @if (!empty($pack['employeeGroup'][0]))
                                        <div>
                                            <small class="project-detail">Employee Group
                                                : {{$pack['employeeGroup'][0]->name.' '.$pack['employeeGroup'][0]->description}}
                                            </small>
                                        </div>
                                        <div>
                                            <small class="project-detail">Employees
                                                : @foreach($pack['employeeGroup'][0]->employees->pluck('first_name') as $name) {{$name}}, @endforeach
                                            </small>
                                        </div>
                                    @endif

                                </div>
                                <button class="col-md-2 btn btn-outline-warning" style="width:6rem">
                                    <i class="fa fa-fire" aria-hidden="true"></i>
                                    packing
                                </button>
                            </div>
                            <hr class="m-0">
                        </a>
                    @endforeach
                    @foreach($packed as $pack)
                        <a>
                            <div class="media row">
                                <div class="col-md-9 media-body">
                                    <h6 class="project-name" id="project-1">{{$pack['endProdcuts']->name}}</h6>
                                    <div>
                                        <small class="project-detail">Packing Quantity : {{$pack->expected_packing_qty}}
                                            units</small>
                                    </div>
                                    <div>
                                        <small class="project-detail">Available Stock
                                            : {{$pack['endProdcuts']->available_qty}} units
                                        </small>
                                    </div>
                                    @if (!empty($pack['employeeGroup'][0]))
                                        <div>
                                            <small class="project-detail">Employee Group
                                                : {{$pack['employeeGroup'][0]->name.' '.$pack['employeeGroup'][0]->description}}
                                            </small>
                                        </div>
                                        <div>
                                            <small class="project-detail">Employees
                                                : @foreach($pack['employeeGroup'][0]->employees->pluck('first_name') as $name) {{$name}}, @endforeach
                                            </small>
                                        </div>
                                    @endif

                                </div>
                                <button class="col-md-2 btn btn-outline-success" style="width:6rem">
                                    <i class="fa fa-gift" aria-hidden="true"></i>
                                    Packed
                                </button>
                            </div>
                            <hr class="m-0">
                        </a>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="modal fade" id="projects-task-modal" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                            <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="col-lg-12">
                            <div class="card">

                                {{ Form::open(array('url' => 'product/packing','id'=>'bootstrap-wizard-form'))}}
                                <div class="wizard p-4" id="bootstrap-wizard-1">
                                    <ul class="nav nav-tabs vertices keep-prefix-suffix" role="tablist">
                                        <li class="nav-item">
                                            <a class=" nav-link active" data-toggle="tab" href="#ex5-step-1"
                                               role="tab">
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#ex5-step-2"
                                               role="tab">
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="ex5-step-1" role="tabpanel">

                                            <div class="row">
                                                <form name="ingredient" id="ingredient">
                                                    <div class="col-sm-8 mt-1">
                                                        <div class="form-group">
                                                            <label for="single">End Product</label>
                                                            <select id="single2" name="end_product"
                                                                    style="width:100%;"
                                                                    onChange="selectEndProducts(this);"
                                                                    class="form-control select2" ui-jp="select2"
                                                                    ui-options="{theme: 'bootstrap'}">
                                                                <option value=''>Select An End Product</option>
                                                                @foreach($endProducts as $product)
                                                                    <option value="{{$product->id}}">{{$product->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-4 mt-1 ">
                                                        <label for="single">Quantity</label>
                                                        <input type="text" placeholder=""
                                                               class="info form-control w-20"
                                                               id="qty" name="qty"
                                                               style="width: 100%;height: calc(2.25rem + 2px);"/>
                                                    </div>
                                                </form>

                                            </div>

                                            <div class="scroll-container">
                                                <div class="col-lg-12 col-md-6">
                                                    <div class="projects-list" id="selecting">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="ex5-step-2" role="tabpanel">
                                            <div class="row">
                                                <div class="col-md-12 py-4 px-1">
                                                    <div class="d-flex mr-auto">
                                                        <div class="form-group">
                                                            <select id="single" name="team"
                                                                    class="form-control select2 " ui-jp="select2"
                                                                    style="width: 15rem"
                                                                    ui-options="{theme: 'bootstrap'}">
                                                                <option value="">Select Team</option>
                                                                @foreach($teams as $emp)
                                                                    <option value=" {{$emp->id}}">{{$emp->employees->pluck('first_name')}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pager d-flex justify-content-center">
                                        <input type="submit" id="finish-btn" class="finish btn btn-success w-50"
                                               value="Finish"/>

                                        <button type="button" id="next-btn" class="next btn btn-success w-50">Next
                                        </button>
                                    </div>
                                </div>
                                {{ Form::close() }}

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/js/jquery.sortable.js')}}"></script>
    <script src="{{asset('assets/examples/js/demos/form.wizard.js')}}"></script>

    <script>
        $("#addEmp").click(function (e) {

            var a = $("#single option:selected").val();
            var o = validateEmp();
            if (o) {
                $.ajax({
                    type: "get",
                    url: '/../employee/get/employee',
                    data: {id: a},

                    success: function (response) {
                        $(".abcz").append('<div class="worker " style="width: 100%" > ' +
                            '<div class="card-body d-flex border-b-1 flex-wrap align-items-center p-2">' +
                            '<div class="d-flex mr-auto">' +
                            '<div>' +
                            '<div class="avatar avatar text-white avatar-md project-icon bg-primary"><img class="card-img-top"src="" alt="">' +
                            '</div>' +
                            '</div>' +
                            '<h5 class="mt-4 mx-4">' + response['first_name'] + ' ' + response['last_name'] + '</h5>' +
                            '<input type="hidden" name="employee[]" value=" ' + response['id'] + ' "/>' +
                            '<input type="hidden"/>' +
                            '<h6 class="float-left mt-4" style=""> </h6>' +
                            '</div>' +
                            '<div class="d-flex activity-counters justify-content-between">' +
                            '<div class="text-center px-2">' +
                            '</div>' +
                            '</div>' +
                            '<div class="section-2">' +
                            '<div onclick="hidediv(this)" class="delete btn btn-light btn-sm ml-2" value=" ' + response['id'] + ' " data-toggle="tooltip" data-placement="left" title="DELETE"> ' +
                            '<i class="zmdi zmdi-close"></i>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>');
                    }
                });

                reset();
            }

        });

        $("#finish-btn").on("click", function (o) {
            e() ? swal("Good job!", "Thanks", "success") : swal("Faild", "Please fill in all fields", "error")
        })

        var o = $("#bootstrap-wizard-form").validate({
            rules: {
                end_product: {required: !0,},
                qty: {required: !0, minlength: 1},

            },
            messages: {
                end_product: "Please select the end product",
                qty: "Please enter quantity",

            },
            errorElement: "div", errorPlacement: function (e, o) {
                e.addClass("form-control-feedback"), o.closest(".form-group").addClass("has-danger"), "checkbox" === o.prop("type") ? e.insertAfter(o.parent(".checkbox")) : e.insertAfter(o)
            }, highlight: function (e, o, r) {
                $(e).closest(".form-group").addClass("has-danger").removeClass("has-success"), $(e).removeClass("form-control-success").addClass("form-control-danger")
            }, unhighlight: function (e, o, r) {
                $(e).closest(".form-group").addClass("has-success").removeClass("has-danger"), $(e).removeClass("form-control-danger").addClass("form-control-success")
            }
        });


        function validateEmp() {
            var y = document.getElementById('single').value;
            if (y == "") {
                swal('Select Employee')
                return false;
            }
            else {
                return true;
            }
        }

        function hidediv(e) {
            $(e).closest(".worker").remove();
        }

        function reset() {
            document.getElementById("single").value = "";
        }


        $('.burning').click(function (e) {
            e.preventDefault();
            id = $(this).attr("value");
            confirmAlert(id);

        });

        function confirmAlert(id) {
            swal({
                    title: "Are you sure to finish packing?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, Finish it!",
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        swal({
                                title: "Add Quantity!",
                                text: "Add actual quantity of the packed:",
                                type: "input",
                                showCancelButton: true,
                                closeOnConfirm: false,
                            },
                            function (inputValue) {
                                if (inputValue === false) return false;
                                if (inputValue === "") {
                                    swal.showInputError("You need to add quantity!");
                                    return false
                                }
                                var qty = inputValue;

                                $.ajax({
                                    type: "get",
                                    url: '../packing/save/qty',
                                    data: {packing_id: id, qty: qty},

                                    success: function (response) {
                                        if (response == 'true') {
                                            swal("Nice!", "You successfully finished the packing", "success");
                                            setTimeout(location.reload.bind(location), 900);
                                        }
                                        else {
                                            swal("Error!", "Something went wrong", "error");
                                        }
                                    }
                                });


                            });

                    } else {
                        swal("Cancelled", "You cancelled the finishing :)", "error");
                    }
                });


        }
    </script>
@stop