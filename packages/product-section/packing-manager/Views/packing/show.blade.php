@extends('layouts.back.master')@section('title','Create Burden')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.wizard.css')}}">



    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        #floating-button {
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #db4437;
            position: fixed;
            top: 5rem;
            right: 20px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .plus {
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }


    </style>
@stop
@section('content')
    <div class="app-wrapper">
        <div class="profile-section-user" id="app-panel">
            {{--@foreach($po as $p)--}}
                <div class="profile-cover-img">
                    <div class="card-body d-flex border-b-1 flex-wrap align-items-center">
                        <div class="d-flex mr-auto">
                            <div id="dash1-easypiechart-1" class="chart easypiechart" data-percent="35">

                                <canvas height="80" width="80"></canvas>
                            </div>
                            <h1 class="display-4 mx-3" data-plugin="counterUp">35</h1><h6 class="float-left mt-2" style="line-height: 1.5"> Total<br>
                                Serveces</h6></div>
                        <div class="d-flex activity-counters justify-content-between">

                            
                        </div>
                    </div>
                </div>
                <div class="profile-info-brief p-3">
                    <div class="text-center"><h5 class="text-uppercase mb-2">h</h5>
                        <div class="hidden-sm-down ">

                        </div>
                        <hr class="m-1 py-1">
                    </div>
                </div>
                <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show"><i
                            class="fa fa-chevron-right"></i>
                    <i class="fa fa-chevron-left"></i>
                </a>
            {{--@endforeach--}}
        </div>


        <div class="app-main">

            <div class="app-main-header">
                <h5 class="app-main-heading text-center">ORDERED PRODUCTS</h5>
            </div>
            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">
                    <div class="container">
                        vv
                    </div>
                </div>

            </div>
        </div>
    </div>





@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{asset('assets/examples/js/demos/form.wizard.js')}}"></script>
    <script src="{{asset('assets/examples/js/apps/projects.js')}}"></script>



    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $('.delete').click(function (e) {
            e.preventDefault();
            id = $(this).attr("value");
            confirmAlert(id);

        });

        function confirmAlert(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "delete",
                        url: id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            }
                            else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

        $("#floating-buttona").click(function () {

            var a = $("#single option:selected").val();
            var qty = $('#qty').val();
            var price = $('#price').val();

            $.ajax({
                type: "post",
                url: '/erp/raw_material/getraw',
                data: {id: a},

                success: function (response) {
//'+ response['name']+'
                    $(".abc").append('<div class="card-body d-flex border-b-1 flex-wrap align-items-center p-2">' +
                        '<div class="d-flex mr-auto"><div>' +
                        '<div class="avatar avatar text-white avatar-md project-icon bg-primary"><img class="card-img-top"src=""alt="">' +
                        '</div></div>' +
                        '<h5 class="mt-4 mx-4">' + response['name'] + '</h5>' +
                        '<input type="hidden" name="new_raw[]" value=" ' + response['raw_material_id'] + ' "/>' +
                        '<input type="hidden" name="new_qty[]" value=" ' + qty + ' "/>' +
                        '<input type="hidden" name="new_price[]" value=" ' + price + ' "/>' +
                        '<h6 class="float-left mt-4" style=""> ' + qty + '</h6>' +
                        '</div>' +
                        '<div class="d-flex activity-counters justify-content-between">' +
                        '<div class="text-center px-2">' +
                        '<input class="form-control" id="formGroupExampleInput" placeholder="" type="text" style="width:8.9rem" disabled>' +
                        '<small class="text-primary">Ordered</small>' +
                        '</div>' +
                        '<div class="text-center px-2">' +
                        '<input class="form-control" id="formGroupExampleInput" value="' + price + '" placeholder="" type="text" style="width:8.9rem" disabled>' +
                        '<small class="text-danger">Price</small>' +
                        '</div>' +
                        '</div>' +
                        '</div>');
                }
            });

//
//            $("#selecting").append('<a>' +
//                '<div class="media">' +
//                ' <div class="avatar avatar-circle avatar-md project-icon" data-plugin="firstLitter" data-target="#' + $id + '"></div>' +
//                ' <div class="media-body"> ' +
//                ' <h6 class="project-name" id=" ' + $id + ' "> ' + $abc + '</h6>' +
//                ' <small class="project-detail">' + $("#qty").val() + '</small>' +
//                ' </div>' +
//                '</div>' +
//                '<input type="hidden" name="ingredients[]" value=" ' + $id + ' ">' +
//                '<input type="hidden" name="qty[]" value=" ' + $("#qty").val() + ' ">' +
//                '</a>');


            reset();
        });

        function reset() {

            document.getElementById("single").value = "";
            document.getElementById("qty").value = "";
            document.getElementById("price").value = "";
        }

    </script>
@stop