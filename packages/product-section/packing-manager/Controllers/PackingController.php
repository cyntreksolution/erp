<?php

namespace PackingManager\Controllers;

use BurdenManager\Models\Burden;
use BurdenManager\Models\BurdenEmployee;
use BurdenManager\Models\BurdenGrade;
use Carbon\Carbon;
use EmployeeManager\Models\Employee;
use EmployeeManager\Models\Group;
use EmployeeManager\Models\GroupEmployee;
use EndProductManager\Classes\ProductContent;
use EndProductManager\Models\EndProduct;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PackingManager\Models\Packed;
use PackingManager\Models\PackingEmployee;
use PackingManager\Models\PackingGroupEmployee;
use Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use RecipeManager\Models\RecipeSemiProduct;
use SemiFinishProductManager\Models\SemiFinishProduct;
use StockManager\Classes\StockTransaction;
use StockManager\Models\StockSemiFinish;
use Validator;

class PackingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect(route('ppacking.create'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = Employee::all();
        $endPrdocuts = EndProduct::all();
        $packing = Packed::where('status', '=', 1)
            ->with('endProdcuts')
            ->get();
        $packed = Packed::where('status', '=', 2)
            ->with('endProdcuts')
            ->get();
        $teams = Group::whereStatus(1)->whereDate('created_at', '=', Carbon::today()->toDateString())->get();

        return view('PackingManager::packing.create',compact('teams','packed'))->with([
            'employees' => $employees,
            'endProducts' => $endPrdocuts,
            'packing' => $packing,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        return $request;
        $validator = Validator::make($request->all(), [
            'end_product' => 'required',
            'team' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect(route('ppacking.create'))
                ->with([
                    'error' => true,
                    'error.title' => 'Error !',
                    'error.message' => 'Please Select Team before finish'
                ])
                ->withInput();
        }

        $team = $request->team;
        $endProduct = $request->end_product;
        $qty = $request->qty;
        $semiQtyOK = 0;
        $packQtyOK = 0;
        $semiProducts = ProductContent::getSemiProductsOfEndProduct(EndProduct::find($endProduct));

        foreach ($semiProducts as $semiProduct) {
            $expectedSemiQty = $semiProduct['pivot']->qty * $qty;
            if ($expectedSemiQty <= $semiProduct->available_qty) {
                $semiQtyOK = 1;
            } else {
                return redirect(route('ppacking.create'))->with([
                    'error' => true,
                    'error.title' => 'Sorry !',
                    'error.message' => 'Not enough semi product'
                ]);
            }
        }
        $packingItems = ProductContent::getPackingItemsOfEndProduct(EndProduct::find($endProduct));
        foreach ($packingItems as $packingItem) {
            $packingItemQty = $packingItem['pivot']->qty * $qty;
            if ($packingItemQty <= $packingItem->available_qty) {
                $packQtyOK = 1;
            } else {
                return redirect(route('ppacking.create'))->with([
                    'error' => true,
                    'error.title' => 'Sorry !',
                    'error.message' => 'Not enough packing item'
                ]);
            }
        }
        if ($semiQtyOK && $packQtyOK) {
            try {
                DB::transaction(function () use ($endProduct, $qty, $team) {
                    $semiProducts = ProductContent::getSemiProductsOfEndProduct(EndProduct::find($endProduct));
                    foreach ($semiProducts as $semiProduct) {
                        $semiProductId = $semiProduct->semi_finish_product_id;
                        $semiProductQty = $semiProduct['pivot']->qty * $qty;

                        StockTransaction::semiFinishStockTransaction(2, $semiProductId, 0, $semiProductQty, 0, 1);
                        StockTransaction::updateSemiFinishAvailableQty(2, $semiProductId, $semiProductQty);
                    }

                    $packingItems = ProductContent::getPackingItemsOfEndProduct(EndProduct::find($endProduct));
                    foreach ($packingItems as $packingItem) {
                        $packingItemId = $packingItem->id;
                        $packingItemQty = $packingItem['pivot']->qty * $qty;

                        StockTransaction::updatePackingItemAvailableQty(2, $packingItemId, $packingItemQty);
                        StockTransaction::PackingItemStockTransaction(2, $packingItemId, $packingItemQty, 0, 1);
                    }

                    $paking = new Packed();
                    $paking->end_product_id = $endProduct;
                    $paking->team = rand() % 26;
                    $paking->expected_packing_qty = $qty;
                    $paking->created_by = Auth::user()->id;
                    $paking->save();

//                    foreach ($employees as $employee) {
//                        $pakingEmployee = new PackingEmployee();
//                        $pakingEmployee->packing_id = $paking->id;
//                        $pakingEmployee->employee_id = $employee;
//                        $pakingEmployee->created_by = Auth::user()->id;
//                        $pakingEmployee->save();
//                    }
                    $t = new PackingGroupEmployee();
                    $t->packing_id = $paking->id;
                    $t->group_id = $team;
                    $t->save();

//                    StockTransaction::endProductStockTransaction(1, $endProduct, 0, 0, 0, $qty, 0, 1);
//                    StockTransaction::updateEndProductAvailableQty(1, $endProduct, $qty);

                });
                return redirect(route('ppacking.create'))->with([
                    'success' => true,
                    'success.title' => 'Congratulations !',
                    'success.message' => 'New End Product has been Created'
                ]);
            } catch (Exception $e) {
                return $e;
                return redirect(route('ppacking.create'))->with([
                    'error' => true,
                    'error.title' => 'Sorry !',
                    'error.message' => 'Something Went Wrong'
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \BurdenManager\Models\Burden $burden
     * @return \Illuminate\Http\Response
     */
    public function show(Burden $burden_order)
    {
        return view('PackingManager::packing.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \BurdenManager\Models\Burden $burden
     * @return \Illuminate\Http\Response
     */
    public function edit(Packed $packed)
    {
        return view('PackingManager::packing.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \BurdenManager\Models\Burden $burden
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Burden $burden)
    {
        $employers = $request->employers;
        try {
            DB::transaction(function () use ($employers, $burden) {

                foreach ($employers as $emp) {
                    $burdenEmp = new BurdenEmployee();
                    $burdenEmp->burden_id = $burden->id;
                    $burdenEmp->employee_id = $emp;
                    $burdenEmp->save();
                }
                $burden->status = 3;
                $burden->save();
            });

            return redirect(route('burden.index'))->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'dasfs'
            ]);
        } catch (Exception $e) {
            return $e;
            return redirect(route('burden.index'))->with([
                'error' => true,
                'error.title' => 'Sorry !',
                'error.message' => 'Something Went Wrong '
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \BurdenManager\Models\Burden $burden
     * @return \Illuminate\Http\Response
     */
    public function destroy(Burden $burden)
    {
        $burden->delete();
        if ($burden->trashed()) {
            return true;
        } else {
            return false;
        }
    }

    public function grade(Request $request)
    {
        $burden_id = $request->burden_id;
        $available_qty = $request->avalableQTY;
        foreach ($request->grade as $key => $gr) {
            $grade = $gr;
            $gradeQty = $request->gradeQty[$key];
            $burdebnGrade = new BurdenGrade();
            $burdebnGrade->burden_id = $burden_id;
            $burdebnGrade->grade_id = $grade;
            $burdebnGrade->burden_qty = $gradeQty;
            $burdebnGrade->save();
        }

        $bd = Burden::find($burden_id);
        $bd->status = 4;
        $bd->actual_qty = $available_qty;
        $bd->save();

        foreach ($request->grade as $key => $gr) {
            $grade = $gr;
            $gradeQty = $request->gradeQty[$key];
            $stockTransaction = new StockSemiFinish();
            $stockTransaction->stock_id = 2;
            $stockTransaction->semi_finish_product_id = $bd->semi_finish_product_id;
            $stockTransaction->grade_id = $grade;
            $stockTransaction->qty = $gradeQty;
            $stockTransaction->save();
        }


        return redirect(route('burden.index'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => 'Garde Added',
        ]);
    }

    public function semiFinishProductRecipeAndCount(Request $request)
    {
        $semi_finish_product = $request->id;
        $res = RecipeSemiProduct::where('semi_finish_product_semi_finish_product_id', '=', $semi_finish_product)->
        where('status', '=', 2)
            ->first();
        $semi = SemiFinishProduct::find($semi_finish_product);

        $req = [
            'recipe' => $res->recipe_id,
            'units_per_recipe' => $res->units,
            'name' => $semi->name,
        ];
        return $req;
    }

    public function saveqty(Request $request)
    {
        $pack = Packed::find($request->packing_id);
        $pack->actual_packing_qty = $request->qty;
        $pack->status = 2;
        $pack->save();
                            StockTransaction::endProductStockTransaction(1, $pack->end_product_id, 0, 0, 0, $pack->actual_packing_qty, 0, 1);
                    StockTransaction::updateEndProductAvailableQty(1, $pack->end_product_id, $pack->actual_packing_qty);

        return 'true';
    }
}
