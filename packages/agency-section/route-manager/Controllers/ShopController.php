<?php

namespace RouteManager\Controllers;

use Illuminate\Support\Facades\Auth;
use PurchasingOrderManager\Models\PurchasingOrder;
use RawMaterialManager\Models\RawAdjust;
use RecipeManager\Models\RecipeContent;
use Response;
use RouteManager\Models\Route;
use RouteManager\Models\Shop;
use Sentinel;
use RawMaterialManager\Models\RawMaterial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use StockManager\Classes\StockTransaction;
use StockManager\Models\Stock;
use StockManager\Models\StockRawMaterial;
use SupplierManager\Models\Supplier;
use VehicleManager\Models\Vehicle;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect(route('shop.create'));
        return view('RouteManager::shop.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $root = Route::where('type', '1')->get();
        $shop = Shop::where('type', '2')->get();

        return view('RouteManager::shop.create')->with([
            'shops' => $shop,
            'rts' => $root
        ]);
    }

    public function status()
    {
        $raws = Route::orderBy('name', 'asc')->get();
        return view('ShopManager::shop.stock')->with([
            'raws' => $raws
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        if (!$request->parent == 0) {
//            $root = $request->parent;
//return $request;
            $route = new Shop();
            $route->parent_id = $request->routes;
            $route->name = $request->name;
            $route->description = $request->desc;
            $route->owner = $request->owner;
            $route->address = $request->address;
            $route->tp_no = $request->tp;
            $route->type = 2;
            $route->color = rand() % 7;
            $route->created_by = Auth::user()->id;
//            $route->parent_id = null;
            $route->save();

//            $route->makeChildOf($root);

//        } else {
//
//
//            $route = new Shop();
//            $route->parent_id = $request->routes;
//            $route->name = $request->name;
//            $route->description = $request->desc;
//            $route->owner = $request->owner;
//            $route->address = $request->address;
//            $route->tp_no = $request->tp;
//            $route->type = 2;
//            $route->color = rand() % 7;
//            $route->created_by = Sentinel::getUser()->id;
//            $route->parent_id = null;
//            $route->save();
//        }
        return redirect(route('shop.create'));

    }


    /**
     * Display the specified resource.
     *
     * @param  \RawMaterialManager\Models\RawMaterial $raw_material
     * @return \Illuminate\Http\Response
     */
    public function show(Shop $shop)
    {
        return view('RouteManager::shop.show')->with([
            'shops' => $shop
        ]);
    }

    public function details(RawMaterial $raw_material)
    {
//        $transactions = StockRawMaterial::with('rawMaterials')
//            ->where('raw_material_id', '=', $raw_material->raw_material_id)->get();
//        return view('VehicleManager::raw_material.details')->with([
//            'raw_material' => $raw_material,
//            'transactions' => $transactions,
//        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \RawMaterialManager\Models\RawMaterial $raw_material
     * @return \Illuminate\Http\Response
     */
    public function edit(RawMaterial $raw_material)
    {
        return view('RouteManager::shop.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \RawMaterialManager\Models\RawMaterial $raw_material
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shop $shop)
    {

        if (isset($request->Name)) {
            $shop->name = $request->Name;
            $shop->save();
            return redirect('shop/' . $shop->id . '/show')->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'name has been updated'
            ]);
        }
//
        if (isset($request->desc)) {
            $shop->description = $request->desc;
            $shop->save();
            return redirect('shop/' . $shop->id . '/show')->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Description has been updated'
            ]);
        }

        if (isset($request->owner)) {
            $shop->owner = $request->owner;
            $shop->save();
            return redirect('shop/' . $shop->id . '/show')->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Owner has been updated'
            ]);
        }

        if (isset($request->address)) {
            $shop->address = $request->address;
            $shop->save();
            return redirect('shop/' . $shop->id . '/show')->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Address has been updated'
            ]);
        }
//
        if (isset($request->tp_no)) {
            $shop->tp_no = $request->tp_no;
            $shop->save();
            return redirect('shop/' . $shop->id . '/show')->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'TP no has been updated'
            ]);
        }
//
//        if (isset($request->stock)) {
//            $currentStock = $raw_material->available_qty;
//            if ($raw_material->measurement == 'gram') {
//                $diffrence = $currentStock - ($request->stock * 1000);
//                $ajustedQty = ($request->stock * 1000);
//            } elseif ($raw_material->measurement == 'milliliter') {
//                $diffrence = $currentStock - ($request->stock * 1000);
//                $ajustedQty = ($request->stock * 1000);
//            } else {
//                $diffrence = $currentStock - $request->stock;
//                $ajustedQty = $request->stock;
//            }
//            $raw_material->available_qty = $ajustedQty;
//            $raw_material->save();
//
//            $adjusted = new RawAdjust();
//            $adjusted->raw_material_raw_material_id = $raw_material->raw_material_id;
//            $adjusted->current_stock = $currentStock;
//            $adjusted->difference = $diffrence;
//            $adjusted->created_by = Sentinel::getUser()->id;
//            $adjusted->save();
//
//            StockTransaction::rawMaterialStockTransaction(3, $raw_material->raw_material_id,$ajustedQty,$adjusted->id,1);
//            return redirect('raw_material/stock/status/' . $raw_material->raw_material_id)->with([
//                'info' => true,
//                'info.title' => 'Congratulations !',
//                'info.message' => 'Current Stock Adjusted'
//            ]);
//        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \RawMaterialManager\Models\RawMaterial $raw_material
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shop $shop)
    {


        $shop->delete();
        if ($shop->trashed()) {
            return 'true';
        } else {
            return 'false';
        }


    }

//    public function getraw(Request $request)
//    {
//        return $raw = RawMaterial::where('raw_material_id', '=', $request->id)->first();
//
//    }

    public function stock(Request $request)
    {
//        $data = Vehicle::all();
//        $jsonList = array();
//        $i = 1;
//
//        foreach ($data as $key => $item) {
//            $dd = array();
//            array_push($dd, $i);
//            array_push($dd, $item->name);
//            $qty = $item->available_qty / 1000;
//            array_push($dd, $item->available_qty);
//            array_push($dd, $item->available_qty);
//            array_push($dd, $item->available_qty);
//
//
////            array_push($dd, '<a href="edit/' . $item->stockpile_id . '" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons">dvr</i></a>');
////            array_push($dd, ' <a href="#" class="btn btn-simple btn-danger btn-icon remove" onclick="del(' . $item->stockpile_id . ')"><i class="material-icons">close</i></a>');
//            array_push($jsonList, $dd);
//            $i++;
//        }
//        return Response::json(array('data' => $jsonList));


    }
}
