<?php

namespace RouteManager\Controllers;

use Illuminate\Support\Facades\Auth;
use PurchasingOrderManager\Models\PurchasingOrder;
use RawMaterialManager\Models\RawAdjust;
use RecipeManager\Models\RecipeContent;
use Response;
use RouteManager\Models\Route;
use RouteManager\Models\Shop;
use Sentinel;
use RawMaterialManager\Models\RawMaterial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use StockManager\Classes\StockTransaction;
use StockManager\Models\Stock;
use StockManager\Models\StockRawMaterial;
use SupplierManager\Models\Supplier;
use VehicleManager\Models\Vehicle;

class RouteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect(route('route.create'));
        return view('RouteManager::route.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $route = Route::where('type', '1')->get();

        return view('RouteManager::route.create')->with([
            'routes' => $route,
        ]);
    }

    public function status()
    {
        $raws = Route::orderBy('name', 'asc')->get();
        return view('RouteManager::route.stock')->with([
            'raws' => $raws
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        return $request;
//        if (!$request->parent == 0) {
//            $root = $request->parent;
//
//            $node = new Route();
//            $node->name = $request->rname;
//            $node->description = $request->desc;
//            $node->type = 1;
//            $node->color = rand() % 7;
//            $node->created_by = Sentinel::getUser()->id;
//            $node->save();
//
//            $node->makeChildOf($root);
//
//        } else {
            $node = new Route();
            $node->name = $request->rname;
            $node->description = $request->desc;
            $node->color = rand() % 7;
            $node->created_by = Auth::user()->id;
            $node->parent_id = null;
            $node->type = 1;
            $node->save();
//        }
        return redirect(route('route.create'));

//            $route = new Route();
//            $route->name = $request->rname;
//            $route->description = $request->desc;
//            $route->color = rand() % 7;
//            $route->created_by = Auth::user()->id;
//            $route->save();
//            return redirect(route('route.create'));
        }


    /**
     * Display the specified resource.
     *
     * @param  \RawMaterialManager\Models\RawMaterial $raw_material
     * @return \Illuminate\Http\Response
     */
    public function show(Route $route)
    {
//        return $route;
        $shop=$route->getImmediateDescendants();
//        $shop = Shop::where('type', '2' )->get();
        return view('RouteManager::route.show')->with([
            'routes' => $route,
            'shops' => $shop
        ]);
    }

    public function details(RawMaterial $raw_material)
    {
//        $transactions = StockRawMaterial::with('rawMaterials')
//            ->where('raw_material_id', '=', $raw_material->raw_material_id)->get();
//        return view('VehicleManager::raw_material.details')->with([
//            'raw_material' => $raw_material,
//            'transactions' => $transactions,
//        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \RawMaterialManager\Models\RawMaterial $raw_material
     * @return \Illuminate\Http\Response
     */
    public function edit(RawMaterial $raw_material)
    {
        return view('RouteManager::route.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \RawMaterialManager\Models\RawMaterial $raw_material
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Route $route)
    {

        if (isset($request->Name)) {
            $route->name = $request->Name;
            $route->save();
            return redirect('route/' . $route->id . '/show')->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'name has been updated'
            ]);
        }
//
        if (isset($request->desc)) {
            $route->description = $request->desc;
            $route->save();
            return redirect('route/' . $route->id . '/show')->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Description has been updated'
            ]);
        }
//
//        if (isset($request->buffer_stock)) {
//            $raw_material->buffer_stock = $request->buffer_stock;
//            $raw_material->save();
//            return redirect('raw_material/' . $raw_material->raw_material_id)->with([
//                'success' => true,
//                'success.title' => 'Congratulations !',
//                'success.message' => 'Buffer stock has been updated'
//            ]);
//        }
//
//        if (isset($request->stock)) {
//            $currentStock = $raw_material->available_qty;
//            if ($raw_material->measurement == 'gram') {
//                $diffrence = $currentStock - ($request->stock * 1000);
//                $ajustedQty = ($request->stock * 1000);
//            } elseif ($raw_material->measurement == 'milliliter') {
//                $diffrence = $currentStock - ($request->stock * 1000);
//                $ajustedQty = ($request->stock * 1000);
//            } else {
//                $diffrence = $currentStock - $request->stock;
//                $ajustedQty = $request->stock;
//            }
//            $raw_material->available_qty = $ajustedQty;
//            $raw_material->save();
//
//            $adjusted = new RawAdjust();
//            $adjusted->raw_material_raw_material_id = $raw_material->raw_material_id;
//            $adjusted->current_stock = $currentStock;
//            $adjusted->difference = $diffrence;
//            $adjusted->created_by = Auth::user()->id;
//            $adjusted->save();
//
//            StockTransaction::rawMaterialStockTransaction(3, $raw_material->raw_material_id,$ajustedQty,$adjusted->id,1);
//            return redirect('raw_material/stock/status/' . $raw_material->raw_material_id)->with([
//                'info' => true,
//                'info.title' => 'Congratulations !',
//                'info.message' => 'Current Stock Adjusted'
//            ]);
//        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \RawMaterialManager\Models\RawMaterial $raw_material
     * @return \Illuminate\Http\Response
     */
    public function destroy(Route $route)
    {

        $route->delete();
        if ($route->trashed()) {
            return 'true';
        } else {
            return 'false';
        }


    }

//    public function getraw(Request $request)
//    {
//        return $raw = RawMaterial::where('raw_material_id', '=', $request->id)->first();
//
//    }

    public function stock(Request $request)
    {
//        $data = Vehicle::all();
//        $jsonList = array();
//        $i = 1;
//
//        foreach ($data as $key => $item) {
//            $dd = array();
//            array_push($dd, $i);
//            array_push($dd, $item->name);
//            $qty = $item->available_qty / 1000;
//            array_push($dd, $item->available_qty);
//            array_push($dd, $item->available_qty);
//            array_push($dd, $item->available_qty);
//
//
////            array_push($dd, '<a href="edit/' . $item->stockpile_id . '" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons">dvr</i></a>');
////            array_push($dd, ' <a href="#" class="btn btn-simple btn-danger btn-icon remove" onclick="del(' . $item->stockpile_id . ')"><i class="material-icons">close</i></a>');
//            array_push($jsonList, $dd);
//            $i++;
//        }
//        return Response::json(array('data' => $jsonList));


    }
}
