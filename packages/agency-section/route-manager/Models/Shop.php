<?php

namespace RouteManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Baum;

class Shop extends Baum\Node
{
    use SoftDeletes;

    protected $table = 'route';

    protected $primaryKey = 'id';

    protected $dates = ['deleted_at'];

    protected $parentColumn = 'parent_id';

    protected $leftColumn = 'lft';

    protected $rightColumn = 'rgt';

    protected $depthColumn = 'depth';

}
