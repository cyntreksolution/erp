<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : erp
 */

Route::middleware(['web','log'])->group(function () {
    Route::prefix('route')->group(function () {
        Route::get('', 'RouteManager\Controllers\RouteController@index')->name('route.index');

//        Route::get('stock/status', 'RouteManager\Controllers\RouteController@status')->name('routes.stock');

//        Route::get('stock/status/{vehicle}', 'RouteManager\Controllers\RouteController@details')->name('routes.stock');

        Route::get('create', 'RouteManager\Controllers\RouteController@create')->name('route.create');

        Route::get('{route}/show', 'RouteManager\Controllers\RouteController@show')->name('route.show');

        Route::get('{route}/edit', 'RouteManager\Controllers\RouteController@edit')->name('route.edit');

//        Route::get('route/list', 'RouteManager\Controllers\RouteController@stock')->name('routes.stock');


        Route::post('', 'RouteManager\Controllers\RouteController@store')->name('route.store');

//        Route::post('getraw', 'RouteManager\Controllers\RouteController@getraw')->name('routes.store');

        Route::post('{route}', 'RouteManager\Controllers\RouteController@update')->name('route.update');

        Route::delete('{route}', 'RouteManager\Controllers\RouteController@destroy')->name('route.destroy');
    });
});

    Route::middleware(['web','log'])->group(function () {
    Route::prefix('shop')->group(function () {
        Route::get('', 'RouteManager\Controllers\ShopController@index')->name('shop.index');

//        Route::get('stock/status', 'RouteManager\Controllers\RouteController@status')->name('routes.stock');

//        Route::get('stock/status/{vehicle}', 'RouteManager\Controllers\RouteController@details')->name('routes.stock');

        Route::get('create', 'RouteManager\Controllers\ShopController@create')->name('shop.create');

        Route::get('{shop}/show', 'RouteManager\Controllers\ShopController@show')->name('shop.show');

        Route::get('{shop}/edit', 'RouteManager\Controllers\ShopController@edit')->name('shop.edit');

//        Route::get('route/list', 'RouteManager\Controllers\RouteController@stock')->name('routes.stock');


        Route::post('', 'RouteManager\Controllers\ShopController@store')->name('shop.store');

//        Route::post('getraw', 'RouteManager\Controllers\RouteController@getraw')->name('routes.store');

        Route::post('{shop}', 'RouteManager\Controllers\ShopController@update')->name('shop.update');

        Route::delete('{shop}', 'RouteManager\Controllers\ShopController@destroy')->name('shop.destroy');
    });
});
