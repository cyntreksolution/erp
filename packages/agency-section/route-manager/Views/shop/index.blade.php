@extends('layouts.back.master')@section('title','Raw Materials')
@section('css')
    <style type="text/css">
        #floating-button{
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #00C851;
            position: fixed;
            bottom: 80px;
            right: 30px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index:2
        }

        .plus{
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }
    </style>
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/datatables.net/css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/datatables.net/css/buttons.dataTables.min.css')}}">
@stop
@section('content')
    <div id="floating-button" data-toggle="tooltip" data-placement="left" data-original-title="NEW RAW MATERIAL" onclick="location.href = '{{url('raw_material/create')}}';">
        <p class="plus">+</p>
    </div>
    <div class="padding">
        <div class="row">
            <table id="datatables" class="table table-striped"
                   cellspacing="0" width="100%" style="width:100%">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Type</th>

                    <th width="1%">Edit</th>
                    <th width="1%">Delete</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script>
        var table;
        $(document).ready(function () {
            table = $('#datatables').dataTable({
                "ajax": '{{url('raw_material/raw/list')}}',
                dom: 'Bfrtip',
                buttons: [
                    'pdf'
                ],

                "autoWidth": false,

                "pagingType": "full_numbers",
                "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
                responsive: true,
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Search raw materials"
                }
            });
            $('.card .material-datatables label').addClass('form-group');
        });
    </script>
@stop