@extends('layouts.back.master')@section('title','Raw Material | Stock')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/inbox.css')}}"/>

    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }
    </style>
@stop
@section('current','Raw Materials')
@section('current_url',route('raw_materials.create'))
@section('current2','Raw Material Stock')
@section('current_url2','')
@section('content')
    <div class="app-wrapper">
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">STOCK STATUS</h5>
            </div>
            <div class="app-main-content">
                <div class="container">
                    <div class="media-list">
                        @foreach($raws as $raw_material)
                                <div class="card-body d-flex border-b-1 flex-wrap align-items-center ">
                                    <div class="d-flex mr-auto" id="dash1-widget-activities">
                                        <div id="dash1-easypiechart-1" class="dash1-easypiechart-1 chart easypiechart"
                                             data-percent="{{$raw_material->available_qty /$raw_material->buffer_stock *100}}">
                                            <div class="avatar avatar-circle avatar-sm bg-red "
                                                 data-plugin="firstLitter"
                                                 data-target="#a-{{$raw_material->raw_material_id}}"
                                                 style="margin: 15px;">
                                            </div>
                                        </div>
                                        <h6 class="float-right mt-3 mx-3 " id="a-{{$raw_material->raw_material_id}}" style="line-height: 1.5">{{$raw_material['name']}}<br>{{round(($raw_material['available_qty']/$raw_material['buffer_stock'])*100)}} %
                                        </h6>
                                    </div>
                                    <div class="d-flex justify-content-between">
                                        <div class="media mb-2" style="border-bottom: none">
                                            <div class="mr-3">
                                                <span class="circle circle-sm bg-primary">
                                                    <i class="fa fa-archive text-white" aria-hidden="true"></i>
                                                    </i>
                                                </span>
                                            </div>
                                            <div class="media-body"><h6 class="my-1 fz-sm">Current Stock</h6>
                                                @if($raw_material->measurement=='gram')
                                                    @if($raw_material->available_qty >= 1000)
                                                        <h6>{{$raw_material->available_qty/1000}} kg</h6>
                                                    @else
                                                        <h6>{{$raw_material->available_qty}} g</h6>
                                                    @endif

                                                @elseif($raw_material->measurement=='milliliter')
                                                    @if($raw_material->available_qty >= 1000)
                                                        <h6>{{$raw_material->available_qty/1000}} l</h6>
                                                    @else
                                                        <h6>{$raw_material->available_qty}} ml</h6>
                                                    @endif
                                                @elseif($raw_material->measurement=='unit')
                                                    <h6>{{$raw_material->available_qty}} units</h6>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="media mb-2" style="border-bottom: none">
                                            <div class="mr-3"><span class="circle circle-sm bg-success">
                                                <i class="fa fa-briefcase text-white" aria-hidden="true"></i></span>
                                            </div>
                                            <div class="media-body"><h6 class="my-1 text-success fz-sm">Buffer Stock</h6>
                                                @if($raw_material->measurement=='gram')
                                                    @if($raw_material->buffer_stock >= 1000)
                                                        <h6>{{$raw_material->buffer_stock/1000}} kg</h6>
                                                    @else
                                                        <h6>{{$raw_material->buffer_stock}} g</h6>
                                                    @endif

                                                @elseif($raw_material->measurement=='milliliter')
                                                    @if($raw_material->buffer_stock >= 1000)
                                                        <h6>{{$raw_material->buffer_stock/1000}} l</h6>
                                                    @else
                                                        <h6>{{$raw_material->buffer_stock}} ml</h6>
                                                    @endif
                                                @elseif($raw_material->measurement=='unit')
                                                    <h6>{{$raw_material->buffer_stock}} units</h6>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="media mb-2 mx-4">
                                            <div class="justify-content-between">
                                                <a href="status\{{$raw_material['raw_material_id']}}" class="create btn  btn-outline-primary btn-rounded btn-sm raw_material_id">
                                                    <span >view transactions</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop

@section('js')

    <script>


    </script>
@stop