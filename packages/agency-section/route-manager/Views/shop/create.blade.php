@extends('layouts.back.master')@section('title','Shops')
@section('css')

    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/modal.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.wizard.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.validation.css')}}">
    <link rel="stylesheet" href="{{asset('assets/packages/route/route.css')}}">

    <style>


    </style>
@stop
@section('current','Shops')
@section('current_url',route('shop.create'))

@section('content')
    <div class="app-wrapper">
        <div class="app-panel" id="app-panel">
            <div class="app-search"><input type="search" class="search-field" placeholder="Search">
                <i class="search-icon fa fa-search"></i></div>
            <div class="app-panel-inner">
                <div class="scroll-container">
                    <div class="p-3">
                        <button class="btn btn-success py-3 btn-block btn-lg" data-toggle="modal"
                                data-target="#projects-task-modal">NEW SHOP
                        </button>
                    </div>

                    <hr class="m-0">
                </div>
                <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show">
                    <i class="fa fa-chevron-right"></i>
                    <i class="fa fa-chevron-left"></i>
                </a>
            </div>
        </div>
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">SHOPS</h5>
            </div>

            <div class="app-main-content px-3 py-4" id="container">
                <div class="content ">

                    <div class="contacts-list">
                        @foreach($shops as $shop)
                            <a href="{{$shop->id}}/show">
                                <div class="panel-item media my-2">

                                        @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                        <div href="{{$shop->id}}"
                                             class="avatar avatar-circle avatar-sm bg-{{$color[$shop->color]}} "
                                             data-plugin="firstLitter"
                                             data-target="#raw-{{$shop->id*874}}">
                                        </div>

                                    <div class="media-body">
                                        <h6 class="media-heading my-1">
                                            <h6 id="raw-{{$shop->id*874}}">{{$shop->name}}</h6>
                                        </h6>
                                        {{--<i class="qty-icon fa fa-archive"></i>--}}
                                        {{--@if($material->measurement=='gram')--}}
                                            {{--<small>{{'Current stock : '.$material->available_qty/1000}} KG</small><br>--}}
                                        {{--@elseif($material->measurement=='milliliter')--}}
                                            {{--<small>{{'Current stock : '.$material->available_qty/1000}} L</small><br>--}}
                                        {{--@elseif($material->measurement=='unit')--}}
                                            {{--<small>{{'Current stock : '.$material->available_qty}} Unit</small><br>--}}
                                        {{--@endif--}}
                                        {{--<small>{{$material->desc}}</small>--}}
                                    </div>
                                    <div class="section-2">
                                        <button class="delete btn btn-light btn-sm ml-2"
                                                value="{{$shop->id}}"
                                                data-toggle="tooltip"
                                                data-placement="left" title="DELETE SHOP">
                                            <i class="zmdi zmdi-close"></i>
                                        </button>
                                    </div>
                                </div>

                            </a>
                        @endforeach
                    </div>

                </div>
                {{--</div>--}}
            </div>
        </div>
    </div>


    <div class="modal fade" id="projects-task-modal" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12">
                        <div class="card">
                            {{ Form::open(array('url' => 'shop','enctype'=>'multipart/form-data' ,'id'=>'bootstrap-wizard-form'))}}
                            <div class="wizard p-4" id="bootstrap-wizard-1">
                                <ul class="nav nav-tabs vertices keep-prefix-suffix" role="tablist">
                                    <li class="nav-item">
                                        <a class=" nav-link active" data-toggle="tab" href="#ex5-step-1"
                                           role="tab">
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="ex5-step-1" role="tabpanel">
                                        <div class="task-name-wrap">
                                            <select id="single2" name="routes" class="form-control select2">
                                                <option value=''>Select Root</option>
                                                @foreach($rts as $rt)
                                                    <option value="{{$rt->id}}">{{$rt->name}}</option>
                                                @endforeach

                                            </select>

                                        </div>
                                        <hr>
                                        <div class="task-name-wrap">
                                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="name"
                                                   placeholder="Shop name">
                                        </div>
                                        <hr>

                                        <div class="task-name-wrap">
                                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="owner"
                                                   placeholder="Name of the owner">
                                        </div>
                                        <hr>

                                        <div class="task-name-wrap">
                                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="address"
                                                   placeholder="Address">
                                        </div>
                                        <hr>
                                        <div class="task-name-wrap">
                                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="tp"
                                                   placeholder="TP no.">
                                        </div>
                                        <hr>

                                        <div class="task-name-wrap">
                                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="desc"
                                                   placeholder="Description">
                                        </div>
                                        <hr>
                                    </div>

                                </div>
                                <div class="pager d-flex justify-content-center">
                                    <input type="submit" id="finish-btn" class="finish btn btn-success w-50"
                                           value="Finish">


                                    <button type="button" id="next-btn" class="next btn btn-success w-50">Next
                                    </button>
                                </div>
                            </div>
                            {!!Form::close()!!}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


@stop
@section('js')

    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('assets/examples/js/apps/projects.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/examples/js/demos/form.wizard.js')}}"></script>

    <script src="{{asset('assets/js/site.js')}}"></script>
    <script src="{{asset('assets/packages/route/route.js')}}"></script>

    <script>

        $('.delete').click(function (e) {
            e.preventDefault();
            id = $(this).attr("value");
            confirmAlert(id);

        });

        function confirmAlert(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "delete",
                        url: id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 600);
                            }
                            else {
                                swal("Error!", "Sorry !. This Ingredient Can not Delete !", "error");
                            }
                        }
                    });

                }
            );

        }
    </script>
@stop