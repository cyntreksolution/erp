@extends('layouts.back.master')@section('title','Shops Details')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/modal.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/lightbox2/dist/css/lightbox.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}"/>

    <style>
        div.out {
            width: 40%;
            height: 120px;
            margin: 0 15px;
            background-color: #d6edfc;
            float: left;
        }

        div.in {
            width: 60%;
            height: 60%;
            background-color: #fc0;
            margin: 10px auto;
        }

        p {
            line-height: 1em;
            margin: 0;
            padding: 0;
        }

        .hide {
            display: none;
        }
    </style>

@stop
@section('current','Shops')
@section('current_url',route('shop.create'))
@section('current2',$shops->name)
@section('current_url2','')
@section('content')
    <div class="app-wrapper">
        <div class="profile-section-user" id="app-panel">
            <div class="profile-cover-img profile-pic">
                <img src="{{asset('assets\img\shop.jpg')}}" alt="">
            </div>
            <div class="profile-info-brief p-3">

                    @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                    <div class="user-profile-avatar avatar avatar-circle avatar-md project-iconr bg-{{$color[$shops->color]}}"
                         data-plugin="firstLitter"
                         data-target="#{{$shops->id*754}}"></div>

                {{--<img class="img-fluid user-profile-avatar"src="{{asset($raw_material->image_path.$raw_material->image)}}">--}}
                    <div class="text-center">
                        <div class="enterleave">
                            <h5 class="text-uppercase mb-1" id="{{$shops->id*754}}">{{$shops->name}}
                                <a href="#" class="hide btn btn-light btn-sm" data-toggle="modal"
                                   data-target="#EditName">
                                    <i class="zmdi zmdi-edit"></i>
                                </a>
                            </h5>
                        </div>
                        <div class="enterleave">
                            <h7 class="text mb-lg-1">{{'Owner : '.$shops->owner}}</h7>
                            <a href="#" class="hide btn btn-light btn-sm" data-target="#EditOwner" data-toggle="modal">
                                <i class="zmdi zmdi-edit"></i>
                            </a>
                        </div>
                        <div class="enterleave">
                            <h7 class="text mb-lg-1">{{'Address : '.$shops->address}}</h7>
                            <a href="#" class="hide btn btn-light btn-sm" data-target="#EditAddress" data-toggle="modal">
                                <i class="zmdi zmdi-edit"></i>
                            </a>
                        </div>
                        <div class="enterleave">
                            <h7 class="text mb-lg-1">{{'TP No : '.$shops->tp_no}}</h7>
                            <a href="#" class="hide btn btn-light btn-sm" data-target="#EditTp" data-toggle="modal">
                                <i class="zmdi zmdi-edit"></i>
                            </a>
                        </div>
                        <hr class="m-0">

                        <div class="enterleave my-2">
                            <h6 class="text mb-lg-1">Description</h6>
                                {{$shops->description}}
                            <a href="#" class="hide btn btn-light btn-sm" data-target="#EditDesc" data-toggle="modal">
                                <i class="zmdi zmdi-edit"></i>
                            </a>
                        </div>

                    </div><!-- /.profile-info-brief -->


            </div>
        </div>
        <div class="app-main bg-white">


            {{--<div class="app-main-header">--}}
            {{--<h5 class="app-main-heading text-center"> Shops on {{$routes->name}} Route </h5>--}}
            {{--</div>--}}
            {{--<div class="scroll-container" id="scroll-container">--}}
                {{--<div class="app-main-content px-3 py-4 ps ps--active-y" id="container">--}}
                    {{--<div class="content ">--}}
                            {{--<div class="contacts-list">--}}
                                {{--<a href="">--}}
                                    {{--<div class="panel-item media my-2">--}}

                                        {{--<div class="avatar avatar avatar-sm project-icon bg-red"--}}
                                             {{--data-plugin="firstLitter"--}}
                                             {{--data-target="#1">--}}

                                        {{--</div>--}}
                                        {{--<div class="media-body">--}}
                                            {{--<div class="">--}}
                                                {{--<h6 class="project-name"--}}
                                                    {{--id="1">Wasana Cake shop--}}
                                                {{--</h6>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}


                                        {{--<div class="text-center mx-5 px-5"><h6--}}
                                                    {{--data-plugin="">22</h6>--}}
                                            {{--<small>units</small>--}}
                                        {{--</div>--}}


                                        {{--<button class="btn btn-outline-primary" style="width:6rem">--}}
                                            {{--<i class="fa fa-eye" aria-hidden="true"></i>--}}
                                            {{--view--}}
                                        {{--</button>--}}
                                    {{--</div>--}}


                                {{--</a>--}}

                            {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>




    <div class="modal fade" id="EditName" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'shop/'.$shops->id, 'id'=>'name' )) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="Name" type="text" class="task-name-field" value="{{$shops->name}}" >
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <div class="modal fade" id="EditDesc" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'shop/'.$shops->id, 'id'=>'desc')) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="desc" type="text" class="task-name-field" value="{{$shops->description}}" >
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <div class="modal fade" id="EditOwner" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'shop/'.$shops->id, 'id'=>'owner')) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="owner" type="text" class="task-name-field" value="{{$shops->owner}}" >
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <div class="modal fade" id="EditAddress" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'shop/'.$shops->id, 'id'=>'address')) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="address" type="text" class="task-name-field" value="{{$shops->address}}" >
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <div class="modal fade" id="EditTp" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'shop/'.$shops->id, 'id'=>'tp_no')) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="tp_no" type="text" class="task-name-field" value="{{$shops->tp_no}}" >
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>



    <div class="modal fade" id="EditAddress" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'shop/'.$shops->id, 'id'=>'address')) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="address" type="text" class="task-name-field" value="{{$shops->address}}" >
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <div class="modal fade" id="EditTp" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'shop/'.$shops->id, 'id'=>'tp_no')) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="tp_no" type="text" class="task-name-field" value="{{$shops->tp_no}}" >
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>



    <div class="modal fade" id="EditAddress" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'shop/'.$shops->id, 'id'=>'address')) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="address" type="text" class="task-name-field" value="{{$shops->address}}" >
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <div class="modal fade" id="EditTp" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'shop/'.$shops->id, 'id'=>'tp_no')) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="tp_no" type="text" class="task-name-field" value="{{$shops->tp_no}}" >
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>



@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/waypoints/lib/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/waypoints/lib/shortcuts/sticky.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/counterup/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/js/jquery.sparkline.min.js')}}"></script>


    <script>
        $("div.enterleave")
            .on("mouseenter", function () {
                $("a:first", this).removeClass('hide');
            })
            .on("mouseleave", function () {
                $("a:first", this).addClass('hide');

            });


        var o = $("#name").validate({
            rules: {
                name: {required: !0}
            },

            messages: {


                name: {required: "Please enter new name"},
            },


        });

        var o = $("#desc").validate({
            rules: {
                name: {required: !0}
            },

            messages: {


                name: {required: "Please enter new name"},
            },


        });


    </script>
@stop