@extends('layouts.back.master')@section('title','Route Details')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/modal.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/lightbox2/dist/css/lightbox.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}"/>

    <style>
        div.out {
            width: 40%;
            height: 120px;
            margin: 0 15px;
            background-color: #d6edfc;
            float: left;
        }

        div.in {
            width: 60%;
            height: 60%;
            background-color: #fc0;
            margin: 10px auto;
        }

        p {
            line-height: 1em;
            margin: 0;
            padding: 0;
        }

        .hide {
            display: none;
        }
    </style>

@stop
@section('current','Route')
@section('current_url',route('route.create'))
@section('current2',$routes->name)
@section('current_url2','')
@section('content')
    <div class="app-wrapper">
        <div class="profile-section-user" id="app-panel">
            <div class="profile-cover-img profile-pic">
                <img src="{{asset('assets\img\vehicle.jpg')}}" alt="">
            </div>
            <div class="profile-info-brief p-3">

                    @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                    <div class="user-profile-avatar avatar avatar-circle avatar-md project-iconr bg-{{$color[$routes->color]}}"
                         data-plugin="firstLitter"
                         data-target="#{{$routes->id*754}}"></div>

                {{--<img class="img-fluid user-profile-avatar"src="{{asset($raw_material->image_path.$raw_material->image)}}">--}}
                    <div class="text-center">
                        <div class="enterleave">
                            <h5 class="text-uppercase mb-1" id="{{$routes->id*754}}">{{$routes->name}}
                                <a href="#" class="hide btn btn-light btn-sm" data-toggle="modal"
                                   data-target="#EditName">
                                    <i class="zmdi zmdi-edit"></i>
                                </a>
                            </h5>
                        </div>
                        <hr class="m-0">

                        <div class="enterleave my-2">
                            <h6 class="text mb-lg-1">Description</h6>
                                {{$routes->description}}
                            <a href="#" class="hide btn btn-light btn-sm" data-target="#EditDesc" data-toggle="modal">
                                <i class="zmdi zmdi-edit"></i>
                            </a>
                        </div>

                    </div><!-- /.profile-info-brief -->


            </div>
        </div>
        <div class="app-main bg-white">


            <div class="app-main-header">
            <h5 class="app-main-heading text-center"> Shops on {{$routes->name}} Route </h5>
            </div>
            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content px-3 py-4 ps ps--active-y" id="container">
                    <div class="content ">
                        @foreach($shops as $shop)
                            <div class="contacts-list">
                                <a href="../../shop/{{$shop->id}}/show">
                                    <div class="panel-item media my-2">
                                        @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                        <div class="avatar avatar avatar-sm project-icon bg-{{$color[$shop->color]}}"
                                             data-plugin="firstLitter"
                                             data-target="#{{$shop->id*754}}">

                                        </div>
                                        <div class="media-body">
                                            <div class="">
                                                <h6 class="project-name"
                                                    id="{{$shop->id*754}}">{{$shop->name}}
                                                </h6>
                                            </div>
                                        </div>


                                        {{--<div class="text-center mx-5 px-5"><h6--}}
                                                    {{--data-plugin="">{{$semiproduct['pivot']->qty}}</h6>--}}
                                            {{--<small>units</small>--}}
                                        {{--</div>--}}


                                        {{--<button class="btn btn-outline-primary" style="width:6rem">--}}
                                            {{--<i class="fa fa-eye" aria-hidden="true"></i>--}}
                                            {{--view--}}
                                        {{--</button>--}}

                                        <div class="section-2">
                                            <button class="delete btn btn-light btn-sm ml-2" value="../../shop/{{$shop->id}}"
                                                    data-toggle="tooltip"
                                                    data-placement="left" title="DELETE SHOP">
                                                <i class="zmdi zmdi-close"></i>
                                            </button>
                                        </div>
                                    </div>


                                </a>

                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="modal fade" id="EditName" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'route/'.$routes->id, 'id'=>'name' )) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="Name" type="text" class="task-name-field" value="{{$routes->name}}" >
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <div class="modal fade" id="EditDesc" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'route/'.$routes->id, 'id'=>'desc')) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="desc" type="text" class="task-name-field" value="{{$routes->description}}" >
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>


@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/waypoints/lib/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/waypoints/lib/shortcuts/sticky.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/counterup/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/js/jquery.sparkline.min.js')}}"></script>


    <script>
        $("div.enterleave")
            .on("mouseenter", function () {
                $("a:first", this).removeClass('hide');
            })
            .on("mouseleave", function () {
                $("a:first", this).addClass('hide');

            });


        // var o = $("#name").validate({
        //     rules: {
        //         name: {required: !0}
        //     },
        //
        //     messages: {
        //
        //
        //         name: {required: "Please enter new name"},
        //     },
        //
        //
        // });
        //
        // var o = $("#desc").validate({
        //     rules: {
        //         name: {required: !0}
        //     },
        //
        //     messages: {
        //
        //
        //         name: {required: "Please enter new name"},
        //     },
        //
        //
        // });



        $('.delete').click(function (e) {
            e.preventDefault();
            id = $(this).attr("value");
            confirmAlert(id);

        });

        function confirmAlert(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "delete",
                        url: id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 600);
                            }
                            else {
                                swal("Error!", "Sorry !. This Ingredient Can not Delete !", "error");
                            }
                        }
                    });

                }
            );

        }

    </script>
@stop