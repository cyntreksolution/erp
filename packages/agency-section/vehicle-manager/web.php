<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web','log'])->group(function () {
    Route::prefix('vehicle')->group(function () {
        Route::get('', 'VehicleManager\Controllers\VehicleController@index')->name('vehicle.index');

        Route::get('stock/status', 'VehicleManager\Controllers\VehicleController@status')->name('vehicle.stock');

        Route::get('stock/status/{vehicle}', 'VehicleManager\Controllers\VehicleController@details')->name('vehicle.stock');

        Route::get('create', 'VehicleManager\Controllers\VehicleController@create')->name('vehicle.create');

        Route::get('{vehicle}', 'VehicleManager\Controllers\VehicleController@show')->name('vehicle.show');

        Route::get('{vehicle}/edit', 'VehicleManager\Controllers\VehicleController@edit')->name('vehicle.edit');

        Route::get('raw/list', 'VehicleManager\Controllers\VehicleController@stock')->name('vehicle.stock');


        Route::post('', 'VehicleManager\Controllers\VehicleController@store')->name('vehicle.store');

//        Route::post('getraw', 'VehicleManager\Controllers\VehicleController@getraw')->name('vehicle.store');

        Route::post('{vehicle}', 'VehicleManager\Controllers\VehicleController@update')->name('vehicle.update');

        Route::delete('{vehicle}', 'VehicleManager\Controllers\VehicleController@destroy')->name('vehicle.destroy');
    });
});
