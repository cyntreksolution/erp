<?php

namespace VehicleManager\Controllers;

use Illuminate\Support\Facades\Auth;
use PurchasingOrderManager\Models\PurchasingOrder;
use RawMaterialManager\Models\RawAdjust;
use RecipeManager\Models\RecipeContent;
use Response;
use Sentinel;
use RawMaterialManager\Models\RawMaterial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use StockManager\Classes\StockTransaction;
use StockManager\Models\Stock;
use StockManager\Models\StockRawMaterial;
use SupplierManager\Models\Supplier;
use VehicleManager\Models\Vehicle;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect(route('vehicle.create'));
        return view('VehicleManager::vehicle.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $supliers = Supplier::orderBy('supplier_name')->get();
        $supliers2 = Supplier::orderBy('supplier_name')->take(4)->get();
        $vehicles = Vehicle::all();

        return view('VehicleManager::vehicle.create')->with([
            'vehicles' => $vehicles,
            'supliers2' => $supliers2,
            'supliers' => $supliers
        ]);
    }

    public function status()
    {
        $raws = Vehicle::orderBy('name', 'asc')->get();
        return view('VehicleManager::vehicle.stock')->with([
            'raws' => $raws
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if (isset($request->image)) {
            $file = $request->file('image');
            $path = '/uploads/images/vehicles/';
            $destinationPath = storage_path($path);
            $file->move($destinationPath, $file->getClientOriginalName());

            $vehicle = new Vehicle();
//            $rw->stock_id = 1;
            $vehicle->name = $request->vehicle_name;
            $vehicle->vehicle_number = $request->vehicle_number;
            $vehicle->class_of_vehicle = $request->vehicle_class;
            $vehicle->fuel_type = $request->fuel_type;
            $vehicle->engine_number = $request->engine_no;
            $vehicle->chassis_number = $request->chassis_no;
            $vehicle->licence_expire_date = $request->licence_ex_date;
            $vehicle->insurance_expire_date = $request->insurance_ex_date;
            $vehicle->image = $file->getClientOriginalName();
            $vehicle->image_path = 'storage' . $path;
            $vehicle->color = rand() % 7;
            $vehicle->created_by = Auth::user()->id;
            $vehicle->save();
            return redirect(route('vehicle.create'));
        } else {
            $vehicle = new Vehicle();
            $vehicle->name = $request->vehicle_name;
            $vehicle->vehicle_number = $request->vehicle_number;
            $vehicle->class_of_vehicle = $request->vehicle_class;
            $vehicle->fuel_type = $request->fuel_type;
            $vehicle->engine_number = $request->engine_no;
            $vehicle->chassis_number = $request->chassis_no;
            $vehicle->licence_expire_date = $request->licence_ex_date;
            $vehicle->insurance_expire_date = $request->insurance_ex_date;
            $vehicle->color = rand() % 7;
            $vehicle->created_by = Auth::user()->id;
            $vehicle->save();
            return redirect(route('vehicle.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \RawMaterialManager\Models\RawMaterial $raw_material
     * @return \Illuminate\Http\Response
     */
    public function show(Vehicle $vehicle)
    {
        return view('VehicleManager::vehicle.show')->with([
            'vehicle' => $vehicle
        ]);
    }

    public function details(RawMaterial $raw_material)
    {
//        $transactions = StockRawMaterial::with('rawMaterials')
//            ->where('raw_material_id', '=', $raw_material->raw_material_id)->get();
//        return view('VehicleManager::raw_material.details')->with([
//            'raw_material' => $raw_material,
//            'transactions' => $transactions,
//        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \RawMaterialManager\Models\RawMaterial $raw_material
     * @return \Illuminate\Http\Response
     */
    public function edit(RawMaterial $raw_material)
    {
        return view('VehicleManager::vehicle.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \RawMaterialManager\Models\RawMaterial $raw_material
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vehicle $vehicle)
    {
        if (isset($request->Name)) {
            $vehicle->name = $request->Name;
            $vehicle->save();
            return redirect('vehicle/' . $vehicle->id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'name has been updated'
            ]);
        }
//
        if (isset($request->desc)) {
            $vehicle->vehicle_number = $request->vnum;
            $vehicle->save();
            return redirect('vehicle/' . $vehicle->id)->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'Vehicle number has been updated'
            ]);
        }
//
//        if (isset($request->buffer_stock)) {
//            $raw_material->buffer_stock = $request->buffer_stock;
//            $raw_material->save();
//            return redirect('raw_material/' . $raw_material->raw_material_id)->with([
//                'success' => true,
//                'success.title' => 'Congratulations !',
//                'success.message' => 'Buffer stock has been updated'
//            ]);
//        }
//
//        if (isset($request->stock)) {
//            $currentStock = $raw_material->available_qty;
//            if ($raw_material->measurement == 'gram') {
//                $diffrence = $currentStock - ($request->stock * 1000);
//                $ajustedQty = ($request->stock * 1000);
//            } elseif ($raw_material->measurement == 'milliliter') {
//                $diffrence = $currentStock - ($request->stock * 1000);
//                $ajustedQty = ($request->stock * 1000);
//            } else {
//                $diffrence = $currentStock - $request->stock;
//                $ajustedQty = $request->stock;
//            }
//            $raw_material->available_qty = $ajustedQty;
//            $raw_material->save();
//
//            $adjusted = new RawAdjust();
//            $adjusted->raw_material_raw_material_id = $raw_material->raw_material_id;
//            $adjusted->current_stock = $currentStock;
//            $adjusted->difference = $diffrence;
//            $adjusted->created_by = Auth::user()->id;
//            $adjusted->save();
//
//            StockTransaction::rawMaterialStockTransaction(3, $raw_material->raw_material_id,$ajustedQty,$adjusted->id,1);
//            return redirect('raw_material/stock/status/' . $raw_material->raw_material_id)->with([
//                'info' => true,
//                'info.title' => 'Congratulations !',
//                'info.message' => 'Current Stock Adjusted'
//            ]);
//        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \RawMaterialManager\Models\RawMaterial $raw_material
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vehicle $vehicle)
    {
        $vehicle->delete();
        if ($vehicle->trashed()) {
            return 'true';
        } else {
            return 'false';
        }


    }

//    public function getraw(Request $request)
//    {
//        return $raw = RawMaterial::where('raw_material_id', '=', $request->id)->first();
//
//    }

    public function stock(Request $request)
    {
//        $data = Vehicle::all();
//        $jsonList = array();
//        $i = 1;
//
//        foreach ($data as $key => $item) {
//            $dd = array();
//            array_push($dd, $i);
//            array_push($dd, $item->name);
//            $qty = $item->available_qty / 1000;
//            array_push($dd, $item->available_qty);
//            array_push($dd, $item->available_qty);
//            array_push($dd, $item->available_qty);
//
//
////            array_push($dd, '<a href="edit/' . $item->stockpile_id . '" class="btn btn-simple btn-warning btn-icon edit"><i class="material-icons">dvr</i></a>');
////            array_push($dd, ' <a href="#" class="btn btn-simple btn-danger btn-icon remove" onclick="del(' . $item->stockpile_id . ')"><i class="material-icons">close</i></a>');
//            array_push($jsonList, $dd);
//            $i++;
//        }
//        return Response::json(array('data' => $jsonList));


    }
}
