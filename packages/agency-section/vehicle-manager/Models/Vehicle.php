<?php

namespace VehicleManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vehicle extends Model
{
    use SoftDeletes;

    protected $table = 'vehicle';

    protected $primaryKey = 'id';

    protected $dates = ['deleted_at'];

}
