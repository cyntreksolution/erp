@extends('layouts.back.master')@section('title','Vehicle')
@section('css')

    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/modal.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.wizard.css')}}">
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.css')}}">
    <link rel="stylesheet" href="{{asset('assets/examples/css/demos/form.validation.css')}}">
    <link rel="stylesheet" href="{{asset('assets/packages/vehicle/vehicle.css')}}">

    <style>


    </style>
@stop
@section('current','Vehicle')
@section('current_url',route('vehicle.create'))

@section('content')
    <div class="app-wrapper">
        <div class="app-panel" id="app-panel">
            <div class="app-search"><input type="search" class="search-field" placeholder="Search">
                <i class="search-icon fa fa-search"></i></div>
            <div class="app-panel-inner">
                <div class="scroll-container">
                    <div class="p-3">
                        <button class="btn btn-success py-3 btn-block btn-lg" data-toggle="modal"
                                data-target="#projects-task-modal">NEW VEHICLE
                        </button>
                    </div>

                </div>
                <a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show">
                    <i class="fa fa-chevron-right"></i> <i class="fa fa-chevron-left"></i></a></div>
        </div>
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">VEHICLES</h5>
            </div>

            <div class="app-main-content px-3 py-4" id="container">
                <div class="content ">

                    <div class="contacts-list">
                        @foreach($vehicles as $vehicle)
                            <a href="{{$vehicle->id}}">
                                <div class="panel-item media my-2">
                                    @if(isset($vehicle->image))
                                        <div class="avatar avatar avatar-sm">
                                            <img src="{{asset($vehicle->image_path.$vehicle->image)}}" alt="">
                                        </div>
                                    @else
                                        @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                                        <div href="{{$vehicle->id}}"
                                             class="avatar avatar-circle avatar-sm bg-{{$color[$vehicle->color]}} "
                                             data-plugin="firstLitter"
                                             data-target="#raw-{{$vehicle->id*874}}">
                                        </div>
                                    @endif
                                    <div class="media-body">
                                        <h6 class="media-heading my-1">
                                            <h6 id="raw-{{$vehicle->id*874}}">{{$vehicle->name}}</h6>
                                        </h6>
                                        {{--<i class="qty-icon fa fa-archive"></i>--}}
                                        {{--@if($material->measurement=='gram')--}}
                                            {{--<small>{{'Current stock : '.$material->available_qty/1000}} KG</small><br>--}}
                                        {{--@elseif($material->measurement=='milliliter')--}}
                                            {{--<small>{{'Current stock : '.$material->available_qty/1000}} L</small><br>--}}
                                        {{--@elseif($material->measurement=='unit')--}}
                                            {{--<small>{{'Current stock : '.$material->available_qty}} Unit</small><br>--}}
                                        {{--@endif--}}
                                        {{--<small>{{$material->desc}}</small>--}}
                                    </div>
                                    <div class="section-2">
                                        <button class="delete btn btn-light btn-sm ml-2"
                                                value="{{$vehicle->id}}"
                                                data-toggle="tooltip"
                                                data-placement="left" title="DELETE VEHICLE">
                                            <i class="zmdi zmdi-close"></i>
                                        </button>
                                    </div>
                                </div>

                            </a>
                        @endforeach
                    </div>

                </div>
                {{--</div>--}}
            </div>
        </div>
    </div>


    <div class="modal fade" id="projects-task-modal" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12">
                        <div class="card">
                            {{ Form::open(array('url' => 'vehicle','enctype'=>'multipart/form-data' ,'id'=>'bootstrap-wizard-form'))}}
                            <div class="wizard p-4" id="bootstrap-wizard-1">
                                <ul class="nav nav-tabs vertices keep-prefix-suffix" role="tablist">
                                    <li class="nav-item">
                                        <a class=" nav-link active" data-toggle="tab" href="#ex5-step-1"
                                           role="tab">
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#ex5-step-2"
                                           role="tab">
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="ex5-step-1" role="tabpanel">

                                        <div class="task-name-wrap">
                                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="vehicle_name"
                                                   placeholder="Vehicle name">
                                        </div>
                                        <hr>

                                        <div class="task-name-wrap">
                                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="vehicle_number"
                                                   placeholder="Vehicle number">
                                        </div>
                                        <hr>

                                        <div class="task-name-wrap">
                                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="vehicle_class"
                                                   placeholder="Class of vehicle">
                                        </div>
                                        <hr>

                                        <div class="task-name-wrap">
                                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="fuel_type"
                                                   placeholder="Fuel type">
                                        </div>
                                        <hr>

                                    </div>
                                    <div class="tab-pane" id="ex5-step-2" role="tabpanel">

                                        <div class="task-name-wrap">
                                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="engine_no"
                                                   placeholder="Engine Number">
                                        </div>
                                        <hr>
                                        <div class="task-name-wrap">
                                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="chassis_no"
                                                   placeholder="Chassis Number">
                                        </div>
                                        <hr>
                                        <div class="task-name-wrap">
                                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="date" name="licence_ex_date"
                                                   placeholder="Licence expaire date">
                                        </div>
                                        <hr>
                                        <div class="task-name-wrap">
                                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="date" name="insurance_ex_date"
                                                   placeholder="Insurance expaire date">
                                        </div>
                                        <hr>
                                        <div class="task-name-wrap">
                                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="file" name="image">
                                        </div>
                                        <hr>
                                    </div>

                                </div>
                                <div class="pager d-flex justify-content-center">
                                    <input type="submit" id="finish-btn" class="finish btn btn-success w-50"
                                           value="Finish">


                                    <button type="button" id="next-btn" class="next btn btn-success w-50">Next
                                    </button>
                                </div>
                            </div>
                            {!!Form::close()!!}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="supplier" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'supplier','enctype'=>'multipart/form-data', 'id'=>'bootstrap-wizard-form'))}}
                {{--<form id="bootstrap-wizard-form">--}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="wizard p-4" id="bootstrap-wizard-1">
                                <ul class="nav nav-tabs vertices keep-prefix-suffix" role="tablist">
                                    <li class="nav-item">
                                        <a class=" nav-link active" data-toggle="tab" href="#ex5-step-1"
                                           role="tab">
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#ex5-step-2"
                                           role="tab">
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="ex5-step-1" role="tabpanel">
                                        <div class="task-name-wrap">
                                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="name"
                                                   placeholder="Supplier Name">
                                        </div>
                                        <hr>
                                        <div class="task-name-wrap">
                                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="address"
                                                   placeholder="Supplier Address">
                                        </div>
                                        <hr>
                                        <div class="task-name-wrap">
                                            <span style="top: 15px;"><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="text" name="tp"
                                                   placeholder="Telephone Number">
                                        </div>
                                        <hr>
                                        {{--<div class="task-name-wrap">--}}
                                        {{--<span><i class="zmdi zmdi-check"></i></span>--}}
                                        {{--<input class="task-name-field" type="text" name="dob"--}}
                                        {{--placeholder="Raw Materials">--}}
                                        {{--</div>--}}

                                        {{--<select class="js-example-responsive" name="permissions[]" multiple="multiple" style="width: 100%">--}}
                                        {{--@foreach($rmaterials as $raw)--}}
                                        {{--<option value=" {{$raw->raw_material_id}}">{{$raw->name}}</option>--}}
                                        {{--@endforeach--}}
                                        {{--</select>--}}
                                        {{--<hr>--}}

                                    </div>
                                    <div class="tab-pane" id="ex5-step-2" role="tabpanel">
                                        <div class="task-name-wrap">
                                            <span><i class="zmdi zmdi-check"></i></span>
                                            <input class="task-name-field" type="file" name="image">
                                        </div>
                                        <hr>
                                    </div>
                                </div>

                                <div class="pager d-flex justify-content-center">
                                    <input type="submit" id="finish-btn" class="finish btn btn-success w-50"
                                           value="Finish"/>

                                    <button type="button" id="next-btn" class="next btn btn-success w-50">Next
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--</form>--}}
                {!!Form::close()!!}
            </div>
        </div>
    </div>

@stop
@section('js')

    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('assets/examples/js/apps/projects.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/examples/js/demos/form.wizard.js')}}"></script>

    <script src="{{asset('assets/js/site.js')}}"></script>
    <script src="{{asset('assets/packages/vehicle/vehicle.js')}}"></script>

    <script>


    </script>
@stop