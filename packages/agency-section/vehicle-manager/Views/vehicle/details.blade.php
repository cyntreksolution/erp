@extends('layouts.back.master')@section('title','Raw Material Details')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/modal.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/lightbox2/dist/css/lightbox.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}"/>

    <style>
        #container {
            position: relative;
            padding: 0px;
            width: 100%;
            height: 330px;
            overflow: auto;
        }

        #container .content {
            width: 100%;
            height: 100%;
        }

        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }
    </style>

@stop
@section('current','Raw Materials')
@section('current_url',route('raw_materials.create'))
@section('current2',$raw_material->name)
@section('current_url2','')
@section('content')
    <div class="app-wrapper">
        <div class="profile-section-user">
            <div class="profile-cover-img profile-pic">
                <img src="{{asset('assets\img\raw material.jpg')}}" alt="">
            </div>
            <div class="profile-info-brief p-3">
                @if(isset($raw_material->image))
                    <div class="user-profile-avatar avatar-circle avatar avatar-sm">
                        <img src="{{asset($raw_material->image_path.$raw_material->image)}}" alt="">
                    </div>
                @else
                    @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                    <div class="user-profile-avatar avatar avatar-circle avatar-md project-iconr bg-{{$color[$raw_material->colour]}}"
                         data-plugin="firstLitter"
                         data-target="#{{$raw_material->id*754}}"></div>
                @endif

                <div class="text-center">
                    <h5 class="text-uppercase mb-1" id="{{$raw_material->id*754}}">{{$raw_material->name}}</h5>
                    @if($raw_material->measurement=='gram')
                        @if($raw_material->available_qty >= 1000)
                            <h6>{{$raw_material->available_qty/1000}} kg</h6>
                        @else
                            <h6>{{$raw_material->available_qty}} g</h6>
                        @endif

                    @elseif($raw_material->measurement=='milliliter')
                        @if($raw_material->available_qty >= 1000)
                            <h6>{{$raw_material->available_qty/1000}} l</h6>
                        @else
                            <h6>{$raw_material->available_qty}} ml</h6>
                        @endif
                    @elseif($raw_material->measurement=='unit')
                        <h6>{{$raw_material->available_qty}} units</h6>
                    @endif
                    {{---------------------------------------------------------------------------------------}}
                    @if(abs($raw_material->available_qty/$raw_material->buffer_stock*100)>75)
                        <div class="progress progress-xs">
                            <div class="progress-bar bg-success" role="progressbar"
                                 style="width: {{abs($raw_material->available_qty/$raw_material->buffer_stock*100)}}%"
                                 aria-valuenow="{{$raw_material->available_qty/$raw_material->buffer_stock*100}}"
                                 aria-valuemin="0" aria-valuemax="100">
                            </div>
                        </div>
                    @elseif(abs($raw_material->available_qty/$raw_material->buffer_stock*100)>50)
                        <div class="progress progress-xs">
                            <div class="progress-bar bg-info" role="progressbar"
                                 style="width: {{abs($raw_material->available_qty/$raw_material->buffer_stock*100)}}%"
                                 aria-valuenow="{{$raw_material->available_qty/$raw_material->buffer_stock*100}}"
                                 aria-valuemin="0" aria-valuemax="100">
                            </div>
                        </div>
                    @elseif(abs($raw_material->available_qty/$raw_material->buffer_stock*100)>25)
                        <div class="progress progress-xs">
                            <div class="progress-bar bg-warning" role="progressbar"
                                 style="width: {{abs($raw_material->available_qty/$raw_material->buffer_stock*100)}}%"
                                 aria-valuenow="{{$raw_material->available_qty/$raw_material->buffer_stock*100}}"
                                 aria-valuemin="0" aria-valuemax="100">
                            </div>
                        </div>
                    @else
                        <div class="progress progress-xs">
                            <div class="progress-bar bg-danger" role="progressbar"
                                 style="width: {{abs($raw_material->available_qty/$raw_material->buffer_stock*100)}}%"
                                 aria-valuenow="{{$raw_material->available_qty/$raw_material->buffer_stock*100}}"
                                 aria-valuemin="0" aria-valuemax="100">
                            </div>
                        </div>
                    @endif
                    <br>
                    @php
                        $user = Sentinel::getUser();
                    @endphp
                    @if ($user->hasAccess(['raw_materials.stock_ajust']))
                        <button type="button" class="btn btn-rounded btn-danger btn-sm" data-toggle="modal"
                                data-target="#ajustStock">ajust current stock
                        </button>
                    @endif

                </div>
            </div>
        </div>
        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">RAW MATERIAL TRANSACTIONS</h5>
            </div>

            <div class="app-main-content px-3 py-4" id="container">
                <div class="content ">
                    <div class="contacts-list">
                        <table class="table table-noborder text-nowrap">
                            <tbody>
                            <tr class="border-b-2">
                                <th>Date</th>
                                <th>Invoice/Burden</th>
                                <th>Value</th>
                                <th>Quantity</th>
                                <th>Type</th>
                            </tr>
                            @foreach($transactions as $transaction)
                                <tr>
                                    <td>{{$transaction->created_at->format('d l F Y')}}</td>
                                    <td>
                                        @if($transaction->type==1)
                                            @php
                                            $invoiceNo=\PurchasingOrderManager\Models\Content::with('purchasingOrder')->where('id','=',$transaction->ref_no)->first();
                                            @endphp
                                            {{$invoiceNo['purchasingOrder']->invoice_no}}
                                        @elseif($transaction->type==2)
                                            @php
                                            $burdenNo=\BurdenManager\Models\Burden::where('id','=',$transaction->ref_no)->first();
                                            @endphp
                                            {{$burdenNo->serial}}
                                        @else
                                            {{$transaction->ref_no}}
                                        @endif
                                        {{--@if($transaction->type==1)--}}
                                        {{--<div class="progress progress-xs">--}}
                                        {{--<div class="progress-bar bg-success" role="progressbar"--}}
                                        {{--style="width: {{abs($transaction->qty/$transaction['rawMaterials']->buffer_stock*100)}}%"--}}
                                        {{--aria-valuenow="{{$transaction->qty/$transaction['rawMaterials']->buffer_stock*100}}"--}}
                                        {{--aria-valuemin="0" aria-valuemax="100">--}}
                                        {{--</div>--}}
                                        {{--</div>--}}
                                        {{--@elseif($transaction->type==2)--}}
                                        {{--<div class="progress progress-xs">--}}
                                        {{--<div class="progress-bar " role="progressbar"--}}
                                        {{--style="width:{{abs($transaction->qty/$transaction['rawMaterials']->buffer_stock*100)}}%"--}}
                                        {{--aria-valuenow="{{$transaction->qty/$transaction['rawMaterials']->buffer_stock*100}}"--}}
                                        {{--aria-valuemin="0" aria-valuemax="100">--}}
                                        {{--</div>--}}
                                        {{--</div>--}}
                                        {{--@else--}}
                                        {{--<div class="progress progress-xs">--}}
                                        {{--<div class="progress-bar bg-danger" role="progressbar"--}}
                                        {{--style="width: {{abs($transaction->qty/$transaction['rawMaterials']->buffer_stock*100)}}%"--}}
                                        {{--aria-valuenow="{{$transaction->qty/$transaction['rawMaterials']->buffer_stock*100}}"--}}
                                        {{--aria-valuemin="0" aria-valuemax="100">--}}
                                        {{--</div>--}}
                                        {{--</div>--}}
                                        {{--@endif--}}
                                    </td>
                                    <td>
                                        @if($transaction->type==1)
                                            @php
                                                $value=\PurchasingOrderManager\Models\Content::where('id','=',$transaction->ref_no)->first();
                                            @endphp
                                            {{$value->raw_total/($transaction->qty/1000)}} LKR
                                        @elseif($transaction->type==2)
                                            -
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>
                                        @if($transaction['rawMaterials']->measurement=='gram')
                                            @if($transaction->qty >= 1000 or $transaction->qty <= -1000 )
                                                <h6>{{$transaction->qty/1000}} kg</h6>
                                            @else
                                                <h6>{{$transaction->qty}} g</h6>
                                            @endif

                                        @elseif($transaction['rawMaterials']->measurement=='milliliter')
                                            @if($transaction->qty >= 1000  or $transaction->qty <= -1000 )
                                                <h6>{{$transaction->qty/1000}} l</h6>
                                            @else
                                                <h6>{$transaction->qty}} ml</h6>
                                            @endif
                                        @elseif($transaction['rawMaterials']->measurement=='unit')
                                            <h6>{{$transaction->qty}} units</h6>
                                        @endif
                                    </td>
                                    <td>
                                        @if($transaction->type==1)
                                            <span class="badge badge-success p-2">Recieved</span>
                                        @elseif($transaction->type==2)
                                            <span class="badge badge-primary p-2">  Send  </span>
                                        @else
                                            <span class="badge badge-danger p-2">Ajusted</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ajustStock" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'raw_material/'.$raw_material->raw_material_id)) }}

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="stock" type="text" class="task-name-field" value="0">
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/waypoints/lib/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/waypoints/lib/shortcuts/sticky.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/counterup/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/js/jquery.sparkline.min.js')}}"></script>
    <script src="{{asset('js/site.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>

    <script>
        var ps = new PerfectScrollbar('#container');
    </script>
@stop
