@extends('layouts.back.master')@section('title','Vehicle Details')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/modal.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/vendor/bower_components/lightbox2/dist/css/lightbox.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}"/>

    <style>
        div.out {
            width: 40%;
            height: 120px;
            margin: 0 15px;
            background-color: #d6edfc;
            float: left;
        }

        div.in {
            width: 60%;
            height: 60%;
            background-color: #fc0;
            margin: 10px auto;
        }

        p {
            line-height: 1em;
            margin: 0;
            padding: 0;
        }

        .hide {
            display: none;
        }
    </style>

@stop
@section('current','Vehicle')
@section('current_url',route('vehicle.create'))
@section('current2',$vehicle->name)
@section('current_url2','')
@section('content')
    <div class="profile-wrapper">
        <div class="profile-section-user">
            <div class="profile-cover-img profile-pic">
                <img src="{{asset('assets\img\vehicle.jpg')}}" alt="">
            </div>
            <div class="profile-info-brief p-3">
                @if(isset($vehicle->image))
                    <div class="user-profile-avatar avatar-circle avatar avatar-sm">
                        <img src="{{asset($vehicle->image_path.$vehicle->image)}}" alt="">
                    </div>
                @else
                    @php $color=array("blue","danger","purple","yellow","success","info","red") @endphp
                    <div class="user-profile-avatar avatar avatar-circle avatar-md project-iconr bg-{{$color[$vehicle->color]}}"
                         data-plugin="firstLitter"
                         data-target="#{{$vehicle->id*754}}"></div>
                @endif

                {{--<img class="img-fluid user-profile-avatar"src="{{asset($raw_material->image_path.$raw_material->image)}}">--}}
                    <div class="text-center">
                        @if(isset($vehicle->image))
                            <button class=" btn btn-outline-primary btn-sm btn-rounded my-3" data-toggle="modal"
                                    data-target="#EditImage">
                                <i class="fa fa-pencil px-1"></i>
                                CHANGE IMAGE
                            </button>
                        @else
                            <button class=" btn btn-outline-primary btn-sm btn-rounded my-3" data-toggle="modal"
                                    data-target="#EditImage">
                                <i class="fa fa-pencil px-1"></i>
                                NEW IMAGE
                            </button>
                        @endif
                        <div class="enterleave">
                            <h5 class="text-uppercase mb-1" id="{{$vehicle->id*754}}">{{$vehicle->name}}
                                <a href="#" class="hide btn btn-light btn-sm" data-toggle="modal"
                                   data-target="#EditName">
                                    <i class="zmdi zmdi-edit"></i>
                                </a>
                            </h5>
                        </div>
                        <hr class="m-0">
                        <div class="enterleave">
                            <h7 class="text mb-lg-1">{{'Vehicle Number : '.$vehicle->vehicle_number	}}</h7>
                            <a href="#" class="hide btn btn-light btn-sm" data-toggle="modal"
                               data-target="#EditNumber">
                                <i class="zmdi zmdi-edit"></i>
                            </a>
                        </div>

                        <div class="enterleave">
                            <h7 class="text mb-lg-1">{{'Vehicle class : '.$vehicle->class_of_vehicle}}</h7>
                            <a href="#" class="hide btn btn-light btn-sm" data-toggle="modal" data-target="#EditBarcode">
                                <i class="zmdi zmdi-edit"></i>
                            </a>
                        </div>
                        <div class="enterleave">
                            <h7 class="enterleave text mb-lg-1">{{'Fuel type : '.$vehicle->fuel_type}}</h7>
                            <a href="#" class="hide btn btn-light btn-sm" data-target="#EditWeight" data-toggle="modal">
                                <i class="zmdi zmdi-edit"></i>
                            </a>
                        </div>
                        <div class="enterleave">
                            <h7 class="text mb-lg-1">{{'Engine number : '.$vehicle->engine_number}}</h7>
                            <a href="#" class="hide btn btn-light btn-sm" data-target="#EditBstock" data-toggle="modal">
                                <i class="zmdi zmdi-edit"></i>
                            </a>
                        </div>

                        <div class="enterleave">
                            <h7 class="text mb-lg-1">{{'Chassis number : '.$vehicle->chassis_number}}</h7>
                            <a href="#" class="hide btn btn-light btn-sm" data-target="#EditMr" data-toggle="modal">
                                <i class="zmdi zmdi-edit"></i>
                            </a>
                        </div>

                        <div class="enterleave">
                            <h7 class="text mb-lg-1">{{'Licence expire at : '.$vehicle->licence_expire_date}}</h7>
                            <a href="#" class="hide btn btn-light btn-sm" data-target="#EditDamage" data-toggle="modal">
                                <i class="zmdi zmdi-edit"></i>
                            </a>
                        </div>

                        <div class="enterleave">
                            <h7 class="text mb-lg-1">{{'Insurance expire at : '.$vehicle->insurance_expire_date}}</h7>
                            <a href="#" class="hide btn btn-light btn-sm" data-target="#EditDamage" data-toggle="modal">
                                <i class="zmdi zmdi-edit"></i>
                            </a>
                        </div>
                    </div><!-- /.profile-info-brief -->


            </div>
        </div>
    </div>



    <div class="modal fade" id="EditImage" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{--{{ Form::open(array('url' => 'assistant','id'=>'jq-validation-example-1')) }}--}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="img" type="file" class="task-name-field" placeholder="first name">
                        </div>
                        <hr>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {{--{{ Form::close() }}--}}
            </div>
        </div>
    </div>

    <div class="modal fade" id="EditName" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'vehicle/'.$vehicle->id)) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="Name" type="text" class="task-name-field" value="{{$vehicle->name}}" >
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

    <div class="modal fade" id="EditNumber" tabindex="-1" role="dialog" aria-divledby="myModaldiv"
         aria-hidden="true">
        <div class="modal-dialog" style="margin-top: 200px" role="document">
            <div class="modal-content">
                {{ Form::open(array('url' => 'vehicle/'.$vehicle->id)) }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-div="Close">
                        <span aria-hidden="true"><i class="zmdi zmdi-close"></i></span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12 p-4" style=" border: 2px solid #eee;">
                        <div class="row task-name-wrap">
                            <span><i class="zmdi zmdi-check"></i></span>
                            <input name="vnum" type="text" class="task-name-field" value="{{$vehicle->vehicle_number}}" >
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-success btn-block" value="ADD">
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>


@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/switchery/dist/switchery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/js/jquery.sparkline.min.js')}}"></script>
    <script src="{{asset('js/site.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>

    <script>
        $("div.enterleave")
            .on("mouseenter", function () {
                $("a:first", this).removeClass('hide');
            })
            .on("mouseleave", function () {
                $("a:first", this).addClass('hide');

            });
    </script>
@stop