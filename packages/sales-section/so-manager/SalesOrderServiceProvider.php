<?php

namespace SalesOrderManager;

use Illuminate\Support\ServiceProvider;

class SalesOrderServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/Views', 'SalesOrderManager');
        require __DIR__ . '/web.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('SalesOrderManager', function($app){
            return new SalesOrderManager;
        });
    }
}
