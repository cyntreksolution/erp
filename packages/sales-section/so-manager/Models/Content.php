<?php

namespace SalesOrderManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Content extends Model
{
    use SoftDeletes;

    protected $fillable = ['sales_order_header_id','end_product_end_product_id','qty'];

    protected $table = 'sales_order_data';

    protected $dates = ['deleted_at'];

    public function salesOrder()
    {
        return $this->belongsTo('SalesOrderManager\Models\SalesOrder');
    }

    public function endProducts()
    {
        return $this->belongsTo('EndProductManager\Models\EndProduct', 'end_product_end_product_id');
    }
}
