<?php

namespace SalesOrderManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RawMaterialManager\Models\RawMaterial;

class RawContent extends Model
{
    use SoftDeletes;

    protected $table = 'sales_order_raw_data';

    protected $dates = ['deleted_at'];

    public function salesOrder(){
        return $this->belongsTo('SalesOrderManager\Models\SalesOrder');
    }

    public function rawMaterial(){
        return $this->belongsTo(RawMaterial::class,'raw_material_id')->withoutGlobalScopes();
    }
}
