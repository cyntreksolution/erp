<?php

namespace SalesOrderManager\Models;

use App\AgentBuffer;
use App\LoadingHeader;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalesOrder extends Model
{
    use SoftDeletes;

    protected $table = 'sales_order_header';

    protected $dates = ['deleted_at'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('type', function (Builder $builder) {
            $builder->where('confirmed', '=', 1);
        });

    }

    public function loadingHeader()
    {
        return $this->hasOne(LoadingHeader::class, 'so_id');
    }

    public function salesRep()
    {
        return $this->belongsTo('SalesRepManager\Models\SalesRep', 'created_by', 'id');
    }

    public function template()
    {
        return $this->belongsTo(AgentBuffer::class);
    }

    public function salesOrderContent()
    {
        return $this->hasMany('SalesOrderManager\Models\Content', 'sales_order_header_id');
    }

    public function salesOrderVariants()
    {
        return $this->hasMany(SalesVariant::class, 'sales_order_header_id');
    }

    public function salesOrderRawContent()
    {
        return $this->hasMany('SalesOrderManager\Models\RawContent', 'sales_order_header_id');
    }

    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }

    public function scopeSearchData($query, $term)
    {
        return $query
            ->Where('id', 'like', "%" . $term . "%")
            ->orWhere('serial', 'like', "%" . $term . "%")
            ->orWhere('status', 'like', "%" . $term . "%")
            ->orWhere('burden_size', 'like', "%" . $term . "%")
            ->orWhere('created_at', 'like', "%" . $term . "%")
            ->orWhereHas('semiProdcuts', function ($query) use ($term) {
                $query->where('name', 'like', "%" . $term . "%");
            })
            ->orWhereHas('recipe', function ($query) use ($term) {
                $query->where('name', 'like', "%" . $term . "%");
            });
    }

    public function scopefilterData($query, $category = null, $day = null, $time = null, $date = null, $agent = null, $status = null, $date_range = null)
    {
        if (!empty($category)) {
            $query->whereHas('template', function ($q) use ($category) {
                return $q->whereCategoryId($category);
            });
        }

        if (!empty($day)) {
            $query->whereHas('template', function ($q) use ($day) {
                return $q->where('day', '=', $day);
            });
        }

        if (!empty($time)) {
            $query->whereHas('template', function ($q) use ($time) {
                return $q->where('time', '=', $time);
            });
        }

        if (!empty($date)) {
            $query->whereDate('order_date', $date);
        }

        if (!empty($agent)) {
            $query->whereCreatedBy($agent);
        }

        if (!empty($status)) {
            $query->whereStatus($status);
        }

        if (!empty($date_range)) {
            $dates = explode(' - ', $date_range);
            $start = $dates[0];
            $end = $dates[1];
            $query->where('created_at', '>=', Carbon::parse($start)->format('Y-m-d'))
                ->where('created_at', '<=', Carbon::parse($end)->addDay()->format('Y-m-d'));
        }
    }

    public function scopeOrderType($query, $type = 'to_burden')
    {
        $query = $query->whereHas('template.category', function ($q) use ($type) {
            $q->where('loading_type', '=', $type);
        });

        return $query;
    }


}
