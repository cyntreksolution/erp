<?php

namespace SalesOrderManager\Models;

use App\AgentBuffer;
use App\Variant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalesVariant extends Model
{
    use SoftDeletes;

    protected $table = 'sales_order_variants';

    protected $dates = ['deleted_at'];

    public function salesOrder()
    {
        return $this->belongsTo('SalesOrderManager\Models\SalesOrder');
    }

    public function variant()
    {
        return $this->belongsTo(Variant::class);
    }
}
