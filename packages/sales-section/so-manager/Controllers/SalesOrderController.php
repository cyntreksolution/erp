<?php

namespace SalesOrderManager\Controllers;

use App\AgentBuffer;
use App\Variant;
use Carbon\Carbon;
use EndProductManager\Models\Category;
use EndProductManager\Models\EndProduct;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use RawMaterialManager\Models\RawMaterial;
use SalesOrderManager\Models\Content;
use SalesOrderManager\Models\RawContent;
use SalesOrderManager\Models\SalesOrder;
use SalesOrderManager\Models\SalesVariant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use SalesRepManager\Models\AgentCategory;

class SalesOrderController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:sales_orders-index', ['only' => ['create','store']]);
        $this->middleware('permission:sales_orders-create', ['only' => ['newOrder']]);
        $this->middleware('permission:sales_orders-list', ['only' => ['list']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $supliers = Supplier::orderBy('supplier_name')->get();
//        $pos = PurchasingOrder::with('supplier', 'purchasingOrderContent.ingredients')
//            ->orderBy('status')
//            ->get();
//        return view('SalesOrderManager::po.index')->with([
//            'supliers' => $supliers,
//            'pos' => $pos
//        ]);
    }

    public function newOrder(Request $request)
    {
        $date = $request->date;
        $bf = AgentBuffer::whereCategoryId($request->category)
            ->where('Day', '=', AgentBuffer::day[$request->day])
            ->where('Time', '=', AgentBuffer::time[$request->time])
            ->whereAgentId(Auth::user()->id)
            ->whereStatus(1)
            ->first();


        if (empty($bf)) {
            return redirect()->back()->with([
                'error' => true,
                'error.title' => 'Sorry !',
                'error.message' => 'No order templates for selected criteria.Please Contact Buntalk Administrations'
            ]);
        }

        $so = SalesOrder::whereCreatedBy(Auth::user()->id)
            ->whereTemplateId($bf->id)
            ->get();
        foreach ($so as $s) {
            if ($s->status == '1' || $s->status == '2' || $s->status == '3') {
                return redirect()->back()->with([
                    'error' => true,
                    'error.title' => 'Sorry !',
                    'error.message' => 'Existing Order!!! You can edit using order list.'
                ]);
            }
        }

        $pending_so = SalesOrder::whereCreatedBy(Auth::user()->id)
            ->whereHas('template', function ($q) use ($request) {
                return $q->whereCategoryId($request->category);
            })
            ->whereStatus(3)
            ->first();
        if (!empty($pending_so)) {
            return redirect()->back()->with([
                'error' => true,
                'error.title' => 'Sorry !',
                'error.message' => 'First you must accept previous invoice, Then Try again.'
            ]);
        }
        $items = $bf->dataset;
        $products = EndProduct::all();
        $raw_materials = RawMaterial::withoutGlobalScope('type')->whereType(4)->get();

        $items = json_decode($items);
//        dd($items);
        return view('SalesOrderManager::so.create2', compact('items', 'products', 'raw_materials', 'bf', 'date'));
    }

    public function newOrderPlacement(Request $request)
    {
//        return $request->all();
        $sales_ref = Auth::user()->id;

        $endProducts = $request->end_product;
        $qtys = $request->end_product_qty;
        $qtys_x = $request->extra_qty;
        $available_qty = $request->available_qty;


        $templateId = $request->template_id;
        $orderDate = $request->order_date;


        //raw material outlet items
        $raw_materials = $request->raw_materials;
        $r_qty = $request->r_qty;


        $description = $request->description;
        $so = 0;

        $so = SalesOrder::where('template_id', '=', $templateId)
            ->where('created_by', '=', $sales_ref)
            ->where('order_date', 'like', Carbon::parse($orderDate)->format('Y-m-d') . '%')
            ->first();

        if (!empty($so) && $so->count()>0) {
            return view('SalesOrderManager::so.preview', compact('so'));
        }

        try {
            DB::transaction(function () use (&$so, $request, $description, $orderDate, $templateId, $available_qty, $sales_ref, $endProducts, $qtys, $qtys_x, $raw_materials, $r_qty) {
                $variants = $request->variant;
                $variant_qty = $request->variant_qty;
                $variant_extra_qty = $request->variant_extra_qty;
                $variant_available_qty = $request->variant_available_qty;



                $so = new SalesOrder();
                $so->created_by = $sales_ref;
                $so->template_id = $templateId;
                $so->description = $description;
                $so->order_date = Carbon::parse($orderDate)->format('Y-m-d');
                $so->save();


                foreach ($endProducts as $key => $item) {
                    $sod = new Content();
                    $sod->sales_order_header_id = $so->id;
                    $sod->end_product_end_product_id = $item;
                    $sod->qty = $qtys[$key];
                    $sod->extra_qty = (!empty($qtys_x[$key])) ? $qtys_x[$key] : 0;
                    $sod->is_extra_order = 0;
                    $sod->available_qty = $available_qty[$key];
                    $sod->save();


//                    if ($qtys_x[$key] != 0) {
//                        $sod = new Content();
//                        $sod->sales_order_header_id = $so->id;
//                        $sod->end_product_end_product_id = $item;
//                        $sod->qty = $qtys_x[$key];
//                        $sod->is_extra_order = 1;
//                        $sod->available_qty = $available_qty[$key];
//                        $sod- >save();
//                    }
                }

                if (!empty($variants)) {
                    $variant_size = [];
                    $variant_size['end_product_end_product_id'] = 0;
                    $variant_size['qty'] = 0;
                    $size = 0;
                    foreach ($variants as $key => $item) {
//                        if ($variant_extra_qty[$key] > 0 || ($variant_qty[$key] - $variant_available_qty[$key])>0) {
                        $sod = new SalesVariant();
                        $sod->sales_order_header_id = $so->id;
                        $sod->variant_id = $item;
                        $sod->qty = $variant_qty[$key];
                        $sod->is_extra_order = 0;
                        $sod->extra_qty = (!empty($variant_extra_qty[$key])) ? $variant_extra_qty[$key] : 0;
                        $sod->available_qty = $variant_available_qty[$key];
                        $sod->save();

//                            $variant = Variant::find($item);
//                            $end_product = EndProduct::find($variant->end_product_id);
//                            $variant_size = $variant->size;
//                            $qty =( (!empty($variant_extra_qty[$key]))?$variant_extra_qty[$key]:0 +  $variant_qty[$key] )-$variant_available_qty[$key];
//                            $requested_qty = $qty * $variant_size;
//
////                            $size = $size + $requested_qty;
//
//                            $variant_size['end_product_end_product_id']=$so->id;
//                            $variant_size['qty'] = $requested_qty;
//
//                            if ()
//                            $variant_end = Content::updateOrCreate(['sales_order_header_id'=>$so->id,'end_product_end_product_id'=>$end_product->id],['qty'=>$size]);
//                        }
//                        }
                    }

                }


                if (!empty($raw_materials)) {
                    foreach ($raw_materials as $key => $item) {
                        $sod = new RawContent();
                        $sod->sales_order_header_id = $so->id;
                        $sod->raw_material_id = $item;
                        $sod->agent_id = $sales_ref;
                        $sod->qty = $r_qty[$key];
                        $sod->save();
                    }
                }
            });


            return view('SalesOrderManager::so.preview', compact('so'));

            return redirect(route('so.create'))->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'New sales order has been added'
            ]);
        } catch (Exception $e) {
            return $e;
            return redirect(route('so.create'))->with([
                'error' => true,
                'error.title' => 'Sorry !',
                'error.message' => 'Something Went Wrong '
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        $products = EndProduct::all();
//        return view('SalesOrderManager::so.create')->with([
//            'products' => $products
//        ]);
        $categories = [];
        $agentCategories = AgentCategory::where('sales_ref_id', '=', Auth::user()->id)->where('status', '=', 1)->get();
        if (!empty($agentCategories)) {
            foreach ($agentCategories as $key => $agentCategory) {
                $categories[] = Category::find($agentCategory->category_id);
            }
        }
        return view('SalesOrderManager::so.newCreate', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sales_ref = Auth::user()->id;
        $endProducts = $request->end_product;
        $qtys = $request->qty;
        try {
            DB::transaction(function () use ($sales_ref, $endProducts, $qtys) {
                $so = new SalesOrder();
                $so->created_by = $sales_ref;
                $so->colour = rand() % 7;
                $so->save();
                foreach ($endProducts as $key => $item) {
                    $sod = new Content();
                    $sod->sales_order_header_id = $so->id;
                    $sod->end_product_end_product_id = $item;
                    $sod->qty = $qtys[$key];
                    $sod->save();
                }
            });

            return redirect(route('so.create'))->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'New sales order has been added'
            ]);
        } catch (Exception $e) {
            return $e;
            return redirect(route('so.create'))->with([
                'error' => true,
                'error.title' => 'Sorry !',
                'error.message' => 'Something Went Wrong '
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \SalesOrderManager\Models\SalesOrder $salesOrder
     * @return \Illuminate\Http\Response
     */
    public function show(SalesOrder $so)
    {
//         $po = PurchasingOrder::with('supplier', 'purchasingOrderContent.ingredients')
//            ->where('id', $purchasingOrder)
//            ->get();

        $endProducts = EndProduct::all();
        return view('SalesOrderManager::so.show')->with([
            'order' => $so, 'endProducts' => $endProducts
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \SalesOrderManager\Models\SalesOrder $salesOrder
     * @return \Illuminate\Http\Response
     */
    public function edit($salesOrder)
    {
//        $raw_materials=RawMaterial::all();
//        $po = PurchasingOrder::with('supplier', 'purchasingOrderContent.ingredients')
//            ->where('id', $purchasingOrder)
//            ->get();
//        return view('SalesOrderManager::po.edit')->with([
//            'po' => $po,
//            'raw_materials'=>$raw_materials,
//
//
//        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \SalesOrderManager\Models\SalesOrder $salesOrder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SalesOrder $salesOrder)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \SalesOrderManager\Models\SalesOrder $salesOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy(SalesOrder $salesOrder)
    {
        $salesOrder->delete();
        if ($salesOrder->trashed()) {
            return true;
        } else {
            return false;
        }
    }

    public function confirmOrder(Request $request)
    {
        if ($request->submit == 'Confirm Order') {
            $order_id = $request->order;
            $order = SalesOrder::whereId($order_id)->withoutGlobalScopes()->first();


            $template_id = $order->template_id;
            $created_by = $order->created_by;
            $order_date = Carbon::parse($order->order_date)->format('Y-m-d');

            $exist_orders = SalesOrder::where('template_id', '=', $template_id)
                ->where('created_by', '=', $created_by)
                ->where('order_date', 'like', $order_date . '%')
                ->where('id', '<>', $order->id)
                ->get();


            if (!empty($exist_orders) && $exist_orders->count() > 0) {
                return redirect(route('so.create'))->with([
                    'error' => true,
                    'error.title' => 'Sorry !',
                    'error.message' => 'Order Has Been Already Placed. Please Edit Using Order List !'
                ]);
            }


            $order->confirmed = 1;
            $order->save();

            return redirect(route('so.create'))->with([
                'success' => true,
                'success.title' => 'Congratulations !',
                'success.message' => 'New sales order has been added'
            ]);
        }

        if ($request->submit == 'Cancel Order') {
            $order_id = $request->order;
            $order = SalesOrder::whereId($order_id)->first();


            $date = $order->order_date;
            $bf = AgentBuffer::find($order->template_id);
            if (empty($bf)) {
                return redirect()->back()->with([
                    'error' => true,
                    'error.title' => 'Sorry !',
                    'error.message' => 'No order templates for selected criteria.Please Contact Buntalk Administrations'
                ]);
            }
            $items = $bf->dataset;
            $products = EndProduct::all();
            $raw_materials = RawMaterial::withoutGlobalScope('type')->whereType(4)->get();

            $items = json_decode($items);

            $contents = Content::where('sales_order_header_id', '=', $order_id)->delete();


            $raws = RawContent::where('sales_order_header_id', '=', $order_id)->delete();


            $sales = SalesVariant::where('sales_order_header_id', '=', $order_id)->delete();


            $order->delete();

//            return redirect()->back()->withInput();
            return view('SalesOrderManager::so.create2', compact('items', 'products', 'raw_materials', 'bf', 'date'));

        }
    }
}
