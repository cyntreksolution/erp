<?php

namespace SalesOrderManager\Controllers;

use App\Exports\BurdenExport;
use App\Http\Controllers\Controller;
use App\LoadingData;
use App\LoadingDataVariant;
use App\LoadingHeader;
use BurdenManager\Models\Burden;
use Carbon\Carbon;
use EndProductManager\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use SalesOrderManager\Models\SalesOrder;
use SemiFinishProductManager\Models\SemiFinishProduct;
use SalesRepManager\Models\SalesRep;

class SalesInvoiceController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:sales_invoice-index', ['only' => ['salesOrdersListShow','tableData']]);
        $this->middleware('permission:SalesHistory-list', ['only' => ['list']]);

    }
    public function salesOrdersListShow(Request $request)
    {
        $categories = Category::pluck('name', 'id');
        $agents = SalesRep::all()->pluck('email', 'id');
        return view('SalesOrderManager::so.list', compact('agents','categories'));
    }

    public function tableData(Request $request)
    {
        $user = Auth::user();
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $invoiceCount = 0;

        $columns = ['id', 'created_at'];
        $order_column = $columns[$order_by[0]['column']];

        $invoiceCountTotal = LoadingHeader::all()->count();
        $invoices = LoadingHeader::tableData($order_column, $order_by_str, $start, $length);

        $stat_data = LoadingHeader::query();
        $total_sum = 0;
        $comission_sum = 0;
        $unit_dis_sum = 0;
        $net_sum = 0;
        $stst = null;

        if ($request->filter == true) {
            $agent = $request->agent;
            $type = $request->type;
            $desc1 = $request->desc1;
            $date_range = $request->date_range;
            $category = $request->category;

            $invoiceCount = LoadingHeader::filterDataSales($date_range, $agent, $category,$type,$desc1, null, null)->count();
            $stst = $stat_data->filterDataSales($date_range, $agent, $category,$type,$desc1, null, null)
                ->select(DB::raw('SUM(total) as  total_sum'), DB::raw('SUM(commission) as com_sum'), DB::raw('SUM(unit_discount) as uni_dis_sum'), DB::raw('SUM(net_amount) as net_sum'))
                ->first();
//            DB::connection()->enableQueryLog();
            $invoices = $invoices->filterDataSales($date_range, $agent, $category,$type,$desc1, null, null)->get();
            $queries = DB::getQueryLog();
//            dd($queries);

            $total_sum = $stst->total_sum;
            $comission_sum = $stst->com_sum;
            $unit_dis_sum = $stst->uni_dis_sum;
            $net_sum = $stst->net_sum;

        } /*elseif (is_null($search) || empty($search)) {
            $invoices = $invoices->get();
            $invoiceCount = LoadingHeader::all()->count();
            $stst = $stat_data
                ->select(DB::raw('SUM(total) as  total_sum'), DB::raw('SUM(commission) as com_sum'), DB::raw('SUM(unit_discount) as uni_dis_sum'), DB::raw('SUM(net_amount) as net_sum'))
                ->first();
        } else {
            $stst = $stat_data->searchData($search)
                ->select(DB::raw('SUM(total) as  total_sum'), DB::raw('SUM(commission) as com_sum'), DB::raw('SUM(unit_discount) as uni_dis_sum'), DB::raw('SUM(net_amount) as net_sum'))
                ->first();
            $invoices = $invoices->searchData($search)->get();
            $invoiceCount = $invoices->count();
        }*/



        $data[][] = array();
        $i = 0;


        foreach ($invoices as $key => $invoice) {
      //      $btn_preview = "<a href='" . route('order-list.preview', [$invoice->id]) . "'  class='ml-2 btn btn-sm btn-primary '>Preview</a>";
            $btn_preview = "<a href='" . route('order-list.preview', [$invoice->id]) . "'  class='btn btn-outline-info btn-sm mr-1' target='_blank' > <i class='fa fa-eye'></i> </a>";
            // $rep = $invoice->salesOrder->salesRep;
            $btnView = '';
            $btnPay = '';
            // $btnView = '<a class="btn btn-outline-info btn-sm mr-1" target="_blank" href=""> <i class="fa fa-eye"></i> </a>';
            // $btnPay = ($invoice->status != 3) ? '<button class="btn btn-outline-success btn-sm mr-1"> <i class="fa fa-dollar"></i> </button>' : null;

            // $agentLink = '<a target="_blank" href="' . route('sales_rep.edit', $rep->id) . '"></i> ' . $rep->first_name . ' ' . $rep->last_name . ' </a>';
            $agentLink = !empty($invoice->salesOrder->salesRep) ? $invoice->salesOrder->salesRep->name_with_initials : '-';
            $statusBtn = null;
          //  if ($invoice->status == 1) {
          //      $statusBtn = '<button class="btn btn-outline-danger btn-sm mr-1">not paid</button>';
          //  } else {
                switch ($invoice->is_paid) {
                    case 0:
                        $statusBtn = '<button class="btn btn-outline-danger btn-sm mr-1">not paid</button>';
                        break;
                    case 1:
                        $statusBtn = '<button class="btn btn-outline-success btn-sm mr-1">paid</button>';
                        break;
                    case 2:
                        $statusBtn = '<button class="btn btn-outline-warning btn-sm mr-1">partial paid</button>';
                        break;

                }
           // }
            $order_inv_id = $invoice->id;
            $itemsum1 = LoadingData::selectRaw('COALESCE(sum(total), 0) as itemtotal')->whereLoadingHeaderId($order_inv_id)->first();
            $itemsum2 = LoadingData::selectRaw('COALESCE(sum(total_discount), 0) as itemdis')->whereLoadingHeaderId($order_inv_id)->first();

            $itemvari1 = LoadingDataVariant::selectRaw('COALESCE(sum(total), 0) as varitotal')->whereLoadingHeaderId($order_inv_id)->first();
            $itemvari2 = LoadingDataVariant::selectRaw('COALESCE(sum(total_discount), 0) as varidis')->whereLoadingHeaderId($order_inv_id)->first();

            $itemtot = LoadingHeader::whereId($order_inv_id)->first();
            $tot1 = $itemtot->total;
            $tot2 = $itemtot->total_discount;

            $tot = (($tot1)-($tot2));

            $load = (($itemsum1->itemtotal)-($itemsum2->itemdis)) + (($itemvari1->varitotal)-($itemvari2->varidis));
            $dif = $tot - $load ;
            $diff = number_format(($dif),0);

            $difval = number_format(($dif),2);

            $btn_wrong = null;

            if($diff != 0){

                $btn_wrong = "<button class='btn btn-sm btn-danger'>Something Went Wrong .....! [ Value of $difval ]</button>";

            }
            // switch ($invoice->status) {
            //     case 1:
            //         $statusBtn = '<button class="btn btn-outline-danger btn-sm mr-1">not paid</button>';
            //         break;
            //     case 2:
            //         $statusBtn = '<button class="btn btn-outline-warning btn-sm mr-1">partial paid</button>';
            //         break;
            //     case 3:
            //         $statusBtn = '<button class="btn btn-outline-success btn-sm mr-1">paid</button>';
            //         break;
            //
            // }
            $cat = $invoice->salesOrder->template->category->name;

            $data[$i] = array(
                $cat . "-" .$invoice->invoice_number,
                Carbon::parse($invoice->created_at)->format('Y-m-d H:m:s'),
                number_format($invoice->total, 2),
                number_format($invoice->commission, 2),
                number_format($invoice->unit_discount, 2),
                number_format($invoice->net_amount, 2),
                $agentLink,
                $statusBtn,
                $btnView . $btnPay .$btn_preview .$btn_wrong,
            );
            $i++;
        }

        if ($invoiceCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($invoiceCountTotal),
            "recordsFiltered" => intval($invoiceCount),
            "data" => $data,
            "total_sum" => number_format($total_sum, 2),
            "comission_sum" => number_format($comission_sum, 2),
            "unit_dis_sum" => number_format($unit_dis_sum, 2),
            "net_sum" => number_format($net_sum, 2),
        ];

        return json_encode($json_data);
    }

    public function exportFilter(Request $request)
    {

        $semi = $request->semi;
        $status = $request->status;
        $date_range = $request->date_range;
        $records = Burden::filterData($semi, $status, $date_range)->get();
        return Excel::download(new BurdenExport($records), Carbon::now() . '_burden_report.xlsx');
    }

}
