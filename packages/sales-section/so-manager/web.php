<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web'])->group(function () {
    Route::prefix('so')->group(function () {
        Route::get('list', 'SalesOrderManager\Controllers\SalesInvoiceController@salesOrdersListShow')->name('sales.list');
        Route::get('table/data', 'SalesOrderManager\Controllers\SalesInvoiceController@tableData')->name('sales.table');
        Route::post('excel/data', 'SalesOrderManager\Controllers\SalesInvoiceController@exportFilter')->name('sales.excel');

        Route::get('', 'SalesOrderManager\Controllers\SalesOrderController@index')->name('so.index');

        Route::get('create', 'SalesOrderManager\Controllers\SalesOrderController@create')->name('so.create');

        Route::get('{so}', 'SalesOrderManager\Controllers\SalesOrderController@show')->name('so.show');

        Route::get('{so}/edit', 'SalesOrderManager\Controllers\SalesOrderController@edit')->name('so.edit');

        Route::post('new', 'SalesOrderManager\Controllers\SalesOrderController@newOrder')->name('so.new');
        Route::post('new/placement', 'SalesOrderManager\Controllers\SalesOrderController@newOrderPlacement')->name('so.newOrder');
        Route::post('confirm/placement', 'SalesOrderManager\Controllers\SalesOrderController@confirmOrder')->name('so.confOrder');
        Route::post('', 'SalesOrderManager\Controllers\SalesOrderController@store')->name('so.store');

        Route::put('{so}', 'SalesOrderManager\Controllers\SalesOrderController@update')->name('so.update');
        Route::delete('{so}', 'SalesOrderManager\Controllers\SalesOrderController@destroy')->name('so.destroy');
        
    });
//        Route::get('so/create', 'SalesOrderManager\Controllers\SalesOrderController@create')->name('so.createmanager');
});
