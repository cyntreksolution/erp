@extends('layouts.back.master')@section('title','Load with discount')
@section('css')

@stop
@section('current','Load with discount')
<!-- @section('current_url',route('raw_materials.create')) -->

@section('content')
    <div class="container">
        <div class="app-wrapper">
            <div class="app-main">
                {!! Form::open(['route'=>'loading.discount','method'=>'post']) !!}
                <input type="hidden" name="order" value="{{$loading->id}}">
                <div class="row">
                    <div class="col-md-6" id="app-panel">
                        <div class="app-main-header">
                            <h5 class="app-main-heading text-center">Items with commission</h5>
                        </div>
                        <div class="app-main-content px-3 py-4">
                            <div class="content">

                                <div class="row ">
                                    <div class="form-group col-md-6">
                                        <label for="">Name</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Qty</label>
                                    </div>
                                    <div class="col-md-1 text-right">
                                        <label class="text-right">Value</label>
                                    </div>
                                    <div class="col-md-2 text-right">
                                        <label class="text-right">Rate %</label>
                                    </div>
                                    <div class="col-md-2 text-right">
                                        <label class="text-right">Commission</label>
                                    </div
                                @foreach($loading->items as $data)
                                    @if($data->qty != 0)
                                        <div class="row ">
                                            <div class="form-group col-md-6">
                                                <input type="checkbox" name="end_product_commission[]"
                                                       id="end_product_commission_{{$data->endProduct->id}}"
                                                       checked
                                                       value="{{$data->endProduct->id}}">
                                                <label class="text-right">{{ $data->endProduct->name }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ $data->qty }}</label>
                                                <input type="hidden" name="end_qty[]" value="{{ $data->qty }}"></input>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ $data->endProduct->selling_price * $data->qty }}</label>
                                            </div>
                                            <div class="col-md-2 text-right">
                                                <label class="text-right">{{ $data->endProduct->commission }}</label>
                                            </div>
                                            <div class="col-md-2 text-right">
                                                <label class="text-right">{{ number_format(($data->endProduct->selling_price * $data->qty)/ 100*$data->endProduct->commission,2) }}</label>
                                                <input type="hidden" name="end_product_commission_value[{{$data->endProduct->id}}]"
                                                       value="{{ ($data->endProduct->selling_price * $data->qty)/ 100*$data->endProduct->commission }}"
                                                       id="end_product_commission_value_{{$data->endProduct->id}}">
                                            </div>
                                        </div>
                                    @endif
                                @endforeach

                                @foreach($loading->variants as $data)
                                    @if($data->weight != 0)
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <input type="checkbox" name="variant_commission[]"
                                                       id="variant_commission_{{$data->variant->product->id}}"
                                                       checked
                                                       value="{{$data->variant->id}}">
                                                <label class="text-right">  {{ $data->variant->product->name.' '.$data->variant->variant }}</label>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ $data->weight }}</label>
                                                <input type="hidden" name="variant_qty[]" value="{{ $data->weight }}"></input>
                                            </div>
                                            <div class="col-md-1 text-right">
                                                <label class="text-right">{{ $data->variant->product->selling_price * $data->weight }}</label>
                                            </div>
                                            <div class="col-md-2 text-right">
                                                <label class="text-right">{{ $data->variant->product->commission }}</label>
                                            </div>
                                            <div class="col-md-2 text-right">
                                                <input type="hidden" name="variant_commission_value[{{$data->variant->id}}]"
                                                       id="variant_commission_value_{{$data->variant->id}}"
                                                       value="{{  ($data->variant->product->selling_price * $data->weight)* $data->variant->product->commission /100 }}">

                                                <label class="text-right">{{ number_format(($data->variant->product->selling_price * $data->weight)* $data->variant->product->commission /100 ,2) }}</label>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="app-main-header">
                            <h5 class="app-main-heading text-center">Items with Discount</h5>
                        </div>

                        <div class="app-main-content px-3 py-4">
                            <div class="content ">
                                <div class="row">
                                    <div class="form-group col-md-9">
                                        <label for="">Name</label>
                                    </div>
                                    <div class="col-md-3">
                                        Unit Discount
                                    </div>
                                </div>
                                @foreach($loading->items as $data)
                                    @if($data->qty != 0)
                                        <div class="row">
                                            <div class="form-group col-md-9">
                                                <label for="">{{ $data->endProduct->name }}</label>
                                                <input type="hidden" name="end_product_item_discount[]" checked
                                                       value="{{$data->endProduct->id}}">
                                            </div>
                                            <div class="col-md-3">
                                                <input required step="0.01" type="number" name="end_product_item_discount_value[]"
                                                       class="form-control" value="{{$data->endProduct->unit_discount}}">
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                                @foreach($loading->variants as $data)
                                    @if($data->weight != 0)
                                        <div class="row">
                                            <div class="form-group col-md-9">
                                                <label for="">{{ $data->variant->product->name.' '.$data->variant->variant }}</label>
                                                <input type="hidden" name="variant_item_discount[]" checked
                                                       value="{{$data->variant->id}}">
                                            </div>
                                            <div class="col-md-3">
                                                <input required step="0.01" type="number" name="variant_item_discount_value[]"
                                                       class="form-control" value="{{$data->variant->product->unit_discount}}">
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        <br>

                        {{--                        <label for="">Total Bill Amount: </label> <label for="" id="billAmount"></label> <br>--}}
                        {{--                        <label for="">Total Commision: </label> <label for="" id="commission"></label> <br>--}}
                        {{--                        <label for="">Total Discount: </label> <label for="" id="Disc"></label> <br>--}}
                        {{--                        <label for="">Total: </label> <label for="" id="finalTOT"></label> <br>--}}
                        <button type="submit" class="btn btn-success btn-block mb-3" id="btnSave" value="stream"
                                name="button">
                            Generate Bill
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@stop
@section('js')


    <script>
        function checkItem(id) {
            let name = '#end_product_commission_' + id;
            let valu_name ='#end_product_commission_value_'+id;
            if ($(name).prop("checked") == true) {
                $(valu_name).removeAttr('disabled');
            } else if ($(name).prop("checked") == false) {
              $(valu_name).attr('disabled');
            }
        }
    </script>

    {{--    <script type="text/javascript">--}}
    {{--        var maxcommission = [];--}}
    {{--        //max price array--}}
    {{--        var maxtotal = [];--}}

    {{--        function countCommission(e, id) {--}}
    {{--            $('.checkbox_' + id).change(function () {--}}
    {{--                //variables--}}
    {{--                let qty = $(".checkbox_" + id).data('id');--}}
    {{--                let price = $('.checkbox_' + id).data('price');--}}
    {{--                let commission = $(".checkbox_" + id).data('info');--}}

    {{--                var billAmount = price * qty;--}}
    {{--                maxtotal.push(billAmount);--}}
    {{--                var maxprice = 0;--}}
    {{--                for (var i = 0; i < maxtotal.length; i++) {--}}
    {{--                    maxprice += maxtotal[i] << 0;--}}
    {{--                }--}}

    {{--                var total = qty * commission;--}}
    {{--                maxcommission.push(total);--}}
    {{--                var totalx = 0;--}}
    {{--                for (var i = 0; i < maxcommission.length; i++) {--}}
    {{--                    totalx += maxcommission[i] << 0;--}}
    {{--                }--}}
    {{--                // alert(totalx);--}}
    {{--                $('#billAmount').html(maxprice);--}}
    {{--                $('#commission').html(totalx);--}}
    {{--            });--}}
    {{--        }--}}

    {{--        var totalDiscount = [];--}}

    {{--        function countDiscount(e, id) {--}}
    {{--            $(".discount_" + id).focusout(function () {--}}
    {{--                // alert(id);--}}
    {{--                let discount = $(this).val();--}}
    {{--                // alert(discount);--}}
    {{--                totalDiscount.push(discount);--}}
    {{--                //discount--}}
    {{--                var discountx = 0;--}}
    {{--                for (var i = 0; i < totalDiscount.length; i++) {--}}
    {{--                    discountx += totalDiscount[i] << 0;--}}
    {{--                }--}}
    {{--                //total commission--}}
    {{--                var totalx = 0;--}}
    {{--                for (var i = 0; i < maxcommission.length; i++) {--}}
    {{--                    totalx += maxcommission[i] << 0;--}}
    {{--                }--}}
    {{--                //total price--}}
    {{--                var maxprice = 0;--}}
    {{--                for (var i = 0; i < maxtotal.length; i++) {--}}
    {{--                    maxprice += maxtotal[i] << 0;--}}
    {{--                }--}}
    {{--                // alert(totalx);--}}
    {{--                var finalTotal = maxprice - (totalx + discountx);--}}
    {{--                // alert(finalTotal);--}}
    {{--                $('#Disc').html(discountx);--}}
    {{--                $('#finalTOT').html(finalTotal);--}}
    {{--            });--}}
    {{--        }--}}

    {{--        function checkOrderStatus(order) {--}}
    {{--            let url = 'item/loading/check/' + order;--}}
    {{--            $.ajax({--}}
    {{--                type: "get",--}}
    {{--                url: url,--}}
    {{--                success: function (response) {--}}
    {{--                    // console.log(response)--}}
    {{--                    if (response == 'true') {--}}
    {{--                        $('#btnSave').removeClass('d-none');--}}
    {{--                    } else {--}}
    {{--                        $('#btnSave').addClass('d-none');--}}
    {{--                    }--}}
    {{--                }--}}
    {{--            });--}}
    {{--        }--}}
    {{--    </script>--}}


@stop
