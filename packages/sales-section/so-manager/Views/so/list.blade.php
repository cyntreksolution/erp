@extends('layouts.back.master')@section('title','Sales History')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <style>
        th {
            text-align: center !important;
        }

        td {
            text-align: right !important;
        }
    </style>
@stop

@section('content')
    {{ Form::open(array('url' => '/burden/excel/data','id'=>'filter_form'))}}
    <div class="row">
        <div class="col-sm-2">
            @if((Sentinel::check()->roles[0]->slug=='owner'))
            <div class="form-group">
                {!! Form::select('agent',$agents , null , ['class' => 'form-control','placeholder'=>'Select Agent','id'=>'agent']) !!}
            </div>
                @if(Auth::user()->hasRole(['Owner','Sales Agent','Super Admin']))

                <div class="form-group col-sm-3">
                    <input type="hidden" name="agent" id="agent" value="{{Auth::user()->id}}">

                </div>
                @endif
            @else
                <div class="form-group">
                    {!! Form::select('agent',$agents , null , ['class' => 'form-control','id'=>'agent']) !!}
                </div>
            @endif
        </div>
        {{--        <div class="col-sm-3">--}}
        {{--            <div class="form-group">--}}
        {{--                {!! Form::select('semi', $semiFinish , null , ['class' => 'form-control','placeholder'=>'Semi Finish Product','id'=>'semi']) !!}--}}
        {{--            </div>--}}
        {{--        </div>--}}

                <div class="col-sm-2">
                    @if((Sentinel::check()->roles[0]->slug=='owner') || (Sentinel::check()->roles[0]->slug == 'sales-ref'))
                    <div class="form-group">
                        {!! Form::select('category',$categories , null , ['class' => 'form-control','placeholder'=>'Category','id'=>'category']) !!}
                    </div>
                    @else
                        <div class="form-group">
                            {!! Form::select('category',$categories , null , ['class' => 'form-control','id'=>'category']) !!}
                        </div>
                    @endif
                </div>
        <div class="col-sm-3">
            <div class="form-group">
                <input type="text" name="date_range" id="date_range" class="form-control">
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                {!! Form::select('desc1', [1=>'Distributor',2=>'Show Room',3=>'Direct Sale'] , null , ['class' => 'form-control','placeholder'=>'Select Type','id'=>'desc1']) !!}
            </div>
        </div>
        <div class="col-sm-2">
            <div class="form-group">
                {!! Form::select('type', [1=>'Unpaid',2=>'Paid'] , null , ['class' => 'form-control','placeholder'=>'Select Pay Status','id'=>'type']) !!}
            </div>
        </div>


        <div class="col-sm-1">
            <div class="form-group">
                <button type="button" class="btn btn-success" onclick="process_form()">Filter</button>
                {{--                <button type="button" class="btn btn-warning" onclick="process_form_reset()">Reset Filter</button>--}}
                {{--                <button type="submit" name="action" value="download_data" class="btn btn-info"><i--}}
                {{--                            class="fa fa-download"></i></button>--}}
            </div>
        </div>
    </div>
    {{ Form::close() }}
    <div class="">
        <table id="invoice_table" class="display text-center">
            <thead>
            <tr>
                <th>Order ID</th>
                <th>Date</th>

                <th>Total Amount</th>
                <th>Commission</th>
                <th>Unit Discount</th>
                <th>Net Amount</th>
                <th>Agent</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Order ID</th>
                <th>Date</th>
                <th>Total Amount</th>
                <th>Commission</th>
                <th>Unit Discount</th>
                <th>Net Amount</th>
                <th>Agent</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
    </div>
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script>
        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019",
            "maxDate":moment()
        });

        $('#date_range').val('')


        var total_sum;
        var comission_sum;
        var unit_dis_sum;
        var net_sum;
        $(document).ready(function () {
            table = $('#invoice_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "ajax": {
                    url: "{{url('/so/table/data')}}", // json datasource
                    type: "get",
                    error: function () {  // error handling code
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    },
                    dataSrc: function (data) {
                        total_sum = data.total_sum;
                        comission_sum = data.comission_sum;
                        unit_dis_sum = data.unit_dis_sum;
                        net_sum = data.net_sum;
                        return data.data;
                    }
                },
                pageLength: 25,
                responsive: true,
                drawCallback: function (settings) {
                    var api = this.api();

                    $(api.column(2).footer()).html(
                        'Total Amount : ' + total_sum
                    );
                    $(api.column(3).footer()).html(
                        'Commission : ' + comission_sum
                    );
                    $(api.column(4).footer()).html(
                        'Unit Discount : ' + unit_dis_sum
                    );
                    $(api.column(5).footer()).html(
                        'Net Amount : ' + net_sum
                    );


                }
            });
        });

        function process_form() {
            let date_range = $("#date_range").val();
            let agent = document.getElementById('agent').value;
            let category = document.getElementById('category').value;
            let type = document.getElementById('type').value;
            let desc1 = document.getElementById('desc1').value;
            let table = $('#invoice_table').DataTable();
            table.ajax.url('/so/table/data?agent=' + agent +'&date_range=' + date_range +'&category=' + category + '&type=' + type + '&desc1=' + desc1 + '&filter=' + true).load();

        }

        function process_form_reset() {
            $("#date_range").val('');
            $("#status").val('');
            $("#paytype").val('');
            $("#semi").val('');
            let table = $('#burden_table').DataTable();
            table.ajax.url('/so/table/data').load();

        }


    </script>
    <script>
        function deleteBurden(id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            swal({
                    title: "Are you sure ?",
                    text: "Do you want to Delete? This action cannot be recover !",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, Delete it!",
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: "post",
                            url: '../burden/delete/burden',
                            data: {burden_id: id},

                            success: function (response) {
                                if (response == 'true') {
                                    swal("Nice!", "You successfully Deleted the burning", "success");
                                    setTimeout(location.reload.bind(location), 900);
                                } else {
                                    swal("Error!", "Something went wrong", "error");
                                }
                            }
                        });


                        // });

                    } else {
                        swal("Cancelled", "You cancelled the finishing :)", "error");
                    }
                });


        }
    </script>
@stop
