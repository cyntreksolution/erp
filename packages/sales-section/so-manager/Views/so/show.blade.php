@extends('layouts.back.master')@section('title','Purchasing order')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}">


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .app-wrapper {
            font-family: Raleway, sans-serif;
            width: 100%;
            border: 1px solid #eee;
        }

        .blinking {
            animation: blinkingText 0.8s infinite;
        }

        @keyframes blinkingText {
            0% {
                color: red;
            }
            49% {
                color: transparent;
            }
            50% {
                color: transparent;
            }
            99% {
                color: transparent;
            }
            100% {
                color: red;
            }
        }
    </style>
@stop
@section('content')
    <div class="app-wrapper">
        {{--        <div class="profile-section-user" id="app-panel">--}}
        {{--@foreach($po as $p)--}}
        {{--<div class="profile-cover-img"><img src="{{asset($p['supplier']->image_path.$p['supplier']->image)}}"--}}
        {{--alt="">--}}
        {{--</div>--}}
        {{--<div class="profile-info-brief p-3"><img class="img-fluid user-profile-avatar"--}}
        {{--src="{{asset($p['supplier']->image_path.$p['supplier']->image)}}"--}}
        {{--alt="">--}}
        {{--<div class="text-center"><h5 class="text-uppercase mb-1">{{$p['supplier']->supplier_name}}</h5>--}}
        {{--<div class="hidden-sm-down ">--}}
        {{--<div>{{$p['supplier']->mobile}}</div>--}}
        {{--<div>NO #{{$p->id}}</div>--}}
        {{--<div>{{$p->created_at}}</div>--}}
        {{--<button class="btn btn-outline-primary btn-sm m-2 btn-rounded " disabled>--}}
        {{--<i class="fa fa-pause px-1" aria-hidden="true"></i>--}}
        {{--PENDING--}}
        {{--</button>--}}
        {{--</div>--}}
        {{--<hr class="m-0 py-2">--}}
        {{--<div class="d-flex justify-content-center flex-wrap p-2">--}}
        {{--<a href="{{$p->id}}/edit" class="btn btn-block btn-success btn-sm m-2 text-white"> RECIEVE</a>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<a href="#" class="app-panel-toggle" data-toggle="class" data-target="#app-panel" data-class="show"><i--}}
        {{--class="fa fa-chevron-right"></i>--}}
        {{--<i class="fa fa-chevron-left"></i>--}}
        {{--</a>--}}
        {{--@endforeach--}}
        {{--        </div>--}}

        @php

            $flag1= !collect(\App\LoadingData::whereLoadingHeaderId($order->id)->get()->pluck('status'))->contains(0);
            $flag2= !collect(\App\LoadingDataVariant::whereLoadingHeaderId($order->id)->get()->pluck('status'))->contains(0);
            $flag3 = ($flag1 && $flag2);
            $flag_min_1_data = \App\LoadingData::whereLoadingHeaderId($order->id)->get()->pluck('qty')->toArray();
            $flag_min_2_data = \App\LoadingDataVariant::whereLoadingHeaderId($order->id)->get()->pluck('weight')->toArray();

            $flag_min_1= !empty($flag_min_1_data)?min($flag_min_1_data)>=0:true;
            $flag_min_2=!empty($flag_min_2_data)?min($flag_min_2_data)>=0:true;

         $ag_id = \SalesOrderManager\Models\SalesOrder::where('id','=',$order->so_id)->first();


        @endphp


        <div class="app-main">
            <div class="app-main-header">
                <h5 class="app-main-heading text-center">ORDERED PRODUCTS</h5>
            </div>

            <div class="scroll-container" id="scroll-container">
                <div class="app-main-content">
                    <div class="">

                        {!! Form::open(['route' => 'loading.store', 'method' => 'post','id'=>'burdenMerge']) !!}
                        <input type="hidden" value="{{$order->id}}" name="id" id="order_id">
                        <input type="hidden" value="{{$ag_id->created_by}}" name="ag_id" id="ag_id">

                        <div class="projects-list">
                            <div class="card-body d-flex  align-items-center p-0">
                                <div class="col-md-2">
                                    <h5 class="title"> product</h5>
                                </div>
                                <div class="col-md-1  text-center">

                                    <h6 class="title">stores available <br>qty </h6>

                                </div>
                                <div class="col-md-1  text-center">

                                    <h6 class="title">price</h6>

                                </div>
                                <div class="col-md-1">

                                    <h6 class="title">qty</h6>

                                </div>
                                <div class="col-md-1 ">

                                    <h6 class="title">extra qty</h6>
                                </div>
                                <div class="col-md-1">

                                    <h6 class="title">available qty</h6>

                                </div>
                                <div class="col-md-2 ">

                                    <h6 class="title">qty</h6>

                                </div>
                                <div class="col-md-1 ">

                                    <h6 class="title">load</h6>

                                </div>
                                <div class="col-md-2 ">

                                    <h6 class="title">reason</h6>

                                </div>

                            </div>
                            <hr class="m-1">

                            @foreach($order->items as $product)

                                <input type="hidden" name="item_id[]" value="{{$product->id}}">


                                <input type="hidden" name="product_id[]" value="{{$product->end_product_id}}">
                                <div class="card-body d-flex  align-items-center p-0">
                                    <div class="col-md-2">
                                        <h5 class="mt-3 {{($product->is_extra_order==1)?'text-danger':null}}">   {{$product->endProduct->name}}</h5>
                                    </div>
                                    <div class="col-md-1  text-center">
                                        <h6>{{$product->endProduct->available_qty}} </h6>
                                    </div>
                                    <div class="col-md-1  text-center">
                                        <input type="hidden" name="unit_price[]" class="form-control"
                                               value="{{$product->endProduct->distributor_price}}">
                                        <h6 class="{{($product->is_extra_order==1)?'text-danger':null}}">{{number_format((float)$product->endProduct->distributor_price, 2, '.', '')}}
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <h6 class="{{($product->is_extra_order==1)?'text-danger':null}}">{{$product->requested_qty}}</h6>
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <h6 class="{{($product->is_extra_order==1)?'text-danger':null}}">{{$product->extra_qty}}</h6>
                                    </div>
                                    <div class="col-md-1 text-center">
                                        <h6 class="{{($product->is_extra_order==1)?'text-danger':null}}">{{(($product->is_extra_order==1)?0:$product->agent_available_qty) }}</h6>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        <input type="number" name="qty[]" class="form-control"
                                               onkeyup="reasonState({{$product->id}},{{($product->extra_qty + $product->requested_qty) - $product->agent_available_qty}})"
                                               id="qty_{{$product->id}}" {{($product->status==1)?'disabled':null}}
                                               value="{{ ($product->status==1)?$product->qty: $product->extra_qty + $product->requested_qty - $product->agent_available_qty}}">
                                    </div>
                                    <div class="col-md-1 text-center">
                                        {{--                                                <button class="btn btn-sm btn-success"--}}
                                        {{--                                                        onclick="loadItem(event,{{$product->id}})">load--}}
                                        {{--                                                </button>--}}
                                        @if ($product->status==0)
                                            <input type="hidden" id="sts_{{$product->id}}" value="load">
                                            <button class="btn btn-sm btn-success load-r " id="btn_{{$product->id}}"
                                                    onclick="loadItem(event,{{$product->id}})">load
                                            </button>
                                            {{--                                                    <a class="btn btn-sm btn-success"--}}
                                            {{--                                                       href="/loading/item/loading?data={{$product->id}}">load</a>--}}
                                        @elseif ($product->status==1)
                                            <input type="hidden" id="sts_{{$product->id}}" value="unload">
                                            <button class="btn btn-sm btn-danger   load-r" id="btn_{{$product->id}}"
                                                    onclick="loadItem(event,{{$product->id}})">unload
                                            </button>
                                            {{--                                                    <a class="btn btn-sm btn-danger"--}}
                                            {{--                                                       href="/loading/item/un-loading?data={{$product->id}}">unload</a>--}}
                                        @endif
                                    </div>
                                    <div class="col-md-2 text-center">
                                        @if (!empty($product->reason))
                                            {!! Form::select('reason[]',[1=>'Out of Stock',2=>'Not Usable Baking',3=>'In Next Order'] ,  $product->reason  , [($product->status==1)?'disabled':null,'placeholder'=>'Select reason if qty < request','class' => "form-control",'id'=>'reason_'.$product->id,'required']) !!}
                                        @else
                                            {!! Form::select('reason[]',[1=>'Out of Stock',2=>'Not Usable Baking',3=>'In Next Order'] ,  $product->reason  , [($product->status==1)?'disabled':null,'placeholder'=>'Select reason if qty < request','class' => "form-control d-none",'id'=>'reason_'.$product->id,'required']) !!}
                                        @endif
                                    </div>

                                </div>
                                <hr class="m-1">

                            @endforeach


                            @if(!empty($order->variants))
                                @foreach($order->variants as $product)


                                    <input type="hidden" name="variant_item_id[]" value="{{$product->id}}">



                                    <input type="hidden" name="variant_id[]" value="{{$product->variant_id}}">
                                    @php $variant = \App\Variant::find($product->variant_id)  @endphp
                                    @php $variant_end_product = \EndProductManager\Models\EndProduct::find($variant->end_product_id) @endphp

                                    <div class="card-body d-flex  align-items-center p-0">
                                        <div class="col-md-2">
                                            <h5 class="mt-3 {{($product->is_extra_order==1)?'text-danger':null}}">   {{$variant->product->name}}</h5>
                                        </div>
                                        <div class="col-md-1  text-center">
                                            <h6 class="{{($product->is_extra_order==1)?'text-danger':null}}">{{$variant_end_product->available_qty}} </h6>
                                        </div>
                                        <div class="col-md-1  text-center">
                                            <input type="hidden" name="variant_unit_price[]" class="form-control"
                                                   value="{{$variant_end_product->distributor_price}}">
                                            <h6 class="{{($product->is_extra_order==1)?'text-danger':null}}">{{number_format((float)$variant_end_product->distributor_price, 2, '.', '')}}
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <h6 class="{{($product->is_extra_order==1)?'text-danger':null}}">{{(!empty($product->qty))?$product->requested_qty:0}}</h6>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <h6 class="{{($product->is_extra_order==1)?'text-danger':null}}">{{($product->extra_qty)?$product->extra_qty:0}}</h6>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <h6 class="{{($product->is_extra_order==1)?'text-danger':null}}">{{(($product->is_extra_order==1)?0:$product->agent_available_qty) }}</h6>
                                        </div>
                                        <div class="col-md-2 text-left">
                                            <span class="text-left"></span>
                                            <input type="number" class="form-control"
                                                   id="v_weight_{{$product->id}}"
                                                   {{($product->status==1)?'disabled':null}}
                                                   value="{{($product->status==1)?$product->weight:($product->extra_qty + $product->requested_qty - $product->agent_available_qty) * $variant->size }}">
                                        </div>

                                        <div class="col-md-2 text-left">
                                            <span class="text-left">{{$variant->variant}}</span>
                                            <input type="number" name="qty[]" class="form-control"
                                                   onkeyup="reasonState({{$product->id}},{{$product->extra_qty + $product->requested_qty - $product->agent_available_qty}})"
                                                   id="vqty_{{$product->id}}" {{($product->status==1)?'disabled':null}}
                                                   value="{{($product->status==1)?$product->qty: $product->extra_qty + $product->requested_qty - $product->agent_available_qty}}">
                                        </div>

                                        <div class="col-md-1 text-center">
                                            {{--                                                <button class="btn btn-sm btn-success"--}}
                                            {{--                                                        onclick="loadItem(event,{{$product->id}})">load--}}
                                            {{--                                                </button>--}}
                                            @if ($product->status==0)
                                                <input type="hidden" id="stsv_{{$product->id}}" value="load">
                                                <button class="btn btn-sm btn-success load-r  "
                                                        id="btnv_{{$product->id}}"
                                                        onclick="loadItemVariant(event,{{$product->id}})">load
                                                </button>
                                                {{--                                                    <a class="btn btn-sm btn-success"--}}
                                                {{--                                                       href="/loading/item/loading?data={{$product->id}}">load</a>--}}
                                            @elseif ($product->status==1)
                                                <input type="hidden" id="stsv_{{$product->id}}" value="unload">
                                                <button class="btn btn-sm btn-danger load-r" id="btnv_{{$product->id}}"
                                                        onclick="loadItemVariant(event,{{$product->id}})">unload
                                                </button>
                                                {{--                                                    <a class="btn btn-sm btn-danger"--}}
                                                {{--                                                       href="/loading/item/un-loading?data={{$product->id}}">unload</a>--}}
                                            @endif
                                        </div>
                                        {{--                                        <div class="col-md-2 text-center">--}}
                                        {{--                                            @if (!empty($product->reason))--}}
                                        {{--                                                {!! Form::select('reason[]',[1=>'Out of Stock',2=>'Not Usable Baking',3=>'In Next Order'] ,  $product->reason  , [($product->status==1)?'disabled':null,'placeholder'=>'Select reason if qty < request','class' => "form-control",'id'=>'reasonv_'.$product->id,'required']) !!}--}}
                                        {{--                                            @else--}}
                                        {{--                                                {!! Form::select('reason[]',[1=>'Out of Stock',2=>'Not Usable Baking',3=>'In Next Order'] ,  $product->reason  , [($product->status==1)?'disabled':null,'placeholder'=>'Select reason if qty < request','class' => "form-control d-none",'id'=>'reasonv_'.$product->id,'required']) !!}--}}
                                        {{--                                            @endif--}}
                                        {{--                                        </div>--}}

                                    </div>
                                    <hr class="m-1">
                                @endforeach
                            @endif



                            @if(!empty($order->rawItems))
                                @foreach($order->rawItems as $product)
                                    <input type="hidden" name="item_id_r[]" value="{{$product->id}}">
                                    <input type="hidden" name="raw_id[]" value="{{$product->raw_material_id}}">
                                    <div class="card-body d-flex  align-items-center p-0 row">
                                        <div class="col-md-3 ml-2">
                                            <div class="text-primary">
                                                <h5 class="mt-3 ">   {{\RawMaterialManager\Models\RawMaterial::withoutGlobalScope('type')->whereRawMaterialId($product->raw_material_id)->first()->name}}</h5>
                                            </div>
                                        </div>

                                        <div class="col-md-1 text-center">
                                            <div class="text-primary">
                                                <h6>{{\RawMaterialManager\Models\RawMaterial::withoutGlobalScope('type')->whereRawMaterialId($product->raw_material_id)->first()->available_qty}}</h6>
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <div class="text-primary">
                                                <h6>{{$product->qty}}</h6>
                                            </div>
                                        </div>
                                        <div class="col-md-3 text-center">
                                            <div class="text-primary">
                                                <input type="number"
                                                       onkeyup="reasonState({{$product->id}},{{$product->qty}})"
                                                       name="qty[]" class="form-control" id="qtyr_{{$product->id}}"
                                                       {{ !($product->status==0)?'readonly':null}}  value="{{$product->qty}}">
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-center px-5">
                                            <div class="text-primary">
                                                <h6 class="blinking text-nowrap font-weight-bold">Did you load ?</h6>
                                                {{--                                                            onclick="loadItemRaw(event,{{$product->id}})">load--}}
                                                {{--                                                    </button>
                                                {{--                                                @if ($product->status==0)--}}
                                                {{--                                                    <input type="hidden" id="stsr_{{$product->id}}" value="load">--}}
                                                {{--                                                    <button class="btn btn-sm btn-success" id="btnr_{{$product->id}}"--}}
                                                {{--                                                            onclick="loadItemRaw(event,{{$product->id}})">load--}}
                                                {{--                                                    </button>--}}
                                                {{--                                                    --}}{{--                                                    <a class="btn btn-sm btn-success"--}}
                                                {{--                                                    --}}{{--                                                       href="/loading/item/loading?data={{$product->id}}">load</a>--}}
                                                {{--                                                @elseif ($product->status==1)--}}
                                                {{--                                                    <input type="hidden" id="stsr_{{$product->id}}" value="unload">--}}
                                                {{--                                                    <button class="btn btn-sm btn-danger" id="btnr_{{$product->id}}"--}}
                                                {{--                                                            onclick="loadItemRaw(event,{{$product->id}})">unload--}}
                                                {{--                                                    </button>--}}
                                                {{--                                                    --}}{{--                                                    <a class="btn btn-sm btn-danger"--}}
                                                {{--                                                    --}}{{--                                                       href="/loading/item/un-loading?data={{$product->id}}">unload</a>--}}
                                                {{--                                                @endif--}}
                                            </div>
                                        </div>
                                        <div class="col-md-3 text-center px-5">
                                            <div class="text-primary">
                                                @if (!empty($product->reason))
                                                    {!! Form::select('reason[]',[1=>'Out of Stock',2=>'Not Usable Baking',3=>'In Next Order'] ,  $product->reason  , [($product->status==1)?'disabled':null,'placeholder'=>'Select reason if qty < request','class' => "form-control",'id'=>'reasonr_'.$product->id.'required']) !!}
                                                @else
                                                    {!! Form::select('reason[]',[1=>'Out of Stock',2=>'Not Usable Baking',3=>'In Next Order'] ,  $product->reason  , [($product->status==1)?'disabled':null,'placeholder'=>'Select reason if qty < request','class' => "form-control d-none",'id'=>'reasonr_'.$product->id.'required']) !!}
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                    <hr class="m-1">
                                @endforeach
                            @endif

                            <div class="col-md-12">
                                @foreach(\RawMaterialManager\Models\RawMaterial::withoutGlobalScopes()->whereType(5)->get() as $crate)
                                @php
                                      $maxid =\App\CrateInquiryAgent::where('crate_id','=',$crate->raw_material_id)
                                                                  ->where('agent_id','=',$ag_id->created_by)
                                                                  ->orderByDesc('id')
                                                                  ->first();
                                 if(empty($maxid)){
                                     $cbal = 0;

                                     }else{
                                      $cbal1 = \App\CrateInquiryAgent::where('id','=',$maxid->id)
                                                                  ->first();
                                      $cbal = $cbal1->end_balance;

                                    }



                                @endphp
                                @if(!empty($crate))
                                    <div class="card-body d-flex  align-items-center p-0 row">
                                        <div class="col-md-4">
                                            <div class="text-primary">
                                                <h5 class="mt-3"> {{$crate->name}}</h5>

                                            </div>
                                        </div>

                                        <div class="col-md-3 text-center">
                                            <div class="text-primary">
                                                <input type="hidden" name="crate[]" value="{{$crate->raw_material_id}}">
                                                <input type="number" name="crate_qty[]" class="form-control" value="0" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3 text-center">
                                            <div class="text-primary">
                                                <h6>Balance {{$crate->name}} = {{$cbal}} </h6>
                                            </div>

                                        </div>
                                    </div>
                                @endif
                                @endforeach
                            </div>
                            <hr>


                            <div class="projects-list">
                                <div class="col-md-12 col-sm-12 my-4">
                                    <textarea name="description" class="form-control" rows="5"
                                              placeholder="Enter Description"></textarea>
                                </div>
                            </div>

                            <div id="appendDiv"></div>

                            {{--                            <div class="col-md-12">--}}
                            {{--                                <div class="row">--}}
                            {{--                                    @if ($order->salesOrder->salesOrderVariants)--}}
                            {{--                                        <div class="row">--}}
                            {{--                                            @foreach($order->salesOrder->salesOrderVariants as $variant)--}}
                            {{--                                                <div class="col-md-2">#</div>--}}
                            {{--                                                <div class="col-md-2"> {{$variant->variant->product->name}}</div>--}}
                            {{--                                                <div class="col-md-2">{{$variant->variant->variant}}</div>--}}
                            {{--                                                <div class="col-md-2">QTY : {{$variant->qty}}</div>--}}
                            {{--                                                <div class="col-md-4">Available QTY--}}
                            {{--                                                    : {{$variant->available_qty}}</div>--}}
                            {{--                                            @endforeach--}}
                            {{--                                        </div>--}}
                            {{--                                    @endif--}}
                            {{--                                </div>--}}


                            {{--                            {{dd(collect(\App\LoadingDataVariant::whereLoadingHeaderId($order->id)->get()->pluck('status'))->contains(0))}}--}}

                            @if ($flag1 && $flag2 && $flag_min_1 && $flag_min_2)

                                @php
                                    $crapp = \App\CratesTransaction::whereType('return')->where('crates_approve', '=', Null)->where('agent_id','=',$ag_id->created_by)->first();

                                @endphp

                                <div class="mt-5 mb-5" id="btnSection">
                                    <div class="row">
                                        @if($discountMode == 2)
                                            <div class="col-md-8">
                                                @if(empty($crapp))
                                                    @if($order->status == 1)
                                                <button type="submit" id="btnSave" name="submit"
                                                        class="btn btn-success btn-lg btn-block"
                                                        value="stream"> Load Now And Print
                                                </button>
                                                    @endif
                                                @else
                                                    <a href="{{ route('crates-return.allcrates') }}" target="_blank" id="btnCrateApprove"
                                                       class="btn btn-warning text-white btn-lg btn-block"> Approve Crates First</a>

                                                @endif
                                            </div>
                                            <div class="col-md-4">
                                                @if($order->status == 1)
                                                <a onclick="removeDisabledStatus()" id="btnEdit"
                                                   class="btn btn-danger text-white btn-lg btn-block"
                                                   value="stream"> Edit Loading
                                                </a>

                                                @endif
                                            </div>
                                        @endif
                                        @if($discountMode == 1)
                                            <div class="col-md-8">
                                                @if(empty($crapp))
                                                    @if(($order->status == 1))
                                                <button type="submit" name="loadDiscount"
                                                        class="btn btn-success btn-lg btn-block" id="loadWithDis"
                                                        value="1">Load with Discount
                                                </button>
                                                    @endif
                                                @else
                                                    <a href="{{ route('crates-return.allcrates') }}" target="_blank" id="btnCrateApprove"
                                                       class="btn btn-warning text-white btn-lg btn-block"> Approve Crates First</a>
                                                @endif

                                            </div>
                                            <div class="col-md-4">
                                                @if($order->status == 1)
                                                <a onclick="removeDisabledStatus()" id="btnEdit"
                                                   class="btn btn-danger text-white btn-lg btn-block"
                                                   value="stream"> Edit Loading
                                                </a>
                                                 @endif

                                            </div>
                                    @endif
                                    <!-- <a href="{{ route('loading.discount', $order->id) }}" class="" id="loadWithDis">Load with Discount</a> -->

                                    </div>
                                </div>
                                {{--                            @else--}}
                                {{--                                <div class="mt-5 mb-5">--}}
                                {{--                                    <div class="row">--}}
                                {{--                                          @if($discountMode == 2)--}}
                                {{--                                          <div class="col-md-12">--}}
                                {{--                                            <button type="submit" id="btnSave" name="submit"--}}
                                {{--                                                    class="btn btn-success btn-lg btn-block"--}}
                                {{--                                                    value="stream"> Load Now And Print--}}
                                {{--                                            </button>--}}
                                {{--                                          </div>--}}
                                {{--                                          @endif--}}

                                {{--                                          @if($discountMode == 1)--}}
                                {{--                                          <div class="col-md-12">--}}
                                {{--                                            <!-- <button type="submit" id="btnSave" name="submit"--}}
                                {{--                                                    class="btn btn-success btn-lg btn-block"--}}
                                {{--                                                    value="stream"> Load With Discount--}}
                                {{--                                            </button> -->--}}
                                {{--                                            <button type="submit"   name="loadDiscount"--}}
                                {{--                                                    class="btn btn-info btn-lg btn-block" value="1"--}}
                                {{--                                                    id="loadWithDis"> Load with Discount--}}
                                {{--                                            </button>--}}
                                {{--                                            </div>--}}
                                {{--                                          @endif--}}
                                {{--                                        <!-- <a href="{{ route('loading.discount', $order->id) }}" class="btn btn-success btn-lg btn-block d-none" id="loadWithDis">Load with Discount</a> -->--}}

                                {{--                                    </div>--}}
                                {{--                                </div>--}}
                            @endif
                            @if($order->status == 1)
                            <a  class="btn btn-block btn-info" href="{{route('order-list.preview2',[$order->id])}}">Preview</a>
                            @else
                            <a  class="btn btn-block btn-secondary" href="{{route('order-list.preview2',[$order->id])}}">Preview [ Already Loaded ]</a>

                            @endif
                        </div>
                        {!!Form::close()!!}
                    </div>
                </div>

            </div>
        </div>
    </div>

    <style>
        .title {
            white-space: nowrap;
            font-size: 12px;
            text-transform: capitalize;
        }
    </style>

@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-validation/dist/jquery.validate.min.js')}}"></script>
    <script>


        $('#burdenMerge').validate();

        function loadItemVariant(e, product) {
            // if (!$('#burdenMerge').valid()) {
            //     swal("Warning!", "Please Select Reason", "warning");
            //     return false;
            // }

            e.preventDefault();
            let qty_element = '#vqty_' + product;
            if ($(qty_element).val() == '') {
                swal("Warning!", "Please Enter QTY", "warning");
                return false;
            }
            let weight_element = '#v_weight_' + product;
            if ($(weight_element).val() == '') {
                swal("Warning!", "Please Enter weight", "warning");
                return false;
            }
            let qty = $(qty_element).val();
            let weight = $(weight_element).val();
            let url = '';
            let button_el = "#btnv_" + product;
            let status = "#stsv_" + product;
            let reason = '#reasonv_' + product;
            let reason_val = $(reason).val();
            let order = $('#order_id').val();
            let agent = $('#ag_id').val()

            command = $(status).val();
            if (command == 'load') {
                url = '/loading/item/loading/variant?data=' + product + '&qty=' + qty + '&weight=' + weight + '&reason=' + reason_val + '&agent=' + agent + '&order=' + order;
                $.ajax({
                    type: "get",
                    url: url,
                    success: function (response) {
                        if (response == 'true') {
                            $(button_el).removeClass('btn-success');
                            $(button_el).addClass('btn-danger');
                            $(button_el).html("unload");
                            $(status).val('unload');
                            $(qty_element).attr('disabled', true);
                            $(weight_element).attr('disabled', true);
                            $(reason).attr('disabled', true);
                            // Command: toastr["info"]("Item Checked")
                        } else {
                            Command: toastr["error"]("Insufficient Items")
                        }
                    }
                });
            }
            if (command == 'unload') {
                url = '/loading/item/un-loading/variant?data=' + product + '&qty=' + qty + '&weight=' + weight + '&reason=' + reason_val + '&agent=' + agent + '&order=' + order;

                $.ajax({
                    type: "get",
                    url: url,
                    success: function (response) {
                        // console.log(response);
                        if (response == 'true') {
                            $(button_el).removeClass('btn-danger');
                            $(button_el).addClass('btn-success');
                            $(button_el).html("load");
                            $(status).val('load')
                            $(reason).val('');
                            $(qty_element).removeAttr('disabled');
                            $(weight_element).removeAttr('disabled');
                            $(reason).removeAttr('disabled');
                        }
                    }
                });
            }
            // checkOrderStatus(order);
        };

        function loadItem(e, product) {
            if (!$('#burdenMerge').valid()) {
                swal("Warning!", "Please Select Reason", "warning");
                return false;
            }
            e.preventDefault();
            let qty_element = '#qty_' + product;
            if ($(qty_element).val() == '') {
                swal("Warning!", "Please Enter QTY", "warning");
                return false;
            }
            let qty = $(qty_element).val();
            let url = '';
            let button_el = "#btn_" + product;
            let status = "#sts_" + product;
            let reason = '#reason_' + product;
            let reason_val = $(reason).val();
            let order = $('#order_id').val();
            let agent = $('#ag_id').val()

            command = $(status).val();
            if (command == 'load') {
                url = '/loading/item/loading?data=' + product + '&qty=' + qty + '&reason=' + reason_val + '&agent=' + agent + '&order=' + order;
                $.ajax({
                    type: "get",
                    url: url,
                    success: function (response) {
                        if (response == 'true') {
                            $(button_el).removeClass('btn-success');
                            $(button_el).addClass('btn-danger');
                            $(button_el).html("unload");
                            $(status).val('unload');
                            $(qty_element).attr('disabled', true);
                            $(reason).attr('disabled', true);
                            // Command: toastr["info"]("Item Checked")
                        } else {
                            Command: toastr["error"]("Insufficient Items")
                        }
                    }
                });
            }
            if (command == 'unload') {
                url = '/loading/item/un-loading?data=' + product + '&qty=' + qty + '&reason=' + reason_val + '&order=' + order + '&agent=' + agent;
                $.ajax({
                    type: "get",
                    url: url,
                    success: function (response) {
                        if (response == 'true') {
                            $(button_el).removeClass('btn-danger');
                            $(button_el).addClass('btn-success');
                            $(button_el).html("load");
                            $(status).val('load')
                            $(reason).val('');
                            $(qty_element).removeAttr('disabled');
                            $(reason).removeAttr('disabled');
                        }
                    }
                });
            }
            // checkOrderStatus(order);
        };

        function loadItemRaw(e, product) {
            e.preventDefault();
            let qty_element = '#qtyr_' + product;
            if ($(qty_element).val() == '') {
                swal("Warning!", "Please Enter QTY", "warning");
                return false;
            }
            let qty = $(qty_element).val();
            let url = '';
            let button_el = "#btnr_" + product;
            let status = "#stsr_" + product;
            let reason = '#reasonr_' + product;
            let reason_val = $(reason).val();
            let order = $('#order_id').val()

            command = $(status).val();
            if (command == 'load') {
                url = '/loading/item/loading/raw?data=' + product + '&qty=' + qty + '&reason=' + reason_val;
                $.ajax({
                    type: "get",
                    url: url,
                    success: function (response) {
                        if (response == 'true') {
                            $(button_el).removeClass('btn-success');
                            $(button_el).addClass('btn-danger');
                            $(button_el).html("unload");
                            $(status).val('unload');
                            $(qty_element).attr('disabled', true);
                            $(reason).attr('disabled', true);
                            Command: toastr["info"]("Item Checked")
                        } else {
                            Command: toastr["error"]("Insufficient Items")
                        }
                    }
                });
            }
            if (command == 'unload') {
                url = '/loading/item/un-loading/raw?data=' + product + '&qty=' + qty + '&reason=' + reason_val + '&agent=' + agent;
                $(button_el).removeClass('btn-danger');
                $(button_el).addClass('btn-success');
                $(button_el).html("load");
                $(status).val('load')
                $(reason).val('');
                $(qty_element).removeAttr('disabled');
                $(reason).removeAttr('disabled');
                $.ajax({
                    type: "get",
                    url: url,
                    success: function (response) {
                        // console.log(response);
                    }
                });
            }
        };

        function reasonState(product, state_value) {
            let qty_element = '#qty_' + product;
            let qty = $(qty_element).val();
            let reason = '#reason_' + product;
            if (qty != state_value) {
                $(reason).removeClass('d-none');
            } else {
                $(reason).addClass('d-none');
            }
        }

        function reasonStateRaw(product, state_value) {
            let qty_element = '#qtyr_' + product;
            let qty = $(qty_element).val();
            let reason = '#reasonr_' + product;
            if (qty != state_value) {
                $(reason).removeClass('d-none');
            } else {
                $(reason).addClass('d-none');
            }
        }

        // function checkOrderStatus(order) {
        //     let url = '/item/loading/check/' + order;
        //     $.ajax({
        //         type: "get",
        //         url: url,
        //         success: function (response) {
        //             // console.log(response)
        //             if (response == 'true') {
        //                 $('#btnSave').removeClass('d-none');
        //                 $('#loadWithDis').removeClass('d-none');
        //             } else {
        //                 $('#btnSave').addClass('d-none');
        //                 $('#loadWithDis').addClass('d-none');
        //             }
        //         }
        //     });
        // }

        $(document).keypress(function (e) {
            // alert(e.keyCode);
            if (e.keyCode == 78 || e.keyCode == 110) {
                $("#btnNew").click();
            }
            if (e.keyCode == 76 || e.keyCode == 108) {
                $("#btnSave").click();
            }
            if (e.keyCode == 69 || e.keyCode == 101) {
                $("#endProduct").focus();
            }
            if (e.keyCode == 67 || e.keyCode == 99) {
                $("#btnClose").click();
            }
        });

        $("#myModal").on('shown', function () {
            $("#endProduct").focus();
        });

        $("#endProduct").change(function () {
            $("#qty").focus();
        });

        $("#qty").keyup(function (event) {
            if (event.keyCode === 13) {
                $("#btnAdd").click();
            }
        });

        $('#myModalt').on('shown', function () {
            alert(1)
        })

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        // async function addProduct() {
        //     let product = document.getElementById("endProduct");
        //     let qty = $('#qty').val();
        //     let product_id = $(product).children("option:selected").val();
        //     let product_name = $(product).children("option:selected").text();
        //     let price = 0;
        //     await $.ajax({
        //         type: "post",
        //         data: {id: product_id},
        //         url: '/end_product/get/endpro',
        //         success: function (response) {
        //             price = response.distributor_price;
        //         }
        //     });
        //     var $newdiv1 = $("<input type=\"hidden\" name=\"product_id[]\" value=\"" + product_id + "\">" +
        //         " <div class=\"card-body d-flex  align-items-center p-0\">\n" +
        //         "                                    <div class=\"d-flex mr-auto\">\n" +
        //         "                                        <div>\n" +
        //         "                                            <div class=\"avatar avatar text-white avatar-md project-icon bg-primary\">\n" +
        //         "                                            </div>\n" +
        //         "                                        </div>\n" +
        //         "                                        <h5 class=\"mt-3 mx-3\"> " + product_name + "</h5>\n" +
        //         "                                    </div>\n" +
        //         "                                    <div class=\"d-flex activity-counters justify-content-between\">\n" +
        //         "<input type=\"hidden\" name=\"unit_price[]\" class=\"form-control\"\n" +
        //         "                                                       value=\"" + parseFloat(price).toFixed(2) + "\">" +
        //         "                                        <div class=\"text-center px-5\">\n" +
        //         "                                            <div class=\"text-primary\">\n" +
        //         "                                                <h6>" + parseFloat(price).toFixed(2) + " LKR</h6>\n" +
        //         "                                            </div>\n" +
        //         "                                        </div>\n" +
        //         "<div class=\"text-center px-5\">\n" +
        //         "                                            <div class=\"text-primary\">\n" +
        //         "                                                <h6>" + qty + "</h6>\n" +
        //         "                                            </div>\n" +
        //         "                                        </div>\n" +
        //         "                                        <div class=\"text-center px-5\">\n" +
        //         "                                            <div class=\"text-primary\">\n" +
        //         "                                                <input type=\"number\" name=\"qty[]\" class=\"form-control\"\n" +
        //         "                                                       value=\"" + qty + "\">\n" +
        //         "                                            </div>\n" +
        //         "                                        </div>\n" +
        //         "                                    </div>\n" +
        //         "                                </div> " +
        //         " <hr class=\"m-1\">"),
        //         newdiv2 = document.createElement("div"),
        //         existingdiv1 = document.getElementById("appendDiv");
        //
        //     $(existingdiv1).append($newdiv1, [newdiv2, existingdiv1]);
        //
        // }

        // $('.delete').click(function (e) {
        //     e.preventDefault();
        //     id = $(this).attr("value");
        //     confirmAlert(id);
        //
        // });


        // $(document).keypress(function ( e ) {
        //     // You may replace `c` with whatever key you want
        //     if ((e.metaKey || e.ctrlKey) && ( String.fromCharCode(e.which).toLowerCase() === 'c') ) {
        //         console.log( "You pressed CTRL + C" );
        //     }
        // });

        // function confirmAlert(id) {
        //     swal({
        //             title: "Are you sure?",
        //             text: "Your will not be able to recover this imaginary file!",
        //             type: "warning",
        //             showCancelButton: true,
        //             confirmButtonColor: '#3085d6',
        //             confirmButtonText: "Yes, delete it!",
        //             closeOnConfirm: false
        //         },
        //         function () {
        //             $.ajax({
        //                 type: "delete",
        //                 url: id,
        //                 success: function (response) {
        //                     if (response == 'true') {
        //                         swal("Deleted!", "Deleted successfully", "success");
        //                         setTimeout(location.reload.bind(location), 900);
        //                     } else {
        //                         swal("Error!", "Something went wrong", "error");
        //                     }
        //                 }
        //             });
        //
        //         }
        //     );
        //
        // }

        function closeOpenedWindow(){
            closeOpenedWindow();
        }

        function removeDisabledStatus() {
            $('#btnSection').addClass('d-none');
            $('.load-r').prop('disabled', false);
        }

        @if ($flag1 && $flag2 && $flag_min_1 && $flag_min_2)
        $('.load-r').prop('disabled', true);
        @endif
    </script>
@stop
