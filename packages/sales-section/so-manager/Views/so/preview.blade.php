@extends('layouts.back.master')@section('title','Sales order Preview')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.css')}}">


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .hide {
            display: none;
        }

        .app-main-content {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .portlet-placeholder {
            border: 1px dotted black;
            margin: 0 1em 1em 0;
            height: 100%;
        }

        #projects-task-modal .modal-bg {
            padding: 50px;
        }

        #projects-task-modal .modal-content {
            box-shadow: none;
            border: none
        }

        #projects-task-modal .close {
            border: none;
            outline: none;
            box-shadow: none;
            position: absolute;
            right: 1rem;
            top: 1rem;
            width: 25px;
            height: 25px;
            line-height: 25px;
            text-align: center;
            font-size: 1.13rem;
            border-radius: 500px;
            border: 1px solid #868e96
        }

        #floating-button {
            width: 55px;
            height: 55px;
            border-radius: 50%;
            background: #db4437;
            position: fixed;
            top: 13rem;
            right: 90px;
            cursor: pointer;
            box-shadow: 0px 2px 5px #666;
            z-index: 2
        }

        .plus {
            color: white;
            position: absolute;
            top: 0;
            display: block;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding: 0;
            margin: 0;
            line-height: 55px;
            font-size: 38px;
            font-family: 'Roboto';
            font-weight: 300;
            animation: plus-out 0.3s;
            transition: all 0.3s;
        }

        #container {
            position: relative;
            padding: 0px;
            width: 100%;
            height: 200px;
            overflow: auto;
        }

        #container .content {
            width: 100%;
            height: 100%;
        }

    </style>
@stop

@section('deniedBack')@endsection
@section('content')
    <div class="app-wrapper">
        <div class="col-md-12" id="app-panel">

            <div class="col-md-12">
                <div class="app-main-header">
                    <div class="row">

                        {{--                        <div class="col-md-6">--}}
                        {{--                            <div class="row">--}}
                        {{--                                <div class="col-md-3">DATE :</div>--}}
                        {{--                                <div class="col-md-9">{{$date}}</div>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        {{--                        <div class="col-md-6">--}}
                        {{--                            <div class="row">--}}
                        {{--                                <div class="col-md-3">CATEGORY :</div>--}}
                        {{--                                <div class="col-md-9"> {{$bf->category->name}}</div>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        {{--                        <div class="col-md-6">--}}
                        {{--                            <div class="row">--}}
                        {{--                                <div class="col-md-3">DAY :</div>--}}
                        {{--                                <div class="col-md-9">{{$bf->day}}</div>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        {{--                        <div class="col-md-6">--}}
                        {{--                            <div class="row">--}}
                        {{--                                <div class="col-md-3">TIME :</div>--}}
                        {{--                                <div class="col-md-9">{{$bf->time}}</div>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}

                    </div>
                </div>


                <div class="app-main-content">
                    <div class="container">
                        @php $orderQtyError @endphp
                        <div class="projects-list">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Product</th>
                                    <th class="text-center">Extra Order</th>
                                    <th class="text-center">Ordered Quantity</th>
                                    <th class="text-center">Total Quantity</th>
                                    <th class="text-right">Amount</th>
                                </tr>
                                </thead>
                                <tbody>

                                @php
                                    $total =0;
                                @endphp
                                @foreach($so->salesOrderContent as $item)
                                    @if ($item->qty - $item->available_qty != 0 || $item->extra_qty != 0)
                                        <tr>
                                            @php
                                                $endProduct =null;
                                                    $order_qty = $item->qty - $item->available_qty;
                                                   $endProduct =\EndProductManager\Models\EndProduct::find($item->end_product_end_product_id);
                                                       $extra_amount = $item->extra_qty *$endProduct->distributor_price;
                                                    $amount = $order_qty*$endProduct->distributor_price;
                                                    $amount =$amount + $extra_amount;
                                                       $total = $total+ $extra_amount + $order_qty*$endProduct->distributor_price ;

                                            @endphp
                                            @php
                                                $itemErr=0;
                                                    if ($order_qty < 0){
            $orderQtyError = 1;
             $itemErr=1;
        }
                                            @endphp
                                            <td class="{{($itemErr)?'text-danger':null}}">{{$endProduct->name}} <span class="font-weight-bold">{{' ('.$endProduct->distributor_price.')'}}</span> </td>
                                            <td class="text-center {{($itemErr)?'text-danger':null}}">{{$item->extra_qty}}</td>
                                            <td class="text-center {{($itemErr)?'text-danger':null}}">{{$order_qty}}</td>
                                            <td class="text-center {{($itemErr)?'text-danger':null}}">{{$item->extra_qty + $order_qty}}</td>

                                            @if($role = Sentinel::check()->roles[0]->slug=='sales-ref')
                                            <td class=" {{($itemErr)?'text-danger':null}}"
                                                style="text-align: right"></td>
                                            @else
                                                <td class=" {{($itemErr)?'text-danger':null}}"
                                                    style="text-align: right">{{number_format($amount,2)}}</td>
                                            @endif
                                        </tr>
                                    @endif
                                @endforeach
                                @foreach($so->salesOrderVariants as $item)
                                    @if ($item->qty - $item->available_qty != 0 || $item->extra_qty != 0)
                                        <tr>
                                            @php
                                                $endProduct =null;
                                                    $order_qty = $item->qty - $item->available_qty;
                                                   $variant =\App\Variant::find($item->variant_id);
                                                       $extra_amount = $item->extra_qty * ($variant->size * $variant->product->distributor_price);
                                                       $amount = $order_qty*  ($variant->size * $variant->product->distributor_price);
                                                       $amount =$amount +$extra_amount;
                                                       //$total = $total+ $order_qty* ($variant->size * $variant->product->distributor_price);
                                                       $total = $total + $amount;
                                            @endphp
                                            @php
                                                $itemErr=0;
                                                   if ($order_qty < 0 || $item->extra_qty < 0 ){
           $orderQtyError = 1;
            $itemErr=1;
       }

                                            @endphp

                                            <td class=" {{($itemErr)?'text-danger':null}} ">{{$variant->product->name.' '.$variant->variant}}<span class="font-weight-bold">{{' ('.$variant->product->distributor_price.')'}}</span> </td>
                                            <td class=" {{($itemErr)?'text-danger':null}} text-center">{{$item->extra_qty}}</td>
                                            <td class=" {{($itemErr)?'text-danger':null}} text-center">{{$order_qty}}</td>
                                            <td class=" {{($itemErr)?'text-danger':null}} text-center">{{$item->extra_qty + $order_qty}}</td>
                                            @if($role = Sentinel::check()->roles[0]->slug=='sales-ref')
                                                <td class=" {{($itemErr)?'text-danger':null}}"
                                                    style="text-align: right"></td>
                                            @else
                                            <td class=" {{($itemErr)?'text-danger':null}} "
                                                style="text-align: right">{{number_format($amount,2)}}</td>
                                            @endif

                                        </tr>
                                    @endif
                                @endforeach
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>TOTAL</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    @if($role = Sentinel::check()->roles[0]->slug=='sales-ref')
                                        <td style="text-align: right"></td>
                                    @else
                                    <td style="text-align: right">{{number_format($total,2)}}</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                @foreach($so->salesOrderRawContent as $item)
                                    @if ($item->qty >0)
                                        <tr>
                                            <td>{{\RawMaterialManager\Models\RawMaterial::withoutGlobalScope('type')->where('raw_material_id','=',$item->raw_material_id)->first()->name}}</td>
                                            <td></td>
                                            <td></td>
                                            <td>{{$item->qty}}</td>
                                        </tr>
                                    @endif
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                        {{----}}


                        <div class="projects-list">
                            <div class="col-md-12 col-sm-12 my-4">
                                <textarea disabled name="description" class="form-control" rows="5"
                                          placeholder="Enter Description">{{$so->description}} </textarea>
                            </div>
                        </div>
                        {{ Form::open(array('url' => route('so.confOrder')))}}
                        <input type="hidden" name="order" value="{{$so->id}}">
                        <div class="row">
                            @if(!isset($orderQtyError) || $orderQtyError ==0)
                                {{--                                <div class="col-md-6">--}}
                                {{--                                    <input type="submit" class="btn btn-danger btn-lg btn-block mb-5" name="submit"--}}
                                {{--                                           value="Cancel Order">--}}
                                {{--                                </div>--}}

                                <div class="col-md-12">
                                    <input type="submit" onclick="this.form.submit();this.disabled = true;" class="btn btn-success btn-lg btn-block mb-5" name="submit"
                                           value="Confirm Order">
                                </div>

                            @else
                                <div class="col-md-12">
                                    <h3 class="text-danger text-center text-capitalize"> Ordered Quantity Cannot Be
                                        Minus ! </h3>
                                </div>
                            @endif
                        </div>
                        {!!Form::close()!!}
                    </div>
                </div>


            </div>
        </div>

        @stop
        @section('js')
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
            <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
            <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
            <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>

            <script>


                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });

                $('.delete').click(function (e) {
                    e.preventDefault();
                    id = $(this).attr("value");
                    confirmAlert(id);

                });

                function confirmAlert(id) {
                    swal({
                            title: "Are you sure?",
                            text: "Your will not be able to recover this imaginary file!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: "Yes, delete it!",
                            closeOnConfirm: false
                        },
                        function () {
                            $.ajax({
                                type: "delete",
                                url: id,
                                success: function (response) {
                                    if (response == 'true') {
                                        swal("Deleted!", "Deleted successfully", "success");
                                        setTimeout(location.reload.bind(location), 900);
                                    } else {
                                        swal("Error!", "Something went wrong", "error");
                                    }
                                }
                            });

                        }
                    );

                }

                $(document).ready(function () {
                    $("#unit").hide();
                    $('#pj-task-1').change(function () {
                        if (this.checked)
                            $("#unit").show();
                        else
                            $("#unit").hide();

                    });
                });


                $("#add").click(function () {

                    var a = $("#single option:selected").val();
                    var qty = $('#qty').val();

                    var o = validateEnd();
                    if (o) {
                        $.ajax({
                            type: "post",
                            url: '../end_product/get/endpro',
                            data: {id: a},

                            success: function (response) {
                                $(".end-product-list").append(' <div class="adiv panel-item media my-2 px-2"  >' +
                                    '<div class="d-flex mr-auto">' +
                                    '<div>' +
                                    '<div class="avatar avatar text-white avatar-md project-icon bg-primary">' +
                                    '<img class="card-img-top" src="" alt="">' +
                                    '</div>' +
                                    '</div>' +
                                    '<h5 class="mt-3 mx-3" >' + response['name'] + '</h5>' +
                                    '<input type="hidden" name="extra_end_product[]" value=" ' + response['id'] + ' "/>' +
                                    '<input type="hidden" name="extra_qty[]" value=" ' + qty + ' "/>' +
                                    '<input type="hidden" name="extra_unit_price[]" value=" ' + response['selling_price'] + ' "/>' +
                                    '</div>' +
                                    '<div class="d-flex activity-counters justify-content-between">' +
                                    '<div class="text-center px-3">' +
                                    '<div class="text-primary"><h4 >' + qty + '</h4>' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="text-center px-3">' +
                                    '<div class="text-primary"><h4 >' + response['selling_price'] + ' LKR </h4>' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="text-center px-3">' +
                                    '<div class="text-primary"><h4 >' + parseFloat(qty) * parseFloat(response['selling_price']) + 'LKR </h4>' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="section-2">' +
                                    '<div onclick="removediv(this)" class="delete btn btn-light btn-sm ml-2" value=" ' + response['id'] + ' " data-toggle="tooltip" data-placement="left" title="DELETE"> ' +
                                    '<i class="zmdi zmdi-close"></i>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div> ' +
                                    '</div>');
                            }

//                    success: function (response) {
//
//                        $(".abc").append('<div class="panel-item media my-2">' +
//                            '<div class="avatar avatar-sm project-icon bg-primary"data-plugin="firstLitter"data-target="#raw-4">' +
//                            '</div>' +
//                            '<div class="media-body">' +
//                            '<h6 class="media-heading my-1">' +
//                            '<h6 id="raw-4">' + response['name'] + '</h6></h6>' +
//                            '<div class="d-flex activity-counters justify-content-between">' +
//                           '<div class="text-center px-5">' +
//                           '<div class="text-primary" style="background-color: #00bcd4"><h4 >' + qty + '</h4>' +
//                           '</div>' +
//                           '</div>' +
//                           '<div class="section-2">' +
//                            '<div onclick="removediv(this)" class="delete btn btn-light btn-sm ml-2" value=" ' + response['id'] + ' " data-toggle="tooltip" data-placement="left" title="DELETE"> ' +
//                           '<i class="zmdi zmdi-close"></i>' +
//                            '</div>' +
//                           '</div>' +
//                            '</div> ' +
//                            '</div>');
//                    }
                        });

                        reset();
                    }


                });

                $("#raw-add").click(function () {

                    var a = $("#raw-material option:selected").val();
                    var qty = $('#r-qty').val();

                    var o = validateRaw();
                    if (o) {
                        $.ajax({
                            type: "post",
                            url: '../raw_material/get/raw',
                            data: {id: a},

                            success: function (response) {
                                $(".raw-list").append(' <div class="adiv panel-item media my-2 px-2"  >' +
                                    '<div class="d-flex mr-auto">' +
                                    '<div>' +
                                    '<div class="avatar avatar text-white avatar-md project-icon bg-primary">' +
                                    '<img class="card-img-top" src="" alt="">' +
                                    '</div>' +
                                    '</div>' +
                                    '<h5 class="mt-3 mx-3" >' + response['name'] + '</h5>' +
                                    '<input type="hidden" name="raw_materials[]" value=" ' + response['raw_material_id'] + ' "/>' +
                                    '<input type="hidden" name="r_qty[]" value=" ' + qty + ' "/>' +
                                    '</div>' +
                                    '<div class="d-flex activity-counters justify-content-between">' +
                                    '<div class="text-center px-3">' +
                                    '<div class="text-primary"><h4 >' + qty + '</h4>' +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="section-2">' +
                                    '<div onclick="removediv(this)" class="delete btn btn-light btn-sm ml-2" value=" ' + response['raw_material_id'] + ' " data-toggle="tooltip" data-placement="left" title="DELETE"> ' +
                                    '<i class="zmdi zmdi-close"></i>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div> ' +
                                    '</div>');
                            }

//                    success: function (response) {
//
//                        $(".abc").append('<div class="panel-item media my-2">' +
//                            '<div class="avatar avatar-sm project-icon bg-primary"data-plugin="firstLitter"data-target="#raw-4">' +
//                            '</div>' +
//                            '<div class="media-body">' +
//                            '<h6 class="media-heading my-1">' +
//                            '<h6 id="raw-4">' + response['name'] + '</h6></h6>' +
//                            '<div class="d-flex activity-counters justify-content-between">' +
//                           '<div class="text-center px-5">' +
//                           '<div class="text-primary" style="background-color: #00bcd4"><h4 >' + qty + '</h4>' +
//                           '</div>' +
//                           '</div>' +
//                           '<div class="section-2">' +
//                            '<div onclick="removediv(this)" class="delete btn btn-light btn-sm ml-2" value=" ' + response['id'] + ' " data-toggle="tooltip" data-placement="left" title="DELETE"> ' +
//                           '<i class="zmdi zmdi-close"></i>' +
//                            '</div>' +
//                           '</div>' +
//                            '</div> ' +
//                            '</div>');
//                    }
                        });

                        reset();
                    }


                });

                function removediv(e) {
                    $(e).closest(".adiv").remove();

                }

                function validateEnd() {
                    var ing = document.getElementById('single').value;
                    var qty = document.getElementById('qty').value;

                    if (ing == "") {
                        swal('Select End Product')
                        return false;
                    }

                    if (qty == "") {
                        swal('Enter Quantity')
                        return false;
                    } else {
                        return true;
                    }

                }

                function validateRaw() {
                    var ing = document.getElementById('raw-material').value;
                    var qty = document.getElementById('r-qty').value;

                    if (ing == "") {
                        swal('Select Outelet Item');
                        return false;
                    }

                    if (qty == "") {
                        swal('Enter Quantity');
                        return false;
                    } else {
                        return true;
                    }

                }

                function reset() {

                    document.getElementById("single").value = "";
                    document.getElementById("qty").value = "";
                }

            </script>
@stop
