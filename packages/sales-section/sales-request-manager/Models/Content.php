<?php

namespace SalesRequestManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Content extends Model
{
    use SoftDeletes;

    protected $table = 'sales_order_data';

    protected $dates = ['deleted_at'];

    public function salesOrder(){
        return $this->belongsTo('SalesOrderManager\Models\SalesOrder');
    }

    public function endProducts(){
        return $this->hasOne('EndProductManager\Models\EndProduct','end_product_id','id');
    }
}
