<?php

namespace SalesRequestManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalesRequest extends Model
{
    use SoftDeletes;

    protected $table = 'sales_order_header';

    protected $dates = ['deleted_at'];

    public function salesRep()
    {
        return $this->belongsTo('SalesRequestManager\Models\SalesRep');
    }

    public function salesOrderContent(){
        return $this->hasMany(Content::class,'sales_order_header_id');
    }

}
