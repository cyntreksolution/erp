@extends('layouts.back.master')@section('title','Sales Request | List')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
    {{--    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/css/dataTables.checkboxes.css" rel="stylesheet"/>--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }

        .abc {
            width: 1000px;
            height: 1000px;
            /* background-color: black; */
            z-index: 8000;

        }

        .sweet-alert {
            z-index: 7999 !important;
        }

        .sweet-overlay {
            z-index: 7998 !important;
        }

    </style>
@stop

@section('content')

    <div id="wait" class="d-none abc" style=""><img src='{{asset('assets/img/4V0b.gif')}}' width="64" height="64"/><br>Loading..
    </div>

    <div class="app-main-content mb-2">
        <div class="row mx-2">
            <div class="col-md-2">
                {!! Form::select('agent',$agents , null , ['class' => 'form-control','placeholder'=>'Select Agent','id'=>'agent']) !!}
            </div>
            <div class="col-md-2">
                @if(Auth::user()->hasRole(['Owner','Sales Manager','Super Admin']))
                    {!! Form::select('category',$cat , null , ['class' => 'form-control','placeholder'=>'Select Category','id'=>'category']) !!}

                @else
                    {!! Form::select('category',$categories , null , ['class' => 'form-control','placeholder'=>'Select Category','id'=>'category']) !!}

                @endif
                    </div>
            <div class="col-md-2">
                {!! Form::select('day', ['Monday'=>'Monday','Tuesday'=>'Tuesday','Wednesday'=>'Wednesday','Thursday'=>'Thursday','Friday'=>'Friday','Saturday'=>'Saturday','Sunday'=>'Sunday','Long Weekend'=>'Long Weekend','Extra'=>'Extra'] , null , ['class' => 'form-control','placeholder'=>'Select Day','id'=>'day']) !!}
            </div>
            <div class="col-md-2">
                {!! Form::select('time', ['Morning'=>'Morning','Noon'=>'Noon','Evening'=>'Evening'] , null , ['class' => 'form-control','placeholder'=>'Select Time','id'=>'time']) !!}
            </div>
            <div class="col-md-2">
                <input type="text" name="date" id="date" class="form-control" placeholder="Select Date">
            </div>
            <div class="col-md-2">
                <button class="btn btn-info " onclick="process_form(this)">Filter</button>
            </div>

            <!-- loading view -->
        </div>
    </div>

    {{ Form::open(array('url' => 'sr/cal/pro','id'=>'mergerForm' ,'target'=>'_blank'))}}
    <div class="">
        <table id="invoice_table" class="display text-center">
            <thead>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>Order Created Date</th>
                <th>Order Date</th>
                <th>Category</th>
                <th>Day</th>
                <th>Time</th>
                <th>Agent</th>
                <th>Territory</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>Order Created Date</th>
                <th>Order Date</th>
                <th>Category</th>
                <th>Day</th>
                <th>Time</th>
                <th>Agent</th>
                <th>Territory</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </tfoot>
        </table>
        <div class="row">
            <div class="col-md-12 mt-5 ">
                @if(Auth::user()->hasRole(['Owner','Stores End Product','Super Admin']))
                <button type="submit" name="submit"  value="summary" onclick="this.form.submit();this.disabled = true;" class="d-none btn btn-block btn-warning" id="btn_sumbit">Merge Show Summary</button>
                @endif
            </div>
{{--            <div class="col-md-6 mt-5 float-right">--}}
{{--                <button type="submit" name="submit"  value="merge" class="btn btn-block btn-success">Merge Selected</button>--}}
{{--            </div>--}}
        </div>
        {{ Form::close() }}
    </div>
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    {{--    <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>--}}
    {{--    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js"></script>--}}



    <script>


        $('#date').daterangepicker({
            "showDropdowns": true,
            "singleDatePicker": true,
            "locale": {
                "format": "YYYY-MM-DD",
            },
        });
        $('#date').val('')

        function process_form(e) {
            let category = $("#category").val();
            let day = $("#day").val();
            let time = $("#time").val();
            let date = $("#date").val();
            let agent = $("#agent").val();
            let table = $('#invoice_table').DataTable();
            if (category!=null && category!=''){
                $('#btn_sumbit').removeClass('d-none');
            }else{
                $('#btn_sumbit').addClass('d-none');
            }

            table.ajax.url('/sr/table/data?category=' + category + '&day=' + day + '&date=' + date + '&agent=' + agent + '&time=' + time + '&filter=' + true).load();
        }


        function process_form_reset() {
            $("#category").val('');
            $("#day").val('');
            $("#time").val('');
            $("#date").val('');
            $("#agent").val('');
            let table = $('#invoice_table').DataTable();
            table.ajax.url('/sr/table/data').load();
        }

        // $(document).ajaxStart(function(){
        //   $("#wait").css("display", "block");
        // });
        // $('#mergerForm').on('submit', function (e) {
        //
        //
        //     var form = this;
        //
        //     var rows_selected = table.column(0).checkboxes.selected();
        //
        //     let orderIds = [];
        //     // Iterate over all selected checkboxes
        //     $.each(rows_selected, function (index, rowId) {
        //         orderIds.push(rowId);
        //     });
        //
        //     swal({
        //             title: "Are you sure ?",
        //             text: "Do you want to Merge? This action cannot be recover !",
        //             type: "warning",
        //             showCancelButton: true,
        //             confirmButtonClass: "btn-danger",
        //             confirmButtonText: "Yes, Merge it!",
        //             cancelButtonText: "No, cancel it!",
        //             closeOnConfirm: false,
        //             closeOnCancel: false
        //         },
        //         function (isConfirm) {
        //             if (isConfirm) {
        //               $('.confirm').attr('disabled',true)
        //                 $.ajaxSetup({
        //                     headers: {
        //                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //                     }
        //                 });
        //
        //                 $.ajax({
        //                     type: "post",
        //                     url: '../sr/cal/pro',
        //                     data: {order: orderIds},
        //                     success: function (response) {
        //                       $('.confirm').attr('disabled',false)
        //                         if (response == 'true') {
        //                             swal("Nice!", "You successfully Merged", "success");
        //                             let table = $('#invoice_table').DataTable();
        //                             table.destroy();
        //                             dataTableLoad();
        //                         } else {
        //                             swal("Error!", "Something went wrong", "error");
        //                         }
        //                     },
        //                     error: function (jqXHR, textStatus, errorThrown) {
        //                       swal("Error!", "Something went wrong", "error");
        //                      }
        //                 });
        //             }
        //         });
        //
        //     e.preventDefault();
        // });

        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": true,
            "timePicker24Hour": true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019"
        });
        $('#date_range').val('')

        $(document).ready(function () {
            table = $('#invoice_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "ajax": {
                    url: "{{url('/sr/table/data')}}", // json datasource
                    type: "get",
                    error: function () {  // error handling code
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    }
                },
                'columnDefs': [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                // 'select': {
                //     'style': 'multi'
                // },

                dom: 'Bfrtip',
                buttons: [
                    {
                        text: '',
                        className: 'btn btn-default icon-md icon-trash datatable-btn',
                        action: function (e, dt, node, config) {

                        }
                    }
                ],
                pageLength: 100,
                responsive: true
            });
        });

        function dataTableLoad() {
            table = $('#invoice_table').DataTable({
                "bProcessing": true,
                "serverSide": true,
                searching: true,
                "ajax": {
                    url: "{{url('/sr/table/data')}}", // json datasource
                    type: "get",
                    error: function () {  // error handling code
                        $("#supplier_wise_materials_table_processing").css("display", "none");
                    }
                },
                'columnDefs': [
                    {
                        'targets': 0,
                        'checkboxes': {
                            'selectRow': true
                        }
                    }
                ],
                // 'select': {
                //     'style': 'multi'
                // },

                dom: 'Bfrtip',
                buttons: [
                    {
                        text: '',
                        className: 'btn btn-default icon-md icon-trash datatable-btn',
                        action: function (e, dt, node, config) {

                        }
                    }
                ],
                pageLength: 25,
                responsive: true
            });
        }


    </script>
    <script>
        function deleteBurden(id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            swal({
                    title: "Are you sure ?",
                    text: "Do you want to Delete? This action cannot be recover !",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, Delete it!",
                    cancelButtonText: "No, cancel it!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: "post",
                            url: '../burden/delete/burden',
                            data: {burden_id: id},

                            success: function (response) {
                                if (response == 'true') {
                                    swal("Nice!", "You successfully Deleted the burning", "success");
                                    setTimeout(location.reload.bind(location), 900);
                                } else {
                                    swal("Error!", "Something went wrong", "error");
                                }
                            }
                        });


                        // });

                    } else {
                        swal("Cancelled", "You cancelled the finishing :)", "error");
                    }
                });


        }
    </script>
@stop
