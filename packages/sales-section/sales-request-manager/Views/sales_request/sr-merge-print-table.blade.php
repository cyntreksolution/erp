<table class="table display text-center" id="raw_table">
    <thead>
    <tr>
        @foreach($table_headers as $item)
            <th>{{$item}}</th>
        @endforeach
    </tr>
    </thead>

    <tbody>
    @foreach($table_rows['end_product'] as $row)
        <tr>
            @php $count = 0 @endphp
            @foreach($row as $item)
                <td>{{$item}}</td>
                @php $qty = (is_numeric($item))?$item:0 ;$count = $count + $qty @endphp
            @endforeach
            <td>{{$count}}</td>
        </tr>
    @endforeach

    @foreach($table_rows['variant'] as $row)
        <tr>
            @php $count = 0 @endphp
            @foreach($row as $item)
                <td>{{$item}}</td>
                @php $qty = (is_numeric($item))?$item:0 ;$count = $count + $qty @endphp
            @endforeach
            <td>{{$count}}</td>
        </tr>
    @endforeach

    </tbody>
</table>

