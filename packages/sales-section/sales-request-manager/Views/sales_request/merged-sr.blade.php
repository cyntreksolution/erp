@extends('layouts.back.master')@section('title','Bulk Burden Bake')
@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendor/dtable/datatables.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendor/daterangepicker/daterangepicker.css')}}">
    <style>
        th {
            text-align: left !important;
        }

        td {
            text-align: left !important;
        }
    </style>
@stop

@section('content')

    {{--    <div class="row">--}}
    {{--        <div class="col-sm-4">--}}
    {{--            <div class="form-group">--}}
    {{--                {!! Form::select('category', $categories , null , ['class' => 'form-control','placeholder'=>'Select Category','id'=>'category']) !!}--}}
    {{--            </div>--}}
    {{--        </div>--}}

    {{--        <div class="col-sm-4">--}}
    {{--            <div class="form-group">--}}
    {{--                {!! Form::select('end_product', $end_products , null , ['class' => 'form-control','placeholder'=>'Select End Product','id'=>'end_product']) !!}--}}
    {{--            </div>--}}
    {{--        </div>--}}

    {{--        <div class="col-sm-4">--}}
    {{--            <div class="form-group">--}}
    {{--                <button type="button" class="btn btn-success" onclick="process_form()">Filter Now</button>--}}
    {{--                <button type="button" class="btn btn-warning" onclick="process_form_reset()">Reset Filter</button>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}

    {!! Form::open(['route' => 'mgvsl.items', 'method' => 'post']) !!}
    <div class="row">
        <div class="col-md-6">
            <div style="border: 1px solid black;border-radius: 15px" class="p-1">
                <h4 class="ml-2 p-2">Merged Burdens</h4>
                <table id="merge_table" class="display text-center">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Serial</th>
                        <th>Created Time</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($end_merges as $merge)
                        <tr>
                            <td><input type="checkbox" name="merge_id[]" value="{{$merge->id}}"></td>
                            <td>{{$merge->serial}}</td>
                            <td>{{$merge->created_at}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th></th>
                        <th>Serial</th>
                        <th>Created Time</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>

        <div class="col-md-6">
            <div style="border: 1px solid black;border-radius: 15px" class="p-1">
                <h4 class="ml-2 p-2">Loaded Invoices</h4>
                <table id="loading_table" class="display text-center">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Invoice Number</th>
                        <th>Loading Time</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($loadings as $loading)
                        <tr>
                            <td><input type="checkbox" name="loading_id[]" value="{{$loading->id}}"></td>
                            <td>{{$loading->invoice_number}}</td>
                            <td>{{$loading->loading_date}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th></th>
                        <th>Invoice Number</th>
                        <th>Loading Time</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <div class="row mt-2">
        <div class="col-md-12">
            <button class="btn btn-success btn-block">Next</button>
        </div>
    </div>

    {!! Form::close() !!}
@stop

@section('js')
    <script src="{{asset('assets/vendor/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/moment/moment.min.js')}}"></script>
    <script src="{{asset('assets/vendor/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>


    <script>
        $('#date_range').daterangepicker({
            "showDropdowns": true,
            "timePicker": false,
            "singleDatePicker": true,
            "locale": {
                "format": "YYYY-MM-DD",
            },
            "minDate": "11/01/2019"
        });
        $('#date_range').val('')

        $(document).ready(function () {
            table = $('#merge_table').DataTable({
                searching: true,
                "ordering": false,
                pageLength: 25,
                responsive: true
            });
        });

        $(document).ready(function () {
            table = $('#loading_table').DataTable({
                searching: true,
                "ordering": false,
                pageLength: 25,
                responsive: true
            });
        });


    </script>

@stop