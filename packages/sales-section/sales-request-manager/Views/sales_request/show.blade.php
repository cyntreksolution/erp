@extends('layouts.back.master')@section('title','Sales Request')
@section('css')
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.css"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/dashboards/dashboard.v1.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/apps/projects.css')}}"/>
    <link rel="stylesheet"
          href="{{asset('assets/vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/examples/css/pages/profile.css')}}">


    <style>
        .app-main {
            -webkit-box-flex: 1;
            -ms-flex: 1 1 0%;
            flex: 1 1 0%;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            border-left: 1px solid #eee;
            background: #fff;
        }

        .app-wrapper {
            font-family: Raleway, sans-serif;
            width: 100%;
            border: 1px solid #eee;
        }
    </style>
@stop
@section('content')
    {!! Form::open(['route' => 'manager.app-order', 'method' => 'post']) !!}
    <div class="app-wrapper">
        <div class="app-main">
            <input type="hidden" name="order_id" value="{{$order->id}}">
            <div class="app-main-header">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">Category:</div>
                            <div class="col-md-6"> {{$order->template->category->name}}</div>
                        </div>
                        @if($order->template->category->loading_type == 'to_loading')
                            <input type="hidden" name="man_id" value=1>
                        @endif


                        <div class="row">
                            <div class="col-md-6">Day:</div>
                            <div class="col-md-6"> {{$order->template->day}}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">Time:</div>
                            <div class="col-md-6"> {{$order->template->time}}</div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">Agent:</div>
                            <div class="col-md-6">{{$order->salesRep->first_name.' '.$order->salesRep->last_name}}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">Date:</div>
                            <div class="col-md-6"> {{$order->created_at}}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">Order ID:</div>
                            <div class="col-md-6"> {{$order->id}}</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="scroll-container mt-5" id="scroll-container">
                <div class="app-main-content">
                    <div class="container">
                        <div class="projects-list">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card-body col-md-12  align-items-center p-0 row">
                                        <div class="col-md-4">
                                            <div class="text-primary">
                                                <h5 class="mt-2">Name</h5>
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <div class="text-primary">
                                                <h6>Template Qty</h6>
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <div class="text-primary">
                                                <h6>Extra Qty</h6>
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <div class="text-primary">
                                                <h6>Available Qty</h6>
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <div class="text-primary">
                                                <h6>Ordered Qty</h6>
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-right">
                                            <div class="text-primary">
                                                <h6>Value</h6>
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <div class="text-primary">
                                                <h6>Return %</h6>
                                            </div>
                                        </div>
                                    </div>
                                    @php
                                        $tempqty = 0;
                                        $extraqty = 0;
                                        $avqty = 0;
                                        $orderqty = 0;
                                        $order_value =0;

                                    @endphp

                                    @foreach($order->salesOrderContent as $product)
                                        @php
                                            //dd($order->salesOrderContent);
                                            $endP = \EndProductManager\Models\EndProduct::whereId($product->end_product_end_product_id)->first();

                                            $tempqty = $tempqty +$product->qty;
                                            $extraqty = $extraqty +$product->extra_qty;
                                            $avqty = $avqty +$product->available_qty;
                                            $orderqty = $orderqty +(($product->qty - $product->available_qty ) +  $product->extra_qty);
                                            $order_value = $order_value +((($product->qty - $product->available_qty ) +  $product->extra_qty)*($endP->distributor_price));
                                        @endphp
                                        <div class="card-body col-md-12  align-items-center p-0 row">
                                            <div class="col-md-4">
                                                <div class="text-primary">
                                                    <h5 class="mt-4">   {{$endP->name}}</h5>
                                                </div>
                                            </div>
                                            <div class="col-md-1 text-center">
                                                <div class="text-primary">
                                                    <h6>{{$product->qty }}</h6>
                                                </div>
                                            </div>
                                            <div class="col-md-1 text-center">
                                                <div class="text-primary">
                                                    <h6>{{$product->extra_qty }}</h6>
                                                </div>
                                            </div>
                                            <div class="col-md-1 text-center">
                                                <div class="text-primary">
                                                    <h6>{{$product->available_qty  }}</h6>
                                                </div>
                                            </div>

                                            <div class="col-md-1 text-center">
                                                <div class="text-primary">
                                                    <h6>{{($product->qty - $product->available_qty ) +  $product->extra_qty }}</h6>
                                                </div>
                                            </div>

                                            <div class="col-md-2 text-right">
                                                <div class="text-primary">
                                                    <h6>{{number_format((($product->qty - $product->available_qty ) +  $product->extra_qty )*($endP->distributor_price),2)}}</h6>
                                                </div>
                                            </div>

                                            <div class="col-md-2 text-center">
                                                <div class="text-primary">
                                                    @php
                                                        $stat  = \App\SalesReturnData::where('end_product_id','=',$product->end_product_end_product_id)->whereHas('header', function ($q) use ($order) {
                                                            $q->where('agent_id','=',$order->salesRep->id);
                                                        })->latest()->first();

                                                        $stat_val = !empty($stat)?$stat->return_stat:0;
                                                         $clz = "";

                                    if ($stat_val<=1) {
                                        $clz ="btn-success";
                                    }elseif ($stat_val<=3){
                                         $clz ="btn-warning";
                                    }else{
                                         $clz ="btn-danger";
                                    }

                                                    @endphp
                                                    <button type="button" class="btn btn-sm {{$clz}}">
                                                        Return {{number_format($stat_val,3)}} %
                                                    </button>
                                                </div>
                                            </div>
                                        </div>



                                        <hr class="m-1">

                                    @endforeach

                                    <div class="card-body col-md-12 bg-cyan align-items-center p-0 row">
                                        <div class="col-md-4">
                                            <div class="text-primary">
                                                <h5 class="mt-2">Non Variant Total</h5>
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <div class="text-primary">
                                                <h6>{{$tempqty}}</h6>
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <div class="text-primary">
                                                <h6>{{$extraqty}}</h6>
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <div class="text-primary">
                                                <h6>{{$avqty}}</h6>
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <div class="text-primary">
                                                <h6>{{$orderqty}}</h6>
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-right">
                                            <div class="text-primary">
                                                <h6>{{number_format($order_value,2)}}</h6>
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <div class="text-primary">
                                                <h6></h6>
                                            </div>
                                        </div>
                                    </div>

                                    <hr class="m-2">


                                    @php
                                        $tempqty_v = 0;
                                        $extraqty_v = 0;
                                        $avqty_v = 0;
                                        $orderqty_v = 0;
                                        $order_value_v =0;

                                    @endphp
                                    @foreach($order->salesOrderVariants as $product)
                                        @php

                                            $endPV = \App\Variant::whereId($product->variant_id)->first();

                                            $endP = \EndProductManager\Models\EndProduct::whereId($endPV->end_product_id)->first();

                                            $tempqty_v = $tempqty_v+($product->qty)*($endPV->size);
                                            $extraqty_v = $extraqty_v+($product->extra_qty)*($endPV->size);
                                            $avqty_v = $avqty_v+($product->available_qty)*($endPV->size);
                                            $orderqty_v = $orderqty_v+(($product->qty - $product->available_qty ) +  $product->extra_qty)*($endPV->size);
                                            $order_value_v =$order_value_v+((($product->qty - $product->available_qty ) +  $product->extra_qty)*($endPV->size))*($endP->distributor_price);
                                        @endphp
                                        <div class="card-body col-md-12  align-items-center p-0 row">
                                            <div class="col-md-4">
                                                <div class="text-primary">
                                                    <h5 class="mt-4">   {{ $product->variant->product->name.' '.$product->variant->variant }}</h5>
                                                </div>
                                            </div>

                                            <div class="col-md-1 text-center">
                                                <div class="text-primary">
                                                    <h6>{{($product->qty)*($endPV->size) }}</h6>
                                                </div>
                                            </div>

                                            <div class="col-md-1 text-center">
                                                <div class="text-primary">
                                                    <h6>{{($product->extra_qty)*($endPV->size)}}</h6>
                                                </div>
                                            </div>

                                            <div class="col-md-1 text-center">
                                                <div class="text-primary">

                                                    <h6>{{($product->available_qty)*($endPV->size) }}</h6>
                                                </div>
                                            </div>

                                            <div class="col-md-1 text-center">
                                                <div class="text-primary">
                                                    <h6>{{(($product->qty - $product->available_qty ) +  $product->extra_qty)*($endPV->size) }}</h6>
                                                </div>
                                            </div>

                                            <div class="col-md-2 text-right">
                                                <div class="text-primary">
                                                    <h6>{{number_format(((($product->qty - $product->available_qty ) +  $product->extra_qty)*($endPV->size))*($endP->distributor_price),2) }}</h6>
                                                </div>
                                            </div>

                                            <div class="col-md-2 text-center">
                                                <div class="text-primary">
                                                    @php
                                                        $stat  = \App\SalesReturnData::where('end_product_id','=',$product->variant->product->id)->whereHas('header', function ($q) use ($order) {
                                                            $q->where('agent_id','=',$order->salesRep->id);
                                                        })->latest()->first();

                                                        $stat_val = !empty($stat)?$stat->return_stat:0;
                                                         $clz = "";

                                    if ($stat_val<=1) {
                                        $clz ="btn-success";
                                    }elseif ($stat_val<=3){
                                         $clz ="btn-warning";
                                    }else{
                                         $clz ="btn-danger";
                                    }

                                                    @endphp
                                                    <button type="button" class="btn btn-sm {{$clz}}">
                                                        Return {{number_format($stat_val,3)}} %
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="m-1">
                                    @endforeach
                                    <div class="card-body col-md-12 bg-cyan align-items-center p-0 row">
                                        <div class="col-md-4">
                                            <div class="text-primary">
                                                <h5 class="mt-2">Variant Total</h5>
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <div class="text-primary">
                                                <h6>{{$tempqty_v}}</h6>
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <div class="text-primary">
                                                <h6>{{$extraqty_v}}</h6>
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <div class="text-primary">
                                                <h6>{{$avqty_v}}</h6>
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <div class="text-primary">
                                                <h6>{{$orderqty_v}}</h6>
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-right">
                                            <div class="text-primary">
                                                <h6>{{number_format($order_value_v,2)}}</h6>
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <div class="text-primary">
                                                <h6></h6>
                                            </div>
                                        </div>
                                    </div>

                                    <hr class="m-2">

                                    <div class="card-body col-md-12 bg-dribbble align-items-center p-0 row">
                                        <div class="col-md-4">
                                            <div class="text-primary">
                                                <h5 class="mt-2">Total</h5>
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <div class="text-primary">
                                                <h6>{{$tempqty+$tempqty_v}}</h6>
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <div class="text-primary">
                                                <h6>{{$extraqty+$extraqty_v}}</h6>
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <div class="text-primary">
                                                <h6>{{$avqty+$avqty_v}}</h6>
                                            </div>
                                        </div>
                                        <div class="col-md-1 text-center">
                                            <div class="text-primary">
                                                <h6>{{$orderqty+$orderqty_v}}</h6>
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-right">
                                            <div class="text-primary">
                                                <h6>{{number_format(($order_value+$order_value_v),2)}}</h6>
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-center">
                                            <div class="text-primary">
                                                <h6></h6>
                                            </div>
                                        </div>
                                    </div>

                                    <hr class="m-2">


                                </div>
                                <div class="col-md-12">
                                    @foreach($order->salesOrderRawContent as $product)
                                        <div class="card-body col-md-12  align-items-center p-0 row">
                                            <div class="col-md-6">
                                                <div class="text-primary">
                                                    <h5 class="mt-6">   {{\RawMaterialManager\Models\RawMaterial::withoutGlobalScopes(['type'])->whereRawMaterialId($product->raw_material_id)->first()->name}}</h5>
                                                </div>
                                            </div>

                                            <div class="col-md-6 text-center">
                                                <div class="text-primary">
                                                    <h6>{{$product->qty}}</h6>
                                                </div>
                                            </div>

                                        </div>
                                        <hr class="m-1">
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-md-6 mt-5 p-0">
                                <label>Description</label>
                                <p>{{$order->description}}</p>
                            </div>


                        </div>

                    </div>
                </div>
                @can('sales_request-Approve')
                    <div class="col-md-12">


                        @if($order->is_approved != 1)
                            <button class="btn btn-block btn-success m-2" type="submit">Approve By Manager</button>
                        @endcan
                    </div>
                @endif
            </div>


        </div>
    </div>
    {!! Form::close() !!}
@stop
@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
    <script src="{{asset('assets/vendor/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $('.delete').click(function (e) {
            e.preventDefault();
            id = $(this).attr("value");
            confirmAlert(id);

        });

        function confirmAlert(id) {
            swal({
                    title: "Are you sure?",
                    text: "Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                },
                function () {
                    $.ajax({
                        type: "delete",
                        url: id,
                        success: function (response) {
                            if (response == 'true') {
                                swal("Deleted!", "Deleted successfully", "success");
                                setTimeout(location.reload.bind(location), 900);
                            } else {
                                swal("Error!", "Something went wrong", "error");
                            }
                        }
                    });

                }
            );

        }

    </script>
@stop