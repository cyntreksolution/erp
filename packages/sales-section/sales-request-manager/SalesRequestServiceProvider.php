<?php

namespace SalesRequestManager;

use Illuminate\Support\ServiceProvider;

class SalesRequestServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/Views', 'SalesRequestManager');
        require __DIR__ . '/web.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('SalesRequestManager', function($app){
            return new SalesRequestManager;
        });
    }
}
