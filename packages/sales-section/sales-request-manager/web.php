<?php
/**
 * User: Chinthaka Dilan F
 * Date: 11/9/2017
 * Project : psms
 */

Route::middleware(['web'])->group(function () {
    Route::prefix('sr')->group(function () {
        Route::get('', 'SalesRequestManager\Controllers\SalesRequestController@index')->name('sr.index');

//        Route::get('create', 'SalesRequestManager\Controllers\SalesRequestController@create')->name('sr.create');

        Route::get('{salesOrder}', 'SalesRequestManager\Controllers\SalesRequestController@show')->name('sr.show');

        Route::get('{sr}/edit', 'SalesRequestManager\Controllers\SalesRequestController@edit')->name('sr.edit');

        Route::post('cal/pro', 'SalesRequestManager\Controllers\SalesRequestController@create')->name('sr.create'); // so.cal changed as sr.create
        Route::post('cal/pro/load', 'SalesRequestManager\Controllers\SalesRequestController@toLoading')->name('sr.load'); // so.cal changed as sr.create
        Route::post('cal/pro/to/burden', 'SalesRequestManager\Controllers\SalesRequestController@toBurden')->name('sr.burden'); // so.cal changed as sr.create


        Route::post('', 'SalesRequestManager\Controllers\SalesRequestController@store')->name('sr.store');
        Route::post('man/app', 'SalesRequestManager\Controllers\SalesRequestController@appManager')->name('manager.app-order');
        Route::put('{sr}', 'SalesRequestManager\Controllers\SalesRequestController@update')->name('sr.update');

        Route::delete('{sr}', 'SalesRequestManager\Controllers\SalesRequestController@destroy')->name('sr.destroy');

        Route::get('/daily/sales/requests', 'SalesRequestManager\Controllers\SalesRequestController@mergedSalesRequests')->name('sr.merged');
        Route::get('/daily/sales/requests/report', 'SalesRequestManager\Controllers\SalesRequestController@mergedSalesRequestsReport')->name('sr.mreport');

        Route::get('table/data', 'SalesRequestManager\Controllers\SalesRequestController@tableData')->name('sr.table');
        Route::post('excel/data', 'SalesRequestManager\Controllers\SalesRequestController@exportFilter')->name('sr.excel');
        Route::post('sr/excel/data', 'SalesRequestManager\Controllers\SalesRequestController@salesReportExcelExport')->name('sre.excel');
    });
});
