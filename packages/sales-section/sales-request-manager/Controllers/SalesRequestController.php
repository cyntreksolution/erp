<?php

namespace SalesRequestManager\Controllers;

use App\Exports\BurdenExport;
use App\Exports\SalesRequestExport;
use App\LoadingData;
use App\LoadingDataRaw;
use App\LoadingDataVariant;
use App\LoadingHeader;
use App\MergeRequest;
use App\Variant;
use BurdenManager\Models\Burden;
use Carbon\Carbon;
use EndProductBurdenManager\Models\EndProductBurden;
use EndProductBurdenManager\Models\EndProductBurdenHeader;
use EndProductManager\Models\Category;
use EndProductRecipeManager\Models\EndProductRecipe;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use EndProductManager\Models\EndProduct;
use Illuminate\Support\Facades\DB;
use ProductionOrderManager\Models\ProductionOrder;
use ProductionOrderManager\Models\ProductionOrderEndProduct;
use ProductionOrderManager\Models\ProductionOrderSemiProduct;
use ProductionOrderManager\Models\ProductionRawMaterial;
use SalesOrderManager\Models\Content;
use SalesOrderManager\Models\SalesOrder;
use SalesRepManager\Models\SalesRep;
use SalesRequestManager\Models\SalesRequest;
use SemiFinishProductManager\Models\SemiFinishProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SalesRequestController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:sales_request-index', ['only' => ['index','tableData']]);
        $this->middleware('permission:sales_request-list', ['only' => ['list']]);
        $this->middleware('permission:sales_request-show', ['only' => ['show']]);
        $this->middleware('permission:sales_request-Approve', ['only' => ['appManager']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::where('loading_type', '=', 'to_loading')->get()->pluck('name', 'id');
        $cat = Category::get()->pluck('name', 'id');

        $agents = SalesRep::all()->pluck('email', 'id');
        return view('SalesRequestManager::sales_request.list', compact('categories', 'agents','cat'));

        $salesOrders = SalesOrder::where('status', '=', 1)
            ->with('salesOrderContent.endProducts')
            ->get();
        $salex = array();
        foreach ($salesOrders as $salesOrder) {
            $rep = $salesOrder->created_by;
            $rep = Auth::findById($rep);

            array_push($salex, [
                'order' => $salesOrder,
                'ref' => $rep
            ]);
        }
        return view('SalesRequestManager::sales_request.index')->with([
            'orders' => $salex
        ]);
    }

    public function appManager(Request $request)
    {


        $data = SalesOrder::find($request->order_id);
        $data->is_approved = 1;
        $data->save();

        if($request->man_id == 1) {
            $categories = Category::where('loading_type', '=', 'to_loading')->get()->pluck('name', 'id');
            $cat = Category::get()->pluck('name', 'id');

            $agents = SalesRep::all()->pluck('email', 'id');
            return view('SalesRequestManager::sales_request.list', compact('categories', 'agents', 'cat'));

            $salesOrders = SalesOrder::where('status', '=', 1)
                ->with('salesOrderContent.endProducts')
                ->get();
            $salex = array();
            foreach ($salesOrders as $salesOrder) {
                $rep = $salesOrder->created_by;
                $rep = Auth::findById($rep);

                array_push($salex, [
                    'order' => $salesOrder,
                    'ref' => $rep
                ]);
            }
            return view('SalesRequestManager::sales_request.index')->with([
                'orders' => $salex
            ]);

        }else{
            $categories = Category::where('loading_type', '=', 'to_burden')->get()->pluck('name', 'id');
            $agents = SalesRep::all()->pluck('email', 'id');
            return view('production.list', compact('categories', 'agents'));

        }
    }


    public function tableData(Request $request)
    {
        $user = Auth::user();
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'created_at', 'order_date'];
        $order_column = $columns[$order_by[0]['column']];

        $salesOrders = SalesOrder::tableData($order_column, $order_by_str, $start, $length)->whereConfirmed(1)->whereStatus(1);
        $salesOrdersCount = SalesOrder::whereStatus(1)->whereConfirmed(1)->count();

        if ($request->filter == true) {
            $category = $request->category;
            $day = $request->day;
            $time = $request->time;
            $date = $request->date;
            $agent = $request->agent;
            $salesOrders = $salesOrders->filterData($category, $day, $time, $date, $agent)->get();
            $salesOrdersCount = $salesOrders->count();
        } elseif (is_null($search) || empty($search)) {
            $salesOrders = $salesOrders->get();
            $salesOrdersCount = SalesOrder::whereStatus(1)->whereConfirmed(1)->count();
        } else {
            $salesOrders = $salesOrders->searchData($search)->get();
            $salesOrdersCount = $salesOrders->count();
        }

        $data[][] = array();
        $i = 0;


        foreach ($salesOrders as $key => $salesOrder) {


            $approving_orders = SalesOrder::whereStatus(4)
                ->whereCreatedBy($salesOrder->created_by)
                ->whereHas('template', function ($q) use ($salesOrder) {
                    return $q->whereCategoryId($salesOrder->template->category_id);
                })->first();



            $app_btn_filter = null;
          if($salesOrder->is_approved != 1) {
                $app_btn_filter = "<button class='btn btn-sm btn-warning mr-2'>Approve Manager </button>";
            }

            $approving_filter = !empty($approving_orders) ? 1 : 0;
            $btn_filter = ($approving_filter) ? "<button class='btn btn-sm btn-danger mr-2'>Due Approvals</button>" : null;



            $rep = $salesOrder->salesRep->type . ' ' . $salesOrder->salesRep->name_with_initials;
            $btnShow = '<a href="' . route('sr.show', $salesOrder->id) . '"  target="_blank" class="btn btn-sm btn-outline-dark mr-1"><i class="fa fa-eye"></i> </a>';
            $btnMerge = '<button class="btn btn-sm btn-outline-dark mr-1"><i class="fa fa-hand-lizard-o"></i> </button>';

            $data[$i] = array(

                ($request->filter == true && !empty($request->category) && !$approving_filter && $salesOrder->is_approved == 1) ? "<input type='checkbox' name='order[]' value='$salesOrder->id'/>" : null,
                $salesOrder->id,
                Carbon::parse($salesOrder->created_at)->format('d-m-Y H:m'),
                Carbon::parse($salesOrder->order_date)->format('d-m-Y'),
                $salesOrder->template->category->name,
                $salesOrder->template->day,
                $salesOrder->template->time,
                $rep,
                $salesOrder->salesRep->territory,
                $btn_filter.$app_btn_filter,
                $btnShow,
            );
            $i++;
        }

        if ($salesOrdersCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($salesOrdersCount),
            "recordsFiltered" => intval($salesOrdersCount),
            "data" => $data
        ];

        return json_encode($json_data);
    }

    public function exportFilter(Request $request)
    {
        $semi = $request->semi;
        $status = $request->status;
        $date_range = $request->date_range;
        $records = Burden::filterData($semi, $status, $date_range)->get();
        return Excel::download(new BurdenExport($records), Carbon::now() . '_burden_report.xlsx');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function mergerSalesRequest(Request $request)
    {
        return $request;
    }

    public function create(Request $request)
    {


        $orders = $request->order;
        $sales_orders = $orders;
        if (empty($orders)) {
            return redirect(route('sr.index'))->with([
                'error' => true,
                'error.title' => 'Sorry !',
                'error.message' => 'No Sales Request Selected.'
            ]);
        }

        $order_s = SalesOrder::whereIn('id', $orders)->with('salesOrderContent')->get();

        $category = null;
        foreach ($order_s as $order_) {
            if ($order_->status != 1) {
                return "Your request " . $order_->id . " has been Merged";
            }
            $category = $order_->template->category;
        }


        $condition = $request->submit == 'summary';

        if ($condition) {
            $table_data = [];
            $table_data_variant = [];

            foreach ($order_s as $order) {
                $order_item = [];
                $order_item['agent_name'] = $order->salesRep->name_with_initials;
                $order_item['name'] = [];
                $order_item['end_product_id'] = [];
                $order_item['qty'] = [];

                $order_item_variant = [];
                $order_item_variant['agent_name'] = $order->salesRep->name_with_initials;
                $order_item_variant['name'] = [];
                $order_item_variant['variant_id'] = [];
                $order_item_variant['qty'] = [];

                foreach ($order->salesOrderContent as $item) {
                    if ($order->id == $item->sales_order_header_id) {
                        array_push($order_item['name'], EndProduct::whereId($item->end_product_end_product_id)->first()->name);
                        array_push($order_item['end_product_id'], $item->end_product_end_product_id);
                        $qty = ($item->extra_qty + $item->qty) - $item->available_qty;
                        array_push($order_item['qty'], $qty);
                    }
                }

                foreach ($order->salesOrderVariants as $item) {
                    if ($order->id == $item->sales_order_header_id) {
                        $variant = Variant::whereId($item->variant_id)->first();
                        array_push($order_item_variant['name'], $variant->product->name . ' ' . $variant->variant);
                        $qty = ($item->extra_qty + $item->qty) - $item->available_qty;
                        array_push($order_item_variant['variant_id'], $item->variant_id);
                        array_push($order_item_variant['qty'], $qty);
                    }
                }

                array_push($table_data, $order_item);
                array_push($table_data_variant, $order_item_variant);
            }


            $name_list = [];
            $name_list['end_product'] = [];
            $name_list ['variant'] = [];
            $table_headers = [];
            array_push($table_headers, 'End Products');

            foreach ($table_data as $key => $agent) {
                array_push($table_headers, $agent['agent_name']);
                foreach ($agent['name'] as $key1 => $name) {
                    if (!in_array($name, $name_list['end_product'])) {
                        array_push($name_list['end_product'], $name);
                    }
                }
            }

            foreach ($table_data_variant as $key => $agent) {
                foreach ($agent['name'] as $key1 => $name) {
                    if (!in_array($name, $name_list['variant'])) {
                        array_push($name_list['variant'], $name);
                    }
                }
            }


            array_push($table_headers, 'Total');

            $table_rows = [];
            $table_rows['end_product'] = [];
            $table_rows['variant'] = [];

            foreach ($name_list['end_product'] as $key => $item) {
                $row = [];
                $name = $item;
                array_push($row, $name);
                foreach ($table_data as $table_datum) {
                    $qty = (isset($table_datum['qty'][$key])) ? $table_datum['qty'][$key] : 0;
                    array_push($row, $qty);
                }
                array_push($table_rows['end_product'], $row);
            }

            foreach ($name_list['variant'] as $key => $item) {
                $row = [];
                $name = $item;
                array_push($row, $name);
                foreach ($table_data_variant as $table_datum) {
                    $qty = (isset($table_datum['qty'][$key])) ? $table_datum['qty'][$key] : 0;
                    array_push($row, $qty);
                }
                array_push($table_rows['variant'], $row);
            }

            $mr = null;
            $serial = 'MR' . Carbon::now()->format('YmdHis') . rand(1000, 9999);
            foreach ($table_rows['end_product'] as $items) {
                $count = 0;
                $endProduct = EndProduct::where('name', '=', $items[0])->first();

                foreach ($items as $item) {
                    $qty = (is_numeric($item)) ? $item : 0;
                    $count = $count + $qty;
                }

                if ($count > 0) {
                    $mr = new MergeRequest();
                    $mr->end_product_id = $endProduct->id;
                    $mr->qty = $count;
                    $mr->serial = $serial;
                    $mr->sales_request_data = json_encode($sales_orders);
                    $mr->save();
                }

            }

            return view('SalesRequestManager::sales_request.sr-merge-print', compact('category', 'table_rows', 'table_headers', 'orders', 'mr'));

        }

       /* $products = DB::table('sales_order_header')
            ->select('end_product.name as end_product_name', 'end_product.image as end_product_image', 'end_product.image_path as end_product_path',
                'end_product_end_product_id', 'end_product.available_qty As end_product_available_qty',
                'end_product.buffer_stock As end_product_buffer_stock', 'semi_finish_product_semi_finish_product_id As semi_finish_product',
                'semi_finish_product.name as semi_name', 'semi_finish_product.image as semi_image', 'semi_finish_product.image_path as semi_path',
                'end_product_semi_finish_product.qty AS semi_qty', 'semi_finish_product.available_qty As semi_finish_availbel_qty',
                'semi_finish_product.buffer_stock as semi_finish_buffer_stock', 'sales_order_data.available_qty as agent_qty',
                DB::raw('SUM(sales_order_data.qty) as total'))
            ->leftJoin('sales_order_data', 'sales_order_header.id', '=', 'sales_order_header_id')
            ->leftJoin('end_product', 'sales_order_data.end_product_end_product_id', '=', 'end_product.id')
            ->leftJoin('end_product_semi_finish_product', 'end_product.id', '=', 'end_product_semi_finish_product.end_product_id')
            ->leftJoin('semi_finish_product', 'end_product_semi_finish_product.semi_finish_product_semi_finish_product_id', '=', 'semi_finish_product.semi_finish_product_id')
            ->where('sales_order_header.status', '=', 1)
            ->groupBy('end_product_end_product_id')
            ->havingRaw('COUNT(sales_order_data.id) > 0')
            ->get();


        $semiproducts = DB::table('sales_order_header')
            ->select('semi_finish_product_semi_finish_product_id As semi_finish_product',
                'semi_finish_product.name as semi_name', 'semi_finish_product.image as semi_image', 'semi_finish_product.image_path as semi_path',
                'end_product_semi_finish_product.qty AS semi_qty', 'semi_finish_product.available_qty As semi_finish_availbel_qty',
                'semi_finish_product.buffer_stock as semi_finish_buffer_stock',
                DB::raw('(SUM(sales_order_data.qty)-SUM(sales_order_data.available_qty))* end_product_semi_finish_product.qty as total'))
            ->leftJoin('sales_order_data', 'sales_order_header.id', '=', 'sales_order_header_id')
            ->leftJoin('end_product', 'sales_order_data.end_product_end_product_id', '=', 'end_product.id')
            ->leftJoin('end_product_semi_finish_product', 'end_product.id', '=', 'end_product_semi_finish_product.end_product_id')
            ->leftJoin('semi_finish_product', 'end_product_semi_finish_product.semi_finish_product_semi_finish_product_id', '=', 'semi_finish_product.semi_finish_product_id')
            ->where('sales_order_header.status', '=', 1)
            ->groupBy('end_product_semi_finish_product.semi_finish_product_semi_finish_product_id')
            ->havingRaw('COUNT(sales_order_data.id) > 0')
            ->get();


        $rawMaterials = DB::table('sales_order_header')
            ->select(DB::raw('SUM(sales_order_raw_data.qty) as total'), 'raw_material.raw_material_id')
            ->leftJoin('sales_order_raw_data', 'sales_order_header.id', '=', 'sales_order_header_id')
            ->leftJoin('raw_material', 'sales_order_raw_data.raw_material_id', '=', 'raw_material.raw_material_id')
            ->where('sales_order_header.status', '=', 1)
            ->groupBy('raw_material.raw_material_id')
            ->havingRaw('COUNT(sales_order_raw_data.id) > 0')
            ->get();


        DB::transaction(function () use ($products, $semiproducts, $rawMaterials, $orders) {
            $po = new ProductionOrder();
            $po->created_by =Auth::user()->id;
            $po->save();

            foreach ($products as $product) {
                $poe = new ProductionOrderEndProduct();
                $poe->production_order_id = $po->id;
                $poe->end_product_id = $product->end_product_end_product_id;
                $poe->semi_product_id = $product->semi_finish_product;
                $poe->semi_qty = $product->semi_qty;
                $poe->total = $product->total - $product->agent_qty;
                $poe->save();
            }

            foreach ($semiproducts as $semiproduct) {
                $pos = new ProductionOrderSemiProduct();
                $pos->production_order_id = $po->id;
                $pos->semi_product_id = $semiproduct->semi_finish_product;
                $pos->semi_qty = $semiproduct->semi_qty;
                $pos->semi_total = $semiproduct->total;
                $pos->save();
            }


            foreach ($rawMaterials as $raw) {
                $pos = new ProductionRawMaterial();
                $pos->production_order_id = $po->id;
                $pos->raw_material_id = $raw->raw_material_id;
                $pos->qty = $raw->total;
                $pos->save();
            }

            foreach ($orders as $order) {
                $orde = SalesOrder::find($order);
                $orde->status = 2;
                $orde->save();

                $loadingHeader = new LoadingHeader();


                $total = 0;
                foreach ($orde->salesOrderContent as $item) {

                    $loadingData = new LoadingData();
                    $loadingData->loading_header_id = $loadingHeader->id;
                    $loadingData->end_product_id = $item->end_product_end_product_id;
                    $loadingData->qty = $item->qty;
                    $loadingData->extra_qty = $item->extra_qty;
                    $loadingData->agent_available_qty = $item->available_qty;
                    $loadingData->agent_approved_qty = 0;
                    $loadingData->admin_approved_qty = 0;
                    $loadingData->unit_value = EndProduct::whereId($item->end_product_end_product_id)->first()->distributor_price;
                    $loadingData->status = 0;
                    $loadingData->is_extra_order = $item->is_extra_order;
                    $loadingData->save();

                    $total = $total + ($loadingData->unit_value * $loadingData->qty);
                }


              //  $loadingHeader->save();


                if (!empty($orde->salesOrderRawContent)) {
                    foreach ($orde->salesOrderRawContent as $item) {
                        $loadingData = new LoadingDataRaw();
                        $loadingData->loading_header_id = $loadingHeader->id;
                        $loadingData->raw_material_id = $item->raw_material_id;
                        $loadingData->qty = $item->qty;
                        $loadingData->agent_approved_qty = 0;
                        $loadingData->admin_approved_qty = 0;
                        $loadingData->status = 0;
                        $loadingData->save();
                    }
                }
                $loadingHeader->so_id = $orde->id;
                $loadingHeader->invoice_number = 'SO-' . Carbon::now()->format('ymd') . $orde->id;;
                $loadingHeader->total = $total;
                $loadingHeader->save();

            }

        });*/

//        return 'true';
        return view('SalesRequestManager::sales_request.create');

    }

    public function toLoading(Request $request)
    {
        $orders = explode(',', $request->order);
        $order_s = SalesOrder::whereIn('id', $orders)->with('salesOrderContent')->get();

        foreach ($order_s as $order_) {
            if ($order_->status != 1) {
                return "Your request " . $order_->id . " has been Merged";
            }
        }


        $products = DB::table('sales_order_header')
            ->select('end_product.name as end_product_name', 'end_product.image as end_product_image', 'end_product.image_path as end_product_path',
                'end_product_end_product_id', 'end_product.available_qty As end_product_available_qty',
                'end_product.buffer_stock As end_product_buffer_stock', 'semi_finish_product_semi_finish_product_id As semi_finish_product',
                'semi_finish_product.name as semi_name', 'semi_finish_product.image as semi_image', 'semi_finish_product.image_path as semi_path',
                'end_product_semi_finish_product.qty AS semi_qty', 'semi_finish_product.available_qty As semi_finish_availbel_qty',
                'semi_finish_product.buffer_stock as semi_finish_buffer_stock', 'sales_order_data.available_qty as agent_qty',
                DB::raw('SUM(sales_order_data.qty) as total'))
            ->leftJoin('sales_order_data', 'sales_order_header.id', '=', 'sales_order_header_id')
            ->leftJoin('end_product', 'sales_order_data.end_product_end_product_id', '=', 'end_product.id')
            ->leftJoin('end_product_semi_finish_product', 'end_product.id', '=', 'end_product_semi_finish_product.end_product_id')
            ->leftJoin('semi_finish_product', 'end_product_semi_finish_product.semi_finish_product_semi_finish_product_id', '=', 'semi_finish_product.semi_finish_product_id')
            ->where('sales_order_header.status', '=', 1)
            ->groupBy('end_product_end_product_id')
            ->havingRaw('COUNT(sales_order_data.id) > 0')
            ->get();


        $semiproducts = DB::table('sales_order_header')
            ->select('semi_finish_product_semi_finish_product_id As semi_finish_product',
                'semi_finish_product.name as semi_name', 'semi_finish_product.image as semi_image', 'semi_finish_product.image_path as semi_path',
                'end_product_semi_finish_product.qty AS semi_qty', 'semi_finish_product.available_qty As semi_finish_availbel_qty',
                'semi_finish_product.buffer_stock as semi_finish_buffer_stock',
                DB::raw('(SUM(sales_order_data.qty)-SUM(sales_order_data.available_qty))* end_product_semi_finish_product.qty as total'))
            ->leftJoin('sales_order_data', 'sales_order_header.id', '=', 'sales_order_header_id')
            ->leftJoin('end_product', 'sales_order_data.end_product_end_product_id', '=', 'end_product.id')
            ->leftJoin('end_product_semi_finish_product', 'end_product.id', '=', 'end_product_semi_finish_product.end_product_id')
            ->leftJoin('semi_finish_product', 'end_product_semi_finish_product.semi_finish_product_semi_finish_product_id', '=', 'semi_finish_product.semi_finish_product_id')
            ->where('sales_order_header.status', '=', 1)
            ->groupBy('end_product_semi_finish_product.semi_finish_product_semi_finish_product_id')
            ->havingRaw('COUNT(sales_order_data.id) > 0')
            ->get();


        $rawMaterials = DB::table('sales_order_header')
            ->select(DB::raw('SUM(sales_order_raw_data.qty) as total'), 'raw_material.raw_material_id')
            ->leftJoin('sales_order_raw_data', 'sales_order_header.id', '=', 'sales_order_header_id')
            ->leftJoin('raw_material', 'sales_order_raw_data.raw_material_id', '=', 'raw_material.raw_material_id')
            ->where('sales_order_header.status', '=', 1)
            ->groupBy('raw_material.raw_material_id')
            ->havingRaw('COUNT(sales_order_raw_data.id) > 0')
            ->get();


        DB::transaction(function () use ($products, $semiproducts, $rawMaterials, $orders) {
            $po = new ProductionOrder();
            $po->created_by = Auth::user()->id;
            $po->save();

            Storage::put('logs/orders', $orders);

            foreach ($products as $product) {
                $poe = new ProductionOrderEndProduct();
                $poe->production_order_id = $po->id;
                $poe->end_product_id = $product->end_product_end_product_id;
                $poe->semi_product_id = $product->semi_finish_product;
                $poe->semi_qty = $product->semi_qty;
                $poe->total = $product->total - $product->agent_qty;
                $poe->save();
            }

            foreach ($semiproducts as $semiproduct) {
                $pos = new ProductionOrderSemiProduct();
                $pos->production_order_id = $po->id;
                $pos->semi_product_id = $semiproduct->semi_finish_product;
                $pos->semi_qty = $semiproduct->semi_qty;
                $pos->semi_total = $semiproduct->total;
                $pos->save();
            }


            foreach ($rawMaterials as $raw) {
                $pos = new ProductionRawMaterial();
                $pos->production_order_id = $po->id;
                $pos->raw_material_id = $raw->raw_material_id;
                $pos->qty = $raw->total;
                $pos->save();
            }

            foreach ($orders as $order) {
                $orde = SalesOrder::find($order);
                $orde->status = 2;
                $orde->save();


                $loadingEx = LoadingHeader::where('so_id', '=', $orde->id)->first();

                if (!empty($loadingEx) && $loadingEx->count() > 0) {
                    Storage::put('logs/errors', $loadingEx);
                } else {
                    $loadingHeader = new LoadingHeader();
                    $loadingHeader->so_id = $orde->id;
                    $loadingHeader->invoice_number = 'SO-' . Carbon::now()->format('ymd') . $orde->id;;
                    $loadingHeader->save();

                    $total = 0;
                    foreach ($orde->salesOrderContent as $item) {
                        $loadingData = new LoadingData();
                        $loadingData->loading_header_id = $loadingHeader->id;
                        $loadingData->end_product_id = $item->end_product_end_product_id;
                        $loadingData->qty = $item->qty;
                        $loadingData->requested_qty = $item->qty;
                        $loadingData->extra_qty = $item->extra_qty;
                        $loadingData->agent_available_qty = $item->available_qty;
                        $loadingData->agent_approved_qty = 0;
                        $loadingData->requested_qty = $item->qty;
                        $loadingData->admin_approved_qty = 0;
                        $loadingData->unit_value = EndProduct::whereId($item->end_product_end_product_id)->first()->distributor_price;
                        $loadingData->status = 0;
                        $loadingData->is_extra_order = $item->is_extra_order;
                        $loadingData->save();

                        $total = $total + ($loadingData->unit_value * $loadingData->qty);
                    }


                    foreach ($orde->salesOrderVariants as $item) {
                        $loadingData = new LoadingDataVariant();
                        $loadingData->loading_header_id = $loadingHeader->id;
                        $loadingData->variant_id = $item->variant_id;
                        $loadingData->qty = $item->qty;
                        $loadingData->requested_qty = $item->qty;
                        $loadingData->extra_qty = $item->extra_qty;
                        $loadingData->requested_qty = $item->qty;
                        $loadingData->agent_available_qty = $item->available_qty;
                        $loadingData->agent_approved_qty = 0;
                        $loadingData->admin_approved_qty = 0;

                        $variant = Variant::find($item->variant_id);

                        $loadingData->unit_value = $variant->product->distributor_price * $variant->size;
                        $loadingData->status = 0;
                        $loadingData->end_product_id = $variant->end_product_id;
                        $loadingData->is_extra_order = $item->is_extra_order;
                        $loadingData->save();

                        $total = $total + ($loadingData->unit_value * $loadingData->qty);
                    }

                    $loadingHeader->total = $total;
                    $loadingHeader->save();


                    if (!empty($orde->salesOrderRawContent)) {
                        foreach ($orde->salesOrderRawContent as $item) {
                            $loadingData = new LoadingDataRaw();
                            $loadingData->loading_header_id = $loadingHeader->id;
                            $loadingData->raw_material_id = $item->raw_material_id;
                            $loadingData->qty = $item->qty;
                            $loadingData->requested_qty = $item->qty;
                            $loadingData->agent_approved_qty = 0;
                            $loadingData->admin_approved_qty = 0;
                            $loadingData->status = 0;
                            $loadingData->save();
                        }
                    }



                }
            }

        });

        return redirect(route('sr.index'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => 'To Loading Request Created Successfully'
        ]);

    }

  /*  public function toBurden(Request $request)
    {
        $orders = explode(',', $request->order);
        $order_s = SalesOrder::whereIn('id', $orders)->with('salesOrderContent')->get();

        foreach ($order_s as $order_) {
            if ($order_->status != 1) {
                return "Your request " . $order_->id . " has been Merged";
            }
        }

        $serial = $request->merge_serial;
        $mr = MergeRequest::whereSerial($serial)->get();

        foreach ($mr as $product) {
            $endProduct = EndProduct::find($product->end_product_id);
            $active_recipe = EndProductRecipe::whereEndProductId($product->end_product_id)->whereStatus(1)->first();

            if (empty($active_recipe)) {
                return 'No Active Recipe Found for ' . $endProduct->name;
            }

            $burdenCount = EndProductBurden::whereDate('created_at', '=', Carbon::today()->toDateString())->get()->count();
            $last_digit = $burdenCount + 1;
            $serial = date('ymd') . '-' . $last_digit;

            $burden = new EndProductBurden();
            $burden->end_product_id = $product->end_product_id;
            $burden->serial = $serial;
            $burden->burden_size = $product->qty;
            $burden->expected_end_product_qty = $product->qty;
            $burden->recipe_id = $active_recipe->id;
            $burden->merge_request_id = $product->id;
            $burden->created_by = Auth::user()->id;
            $burden->save();

            $mr = MergeRequest::find($product->id);
            $mr->status = 2;
            $mr->save();
        }


        $products = DB::table('sales_order_header')
            ->select('end_product.name as end_product_name', 'end_product.image as end_product_image', 'end_product.image_path as end_product_path',
                'end_product_end_product_id', 'end_product.available_qty As end_product_available_qty',
                'end_product.buffer_stock As end_product_buffer_stock', 'semi_finish_product_semi_finish_product_id As semi_finish_product',
                'semi_finish_product.name as semi_name', 'semi_finish_product.image as semi_image', 'semi_finish_product.image_path as semi_path',
                'end_product_semi_finish_product.qty AS semi_qty', 'semi_finish_product.available_qty As semi_finish_availbel_qty',
                'semi_finish_product.buffer_stock as semi_finish_buffer_stock', 'sales_order_data.available_qty as agent_qty',
                DB::raw('SUM(sales_order_data.qty) as total'))
            ->leftJoin('sales_order_data', 'sales_order_header.id', '=', 'sales_order_header_id')
            ->leftJoin('end_product', 'sales_order_data.end_product_end_product_id', '=', 'end_product.id')
            ->leftJoin('end_product_semi_finish_product', 'end_product.id', '=', 'end_product_semi_finish_product.end_product_id')
            ->leftJoin('semi_finish_product', 'end_product_semi_finish_product.semi_finish_product_semi_finish_product_id', '=', 'semi_finish_product.semi_finish_product_id')
            ->where('sales_order_header.status', '=', 1)
            ->groupBy('end_product_end_product_id')
            ->havingRaw('COUNT(sales_order_data.id) > 0')
            ->get();


        $semiproducts = DB::table('sales_order_header')
            ->select('semi_finish_product_semi_finish_product_id As semi_finish_product',
                'semi_finish_product.name as semi_name', 'semi_finish_product.image as semi_image', 'semi_finish_product.image_path as semi_path',
                'end_product_semi_finish_product.qty AS semi_qty', 'semi_finish_product.available_qty As semi_finish_availbel_qty',
                'semi_finish_product.buffer_stock as semi_finish_buffer_stock',
                DB::raw('(SUM(sales_order_data.qty)-SUM(sales_order_data.available_qty))* end_product_semi_finish_product.qty as total'))
            ->leftJoin('sales_order_data', 'sales_order_header.id', '=', 'sales_order_header_id')
            ->leftJoin('end_product', 'sales_order_data.end_product_end_product_id', '=', 'end_product.id')
            ->leftJoin('end_product_semi_finish_product', 'end_product.id', '=', 'end_product_semi_finish_product.end_product_id')
            ->leftJoin('semi_finish_product', 'end_product_semi_finish_product.semi_finish_product_semi_finish_product_id', '=', 'semi_finish_product.semi_finish_product_id')
            ->where('sales_order_header.status', '=', 1)
            ->groupBy('end_product_semi_finish_product.semi_finish_product_semi_finish_product_id')
            ->havingRaw('COUNT(sales_order_data.id) > 0')
            ->get();


        $rawMaterials = DB::table('sales_order_header')
            ->select(DB::raw('SUM(sales_order_raw_data.qty) as total'), 'raw_material.raw_material_id')
            ->leftJoin('sales_order_raw_data', 'sales_order_header.id', '=', 'sales_order_header_id')
            ->leftJoin('raw_material', 'sales_order_raw_data.raw_material_id', '=', 'raw_material.raw_material_id')
            ->where('sales_order_header.status', '=', 1)
            ->groupBy('raw_material.raw_material_id')
            ->havingRaw('COUNT(sales_order_raw_data.id) > 0')
            ->get();


        DB::transaction(function () use ($products, $semiproducts, $rawMaterials, $orders) {
            $po = new ProductionOrder();
            $po->created_by = Auth::user()->id;
            $po->save();

            foreach ($products as $product) {
                $poe = new ProductionOrderEndProduct();
                $poe->production_order_id = $po->id;
                $poe->end_product_id = $product->end_product_end_product_id;
                $poe->semi_product_id = $product->semi_finish_product;
                $poe->semi_qty = $product->semi_qty;
                $poe->total = $product->total - $product->agent_qty;
                $poe->save();
            }

            foreach ($semiproducts as $semiproduct) {
                $pos = new ProductionOrderSemiProduct();
                $pos->production_order_id = $po->id;
                $pos->semi_product_id = $semiproduct->semi_finish_product;
                $pos->semi_qty = $semiproduct->semi_qty;
                $pos->semi_total = $semiproduct->total;
                $pos->save();
            }


            foreach ($rawMaterials as $raw) {
                $pos = new ProductionRawMaterial();
                $pos->production_order_id = $po->id;
                $pos->raw_material_id = $raw->raw_material_id;
                $pos->qty = $raw->total;
                $pos->save();
            }

            foreach ($orders as $order) {
                $orde = SalesOrder::find($order);
                $orde->status = 2;
                $orde->save();

                $loadingHeader = new LoadingHeader();
                $loadingHeader->so_id = $orde->id;
                $loadingHeader->invoice_number = 'SO-' . Carbon::now()->format('ymd') . $orde->id;;
                $loadingHeader->save();

                $total = 0;
                foreach ($orde->salesOrderContent as $item) {
                    $loadingData = new LoadingData();
                    $loadingData->loading_header_id = $loadingHeader->id;
                    $loadingData->end_product_id = $item->end_product_end_product_id;
                    $loadingData->qty = $item->qty;
                    $loadingData->requested_qty = $item->qty;
                    $loadingData->extra_qty = $item->extra_qty;
                    $loadingData->agent_available_qty = $item->available_qty;
                    $loadingData->agent_approved_qty = 0;
                    $loadingData->admin_approved_qty = 0;
                    $loadingData->unit_value = EndProduct::whereId($item->end_product_end_product_id)->first()->distributor_price;
                    $loadingData->status = 0;
                    $loadingData->is_extra_order = $item->is_extra_order;
                    $loadingData->save();

                    $total = $total + ($loadingData->unit_value * $loadingData->qty);
                }

                $loadingHeader->total = $total;
                $loadingHeader->save();


                if (!empty($orde->salesOrderRawContent)) {
                    foreach ($orde->salesOrderRawContent as $item) {
                        $loadingData = new LoadingDataRaw();
                        $loadingData->loading_header_id = $loadingHeader->id;
                        $loadingData->raw_material_id = $item->raw_material_id;
                        $loadingData->qty = $item->qty;
                        $loadingData->agent_approved_qty = 0;
                        $loadingData->admin_approved_qty = 0;
                        $loadingData->status = 0;
                        $loadingData->save();
                    }
                }


            }

        });


        return redirect(route('sr.index'))->with([
            'success' => true,
            'success.title' => 'Congratulations !',
            'success.message' => 'new burdens created successfully'
        ]);

    }*/

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return "st";
    }

    /**
     * Display the specified resource.
     *
     * @param \SalesRequestManager\Models\SalesRequest $sr
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $order)
    {
        $order = SalesOrder::whereId($order)->first();
        return view('SalesRequestManager::sales_request.show', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \SalesRequestManager\Models\SalesRequest $sr
     * @return \Illuminate\Http\Response
     */
    public function edit($sr)
    {
//        $raw_materials=RawMaterial::all();
//        $po = PurchasingOrder::with('supplier', 'purchasingOrderContent.ingredients')
//            ->where('id', $purchasingOrder)
//            ->get();
//        return view('SalesOrderManager::po.edit')->with([
//            'po' => $po,
//            'raw_materials'=>$raw_materials,
//
//
//        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \SalesRequestManager\Models\SalesRequest $sr
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SalesRequest $sr)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \SalesRequestManager\Models\SalesRequest $sr
     * @return \Illuminate\Http\Response
     */
    public function destroy(SalesRequest $sr)
    {
        $sr->delete();
        if ($sr->trashed()) {
            return 'true';
        } else {
            return 'false';
        }
    }

    public function mergedSalesRequests(Request $request)
    {
        $end_merges = EndProductBurdenHeader::whereHas('category', function ($q) {
            $q->where('loading_type', '=', 'to_burden');
        })->whereIsTracked(0)->get();


        $loadings = LoadingHeader::whereHas('salesOrder.template.category', function ($q) {
            $q->where('loading_type', '=', 'to_burden');
        })->where('status', '>', 3)->whereIsTracked(0)->get();

        return view('SalesRequestManager::sales_request.merged-sr', compact('end_merges', 'loadings'));
    }

    public function download_production_order(Request $request)
    {
        $data = ProductionOrder::whereId($request->production_order_id)->get();
        $end_product_data = array();
        $semi_product_data = array();
        foreach ($data as $order) {
            $end_products = EndProduct::select("end_product.name", "po_end_product.total")
                ->join("po_end_product", "po_end_product.end_product_id", "=", "end_product.id")
                ->where("po_end_product.end_product_id", "=", $order->id)
                ->get();
            foreach ($end_products as $end_product) {
                array_push($end_product_data, array($order->id, $end_product->name, $end_product->total));
            }

            $semi_products = SemiFinishProduct::select("semi_finish_product.name", "po_semi.semi_total")
                ->join("po_semi", "po_semi.semi_product_id", "=", "semi_finish_product.semi_finish_product_id")
                ->where("po_semi.production_order_id", "=", $order->id)
                ->get();
            foreach ($semi_products as $semi_product) {
                array_push($semi_product_data, array($semi_product->name, $semi_product->semi_total));
            }
        }

        $pdf = PDF::loadView('merged_sales_requests_download', compact('end_product_data', 'semi_product_data'))->setPaper('a4', 'portrait');
        return $pdf->download($request->production_order_id . '-PO.pdf');
    }

    public function mergedSalesRequestsReport(Request $request)
    {
        return view();
    }

    public function salesReportExcelExport(Request $request)
    {
        $orders = $request->orders;
        return Excel::download(new SalesRequestExport($orders), 'sales_request.xlsx');
    }
}
