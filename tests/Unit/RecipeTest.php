<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class RecipeTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRecipeStore()
    {
        $response = $this->json('POST', 'recipe', [
            'recipe_name' => 'Sally',
            'desc' => 'Sally',
            'semi_product' => 2,
        ]);
        $response->assertStatus(302)
            ->assertRedirect('recipe/create')
            ->assertSessionHas('success.title', $value = 'Congratulations !');
    }
}
