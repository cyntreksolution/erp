<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EndProductTest extends TestCase
{
    use WithoutMiddleware;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testEndProduct()
    {
        $response = $this->json('POST', 'end_product', [
            'end_product_name' => 'Sally',
            'barcode' => 12345,
            'weight' => 2,
            'buffer_stock' => 2,
            'unit_price' => 2,
            'desc' => 'dsa',
            'image' => 'abc.jpg',
        ]);
        $response->assertStatus(200);
    }
}
